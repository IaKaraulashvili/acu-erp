﻿using System;

namespace AG.Utils
{
    public static class DateTimeHelper
    {
        public static DateTime ConstructDateTime(DateTime date, int time)
        {
            return new DateTime(date.Year, date.Month, date.Day, time / 60, time % 60, 0);
        }
    }
}

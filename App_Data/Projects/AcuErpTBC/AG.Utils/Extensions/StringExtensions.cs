﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.Utils.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string val)
        {
            return string.IsNullOrEmpty(val);
        }

        public static DateTime? ToDateTime(this string val, DateTime? defaultValue = null, IFormatProvider format = null)
        {
            if (val.IsNullOrEmpty())
            {
                return defaultValue;
            }

            var culture = format ?? CultureInfo.CurrentCulture;
            
            return DateTime.Parse(val, culture);
        }

        public static decimal? ToDecimal(this string val, decimal? defaultValue = null, IFormatProvider format = null)
        {
            if (val.IsNullOrEmpty())
            {
                return defaultValue;
            }

            var culture = format ?? CultureInfo.CurrentCulture;

            return decimal.Parse(val, culture);
        }

        public static int? ToInt(this string val, int? defaultValue = null)
        {
            if (val.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return int.Parse(val);
        }

        public static string NullIfEmpty(this string val)
        {
            return string.IsNullOrWhiteSpace(val) ? null : val;
        }
    }
}

using PX.Data;
using AG.EA.DAC;

namespace AG.EA
{
    public class AssetClassMaint : AGGraph<AssetClassMaint, EAAssetClass>
    {

        public PXSelect<EAAssetClass> AssetClasses;

        public PXSelect<EAAssetClass, Where<EAAssetClass.assetClassID, Equal<Current<EAAssetClass.assetClassID>>>> CurrentAssetClass;
    }
}
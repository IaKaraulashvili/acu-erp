using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.EA.Shared;
using System.Linq;
using AG.EA.DAC;
using AG.RS.DAC;

namespace AG.EA
{
    public class AssetEntry : AGGraph<AssetEntry, EAAsset>
    {
        #region Data Views

        public PXSelect<EAAsset> Assets;
        public PXSelect<EAAsset, Where<EAAsset.assetID, Equal<Current<EAAsset.assetID>>>> CurrentAsset;
        public PXSelect<EAAssetLocationHistory, Where<EAAssetLocationHistory.assetID, Equal<Optional<EAAsset.assetID>>,
                And<EAAssetLocationHistory.revisionID, Equal<Optional<EAAsset.locationRevID>>>>> AssetLocation;
        public PXSelect<EAAssetLocationHistory,
                Where<EAAssetLocationHistory.assetID, Equal<Current<EAAsset.assetID>>>,
                OrderBy<Desc<EAAssetLocationHistory.revisionID>>> LocationHistory;

        public PXSelectReadonly2<EAAssetWaybill,
            LeftJoin<RSWaybill, On<RSWaybill.waybillNbr, Equal<EAAssetWaybill.waybillCD>>>,
            Where<EAAssetWaybill.assetID, Equal<Current<EAAsset.assetID>>>> Waybills;

        public PXSetup<EAAssetSetup> Setup;

        public PXFilter<DisposalDetails> DisposalDialog;



        #endregion

        #region ctor

        public AssetEntry()
        {
            var setup = Setup.Current;

            actionsMenu.AddMenuAction(placeInService);
            actionsMenu.AddMenuAction(prepareForDisposal);
            actionsMenu.AddMenuAction(disposeAsset);
        }

        #endregion

        #region EAAsset Events

        protected virtual void EAAsset_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = e.Row as EAAsset;

            if (row == null) return;

            var onHold = row.Status == EAAssetStatus.Hold;
            var forDisposal = row.Status == EAAssetStatus.ForDisposal;
            var disposed = row.Status == EAAssetStatus.Disposed;

            releaseAsset.SetEnabled(row.CanRelease());
            placeInService.SetEnabled(row.CanPlaceInService());
            prepareForDisposal.SetEnabled(row.CanPrepareForDisposal());
            disposeAsset.SetEnabled(row.CanDispose());

            cache.AllowDelete = onHold;
            cache.AllowUpdate = !forDisposal && !disposed;
            AssetLocation.AllowUpdate = !forDisposal && !disposed;
        }

        protected virtual void EAAsset_RowUpdated(PXCache cache, PXRowUpdatedEventArgs e)
        {
            var row = e.Row as EAAsset;

            if (row == null) return;

            if (!cache.ObjectsEqual<EAAsset.classID>(row, e.OldRow))
            {
                SetAssetClassValues(row);
            }
        }

        protected virtual void EAAsset_RowInserting(PXCache cache, PXRowInsertingEventArgs e)
        {
            var row = e.Row as EAAsset;

            if (row == null) return;

            SetAssetClassValues(row, setDefault: true);
        }

        protected virtual void EAAsset_RowInserted(PXCache sender, PXRowInsertedEventArgs e)
        {
            AssetLocation.Cache.Insert();
            AssetLocation.Cache.IsDirty = false;
        }

        #endregion

        #region EAAssetLocationHistory Events

        protected virtual void EAAssetLocationHistory_RowUpdating(PXCache sender, PXRowUpdatingEventArgs e)
        {
            EAAssetLocationHistory location = e.NewRow as EAAssetLocationHistory;
            if (location == null) return;

            if (IsLocationChanged(this, e.Row as EAAssetLocationHistory, location))
            {
                EAAssetLocationHistory newLocation = PXCache<EAAssetLocationHistory>.CreateCopy(location);
                try
                {
                    sender.SmartSetStatus(e.Row, PXEntryStatus.Notchanged, PXEntryStatus.Updated);
                    newLocation.RevisionID = CurrentAsset.Current.LocationRevID;
                    newLocation.TransactionDate = Accessinfo.BusinessDate;
                    newLocation = (EAAssetLocationHistory)sender.Insert(newLocation);
                }
                finally
                {
                    if (!(e.Cancel = (newLocation != null)))
                    {
                        sender.SmartSetStatus(e.Row, PXEntryStatus.Updated);
                    }
                }
            }
        }

        protected virtual void EAAssetLocationHistory_RowInserting(PXCache sender, PXRowInsertingEventArgs e)
        {
            EAAssetLocationHistory newLocation = (EAAssetLocationHistory)e.Row;
            if (newLocation == null) return;

            foreach (EAAssetLocationHistory row in sender.Cached)
            {
                PXEntryStatus status = sender.GetStatus(row);
                if (status == PXEntryStatus.Inserted || status == PXEntryStatus.Updated)
                {
                    CopyLocation(newLocation, row);
                    e.Cancel = true;
                    break;
                }
            }

            if (!e.Cancel)
            {
                if (CurrentAsset.Current == null)
                {
                    CurrentAsset.Current = CurrentAsset.Select();
                }
                CurrentAsset.Current.LocationRevID = ++newLocation.RevisionID;
                CurrentAsset.Cache.Update(CurrentAsset.Current);
            }
        }

        protected virtual void EAAssetLocationHistory_EmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            EAAssetLocationHistory hist = (EAAssetLocationHistory)e.Row;
            sender.SetDefaultExt<EAAssetLocationHistory.branchID>(hist);
            sender.SetDefaultExt<EAAssetLocationHistory.department>(hist);
        }

        protected virtual void EAAssetLocationHistory_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.ExternalCall)
            {
                sender.SetDefaultExt<EAAssetLocationHistory.buildingID>(e.Row);
                sender.SetValuePending<EAAssetLocationHistory.buildingID>(e.Row, null);
            }
        }

        protected virtual void EAAssetLocationHistory_BuildingID_FieldVerifying(PXCache sender, PXFieldVerifyingEventArgs e)
        {
            if (!e.ExternalCall) e.Cancel = true;
        }

        #endregion

        #region Actions

        public PXAction<EAAsset> releaseAsset;
        [PXUIField(DisplayName = "Release", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXProcessButton]
        public virtual IEnumerable ReleaseAsset(PXAdapter adapter)
        {
            this.Save.Press();

            PXLongOperation.StartOperation(this, delegate { ReleaseAssetImpl(CurrentAsset.Current); });

            return new List<EAAsset> { CurrentAsset.Current };
        }

        public void ReleaseAssetImpl(EAAsset asset)
        {
            if (!asset.CanRelease())
            {
                throw new PXException(AG.Common.Messages.AssetInappropriateStatus);
            }

            asset.Status = EAAssetStatus.Active;
            asset.IssueDate = Accessinfo.BusinessDate;

            CurrentAsset.Update(asset);

            Save.Press();
        }

        public PXAction<EAAsset> placeInService;
        [PXUIField(DisplayName = "Place In Service", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXProcessButton]
        public virtual IEnumerable PlaceInService(PXAdapter adapter)
        {
            Save.Press();

            PXLongOperation.StartOperation(this, delegate { PlaceInServiceImpl(CurrentAsset.Current); });

            return adapter.Get();
        }
        public void PlaceInServiceImpl(EAAsset asset, bool isMassProcess = false)
        {
            if (!asset.CanPlaceInService())
            {
                throw new PXException(AG.Common.Messages.AssetInappropriateStatus);
            }

            asset.Status = EAAssetStatus.InService;
            asset.PlacedInServiceDate = Accessinfo.BusinessDate;

            CurrentAsset.Update(asset);

            Save.Press();
            //reveiw test
            //review 2
            if (isMassProcess)
            {
                PXProcessing.SetInfo(String.Format("Asset {0} has been successfully proceed.", asset.AssetCD));
            }
        }

        public PXAction<EAAsset> prepareForDisposal;
        [PXUIField(DisplayName = "Prepare For Disposal", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXProcessButton]
        public virtual IEnumerable PrepareForDisposal(PXAdapter adapter)
        {
            Save.Press();

            PXLongOperation.StartOperation(this, delegate { PrepareForDisposalImpl(CurrentAsset.Current); });

            return adapter.Get();
        }
        public void PrepareForDisposalImpl(EAAsset asset)
        {
            if (!asset.CanPrepareForDisposal())
            {
                throw new PXException(AG.Common.Messages.AssetInappropriateStatus);
            }

            asset.Status = EAAssetStatus.ForDisposal;

            CurrentAsset.Update(asset);

            Save.Press();
        }

        public PXAction<EAAsset> disposeAsset;
        [PXUIField(DisplayName = "Dispose", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton(CommitChanges = true)]
        public virtual IEnumerable DisposeAsset(PXAdapter adapter)
        {
            if (DisposalDialog.AskExt(true) != WebDialogResult.OK || DisposalDialog?.Current?.DisposalReason == null) return adapter.Get();

            Save.Press();

            PXLongOperation.StartOperation(this, delegate { DisposeAssetImpl(CurrentAsset.Current, DisposalDialog.Current.DisposalReason); });

            return adapter.Get();
        }

        public void DisposeAssetImpl(EAAsset asset, string reason)
        {

            if (!asset.CanDispose())
            {
                throw new PXException(AG.Common.Messages.AssetInappropriateStatus);
            }

            asset.Status = EAAssetStatus.Disposed;
            asset.DisposalDate = Accessinfo.BusinessDate;
            asset.DisposalReason = reason;

            CurrentAsset.Update(asset);

            Save.Press();
        }

        public PXAction<EAAsset> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        #endregion

        #region Methods

        public void SetAssetClassValues(EAAsset asset, bool setDefault = false)
        {
            EAAssetClass assetClass = null;

            if (setDefault)
            {
                asset.ClassID = Setup.Current.DefAssetClassID;
            }

            if (asset.ClassID != null)
            {
                assetClass = PXSelect<EAAssetClass,
                    Where<EAAssetClass.assetClassID, Equal<Required<EAAssetClass.assetClassID>>>>
                    .Select(this, asset.ClassID).FirstOrDefault();
            }

            asset.State = assetClass?.State;
            asset.Category = assetClass?.Category;
            asset.Condition = assetClass?.Condition;
            asset.UOM = assetClass?.UOM;
        }

        private static void CopyLocation(EAAssetLocationHistory from, EAAssetLocationHistory to)
        {
            to.BranchID = from.BranchID;
            to.BuildingID = from.BuildingID;
            to.Floor = from.Floor;
            to.Room = from.Room;
            to.EmployeeID = from.EmployeeID;
            to.Department = from.Department;
            to.SiteID = from.SiteID;
            to.Reason = from.Reason;
        }

        public static bool IsLocationChanged(PXGraph graph, EAAssetLocationHistory current, EAAssetLocationHistory prev)
        {
            return !graph.Caches<EAAssetLocationHistory>().ObjectsEqual<
                EAAssetLocationHistory.branchID,
                EAAssetLocationHistory.buildingID,
                EAAssetLocationHistory.floor,
                EAAssetLocationHistory.room,
                EAAssetLocationHistory.employeeID,
                EAAssetLocationHistory.department,
                EAAssetLocationHistory.siteID>(prev, current);
        }

        public EAAsset FindById(int assetID)
        {
            return PXSelect<EAAsset, 
                Where<EAAsset.assetID, Equal<Required<EAAsset.assetID>>>>.Select(this, assetID);
        }

        #endregion

        #region Classes

        public class DisposalDetails : IBqlTable
        {
            #region DisposalReason
            public abstract class disposalReason : PX.Data.IBqlField
            {
            }
            protected string _DisposalReason;
            [PXString(250, IsUnicode = true)]
            [PXDefault()]
            [PXUIField(DisplayName = "Disposal Reason")]
            public virtual string DisposalReason
            {
                get
                {
                    return this._DisposalReason;
                }
                set
                {
                    this._DisposalReason = value;
                }
            }
            #endregion
        }

        #endregion
    }
}
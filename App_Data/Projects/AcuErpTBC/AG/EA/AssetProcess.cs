using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.EA.Shared;
using AG.EA.DAC;

namespace AG.EA
{
    public class AssetProcess : AGGraph<AssetProcess>
    {

        public PXSetup<EAAssetSetup> Setup;

        public PXProcessingJoin<EAAsset, LeftJoin<EAAssetLocationHistory,
            On<EAAssetLocationHistory.assetID, Equal<EAAsset.assetID>,
                And<EAAssetLocationHistory.revisionID, Equal<EAAsset.locationRevID>>>>,
            Where<EAAsset.status, Equal<EAAssetStatus.active>,
                And<Where<DateDiff<EAAsset.lastTransportationDate, Current<AccessInfo.businessDate>, DateDiff.day>, Greater<Current<EAAssetSetup.placeInServiceAfterDays>>,
                    Or<DateDiff<EAAsset.issueDate, Current<AccessInfo.businessDate>, DateDiff.day>, Greater<Current<EAAssetSetup.placeInServiceAfterDays>>>>>>> Assets;



        public AssetProcess()
        {
            var setup = Setup.Current;
            Assets.SetProcessDelegate<AssetEntry>(delegate (AssetEntry graph, EAAsset asset)
            {
                graph.Clear();
                graph.PlaceInServiceImpl(asset);
            });
        }

        //public class EAAssetSelected : EAAsset
        //{
        //    #region Selected
        //    public abstract class selected : IBqlField
        //    {
        //    }
        //    [PXBool]
        //    [PXUIField(DisplayName = "Selected")]
        //    public virtual bool? Selected { get; set; }
        //    #endregion
        //}
    }
}
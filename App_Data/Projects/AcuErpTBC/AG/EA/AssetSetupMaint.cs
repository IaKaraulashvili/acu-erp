using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.EA.DAC;

namespace AG.EA
{
    public class AssetSetupMaint : AGGraph<AssetSetupMaint>
    {
        public PXSelect<EAAssetSetup> Setup;
        public PXSave<EAAssetSetup> Save;
        public PXCancel<EAAssetSetup> Cancel;

        protected virtual void EAAssetSetup_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (EAAssetSetup)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<EAAssetSetup.lCABulkRegistrationAutoRelease>(sender, row, !row.LCABulkRegistrationHoldOnEntry.GetValueOrDefault() && row.LCABulkRegistrationAutoCreate.GetValueOrDefault());
            PXUIFieldAttribute.SetEnabled<EAAssetSetup.assetAutoRelease>(sender, row, !row.AssetHoldOnEntry.GetValueOrDefault());

            if (row.LCABulkRegistrationHoldOnEntry.GetValueOrDefault() || !row.LCABulkRegistrationAutoCreate.GetValueOrDefault())
            {
                row.LCABulkRegistrationAutoRelease = false;
            }

            if (row.AssetHoldOnEntry.GetValueOrDefault())
            {
                row.AssetAutoRelease = false;
            }
        }
    }
}
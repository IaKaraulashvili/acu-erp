﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using Shared;
    using PX.Objects.IN;
    using PX.Objects.CS;
    using PX.Objects.CM;

    [PXPrimaryGraph(typeof(AssetEntry))]
    [System.SerializableAttribute()]
    [PXEMailSource]
    public class EAAsset : PX.Data.IBqlTable
    {
        #region Selected
        public abstract class selected : IBqlField
        {
        }
        [PXBool]
        [PXUIField(DisplayName = "Selected")]
        public virtual bool? Selected { get; set; }
        #endregion

        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region AssetCD
        public abstract class assetCD : PX.Data.IBqlField
        {
        }
        protected string _AssetCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXSelector(typeof(Search<EAAsset.assetCD>))]
        [AutoNumber(typeof(EAAssetSetup.assetNumberingID), typeof(EAAsset.createdDateTime))]
        [PXUIField(DisplayName = "Asset ID", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string AssetCD
        {
            get
            {
                return this._AssetCD;
            }
            set
            {
                this._AssetCD = value;
            }
        }
        #endregion
        #region Description
        public abstract class description : PX.Data.IBqlField
        {
        }
        protected string _Description;
        [PXDBString(250, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                this._Description = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(1, IsFixed = true)]
        [EAAssetStatusList]
        [SetStatus]
        [PXUIField(DisplayName = "Status", Enabled = false, Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected bool? _Hold;
        [PXDBBool()]
        [PXDefault(typeof(EAAssetSetup.assetHoldOnEntry))]
        [PXUIField(DisplayName = "Hold")]
        public virtual bool? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion
        #region ClassID
        public abstract class classID : PX.Data.IBqlField
        {
        }
        protected string _ClassID;
        [PXDBString(15, IsUnicode = true)]
        [EAAssetClassSelector]
        [PXUIField(DisplayName = "Asset Class", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string ClassID
        {
            get
            {
                return this._ClassID;
            }
            set
            {
                this._ClassID = value;
            }
        }
        #endregion
        #region State
        public abstract class state : PX.Data.IBqlField
        {
        }
        protected string _State;
        [PXDBString(1, IsFixed = true)]
        [EAAssetStateList]
        [PXUIField(DisplayName = "Asset State")]
        public virtual string State
        {
            get
            {
                return this._State;
            }
            set
            {
                this._State = value;
            }
        }
        #endregion
        #region Category
        public abstract class category : PX.Data.IBqlField
        {
        }
        protected string _Category;
        [PXDBString(2, IsFixed = true)]
        [EAAssetCategoryList]
        [PXUIField(DisplayName = "Asset Category")]
        public virtual string Category
        {
            get
            {
                return this._Category;
            }
            set
            {
                this._Category = value;
            }
        }
        #endregion
        #region Condition
        public abstract class condition : PX.Data.IBqlField
        {
        }
        protected string _Condition;
        [PXDBString(1, IsFixed = true)]
        [EAAssetConditionList]
        [PXUIField(DisplayName = "Condition")]
        public virtual string Condition
        {
            get
            {
                return this._Condition;
            }
            set
            {
                this._Condition = value;
            }
        }
        #endregion
        #region UOM
        public abstract class uOM : PX.Data.IBqlField
        {
        }
        protected string _UOM;
        [INUnit]
        public virtual string UOM
        {
            get
            {
                return this._UOM;
            }
            set
            {
                this._UOM = value;
            }
        }
        #endregion
        #region Cost
        public abstract class cost : PX.Data.IBqlField
        {
        }
        protected decimal? _Cost;
        [PXDBBaseCury]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Asset Cost")]
        public virtual decimal? Cost
        {
            get
            {
                return this._Cost;
            }
            set
            {
                this._Cost = value;
            }
        }
        #endregion
        #region Qty
        public abstract class qty : PX.Data.IBqlField
        {
        }
        protected decimal? _Qty;
        [PXDBQuantity(MinValue = 1)]
        [PXDefault(TypeCode.Decimal, "1.0")]
        [PXUIField(DisplayName = "Quantity")]
        public virtual decimal? Qty
        {
            get
            {
                return this._Qty;
            }
            set
            {
                this._Qty = value;
            }
        }
        #endregion
        #region IssueDate
        public abstract class issueDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _IssueDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Issue Date", Enabled = false)]
        public virtual DateTime? IssueDate
        {
            get
            {
                return this._IssueDate;
            }
            set
            {
                this._IssueDate = value;
            }
        }
        #endregion
        #region PlacedInServiceDate
        public abstract class placedInServiceDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _PlacedInServiceDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Placed In Service Date", Enabled = false)]
        public virtual DateTime? PlacedInServiceDate
        {
            get
            {
                return this._PlacedInServiceDate;
            }
            set
            {
                this._PlacedInServiceDate = value;
            }
        }
        #endregion
        #region DisposalDate
        public abstract class disposalDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _DisposalDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Disposal Date", Enabled = false)]
        public virtual DateTime? DisposalDate
        {
            get
            {
                return this._DisposalDate;
            }
            set
            {
                this._DisposalDate = value;
            }
        }
        #endregion
        #region DisposalReason
        public abstract class disposalReason : PX.Data.IBqlField
        {
        }
        protected string _DisposalReason;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Disposal Reason")]
        public virtual string DisposalReason
        {
            get
            {
                return this._DisposalReason;
            }
            set
            {
                this._DisposalReason = value;
            }
        }
        #endregion
        #region LocationRevID
        public abstract class locationRevID : IBqlField
        {
        }
        protected Int32? _LocationRevID;
        /// <summary>
        /// The number of the actual revision of the asset location.
        /// This field is a part of the compound reference to <see cref="EAAssetLocationHistory"/>.
        /// The full reference contains the <see cref="assetID"/> and <see cref="locationRevID"/> fields.
        /// </summary>
        [PXDBInt]
        public virtual Int32? LocationRevID
        {
            get
            {
                return _LocationRevID;
            }
            set
            {
                _LocationRevID = value;
            }
        }
        #endregion
        #region LastTransportationDate
        public abstract class lastTransportationDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastTransportationDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Last Transportation Date", Enabled = false)]
        public virtual DateTime? LastTransportationDate
        {
            get
            {
                return this._LastTransportationDate;
            }
            set
            {
                this._LastTransportationDate = value;
            }
        }
        #endregion
        #region InventoryID
        public abstract class inventoryID : PX.Data.IBqlField
        {
        }
        protected int? _InventoryID;
        [StockItem(Enabled = false)]
        public virtual int? InventoryID
        {
            get
            {
                return this._InventoryID;
            }
            set
            {
                this._InventoryID = value;
            }
        }
        #endregion
        #region LotSerialNbr
        public abstract class lotSerialNbr : PX.Data.IBqlField
        {
        }
        protected string _LotSerialNbr;
        [LotSerialNbr]
        public virtual string LotSerialNbr
        {
            get
            {
                return this._LotSerialNbr;
            }
            set
            {
                this._LotSerialNbr = value;
            }
        }
        #endregion
        #region LCARegistrationID
        public abstract class lCARegistrationID : PX.Data.IBqlField
        {
        }
        protected int? _LCARegistrationID;
        [PXDBInt()]
        [EALCARegistrationSelector]
        [PXUIField(DisplayName = "Registration Ref. Nbr", Enabled = false)]
        public virtual int? LCARegistrationID
        {
            get
            {
                return this._LCARegistrationID;
            }
            set
            {
                this._LCARegistrationID = value;
            }
        }
        #endregion
        #region LCADisposalID
        public abstract class lCADisposalID : PX.Data.IBqlField
        {
        }
        protected int? _LCADisposalID;
        [PXDBInt()]
        [EALCADisposalSelector]
        [PXUIField(DisplayName = "Disposal Ref. Nbr", Enabled = false)]
        public virtual int? LCADisposalID
        {
            get
            {
                return this._LCADisposalID;
            }
            set
            {
                this._LCADisposalID = value;
            }
        }
        #endregion
        #region ExtRefID
        public abstract class extRefID : PX.Data.IBqlField
        {
        }
        protected string _ExtRefID;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Ext. Ref. ID")]
        public virtual string ExtRefID
        {
            get
            {
                return this._ExtRefID;
            }
            set
            {
                this._ExtRefID = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region Methods

        public bool CanRelease()
        {
            return Status == EAAssetStatus.Open;
        }

        public bool CanPlaceInService()
        {
            return Status == EAAssetStatus.Active
                || Status == EAAssetStatus.ForDisposal;
        }

        public bool CanPrepareForDisposal()
        {
            return Status == EAAssetStatus.Active
                || Status == EAAssetStatus.InService;
        }

        public bool CanDispose()
        {
            return Status == EAAssetStatus.Active
                || Status == EAAssetStatus.InService
                || Status == EAAssetStatus.ForDisposal;
        }

        #endregion

        #region Attributes

        public class SetStatusAttribute : PXEventSubscriberAttribute, IPXFieldDefaultingSubscriber
        {
            public override void CacheAttached(PXCache sender)
            {
                base.CacheAttached(sender);

                sender.Graph.FieldUpdating.AddHandler(
                    sender.GetItemType(),
                    nameof(EAAsset.hold),
                    (cache, e) =>
                    {
                        PXBoolAttribute.ConvertValue(e);

                        var row = e.Row as EAAsset;

                        if (row != null)
                        {
                            SetStatus(cache, row, (bool?)e.NewValue);
                        }
                    });

                sender.Graph.RowSelected.AddHandler(
                    sender.GetItemType(),
                    (cache, e) =>
                    {
                        var row = e.Row as EAAsset;

                        if (row != null)
                        {
                            PXUIFieldAttribute.SetEnabled<EAAsset.hold>(cache, row, row.Status == EAAssetStatus.Hold || row.Status == EAAssetStatus.Open);
                        }
                    });
            }

            public virtual void FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
            {
                var row = e.Row as EAAsset;

                if (row == null) return;

                var setup = cache.Graph.Caches[typeof(EAAssetSetup)].Current as EAAssetSetup;
                if (setup == null)
                    return;

                SetStatus(cache, row, setup.AssetHoldOnEntry);

                e.NewValue = row.Status;
                e.Cancel = true;
            }

            protected virtual void SetStatus(PXCache cache, EAAsset row, bool? hold)
            {
                if (row == null)
                    return;

                row.Status = hold == true ? EAAssetStatus.Hold : EAAssetStatus.Open;
            }
        }

        #endregion
    }
}

﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using Shared;

    [PXPrimaryGraph(typeof(AssetClassMaint))]
    [System.SerializableAttribute()]
	public class EAAssetClass : PX.Data.IBqlTable
	{  
        #region AssetClassCD
        public abstract class assetClassID : PX.Data.IBqlField
		{
		}
		protected string _AssetClassID;
		[PXDBString(15, IsUnicode = true, IsKey = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "ID", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<EAAssetClass.assetClassID>))]
		public virtual string AssetClassID
		{
			get
			{
				return this._AssetClassID;
			}
			set
			{
				this._AssetClassID = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(250, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
        #endregion
        #region State
        public abstract class state : PX.Data.IBqlField
        {
        }
        protected string _State;
        [PXDBString(2, IsFixed = true)]
        [PXUIField(DisplayName = "Asset State", Visibility = PXUIVisibility.SelectorVisible)]
        [EAAssetStateList]
        [PXDefault(EAAssetState.New, PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual string State
        {
            get
            {
                return this._State;
            }
            set
            {
                this._State = value;
            }
        }
        #endregion
        #region Condition
        public abstract class condition : PX.Data.IBqlField
		{
		}
		protected string _Condition;
		[PXDBString(1, IsFixed = true)]
		[PXUIField(DisplayName = "Condition", Visibility = PXUIVisibility.SelectorVisible)]
        [EAAssetConditionList]
        [PXDefault(EAAssetCondition.Good, PersistingCheck = PXPersistingCheck.Nothing)]
		public virtual string Condition
		{
			get
			{
				return this._Condition;
			}
			set
			{
				this._Condition = value;
			}
		}
		#endregion
		#region Category
		public abstract class category : PX.Data.IBqlField
		{
		}
		protected string _Category;
		[PXDBString(2, IsFixed = true)]
		[PXUIField(DisplayName = "Asset Category", Visibility = PXUIVisibility.SelectorVisible)]
        [EAAssetCategoryList]
        [PXDefault(EAAssetCategory.Category1, PersistingCheck = PXPersistingCheck.Nothing)]
		public virtual string Category
		{
			get
			{
				return this._Category;
			}
			set
			{
				this._Category = value;
			}
		}
		#endregion
		#region UOM
		public abstract class uOM : PX.Data.IBqlField
		{
		}
		protected string _UOM;
        [INUnit(Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string UOM
		{
			get
			{
				return this._UOM;
			}
			set
			{
				this._UOM = value;
			}
		}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
    }
}

﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.GL;
    using PX.Objects.CR;
    using PX.Objects.EP;
    using PX.Objects.IN;

    [System.SerializableAttribute()]
    public class EAAssetLocationHistory : PX.Data.IBqlTable
    {
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXUIField(Visible = false, Visibility = PXUIVisibility.Invisible)]
        [PXParent(typeof(Select<EAAsset, Where<EAAsset.assetID, Equal<Current<EAAssetLocationHistory.assetID>>>>))]
        [PXDBLiteDefault(typeof(EAAsset.assetID))]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region RevisionID
        public abstract class revisionID : PX.Data.IBqlField
        {
        }
        protected int? _RevisionID;
        [PXDBInt(IsKey = true)]
        [PXDefault(0)]
        public virtual int? RevisionID
        {
            get
            {
                return this._RevisionID;
            }
            set
            {
                this._RevisionID = value;
            }
        }
        #endregion
        #region TransactionDate
        public abstract class transactionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _TransactionDate;
        [PXDBDate()]
        [PXDefault(typeof(AccessInfo.businessDate))]
        [PXUIField(DisplayName = "Transaction Date")]
        public virtual DateTime? TransactionDate
        {
            get
            {
                return this._TransactionDate;
            }
            set
            {
                this._TransactionDate = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected int? _BranchID;
        [Branch(typeof(Coalesce<
            Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>, 
                Where<EPEmployee.bAccountID, Equal<Current<EAAssetLocationHistory.employeeID>>>>,
            Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false)]
        public virtual int? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region BuildingID
        public abstract class buildingID : PX.Data.IBqlField
        {
        }
        protected int? _BuildingID;
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<EAAssetLocationHistory.branchID>>>>),
            SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXDefault]
        [PXUIField(DisplayName = "Building")]
        public virtual int? BuildingID
        {
            get
            {
                return this._BuildingID;
            }
            set
            {
                this._BuildingID = value;
            }
        }
        #endregion
        #region Floor
        public abstract class floor : PX.Data.IBqlField
        {
        }
        protected string _Floor;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Floor")]
        public virtual string Floor
        {
            get
            {
                return this._Floor;
            }
            set
            {
                this._Floor = value;
            }
        }
        #endregion
        #region Room
        public abstract class room : PX.Data.IBqlField
        {
        }
        protected string _Room;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Room")]
        public virtual string Room
        {
            get
            {
                return this._Room;
            }
            set
            {
                this._Room = value;
            }
        }
        #endregion
        #region EmployeeID
        public abstract class employeeID : PX.Data.IBqlField
        {
        }
        protected int? _EmployeeID;
        [PXDBInt()]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        [PXDefault]
        [PXUIField(DisplayName = "Custodian")]
        public virtual int? EmployeeID
        {
            get
            {
                return this._EmployeeID;
            }
            set
            {
                this._EmployeeID = value;
            }
        }
        #endregion
        #region Custodian
        public abstract class custodian : PX.Data.IBqlField
        {
        }
        protected Guid? _Custodian;
        [PXDBField()]
        [PXFormula(typeof(Selector<EAAssetLocationHistory.employeeID, EPEmployee.userID>))]
        public virtual Guid? Custodian
        {
            get
            {
                return this._Custodian;
            }
            set
            {
                this._Custodian = value;
            }
        }
        #endregion
        #region Department
        public abstract class department : PX.Data.IBqlField
        {
        }
        protected string _Department;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<EAAssetLocationHistory.employeeID>>>>))]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        [PXUIField(DisplayName = "Department")]
        public virtual string Department
        {
            get
            {
                return this._Department;
            }
            set
            {
                this._Department = value;
            }
        }
        #endregion
        #region SiteID
        public abstract class siteID : PX.Data.IBqlField
        {
        }
        protected int? _SiteID;
        [Site]
        public virtual int? SiteID
        {
            get
            {
                return this._SiteID;
            }
            set
            {
                this._SiteID = value;
            }
        }
        #endregion
        #region Reason
        public abstract class reason : PX.Data.IBqlField
        {
        }
        protected string _Reason;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Reason")]
        public virtual string Reason
        {
            get
            {
                return this._Reason;
            }
            set
            {
                this._Reason = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        [PXUIField(DisplayName = "Modification Date")]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}

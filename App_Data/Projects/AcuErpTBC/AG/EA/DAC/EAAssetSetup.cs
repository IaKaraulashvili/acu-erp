﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.CS;
    using Shared;

    [System.SerializableAttribute()]
    public class EAAssetSetup : PX.Data.IBqlTable
    {
        #region AssetNumberingID
        public abstract class assetNumberingID : PX.Data.IBqlField
        {
        }
        protected string _AssetNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault()]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Asset Numbering")]
        public virtual string AssetNumberingID
        {
            get
            {
                return this._AssetNumberingID;
            }
            set
            {
                this._AssetNumberingID = value;
            }
        }
        #endregion
        #region LCABulkRegistrationNumberingID
        public abstract class lCABulkRegistrationNumberingID : PX.Data.IBqlField
        {
        }
        protected string _LCABulkRegistrationNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault()]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "LCA Bulk Registration Numbering")]
        public virtual string LCABulkRegistrationNumberingID
        {
            get
            {
                return this._LCABulkRegistrationNumberingID;
            }
            set
            {
                this._LCABulkRegistrationNumberingID = value;
            }
        }
        #endregion
        #region LCABulkDisposeNumberingID
        public abstract class lCABulkDisposeNumberingID : PX.Data.IBqlField
        {
        }
        protected string _LCABulkDisposeNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault()]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "LCA Bulk Dispose Numbering")]
        public virtual string LCABulkDisposeNumberingID
        {
            get
            {
                return this._LCABulkDisposeNumberingID;
            }
            set
            {
                this._LCABulkDisposeNumberingID = value;
            }
        }
        #endregion
        #region LCABulkTransferNumberingID
        public abstract class lCABulkTransferNumberingID : PX.Data.IBqlField
        {
        }
        protected string _LCABulkTransferNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault()]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "LCA Bulk Transfer Numbering")]
        public virtual string LCABulkTransferNumberingID
        {
            get
            {
                return this._LCABulkTransferNumberingID;
            }
            set
            {
                this._LCABulkTransferNumberingID = value;
            }
        }
        #endregion
        #region LCAOffBalanceAccount
        public abstract class lCAOffBalanceAccount : PX.Data.IBqlField
        {
        }
        protected string _LCAOffBalanceAccount;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "ALTA LCA Off-Balance Account")]
        public virtual string LCAOffBalanceAccount
        {
            get
            {
                return this._LCAOffBalanceAccount;
            }
            set
            {
                this._LCAOffBalanceAccount = value;
            }
        }
        #endregion
        #region LCATransitOffBalanceAccount
        public abstract class lCATransitOffBalanceAccount : PX.Data.IBqlField
        {
        }
        protected string _LCATransitOffBalanceAccount;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "ALTA LCA Transit Off-Balance Account")]
        public virtual string LCATransitOffBalanceAccount
        {
            get
            {
                return this._LCATransitOffBalanceAccount;
            }
            set
            {
                this._LCATransitOffBalanceAccount = value;
            }
        }
        #endregion
        #region DefAssetClassID
        public abstract class defAssetClassID : PX.Data.IBqlField
        {
        }
        protected string _DefAssetClassID;
        [PXDBString(15, IsUnicode = true)]
        [PXDefault()]
        [EAAssetClassSelector]
        [PXUIField(DisplayName = "Default Asset Class")]
        public virtual string DefAssetClassID
        {
            get
            {
                return this._DefAssetClassID;
            }
            set
            {
                this._DefAssetClassID = value;
            }
        }
        #endregion
        #region PlaceInServiceAfterDays
        public abstract class placeInServiceAfterDays : PX.Data.IBqlField
        {
        }
        protected int? _PlaceInServiceAfterDays;
        [PXDBInt()]
        [PXUIField(DisplayName = "Place In Service After")]
        public virtual int? PlaceInServiceAfterDays
        {
            get
            {
                return this._PlaceInServiceAfterDays;
            }
            set
            {
                this._PlaceInServiceAfterDays = value;
            }
        }
        #endregion
        #region AssetHoldOnEntry
        public abstract class assetHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected bool? _AssetHoldOnEntry;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Hold Asset On Entry")]
        public virtual bool? AssetHoldOnEntry
        {
            get
            {
                return this._AssetHoldOnEntry;
            }
            set
            {
                this._AssetHoldOnEntry = value;
            }
        }
        #endregion
        #region LCABulkRegistrationHoldOnEntry
        public abstract class lCABulkRegistrationHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected bool? _LCABulkRegistrationHoldOnEntry;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Hold LCA Bulk Registration On Entry")]
        public virtual bool? LCABulkRegistrationHoldOnEntry
        {
            get
            {
                return this._LCABulkRegistrationHoldOnEntry;
            }
            set
            {
                this._LCABulkRegistrationHoldOnEntry = value;
            }
        }
        #endregion
        #region LCABulkDisposeHoldOnEntry
        public abstract class lCABulkDisposeHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected bool? _LCABulkDisposeHoldOnEntry;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Hold LCA Bulk Disposal On Entry")]
        public virtual bool? LCABulkDisposeHoldOnEntry
        {
            get
            {
                return this._LCABulkDisposeHoldOnEntry;
            }
            set
            {
                this._LCABulkDisposeHoldOnEntry = value;
            }
        }
        #endregion
        #region LCABulkTransferHoldOnEntry
        public abstract class lCABulkTransferHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected bool? _LCABulkTransferHoldOnEntry;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Hold LCA Bulk Transfer On Entry")]
        public virtual bool? LCABulkTransferHoldOnEntry
        {
            get
            {
                return this._LCABulkTransferHoldOnEntry;
            }
            set
            {
                this._LCABulkTransferHoldOnEntry = value;
            }
        }
        #endregion
        #region LCABulkRegistrationAutoCreate
        public abstract class lCABulkRegistrationAutoCreate : PX.Data.IBqlField
        {
        }
        protected bool? _LCABulkRegistrationAutoCreate;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Automatically Create LCA Bulk Registration On Inventory Issue")]
        public virtual bool? LCABulkRegistrationAutoCreate
        {
            get
            {
                return this._LCABulkRegistrationAutoCreate;
            }
            set
            {
                this._LCABulkRegistrationAutoCreate = value;
            }
        }
        #endregion
        #region LCABulkRegistrationAutoRelease
        public abstract class lCABulkRegistrationAutoRelease : PX.Data.IBqlField
        {
        }
        protected bool? _LCABulkRegistrationAutoRelease;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Automatically Release LCA Bulk Registration on Inventory Issue")]
        public virtual bool? LCABulkRegistrationAutoRelease
        {
            get
            {
                return this._LCABulkRegistrationAutoRelease;
            }
            set
            {
                this._LCABulkRegistrationAutoRelease = value;
            }
        }
        #endregion
        #region AssetAutoRelease
        public abstract class assetAutoRelease : PX.Data.IBqlField
        {
        }
        protected bool? _AssetAutoRelease;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Automatically Release Assets on LCA Bulk Registration")]
        public virtual bool? AssetAutoRelease
        {
            get
            {
                return this._AssetAutoRelease;
            }
            set
            {
                this._AssetAutoRelease = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}

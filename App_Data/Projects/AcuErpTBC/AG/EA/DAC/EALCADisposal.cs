﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using Shared;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(LCADisposalEntry))]
    public class EALCADisposal : PX.Data.IBqlTable
	{
        #region LCADisposalID
        public abstract class lCADisposalID : PX.Data.IBqlField
        {
        }
        protected int? _LCADisposalID;
        [PXDBIdentity()]
        [PXUIField(Enabled =false)]
        public virtual int? LCADisposalID
        {
            get
            {
                return this._LCADisposalID;
            }
            set
            {
                this._LCADisposalID = value;
            }
        }
        #endregion
        #region LCADisposalCD
        public abstract class lCADisposalCD : PX.Data.IBqlField
		{
		}
		protected string _LCADisposalCD;
		[PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
		[PXUIField(DisplayName = "Reference Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
        [EALCADisposalNumbering]
        [PXSelector(typeof(Search<EALCADisposal.lCADisposalCD>))]
        public virtual string LCADisposalCD
		{
			get
			{
				return this._LCADisposalCD;
			}
			set
			{
				this._LCADisposalCD = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected string _Status;
		[PXDBString(1, IsUnicode = true, IsFixed = true)]
		[PXUIField(DisplayName = "Status", Enabled = false, Visibility = PXUIVisibility.SelectorVisible)]
		[EALCADisposalStatusList()]
        [SetStatus]
		public virtual string Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region Hold
		public abstract class hold : PX.Data.IBqlField
		{
		}
		protected bool? _Hold;
		[PXDBBool()]
		[PXDefault(typeof(EAAssetSetup.lCABulkDisposeHoldOnEntry))]
        [PXUIField(DisplayName = "Hold")]
        public virtual bool? Hold
		{
			get
			{
				return this._Hold;
			}
			set
			{
				this._Hold = value;
			}
		}
		#endregion
		#region DisposalReason
		public abstract class disposalReason : PX.Data.IBqlField
		{
		}
		protected string _DisposalReason;
		[PXDBString(250, IsUnicode = true)]
		[PXDefault]
		[PXUIField(DisplayName = "Disposal Reason", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual string DisposalReason
		{
			get
			{
				return this._DisposalReason;
			}
			set
			{
				this._DisposalReason = value;
			}
		}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region Methods

        public bool CanPrepareForDisposal()
        {
            return Status == EALCADisposalStatus.Open;
        }

        public bool CanDispose()
        {
            return Status == EALCADisposalStatus.Open || Status == EALCADisposalStatus.ForDisposal;
        }

        #endregion

        #region Attributes

        public class SetStatusAttribute : PXEventSubscriberAttribute, IPXFieldDefaultingSubscriber
        {
            public override void CacheAttached(PXCache sender)
            {
                base.CacheAttached(sender);

                sender.Graph.FieldUpdating.AddHandler(
                    sender.GetItemType(),
                    nameof(EALCADisposal.hold),
                    (cache, e) =>
                    {
                        PXBoolAttribute.ConvertValue(e);

                        var row = e.Row as EALCADisposal;

                        if (row != null)
                        {
                            SetStatus(cache, row, (bool?)e.NewValue);
                        }
                    });

                sender.Graph.RowSelected.AddHandler(
                    sender.GetItemType(),
                    (cache, e) =>
                    {
                        var row = e.Row as EALCADisposal;

                        if (row != null)
                        {
                            PXUIFieldAttribute.SetEnabled<EALCADisposal.hold>(cache, row, row.Status == EALCADisposalStatus.Hold || row.Status == EALCADisposalStatus.Open);
                        }
                    });
            }

            public virtual void FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
            {
                var row = e.Row as EALCADisposal;

                if (row == null) return;

                var setup = cache.Graph.Caches[typeof(EAAssetSetup)].Current as EAAssetSetup;

                SetStatus(cache, row, setup.LCABulkDisposeHoldOnEntry);

                e.NewValue = row.Status;
                e.Cancel = true;
            }

            protected virtual void SetStatus(PXCache cache, EALCADisposal row, bool? hold)
            {
                row.Status = hold == true ? EALCADisposalStatus.Hold : EALCADisposalStatus.Open;
            }
        }

        #endregion
    }
}

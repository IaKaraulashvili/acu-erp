﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using Shared;

    [System.SerializableAttribute()]
    public class EALCADisposalDetail : PX.Data.IBqlTable
    {
        #region LCADisposalID
        public abstract class lCADisposalID : PX.Data.IBqlField
        {
        }
        protected int? _LCADisposalID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(EALCADisposal.lCADisposalID))]
        [PXParent(typeof(Select<EALCADisposal,
                                 Where<EALCADisposal.lCADisposalID, Equal<Current<EALCADisposalDetail.lCADisposalID>>>>))]
        public virtual int? LCADisposalID
        {
            get
            {
                return this._LCADisposalID;
            }
            set
            {
                this._LCADisposalID = value;
            }
        }
        #endregion
        #region LCADisposalDetailID
        public abstract class lCADisposalDetailID : PX.Data.IBqlField
        {
        }
        protected int? _LCADisposalDetailID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? LCADisposalDetailID
        {
            get
            {
                return this._LCADisposalDetailID;
            }
            set
            {
                this._LCADisposalDetailID = value;
            }
        }
        #endregion
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Asset ID")]
        [EAAssetSelector(typeof(Search<EAAsset.assetID,
            Where<EAAsset.status, NotEqual<EAAssetStatus.hold>,
               And<EAAsset.status, NotEqual<EAAssetStatus.open>,
                   And<EAAsset.status, NotEqual<EAAssetStatus.disposed>>>>>))]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion 
    }
}

﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using PX.Objects.CS;
    using Shared;
    using RS.DAC;
    using AG.Utils.Extensions;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(LCARegistrationEntry))]
    public class EALCARegistration : PX.Data.IBqlTable
    {
        #region LCARegistrationID
        public abstract class lCARegistrationID : PX.Data.IBqlField
        {
        }
        protected int? _LCARegistrationID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? LCARegistrationID
        {
            get
            {
                return this._LCARegistrationID;
            }
            set
            {
                this._LCARegistrationID = value;
            }
        }
        #endregion
        #region LCARegistrationCD
        public abstract class lCARegistrationCD : PX.Data.IBqlField
        {
        }
        protected string _LCARegistrationCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Reference Nbr.")]
        [PXSelector(typeof(Search<EALCARegistration.lCARegistrationCD>))]
        [AutoNumber(typeof(EAAssetSetup.lCABulkRegistrationNumberingID), typeof(EALCARegistration.createdDateTime))]
        public virtual string LCARegistrationCD
        {
            get
            {
                return this._LCARegistrationCD;
            }
            set
            {
                this._LCARegistrationCD = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(1, IsFixed = true)]
        [PXUIField(DisplayName = "Status", Enabled = false)]
        [EALCARegistrationStatusList]
        [SetStatus]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected bool? _Hold;
        [PXDBBool()]
        [PXDefault(typeof(EAAssetSetup.lCABulkRegistrationHoldOnEntry))]
        [PXUIField(DisplayName = "Hold")]
        public virtual bool? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion     
        #region INRegisterRefNbr
        public abstract class iNRegisterRefNbr : PX.Data.IBqlField
        {
        }
        protected string _INRegisterRefNbr;
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        //[PXDefault()]
        [PXUIField(DisplayName = "Inventory Issue Ref. Nbr", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(Search<INRegister.refNbr, Where<INRegister.docType, Equal<INDocType.issue>>, OrderBy<Desc<INRegister.refNbr>>>), Filterable = true)]
        [PX.Data.EP.PXFieldDescription]

        public virtual string INRegisterRefNbr
        {
            get
            {
                return this._INRegisterRefNbr;
            }
            set
            {
                this._INRegisterRefNbr = value;
            }
        }
        #endregion
        #region WaybillRefNbr
        public abstract class waybillRefNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillRefNbr;
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Waybill Ref. Nbr", Enabled = false)]
        [PXSelector(typeof(Search<RSWaybill.waybillNbr>),
            SubstituteKey = typeof(RSWaybill.waybillNbr))]
        public virtual string WaybillRefNbr
        {
            get
            {
                return this._WaybillRefNbr;
            }
            set
            {
                this._WaybillRefNbr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region Attributes

        public class SetStatusAttribute : PXEventSubscriberAttribute, IPXFieldDefaultingSubscriber
        {
            public override void CacheAttached(PXCache sender)
            {
                base.CacheAttached(sender);

                sender.Graph.FieldUpdating.AddHandler(
                    sender.GetItemType(),
                    nameof(EALCARegistration.hold),
                    (cache, e) =>
                    {
                        PXBoolAttribute.ConvertValue(e);

                        var row = e.Row as EALCARegistration;

                        if (row != null)
                        {
                            SetStatus(cache, row, (bool?)e.NewValue);
                        }
                    });

                sender.Graph.RowSelected.AddHandler(
                    sender.GetItemType(),
                    (cache, e) =>
                    {
                        var row = e.Row as EAAsset;

                        if (row != null)
                        {
                            PXUIFieldAttribute.SetEnabled<EALCARegistration.hold>(cache, row, row.Status == EALCARegistrationStatus.Hold || row.Status == EALCARegistrationStatus.Open);
                        }
                    });
            }

            public virtual void FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
            {
                var row = e.Row as EALCARegistration;

                if (row == null) return;

                var setup = cache.Graph.Caches[typeof(EAAssetSetup)].Current as EAAssetSetup;

                SetStatus(cache, row, setup.LCABulkRegistrationHoldOnEntry);

                e.NewValue = row.Status;
                e.Cancel = true;
            }

            protected virtual void SetStatus(PXCache cache, EALCARegistration row, bool? hold)
            {
                row.Status = hold == true ? EALCARegistrationStatus.Hold : EALCARegistrationStatus.Open;
            }
        }

        #endregion

        #region Methods

        public bool CanRelease()
        {
            return Status == EALCARegistrationStatus.Open;
        }

        public bool CanCreateWaybill()
        {
            return this.Status == EALCARegistrationStatus.Released && this.WaybillRefNbr.IsNullOrEmpty();
        }

        #endregion
    }
}

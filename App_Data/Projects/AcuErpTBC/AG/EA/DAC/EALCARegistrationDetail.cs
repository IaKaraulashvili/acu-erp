﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using PX.Objects.CM;
    using PX.Objects.CR;
    using PX.Objects.GL;
    using PX.Objects.EP;
    using Shared;

    [System.SerializableAttribute()]
	public class EALCARegistrationDetail : PX.Data.IBqlTable
	{
		#region LCARegistrationDetailID
		public abstract class lCARegistrationDetailID : PX.Data.IBqlField
		{
		}
		protected int? _LCARegistrationDetailID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? LCARegistrationDetailID
		{
			get
			{
				return this._LCARegistrationDetailID;
			}
			set
			{
				this._LCARegistrationDetailID = value;
			}
		}
		#endregion
		#region LCARegistrationID
		public abstract class lCARegistrationID : PX.Data.IBqlField
		{
		}
		protected int? _LCARegistrationID;
		[PXDBInt()]
        [PXParent(typeof(Select<EALCARegistration, Where<EALCARegistration.lCARegistrationID, Equal<Current<EALCARegistrationDetail.lCARegistrationID>>>>))]
        [PXDBDefault(typeof(EALCARegistration.lCARegistrationID))]
		public virtual int? LCARegistrationID
		{
			get
			{
				return this._LCARegistrationID;
			}
			set
			{
				this._LCARegistrationID = value;
			}
		}
        #endregion
        #region InventoryID
        public abstract class inventoryID : PX.Data.IBqlField
        {
        }
        protected int? _InventoryID;
        [StockItem(DisplayName = "Inventory ID")]
        public virtual int? InventoryID
        {
            get
            {
                return this._InventoryID;
            }
            set
            {
                this._InventoryID = value;
            }
        }
        #endregion
        #region LotSerialNbr
        public abstract class lotSerialNbr : PX.Data.IBqlField
		{
		}
		protected string _LotSerialNbr;
		[LotSerialNbr]
		public virtual string LotSerialNbr
        {
			get
			{
				return this._LotSerialNbr;
			}
			set
			{
				this._LotSerialNbr = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Description")]
        [PXDefault]
        public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
		#endregion
		#region Cost
		public abstract class cost : PX.Data.IBqlField
		{
		}
		protected decimal? _Cost;
        [PXDBBaseCury]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Asset Cost")]
		public virtual decimal? Cost
		{
			get
			{
				return this._Cost;
			}
			set
			{
				this._Cost = value;
			}
		}
		#endregion
		#region ClassID
		public abstract class classID : PX.Data.IBqlField
		{
		}
		protected string _ClassID;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Asset Class")]
        [PXDefault(typeof(EAAssetSetup.defAssetClassID))]
        [EAAssetClassSelector]
        public virtual string ClassID
		{
			get
			{
				return this._ClassID;
			}
			set
			{
				this._ClassID = value;
			}
		}
		#endregion
		#region UOM
		public abstract class uOM : PX.Data.IBqlField
		{
		}
		protected string _UOM;		
        [INUnit]
        public virtual string UOM
		{
			get
			{
				return this._UOM;
			}
			set
			{
				this._UOM = value;
			}
		}
		#endregion
		#region Qty
		public abstract class qty : PX.Data.IBqlField
		{
		}
		protected decimal? _Qty;
        [PXDBQuantity(MinValue = 1)]
        [PXDefault(TypeCode.Decimal, "1.0")]
        [PXUIField(DisplayName = "Quantity")]

        public virtual decimal? Qty
		{
			get
			{
				return this._Qty;
			}
			set
			{
				this._Qty = value;
			}
		}
		#endregion
		#region AssetCD
		public abstract class assetCD : PX.Data.IBqlField
		{
		}
		protected string _AssetCD;
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Asset ID")]
        public virtual string AssetCD
		{
			get
			{
				return this._AssetCD;
			}
			set
			{
				this._AssetCD = value;
			}
		}
		#endregion
		#region BranchID
		public abstract class branchID : PX.Data.IBqlField
		{
		}
		protected int? _BranchID;
        [Branch(typeof(Coalesce<
            Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>, 
                Where<EPEmployee.bAccountID, Equal<Current<EALCARegistrationDetail.employeeID>>>>,
            Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false)]
        public virtual int? BranchID
		{
			get
			{
				return this._BranchID;
			}
			set
			{
				this._BranchID = value;
			}
		}
		#endregion
		#region BuildingID
		public abstract class buildingID : PX.Data.IBqlField
		{
		}
		protected int? _BuildingID;
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<EALCARegistrationDetail.branchID>>>>),
            SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXDefault]
        [PXUIField(DisplayName = "Building")]
        public virtual int? BuildingID
		{
			get
			{
				return this._BuildingID;
			}
			set
			{
				this._BuildingID = value;
			}
		}
		#endregion
		#region Floor
		public abstract class floor : PX.Data.IBqlField
		{
		}
		protected string _Floor;
		[PXDBString(5, IsUnicode = true)]
		[PXUIField(DisplayName = "Floor")]
		public virtual string Floor
		{
			get
			{
				return this._Floor;
			}
			set
			{
				this._Floor = value;
			}
		}
		#endregion
		#region Room
		public abstract class room : PX.Data.IBqlField
		{
		}
		protected string _Room;
		[PXDBString(5, IsUnicode = true)]
		[PXUIField(DisplayName = "Room")]
		public virtual string Room
		{
			get
			{
				return this._Room;
			}
			set
			{
				this._Room = value;
			}
		}
		#endregion
		#region EmployeeID
		public abstract class employeeID : PX.Data.IBqlField
		{
		}
		protected int? _EmployeeID;
        [PXDBInt()]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        [PXDefault]
        [PXUIField(DisplayName = "Custodian")]
        public virtual int? EmployeeID
		{
			get
			{
				return this._EmployeeID;
			}
			set
			{
				this._EmployeeID = value;
			}
		}
		#endregion
		#region Custodian
		public abstract class custodian : PX.Data.IBqlField
		{
		}
		protected Guid? _Custodian;
		[PXDBField()]
        [PXFormula(typeof(Selector<EALCARegistrationDetail.employeeID, EPEmployee.userID>))]
        public virtual Guid? Custodian
		{
			get
			{
				return this._Custodian;
			}
			set
			{
				this._Custodian = value;
			}
		}
		#endregion
		#region Department
		public abstract class department : PX.Data.IBqlField
		{
		}
		protected string _Department;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<EALCARegistrationDetail.employeeID>>>>))]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        [PXUIField(DisplayName = "Department")]
        public virtual string Department
		{
			get
			{
				return this._Department;
			}
			set
			{
				this._Department = value;
			}
		}
		#endregion
		#region SiteID
		public abstract class siteID : PX.Data.IBqlField
		{
		}
		protected int? _SiteID;
        [Site]
        public virtual int? SiteID
		{
			get
			{
				return this._SiteID;
			}
			set
			{
				this._SiteID = value;
			}
		}
		#endregion
		#region Reason
		public abstract class reason : PX.Data.IBqlField
		{
		}
		protected string _Reason;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Reason")]
		public virtual string Reason
		{
			get
			{
				return this._Reason;
			}
			set
			{
				this._Reason = value;
			}
		}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion    
    }
}
﻿namespace AG.EA.DAC
{
    using PX.Data;
    using PX.Objects.CR;
    using PX.Objects.CS;
    using PX.Objects.EP;
    using PX.Objects.GL;
    using PX.Objects.IN;
    using RS.DAC;
    using Shared;
    using System;
    using AG.Utils.Extensions;
    [System.SerializableAttribute()]
    public class EALCATransfer : PX.Data.IBqlTable
    {
        #region LCATransferID
        public abstract class lCATransferID : PX.Data.IBqlField
        {
        }
        protected int? _LCATransferID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? LCATransferID
        {
            get
            {
                return this._LCATransferID;
            }
            set
            {
                this._LCATransferID = value;
            }
        }
        #endregion
        #region LCATransferCD
        public abstract class lCATransferCD : PX.Data.IBqlField
        {
        }
        protected string _LCATransferCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Reference Nbr.")]
        [PXSelector(typeof(Search<EALCATransfer.lCATransferCD>))]
        [AutoNumber(typeof(EAAssetSetup.lCABulkTransferNumberingID), typeof(EALCATransfer.createdDateTime))]
        public virtual string LCATransferCD
        {
            get
            {
                return this._LCATransferCD;
            }
            set
            {
                this._LCATransferCD = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(1, IsFixed = true)]
        [PXUIField(DisplayName = "Status", Enabled = false, Visibility = PXUIVisibility.SelectorVisible)]
        [EALCATransferStatusList]
        [SetStatus]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected bool? _Hold;
        [PXDBBool()]
        [PXDefault(typeof(EAAssetSetup.lCABulkTransferHoldOnEntry))]
        [PXUIField(DisplayName = "Hold")]
        public virtual bool? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion
        #region CreateWaybill
        public abstract class createWaybill : PX.Data.IBqlField
        {
        }
        protected bool? _CreateWaybill;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Create Waybill")]
        public virtual bool? CreateWaybill
        {
            get
            {
                return this._CreateWaybill;
            }
            set
            {
                this._CreateWaybill = value;
            }
        }
        #endregion
        #region WaybillRefNbr
        public abstract class waybillRefNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillRefNbr;
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Waybill Ref. Nbr", Enabled = false)]
        [PXSelector(typeof(Search<RSWaybill.waybillNbr>),
            SubstituteKey = typeof(RSWaybill.waybillNbr))]
        public virtual string WaybillRefNbr
        {
            get
            {
                return this._WaybillRefNbr;
            }
            set
            {
                this._WaybillRefNbr = value;
            }
        }
        #endregion

        #region WaybillSourceBuildingID
        public abstract class waybillSourceBuildingID : PX.Data.IBqlField
        {
        }
        protected int? _WaybillSourceBuildingID;
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID>),
           SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXUIField(DisplayName = "Building")]
        public virtual int? WaybillSourceBuildingID
        {
            get
            {
                return this._WaybillSourceBuildingID;
            }
            set
            {
                this._WaybillSourceBuildingID = value;
            }
        }
        #endregion

        #region ReleaseDate
        public abstract class releaseDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ReleaseDate;
        [PXDBDateAndTime()]
        [PXUIField(DisplayName = "Release Date", IsReadOnly = true)]
        public virtual DateTime? ReleaseDate
        {
            get
            {
                return this._ReleaseDate;
            }
            set
            {
                this._ReleaseDate = value;
            }
        }
        #endregion
        #region TransferReason
        public abstract class transferReason : PX.Data.IBqlField
        {
        }
        protected string _TransferReason;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Transfer Reason")]
        public virtual string TransferReason
        {
            get
            {
                return this._TransferReason;
            }
            set
            {
                this._TransferReason = value;
            }
        }
        #endregion
        #region ToBranchID
        public abstract class toBranchID : PX.Data.IBqlField
        {
        }
        protected int? _ToBranchID;
        [Branch(typeof(Coalesce<
            Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>,
                Where<EPEmployee.bAccountID, Equal<Current<EALCATransfer.toEmployeeID>>>>,
            Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false)]
        public virtual int? ToBranchID
        {
            get
            {
                return this._ToBranchID;
            }
            set
            {
                this._ToBranchID = value;
            }
        }
        #endregion
        #region ToBuildingID
        public abstract class toBuildingID : PX.Data.IBqlField
        {
        }
        protected int? _ToBuildingID;
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<EALCATransfer.toBranchID>>>>),
           SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXDefault]
        [PXUIField(DisplayName = "Building")]
        public virtual int? ToBuildingID
        {
            get
            {
                return this._ToBuildingID;
            }
            set
            {
                this._ToBuildingID = value;
            }
        }
        #endregion
        #region ToFloor
        public abstract class toFloor : PX.Data.IBqlField
        {
        }
        protected string _ToFloor;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Floor")]
        public virtual string ToFloor
        {
            get
            {
                return this._ToFloor;
            }
            set
            {
                this._ToFloor = value;
            }
        }
        #endregion
        #region ToRoom
        public abstract class toRoom : PX.Data.IBqlField
        {
        }
        protected string _ToRoom;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Room")]
        public virtual string ToRoom
        {
            get
            {
                return this._ToRoom;
            }
            set
            {
                this._ToRoom = value;
            }
        }
        #endregion
        #region ToEmployeeID
        public abstract class toEmployeeID : PX.Data.IBqlField
        {
        }
        protected int? _ToEmployeeID;
        [PXDBInt()]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        [PXDefault]
        [PXUIField(DisplayName = "Custodian")]
        public virtual int? ToEmployeeID
        {
            get
            {
                return this._ToEmployeeID;
            }
            set
            {
                this._ToEmployeeID = value;
            }
        }
        #endregion
        #region ToCustodian
        public abstract class toCustodian : PX.Data.IBqlField
        {
        }
        protected Guid? _ToCustodian;
        [PXDBField()]
        [PXFormula(typeof(Selector<EALCATransfer.toEmployeeID, EPEmployee.userID>))]
        public virtual Guid? ToCustodian
        {
            get
            {
                return this._ToCustodian;
            }
            set
            {
                this._ToCustodian = value;
            }
        }
        #endregion
        #region ToDepartment
        public abstract class toDepartment : PX.Data.IBqlField
        {
        }
        protected string _ToDepartment;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<EALCATransfer.toEmployeeID>>>>))]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        [PXUIField(DisplayName = "Department")]
        public virtual string ToDepartment
        {
            get
            {
                return this._ToDepartment;
            }
            set
            {
                this._ToDepartment = value;
            }
        }
        #endregion
        #region ToSiteID
        public abstract class toSiteID : PX.Data.IBqlField
        {
        }
        protected int? _ToSiteID;
        [Site]
        public virtual int? ToSiteID
        {
            get
            {
                return this._ToSiteID;
            }
            set
            {
                this._ToSiteID = value;
            }
        }
        #endregion

        #region System Fields

        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        [PXUIField(DisplayName = "Create Date", IsReadOnly = true)]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #endregion

        #region Attributes

        public class SetStatusAttribute : PXEventSubscriberAttribute, IPXFieldDefaultingSubscriber
        {
            public override void CacheAttached(PXCache sender)
            {
                base.CacheAttached(sender);

                sender.Graph.FieldUpdating.AddHandler(
                    sender.GetItemType(),
                    nameof(EALCATransfer.hold),
                    (cache, e) =>
                    {
                        PXBoolAttribute.ConvertValue(e);

                        var row = e.Row as EALCATransfer;

                        if (row != null)
                        {
                            SetStatus(cache, row, (bool?)e.NewValue);
                        }
                    });

                sender.Graph.RowSelected.AddHandler(
                    sender.GetItemType(),
                    (cache, e) =>
                    {
                        var row = e.Row as EALCATransfer;

                        if (row != null)
                        {
                            PXUIFieldAttribute.SetEnabled<EALCATransfer.hold>(cache, row, row.Status == EALCATransferStatus.Hold || row.Status == EALCATransferStatus.Open);
                        }
                    });
            }

            public virtual void FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
            {
                var row = e.Row as EALCATransfer;

                if (row == null) return;

                var setup = cache.Graph.Caches[typeof(EAAssetSetup)].Current as EAAssetSetup;

                SetStatus(cache, row, setup.LCABulkTransferHoldOnEntry);

                e.NewValue = row.Status;
                e.Cancel = true;
            }

            protected virtual void SetStatus(PXCache cache, EALCATransfer row, bool? hold)
            {
                row.Status = hold == true ? EALCATransferStatus.Hold : EALCATransferStatus.Open;
            }
        }

        #endregion

        #region Methods

        public bool CanRelease()
        {
            return Status == EALCATransferStatus.Open;
        }

        public bool CanCreateWaybill()
        {
            return this.Status == EALCATransferStatus.Released && this.WaybillRefNbr.IsNullOrEmpty() && this.CreateWaybill.GetValueOrDefault();
        }

        #endregion
    }
}

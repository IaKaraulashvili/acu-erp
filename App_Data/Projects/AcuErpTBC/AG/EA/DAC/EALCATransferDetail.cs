﻿namespace AG.EA.DAC
{
    using System;
    using PX.Data;
    using Shared;
    using PX.Objects.GL;
    using PX.Objects.CR;
    using PX.Objects.IN;
    using PX.Objects.EP;
    using PX.Objects.CM;

    [System.SerializableAttribute()]
    public class EALCATransferDetail : PX.Data.IBqlTable
    {
        #region LCATransferDetailID
        public abstract class lCATransferDetailID : PX.Data.IBqlField
        {
        }
        protected int? _LCATransferDetailID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? LCATransferDetailID
        {
            get
            {
                return this._LCATransferDetailID;
            }
            set
            {
                this._LCATransferDetailID = value;
            }
        }
        #endregion
        #region LCATransferID
        public abstract class lCATransferID : PX.Data.IBqlField
        {
        }
        protected int? _LCATransferID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(EALCATransfer.lCATransferID))]
        [PXParent(typeof(Select<EALCATransfer,
                                 Where<EALCATransfer.lCATransferID, Equal<Current<EALCATransferDetail.lCATransferID>>>>))]
        public virtual int? LCATransferID
        {
            get
            {
                return this._LCATransferID;
            }
            set
            {
                this._LCATransferID = value;
            }
        }
        #endregion
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXRestrictor(typeof(Where<EAAsset.status, Equal<EAAssetStatus.active>, 
            Or<EAAsset.status, Equal<EAAssetStatus.inService>>>), "The asset ({0}) status is '{1}'.", new[] { typeof(EAAsset.assetCD), typeof(EAAsset.status) })]
        [EAAssetSelector]
        [PXUIField(DisplayName = "Asset ID")]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region UOM
        public abstract class uOM : PX.Data.IBqlField
        {
        }
        protected string _UOM;
        [INUnit(Enabled = false)]
        public virtual string UOM
        {
            get
            {
                return this._UOM;
            }
            set
            {
                this._UOM = value;
            }
        }
        #endregion
        #region Cost
        public abstract class cost : PX.Data.IBqlField
        {
        }
        protected decimal? _Cost;
        [PXDBBaseCury]
        [PXUIField(DisplayName = "Asset Cost", Enabled = false)]
        public virtual decimal? Cost
        {
            get
            {
                return this._Cost;
            }
            set
            {
                this._Cost = value;
            }
        }
        #endregion
        #region Condition
        public abstract class condition : PX.Data.IBqlField
        {
        }
        protected string _Condition;
        [PXDBString(1, IsFixed = true)]
        [EAAssetConditionList]
        [PXUIField(DisplayName = "Condition", Enabled = false)]
        public virtual string Condition
        {
            get
            {
                return this._Condition;
            }
            set
            {
                this._Condition = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected int? _BranchID;
        [Branch(typeof(Coalesce<
            Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>,
                Where<EPEmployee.bAccountID, Equal<Current<EALCATransferDetail.employeeID>>>>,
            Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false, Enabled = false)]
        public virtual int? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region BuildingID
        public abstract class buildingID : PX.Data.IBqlField
        {
        }
        protected int? _BuildingID;
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<EALCATransferDetail.branchID>>>>),
            SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXUIField(DisplayName = "Building", Enabled = false)]
        public virtual int? BuildingID
        {
            get
            {
                return this._BuildingID;
            }
            set
            {
                this._BuildingID = value;
            }
        }
        #endregion
        #region Floor
        public abstract class floor : PX.Data.IBqlField
        {
        }
        protected string _Floor;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Floor", Enabled = false)]
        public virtual string Floor
        {
            get
            {
                return this._Floor;
            }
            set
            {
                this._Floor = value;
            }
        }
        #endregion
        #region Room
        public abstract class room : PX.Data.IBqlField
        {
        }
        protected string _Room;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Room", Enabled = false)]
        public virtual string Room
        {
            get
            {
                return this._Room;
            }
            set
            {
                this._Room = value;
            }
        }
        #endregion
        #region EmployeeID
        public abstract class employeeID : PX.Data.IBqlField
        {
        }
        protected int? _EmployeeID;
        [PXDBInt()]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        [PXUIField(DisplayName = "Custodian", Enabled = false)]
        public virtual int? EmployeeID
        {
            get
            {
                return this._EmployeeID;
            }
            set
            {
                this._EmployeeID = value;
            }
        }
        #endregion
        #region Custodian
        public abstract class custodian : PX.Data.IBqlField
        {
        }
        protected Guid? _Custodian;
        [PXDBField()]
        [PXFormula(typeof(Selector<EALCATransferDetail.employeeID, EPEmployee.userID>))]
        public virtual Guid? Custodian
        {
            get
            {
                return this._Custodian;
            }
            set
            {
                this._Custodian = value;
            }
        }
        #endregion
        #region Department
        public abstract class department : PX.Data.IBqlField
        {
        }
        protected string _Department;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        [PXUIField(DisplayName = "Department", Enabled = false)]
        public virtual string Department
        {
            get
            {
                return this._Department;
            }
            set
            {
                this._Department = value;
            }
        }
        #endregion
        #region SiteID
        public abstract class siteID : PX.Data.IBqlField
        {
        }
        protected int? _SiteID;
        [Site(Enabled = false)]
        public virtual int? SiteID
        {
            get
            {
                return this._SiteID;
            }
            set
            {
                this._SiteID = value;
            }
        }
        #endregion

        #region System Fields

        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #endregion
    }
}

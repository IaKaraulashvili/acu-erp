using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CS;
using AG.EA.Shared;
using AG.EA.DAC;

namespace AG.EA
{

    
    public class LCADisposalEntry : AGGraph<LCADisposalEntry, EALCADisposal>
    {
        
        public PXSelect<EALCADisposal> LCADisposals;

        [PXImport(typeof(EALCADisposal))]
        public PXSelectJoin<EALCADisposalDetail,
            LeftJoin<EAAsset, On<EAAsset.assetID, Equal<EALCADisposalDetail.assetID>>,
                LeftJoin<EAAssetLocationHistory, On<EAAsset.assetID, Equal<EAAssetLocationHistory.assetID>,
                     And<EAAsset.locationRevID, Equal<EAAssetLocationHistory.revisionID>>>>>,
            Where<EALCADisposalDetail.lCADisposalID, Equal<Current<EALCADisposal.lCADisposalID>>>> LCADisposalDetails;

        public PXSetup<EAAssetSetup> Setup;

        public LCADisposalEntry()
        {
            var setup = Setup.Current;
        }

        #region Events

        protected virtual void EALCADisposal_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (EALCADisposal)e.Row;
            if (row == null) return;

            var forDisposal = row.Status == EALCADisposalStatus.ForDisposal;
            var disposed = row.Status == EALCADisposalStatus.Disposed;

            PrepareForDisposal.SetEnabled(row.CanPrepareForDisposal());
            Dispose.SetEnabled(row.CanDispose());
            LCADisposals.Cache.AllowDelete = row.Status == EALCADisposalStatus.Hold;
            LCADisposalDetails.Cache.AllowUpdate = !forDisposal && !disposed;
            LCADisposals.Cache.AllowUpdate =  !disposed;
        }

        #endregion


        #region Actions

        public PXAction<EALCADisposal> PrepareForDisposal;
        [PXProcessButton]
        [PXUIField(DisplayName = "Prepare For Disposal", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        public virtual IEnumerable prepareForDisposal(PXAdapter adapter)
        {
            Save.Press();

            PXLongOperation.StartOperation(this, delegate
            {
                PrepareForDisposalImpl(LCADisposals.Current);
            });

            return new List<EALCADisposal> { LCADisposals.Current };
        }

        public PXAction<EALCADisposal> Dispose;
        [PXProcessButton]
        [PXUIField(DisplayName = "Dispose", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        public virtual IEnumerable dispose(PXAdapter adapter)
        {

            Save.Press();

            PXLongOperation.StartOperation(this, delegate
            {
                DisposeImpl(LCADisposals.Current);
            });

            return new List<EALCADisposal> { LCADisposals.Current };
        }

        public void PrepareForDisposalImpl(EALCADisposal lCADisposal)
        {
            using (var scope = new PXTransactionScope())
            {
                if (!lCADisposal.CanPrepareForDisposal())
                {
                    throw new PXException(AG.Common.Messages.InappropriateStatusForAction);
                }

                lCADisposal.Status = EALCADisposalStatus.ForDisposal;

                foreach (EALCADisposalDetail detail in LCADisposalDetails.Select())
                {
                    var assetGraph = PXGraph.CreateInstance<AssetEntry>();
                    assetGraph.Clear();

                    var asset = (EAAsset)PXSelect<EAAsset, Where<EAAsset.assetID, Equal<Required<EAAsset.assetID>>>>.Select(this, detail.AssetID);

                    if (asset.Status == EAAssetStatus.ForDisposal || asset.Status == EAAssetStatus.Disposed)
                        continue;

                    assetGraph.PrepareForDisposalImpl(asset);
                }

                LCADisposals.Update(lCADisposal);

                Save.Press();

                scope.Complete();
            }
        }

        public void DisposeImpl(EALCADisposal lCADisposal)
        {
            using (var scope = new PXTransactionScope())
            {
                if (!lCADisposal.CanDispose())
                {
                    throw new PXException(AG.Common.Messages.InappropriateStatusForAction);
                }

                lCADisposal.Status = EALCADisposalStatus.Disposed;

                foreach (EALCADisposalDetail detail in LCADisposalDetails.Select())
                {

                    var assetGraph = PXGraph.CreateInstance<AssetEntry>();
                    assetGraph.Clear();

                    var asset = (EAAsset)PXSelect<EAAsset, Where<EAAsset.assetID, Equal<Required<EAAsset.assetID>>>>.Select(this, detail.AssetID);

                    if (asset.Status == EAAssetStatus.Disposed)
                        continue;

                    asset.LCADisposalID = lCADisposal.LCADisposalID;

                    assetGraph.DisposeAssetImpl(asset, lCADisposal.DisposalReason);
                }

                LCADisposals.Update(lCADisposal);

                Save.Press();

                scope.Complete();
            }
        }
        #endregion
    }
}
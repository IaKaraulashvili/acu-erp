using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.EA.Shared;
using System.Linq;
using PX.Objects.IN;
using PX.Objects.CS;
using AG.EA.DAC;
using AG.RS;
using AG.RS.DAC;
using AG.RS.Shared;
using AG.Utils.Extensions;

namespace AG.EA
{
    public class LCARegistrationEntry : AGGraph<LCARegistrationEntry, EALCARegistration>
    {
        #region Data Views

        public PXSelect<EALCARegistration> LCARegistration;

        public PXSelect<EALCARegistrationDetail,
            Where<EALCARegistrationDetail.lCARegistrationID,
                Equal<Current<EALCARegistration.lCARegistrationID>>>> LCARegistrationDetails;

        public PXSetup<EAAssetSetup> Setup;

        public PXSelectJoin<Numbering,
            InnerJoin<EAAssetSetup, On<EAAssetSetup.lCABulkRegistrationNumberingID, Equal<Numbering.numberingID>>>> numbering;

        #endregion

        #region ctor

        public LCARegistrationEntry()
        {
            var setup = Setup.Current;
        }

        #endregion

        #region EALCARegistration Events

        protected virtual void EALCARegistration_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = e.Row as EALCARegistration;

            if (row == null) return;

            var onHold = row.Status == EALCARegistrationStatus.Hold;
            var released = row.Status == EALCARegistrationStatus.Released;

            releaseAction.SetEnabled(row.CanRelease());

            cache.AllowDelete = onHold;
            cache.AllowUpdate = !released;

            LCARegistrationDetails.Cache.AllowInsert = !released;
            LCARegistrationDetails.Cache.AllowUpdate = !released;
            LCARegistrationDetails.Cache.AllowDelete = !released;

            createWaybillAction.SetEnabled(row.CanCreateWaybill());
        }

        protected virtual void EALCARegistrationDetail_EmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var hist = (EALCARegistrationDetail)e.Row;
            sender.SetDefaultExt<EALCARegistrationDetail.branchID>(hist);
            sender.SetDefaultExt<EALCARegistrationDetail.department>(hist);
        }

        protected virtual void EALCARegistrationDetail_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.ExternalCall)
            {
                sender.SetDefaultExt<EALCARegistrationDetail.buildingID>(e.Row);
                sender.SetValuePending<EALCARegistrationDetail.buildingID>(e.Row, null);
            }
        }

        protected virtual void EALCARegistrationDetail_InventoryID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = e.Row as EALCARegistrationDetail;

            if (row == null) return;

            InventoryItem inv = null;

            if (row.InventoryID != null)
            {
                inv = PXSelect<InventoryItem,
                    Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                    .Select(this, row.InventoryID).FirstOrDefault();
            }

            row.Description = inv?.Descr;
            row.UOM = inv?.BaseUnit;
        }

        #endregion

        #region EALCARegistrationDetail Events

        protected virtual void EALCARegistrationDetail_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = e.Row as EALCARegistrationDetail;

            if (row == null) return;

            Numbering nbr = numbering.Select();
            //PXUIFieldAttribute.SetEnabled<EALCARegistrationDetail.assetCD>(cache, row, nbr == null || nbr.UserNumbering == true);
        }

        #endregion

        #region Actions

        public PXAction<EALCARegistration> releaseAction;
        [PXUIField(DisplayName = "Release")]
        public IEnumerable ReleaseAction(PXAdapter adapter)
        {
            this.Save.Press();

            PXLongOperation.StartOperation(this, delegate
            {
                ReleaseDocument(LCARegistration.Current);
            });

            return new List<EALCARegistration> { LCARegistration.Current };
        }


        public PXAction<EALCARegistration> createWaybillAction;
        [PXUIField(DisplayName = "Create Waybill")]
        [PXButton]
        public void CreateWaybillAction()
        {
            var row = LCARegistration.Current;
            if (row == null) return;

            if (!row.CanCreateWaybill())
                throw new PXException("Waybill can not be created");

            if (!row.INRegisterRefNbr.IsNullOrEmpty())
            {
                var inIssue = (INRegister)PXSelect<INRegister, Where<INRegister.docType, Equal<INDocType.issue>,
                    And<INRegister.refNbr, Equal<Required<INRegister.refNbr>>>>>.Select(this, row.INRegisterRefNbr);

                if (inIssue != null)
                {
                    var inIssueExt = inIssue.GetExtension<INRegisterExt>();
                    if (!inIssueExt.UsrWaybillRefNbr.IsNullOrEmpty())
                    {
                        throw new PXException("Waybill already created from Inventory Issue");
                    }
                }
            }

            this.Save.Press();

            CreateWaybill();
        }


        public PXAction<EALCARegistration> viewAsset;
        [PXUIField(MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton]
        public virtual void ViewAsset()
        {
            if (LCARegistrationDetails.Current == null) return;

            AssetEntry graph = PXGraph.CreateInstance<AssetEntry>();

            graph.Assets.Current = graph.Assets.Search<EAAsset.assetCD>(LCARegistrationDetails.Current.AssetCD);

            throw new PXRedirectRequiredException(graph, "ViewAsset", true);

        }
        #endregion

        #region Methods

        public void ReleaseDocument(EALCARegistration lcaRegistration)
        {
            using (var tran = new PXTransactionScope())
            {
                var details = LCARegistrationDetails.Select().FirstTableItems;

                var assetGraph = PXGraph.CreateInstance<AssetEntry>();

                assetGraph.Clear();

                foreach (var item in details)
                {
                    var asset = assetGraph.Assets.Insert();

                    asset.Description = item.Description;
                    assetGraph.Assets.Cache.SetValueExt<EAAsset.classID>(asset, item.ClassID);
                    asset.Cost = item.Cost;
                    asset.Qty = item.Qty;
                    asset.UOM = item.UOM;
                    asset.InventoryID = item.InventoryID;
                    asset.LotSerialNbr = item.LotSerialNbr;
                    asset.LCARegistrationID = lcaRegistration.LCARegistrationID;

                    assetGraph.Assets.Update(asset);

                    var loc = assetGraph.AssetLocation.Current;

                    assetGraph.AssetLocation.Cache.SetValueExt<EAAssetLocationHistory.employeeID>(loc, item.EmployeeID);
                    loc.Custodian = item.Custodian;
                    loc.BranchID = item.BranchID;
                    loc.Department = item.Department;
                    loc.BuildingID = item.BuildingID;
                    loc.Floor = item.Floor;
                    loc.Room = item.Room;
                    loc.SiteID = item.SiteID;
                    loc.Reason = item.Reason;

                    assetGraph.AssetLocation.Update(loc);

                    assetGraph.Save.Press();

                    if (Setup.Current.AssetAutoRelease.GetValueOrDefault())
                    {
                        assetGraph.ReleaseAssetImpl(asset);
                    }

                    // Update AssetCD
                    LCARegistrationDetails.Current = item;
                    item.AssetCD = asset.AssetCD;
                    LCARegistrationDetails.Update(item);
                }

                lcaRegistration.Status = EALCARegistrationStatus.Released;
                LCARegistration.Update(lcaRegistration);

                Save.Press();

                tran.Complete();
            }
        }

        public void CreateWaybill()
        {
            var row = LCARegistration.Current;
            if (row == null) return;

            var graph = PXGraph.CreateInstance<WaybillEntry>();

            var waybill = graph.Waybills.Insert();

            waybill.SourceNbr = row.LCARegistrationCD;
            waybill.SourceType = WaybillSourceType.LCABulkRegistration;

            waybill.SourceAddress = null;
            waybill.SupplierDestinationAddress = null;

            foreach (EALCARegistrationDetail item in LCARegistrationDetails.Select())
            {
                var waybillItem = graph.WaybillItems.Insert();
                var waybillItemsCache = graph.WaybillItems.Cache;

                EAAsset asset = PXSelect<EAAsset, Where<EAAsset.assetCD, Equal<Required<EAAsset.assetCD>>>>.Select(this, item.AssetCD);

                waybillItemsCache.SetValueExt<RSWaybillItem.waybillItemType>(waybillItem, WaybillItemType.EnterpriseAsset);
                waybillItemsCache.SetValueExt<RSWaybillItem.eAAssetID>(waybillItem, asset.AssetID);
                waybillItem.UnitPrice = asset.Cost;
                waybillItemsCache.SetValueExt<RSWaybillItem.itemQty>(waybillItem, 1m);

                waybillItem.TaxType = GETaxTypes.Normal;
            }

            throw new PXPopupRedirectException(graph, "ViewWaybill", true);
        }
        #endregion
    }
}
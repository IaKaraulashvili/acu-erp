using AG.EA.DAC;
using AG.EA.Shared;
using AG.RS;
using AG.RS.DAC;
using AG.RS.Shared;
using PX.Data;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using System;
using System.Collections;
using System.Collections.Generic;
using PX.Objects.IN;
using PX.Objects.CM;
using AG.Utils.Extensions;
using System.Linq;

namespace AG.EA
{
    public class LCATransferEntry : AGGraph<LCATransferEntry, EALCATransfer>
    {
        #region Data Views

        public PXSelect<EALCATransfer> Document;

        [PXImport(typeof(EALCATransfer))]
        public PXSelectJoin<EALCATransferDetail,
            LeftJoin<EAAsset, On<EALCATransferDetail.assetID, Equal<EAAsset.assetID>>>,
            Where<EALCATransferDetail.lCATransferID, Equal<Current<EALCATransfer.lCATransferID>>>> Details;

        public PXSelect<EALCATransfer, 
            Where<EALCATransfer.lCATransferID, Equal<Current<EALCATransfer.lCATransferID>>>> WaybillSource;

        public PXSetup<EAAssetSetup> Setup;

        #endregion

        #region ctor

        public LCATransferEntry()
        {
            var setup = Setup.Current;
        }

        #endregion

        #region Event handlers

        protected virtual void EALCATransfer_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = e.Row as EALCATransfer;

            if (row == null) return;

            var onHold = row.Status == EALCATransferStatus.Hold;
            var released = row.Status == EALCATransferStatus.Released;

            releaseAction.SetEnabled(row.CanRelease());
            addAsset.SetEnabled(!released);
            createWaybillAction.SetEnabled(row.CanCreateWaybill());

            cache.AllowDelete = onHold;
            cache.AllowUpdate = !released;

            Details.Cache.AllowInsert = !released;
            Details.Cache.AllowUpdate = !released;
            Details.Cache.AllowDelete = !released;
        }


        protected virtual void EALCATransfer_ToEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (EALCATransfer)e.Row;
            sender.SetDefaultExt<EALCATransfer.toBranchID>(row);
            sender.SetDefaultExt<EALCATransfer.toDepartment>(row);
        }

        protected virtual void EALCATransfer_ToBranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.ExternalCall)
            {
                sender.SetDefaultExt<EALCATransfer.toBuildingID>(e.Row);
                sender.SetValuePending<EALCATransfer.toBuildingID>(e.Row, null);
            }
        }

        protected virtual void EALCATransferDetail_AssetID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (EALCATransferDetail)e.Row;

            var sel = PXSelectJoin<EAAsset,
                LeftJoin<EAAssetLocationHistory, On<EAAsset.assetID, Equal<EAAssetLocationHistory.assetID>,
                    And<EAAsset.locationRevID, Equal<EAAssetLocationHistory.revisionID>>>>,
                Where<EAAsset.assetID, Equal<Required<EAAsset.assetID>>>>.Select(this, row.AssetID);

            if (sel != null && sel.Count > 0)
            {
                foreach (PXResult<EAAsset, EAAssetLocationHistory> item in sel)
                {
                    var asset = (EAAsset)item;
                    var loc = (EAAssetLocationHistory)item;

                    row.UOM = asset.UOM;
                    row.Cost = asset.Cost;
                    row.Condition = asset.Condition;

                    row.BranchID = loc.BranchID;
                    row.BuildingID = loc.BuildingID;
                    row.Floor = loc.Floor;
                    row.Room = loc.Room;
                    row.EmployeeID = loc.EmployeeID;
                    row.Department = loc.Department;
                    row.SiteID = loc.SiteID;
                }

                if (Document.Current.WaybillSourceBuildingID == null)
                {
                    Document.Current.WaybillSourceBuildingID = row.BuildingID;
                }
            }
        }

        #endregion

        #region Actions

        public PXAction<EALCATransfer> addAsset;

        [PXUIField(DisplayName = "Add Item", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton(CommitChanges = true)]
        public virtual IEnumerable AddAsset(PXAdapter adapter)
        {
            assetFilter.Cache.Clear();

            if (Assets.AskExt() == WebDialogResult.OK)
            {
                return AddAssetSel(adapter);
            }

            assetFilter.Cache.Clear();
            Assets.Cache.Clear();

            return adapter.Get();
        }

        public PXAction<EALCATransfer> releaseAction;
        [PXUIField(DisplayName = "Release")]
        [PXButton]
        public IEnumerable ReleaseAction(PXAdapter adapter)
        {
            this.Save.Press();

            PXLongOperation.StartOperation(this, delegate
            {
                ReleaseDocument();
            });

            return new List<EALCATransfer> { Document.Current };
        }

        public PXAction<EALCATransfer> createWaybillAction;
        [PXUIField(DisplayName = "Create Waybill")]
        [PXButton]
        public void CreateWaybillAction()
        {
            var row = Document.Current;
            if (row == null) return;

            if (!row.CanCreateWaybill())
                throw new PXException("Waybill can not be created");

            this.Save.Press();


            var details = Details.Select().FirstTableItems;
            int? buildingId = details.FirstOrDefault()?.BuildingID;
            bool waybillDiffSourceAddreses = false;

            foreach (var item in details)
            {
                if (item.BuildingID != buildingId)
                    waybillDiffSourceAddreses = true;
            }

            var buildingDesc = PXSelect<Building, Where<Building.branchID, Equal<Current<EALCATransfer.toBranchID>>,
               And<Building.buildingID, Equal<Required<Building.buildingID>>>>>.Select(this, row.WaybillSourceBuildingID).FirstTableItems.FirstOrDefault()?.Description;

            if (waybillDiffSourceAddreses)
            {
                if (Document.Ask("Warning", "Different source addresses are defined on document details. Do you want to processed waybill with " + buildingDesc + "?", MessageButtons.YesNo, false) == WebDialogResult.Yes)
                {
                    PXLongOperation.StartOperation(this, delegate
                    {
                        CreateWaybill(buildingDesc);
                    });
                }
            }
            else
            {
                PXLongOperation.StartOperation(this, delegate
                {
                    CreateWaybill(buildingDesc);
                });
            }



        }

        #endregion

        #region Asset Lookup

        public PXFilter<AssetFilter> assetFilter;

        [PXFilterable]
        [PXCopyPasteHiddenView]
        public PXSelect<AssetSelected> Assets;

        public IEnumerable assets()
        {
            var filter = assetFilter.Current;

            var query = new PXSelect<AssetSelected>(this);

            if (filter.BranchID.HasValue)
            {
                query.WhereAnd(typeof(Where<AssetSelected.branchID, Equal<Current<AssetFilter.branchID>>>));
            }

            if (filter.BuildingID.HasValue)
            {
                query.WhereAnd(typeof(Where<AssetSelected.buildingID, Equal<Current<AssetFilter.buildingID>>>));
            }

            if (!filter.Floor.IsNullOrEmpty())
            {
                query.WhereAnd(typeof(Where<AssetSelected.floor, Equal<Current<AssetFilter.floor>>>));
            }

            if (!filter.Room.IsNullOrEmpty())
            {
                query.WhereAnd(typeof(Where<AssetSelected.room, Equal<Current<AssetFilter.room>>>));
            }

            if (filter.EmployeeID.HasValue)
            {
                query.WhereAnd(typeof(Where<AssetSelected.employeeID, Equal<Current<AssetFilter.employeeID>>>));
            }

            if (!filter.Department.IsNullOrEmpty())
            {
                query.WhereAnd(typeof(Where<AssetSelected.department, Equal<Current<AssetFilter.department>>>));
            }

            if (filter.SiteID.HasValue)
            {
                query.WhereAnd(typeof(Where<AssetSelected.siteID, Equal<Current<AssetFilter.siteID>>>));
            }

            return query.Select();
        }

        public PXAction<EALCATransfer> addAssetSel;

        [PXUIField(DisplayName = "Add", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select, Visible = false)]
        [PXButton(CommitChanges = true)]
        public virtual IEnumerable AddAssetSel(PXAdapter adapter)
        {
            foreach (AssetSelected item in Assets.Cache.Cached)
            {
                if (item.Selected == true)
                {
                    var detail = new EALCATransferDetail();

                    detail.AssetID = item.AssetID;

                    Details.Insert(detail);

                    item.Selected = false;
                }
            }

            Assets.Cache.Clear();

            return adapter.Get();
        }

        protected virtual void AssetFilter_EmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (AssetFilter)e.Row;
            sender.SetDefaultExt<AssetFilter.branchID>(row);
            sender.SetDefaultExt<AssetFilter.department>(row);
        }

        protected virtual void AssetFilter_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            if (e.ExternalCall)
            {
                sender.SetDefaultExt<AssetFilter.buildingID>(e.Row);
                sender.SetValuePending<AssetFilter.buildingID>(e.Row, null);
            }
        }

        public class AssetFilter : IBqlTable
        {
            #region BranchID
            public abstract class branchID : PX.Data.IBqlField
            {
            }
            protected int? _BranchID;
            [Branch(typeof(Coalesce<
                Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>,
                    Where<EPEmployee.bAccountID, Equal<Current<AssetFilter.employeeID>>>>,
                Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false)]
            public virtual int? BranchID
            {
                get
                {
                    return this._BranchID;
                }
                set
                {
                    this._BranchID = value;
                }
            }
            #endregion
            #region BuildingID
            public abstract class buildingID : PX.Data.IBqlField
            {
            }
            protected int? _BuildingID;
            [PXInt]
            [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<AssetFilter.branchID>>>>),
                SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
            [PXUIField(DisplayName = "Building")]
            public virtual int? BuildingID
            {
                get
                {
                    return this._BuildingID;
                }
                set
                {
                    this._BuildingID = value;
                }
            }
            #endregion
            #region Floor
            public abstract class floor : PX.Data.IBqlField
            {
            }
            protected string _Floor;
            [PXString(5, IsUnicode = true)]
            [PXUIField(DisplayName = "Floor")]
            public virtual string Floor
            {
                get
                {
                    return this._Floor;
                }
                set
                {
                    this._Floor = value;
                }
            }
            #endregion
            #region Room
            public abstract class room : PX.Data.IBqlField
            {
            }
            protected string _Room;
            [PXString(5, IsUnicode = true)]
            [PXUIField(DisplayName = "Room")]
            public virtual string Room
            {
                get
                {
                    return this._Room;
                }
                set
                {
                    this._Room = value;
                }
            }
            #endregion
            #region EmployeeID
            public abstract class employeeID : PX.Data.IBqlField
            {
            }
            protected int? _EmployeeID;
            [PXInt()]
            [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
            [PXUIField(DisplayName = "Custodian")]
            public virtual int? EmployeeID
            {
                get
                {
                    return this._EmployeeID;
                }
                set
                {
                    this._EmployeeID = value;
                }
            }
            #endregion
            #region Department
            public abstract class department : PX.Data.IBqlField
            {
            }
            protected string _Department;
            [PXString(10, IsUnicode = true)]
            [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<AssetFilter.employeeID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
            [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
            [PXUIField(DisplayName = "Department")]
            public virtual string Department
            {
                get
                {
                    return this._Department;
                }
                set
                {
                    this._Department = value;
                }
            }
            #endregion
            #region SiteID
            public abstract class siteID : PX.Data.IBqlField
            {
            }
            protected int? _SiteID;
            [Site]
            public virtual int? SiteID
            {
                get
                {
                    return this._SiteID;
                }
                set
                {
                    this._SiteID = value;
                }
            }
            #endregion
        }

        [System.SerializableAttribute()]
        [PXProjection(typeof(Select2<EAAsset,
            LeftJoin<EAAssetLocationHistory, On<EAAsset.assetID, Equal<EAAssetLocationHistory.assetID>,
                And<EAAsset.locationRevID, Equal<EAAssetLocationHistory.revisionID>>>>,
            Where<EAAsset.status, Equal<EAAssetStatus.active>, Or<EAAsset.status, Equal<EAAssetStatus.inService>>>>), Persistent = false)]
        public class AssetSelected : IBqlTable
        {
            #region Selected
            public abstract class selected : PX.Data.IBqlField
            {
            }
            protected bool? _Selected = false;
            [PXBool]
            [PXDefault(false)]
            [PXUIField(DisplayName = "Selected")]
            public virtual bool? Selected
            {
                get
                {
                    return _Selected;
                }
                set
                {
                    _Selected = value;
                }
            }
            #endregion
            #region AssetID
            public abstract class assetID : PX.Data.IBqlField
            {
            }
            protected int? _AssetID;
            [PXDBInt(IsKey = true, BqlField = typeof(EAAsset.assetID))]
            [EAAssetSelector]
            [PXUIField(Enabled = false)]
            public virtual int? AssetID
            {
                get
                {
                    return this._AssetID;
                }
                set
                {
                    this._AssetID = value;
                }
            }
            #endregion
            #region Description
            public abstract class description : PX.Data.IBqlField
            {
            }
            protected string _Description;
            [PXDBString(250, IsUnicode = true, BqlField = typeof(EAAsset.description))]
            [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
            public virtual string Description
            {
                get
                {
                    return this._Description;
                }
                set
                {
                    this._Description = value;
                }
            }
            #endregion
            #region UOM
            public abstract class uOM : PX.Data.IBqlField
            {
            }
            protected string _UOM;
            [INUnit(BqlField = typeof(EAAsset.uOM))]
            public virtual string UOM
            {
                get
                {
                    return this._UOM;
                }
                set
                {
                    this._UOM = value;
                }
            }
            #endregion
            #region Cost
            public abstract class cost : PX.Data.IBqlField
            {
            }
            protected decimal? _Cost;
            [PXDBBaseCury(BqlField = typeof(EAAsset.cost))]
            [PXUIField(DisplayName = "Asset Cost")]
            public virtual decimal? Cost
            {
                get
                {
                    return this._Cost;
                }
                set
                {
                    this._Cost = value;
                }
            }
            #endregion
            #region Condition
            public abstract class condition : PX.Data.IBqlField
            {
            }
            protected string _Condition;
            [PXDBString(1, IsFixed = true, BqlField = typeof(EAAsset.condition))]
            [EAAssetConditionList]
            [PXUIField(DisplayName = "Condition")]
            public virtual string Condition
            {
                get
                {
                    return this._Condition;
                }
                set
                {
                    this._Condition = value;
                }
            }
            #endregion
            #region BranchID
            public abstract class branchID : PX.Data.IBqlField
            {
            }
            protected int? _BranchID;
            [Branch(typeof(Coalesce<
                Search2<Location.vBranchID, InnerJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<Location.bAccountID>, And<EPEmployee.defLocationID, Equal<Location.locationID>>>>,
                    Where<EPEmployee.bAccountID, Equal<Current<AssetFilter.employeeID>>>>,
                Search<Branch.branchID, Where<Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false, BqlField = typeof(EAAssetLocationHistory.branchID))]
            public virtual int? BranchID
            {
                get
                {
                    return this._BranchID;
                }
                set
                {
                    this._BranchID = value;
                }
            }
            #endregion
            #region BuildingID
            public abstract class buildingID : PX.Data.IBqlField
            {
            }
            protected int? _BuildingID;
            [PXDBInt(BqlField = typeof(EAAssetLocationHistory.buildingID))]
            [PXSelector(typeof(Search<Building.buildingID, Where<Building.branchID, Equal<Current<AssetFilter.branchID>>>>),
                SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
            [PXUIField(DisplayName = "Building")]
            public virtual int? BuildingID
            {
                get
                {
                    return this._BuildingID;
                }
                set
                {
                    this._BuildingID = value;
                }
            }
            #endregion
            #region Floor
            public abstract class floor : PX.Data.IBqlField
            {
            }
            protected string _Floor;
            [PXDBString(5, IsUnicode = true, BqlField = typeof(EAAssetLocationHistory.floor))]
            [PXUIField(DisplayName = "Floor")]
            public virtual string Floor
            {
                get
                {
                    return this._Floor;
                }
                set
                {
                    this._Floor = value;
                }
            }
            #endregion
            #region Room
            public abstract class room : PX.Data.IBqlField
            {
            }
            protected string _Room;
            [PXDBString(5, IsUnicode = true, BqlField = typeof(EAAssetLocationHistory.room))]
            [PXUIField(DisplayName = "Room")]
            public virtual string Room
            {
                get
                {
                    return this._Room;
                }
                set
                {
                    this._Room = value;
                }
            }
            #endregion
            #region EmployeeID
            public abstract class employeeID : PX.Data.IBqlField
            {
            }
            protected int? _EmployeeID;
            [PXDBInt(BqlField = typeof(EAAssetLocationHistory.employeeID))]
            [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
            [PXUIField(DisplayName = "Custodian")]
            public virtual int? EmployeeID
            {
                get
                {
                    return this._EmployeeID;
                }
                set
                {
                    this._EmployeeID = value;
                }
            }
            #endregion
            #region Department
            public abstract class department : PX.Data.IBqlField
            {
            }
            protected string _Department;
            [PXDBString(10, IsUnicode = true, BqlField = typeof(EAAssetLocationHistory.department))]
            [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
            [PXUIField(DisplayName = "Department")]
            public virtual string Department
            {
                get
                {
                    return this._Department;
                }
                set
                {
                    this._Department = value;
                }
            }
            #endregion
            #region SiteID
            public abstract class siteID : PX.Data.IBqlField
            {
            }
            protected int? _SiteID;
            [Site(BqlField = typeof(EAAssetLocationHistory.siteID))]
            public virtual int? SiteID
            {
                get
                {
                    return this._SiteID;
                }
                set
                {
                    this._SiteID = value;
                }
            }
            #endregion
        }

        #endregion

        #region Methods

        public void ReleaseDocument()
        {
            var doc = Document.Current;

            if (doc == null) return;

            using (var tran = new PXTransactionScope())
            {
                var details = Details.Select().FirstTableItems;

                if (!details.Any())
                {
                    throw new PXException(AG.Common.Messages.DetailsAreEmpty);
                }

                var assetGraph = PXGraph.CreateInstance<AssetEntry>();

                foreach (var item in details)
                {
                    assetGraph.Clear();
                    assetGraph.Assets.Current = assetGraph.FindById(item.AssetID.Value);

                    var asset = assetGraph.Assets.Current;

                    asset.Status = EAAssetStatus.Active;
                    asset.LastTransportationDate = DateTime.Now;

                    assetGraph.Assets.Update(asset);

                    EAAssetLocationHistory loc = assetGraph.AssetLocation.Select();

                    assetGraph.AssetLocation.Cache.SetValueExt<EAAssetLocationHistory.employeeID>(loc, doc.ToEmployeeID);
                    loc.BranchID = doc.ToBranchID;
                    loc.Department = doc.ToDepartment;
                    loc.BuildingID = doc.ToBuildingID;
                    loc.Floor = doc.ToFloor;
                    loc.Room = doc.ToRoom;
                    loc.SiteID = doc.ToSiteID;
                    loc.Reason = doc.TransferReason;

                    assetGraph.AssetLocation.Update(loc);

                    assetGraph.Save.Press();
                }

                doc.Status = EALCATransferStatus.Released;
                doc.ReleaseDate = DateTime.Now;

                Document.Update(doc);

                Save.Press();

                tran.Complete();
            }
        }

        public void CreateWaybill(string sourceAddress)
        {
            var row = Document.Current;
            if (row == null) return;

            var graph = PXGraph.CreateInstance<WaybillEntry>();

            var waybill = graph.Waybills.Insert();
            waybill.WaybillType = WaybillType.Internal;
            waybill.WaybillCategory = WaybillCategory.Normal;

            waybill.SourceNbr = row.LCATransferCD;
            waybill.SourceType = WaybillSourceType.LCABulkTransfer;

            waybill.SourceAddress = sourceAddress;

            waybill.SupplierDestinationAddress = PXSelect<Building, Where<Building.branchID, Equal<Current<EALCATransfer.toBranchID>>,
                And<Building.buildingID, Equal<Required<Building.buildingID>>>>>.Select(this, row.ToBuildingID).FirstTableItems.FirstOrDefault()?.Description;

            waybill.Comment = row.TransferReason;

            foreach (EALCATransferDetail item in Details.Select())
            {
                var waybillItem = graph.WaybillItems.Insert();
                var waybillItemsCache = graph.WaybillItems.Cache;

                EAAsset asset = PXSelect<EAAsset, Where<EAAsset.assetID, Equal<Required<EAAsset.assetCD>>>>.Select(this, item.AssetID);

                waybillItemsCache.SetValueExt<RSWaybillItem.waybillItemType>(waybillItem, WaybillItemType.EnterpriseAsset);
                waybillItemsCache.SetValueExt<RSWaybillItem.eAAssetID>(waybillItem, asset.AssetID);
                waybillItem.UnitPrice = asset.Cost;
                waybillItemsCache.SetValueExt<RSWaybillItem.itemQty>(waybillItem, 1m);

                waybillItem.TaxType = GETaxTypes.Normal;
            }

            throw new PXPopupRedirectException(graph, "ViewWaybill", true);
        }
        #endregion
    }
}
﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EAAssetCategory
    {
        public const string Category1 = "C1";
        public class category1 : Constant<String>
        {
            public category1()
                : base(Category1)
            {
            }
        }

        public const string Category2 = "C2";
        public class category2 : Constant<String>
        {
            public category2()
                : base(Category2)
            {
            }
        }

        public const string Category3 = "C3";
        public class category3 : Constant<String>
        {
            public category3()
                : base(Category3)
            {
            }
        }

        public const string Category4 = "C4";
        public class category4 : Constant<String>
        {
            public category4()
                : base(Category4)
            {
            }
        }

        public class UI
        {
            public const string Category1 = "Category 1";
            public const string Category2 = "Category 2";
            public const string Category3 = "Category 3";
            public const string Category4 = "Category 4";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EAAssetCategoryListAttribute : PXStringListAttribute
    {
        public EAAssetCategoryListAttribute() : base(
            new string[] {
                EAAssetCategory.Category1,
                EAAssetCategory.Category2,
                EAAssetCategory.Category3,
                EAAssetCategory.Category4
            },
            new string[] {
                EAAssetCategory.UI.Category1,
                EAAssetCategory.UI.Category2,
                EAAssetCategory.UI.Category3,
                EAAssetCategory.UI.Category4
            })
        { }
    }
}

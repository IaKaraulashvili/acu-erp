﻿using AG.EA.DAC;
using PX.Data;

namespace AG.EA.Shared
{
    public class EAAssetClassSelectorAttribute : PXSelectorAttribute
    {
        public EAAssetClassSelectorAttribute()
            : base(typeof(Search<EAAssetClass.assetClassID>),
                  typeof(EAAssetClass.assetClassID),
                  typeof(EAAssetClass.description),
                  typeof(EAAssetClass.state),
                  typeof(EAAssetClass.condition),
                  typeof(EAAssetClass.category),
                  typeof(EAAssetClass.uOM))
        {
            DescriptionField = typeof(EAAssetClass.description);
            SelectorMode = PXSelectorMode.DisplayModeHint;
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public static class EAAssetCondition
    {
        public const string Good = "G";
        public class good : Constant<String>
        {
            public good()
                : base(Good)
            {
            }
        }
        public const string Avarage = "A";
        public class avarage : Constant<String>
        {
            public avarage()
                : base(Avarage)
            {
            }
        }

        public const string Poor = "P";
        public class poor : Constant<String>
        {
            public poor()
                : base(Poor)
            {
            }
        }

        public const string Damaged = "D";
        public class damaged : Constant<String>
        {
            public damaged()
                : base(Damaged)
            {
            }
        }

        public class UI
        {
            public const string Good = "Good";
            public const string Avarage = "Avarage";
            public const string Poor = "Poor";
            public const string Damaged = "Damaged";
        }

    }
}

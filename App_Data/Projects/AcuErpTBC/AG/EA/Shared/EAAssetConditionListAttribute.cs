﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EAAssetConditionListAttribute : PXStringListAttribute
    {
        public EAAssetConditionListAttribute() : base(
            new string[] {
                EAAssetCondition.Good,
                EAAssetCondition.Avarage,
                EAAssetCondition.Poor,
                EAAssetCondition.Damaged
            },
            new string[] {
                EAAssetCondition.UI.Good,
                EAAssetCondition.UI.Avarage,
                EAAssetCondition.UI.Poor,
                EAAssetCondition.UI.Damaged
            })
        { }
    }
}

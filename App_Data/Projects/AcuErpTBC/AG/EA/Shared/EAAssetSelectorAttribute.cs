﻿using AG.EA.DAC;
using PX.Data;
using System;

namespace AG.EA.Shared
{
    public class EAAssetSelectorAttribute : PXSelectorAttribute
    {
        public EAAssetSelectorAttribute() :
            base(typeof(Search<EAAsset.assetID>))
        {
            SubstituteKey = typeof(EAAsset.assetCD);
        }

        public EAAssetSelectorAttribute(Type search) : base(search)
        {
            SubstituteKey = typeof(EAAsset.assetCD);
        }
    }
}

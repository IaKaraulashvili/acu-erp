﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public static class EAAssetState
    {
        public const string New = "N";
        public class @new : Constant<String>
        {
            public @new()
                : base(New)
            {
            }
        }
        public const string Used = "U";
        public class used : Constant<String>
        {
            public used()
                : base(Used)
            {
            }
        }

        public class UI
        {
            public const string New = "New";
            public const string Used = "Used";
        }

    }
}

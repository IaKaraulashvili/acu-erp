﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EAAssetStateListAttribute : PXStringListAttribute
    {
        public EAAssetStateListAttribute() : base(
            new string[] {
                EAAssetState.New,
                EAAssetState.Used
            },
            new string[] {
                EAAssetState.UI.New,
                EAAssetState.UI.Used
            })
        { }
    }
}

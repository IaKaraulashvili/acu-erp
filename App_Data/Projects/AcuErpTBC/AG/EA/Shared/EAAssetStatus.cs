﻿using PX.Data;
using System;

namespace AG.EA.Shared
{
    public static class EAAssetStatus
    {
        public const string Hold = "H";
        public class hold : Constant<String>
        {
            public hold()
                : base(Hold)
            {
            }
        }
        public const string Open = "O";
        public class open : Constant<String>
        {
            public open()
                : base(Open)
            {
            }
        }
        public const string Active = "A";
        public class active : Constant<String>
        {
            public active()
                : base(Active)
            {
            }
        }
        public const string InService = "S";
        public class inService : Constant<String>
        {
            public inService()
                : base(InService)
            {
            }
        }
        public const string ForDisposal = "F";
        public class forDisposal : Constant<String>
        {
            public forDisposal()
                : base(ForDisposal)
            {
            }
        }
        public const string Disposed = "D";
        public class disposed : Constant<String>
        {
            public disposed()
                : base(Disposed)
            {
            }
        }

        public class UI
        {
            public const string Hold = "On Hold";
            public const string Open = "Open";
            public const string Active = "Active";
            public const string InService = "In Service";
            public const string ForDisposal = "For Disposal";
            public const string Disposed = "Disposed";
        }
    }
}

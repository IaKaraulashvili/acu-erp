﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EAAssetStatusListAttribute : PXStringListAttribute
    {
        public EAAssetStatusListAttribute() : base(
            new string[] {
                EAAssetStatus.Hold,
                EAAssetStatus.Open,
                EAAssetStatus.Active,
                EAAssetStatus.InService,
                EAAssetStatus.ForDisposal,
                EAAssetStatus.Disposed,
            },
            new string[] {
                EAAssetStatus.UI.Hold,
                EAAssetStatus.UI.Open,
                EAAssetStatus.UI.Active,
                EAAssetStatus.UI.InService,
                EAAssetStatus.UI.ForDisposal,
                EAAssetStatus.UI.Disposed,
            })
        { }
    }
}

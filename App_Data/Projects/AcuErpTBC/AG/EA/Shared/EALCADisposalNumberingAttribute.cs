﻿using AG.EA.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EALCADisposalNumberingAttribute : PX.Objects.CS.AutoNumberAttribute
    {
        public EALCADisposalNumberingAttribute() : base(typeof(EAAssetSetup.lCABulkDisposeNumberingID), typeof(EALCADisposal.createdDateTime))
        {

        }
    }
}

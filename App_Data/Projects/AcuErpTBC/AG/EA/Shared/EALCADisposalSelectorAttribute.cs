﻿using AG.EA.DAC;
using PX.Data;

namespace AG.EA.Shared
{
    public class EALCADisposalSelectorAttribute : PXSelectorAttribute
    {
        public EALCADisposalSelectorAttribute()
            : base(typeof(Search<EALCADisposal.lCADisposalID>),
            typeof(EALCADisposal.lCADisposalCD),
            typeof(EALCADisposal.disposalReason))
        {
            SubstituteKey = typeof(EALCADisposal.lCADisposalCD);
        }
    }
}
﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public static class EALCADisposalStatus
    {
        public const string Hold = "H";
        public class hold : Constant<String>
        {
            public hold()
                : base(Hold)
            {
            }
        }

        public const string Open = "O";
        public class open : Constant<String>
        {
            public open()
                : base(Open)
            {
            }
        }

        public const string ForDisposal = "F";
        public class forDisposal : Constant<String>
        {
            public forDisposal()
                : base(ForDisposal)
            {
            }
        }
        public const string Disposed = "D";
        public class disposed : Constant<String>
        {
            public disposed()
                : base(Disposed)
            {
            }
        }

        public class UI
        {
            public const string Hold = "On Hold";
            public const string Open = "Open";
            public const string ReadyForDispose = "For Disposal";
            public const string Disposed = "Disposed";
        }
    }
}

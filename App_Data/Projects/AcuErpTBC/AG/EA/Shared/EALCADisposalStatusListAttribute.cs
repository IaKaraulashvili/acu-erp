﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EALCADisposalStatusListAttribute : PXStringListAttribute
    {
        public EALCADisposalStatusListAttribute() : base(
            new string[]{
                EALCADisposalStatus.Hold,
                EALCADisposalStatus.Open,
                EALCADisposalStatus.ForDisposal,
                EALCADisposalStatus.Disposed
            },
            new string[]{
                EALCADisposalStatus.UI.Hold,
                EALCADisposalStatus.UI.Open,
                EALCADisposalStatus.UI.ReadyForDispose,
                EALCADisposalStatus.UI.Disposed
            })
        { }
    }
}

﻿using AG.EA.DAC;
using PX.Data;

namespace AG.EA.Shared
{
    public class EALCARegistrationSelectorAttribute : PXSelectorAttribute
    {
        public EALCARegistrationSelectorAttribute()
            : base(typeof(Search<EALCARegistration.lCARegistrationID>),
            typeof(EALCARegistration.lCARegistrationCD),
            typeof(EALCARegistration.status))
        {
            SubstituteKey = typeof(EALCARegistration.lCARegistrationCD);
        }
    }
}
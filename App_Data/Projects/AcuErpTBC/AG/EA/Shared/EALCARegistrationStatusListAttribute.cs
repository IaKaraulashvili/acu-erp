﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public class EALCARegistrationStatusListAttribute : PXStringListAttribute
    {
        public EALCARegistrationStatusListAttribute() : base(
            new string[] {
                EALCARegistrationStatus.Hold,
                EALCARegistrationStatus.Open,
                EALCARegistrationStatus.Released
            },
            new string[] {
                EALCARegistrationStatus.UI.Hold,
                EALCARegistrationStatus.UI.Open,
                EALCARegistrationStatus.UI.Released
            })
        { }
    }
}

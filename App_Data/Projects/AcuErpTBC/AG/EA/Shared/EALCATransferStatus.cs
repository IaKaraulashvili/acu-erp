﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.EA.Shared
{
    public static class EALCATransferStatus
    {
        public const string Hold = "H";
        public class hold : Constant<String>
        {
            public hold()
                : base(Hold)
            {
            }
        }

        public const string Open = "O";
        public class open : Constant<String>
        {
            public open()
                : base(Open)
            {
            }
        }

        public const string Released = "R";
        public class released : Constant<String>
        {
            public released()
                : base(Released)
            {
            }
        }

        public class UI
        {
            public const string Hold = "On Hold";
            public const string Open = "Open";
            public const string Released = "Released";
        }
    }
}

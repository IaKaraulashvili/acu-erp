﻿using PX.Data;

namespace AG.EA.Shared
{
    public class EALCATransferStatusListAttribute : PXStringListAttribute
    {
        public EALCATransferStatusListAttribute() : base(
            new string[] {
                EALCATransferStatus.Hold,
                EALCATransferStatus.Open,
                EALCATransferStatus.Released
            },
            new string[] {
                EALCATransferStatus.UI.Hold,
                EALCATransferStatus.UI.Open,
                EALCATransferStatus.UI.Released
            })
        { }
    }
}

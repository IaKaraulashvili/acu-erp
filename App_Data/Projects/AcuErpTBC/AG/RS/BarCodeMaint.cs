using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;


namespace AG.RS
{
    public class BarCodeMaint : AGGraph<BarCodeMaint, RSBarCode>
    {
        public PXSelect<RSBarCode> BarCodes;
    }
}
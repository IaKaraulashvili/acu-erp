﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AG.RS.DAC
{
    public interface IRSBranch
    {
         int? BranchID { get; set; }
         string BranchTaxRegistrationID { get; set; }
         string BranchName { get; set; }
    }
}

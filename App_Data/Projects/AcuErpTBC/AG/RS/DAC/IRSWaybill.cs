﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AG.RS.DAC
{
    public interface IRSWaybill
    {
        string WaybillID { get; set; }
        int? WaybillState { get; set; }
        int? WaybillType { get; set; }
        int? TransStartTime { get; set; }
        DateTime? TransStartDate { get; set; }
        int? DeliveryTime { get; set; }
        DateTime? DeliveryDate { get; set; }
        string Status { get; set; }
        Boolean? Hold { get; set; }
        Guid? NoteID { get; set; }
    }
}

﻿﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.AP;

    [System.SerializableAttribute()]
	public class RSAPBillReceivedTaxInvoice : PX.Data.IBqlTable
	{
		#region ID
		public abstract class iD : PX.Data.IBqlField
		{
		}
		protected int? _ID;
		[PXDBIdentity(IsKey =true)]
		[PXUIField(Enabled = false)]
		public virtual int? ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				this._ID = value;
			}
		}
		#endregion
		#region APBillRefNbr
		public abstract class aPBillRefNbr : PX.Data.IBqlField
		{
		}
		protected string _APBillRefNbr;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "APBill RefNbr")]
        [PXDBDefault(typeof(APInvoice.refNbr))]
        [PXParent(typeof(Select<APInvoice,
                                 Where<APInvoice.refNbr, Equal<Current<RSAPBillReceivedTaxInvoice.aPBillRefNbr>>>>))]
        public virtual string APBillRefNbr
		{
			get
			{
				return this._APBillRefNbr;
			}
			set
			{
				this._APBillRefNbr = value;
			}
		}
		#endregion
		#region TaxInvoiceID
		public abstract class taxInvoiceID : PX.Data.IBqlField
		{
		}
		protected int? _TaxInvoiceID;
		[PXDBInt()]
		[PXUIField(DisplayName = "Tax Invoice ID")]
        [PXDefault(typeof(RSReceivedTaxInvoice.taxInvoiceID))]
        [PXParent(typeof(Select<RSReceivedTaxInvoice,
                                 Where<RSReceivedTaxInvoice.taxInvoiceID, Equal<Current<RSAPBillReceivedTaxInvoice.taxInvoiceID>>>>))]
        public virtual int? TaxInvoiceID
		{
			get
			{
				return this._TaxInvoiceID;
			}
			set
			{
				this._TaxInvoiceID = value;
			}
		}
		#endregion
		#region TaxInvoiceStatus
		public abstract class taxInvoiceStatus : PX.Data.IBqlField
		{
		}
		protected string _TaxInvoiceStatus;
		[PXDBString(1, IsFixed = true)]
		[PXUIField(DisplayName = "Tax Invoice Status")]
        [PXDefault(typeof(RSReceivedTaxInvoice.status))]
        public virtual string TaxInvoiceStatus
		{
			get
			{
				return this._TaxInvoiceStatus;
			}
			set
			{
				this._TaxInvoiceStatus = value;
			}
		}
		#endregion
	}
}

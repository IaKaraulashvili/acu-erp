﻿﻿namespace AG.RS.DAC
{
	using System;
	using PX.Data;
	
	[System.Serializable()]
	public class RSBarCode : PX.Data.IBqlTable
	{
		#region BarCode
		public abstract class barCode : PX.Data.IBqlField
		{
		}
		protected string _BarCode;
		[PXDBString(50, IsKey = true, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Bar Code")]
        [PXSelector(
        typeof(RSBarCode.barCode),
        typeof(RSBarCode.goodsName),
        typeof(RSBarCode.unitID),
         typeof(RSBarCode.unitTxt)
        )]
		public virtual string BarCode
		{
			get
			{
				return this._BarCode;
			}
			set
			{
				this._BarCode = value;
			}
		}
		#endregion
		#region GoodsName
		public abstract class goodsName : PX.Data.IBqlField
		{
		}
		protected string _GoodsName;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Goods Name")]
		public virtual string GoodsName
		{
			get
			{
				return this._GoodsName;
			}
			set
			{
				this._GoodsName = value;
			}
		}
		#endregion
		#region UnitID
		public abstract class unitID : PX.Data.IBqlField
		{
		}
		protected int? _UnitID;
		[PXDBInt()]
		[PXUIField(DisplayName = "UnitID")]
		public virtual int? UnitID
		{
			get
			{
				return this._UnitID;
			}
			set
			{
				this._UnitID = value;
			}
		}
		#endregion
		#region UnitTxt
		public abstract class unitTxt : PX.Data.IBqlField
		{
		}
		protected string _UnitTxt;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Unit Text")]
		public virtual string UnitTxt
		{
			get
			{
				return this._UnitTxt;
			}
			set
			{
				this._UnitTxt = value;
			}
		}
		#endregion
		#region ExciseID
		public abstract class exciseID : PX.Data.IBqlField
		{
		}
		protected int? _ExciseID;
		[PXDBInt()]
		[PXUIField(DisplayName = "ExciseID")]
		public virtual int? ExciseID
		{
			get
			{
				return this._ExciseID;
			}
			set
			{
				this._ExciseID = value;
			}
		}
		#endregion
	}
}

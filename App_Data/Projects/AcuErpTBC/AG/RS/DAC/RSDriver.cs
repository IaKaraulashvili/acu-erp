﻿﻿namespace AG.RS.DAC
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class RSDriver : PX.Data.IBqlTable
	{
		#region DriverID
		public abstract class driverID : PX.Data.IBqlField
		{
		}
		protected int? _DriverID;
		[PXDBIdentity(IsKey =true)]
		public virtual int? DriverID
		{
			get
			{
				return this._DriverID;
			}
			set
			{
				this._DriverID = value;
			}
		}
		#endregion
		#region DriverTaxID
		public abstract class driverTaxID : PX.Data.IBqlField
		{
		}
		protected string _DriverTaxID;
		[PXDBString(50, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Driver Tax ID")]
		public virtual string DriverTaxID
		{
			get
			{
				return this._DriverTaxID;
			}
			set
			{
				this._DriverTaxID = value;
			}
		}
		#endregion
		#region DriverName
		public abstract class driverName : PX.Data.IBqlField
		{
		}
		protected string _DriverName;
		[PXDBString(40, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Driver Name")]
		public virtual string DriverName
		{
			get
			{
				return this._DriverName;
			}
			set
			{
				this._DriverName = value;
			}
		}
		#endregion
		#region IsForeignCitizen
		public abstract class isForeignCitizen : PX.Data.IBqlField
		{
		}
		protected bool? _IsForeignCitizen;
		[PXDBBool()]
		[PXUIField(DisplayName = "Is Foreign Citizen")]
		public virtual bool? IsForeignCitizen
		{
			get
			{
				return this._IsForeignCitizen;
			}
			set
			{
				this._IsForeignCitizen = value;
			}
		}
		#endregion
		#region DefaultCarNumber
		public abstract class defaultCarNumber : PX.Data.IBqlField
		{
		}
		protected string _DefaultCarNumber;
		[PXDBString(10, IsUnicode = true)]
		[PXUIField(DisplayName = "Default Car Number")]
		public virtual string DefaultCarNumber
		{
			get
			{
				return this._DefaultCarNumber;
			}
			set
			{
				this._DefaultCarNumber = value;
			}
		}
		#endregion
	}
}

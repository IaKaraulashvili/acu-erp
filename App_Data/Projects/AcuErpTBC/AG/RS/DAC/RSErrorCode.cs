﻿﻿namespace AG.RS.DAC
 {
     using System;
     using PX.Data;

     [System.Serializable()]
     public class RSErrorCode : PX.Data.IBqlTable
     {
         #region ID
         public abstract class iD : PX.Data.IBqlField
         {
         }
         protected int? _ID;
         [PXDBInt(IsKey = true)]
         [PXDefault()]
         [PXUIField(DisplayName = "ID")]
         [PXSelector(
            typeof(RSErrorCode.iD),
            typeof(RSErrorCode.name))]
         public virtual int? ID
         {
             get
             {
                 return this._ID;
             }
             set
             {
                 this._ID = value;
             }
         }
         #endregion
         #region Name
         public abstract class name : PX.Data.IBqlField
         {
         }
         protected string _Name;
         [PXDBString(200, IsUnicode = true)]
         [PXUIField(DisplayName = "Name")]
         public virtual string Name
         {
             get
             {
                 return this._Name;
             }
             set
             {
                 this._Name = value;
             }
         }
         #endregion
         #region Type
         public abstract class type : PX.Data.IBqlField
         {
         }
         protected string _Type;
         [PXDBString(50, IsUnicode = true)]
         [PXUIField(DisplayName = "Type")]
         public virtual string Type
         {
             get
             {
                 return this._Type;
             }
             set
             {
                 this._Type = value;
             }
         }
         #endregion
     }
 }
using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace AG.RS.DAC
{
    [System.Serializable()]
    public class RSExcise : PX.Data.IBqlTable
    {
        #region ID
        public abstract class iD : PX.Data.IBqlField
        {

        }
        protected int? _ID;
        [PXDBInt(IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "ID")]
        [PXSelector(
            typeof(RSExcise.iD),
            typeof(RSExcise.name),
            typeof(RSExcise.measurement),
            typeof(RSExcise.itemCode),
            typeof(RSExcise.rate))]
        public virtual int? ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                this._ID = value;
            }
        }
        #endregion
        #region Name
        public abstract class name : PX.Data.IBqlField
        {
        }
        protected string _Name;
        [PXDBString(1000, IsUnicode = true)]
        [PXUIField(DisplayName = "Name")]
        public virtual string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                this._Name = value;
            }
        }
        #endregion
        #region Measurement
        public abstract class measurement : PX.Data.IBqlField
        {
        }
        protected string _Measurement;
        [PXDBString(100, IsUnicode = true)]
        [PXUIField(DisplayName = "Measurement")]
        public virtual string Measurement
        {
            get
            {
                return this._Measurement;
            }
            set
            {
                this._Measurement = value;
            }
        }
        #endregion
        #region ItemCode
        public abstract class itemCode : PX.Data.IBqlField
        {
        }
        protected string _ItemCode;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "ItemCode")]
        public virtual string ItemCode
        {
            get
            {
                return this._ItemCode;
            }
            set
            {
                this._ItemCode = value;
            }
        }
        #endregion
        #region Rate
        public abstract class rate : PX.Data.IBqlField
        {
        }
        protected decimal? _Rate;
        [PXDBDecimal(6)]
        [PXUIField(DisplayName = "Rate")]
        public virtual decimal? Rate
        {
            get
            {
                return this._Rate;
            }
            set
            {
                this._Rate = value;
            }
        }
        #endregion
    }
}
﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.GL;
    using AG.RS.Descriptor;
    using PX.Objects.TX;
    using PX.Objects.CR;
    using AG.Common;
    using Extensions.SM.Shared;
    using Extensions.AP.Shared;
    using Shared;
    using static Shared.RecivedTaxInvoiceStatusAttribute;
    using PX.Objects.CS;

    public class RSReceivedTaxInvoiceCorrected : RSReceivedTaxInvoice
    {

        public new abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }

        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Next Corrected", Enabled = false)]
        public override string TaxInvoiceNbr
        {
            get
            {
                return base.TaxInvoiceNbr;
            }

            set
            {
                base.TaxInvoiceNbr = value;
            }
        }


        public new abstract class correctedRefNbr : PX.Data.IBqlField
        {
        }

        [PXDBString(15, IsUnicode = true)]
        [PXUIField()]
        public override string CorrectedRefNbr
        {
            get
            {
                return base.CorrectedRefNbr;
            }

            set
            {
                base.CorrectedRefNbr = value;
            }
        }
    }

    [System.Serializable()]
    [PXPrimaryGraph(typeof(ReceivedTaxInvoiceEntry))]
    public class RSReceivedTaxInvoice : PX.Data.IBqlTable, IRSBranch
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Referrence Nbr")]
        [AutoNumber(typeof(RSSetup.receivedTaxInvoiceRequestNumberingID), typeof(RSReceivedTaxInvoiceRequest.createdDateTime))]
        [PXSelector(typeof(RSReceivedTaxInvoice.taxInvoiceNbr),
            typeof(RSReceivedTaxInvoice.taxInvoiceNbr),
            typeof(RSReceivedTaxInvoice.status),
            typeof(RSReceivedTaxInvoice.taxInvoiceNumber),
            typeof(RSReceivedTaxInvoice.vendorName),
            typeof(RSReceivedTaxInvoice.taxInvoiceSeriesNumber),
            typeof(RSReceivedTaxInvoice.taxInvoiceID),
            Filterable = true)]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region PostPeriod
        public abstract class postPeriod : PX.Data.IBqlField
        {
        }
        protected string _PostPeriod;
        [FinPeriodID()]
        [PXDefault()]
        [PXUIField(DisplayName = "Post Period", Visibility = PXUIVisibility.Visible)]
        [PXSelector(typeof(Search<TaxPeriod.taxPeriodID, Where<TaxPeriod.vendorID,
        Equal<Current<RSSetup.taxInvoiceVendorID>>>>), DirtyRead = true)]
        public virtual string PostPeriod
        {
            get
            {
                return this._PostPeriod;
            }
            set
            {
                this._PostPeriod = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(1, IsFixed = true)]
        [PXDefault(RecivedTaxInvoiceStatusAttribute.Status.Open)]
        [PXUIField(DisplayName = "Status")]
        [RecivedTaxInvoiceStatusAttribute()]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region TaxInvoiceSeries
        public abstract class taxInvoiceSeries : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceSeries;
        [PXDBString(50, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Tax Invoice Series")]
        public virtual string TaxInvoiceSeries
        {
            get
            {
                return this._TaxInvoiceSeries;
            }
            set
            {
                this._TaxInvoiceSeries = value;
            }
        }
        #endregion
        #region VendorID
        public abstract class vendorID : PX.Data.IBqlField
        {
        }
        protected int? _VendorID;

        [PXInt()]
        [PXUIField(DisplayName = "Vendor")]
        [VendorSelector(typeof(Where<LocationExtAddress.taxRegistrationID, Equal<Current<RSReceivedTaxInvoice.vendorTaxRegistrationID>>>))]
        [VendorUnbound(typeof(Where<LocationExtAddress.taxRegistrationID, Equal<RSReceivedTaxInvoice.vendorTaxRegistrationID>>))]
        public virtual int? VendorID
        {
            get
            {
                return this._VendorID;
            }
            set
            {
                this._VendorID = value;
            }
        }
        #endregion
        #region VendorTaxRegistrationID
        public abstract class vendorTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _VendorTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Vendor TaxRegistration ID")]
        public virtual string VendorTaxRegistrationID
        {
            get
            {
                return this._VendorTaxRegistrationID;
            }
            set
            {
                this._VendorTaxRegistrationID = value;
            }
        }
        #endregion
        #region VendorName
        public abstract class vendorName : PX.Data.IBqlField
        {
        }
        protected string _VendorName;
        [PXDBString(30, IsUnicode = true)]
        [PXUIField(DisplayName = "Vendor Name")]
        public virtual string VendorName
        {
            get
            {
                return this._VendorName;
            }
            set
            {
                this._VendorName = value;
            }
        }
        #endregion
        #region Note
        public abstract class note : PX.Data.IBqlField
        {
        }
        protected string _Note;
        [PXDBString(225, IsUnicode = true)]
        [PXUIField(DisplayName = "Note")]
        public virtual string Note
        {
            get
            {
                return this._Note;
            }
            set
            {
                this._Note = value;
            }
        }
        #endregion
        #region CreateDate
        public abstract class createDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreateDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Create Date")]
        public virtual DateTime? CreateDate
        {
            get
            {
                return this._CreateDate;
            }
            set
            {
                this._CreateDate = value;
            }
        }
        #endregion
        #region RequestDate
        public abstract class requestDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _RequestDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Request Date")]
        public virtual DateTime? RequestDate
        {
            get
            {
                return this._RequestDate;
            }
            set
            {
                this._RequestDate = value;
            }
        }
        #endregion
        #region ConfirmDate
        public abstract class confirmDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ConfirmDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Confirm Date")]
        public virtual DateTime? ConfirmDate
        {
            get
            {
                return this._ConfirmDate;
            }
            set
            {
                this._ConfirmDate = value;
            }
        }
        #endregion
        #region ConfirmedByID
        public abstract class confirmedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _ConfirmedByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Confirmed By", Enabled = false)]
        [UserSelector]
        public virtual Guid? ConfirmedByID
        {
            get
            {
                return this._ConfirmedByID;
            }
            set
            {
                this._ConfirmedByID = value;
            }
        }
        #endregion
        #region TaxInvoiceNumber
        public abstract class taxInvoiceNumber : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNumber;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "TaxInvoice Number")]
        public virtual string TaxInvoiceNumber
        {
            get
            {
                return this._TaxInvoiceNumber;
            }
            set
            {
                this._TaxInvoiceNumber = value;
            }
        }
        #endregion
        #region TaxInvoiceSeriesNumber
        public abstract class taxInvoiceSeriesNumber : PX.Data.IBqlField
        {
        }
        [PXString(55, IsUnicode = true)]
        [PXUIField(DisplayName = "TaxInvoice Series & Number")]
        public string TaxInvoiceSeriesNumber { get { return _TaxInvoiceSeries + " " + _TaxInvoiceNumber; } }
        #endregion
        #region TaxInvoiceID
        public abstract class taxInvoiceID : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceID;
        [PXDBInt()]
        [PXUIField(DisplayName = "TaxInvoice ID")]
        public virtual int? TaxInvoiceID
        {
            get
            {
                return this._TaxInvoiceID;
            }
            set
            {
                this._TaxInvoiceID = value;
            }
        }
        #endregion
        #region DeclarationNumber
        public abstract class declarationNumber : PX.Data.IBqlField
        {
        }
        protected string _DeclarationNumber;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Declaration Number")]
        public virtual string DeclarationNumber
        {
            get
            {
                return this._DeclarationNumber;
            }
            set
            {
                this._DeclarationNumber = value;
            }
        }
        #endregion
        #region TaxInvoiceStatus
        public abstract class taxInvoiceStatus : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "TaxInvoice Status")]
        [PXDefault(ExtStatus.Pending)]
        [PXIntList(
            new int[]
            {
                ExtStatus.Pending,
                ExtStatus.Rejected,
                ExtStatus.Confirmed,
                ExtStatus.Initial,
                ExtStatus.Corrected,
                ExtStatus.Canceled,
                ExtStatus.WaitingApprove,
                ExtStatus.ApprovedByVendor

            },
            new string[]
            {
                "Pending",
                "Rejected",
                "Confirmed",
                "Initial",
                "Corrected",
                "Canceled",
                "Waiting Approve",
                "Approved By Vendor"

            })]
        public virtual int? TaxInvoiceStatus
        {
            get
            {
                return this._TaxInvoiceStatus;
            }
            set
            {
                this._TaxInvoiceStatus = value;
            }
        }
        #endregion
        #region InitialTaxInvoiceNbr
        public abstract class initialTaxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _InitialTaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Initial Nbr")]
        public virtual string InitialTaxInvoiceNbr
        {
            get
            {
                return this._InitialTaxInvoiceNbr;
            }
            set
            {
                this._InitialTaxInvoiceNbr = value;
            }
        }
        #endregion
        #region InitialTaxInvoiceID
        public abstract class initialTaxInvoiceID : PX.Data.IBqlField
        {
        }
        protected int? _InitialTaxInvoiceID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Initial TaxInvoice ID")]
        public virtual int? InitialTaxInvoiceID
        {
            get
            {
                return this._InitialTaxInvoiceID;
            }
            set
            {
                this._InitialTaxInvoiceID = value;
            }
        }
        #endregion
        #region CorrectionType
        public abstract class correctionType : PX.Data.IBqlField
        {
        }
        protected int? _CorrectionType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Correction Type")]
        [PXIntList(
            new int[]
            {
                CorrectionTypeStatus.Canceled,
                CorrectionTypeStatus.TypeChange,
                CorrectionTypeStatus.AmountChange,
                CorrectionTypeStatus.Return
            },
            new string[]
            {
                "Canceled",
                "Type Change",
                "Amount Change",
                "Return"

            })]
        public virtual int? CorrectionType
        {
            get
            {
                return this._CorrectionType;
            }
            set
            {
                this._CorrectionType = value;
            }
        }
        #endregion
        #region CorrectionDate
        public abstract class correctionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CorrectionDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Correction Date")]
        public virtual DateTime? CorrectionDate
        {
            get
            {
                return this._CorrectionDate;
            }
            set
            {
                this._CorrectionDate = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected int? _BranchID;
        [BranchExt()]
        [PXUIField(DisplayName = "Branch")]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual int? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region BranchTaxRegistrationID
        public abstract class branchTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _BranchTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Tax ID")]
        public virtual string BranchTaxRegistrationID
        {
            get
            {
                return this._BranchTaxRegistrationID;
            }
            set
            {
                this._BranchTaxRegistrationID = value;
            }
        }
        #endregion
        #region BranchName
        public abstract class branchName : PX.Data.IBqlField
        {
        }
        protected string _BranchName;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Name")]
        public virtual string BranchName
        {
            get
            {
                return this._BranchName;
            }
            set
            {
                this._BranchName = value;
            }
        }
        #endregion
        #region VATAmt
        public abstract class vATAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _VATAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0", PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "VAT Amt", Enabled = false)]
        public virtual decimal? VATAmt
        {
            get
            {
                return this._VATAmt;
            }
            set
            {
                this._VATAmt = value;
            }
        }
        #endregion
        #region FullAmt
        public abstract class fullAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _FullAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Amount", Enabled = false)]
        public virtual decimal? FullAmt
        {
            get
            {
                return this._FullAmt;
            }
            set
            {
                this._FullAmt = value;
            }
        }
        #endregion
        #region TotalAmt
        public abstract class totalAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _TotalAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Total Amount", Enabled = false)]
        public virtual decimal? TotalAmt
        {
            get
            {
                return this._TotalAmt;
            }
            set
            {
                this._TotalAmt = value;
            }
        }
        #endregion
        #region TotalExciseAmt
        public abstract class totalExciseAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _TotalExciseAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Excise", Enabled = false)]
        public virtual decimal? TotalExciseAmt
        {
            get
            {
                return this._TotalExciseAmt;
            }
            set
            {
                this._TotalExciseAmt = value;
            }
        }
        #endregion
        #region Selected
        public abstract class selected : IBqlField
        {
        }
        [PXBool]
        [PXUIField(DisplayName = "Selected")]
        public virtual bool? Selected { get; set; }
        #endregion
        #region CorrectedRefNbr
        public abstract class correctedRefNbr : PX.Data.IBqlField
        {
        }
        protected string _CorrectedRefNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Prev. Corrected", Visibility = PXUIVisibility.Visible, Enabled = false)]
        public virtual string CorrectedRefNbr
        {
            get
            {
                return this._CorrectedRefNbr;
            }
            set
            {
                this._CorrectedRefNbr = value;
            }
        }
        #endregion
        #region PrepaymentTaxInvoice
        public abstract class prepaymentTaxInvoice : PX.Data.IBqlField
        {
        }
        protected bool? _PrepaymentTaxInvoice;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Prepayment Tax Invoice", Visibility = PXUIVisibility.Visible)]
        public virtual bool? PrepaymentTaxInvoice
        {
            get
            {
                return this._PrepaymentTaxInvoice;
            }
            set
            {
                this._PrepaymentTaxInvoice = value;
            }
        }
        #endregion

        #region System Properties
        #region ItemCntr
        public abstract class itemCntr : PX.Data.IBqlField
        {
        }
        protected Int32? _ItemCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual Int32? ItemCntr
        {
            get
            {
                return this._ItemCntr;
            }
            set
            {
                this._ItemCntr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote(DescriptionField = typeof(RSWaybill.waybillNbr))]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
        #endregion
        #region Methods

        public bool CanConfirmTaxInvoice()
        {
            bool statusOk = false;

            switch (this.TaxInvoiceStatus)
            {
                case ExtStatus.WaitingApprove:
                case ExtStatus.Corrected:
                    statusOk = true;
                    break;
                default:
                    break;
            }

            return statusOk && VendorID != null;
        }

        public bool CanRejectTaxInvoice()
        {
            bool result = false;
            switch (this.TaxInvoiceStatus)
            {
                case ExtStatus.WaitingApprove:
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public bool CanCreateBillAndCorrect()
        {
            bool result = false;
            switch (this.TaxInvoiceStatus)
            {
                case ExtStatus.ApprovedByVendor:
                    result = true;
                    break;
                case ExtStatus.Corrected:
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public RSReceivedTaxInvoice CopyFrom(RSTaxInvoice taxInvoice)
        {
            DACMapper.Map<RSTaxInvoice, RSReceivedTaxInvoice>(taxInvoice, this);

            return this;
        }

        #endregion
    }
}

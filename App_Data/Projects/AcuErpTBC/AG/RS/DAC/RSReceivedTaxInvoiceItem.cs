﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.IN;
using AG.RS;
using AG.RS.Descriptor;
using AG.RS.Shared;

namespace AG.RS.DAC
{
    [System.Serializable()]
    public class RSReceivedTaxInvoiceItem : PX.Data.IBqlTable
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "TaxInvoiceNbr")]
        [PXDBDefault(typeof(RSReceivedTaxInvoice.taxInvoiceNbr), DefaultForUpdate = false)]
        [PXParent(typeof(Select<RSReceivedTaxInvoice, Where<RSReceivedTaxInvoice.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoiceItem.taxInvoiceNbr>>>>))]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region TaxInvoiceItemNbr
        public abstract class taxInvoiceItemNbr : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceItemNbr;
        [PXDBInt(IsKey = true)]
        [PXLineNbr(typeof(RSReceivedTaxInvoice.itemCntr))]
        [PXUIField(DisplayName = "Tax Invoice Item Nbr", Visible = false)]
        public virtual int? TaxInvoiceItemNbr
        {
            get
            {
                return this._TaxInvoiceItemNbr;
            }
            set
            {
                this._TaxInvoiceItemNbr = value;
            }
        }
        #endregion
        #region WaybillNbr
        public abstract class waybillNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Waybill Nbr")]
        public virtual string WaybillNbr
        {
            get
            {
                return this._WaybillNbr;
            }
            set
            {
                this._WaybillNbr = value;
            }
        }
        #endregion
        #region ItemRowID
        public abstract class itemRowID : PX.Data.IBqlField
        {
        }
        protected int? _ItemRowID;
        [PXDBInt()]
        [PXDefault(0)]
        [PXUIField(DisplayName = "GE Row ID")]
        public virtual int? ItemRowID
        {
            get
            {
                return this._ItemRowID;
            }
            set
            {
                this._ItemRowID = value;
            }
        }
        #endregion
        #region InventoryID
        public abstract class inventoryID : PX.Data.IBqlField
        {
        }
        protected int? _InventoryID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Inventory ID")]
        [PXSelector(typeof(Search<PX.Objects.IN.InventoryItem.inventoryID, Where<InventoryItem.itemType, Equal<INItemTypes.subAssembly>,
                                                                            Or<InventoryItem.itemType, Equal<INItemTypes.finishedGood>,
                                                                            Or<InventoryItem.itemType, Equal<INItemTypes.nonStockItem>,
                                                                            Or<InventoryItem.itemType, Equal<INItemTypes.component>>>>>>),
         typeof(PX.Objects.IN.InventoryItem.inventoryID), typeof(PX.Objects.IN.InventoryItem.descr),
         typeof(PX.Objects.IN.InventoryItem.itemClassID), typeof(PX.Objects.IN.InventoryItem.itemStatus),
         typeof(PX.Objects.IN.InventoryItem.itemType), SubstituteKey = typeof(PX.Objects.IN.InventoryItem.inventoryCD))]
        public virtual int? InventoryID
        {
            get
            {
                return this._InventoryID;
            }
            set
            {
                this._InventoryID = value;
            }
        }
        #endregion
        #region ItemName
        public abstract class itemName : PX.Data.IBqlField
        {
        }
        protected string _ItemName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Item Name")]
        public virtual string ItemName
        {
            get
            {
                return this._ItemName;
            }
            set
            {
                this._ItemName = value;
            }
        }
        #endregion
        #region ItemRowStatus
        public abstract class itemRowStatus : PX.Data.IBqlField
        {
        }
        protected int? _ItemRowStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "Status")]
        [PXDefault(AG.RS.DAC.RSTaxInvoiceItem.RSTaxInvoiceItemStatuses.Active)]
        [PXIntList(
           new int[]{
                AG.RS.DAC.RSTaxInvoiceItem.RSTaxInvoiceItemStatuses.Active,
                AG.RS.DAC.RSTaxInvoiceItem.RSTaxInvoiceItemStatuses.Deleted
            },
           new string[]{
                   "Active",
                   "Deleted"
            }
               )]
        public virtual int? ItemRowStatus
        {
            get
            {
                return this._ItemRowStatus;
            }
            set
            {
                this._ItemRowStatus = value;
            }
        }
        #endregion
        #region ItemBarCode
        public abstract class itemBarCode : PX.Data.IBqlField
        {
        }
        protected string _ItemBarCode;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Item Code")]
        public virtual string ItemBarCode
        {
            get
            {
                return this._ItemBarCode;
            }
            set
            {
                this._ItemBarCode = value;
            }
        }
        #endregion
        #region ExtUOM
        public abstract class extUOM : PX.Data.IBqlField
        {
        }
        protected int? _ExtUOM;
        [PXDBInt()]
        [PXUIField(DisplayName = "UOM")]
        [RSUnitList]
        public virtual int? ExtUOM
        {
            get
            {
                return this._ExtUOM;
            }
            set
            {
                this._ExtUOM = value;
            }
        }
        #endregion
        #region OtherUOMDescription
        public abstract class otherUOMDescription : PX.Data.IBqlField
        {
        }
        protected string _OtherUOMDescription;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Other UOMD escription")]
        public virtual string OtherUOMDescription
        {
            get
            {
                return this._OtherUOMDescription;
            }
            set
            {
                this._OtherUOMDescription = value;
            }
        }
        #endregion
        #region ItemQty
        public abstract class itemQty : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemQty;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Item Qty")]
        public virtual decimal? ItemQty
        {
            get
            {
                return this._ItemQty;
            }
            set
            {
                this._ItemQty = value;
            }
        }
        #endregion
        #region ItemExtraQty
        public abstract class itemExtraQty : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemExtraQty;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "ItemExtraQty")]
        public virtual decimal? ItemExtraQty
        {
            get
            {
                return this._ItemExtraQty;
            }
            set
            {
                this._ItemExtraQty = value;
            }
        }
        #endregion

        #region ItemAmt
        public abstract class itemAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Amount")]
        [PXFormula(
                   null,
                   typeof(SumCalc<RSReceivedTaxInvoice.fullAmt>))]
        public virtual decimal? ItemAmt
        {
            get
            {
                return this._ItemAmt;
            }
            set
            {
                this._ItemAmt = value;
            }
        }
        #endregion
        #region ExciseID
        public abstract class exciseID : PX.Data.IBqlField
        {
        }
        protected int? _ExciseID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Excise ID")]
        public virtual int? ExciseID
        {
            get
            {
                return this._ExciseID;
            }
            set
            {
                this._ExciseID = value;
            }
        }
        #endregion
        #region ExciseRate
        public abstract class exciseRate : PX.Data.IBqlField
        {
        }
        protected decimal? _ExciseRate;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Excise Rate")]
        public virtual decimal? ExciseRate
        {
            get
            {
                return this._ExciseRate;
            }
            set
            {
                this._ExciseRate = value;
            }
        }
        #endregion
        #region ExciseAmt
        public abstract class exciseAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _ExciseAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0", PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Excise Amt")]
        [PXFormula(
                   null,
                   typeof(SumCalc<RSReceivedTaxInvoice.totalExciseAmt>))]
        public virtual decimal? ExciseAmt
        {
            get
            {
                return this._ExciseAmt;
            }
            set
            {
                this._ExciseAmt = value;
            }
        }
        #endregion
        #region TaxType
        public abstract class taxType : PX.Data.IBqlField
        {
        }
        protected int? _TaxType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Tax Type")]
        [PXDefault(GETaxTypes.Normal)]
        [PXIntList(
            new int[]{
                GETaxTypes.Normal,
                GETaxTypes.Nullable,
                GETaxTypes.TaxFree
            },
            new string[]{
                   "Normal",
                   "Nullable",
                   "TaxFree"
            }
                )]
        public virtual int? TaxType
        {
            get
            {
                return this._TaxType;
            }
            set
            {
                this._TaxType = value;
            }
        }
        #endregion
        #region VATAmt
        public abstract class vATAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _VATAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0", PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "VAT Amt")]
        [PXFormula(
                   null,
                   typeof(SumCalc<RSReceivedTaxInvoice.vATAmt>))]
        public virtual decimal? VATAmt
        {
            get
            {
                return this._VATAmt;
            }
            set
            {
                this._VATAmt = value;
            }
        }
        #endregion
        #region FullAmt
        public abstract class fullAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _FullAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Full Amt")]
        [PXFormula(
                   null,
                   typeof(SumCalc<RSReceivedTaxInvoice.totalAmt>))]
        public virtual decimal? FullAmt
        {
            get
            {
                return this._FullAmt;
            }
            set
            {
                this._FullAmt = value;
            }
        }
        #endregion


        #region UnitPrice
        public abstract class unitPrice : PX.Data.IBqlField
        {
        }
        protected decimal? _UnitPrice;
        [PXDBDecimal(6)]
        [PXUIField(DisplayName = "Unit Price")]
        //[PXFormula(typeof(Div<RSReceivedTaxInvoiceItem.fullAmt, RSReceivedTaxInvoiceItem.itemQty>))]
        public virtual decimal? UnitPrice
        {
            get
            {
                return this._UnitPrice;
            }
            set
            {
                this._UnitPrice = value;
            }
        }
        #endregion


        #region RowID
        public abstract class rowID : PX.Data.IBqlField
        {
        }
        protected int? _RowID;
        [PXInt()]        
        [PXUIField(DisplayName = "GE Row ID")]
        public virtual int? RowID
        {
            get
            {
                return this._RowID;
            }
            set
            {
                this._RowID = value;
            }
        }
        #endregion

        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion



    }
}
﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using PX.Objects.AP;
    using PX.Objects.TX;
    using AG.RS.Descriptor;
    using PX.Objects.GL;
    using PX.Objects.CR;
    using PX.Objects.AR;
    using PX.Objects.CS;
    using Shared;

    [System.Serializable()]
    public class RSReceivedTaxInvoiceRequest : PX.Data.IBqlTable, IRSBranch
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Referrence Nbr.")]
        [AutoNumber(typeof(RSSetup.receivedTaxInvoiceRequestNumberingID), typeof(RSReceivedTaxInvoiceRequest.createdDateTime))]
        [PXSelector(typeof(RSReceivedTaxInvoiceRequest.taxInvoiceNbr),
            typeof(RSReceivedTaxInvoiceRequest.taxInvoiceNbr),
            typeof(RSReceivedTaxInvoiceRequest.status),
            typeof(RSReceivedTaxInvoiceRequest.viewDate),
            typeof(RSReceivedTaxInvoiceRequest.requestDate),
            typeof(RSReceivedTaxInvoiceRequest.confirmDate),
            typeof(RSReceivedTaxInvoiceRequest.createDate),
            typeof(RSReceivedTaxInvoiceRequest.customerName),
            Filterable = true)]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region PostPeriod
        public abstract class postPeriod : PX.Data.IBqlField
        {
        }
        protected string _PostPeriod;
        [FinPeriodID()]
        [PXDefault()]
        [PXUIField(DisplayName = "Post Period", Visibility = PXUIVisibility.Visible)]
        [PXSelector(typeof(Search<TaxPeriod.taxPeriodID, Where<TaxPeriod.vendorID,
        Equal<Current<RSSetup.taxInvoiceVendorID>>>>), DirtyRead = true)]
        public virtual String PostPeriod
        {
            get
            {
                return this._PostPeriod;
            }
            set
            {
                this._PostPeriod = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString()]
        [PXUIField(DisplayName = "Status")]
        [PXStringList(
                    new string[]
                    {
                       TaxInvoiceStatus.OnHold,
                       TaxInvoiceStatus.Received,
                       TaxInvoiceStatus.Confirmed,
                       TaxInvoiceStatus.Canceled
                    },
                    new string[]
                    {
                       TaxInvoiceStatus.UI.OnHold,
                       TaxInvoiceStatus.UI.Received,
                       TaxInvoiceStatus.UI.Confirmed,
                       TaxInvoiceStatus.UI.Canceled
                    })]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected bool? _Hold;
        [PXDBBool()]
        [PXDefault(true)]
        [PXUIField(DisplayName = "Hold")]
        public virtual bool? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }


        #endregion
        #region CustomerID
        public abstract class customerID : PX.Data.IBqlField
        {
        }
        protected Int32? _CustomerID;
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Customer", Required = false)]
        [CustomerActive(Visibility = PXUIVisibility.SelectorVisible, DescriptionField = typeof(Customer.acctName), Filterable = true, TabOrder = 2)]
        public virtual Int32? CustomerID
        {
            get
            {
                return this._CustomerID;
            }
            set
            {
                this._CustomerID = value;
            }
        }
        #endregion
        #region CustomerTaxRegistrationID
        public abstract class customerTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _CustomerTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Customer Tax Registration ID")]
        public virtual string CustomerTaxRegistrationID
        {
            get
            {
                return this._CustomerTaxRegistrationID;
            }
            set
            {
                this._CustomerTaxRegistrationID = value;
            }
        }
        #endregion
        #region CustomerName
        public abstract class customerName : PX.Data.IBqlField
        {
        }
        protected string _CustomerName;
        [PXDBString(225, IsUnicode = true)]
        [PXUIField(DisplayName = "Customer Name")]
        public virtual string CustomerName
        {
            get
            {
                return this._CustomerName;
            }
            set
            {
                this._CustomerName = value;
            }
        }
        #endregion
        #region TaxInvoiceNumber
        public abstract class taxInvoiceNumber : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNumber;
        [PXDBString(225, IsUnicode = true)]
        [PXUIField(DisplayName = "Tax Invoice Number")]
        public virtual string TaxInvoiceNumber
        {
            get
            {
                return this._TaxInvoiceNumber;
            }
            set
            {
                this._TaxInvoiceNumber = value;
            }
        }
        #endregion
        #region CreateDate
        public abstract class createDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreateDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Create Date")]
        public virtual DateTime? CreateDate
        {
            get
            {
                return this._CreateDate;
            }
            set
            {
                this._CreateDate = value;
            }
        }
        #endregion
        #region RequestDate
        public abstract class requestDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _RequestDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Request Date")]
        public virtual DateTime? RequestDate
        {
            get
            {
                return this._RequestDate;
            }
            set
            {
                this._RequestDate = value;
            }
        }
        #endregion
        #region ViewDate
        public abstract class viewDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ViewDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "View Date")]
        public virtual DateTime? ViewDate
        {
            get
            {
                return this._ViewDate;
            }
            set
            {
                this._ViewDate = value;
            }
        }
        #endregion
        #region ConfirmDate
        public abstract class confirmDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ConfirmDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Confirm Date")]
        public virtual DateTime? ConfirmDate
        {
            get
            {
                return this._ConfirmDate;
            }
            set
            {
                this._ConfirmDate = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected Int32? _BranchID;
        [PXUIField(Required = false)]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [BranchExt(DisplayName = "New Branch")]
        public virtual Int32? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region BranchTaxRegistrationID
        public abstract class branchTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _BranchTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Tax Registration ID")]
        public virtual string BranchTaxRegistrationID
        {
            get
            {
                return this._BranchTaxRegistrationID;
            }
            set
            {
                this._BranchTaxRegistrationID = value;
            }
        }
        #endregion
        #region BranchName
        public abstract class branchName : PX.Data.IBqlField
        {
        }
        protected string _BranchName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Name")]
        public virtual string BranchName
        {
            get
            {
                return this._BranchName;
            }
            set
            {
                this._BranchName = value;
            }
        }
        #endregion
        #region SenderNote
        public abstract class senderNote : PX.Data.IBqlField
        {
        }
        protected string _SenderNote;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Sender Note")]
        public virtual string SenderNote
        {
            get
            {
                return this._SenderNote;
            }
            set
            {
                this._SenderNote = value;
            }
        }
        #endregion
        #region System Properties
        #region ItemCntr
        public abstract class itemCntr : PX.Data.IBqlField
        {
        }
        protected Int32? _ItemCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual Int32? ItemCntr
        {
            get
            {
                return this._ItemCntr;
            }
            set
            {
                this._ItemCntr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote(DescriptionField = typeof(RSReceivedWaybill.waybillNbr))]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
        #endregion

        #region TaxInvoiceStatus
        public class TaxInvoiceStatus
        {
            public const string OnHold = "H";
            public const string Received = "R";
            public const string Confirmed = "C";
            public const string Canceled = "C";

            public class UI
            {
                public const string OnHold = "Hold";
                public const string Received = "Received";
                public const string Confirmed = "Confirmed";
                public const string Canceled = "Canceled";
            }
        }
        #endregion
    }
}

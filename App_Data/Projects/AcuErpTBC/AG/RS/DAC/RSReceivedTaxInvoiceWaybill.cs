﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using Descriptor;

    [System.Serializable()]
    public class RSReceivedTaxInvoiceWaybill : PX.Data.IBqlTable
    {

        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Tax Invoice Nbr")]
        [PXDBDefault(typeof(RSReceivedTaxInvoice.taxInvoiceNbr), DefaultForUpdate = true, DefaultForInsert = true)]
        [PXParent(typeof(Select<RSReceivedTaxInvoice, Where<RSReceivedTaxInvoice.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoiceWaybill.taxInvoiceNbr>>>>))]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region WaybillNbr
        public abstract class wayBillNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Waybill Ref. Nbr")]
        [PXSelector(typeof(Search<RSReceivedWaybill.waybillNbr>), typeof(RSReceivedWaybill.waybillNumber))]
        public virtual string WaybillNbr
        {
            get
            {
                return this._WaybillNbr;
            }
            set
            {
                this._WaybillNbr = value;
            }
        }
        #endregion
        //#region WaybillType
        //public abstract class wayBillType : PX.Data.IBqlField
        //{
        //}
        //protected int? _WaybillType;
        //[PXDBInt()]
        //[PXDefault(DAC.WaybillType.Internal)]
        //[PXUIField(DisplayName = "Waybill Type")]
        //[WaybillTypeList]
        //public virtual int? WaybillType
        //{
        //    get
        //    {
        //        return this._WaybillType;
        //    }
        //    set
        //    {
        //        this._WaybillType = value;
        //    }
        //}
        //#endregion

        #region ActivationDate
        public abstract class activationDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ActivationDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Activation Date")]
        public virtual DateTime? ActivationDate
        {
            get
            {
                return this._ActivationDate;
            }
            set
            {
                this._ActivationDate = value;
            }
        }
        #endregion

        #region TransStartDate
        public abstract class transStartDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _TransStartDate;
        [PXDBDate(UseTimeZone = false)]
        [PXUIField(DisplayName = "Transportation Start Date")]
        public virtual DateTime? TransStartDate
        {
            get
            {
                return this._TransStartDate;
            }
            set
            {
                this._TransStartDate = value;
            }
        }
        #endregion


        #region Amount
        public abstract class amount : PX.Data.IBqlField
        {
        }
        protected decimal? _Amount;
        [PXDBDecimal(6)]
        [PXUIField(DisplayName = "Amount")]
        public virtual decimal? Amount
        {
            get
            {
                return this._Amount;
            }
            set
            {
                this._Amount = value;
            }
        }
        #endregion

        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.AP;
using AG.RS;


namespace AG.RS.DAC
{

    [System.Serializable()]
    [PXPrimaryGraph(typeof(RSSetupMaint))]
    [PXCacheName("RS Preferences")]
    public partial class RSSetup : PX.Data.IBqlTable
    {
        #region WaybillIsActive
        public abstract class waybillIsActive : PX.Data.IBqlField
        {
        }
        protected Boolean? _WaybillIsActive;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Active")]
        public virtual Boolean? WaybillIsActive
        {
            get
            {
                return this._WaybillIsActive;
            }
            set
            {
                this._WaybillIsActive = value;
            }
        }
        #endregion
        #region WaybillAccount
        public abstract class waybillAccount : PX.Data.IBqlField
        {
        }
        protected String _WaybillAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Account")]
        public virtual String WaybillAccount
        {
            get
            {
                return this._WaybillAccount;
            }
            set
            {
                this._WaybillAccount = value;
            }
        }
        #endregion
        #region WaybillLicence
        public abstract class waybillLicence : PX.Data.IBqlField
        {
        }
        protected String _WaybillLicence;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "License Key")]
        //[PXGEACryptString()]
        public virtual String WaybillLicence
        {
            get
            {
                return this._WaybillLicence;
            }
            set
            {
                this._WaybillLicence = value;
            }
        }
        #endregion
        #region WaybillUrl
        public abstract class waybillUrl : PX.Data.IBqlField
        {
        }
        protected String _WaybillUrl;
        [PXDBString(60, IsUnicode = true)]
        [PXUIField(DisplayName = "URL")]
        public virtual String WaybillUrl
        {
            get
            {
                return this._WaybillUrl;
            }
            set
            {
                this._WaybillUrl = value;
            }
        }
        #endregion
        #region WaybillTimeout
        public abstract class waybillTimeout : PX.Data.IBqlField
        {
        }
        protected Int32? _WaybillTimeout;
        [PXDBInt()]
        [PXDefault(30)]
        [PXUIField(DisplayName = "Request Timeout (sec.)")]
        public virtual Int32? WaybillTimeout
        {
            get
            {
                return this._WaybillTimeout;
            }
            set
            {
                this._WaybillTimeout = value;
            }
        }
        #endregion
        #region WaybillNumberingID
        public abstract class waybillNumberingID : PX.Data.IBqlField
        {
        }
        protected String _WaybillNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Waybill Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual String WaybillNumberingID
        {
            get
            {
                return this._WaybillNumberingID;
            }
            set
            {
                this._WaybillNumberingID = value;
            }
        }
        #endregion
        #region ReceivedWaybillNumberingID
        public abstract class receivedWaybillNumberingID : PX.Data.IBqlField
        {
        }
        protected String _ReceivedWaybillNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Received Waybill Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual String ReceivedWaybillNumberingID
        {
            get
            {
                return this._ReceivedWaybillNumberingID;
            }
            set
            {
                this._ReceivedWaybillNumberingID = value;
            }
        }
        #endregion
        #region WaybillHistoryNumberingID
        public abstract class waybillHistoryNumberingID : PX.Data.IBqlField
        {
        }
        protected String _WaybillHistoryNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Waybill History Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual String WaybillHistoryNumberingID
        {
            get
            {
                return this._WaybillHistoryNumberingID;
            }
            set
            {
                this._WaybillHistoryNumberingID = value;
            }
        }
        #endregion
        #region WaybillHoldOnEntry
        public abstract class waybillHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected Boolean? _WaybillHoldOnEntry;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Hold Waybill On Entry")]
        public virtual Boolean? WaybillHoldOnEntry
        {
            get
            {
                return this._WaybillHoldOnEntry;
            }
            set
            {
                this._WaybillHoldOnEntry = value;
            }
        }
        #endregion
        #region WaybillPostOnShipment
        public abstract class waybillPostOnShipment : PX.Data.IBqlField
        {
        }
        protected Boolean? _WaybillPostOnShipment;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Post Automatically Waybill On Shipment")]
        public virtual Boolean? WaybillPostOnShipment
        {
            get
            {
                return this._WaybillPostOnShipment;
            }
            set
            {
                this._WaybillPostOnShipment = value;
            }
        }
        #endregion
        #region WaybillPostOnTransfer
        public abstract class waybillPostOnTransfer : PX.Data.IBqlField
        {
        }
        protected Boolean? _WaybillPostOnTransfer;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Post Automatically Waybill On Transfer")]
        public virtual Boolean? WaybillPostOnTransfer
        {
            get
            {
                return this._WaybillPostOnTransfer;
            }
            set
            {
                this._WaybillPostOnTransfer = value;
            }
        }
        #endregion
        #region TaxInvoiceIsActive
        public abstract class taxInvoiceIsActive : PX.Data.IBqlField
        {
        }
        protected Boolean? _TaxInvoiceIsActive;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Active")]
        public virtual Boolean? TaxInvoiceIsActive
        {
            get
            {
                return this._TaxInvoiceIsActive;
            }
            set
            {
                this._TaxInvoiceIsActive = value;
            }
        }
        #endregion
        #region TaxInvoiceAccount
        public abstract class taxInvoiceAccount : PX.Data.IBqlField
        {
        }
        protected String _TaxInvoiceAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Account")]
        public virtual String TaxInvoiceAccount
        {
            get
            {
                return this._TaxInvoiceAccount;
            }
            set
            {
                this._TaxInvoiceAccount = value;
            }
        }
        #endregion
        #region TaxInvoiceLicence
        public abstract class taxInvoiceLicence : PX.Data.IBqlField
        {
        }
        protected String _TaxInvoiceLicence;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "License Key")]
        //[PXGEACryptString()]
        public virtual String TaxInvoiceLicence
        {
            get
            {
                return this._TaxInvoiceLicence;
            }
            set
            {
                this._TaxInvoiceLicence = value;
            }
        }
        #endregion
        #region TaxInvoiceUrl
        public abstract class taxInvoiceUri : PX.Data.IBqlField
        {
        }
        protected String _TaxInvoiceUri;
        [PXDBString(60, IsUnicode = true)]
        [PXUIField(DisplayName = "URL")]
        public virtual String TaxInvoiceUri
        {
            get
            {
                return this._TaxInvoiceUri;
            }
            set
            {
                this._TaxInvoiceUri = value;
            }
        }
        #endregion
        #region TaxInvoiceTimeout
        public abstract class taxInvoiceTimeout : PX.Data.IBqlField
        {
        }
        protected Int32? _TaxInvoiceTimeout;
        [PXDBInt()]
        [PXDefault(30)]
        [PXUIField(DisplayName = "Request Timeout (sec.)")]
        public virtual Int32? TaxInvoiceTimeout
        {
            get
            {
                return this._TaxInvoiceTimeout;
            }
            set
            {
                this._TaxInvoiceTimeout = value;
            }
        }
        #endregion
        #region TaxInvoiceHoldOnEntry
        public abstract class taxInvoiceHoldOnEntry : PX.Data.IBqlField
        {
        }
        protected Boolean? _TaxInvoiceOnEntry;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Hold Tax Invoice On Entry")]
        public virtual Boolean? TaxInvoiceHoldOnEntry
        {
            get
            {
                return this._TaxInvoiceOnEntry;
            }
            set
            {
                this._TaxInvoiceOnEntry = value;
            }
        }
        #endregion
        #region TaxInvoicePostOnBillAdjustment
        public abstract class taxInvoicePostOnBillAdjustment : PX.Data.IBqlField
        {
        }
        protected Boolean? _TaxInvoicePostOnBillAdjustment;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Post Automatically Tax Invoice On Bill&Adjustment")]
        public virtual Boolean? TaxInvoicePostOnBillAdjustment
        {
            get
            {
                return this._TaxInvoicePostOnBillAdjustment;
            }
            set
            {
                this._TaxInvoicePostOnBillAdjustment = value;
            }
        }
        #endregion
        #region TaxInvoicePostOnInvoiceAndMemo
        public abstract class taxInvoicePostOnInvoiceAndMemo : PX.Data.IBqlField
        {
        }
        protected Boolean? _TaxInvoicePostOnInvoiceAndMemo;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Post Automatically Tax Invoice On Invoice And Memo")]
        public virtual Boolean? TaxInvoicePostOnInvoiceAndMemo
        {
            get
            {
                return this._TaxInvoicePostOnInvoiceAndMemo;
            }
            set
            {
                this._TaxInvoicePostOnInvoiceAndMemo = value;
            }
        }
        #endregion
        #region TaxInvoiceRequestNumberingID
        public abstract class taxInvoiceRequestNumberingID : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceRequestNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Tax Invoice Request Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual string TaxInvoiceRequestNumberingID
        {
            get
            {
                return this._TaxInvoiceRequestNumberingID;
            }
            set
            {
                this._TaxInvoiceRequestNumberingID = value;
            }
        }
        #endregion
        #region ReceivedTaxInvoiceRequestNumberingID
        public abstract class receivedTaxInvoiceRequestNumberingID : PX.Data.IBqlField
        {
        }
        protected string _ReceivedTaxInvoiceRequestNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Received Taxt Invoice Request Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual string ReceivedTaxInvoiceRequestNumberingID
        {
            get
            {
                return this._ReceivedTaxInvoiceRequestNumberingID;
            }
            set
            {
                this._ReceivedTaxInvoiceRequestNumberingID = value;
            }
        }


        #endregion
        #region TaxInvoiceNumbering
        public abstract class taxInvoiceNumberingID : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Taxt Invoice Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual string TaxInvoiceNumberingID
        {
            get
            {
                return this._TaxInvoiceNumberingID;
            }
            set
            {
                this._TaxInvoiceNumberingID = value;
            }
        }

        #endregion
        #region ReceivedTaxInvoiceNumberingID
        public abstract class receivedTaxInvoiceNumberingID : PX.Data.IBqlField
        {
        }
        protected string _ReceivedTaxInvoiceNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        [PXUIField(DisplayName = "Received Taxt Invoice Numbering", Visibility = PXUIVisibility.Visible)]
        public virtual string ReceivedTaxInvoiceNumberingID
        {
            get
            {
                return this._ReceivedTaxInvoiceNumberingID;
            }
            set
            {
                this._ReceivedTaxInvoiceNumberingID = value;
            }
        }
        #endregion  
        #region TaxInvoiceVendorID
        public abstract class taxInvoiceVendorID : PX.Data.IBqlField
        {
        }
        protected Int32? _TaxInvoiceVendorID;
        [Vendor(typeof(Search<Vendor.bAccountID, Where<Vendor.taxAgency, Equal<boolTrue>>>), DisplayName = "Tax Agency")]
        public virtual Int32? TaxInvoiceVendorID
        {
            get
            {
                return this._TaxInvoiceVendorID;
            }
            set
            {
                this._TaxInvoiceVendorID = value;
            }
        }
        #endregion
        #region System Columns
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected Byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual Byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected String _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual String CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected String _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual String LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #endregion
    }
}


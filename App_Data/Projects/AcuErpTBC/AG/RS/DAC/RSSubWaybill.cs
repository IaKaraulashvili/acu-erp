﻿
namespace AG.RS.DAC
{
    using System;
    using PX.Data;

    [System.Serializable()]
    public class RSSubWaybill : PX.Data.IBqlTable
    {
        #region WaybillID
        public abstract class waybillID : PX.Data.IBqlField
        {
        }
        protected long? _WaybillID;
        [PXDBLong(IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "WaybillID")]
        public virtual long? WaybillID
        {
            get
            {
                return this._WaybillID;
            }
            set
            {
                this._WaybillID = value;
            }
        }
        #endregion
        #region SubWaybillID
        public abstract class subWaybillID : PX.Data.IBqlField
        {
        }
        protected string _SubWaybillID;
        [PXDBString(20, IsKey = true, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "SubWaybillID")]
        public virtual string SubWaybillID
        {
            get
            {
                return this._SubWaybillID;
            }
            set
            {
                this._SubWaybillID = value;
            }
        }
        #endregion
        #region SubWayBIllNumber
        public abstract class subWayBIllNumber : PX.Data.IBqlField
        {
        }
        protected string _SubWayBIllNumber;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "SubWayBIllNumber")]
        public virtual string SubWayBIllNumber
        {
            get
            {
                return this._SubWayBIllNumber;
            }
            set
            {
                this._SubWayBIllNumber = value;
            }
        }
        #endregion
    }
}
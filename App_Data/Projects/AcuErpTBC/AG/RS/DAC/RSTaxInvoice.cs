﻿using System;
using AG.RS.Descriptor;
using PX.Objects.AR;
using PX.Objects.GL;

namespace AG.RS.DAC
{
    using PX.Data;
    using PX.Objects.CS;
    using PX.Objects.TX;
    using Shared;
    using static Shared.TaxInvoiceStatusAttribute;

    public class TaxInvoiceCorrectionPopup : IBqlTable
    {

        public abstract class correctionType : PX.Data.IBqlField
        {
        }
        protected int? _CorrectionType;
        [PXInt()]
        [PXUIField(DisplayName = "Correction Type")]
        [TaxInvoiceCorrectionTypeList()]
        public virtual int? CorrectionType
        {
            get
            {
                return this._CorrectionType;
            }
            set
            {
                this._CorrectionType = value;
            }
        }
    }

    public class RSTaxInvoiceCorrected : RSTaxInvoice
    {

        public new abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }

        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Next Corrected")]
        public override string TaxInvoiceNbr
        {
            get
            {
                return base.TaxInvoiceNbr;
            }

            set
            {
                base.TaxInvoiceNbr = value;
            }
        }


        public new abstract class correctedRefNbr : PX.Data.IBqlField
        {
        }

        [PXDBString(15, IsUnicode = true)]
        [PXUIField()]
        public override string CorrectedRefNbr
        {
            get
            {
                return base.CorrectedRefNbr;
            }

            set
            {
                base.CorrectedRefNbr = value;
            }
        }
    }

    [System.Serializable()]
    [PXCacheName("RSTaxInvoice")]
    [PXPrimaryGraph(typeof(TaxInvoiceEntry))]
    public class RSTaxInvoice : PX.Data.IBqlTable, IRSBranch
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Referrence Nbr.")]
        [AutoNumber(typeof(RSSetup.taxInvoiceNumberingID), typeof(RSTaxInvoice.createdDateTime))]
        [PXSelector(typeof(RSTaxInvoice.taxInvoiceNbr),
           typeof(RSTaxInvoice.taxInvoiceNbr),
           typeof(RSTaxInvoice.status),
           typeof(RSTaxInvoice.taxInvoiceID),
           typeof(RSTaxInvoice.customerName),
           Filterable = true)]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region PostPeriod
        public abstract class postPeriod : PX.Data.IBqlField
        {
        }
        protected string _PostPeriod;
        [FinPeriodID()]
        [PXDefault()]
        [PXUIField(DisplayName = "Post Period", Visibility = PXUIVisibility.Visible)]
        [PXSelector(typeof(Search<TaxPeriod.taxPeriodID, Where<TaxPeriod.vendorID,
                               Equal<Current<RSSetup.taxInvoiceVendorID>>>>), DirtyRead = true)]
        public virtual String PostPeriod
        {
            get
            {
                return this._PostPeriod;
            }
            set
            {
                this._PostPeriod = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(2)]
        [PXUIField(DisplayName = "Status")]
        [PXDefault(Statuses.Open)]
        [TaxInvoiceStatusAttribute]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected bool? _Hold;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Hold", Visibility = PXUIVisibility.Visible)]
        public virtual bool? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion
        #region CustomerID
        public abstract class customerID : PX.Data.IBqlField
        {
        }
        protected int? _CustomerID;
        [PXUIField(DisplayName = "Customer")]
        [CustomerActive(Visibility = PXUIVisibility.SelectorVisible, DescriptionField = typeof(Customer.acctName), Filterable = true, TabOrder = 2)]
        public virtual int? CustomerID
        {
            get
            {
                return this._CustomerID;
            }
            set
            {
                this._CustomerID = value;
            }
        }
        #endregion
        #region CustomerTaxRegistrationID
        public abstract class customerTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _CustomerTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Customer Tax Registration ID")]
        public virtual string CustomerTaxRegistrationID
        {
            get
            {
                return this._CustomerTaxRegistrationID;
            }
            set
            {
                this._CustomerTaxRegistrationID = value;
            }
        }
        #endregion
        #region CustomerName
        public abstract class customerName : PX.Data.IBqlField
        {
        }
        protected string _CustomerName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Customer Name")]
        public virtual string CustomerName
        {
            get
            {
                return this._CustomerName;
            }
            set
            {
                this._CustomerName = value;
            }
        }
        #endregion
        #region Note
        public abstract class note : PX.Data.IBqlField
        {
        }
        protected string _Note;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Note")]
        public virtual string Note
        {
            get
            {
                return this._Note;
            }
            set
            {
                this._Note = value;
            }
        }
        #endregion
        #region CreateDate
        public abstract class createDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreateDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Create Date")]
        public virtual DateTime? CreateDate
        {
            get
            {
                return this._CreateDate;
            }
            set
            {
                this._CreateDate = value;
            }
        }
        #endregion
        #region RegistrationDate
        public abstract class registrationDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _RegistrationDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Registration Date")]
        public virtual DateTime? RegistrationDate
        {
            get
            {
                return this._RegistrationDate;
            }
            set
            {
                this._RegistrationDate = value;
            }
        }
        #endregion
        #region ConfirmDate
        public abstract class confirmDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ConfirmDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Confirm Date")]
        public virtual DateTime? ConfirmDate
        {
            get
            {
                return this._ConfirmDate;
            }
            set
            {
                this._ConfirmDate = value;
            }
        }
        #endregion
        #region TaxInvoiceID
        public abstract class taxInvoiceID : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceID;
        [PXDBInt()]
        [PXUIField(DisplayName = "TaxInvoice ID")]
        public virtual int? TaxInvoiceID
        {
            get
            {
                return this._TaxInvoiceID;
            }
            set
            {
                this._TaxInvoiceID = value;
            }
        }
        #endregion
        #region TaxInvoiceNumber
        public abstract class taxInvoiceNumber : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNumber;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "TaxInvoice Number")]
        public virtual string TaxInvoiceNumber
        {
            get
            {
                return this._TaxInvoiceNumber;
            }
            set
            {
                this._TaxInvoiceNumber = value;
            }
        }
        #endregion
        #region DeclarationNumber
        public abstract class declarationNumber : PX.Data.IBqlField
        {
        }
        protected string _DeclarationNumber;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Declaration Nbr")]
        public virtual string DeclarationNumber
        {
            get
            {
                return this._DeclarationNumber;
            }
            set
            {
                this._DeclarationNumber = value;
            }
        }
        #endregion
        #region TaxInvoiceStatus
        public abstract class taxInvoiceStatus : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "Tax Invoice Status")]
        [PXIntList(
            new int[]{
                SentTaxInvoiceStatuses.Sent,
                SentTaxInvoiceStatuses.Confirmed,
                SentTaxInvoiceStatuses.Canceled,
                SentTaxInvoiceStatuses.CorrectedNew,
                SentTaxInvoiceStatuses.CorrectedSent,
                SentTaxInvoiceStatuses.CorrectedConfirmed,
                SentTaxInvoiceStatuses.Initial,
                SentTaxInvoiceStatuses.Replaced,
                SentTaxInvoiceStatuses.Rejected,
                SentTaxInvoiceStatuses.Deleted,
                SentTaxInvoiceStatuses.CanceledByGE,
                SentTaxInvoiceStatuses.ToBeCanceled

            },
            new string[]{
                "Sent",
                "Confirmed",
                "Canceled",
                "New (Corrected)",
                "Sent (Corrected)",
                "Confirmed (Corrected)",
                "Initial",
                "Replaced",
                "Rejected",
                "Deleted",
                "Canceled By GE",
                "To Be Canceled"
            })]
        public virtual int? TaxInvoiceStatus
        {
            get
            {
                return this._TaxInvoiceStatus;
            }
            set
            {
                this._TaxInvoiceStatus = value;
            }
        }
        #endregion
        #region InitialTaxInvoiceNbr
        public abstract class initialTaxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _InitialTaxInvoiceNbr;
        [PXDBString(15)]
        [PXUIField(DisplayName = "Initial TaxInvoice Nbr")]
        public virtual string InitialTaxInvoiceNbr
        {
            get
            {
                return this._InitialTaxInvoiceNbr;
            }
            set
            {
                this._InitialTaxInvoiceNbr = value;
            }
        }
        #endregion
        #region InitialTaxInvoiceID
        public abstract class initialTaxInvoiceID : PX.Data.IBqlField
        {
        }
        protected int? _InitialTaxInvoiceID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Initial TaxInvoice ID")]
        public virtual int? InitialTaxInvoiceID
        {
            get
            {
                return this._InitialTaxInvoiceID;
            }
            set
            {
                this._InitialTaxInvoiceID = value;
            }
        }
        #endregion
        #region CorrectionType
        public abstract class correctionType : PX.Data.IBqlField
        {
        }
        protected int? _CorrectionType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Correction Type")]
        [TaxInvoiceCorrectionTypeList()]
        public virtual int? CorrectionType
        {
            get
            {
                return this._CorrectionType;
            }
            set
            {
                this._CorrectionType = value;
            }
        }
        #endregion
        #region CorrectionDate
        public abstract class correctionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CorrectionDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Correction Date")]
        public virtual DateTime? CorrectionDate
        {
            get
            {
                return this._CorrectionDate;
            }
            set
            {
                this._CorrectionDate = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected int? _BranchID;
        [BranchExt()]
        [PXUIField(Required = false)]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual int? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region BranchTaxRegistrationID
        public abstract class branchTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _BranchTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Tax Registration ID")]
        public virtual string BranchTaxRegistrationID
        {
            get
            {
                return this._BranchTaxRegistrationID;
            }
            set
            {
                this._BranchTaxRegistrationID = value;
            }
        }
        #endregion
        #region BranchName
        public abstract class branchName : PX.Data.IBqlField
        {
        }
        protected string _BranchName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Name")]
        public virtual string BranchName
        {
            get
            {
                return this._BranchName;
            }
            set
            {
                this._BranchName = value;
            }
        }
        #endregion
        #region ItemCntr
        public abstract class itemCntr : PX.Data.IBqlField
        {
        }
        protected Int32? _ItemCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual Int32? ItemCntr
        {
            get
            {
                return this._ItemCntr;
            }
            set
            {
                this._ItemCntr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion        
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        //[PXNote(DescriptionField = typeof(RSTaxInvoice.taxInvoiceNbr))]
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region CorrectedRefNbr
        public abstract class correctedRefNbr : PX.Data.IBqlField
        {
        }
        protected string _CorrectedRefNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Prev. Corrected", Visibility = PXUIVisibility.Visible, Enabled = false)]
        public virtual string CorrectedRefNbr
        {
            get
            {
                return this._CorrectedRefNbr;
            }
            set
            {
                this._CorrectedRefNbr = value;
            }
        }
        #endregion
        #region PrepaymentTaxInvoice
        public abstract class prepaymentTaxInvoice : PX.Data.IBqlField
        {
        }
        protected bool? _PrepaymentTaxInvoice;
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Prepayment Tax Invoice", Visibility = PXUIVisibility.Visible)]
        public virtual bool? PrepaymentTaxInvoice
        {
            get
            {
                return this._PrepaymentTaxInvoice;
            }
            set
            {
                this._PrepaymentTaxInvoice = value;
            }
        }
        #endregion
        #region VATAmt
        public abstract class vATAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _VATAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0", PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "VAT", Enabled = false)]
        public virtual decimal? VATAmt
        {
            get
            {
                return this._VATAmt;
            }
            set
            {
                this._VATAmt = value;
            }
        }
        #endregion
        #region FullAmt
        public abstract class fullAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _FullAmt;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Amount", Enabled = false)]
        public virtual decimal? FullAmt
        {
            get
            {
                return this._FullAmt;
            }
            set
            {
                this._FullAmt = value;
            }
        }
        #endregion
        #region TotalAmt
        public abstract class totalAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _TotalAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Total Amount", Enabled = false)]
        public virtual decimal? TotalAmt
        {
            get
            {
                return this._TotalAmt;
            }
            set
            {
                this._TotalAmt = value;
            }
        }
        #endregion
        #region TotalExciseAmt
        public abstract class totalExciseAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _TotalExciseAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Excise", Enabled = false)]
        public virtual decimal? TotalExciseAmt
        {
            get
            {
                return this._TotalExciseAmt;
            }
            set
            {
                this._TotalExciseAmt = value;
            }
        }
        #endregion




        public bool CanPostTaxInvoice()
        {
            bool result = false;
            if (this.Status == Statuses.Open)
                result = true;
            switch (this.TaxInvoiceStatus)
            {
                case SentTaxInvoiceStatuses.CorrectedNew: result = true; break;
                default:
                    break;
            }
            return result;
        }

        public bool CanDeleteTaxInvoice()
        {
            bool result = false;
            switch (this.TaxInvoiceStatus)
            {
                case SentTaxInvoiceStatuses.Sent: result = true; break;
                case SentTaxInvoiceStatuses.CorrectedNew: result = true; break;
                case SentTaxInvoiceStatuses.CorrectedSent: result = true; break;
                case SentTaxInvoiceStatuses.Rejected: result = true; break;
                default:
                    break;
            }
            return result;
        }
        public bool CanCorrectTaxInvoice()
        {
            bool result = false;
            switch (this.TaxInvoiceStatus)
            {
                case SentTaxInvoiceStatuses.Confirmed: result = true; break;
                case SentTaxInvoiceStatuses.CorrectedConfirmed: result = true; break;
                default:
                    break;
            }
            return result;
        }
    }
}

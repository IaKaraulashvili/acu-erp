﻿using PX.Data;
using PX.Objects.AR;
using PX.Objects.GL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.DAC
{
    using System;
    using PX.Data;

    [System.Serializable()]
    public class RSTaxInvoiceInvoice : PX.Data.IBqlTable
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Referrence Nbr.", Visible = false)]
        [PXDBDefault(typeof(RSTaxInvoice.taxInvoiceNbr), DefaultForUpdate = false)]
        [PXParent(typeof(Select<RSTaxInvoice,
            Where<RSTaxInvoice.taxInvoiceNbr, Equal<Current<RSTaxInvoiceInvoice.taxInvoiceNbr>>>>))]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region taxInvoiceItemNbr
        public abstract class waybillItemNbr : PX.Data.IBqlField
        {
        }
        protected int? _taxInvoiceItemNbr;
        [PXDBInt(IsKey = true)]
        [PXLineNbr(typeof(RSTaxInvoice.itemCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? taxInvoiceItemNbr
        {
            get
            {
                return this._taxInvoiceItemNbr;
            }
            set
            {
                this._taxInvoiceItemNbr = value;
            }
        }

        #endregion
        #region invoiceDocType
        public abstract class invoiceDocType : PX.Data.IBqlField
        {
        }
        protected string _invoiceDocType;
        [PXDBString(3)]
        [PXUIField(DisplayName = "Doc Type", Visible = false)]
        [PXDefault(ARDocType.Invoice)]
        public virtual String InvoiceDocType
        {
            get
            {
                return this._invoiceDocType;
            }
            set
            {
                this._invoiceDocType = value;
            }
        }

        #endregion
        #region invoiceNbr
        public abstract class invoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _invoiceNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXDefault()]
        [PXUIField(DisplayName = "Reference Nbr.", Visibility = PXUIVisibility.SelectorVisible, TabOrder = 1)]
        [ARInvoiceType.RefNbr(typeof(Search2<ARInvoice.refNbr,
        InnerJoin<Customer, On<ARInvoice.customerID, Equal<Customer.bAccountID>>>,
        Where2<Where<ARInvoice.docType, Equal<Current<RSTaxInvoiceInvoice.invoiceDocType>>>, // SoLaRiS ~ ~
        And<Match<Customer, Current<AccessInfo.userName>>>>, OrderBy<Desc<ARInvoice.refNbr>>>), Filterable = true)]
        [ARInvoiceNbr()]
        public virtual String InvoiceNbr
        {
            get
            {
                return this._invoiceNbr;
            }
            set
            {
                this._invoiceNbr = value;
            }
        }

        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}

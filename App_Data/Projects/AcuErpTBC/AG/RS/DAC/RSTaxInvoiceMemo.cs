﻿﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.AR;
    using PX.Objects.SO;

    [System.SerializableAttribute()]
	public class RSTaxInvoiceMemo : PX.Data.IBqlTable
	{

        #region RSTaxInvoiceMemoID
        public abstract class rSTaxInvoiceMemoID : PX.Data.IBqlField
        {
        }
        protected int? _RSTaxInvoiceMemoID;
        [PXDBIdentity(IsKey = true)]
        [PXUIField(Enabled = false)]
        public virtual int? RSTaxInvoiceMemoID
        {
            get
            {
                return this._RSTaxInvoiceMemoID;
            }
            set
            {
                this._RSTaxInvoiceMemoID = value;
            }
        }
        #endregion

        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
		{
		}
		protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Tax Invoice Nbr", Visible = false)]
        [PXDBDefault(typeof(RSTaxInvoice.taxInvoiceNbr), DefaultForUpdate = true, DefaultForInsert = true)]
        [PXParent(typeof(Select<RSTaxInvoice, Where<RSTaxInvoice.taxInvoiceNbr, Equal<Current<RSTaxInvoiceMemo.taxInvoiceNbr>>>>))]
        public virtual string TaxInvoiceNbr
        {
			get
			{
				return this._TaxInvoiceNbr;
			}
			set
			{
				this._TaxInvoiceNbr = value;
			}
		}
		#endregion

		#region ArInvoiceNbr
		public abstract class arInvoiceNbr : PX.Data.IBqlField
		{
		}
		protected string _ArInvoiceNbr;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Invoice And Memo Nbr")]
        [PXSelector(typeof(Search<ARInvoice.refNbr>))]
        public virtual string ArInvoiceNbr
		{
			get
			{
				return this._ArInvoiceNbr;
			}
			set
			{
				this._ArInvoiceNbr = value;
			}
		}
		#endregion

		#region ShipmentNbr
		public abstract class shipmentNbr : PX.Data.IBqlField
		{
		}
		protected string _ShipmentNbr;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Shipment Nbr")]
        [PXSelector(typeof(Search<SOShipment.shipmentNbr>))]
        public virtual string ShipmentNbr
		{
			get
			{
				return this._ShipmentNbr;
			}
			set
			{
				this._ShipmentNbr = value;
			}
		}
        #endregion


        #region ShipmentDate
        public abstract class shipmentDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ShipmentDate;
        [PXDBDate(UseTimeZone = false)]
        [PXUIField(DisplayName = "Shipment Date" ,Enabled = false)]
        public virtual DateTime? ShipmentDate
        {
            get
            {
                return this._ShipmentDate;
            }
            set
            {
                this._ShipmentDate = value;
            }
        }
        #endregion

        #region InvoiceAndMemoDate
        public abstract class invoiceAndMemoDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _InvoiceAndMemoDate;
        [PXDBDate(UseTimeZone = false)]
        [PXUIField(DisplayName = "Invoice And Memo Date" ,Enabled = false)]
        public virtual DateTime? InvoiceAndMemoDate
        {
            get
            {
                return this._InvoiceAndMemoDate;
            }
            set
            {
                this._InvoiceAndMemoDate = value;
            }
        }
        #endregion
    }
}

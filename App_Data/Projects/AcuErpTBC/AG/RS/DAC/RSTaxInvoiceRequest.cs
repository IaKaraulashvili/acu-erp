﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;
using AG.RS;
using PX.Objects.GL;
using PX.Objects.TX;
using AG.RS.Descriptor;
using PX.Objects.CS;
using AG.RS.Shared;

namespace AG.RS.DAC
{
    [System.Serializable()]
    public class RSTaxInvoiceRequest : PX.Data.IBqlTable, IRSBranch
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField { }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Referrence Nbr.")]
        [AutoNumber(typeof(RSSetup.taxInvoiceRequestNumberingID), typeof(RSTaxInvoiceRequest.createdDateTime))]
        [PXSelector(typeof(Search<RSTaxInvoiceRequest.taxInvoiceNbr>),
           typeof(RSTaxInvoiceRequest.taxInvoiceNbr),
           typeof(RSTaxInvoiceRequest.status),
           typeof(RSTaxInvoiceRequest.taxInvoiceNumber),
           typeof(RSTaxInvoiceRequest.vendorName),
           Filterable = true)]
        public virtual string TaxInvoiceNbr
        {
            get { return this._TaxInvoiceNbr; }
            set { this._TaxInvoiceNbr = value; }
        }
        #endregion
        #region PostPeriod
        public abstract class postPeriod : PX.Data.IBqlField { }
        protected String _PostPeriod;
        [FinPeriodID()]
        [PXDefault()]
        [PXUIField(DisplayName = "Post Period", Visibility = PXUIVisibility.Visible)]
        [PXSelector(typeof(Search<TaxPeriod.taxPeriodID, Where<TaxPeriod.vendorID,
                                Equal<Current<RSSetup.taxInvoiceVendorID>>>>), DirtyRead = true)]
        public virtual String PostPeriod
        {
            get { return this._PostPeriod; }
            set { this._PostPeriod = value; }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField { }
        protected string _Status;
        [PXDBString(1, IsFixed = true, IsUnicode = true)]
        [PXDefault(TaxInvoiceStatus.Hold)]
        [PXUIField(DisplayName = "Status")]
        [PXStringList(
            new string[]{
            TaxInvoiceStatus.Open,
            TaxInvoiceStatus.Hold,
            TaxInvoiceStatus.Sent,
            TaxInvoiceStatus.Confirmed,
            TaxInvoiceStatus.Deleted},
            new string[]{
            TaxInvoiceStatus.UI.Open,
            TaxInvoiceStatus.UI.Hold,
            TaxInvoiceStatus.UI.Sent,
            TaxInvoiceStatus.UI.Confirmed,
            TaxInvoiceStatus.UI.Deleted})]
        public virtual string Status
        {
            get { return this._Status; }
            set { this._Status = value; }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField { }
        protected Boolean? _Hold;
        [PXDBBool()]
        [PXDefault(true)]
        [PXUIField(DisplayName = "Hold", Visibility = PXUIVisibility.Visible)]
        public virtual Boolean? Hold
        {
            get { return this._Hold; }
            set { this._Hold = value; }
        }
        #endregion
        #region Vendor
        public abstract class vendorID : PX.Data.IBqlField { }
        protected int? _VendorID;
        [PXUIField(DisplayName = "Vendor", Required = false)]
        [PX.Objects.AP.VendorActive(Visibility = PXUIVisibility.SelectorVisible, DescriptionField = typeof(PX.Objects.AP.Vendor.acctName), CacheGlobal = true, Filterable = true, Required = false)]
        public virtual int? VendorID
        {
            get { return this._VendorID; }
            set { this._VendorID = value; }
        }
        #endregion
        #region VendorTaxRegistrationID
        public abstract class vendorTaxRegistrationID : PX.Data.IBqlField { }
        protected string _VendorTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Vendor Tax Registration ID")]
        public virtual string VendorTaxRegistrationID
        {
            get { return this._VendorTaxRegistrationID; }
            set { this._VendorTaxRegistrationID = value; }
        }
        #endregion
        #region VendorName
        public abstract class vendorName : PX.Data.IBqlField { }
        protected string _VendorName;
        [PXDBString(100, IsUnicode = true)]
        [PXUIField(DisplayName = "Vendor Name")]
        public virtual string VendorName
        {
            get { return this._VendorName; }
            set { this._VendorName = value; }
        }
        #endregion
        #region SenderNote
        public abstract class senderNote : PX.Data.IBqlField { }
        protected string _SenderNote;
        [PXDBString(300, IsUnicode = true)]
        [PXUIField(DisplayName = "Sender Note")]
        public virtual string SenderNote
        {
            get { return this._SenderNote; }
            set { this._SenderNote = value; }
        }
        #endregion
        #region CreateDate
        public abstract class createDate : PX.Data.IBqlField { }
        protected DateTime? _CreateDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Create Date")]
        public virtual DateTime? CreateDate
        {
            get { return this._CreateDate; }
            set { this._CreateDate = value; }
        }
        #endregion
        #region RequestDate
        public abstract class requsestDate : PX.Data.IBqlField { }
        protected DateTime? _RequestDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Request Date")]
        public virtual DateTime? RequestDate
        {
            get { return this._RequestDate; }
            set { this._RequestDate = value; }
        }
        #endregion
        #region ViewDate
        public abstract class viewDate : PX.Data.IBqlField { }
        protected DateTime? _ViewDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "View Date")]
        public virtual DateTime? ViewDate
        {
            get { return this._ViewDate; }
            set { this._ViewDate = value; }
        }
        #endregion
        #region ConfirmDate
        public abstract class confirmDate : PX.Data.IBqlField { }
        protected DateTime? _ConfirmDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Confirm Date")]
        public virtual DateTime? ConfirmDate
        {
            get { return this._ConfirmDate; }
            set { this._ConfirmDate = value; }
        }
        #endregion
        #region TaxInvoiceNumber
        public abstract class taxInvoiceNumber : PX.Data.IBqlField { }
        protected string _TaxInvoiceNumber;
        [PXDBString(30, IsUnicode = true)]
        [PXUIField(DisplayName = "Tax Invoice Nbr")]
        public virtual string TaxInvoiceNumber
        {
            get { return this._TaxInvoiceNumber; }
            set { this._TaxInvoiceNumber = value; }
        }
        #endregion
        #region Branch
        public abstract class branchID : PX.Data.IBqlField { }
        protected int? _BranchID;
        [BranchExt()]
        [PXUIField(DisplayName = "Branch")]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual int? BranchID
        {
            get { return this._BranchID; }
            set { this._BranchID = value; }
        }
        #endregion
        #region BranchTaxRegistrationID
        public abstract class branchTaxRegistrationID : PX.Data.IBqlField { }
        protected string _BranchTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Tax Registration ID")]
        public virtual string BranchTaxRegistrationID
        {
            get { return this._BranchTaxRegistrationID; }
            set { this._BranchTaxRegistrationID = value; }
        }
        #endregion
        #region BranchName
        public abstract class branchName : PX.Data.IBqlField { }
        protected string _BranchName;
        [PXDBString(100, IsUnicode = true)]
        [PXUIField(DisplayName = "Branch Name")]
        public virtual string BranchName
        {
            get { return this._BranchName; }
            set { this._BranchName = value; }
        }
        #endregion
        #region ItemCntr
        public abstract class itemCntr : PX.Data.IBqlField
        {
        }
        protected Int32? _ItemCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual Int32? ItemCntr
        {
            get
            {
                return this._ItemCntr;
            }
            set
            {
                this._ItemCntr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote(DescriptionField = typeof(RSWaybill.waybillNbr))]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region TaxInvoiceStatus
        public class TaxInvoiceStatus
        {
            public const string Open = "O";
            public const string Hold = "H";
            public const string Sent = "S";
            public const string Confirmed = "C";
            public const string Deleted = "D";

            public class UI
            {
                public const string Open = "Open";
                public const string Hold = "On Hold";
                public const string Sent = "Sent";
                public const string Confirmed = "Confirmed";
                public const string Deleted = "Deleted";
            }
        } 
        #endregion
    }
}
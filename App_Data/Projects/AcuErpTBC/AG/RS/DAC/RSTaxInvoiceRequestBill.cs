﻿namespace AR.RS.DAC
{
    using System;
    using PX.Data;
    using AG.RS.DAC;
    using PX.Objects.AP;

    [System.Serializable()]
    public class RSTaxInvoiceRequestBill : PX.Data.IBqlTable
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true)]
        [PXDBDefault(typeof(RSTaxInvoiceRequest.taxInvoiceNbr), DefaultForUpdate = false)]
        [PXParent(typeof(Select<RSTaxInvoiceRequest, Where<RSTaxInvoiceRequest.taxInvoiceNbr, Equal<Current<RSTaxInvoiceRequestBill.taxInvoiceNbr>>>>))]
        [PXUIField(DisplayName = "Tax Invoice Nbr", Visible = false)]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
        #region TaxInvoiceItemNbr
        public abstract class taxInvoiceItemNbr : PX.Data.IBqlField
        {
        }
        protected int? _TaxInvoiceItemNbr;
        [PXDBInt(IsKey = true)]
        [PXDefault()]
        [PXLineNbr(typeof(RSTaxInvoiceRequest.itemCntr))]
        [PXUIField(DisplayName = "Tax Invoice Item Nbr", Visible = false)]
        public virtual int? TaxInvoiceItemNbr
        {
            get
            {
                return this._TaxInvoiceItemNbr;
            }
            set
            {
                this._TaxInvoiceItemNbr = value;
            }
        }
        #endregion
        #region InvoiceDocType
        public abstract class invoiceDocType : PX.Data.IBqlField
        {
        }
        protected string _InvoiceDocType;
        [PXDBString(3, IsFixed = true)]
        //[PXDBDefault(typeof(PX.Objects.AP.APDocType.invoice), DefaultForInsert = true)]
        [PXDefault(PX.Objects.AP.APDocType.Invoice)]
        [PXUIField(DisplayName = "Invoice DocType")]
        public virtual string InvoiceDocType
        {
            get
            {
                return this._InvoiceDocType;
            }
            set
            {
                this._InvoiceDocType = value;
            }
        }
        #endregion
        #region APInvoiceNbr
        public abstract class aPInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _APInvoiceNbr;
        [PXDBString(15, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "APInvoice Nbr")]
        [PX.Objects.AP.APInvoiceType.RefNbr(typeof(Search2<APInvoice.refNbr,
            InnerJoin<Vendor, On<Vendor.bAccountID, Equal<Current<RSTaxInvoiceRequest.vendorID>>>>,
            Where<APInvoice.docType, Equal<Optional<RSTaxInvoiceRequestBill.invoiceDocType>>,
            And2<Where<APInvoice.origModule, NotEqual<PX.Objects.GL.BatchModule.moduleTX>, Or<APInvoice.released, Equal<True>>>,
            And<Match<Vendor, Current<AccessInfo.userName>>>>>, OrderBy<Desc<APInvoice.refNbr>>
            >)
            , Filterable = true)]
        public virtual string APInvoiceNbr
        {
            get
            {
                return this._APInvoiceNbr;
            }
            set
            {
                this._APInvoiceNbr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}

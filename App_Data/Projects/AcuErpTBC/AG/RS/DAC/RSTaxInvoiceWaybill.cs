﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;

    [System.Serializable()]
    public class RSTaxInvoiceWaybill : PX.Data.IBqlTable
    {
        #region TaxInvoiceNbr
        public abstract class taxInvoiceNbr : PX.Data.IBqlField
        {
        }
        protected string _TaxInvoiceNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true)]
        [PXUIField(DisplayName = "Tax Invoice Nbr", Visible = false)]
        [PXDBDefault(typeof(RSTaxInvoice.taxInvoiceNbr), DefaultForUpdate = true, DefaultForInsert = true)]
        [PXParent(typeof(Select<RSTaxInvoice, Where<RSTaxInvoice.taxInvoiceNbr, Equal<Current<RSTaxInvoiceWaybill.taxInvoiceNbr>>>>))]
        public virtual string TaxInvoiceNbr
        {
            get
            {
                return this._TaxInvoiceNbr;
            }
            set
            {
                this._TaxInvoiceNbr = value;
            }
        }
        #endregion
//        #region WaybillType
//        public abstract class waybillType : PX.Data.IBqlField
//        {
//        }
//        protected int? _WaybillType;
//        [PXDBInt()]
//      //  [PXDefault()]
//        [PXDefault(WaybillTypes.Internal)]
//        [PXUIField(DisplayName = "Waybill Type" ,Enabled = false)]
//        [PXIntList(
//new int[]{
//            WaybillTypes.Internal,
//            WaybillTypes.Transporation,
//            WaybillTypes.WithoutTransporation,
//            WaybillTypes.Distribution,
//            WaybillTypes.Rtrn
//        },
//new string[]{
//            "Internal",
//            "Trans.",
//            "Without Trans.",
//            "Distribution",
//            "Return"
//        })]
//        public virtual int? WaybillType
//        {
//            get
//            {
//                return this._WaybillType;
//            }
//            set
//            {
//                this._WaybillType = value;
//            }
//        }
//        #endregion
        #region WaybillNbr
        public abstract class waybillNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true)]
        [PXUIField(DisplayName = "Waybill Nbr")]
        [PXSelector(typeof(Search<RSWaybill.waybillNbr>))]
        public virtual string WaybillNbr
        {
            get
            {
                return this._WaybillNbr;
            }
            set
            {
                this._WaybillNbr = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}


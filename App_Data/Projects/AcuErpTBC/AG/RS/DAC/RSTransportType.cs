﻿﻿namespace AG.RS.DAC
{
	using System;
	using PX.Data;
	
	[System.Serializable()]
	public class RSTransportType : PX.Data.IBqlTable
	{
		#region ID
		public abstract class iD : PX.Data.IBqlField
		{
		}
		protected int? _ID;
		[PXDBInt(IsKey = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "ID")]
		public virtual int? ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				this._ID = value;
			}
		}
		#endregion
		#region Name
		public abstract class name : PX.Data.IBqlField
		{
		}
		protected string _Name;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Name")]
		public virtual string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				this._Name = value;
			}
		}
		#endregion
	}
}

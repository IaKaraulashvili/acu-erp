﻿﻿namespace AG.RS.DAC
{
	using System;
	using PX.Data;
    using PX.Objects.IN;
	
	[System.Serializable()]
	public class RSUOMMapper : PX.Data.IBqlTable
	{
        #region FromUnit
        public abstract class unit : PX.Data.IBqlField
        {
        }
        protected string _Unit;
        [INUnit(DisplayName = "System UOM")]
        [PXDefault()]
        public virtual string Unit
        {
            get
            {
                return this._Unit; 
            }
            set
            {
                this._Unit = value;
            }
        }
        #endregion
        #region RSUOMID
        public abstract class rSUOMID : PX.Data.IBqlField
        {
        }
        protected int? _RSUOMID;
        [PXDBInt()]
        [PXDefault()]
        [PXUIField(DisplayName = "RS UOM")]
        [PXSelector(typeof(Search<RSUnit.iD>),
                    typeof(RSUnit.iD),
                    typeof(RSUnit.name),
                    SubstituteKey = typeof(RSUnit.name))]
        public virtual int? RSUOMID
        {
            get
            {
                return this._RSUOMID;
            }
            set
            {
                this._RSUOMID = value;
            }
        }
        #endregion
        #region UomID
        public abstract class uomID : PX.Data.IBqlField
        {
        }
        protected int? _UomID;
        [PXDBIdentity(IsKey = true)]
        [PXUIField(Enabled = false)]
        public virtual int? UomID
        {
            get
            {
                return this._UomID;
            }
            set
            {
                this._UomID = value;
            }
        }
        #endregion
    }
}

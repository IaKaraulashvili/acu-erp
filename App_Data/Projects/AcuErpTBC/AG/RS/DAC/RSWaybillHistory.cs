﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using AG.RS.Descriptor;
    using PX.Objects.AR;
    using PX.Objects.GL;
    using AG.Common;
    using PX.SM;
    using Extensions.SM.Shared;
    using Shared;
    using PX.Objects.CS;
    using Utils;

    [System.Serializable()]
    [PXPrimaryGraph(typeof(RSWaybillHistory))]
    public class RSWaybillHistory : PX.Data.IBqlTable, IRSWaybill
    {
        #region WaybillHistoryID
        public abstract class waybillHistoryID : PX.Data.IBqlField
        {
        }
        protected int? _WaybillHistoryID;
        [PXDBIdentity]
        public virtual int? WaybillHistoryID
        {
            get
            {
                return this._WaybillHistoryID;
            }
            set
            {
                this._WaybillHistoryID = value;
            }
        }
        #endregion
        #region ClassID
        public abstract class classID : PX.Data.IBqlField
        {
        }
        protected string _ClassID;
        [PXDBString(1, IsKey = true)]
        [PXUIField(DisplayName = "Class")]
        [WaybillClassList]
        public virtual string ClassID
        {
            get
            {
                return this._ClassID;
            }
            set
            {
                this._ClassID = value;
            }
        }
        #endregion  
        #region WaybillNbr
        public abstract class waybillNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillNbr;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Waybill Reference Nbr")]

        public virtual string WaybillNbr
        {
            get
            {
                return this._WaybillNbr;
            }
            set
            {
                this._WaybillNbr = value;
            }
        }
        #endregion
        #region WaybillHistoryNbr
        public abstract class waybillHistoryNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillHistoryNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Reference Nbr")]
        [AutoNumber(typeof(RSSetup.waybillHistoryNumberingID), typeof(RSWaybillHistory.createdDateTime))]
        [WaybillHistorySelector()]
        public virtual string WaybillHistoryNbr
        {
            get
            {
                return this._WaybillHistoryNbr;
            }
            set
            {
                this._WaybillHistoryNbr = value;
            }
        }
        #endregion
        #region WaybillType
        public abstract class waybillType : PX.Data.IBqlField
        {
        }
        protected int? _WaybillType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Waybill Type")]
        [PXDefault()]
        [WaybillTypeListAttribute]
        public virtual int? WaybillType
        {
            get
            {
                return this._WaybillType;
            }
            set
            {
                this._WaybillType = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDBString(1, IsUnicode = false, IsFixed = true)]
        [PXUIField(DisplayName = "Status")]
        [PXDefault(Shared.WaybillStatus.Hold)]
        [WaybillStatusList]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Hold
        public abstract class hold : PX.Data.IBqlField
        {
        }
        protected Boolean? _Hold;
        [PXDBBool()]
        [PXUIField(DisplayName = "Hold", Visibility = PXUIVisibility.Visible)]
        [PXDefault(true)]
        public virtual Boolean? Hold
        {
            get
            {
                return this._Hold;
            }
            set
            {
                this._Hold = value;
            }
        }
        #endregion
        #region WaybillID
        public abstract class waybillID : PX.Data.IBqlField
        {
        }
        protected string _WaybillID;
        [PXDBString(20)]
        [PXUIField(DisplayName = "Waybill ID")]
        public virtual string WaybillID
        {
            get
            {
                return this._WaybillID;
            }
            set
            {
                this._WaybillID = value;
            }
        }
        #endregion
        #region ParentWaybillID
        public abstract class parentWaybillID : PX.Data.IBqlField
        {
        }
        protected string _ParentWaybillID;
        [PXDBString(20)]
        [PXUIField(DisplayName = "Parent Waybill")]
        public virtual string ParentWaybillID
        {
            get
            {
                return this._ParentWaybillID;
            }
            set
            {
                this._ParentWaybillID = value;
            }
        }
        #endregion
        #region IsCorrected
        public abstract class isCorrected : PX.Data.IBqlField
        {
        }
        protected bool? _IsCorrected;
        [PXDBBool]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Is Corrected")]
        public virtual bool? IsCorrected
        {
            get
            {
                return this._IsCorrected;
            }
            set
            {
                this._IsCorrected = value;
            }
        }
        #endregion
        #region WaybillNumber
        public abstract class waybillNumber : PX.Data.IBqlField
        {
        }
        protected string _WaybillNumber;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Waybill Number")]
        public virtual string WaybillNumber
        {
            get
            {
                return this._WaybillNumber;
            }
            set
            {
                this._WaybillNumber = value;
            }
        }
        #endregion
        #region WaybillStatus
        public abstract class waybillStatus : PX.Data.IBqlField
        {
        }
        protected int? _WaybillStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "Waybill Status")]
        //[PXDefault(WaybillStatuses.Saved)]
        [WaybillExtStatusList]
        public virtual int? WaybillStatus
        {
            get
            {
                return this._WaybillStatus;
            }
            set
            {
                this._WaybillStatus = value;
            }
        }
        #endregion
        #region WaybillState
        public abstract class waybillState : PX.Data.IBqlField
        {
        }
        protected int? _WaybillState;
        [PXDBInt()]
        [PXUIField(DisplayName = "Waybill State")]
        //[PXDefault(WaybillStates.Receivable)]
        [WaybillStateList]
        public virtual int? WaybillState
        {
            get
            {
                return this._WaybillState;
            }
            set
            {
                this._WaybillState = value;
            }
        }
        #endregion
        #region WaybillCreateDate
        public abstract class waybillCreateDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _WaybillCreateDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Create Date")]
        public virtual DateTime? WaybillCreateDate
        {
            get
            {
                return this._WaybillCreateDate;
            }
            set
            {
                this._WaybillCreateDate = value;
            }
        }
        #endregion
        #region WaybillActivationDate
        public abstract class waybillActivationDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _WaybillActivationDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Activation Date")]
        public virtual DateTime? WaybillActivationDate
        {
            get
            {
                return this._WaybillActivationDate;
            }
            set
            {
                this._WaybillActivationDate = value;
            }
        }
        #endregion
        #region WaybillCloseDate
        public abstract class waybillCloseDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _WaybillCloseDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Close Date")]
        public virtual DateTime? WaybillCloseDate
        {
            get
            {
                return this._WaybillCloseDate;
            }
            set
            {
                this._WaybillCloseDate = value;
            }
        }
        #endregion
        #region WaybillCorrectionDate
        public abstract class waybillCorrectionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _WaybillCorrectionDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Correction Date")]
        public virtual DateTime? WaybillCorrectionDate
        {
            get
            {
                return this._WaybillCorrectionDate;
            }
            set
            {
                this._WaybillCorrectionDate = value;
            }
        }
        #endregion
        #region WaybillCancelDate
        public abstract class waybillCancelDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _WaybillCancelDate;
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Cancel Date")]
        public virtual DateTime? WaybillCancelDate
        {
            get
            {
                return this._WaybillCancelDate;
            }
            set
            {
                this._WaybillCancelDate = value;
            }
        }
        #endregion
        #region WaybillCategory
        public abstract class waybillCategory : PX.Data.IBqlField
        {
        }
        protected int? _WaybillCategory;
        [PXDBInt()]
        [PXUIField(DisplayName = "Waybill Category")]
        [PXDefault(Shared.WaybillCategory.Normal)]
     [WaybillCategoryListAttribute]
        public virtual int? WaybillCategory
        {
            get
            {
                return this._WaybillCategory;
            }
            set
            {
                this._WaybillCategory = value;
            }
        }
        #endregion
        #region CustomerID
        public abstract class customerID : PX.Data.IBqlField
        {
        }
        protected Int32? _CustomerID;
        [PXUIField(Required = false)]
        [CustomerActive(Visibility = PXUIVisibility.SelectorVisible, DescriptionField = typeof(Customer.acctName), Filterable = true, TabOrder = 2)]
        public virtual Int32? CustomerID
        {
            get
            {
                return this._CustomerID;
            }
            set
            {
                this._CustomerID = value;
            }
        }
        #endregion
        #region RecipientName
        public abstract class recipientName : PX.Data.IBqlField
        {
        }
        protected string _RecipientName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Recipient Name")]
        public virtual string RecipientName
        {
            get
            {
                return this._RecipientName;
            }
            set
            {
                this._RecipientName = value;
            }
        }
        #endregion  
        #region RecipientTaxRegistrationID
        public abstract class recipientTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _RecipientTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Recipient Tax Registration ID")]
        public virtual string RecipientTaxRegistrationID
        {
            get
            {
                return this._RecipientTaxRegistrationID;
            }
            set
            {
                this._RecipientTaxRegistrationID = value;
            }
        }
        #endregion
        #region RecipientIsForeignCitizen
        public abstract class recipientIsForeignCitizen : PX.Data.IBqlField
        {
        }
        protected bool? _RecipientIsForeignCitizen;
        [PXDBBool()]
        [PXUIField(DisplayName = "Recipient Foreign  Citizen")]
        public virtual bool? RecipientIsForeignCitizen
        {
            get
            {
                return this._RecipientIsForeignCitizen;
            }
            set
            {
                this._RecipientIsForeignCitizen = value;
            }
        }
        #endregion
        #region RecipientInfo
        public abstract class recipientInfo : PX.Data.IBqlField
        {
        }
        protected string _RecipientInfo;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Recipient Info")]
        public virtual string RecipientInfo
        {
            get
            {
                return this._RecipientInfo;
            }
            set
            {
                this._RecipientInfo = value;
            }
        }
        #endregion
        #region RecipientStatus
        public abstract class recipientStatus : PX.Data.IBqlField
        {
        }
        protected int? _RecipientStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "Recipient Status")]
        [PXDefault(RecepientStatus.Without)]
        [RecepientStatusList]
        public virtual int? RecipientStatus
        {
            get
            {
                return this._RecipientStatus;
            }
            set
            {
                this._RecipientStatus = value;
            }
        }
        #endregion
        #region SourceAddress
        public abstract class sourceAddress : PX.Data.IBqlField
        {
        }
        protected string _SourceAddress;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Source Address")]
        public virtual string SourceAddress
        {
            get
            {
                return this._SourceAddress;
            }
            set
            {
                this._SourceAddress = value;
            }
        }
        #endregion
        #region DestinationAddress
        public abstract class destinationAddress : PX.Data.IBqlField
        {
        }
        protected string _DestinationAddress;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Destination Address")]
        public virtual string DestinationAddress
        {
            get
            {
                return this._DestinationAddress;
            }
            set
            {
                this._DestinationAddress = value;
            }
        }
        #endregion
        #region SupplierDestinationAddress
        public abstract class supplierDestinationAddress : PX.Data.IBqlField
        {
        }
        protected string _SupplierDestinationAddress;
        [PXString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Supplier Destination Address")]
        public virtual string SupplierDestinationAddress
        {
            get
            {
                return this._SupplierDestinationAddress;
            }
            set
            {
                this._SupplierDestinationAddress = value;
            }
        }
        #endregion
        #region RecipientDestinationAddress
        public abstract class recipientDestinationAddress : PX.Data.IBqlField
        {
        }
        protected string _RecipientDestinationAddress;
        [PXString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Recipient Destination Address")]
        public virtual string RecipientDestinationAddress
        {
            get
            {
                return this._RecipientDestinationAddress;
            }
            set
            {
                this._RecipientDestinationAddress = value;
            }
        }
        #endregion
        #region ShippingCost
        public abstract class shippingCost : PX.Data.IBqlField
        {
        }
        protected decimal? _ShippingCost;
        [PXDBDecimal(6)]
        [PXUIField(DisplayName = "Shipping Cost")]
        public virtual decimal? ShippingCost
        {
            get
            {
                return this._ShippingCost;
            }
            set
            {
                this._ShippingCost = value;
            }
        }
        #endregion
        #region SupplierTaxRegistrationID
        public abstract class supplierTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _SupplierTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Supplier Tax Registration ID")]
        public virtual string SupplierTaxRegistrationID
        {
            get
            {
                return this._SupplierTaxRegistrationID;
            }
            set
            {
                this._SupplierTaxRegistrationID = value;
            }
        }
        #endregion
        #region SupplierName
        public abstract class supplierName : PX.Data.IBqlField
        {
        }
        protected string _SupplierName;
        [PXDBString(255, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Supplier Name")]
        public virtual string SupplierName
        {
            get
            {
                return this._SupplierName;
            }
            set
            {
                this._SupplierName = value;
            }
        }
        #endregion
        #region BranchID
        public abstract class branchID : PX.Data.IBqlField
        {
        }
        protected Int32? _BranchID;
        [Branch()]
        [PXUIField(Required = false)]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual Int32? BranchID
        {
            get
            {
                return this._BranchID;
            }
            set
            {
                this._BranchID = value;
            }
        }
        #endregion
        #region SupplierInfo
        public abstract class supplierInfo : PX.Data.IBqlField
        {
        }
        protected string _SupplierInfo;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Supplier Info")]
        public virtual string SupplierInfo
        {
            get
            {
                return this._SupplierInfo;
            }
            set
            {
                this._SupplierInfo = value;
            }
        }
        #endregion
        #region DriverIsForeignCitizen
        public abstract class driverIsForeignCitizen : PX.Data.IBqlField
        {
        }
        protected bool? _DriverIsForeignCitizen;
        [PXDBBool()]
        [PXUIField(DisplayName = "Driver Is Foreign Citizen")]
        public virtual bool? DriverIsForeignCitizen
        {
            get
            {
                return this._DriverIsForeignCitizen;
            }
            set
            {
                this._DriverIsForeignCitizen = value;
            }
        }
        #endregion
        #region DriverName
        public abstract class driverName : PX.Data.IBqlField
        {
        }
        protected string _DriverName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Driver Name")]
        public virtual string DriverName
        {
            get
            {
                return this._DriverName;
            }
            set
            {
                this._DriverName = value;
            }
        }
        #endregion
        #region DriverUID
        public abstract class driverUID : PX.Data.IBqlField
        {
        }
        protected string _DriverUID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Driver UID")]
        public virtual string DriverUID
        {
            get
            {
                return this._DriverUID;
            }
            set
            {
                this._DriverUID = value;
            }
        }
        #endregion
        #region CarrierTaxRegistrationID
        public abstract class carrierTaxRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _CarrierTaxRegistrationID;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Carrier Tax Registration ID")]
        public virtual string CarrierTaxRegistrationID
        {
            get
            {
                return this._CarrierTaxRegistrationID;
            }
            set
            {
                this._CarrierTaxRegistrationID = value;
            }
        }
        #endregion
        #region CarrierInfo
        public abstract class carrierInfo : PX.Data.IBqlField
        {
        }
        protected string _CarrierInfo;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Carrier Name")]
        public virtual string CarrierInfo
        {
            get
            {
                return this._CarrierInfo;
            }
            set
            {
                this._CarrierInfo = value;
            }
        }
        #endregion
        #region Trailer
        public abstract class trailer : PX.Data.IBqlField
        {
        }
        protected string _Trailer;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Trailer")]
        public virtual string Trailer
        {
            get
            {
                return this._Trailer;
            }
            set
            {
                this._Trailer = value;
            }
        }
        #endregion
        #region CarRegistrationNumber
        public abstract class carRegistrationNumber : PX.Data.IBqlField
        {
        }
        protected string _CarRegistrationNumber;
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Car Registration Number")]
        public virtual string CarRegistrationNumber
        {
            get
            {
                return this._CarRegistrationNumber;
            }
            set
            {
                this._CarRegistrationNumber = value;
            }
        }
        #endregion
        #region TransportationStartDateTime
        public abstract class transportationStartDateTime : PX.Data.IBqlField
        {
        }
        [PXDateAndTime()]
        // [PXDefault(typeof(AccessInfo.businessDate))]
        [PXUIField(DisplayName = "Transportation Start Date")]
        public DateTime? TransportationStartDateTime
        {
            get
            {
                if (!_TransStartDate.HasValue)
                {
                    return null;
                }

                return DateTimeHelper.ConstructDateTime(_TransStartDate.Value, _TransStartTime ?? 0);
            }
        }
        #endregion
        #region TransStartDate
        public abstract class transStartDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _TransStartDate;
        [PXDBDate(UseTimeZone = false)]
        [PXUIField(DisplayName = "Transportation Start Date")]
        public virtual DateTime? TransStartDate
        {
            get
            {
                return this._TransStartDate;
            }
            set
            {
                this._TransStartDate = value;
            }
        }
        #endregion
        #region TransStartTime
        public abstract class transStartTime : PX.Data.IBqlField
        {
        }
        protected int? _TransStartTime;
        [PXDBTimeSpan(DisplayMask = "t", InputMask = "t")]
        [PXUIField(DisplayName = "Transportation Start Time")]
        public virtual int? TransStartTime
        {
            get
            {
                return this._TransStartTime;
            }
            set
            {
                this._TransStartTime = value;
            }
        }
        #endregion
        #region ShippingCostPayer
        public abstract class shippingCostPayer : PX.Data.IBqlField
        {
        }
        protected int? _ShippingCostPayer;
        [PXDBInt()]
        [PXUIField(DisplayName = "Shipping Cost Payer")]
        [ShippingCostPayerList]
        public virtual int? ShippingCostPayer
        {
            get
            {
                return this._ShippingCostPayer;
            }
            set
            {
                this._ShippingCostPayer = value;
            }
        }
        #endregion
        #region OtherTransportationTypeDescription
        public abstract class otherTransportationTypeDescription : PX.Data.IBqlField
        {
        }
        protected string _OtherTransportationTypeDescription;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Other Transportation Type Description")]
        public virtual string OtherTransportationTypeDescription
        {
            get
            {
                return this._OtherTransportationTypeDescription;
            }
            set
            {
                this._OtherTransportationTypeDescription = value;
            }
        }
        #endregion
        #region TransportationType
        public abstract class transportationType : PX.Data.IBqlField
        {
        }
        protected int? _TransportationType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Transportation Type")]
        //[PXDefault(TransportationTypes.Vehicle)]
        [WaybillTransportationTypeListAttribute]
        public virtual int? TransportationType
        {
            get
            {
                return this._TransportationType;
            }
            set
            {
                this._TransportationType = value;
            }
        }
        #endregion     
        #region DeliveryDate
        public abstract class deliveryDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _DeliveryDate;
        [PXDBDate(UseTimeZone = false)]
        [PXUIField(DisplayName = "Delivery Date")]
        public virtual DateTime? DeliveryDate
        {
            get
            {
                return this._DeliveryDate;
            }
            set
            {
                this._DeliveryDate = value;
            }
        }
        #endregion
        #region DeliveryTime
        public abstract class deliveryTime : PX.Data.IBqlField
        {
        }
        protected int? _DeliveryTime;
        [PXDBTimeSpan(DisplayMask = "t", InputMask = "t")]
        [PXUIField(DisplayName = "Delivered On")]
        public virtual int? DeliveryTime
        {
            get
            {
                return this._DeliveryTime;
            }
            set
            {
                this._DeliveryTime = value;
            }
        }
        #endregion
        #region TotalAmount
        public abstract class totalAmount : PX.Data.IBqlField
        {
        }
        protected decimal? _TotalAmount;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Total Amount")]
        public virtual decimal? TotalAmount
        {
            get
            {
                return this._TotalAmount;
            }
            set
            {
                this._TotalAmount = value;
            }
        }
        #endregion
        #region TotalAmountInWords
        public abstract class totalAmountInWords : PX.Data.IBqlField
        {
        }
        protected string _TotalAmountInWords;
        [PXString]
        public virtual string TotalAmountInWords
        {
            get
            {
                return AG.RS.Helpers.PXGraphHelpers.MoneyToWords(this._TotalAmount);
            }
            set
            {
                this._TotalAmountInWords = AG.RS.Helpers.PXGraphHelpers.MoneyToWords(this._TotalAmount);
            }
        }
        #endregion
        #region Comment
        public abstract class comment : PX.Data.IBqlField
        {
        }
        protected string _Comment;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Comment")]
        public virtual string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
            }
        }
        #endregion
        #region SourceNbr
        public abstract class sourceNbr : PX.Data.IBqlField
        {
        }
        protected String _SourceNbr;
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Source Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual String SourceNbr
        {
            get
            {
                return this._SourceNbr;
            }
            set
            {
                this._SourceNbr = value;
            }
        }
        #endregion

        #region SourceType
        public abstract class sourceType : PX.Data.IBqlField
        {
        }
        protected int? _SourceType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Source Type")]
        [WaybillSourceTypeList]
        public virtual int? SourceType
        {
            get
            {
                return this._SourceType;
            }
            set
            {
                this._SourceType = value;
            }
        }
        #endregion
        #region WaybillCreatedByID
        public abstract class waybillCreatedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _WaybillCreatedByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Created By", Enabled = false)]
        [UserSelector]
        public virtual Guid? WaybillCreatedByID
        {
            get
            {
                return this._WaybillCreatedByID;
            }
            set
            {
                this._WaybillCreatedByID = value;
            }
        }
        #endregion
        #region PostedByID
        public abstract class postedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _PostedByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Posted By", Enabled = false)]
        [UserSelector]
        public virtual Guid? PostedByID
        {
            get
            {
                return this._PostedByID;
            }
            set
            {
                this._PostedByID = value;
            }
        }
        #endregion
        #region ClosedByID
        public abstract class closedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _ClosedByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Closed By", Enabled = false)]
        [UserSelector]
        public virtual Guid? ClosedByID
        {
            get
            {
                return this._ClosedByID;
            }
            set
            {
                this._ClosedByID = value;
            }
        }
        #endregion
        #region CanceledByID
        public abstract class canceledByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CanceledByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Canceled By", Enabled = false)]
        [UserSelector]
        public virtual Guid? CanceledByID
        {
            get
            {
                return this._CanceledByID;
            }
            set
            {
                this._CanceledByID = value;
            }
        }
        #endregion
        #region CorrectedByID
        public abstract class correctedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CorrectedByID;
        [PXDBGuid]
        [PXUIField(DisplayName = "Corrected By", Enabled = false)]
        [UserSelector]
        public virtual Guid? CorrectedByID
        {
            get
            {
                return this._CorrectedByID;
            }
            set
            {
                this._CorrectedByID = value;
            }
        }
        #endregion
        #region ItemCntr
        public abstract class itemCntr : PX.Data.IBqlField
        {
        }
        protected Int32? _ItemCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual Int32? ItemCntr
        {
            get
            {
                return this._ItemCntr;
            }
            set
            {
                this._ItemCntr = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote(DescriptionField = typeof(RSWaybillHistory.waybillNbr))]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region Methods

        public RSWaybillHistory CopyFrom<T>(T waybill)
        {
            DACMapper.Map(waybill, this);

            return this;
        }

        #endregion
    }
}

﻿namespace AG.RS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using AG.Common;
    using Shared;
    using EA.Shared;
    using Extensions.FA.Shared;

    [System.Serializable()]
    public class RSWaybillItemHistory : PX.Data.IBqlTable
    {
        #region WaybillItemHistoryID
        public abstract class waybillItemHistoryID : PX.Data.IBqlField
        {
        }
        protected int? _WaybillItemHistoryID;
        [PXDBIdentity]
        public virtual int? WaybillItemHistoryID
        {
            get
            {
                return this._WaybillItemHistoryID;
            }
            set
            {
                this._WaybillItemHistoryID = value;
            }
        }
        #endregion
        #region WaybillHistoryID
        public abstract class waybillHistoryID : PX.Data.IBqlField
        {
        }
        protected int? _WaybillHistoryID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RSWaybillHistory.waybillHistoryID), DefaultForUpdate = false)]
        [PXParent(typeof(Select<RSWaybillHistory, Where<RSWaybillHistory.waybillHistoryID, Equal<Current<RSWaybillItemHistory.waybillHistoryID>>>>))]
        public virtual int? WaybillHistoryID
        {
            get
            {
                return this._WaybillHistoryID;
            }
            set
            {
                this._WaybillHistoryID = value;
            }
        }
        #endregion
        #region WaybillItemNbr
        public abstract class waybillItemNbr : PX.Data.IBqlField
        {
        }
        protected int? _WaybillItemNbr;
        [PXDBInt(IsKey = true)]
        [PXLineNbr(typeof(RSWaybillHistory.itemCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? WaybillItemNbr
        {
            get
            {
                return this._WaybillItemNbr;
            }
            set
            {
                this._WaybillItemNbr = value;
            }
        }
        #endregion
        #region WaybillItemType
        public abstract class waybillItemType : PX.Data.IBqlField
        {
        }
        protected int? _WaybillItemType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Type")]
        [WaybillItemTypeList]
        public virtual int? WaybillItemType
        {
            get
            {
                return this._WaybillItemType;
            }
            set
            {
                this._WaybillItemType = value;
            }
        }
        #endregion

        #region ItemRowID
        public abstract class itemRowID : PX.Data.IBqlField
        {
        }
        protected string _ItemRowID;
        [PXDBString()]
        [PXDefault("")]
        [PXUIField(DisplayName = "RS Row ID")]
        public virtual string ItemRowID
        {
            get
            {
                return this._ItemRowID;
            }
            set
            {
                this._ItemRowID = value;
            }
        }
        #endregion
        #region InventoryID
        public abstract class inventoryID : PX.Data.IBqlField
        {
        }
        protected int? _InventoryID;
        [Inventory]
        public virtual int? InventoryID
        {
            get
            {
                return this._InventoryID;
            }
            set
            {
                this._InventoryID = value;
            }
        }
        #endregion

        #region FixedAssetID
        public abstract class fixedAssetID : PX.Data.IBqlField
        {
        }
        protected int? _FixedAssetID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Fixed Asset ID")]
        [FixedAssetSelector]
        public virtual int? FixedAssetID
        {
            get
            {
                return this._FixedAssetID;
            }
            set
            {
                this._FixedAssetID = value;
            }
        }
        #endregion
        #region EAAssetID
        public abstract class eAAssetID : PX.Data.IBqlField
        {
        }
        protected int? _EAAssetID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Enterprise Asset ID")]
        [EAAssetSelector]
        public virtual int? EAAssetID
        {
            get
            {
                return this._EAAssetID;
            }
            set
            {
                this._EAAssetID = value;
            }
        }
        #endregion
        #region ItemName
        public abstract class itemName : PX.Data.IBqlField
        {
        }
        protected string _ItemName;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Item Name")]
        public virtual string ItemName
        {
            get
            {
                return this._ItemName;
            }
            set
            {
                this._ItemName = value;
            }
        }
        #endregion
        #region ItemRowStatus
        public abstract class itemRowStatus : PX.Data.IBqlField
        {
        }
        protected int? _ItemRowStatus;
        [PXDBInt()]
        [PXUIField(DisplayName = "Status")]
        [PXDefault(RSWaybillItemStatuses.Active)]
        [PXIntList(
           new int[]{
                RSWaybillItemStatuses.Active,
                RSWaybillItemStatuses.Deleted
           },
           new string[]{
                   "Active",
                   "Deleted"
           }
               )]
        public virtual int? ItemRowStatus
        {
            get
            {
                return this._ItemRowStatus;
            }
            set
            {
                this._ItemRowStatus = value;
            }
        }
        #endregion
        #region ItemBarCode
        public abstract class itemCode : PX.Data.IBqlField
        {
        }
        protected string _ItemCode;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Item Code")]
        public virtual string ItemCode
        {
            get
            {
                return this._ItemCode;
            }
            set
            {
                this._ItemCode = value;
            }
        }
        #endregion
        #region ExtUOM
        public abstract class extUOM : PX.Data.IBqlField
        {
        }
        protected int? _ExtUOM;
        [PXDBInt()]
        [PXUIField(DisplayName = "UOM")]
        [PXIntList(
            new int[]{
                1
           },
            new string[]{
                "Other"
           }

            )]
        public virtual int? ExtUOM
        {
            get
            {
                return this._ExtUOM;
            }
            set
            {
                this._ExtUOM = value;
            }
        }
        #endregion
        #region OtherUOMDescription
        public abstract class otherUOMDescription : PX.Data.IBqlField
        {
        }
        protected string _OtherUOMDescription;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Other UOM Description")]
        public virtual string OtherUOMDescription
        {
            get
            {
                return this._OtherUOMDescription;
            }
            set
            {
                this._OtherUOMDescription = value;
            }
        }
        #endregion
        #region ItemQty
        public abstract class itemQty : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemQty;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Item Qty")]
        public virtual decimal? ItemQty
        {
            get
            {
                return this._ItemQty;
            }
            set
            {
                this._ItemQty = value;
            }
        }
        #endregion
        #region ItemExtraQty
        public abstract class itemExtraQty : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemExtraQty;
        [PXDBDecimal(6)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Item Extra Qty")]
        public virtual decimal? ItemExtraQty
        {
            get
            {
                return this._ItemExtraQty;
            }
            set
            {
                this._ItemExtraQty = value;
            }
        }
        #endregion
        #region UnitPrice
        public abstract class unitPrice : PX.Data.IBqlField
        {
        }
        protected decimal? _UnitPrice;
        [PXDBDecimal(6)]
        [PXUIField(DisplayName = "Unit Price")]
        public virtual decimal? UnitPrice
        {
            get
            {
                return this._UnitPrice;
            }
            set
            {
                this._UnitPrice = value;
            }
        }
        #endregion
        #region ItemAmt
        public abstract class itemAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _ItemAmt;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Amount", Enabled = false)]
        [PXFormula(
               typeof(Mult<unitPrice, Add<itemQty, itemExtraQty>>),
               typeof(SumCalc<RSWaybillHistory.totalAmount>))]
        public virtual decimal? ItemAmt
        {
            get
            {
                return this._ItemAmt;
            }
            set
            {
                this._ItemAmt = value;
            }
        }
        #endregion
        #region ExciseID
        public abstract class exciseID : PX.Data.IBqlField
        {
        }
        protected int? _ExciseID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Excise")]
        public virtual int? ExciseID
        {
            get
            {
                return this._ExciseID;
            }
            set
            {
                this._ExciseID = value;
            }
        }
        #endregion
        #region TaxType
        public abstract class taxType : PX.Data.IBqlField
        {
        }
        protected int? _TaxType;
        [PXDBInt()]
        [PXUIField(DisplayName = "Tax Type")]
        [PXDefault(GETaxTypes.Normal)]
        [PXIntList(
            new int[]{
                GETaxTypes.Normal,
                GETaxTypes.Nullable,
                GETaxTypes.TaxFree
           },
            new string[]{
                   "Normal",
                   "Nullable",
                   "TaxFree"
           }
                )]
        public virtual int? TaxType
        {
            get
            {
                return this._TaxType;
            }
            set
            {
                this._TaxType = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion

        #region Methods

        public RSWaybillItemHistory CopyFrom(RSWaybillItem waybillItem)
        {
            DACMapper.Map<RSWaybillItem, RSWaybillItemHistory>(waybillItem, this);

            return this;
        }

        public global::RS.Services.Domain.WAYBILLGOODS_LISTGOODS GetExternalWaybillItem()
        {
            return new global::RS.Services.Domain.WAYBILLGOODS_LISTGOODS
            {
                A_ID = Convert.ToString(this.ExciseID),
                AMOUNT = Convert.ToString(this.ItemAmt),
                BAR_CODE = this.ItemCode,
                ID = this.ItemRowID,
                PRICE = Convert.ToString(this.UnitPrice),
                QUANTITY = Convert.ToString(this.ItemQty),
                STATUS = this.ItemRowStatus.ToString(),
                UNIT_ID = Convert.ToString(this.ExtUOM),
                UNIT_TXT = this.OtherUOMDescription,
                VAT_TYPE = Convert.ToString(this.TaxType),
                W_NAME = this.ItemName
            };
        }

        #endregion
    }
}
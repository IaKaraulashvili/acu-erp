﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PX.Data;

namespace AG.RS.DAC
{
    [System.Serializable()]
    public class RSWaybillSource : PX.Data.IBqlTable
    {
        #region WaybillNbr
        public abstract class waybillNbr : PX.Data.IBqlField
        {
        }
        protected string _WaybillNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Waybill Nbr")]
        [PXDBDefault(typeof(RSWaybill.waybillNbr), DefaultForUpdate = false)]
        [PXParent(typeof(Select<RSWaybill, Where<RSWaybill.waybillNbr, Equal<Current<RSWaybillSource.waybillNbr>>>>))]
        public virtual string WaybillNbr
        {
            get
            {
                
                return this._WaybillNbr;
            }
            set
            {
                this._WaybillNbr = value;
            }
        }
        #endregion
        #region SourceNbr
        public abstract class sourceNbr : PX.Data.IBqlField
        {
        }
        protected String _SourceNbr;
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXDefault()]
        [PXUIField(DisplayName = "Document Nbr.", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual String SourceNbr
        {
            get
            {
                return this._SourceNbr;
            }
            set
            {
                this._SourceNbr = value;
            }
        }
        #endregion
        #region SourceType
        public abstract class sourceType : PX.Data.IBqlField
        {
        }
        protected int? _SourceType;
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Document Type")]
        [PXIntList(
            new int[]{
            SourceTypes.Shipment,
            SourceTypes.Transfer
        },
           new string[]{
            "Shipment",
            "Transfer"
        })]
        public virtual int? SourceType
        {
            get
            {
                return this._SourceType;
            }
            set
            {
                this._SourceType = value;
            }
        }
        #endregion

        #region Helper Classes
        public static class SourceTypes
        {
            public const int Shipment = 0;
            public class shipment : Constant<Int32>
            {
                public shipment()
                    : base(Shipment)
                {
                }
            }
            public const int Transfer = 1;
            public class transfer : Constant<Int32>
            {
                public transfer()
                    : base(Transfer)
                {
                }
            }
        }
        #endregion
    }
}

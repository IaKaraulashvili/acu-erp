﻿using System;
using PX.Data;
using PX.Common;
using System.Collections.Generic;

namespace AG.RS.Descriptor
{
    [PXLocalizable(Messages.Prefix)]
    public static class Messages
    {
        public const string Prefix = "";

        #region Custom Actions

        public const string TestWBConnection = "Test Waybill Connection";
        public const string TestTIConnection = "Test TaxInvoice Connection";
        public const string TestConnection = "Test Connection";

        #endregion

        public const string RSWaybillSetup = "Way Bill Setup";
        public const string Regular = "Normal";
        public const string ConnectionToWaybillFailed = "Connection to Waybill failed.";
        public const string ConnectionToTaxInvoiceFailed = "Connection to Tax Invoice failed.";
        public const string ConnectionToWaybillSuccessfull = " Connection to Waybill was successful.";
        public const string ConnectionToTaxInvoiceSuccessfull = " Connection to Tax Invoice was successful.";
        public const string RSIsNotConfigured = "RS Setup is not configured.";
        public const string CannotProcessHoldStatus = "Cannot Process Waybill, It Has A Hold Status.";
        public const string CantPostWithoutTrans = "Cannot Process Waybill, It Has Without Transportation Type";
        public const string WaybillAlreadyPosted = "Waybill Is Already Posted";
        public const string CanNotConfirm = "Can not confirm";
        public const string CanNotReject = "Can not reject";
        public const string WaybillAlreadyCanceled = "Waybill Is Already Canceled";
        public const string WaybillAlreadyActivated = "Waybill Is Already Activated";
        public const string WaybillIsNotActivated = "Waybill Is Not Activated";
        public const string WaybillWithoutTransporation = "Cannot Close Already Posted Without Transportation Type Waybills";
        public const string WaybillDriverUIDLength = "Driver UID's length must be 11!";
        public const string WaybillRecipientTaxIDLength = "Recipient Tax Registration ID's length must be 11!";
        public const string GetDriverName = "Get Name";
        public const string ViewPOReceipt = "View PO Receipt";
        public const string AddInvoiceAndMemo = "Add Invoice And Memo";
        public const string AddBillAndAdjustment = "Add Bill And Adjustment";
        public const string Add = "Add";
        public const string Choose = "Choose";
        public const string ReceivedWaybillDoesNotExist = "Received waybill doesn't exist!";
        public const string ReceivedTaxInvoicFullAmtDisplayName = "Prepayment Amount";
        public const string ReceivedTaxInvoicFullAmtDefaultDisplayName = "Full Amount";
        public const string VendorDoesNotExist = "Vendor doesn't exist!";
    }
}

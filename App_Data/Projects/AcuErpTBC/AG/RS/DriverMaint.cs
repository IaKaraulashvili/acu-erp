using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using System.Linq;
namespace AG.RS
{
    public class DriverMaint : AGGraph<DriverMaint>
    {
        public PXSave<RSDriver> Save;
        public PXCancel<RSDriver> Cancel;
        public PXSelect<RSDriver> Drivers;

        protected virtual void RSDriver_DriverTaxID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSDriver)e.Row;
            foreach (RSDriver line in Drivers.Select())
            {
                if (row.DriverID == line.DriverID)
                    continue;
                if (row.DriverTaxID == line.DriverTaxID)
                    throw new PXException("Driver Tax ID already exists!");
            }
        }
    }
}
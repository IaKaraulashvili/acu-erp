using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.IN;
using global::RS.Services.Domain;
using AG.RS.Descriptor;
using System.Linq;
using PX.Objects.AR;
using PX.Objects.GL;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using AG.RS.Helpers;

namespace AG.RS
{
    public class ExciseMaint : AGGraph<ExciseMaint, RSExcise>
    {
        public PXSelect<RSExcise> Excise;
    }
}
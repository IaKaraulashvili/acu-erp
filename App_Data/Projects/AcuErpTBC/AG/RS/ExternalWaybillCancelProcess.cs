using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using AG.RS.DAC;
using AG.RS.Shared;

namespace AG.RS
{
    public class ExternalWaybillCancel : AGGraph<ExternalWaybillCancel>
    {
        [PXFilterable]
        public PXProcessing<RSWaybill, Where<RSWaybill.waybillStatus, Equal<WaybillExtStatus.activated>>> Waybills;
        //public PXSetup<RSSetup> Setup;
        //public PXSelect<RSErrorCode> ErrorCodes;

        public ExternalWaybillCancel()
        {
            Waybills.SetSelected<RSWaybill.selected>();
            Waybills.SetProcessDelegate(Cancel);
            Waybills.SetProcessCaption("Cancel");
            Waybills.SetProcessAllCaption("Cancel All");
        }

        public static void Cancel(List<RSWaybill> waybills)
        {
            WaybillEntry rg = PXGraph.CreateInstance<WaybillEntry>();
            for (int i = 0; i < waybills.Count; i++)
            {
                if (!waybills[i].Selected ?? false)
                    continue;

                RSWaybill wb = waybills[i];

                try
                {
                    rg.Clear();
                    rg.Waybills.Current = wb;
                    rg.CancelWB();
                    PXProcessing<RSWaybill>.SetInfo(i, ActionsMessages.RecordProcessed);
                }
                catch (Exception e)
                {
                    PXProcessing<RSWaybill>.SetError(i, e is PXOuterException ? e.Message + "\r\n" + String.Join("\r\n", ((PXOuterException)e).InnerMessages) : e.Message);
                }

            }
        }

        //public string Cancel(string waybillID)
        //{
        //    using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.Account, Setup.Current.Licence, Setup.Current.Url, Setup.Current.Timeout))
        //    {
        //        int result = context.CancelWaybills(int.Parse(waybillID));
        //        string message = AG.RS.Descriptor.GEOperations.GenarateStatus(result, waybillID, GEOperations.Operations.Cancel);
        //        if (result < 0)
        //        {
        //            throw new PXException(message);
        //        }
        //        else
        //        {
        //            PXDatabase.Update<RSWaybill>(new PXDataFieldAssign("WaybillStatus", WaybillStatuses.Finished));
        //            //PXDatabase.Update<RSWaybill>(new PXDataFieldAssign("Status", WBStatuses.Posted));
        //        }
        //        return message;
        //    }
        //}


        public PXAction<RSWaybill> viewWaybill;
        [PXUIField(DisplayName = "View Waybill", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
        public virtual IEnumerable ViewWaybill(PXAdapter adapter)
        {
            if (this.Waybills.Current != null)
            {
                WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
                graph.Waybills.Current = graph.Waybills.Search<RSWaybill.waybillNbr>(this.Waybills.Current.WaybillNbr);
                if (graph.Waybills.Current != null)
                {
                    throw new PXRedirectRequiredException(graph, true, "ViewWaybill") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
                }
            }
            return adapter.Get();
        }        
    }
}
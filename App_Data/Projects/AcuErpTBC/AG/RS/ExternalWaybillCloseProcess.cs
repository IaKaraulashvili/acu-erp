using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using AG.RS.DAC;
using AG.RS.Shared;

namespace AG.RS
{
    public class ExternalWaybillCloseProcess : AGGraph<ExternalWaybillCloseProcess>
    {
        [PXFilterable]
        public PXProcessing<RSWaybill, Where<RSWaybill.waybillStatus, Equal<WaybillExtStatus.activated>>> Waybills;
        //public PXSetup<RSSetup> Setup;

        public ExternalWaybillCloseProcess()
        {
            //var current = Setup.Current;

            Waybills.SetSelected<RSWaybill.selected>();
            Waybills.SetProcessDelegate(Close);
            Waybills.SetProcessCaption("Close");
            Waybills.SetProcessAllCaption("Close All");
        }

        public static void Close(List<RSWaybill> waybills)
        {
            WaybillEntry rg = PXGraph.CreateInstance<WaybillEntry>();
            for (int i = 0; i < waybills.Count; i++)
            {
                RSWaybill wb = waybills[i];

                if (!waybills[i].Selected ?? false)
                    continue;

                try
                {
                    rg.Clear();
                    rg.Waybills.Current = waybills[i];
                    rg.CloseWB();
                    PXProcessing<RSWaybill>.SetInfo(i, ActionsMessages.RecordProcessed);
                }
                catch (Exception e)
                {
                    PXProcessing<RSWaybill>.SetError(i, e is PXOuterException ? e.Message + "\r\n" + String.Join("\r\n", ((PXOuterException)e).InnerMessages) : e.Message);
                }
            }
        }


        public PXAction<RSWaybill> viewWaybill;
        [PXUIField(DisplayName = "View Waybill", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
        public virtual IEnumerable ViewWaybill(PXAdapter adapter)
        {
            if (this.Waybills.Current != null)
            {
                WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
                graph.Waybills.Current = graph.Waybills.Search<RSWaybill.waybillNbr>(this.Waybills.Current.WaybillNbr);
                if (graph.Waybills.Current != null)
                {
                    throw new PXRedirectRequiredException(graph, true, "ViewWaybill") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
                }
            }
            return adapter.Get();
        }      
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using global::RS.Services.Domain;

namespace AG.RS
{
    //public class ExternalWaybillDeleteProcess : PXGraph<ExternalWaybillDeleteProcess>
    //{
    //    //[PXFilterable]
    //    //public PXProcessing<RSWaybill> Waybills;
    //    public PXSetup<RSSetup> Setup;

    //    [PXFilterable]
    //    public PXProcessing<RSWaybill, Where<RSWaybill.waybillStatus, Equal<WaybillStatuses.finished>>> Waybills;

    //    public ExternalWaybillDeleteProcess()
    //    {
    //        var current = Setup.Current;

    //        Waybills.SetSelected<RSWaybill.selected>();
    //        Waybills.SetProcessDelegate(Delete);
    //        Waybills.SetProcessCaption("Delete");
    //        Waybills.SetProcessAllCaption("Delete All");
    //    }

    //    //public static void Delete(List<RSWaybill> waybills)
    //    //{
    //    //    WaybillEntry rg = PXGraph.CreateInstance<WaybillEntry>();
    //    //    for (int i = 0; i < waybills.Count; i++)
    //    //    {
    //    //        if (!waybills[i].Selected ?? false)
    //    //            continue;

    //    //        try
    //    //        {
    //    //            rg.Clear();
    //    //            rg.Waybills.Current = waybills[i];
    //    //            rg.Delete();
    //    //            PXProcessing<RSWaybill>.SetInfo(i,ActionsMessages.RecordProcessed);
    //    //        }
    //    //        catch (Exception e)
    //    //        {
    //    //            PXProcessing<RSWaybill>.SetError(i, e is PXOuterException ? e.Message + "\r\n" + String.Join("\r\n", ((PXOuterException)e).InnerMessages) : e.Message);
    //    //        }

    //    //    }
    //    //}


    //    public PXAction<RSWaybill> ViewWaybill;
    //    [PXUIField(DisplayName = "View Waybill", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
    //    [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
    //    public virtual IEnumerable viewWaybill(PXAdapter adapter)
    //    {
    //        if (this.Waybills.Current != null)
    //        {
    //            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
    //            graph.Waybills.Current = graph.Waybills.Search<RSWaybill.waybillNbr>(this.Waybills.Current.WaybillNbr);
    //            if (graph.Waybills.Current != null)
    //            {
    //                throw new PXRedirectRequiredException(graph, true, "ViewWaybill") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
    //            }
    //        }
    //        return adapter.Get();
    //    }

    //}
}
using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using AG.RS.DAC;
using AG.RS.Shared;


namespace AG.RS
{
    public class ExternalWaybillPostProcess : AGGraph<ExternalWaybillPostProcess>
    {
        [PXFilterable]
        public PXProcessing<RSWaybill,
            Where2<
                Where<RSWaybill.waybillActivationDate, IsNull, And<RSWaybill.waybillCreateDate, IsNull, And<RSWaybill.waybillStatus, Equal<WaybillExtStatus.saved>>>>,
                  Or<RSWaybill.status, Equal<WaybillStatus.open>>>> Waybills;


        public ExternalWaybillPostProcess()
        {
            //var a = Setup.Current;
            //var b = ErrorCodes.Current;
            //var c = TransportTypes.Current;
            var d = Waybills.Current;

            Waybills.SetSelected<RSWaybill.selected>();
            Waybills.SetProcessDelegate(Release);
            Waybills.SetProcessCaption("Post");
            Waybills.SetProcessAllCaption("Post All");
        }

        #region Button
        public PXAction<RSWaybill> viewWaybill;
        [PXUIField(DisplayName = "View Waybill", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
        public virtual IEnumerable ViewWaybill(PXAdapter adapter)
        {
            if (this.Waybills.Current != null)
            {
                WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
                graph.Waybills.Current = PXSelect<RSWaybill, Where<RSWaybill.waybillNbr, Equal<Required<RSWaybill.waybillNbr>>>>.Select(graph, this.Waybills.Current.WaybillNbr);
                if (graph.Waybills.Current != null)
                {
                    throw new PXRedirectRequiredException(graph, true, "ViewWaybill") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
                }
            }
            return adapter.Get();
        }
        #endregion

        public static void Release(List<RSWaybill> waybills)
        {
            WaybillEntry rg = PXGraph.CreateInstance<WaybillEntry>();
            for (int i = 0; i < waybills.Count; i++)
            {
                RSWaybill wb = waybills[i];

                if (!wb.Selected ?? false)
                    continue;

                try
                {
                    rg.Clear();
                    rg.Waybills.Current = waybills[i];
                    rg.PostWB();
                    if (waybills[i].WaybillType == WaybillType.WithoutTransporation)
                    {
                        rg.CloseWB();
                    }
                    PXProcessing<RSWaybill>.SetInfo(i, ActionsMessages.RecordProcessed);
                }
                catch (Exception e)
                {
                    PXProcessing<RSWaybill>.SetError(i, e is PXOuterException ? e.Message + "\r\n" + String.Join("\r\n", ((PXOuterException)e).InnerMessages) : e.Message);
                }

            }
        }

    }
}
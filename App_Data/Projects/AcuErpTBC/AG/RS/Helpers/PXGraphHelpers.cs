﻿using PX.Data;
using AG.RS.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using AG.RS;
using PX.Objects.GL;
using PX.Objects.CR;
using PX.Objects.AP;
using AG.RS.Shared;
using AG.EA;
using AG.EA.DAC;
using PX.Objects.IN;

namespace AG.RS.Helpers
{
    public static class PXGraphHelpers
    {
        public static void SetWaybillStatus(IRSWaybill row)
        {
            if (row.Hold == true)
            {
                row.Status = WaybillStatus.Hold;
            }
            else if (row.Status == WaybillStatus.Hold && row.Hold == false)
            {
                row.Status = WaybillStatus.Open;
            }
        }

        public static void SetWaybillState(IRSWaybill row, string state)
        {
            switch (state)
            {
                case "0":
                    row.WaybillState = WaybillStates.Receivable;
                    break;
                case "1":
                    row.WaybillState = WaybillStates.Received;
                    break;
                case "-1":
                    row.WaybillState = WaybillStates.Rejected;
                    break;
                default:
                    break;
            }
        }

        public static void SetNote(PXGraph graph, IRSWaybill row)
        {
            var noteCache = graph.Caches[typeof(Note)];
            var note = (Note)noteCache.Insert();

            note.NoteText = string.Empty;
            note.ExternalKey = row.WaybillID;
            note.GraphType = graph.GetType().FullName;
            note.EntityType = typeof(RSWaybill).FullName;

            row.NoteID = note.NoteID;
        }

        public static void SetBranchInfo(WaybillEntry graph, RSWaybill row)
        {
            if (row != null)
            {
                var branch = PXSelect<Branch, Where<Branch.branchID, Equal<Required<Branch.branchID>>>>.Select(graph, row.BranchID).FirstTableItems;
                if (branch != null && branch.Count() > 0)
                {
                    var baccount = PXSelect<BAccount2, Where<BAccount2.bAccountID, Equal<Required<BAccount2.bAccountID>>>>
                                    .Select(graph, branch.First().BAccountID).FirstTableItems;
                    if (baccount != null && baccount.Count() > 0)
                    {
                        row.SupplierTaxRegistrationID = baccount.First().TaxRegistrationID;

                        var contact = PXSelect<Contact, Where<Contact.bAccountID, Equal<Required<BAccount.bAccountID>>, And<Contact.contactID, Equal<Required<BAccount.defContactID>>>>>.Select(graph, baccount.First().BAccountID, baccount.First().DefContactID).FirstTableItems;

                        if (contact != null && contact.Count() > 0)
                        {
                            row.SupplierName = contact.First().FullName;
                        }

                        var address = PXSelect<Address, Where<Address.bAccountID, Equal<Required<Address.bAccountID>>, And<Address.addressID, Equal<Required<Address.addressID>>>>>.Select(graph, baccount.First().BAccountID, baccount.First().DefAddressID).FirstTableItems;
                        if (address != null && baccount.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(address.First().AddressLine1))
                            {
                                graph.Waybills.Cache.SetValueExt<RSWaybill.sourceAddress>(row, address.First().AddressLine1);
                            }
                        }
                    }
                }
            }
        }

        public static void SetBranch(PXGraph graph, IRSBranch row)
        {
            var branch = PXSelect<Branch, Where<Branch.branchID, Equal<Required<Branch.branchID>>>>.Select(graph, row.BranchID).FirstTableItems;
            if (branch != null && branch.Count() > 0)
            {
                var baccount = PXSelect<BAccount2, Where<BAccount2.bAccountID, Equal<Required<BAccount2.bAccountID>>>>
                                    .Select(graph, branch.First().BAccountID).FirstTableItems;
                if (baccount != null && baccount.Count() > 0)
                {
                    row.BranchTaxRegistrationID = baccount.First().TaxRegistrationID;

                    var contact = PXSelect<PX.Objects.CR.Contact, Where<PX.Objects.CR.Contact.bAccountID, Equal<Required<PX.Objects.CR.BAccount.bAccountID>>, And<PX.Objects.CR.Contact.contactID, Equal<Required<PX.Objects.CR.BAccount.defContactID>>>>>.Select(graph, baccount.First().BAccountID, baccount.First().DefContactID).FirstTableItems;

                    if (contact != null && contact.Count() > 0)
                    {
                        row.BranchName = contact.First().FullName;
                    }
                }
            }
        }

        public static string MoneyToWords(decimal? money)
        {
            if (!money.HasValue)
                return "";


            string[] parts = string.Format("{0:0.00}", Math.Abs(money.Value)).Split(new char[] { '.', ',' });

            long dollars = Convert.ToInt64(parts[0]);
            int cents = Convert.ToInt32(parts[1]);


            string[] ones ={
                  "ნული",
                  "ერთი",
                  "ორი",
                  "სამი",
                  "ოთხი",
                  "ხუთი",
                  "ექვსი",
                  "შვიდი",
                  "რვა",
                  "ცხრა",
                  "ათი",
                  "თერთმეტი",
                  "თორმეტი",
                  "ცამეტი",
                  "თოთხმეტი",
                  "თხუტმეტი",
                  "თექსვმეტი",
                  "ჩვიდმეტი",
                  "თვრამეტი",
                  "ცხრამეტი"
                };
            string[] twenties ={
                  "",
                  "ოცი",
                  "ორმოცი",
                  "სამოცი",
                  "ოთხმოცი"
                };
            string hund = "ასი";

            string[] thou ={
                  "",
                  "ათასი",
                  "მილიონი",
                  "მილიარდი",
                  "ტრილიონი"
                };

            string gel = "ლარი";
            string cent = "თეთრი";



            int dig1, level, threeDigits, twoDigits, twentyPart;

            string s = dollars.ToString();
            string words = string.Empty;


            if (money < 0)
                words += "მინუს ";


            while (s.Length > 0)
            {
                threeDigits = int.Parse((s.Length < 3) ? s : s.Substring(0, s.Length % 3 == 0 ? 3 : s.Length % 3));
                twoDigits = threeDigits % 100;
                twentyPart = twoDigits % 20;

                level = (s.Length - 1) / 3;

                dig1 = threeDigits / 100;


                if (dig1 > 0)
                {
                    if (dig1 != 1)
                    {
                        words += ones[dig1].Substring(0, ones[dig1].EndsWith("ი") ? ones[dig1].Length - 1 : ones[dig1].Length) + hund.Substring(0, twoDigits == 0 ? hund.Length : hund.Length - 1) + " ";
                    }
                    else
                    {
                        words += hund.Substring(0, twoDigits == 0 ? hund.Length : hund.Length - 1) + " ";
                    }
                }
                if (twoDigits / 20 > 0)
                {
                    if (twentyPart > 0)
                    {
                        words += twenties[twoDigits / 20].Substring(0, twenties[twoDigits / 20].Length - 1) + "და";
                    }
                    else
                    {
                        words += twenties[twoDigits / 20] + " ";
                    }
                }
                if (twentyPart > 0)
                {
                    if (!(threeDigits == 1 && level > 0))
                    {
                        words += ones[twentyPart] + " ";
                    }
                }
                if (level > 0 && threeDigits != 0)
                {
                    words += thou[level].Substring(0, Convert.ToInt64(s.Remove(0, s.Length % 3 == 0 ? 3 : s.Length % 3)) == 0 ? thou[level].Length : thou[level].Length - 1) + " ";
                }
                if (dollars == 0)
                {
                    words += ones[twentyPart] + " ";
                }

                s = s.Remove(0, s.Length % 3 == 0 ? 3 : s.Length % 3);
            }

            words += gel + " და ";




            if (cents / 20 > 0)
            {
                if (cents % 20 > 0)
                {
                    words += twenties[(int)cents / 20].Substring(0, twenties[(int)cents / 20].Length - 1) + "და";
                }
                else
                {
                    words += twenties[(int)cents / 20] + " ";
                }
            }
            if (cents % 20 > 0)
            {
                words += ones[cents % 20] + " ";
            }
            if (cents == 0)
            {
                words += ones[cents] + " ";
            }

            words += cent;

            return words;
        }

        public static RSDriver GetDriverByUID(PXGraph graph, string driverUID)
        {
            return PXSelect<RSDriver,
                                  Where<RSDriver.driverTaxID, Equal<Required<RSDriver.driverTaxID>>>>
                              .Select(graph, driverUID);
        }

        public static RSWaybill GetWaybillByRefNbr(PXGraph graph, string refNbr)
        {
            return PXSelect<RSWaybill,
                                  Where<RSWaybill.waybillNbr, Equal<Required<RSWaybill.waybillNbr>>>>
                                            .Select(graph, refNbr);
        }

        public static RSReceivedWaybill GetReceivedWaybillByRefNbr(PXGraph graph, string refNbr)
        {
            return PXSelect<RSReceivedWaybill,
                                  Where<RSReceivedWaybill.waybillNbr, Equal<Required<RSReceivedWaybill.waybillNbr>>>>
                                            .Select(graph, refNbr);
        }

        public static string GetGEErrorText(this PXGraph graph, string statusID)
        {
            var errorCode = (RSErrorCode)PXSelect<RSErrorCode,
                Where<RSErrorCode.iD, Equal<Required<RSErrorCode.iD>>>>.Select(new PXGraph(), statusID);

            return errorCode.Name;
        }

        public static int? GetVendorIDByTaxRegistrationID(this PXGraph graph, string taxRegistrationID)
        {
            IEnumerable<string> fields = new List<string>
            {
                "BAccountID"
            };

            var query = new PXSelectJoin<Vendor,
                LeftJoin<LocationExtAddress, On<Vendor.bAccountID, Equal<LocationExtAddress.bAccountID>>>,
                    Where<LocationExtAddress.taxRegistrationID, Equal<Required<LocationExtAddress.taxRegistrationID>>>>(graph);

            using (new PXFieldScope(query.View, fields))
            {
                PXView select = new PXView(graph, true, query.View.BqlSelect);

                Vendor result = query.View.SelectSingle(taxRegistrationID) as PXResult<Vendor>;

                return result?.BAccountID;
            }
        }

        public static void ViewWaybillSource(PXGraph graph, int? sourceType, string sourceNbr)
        {
            switch (sourceType)
            {

                case WaybillSourceType.LCABulkRegistration:
                    var lCARegistrationGraph = PXGraph.CreateInstance<LCARegistrationEntry>();

                    lCARegistrationGraph.LCARegistration.Current = PXSelect<EALCARegistration,
                      Where<EALCARegistration.lCARegistrationCD, Equal<Required<EALCARegistration.lCARegistrationCD>>>>.Select(lCARegistrationGraph, sourceNbr);

                    throw new PXRedirectRequiredException(lCARegistrationGraph, true, "ViewSource") { Mode = PXBaseRedirectException.WindowMode.NewWindow };

                case WaybillSourceType.LCABulkTransfer:

                    var lCATransferGraph = PXGraph.CreateInstance<LCATransferEntry>();

                    lCATransferGraph.Document.Current = PXSelect<EALCATransfer,
                          Where<EALCATransfer.lCATransferCD, Equal<Required<EALCATransfer.lCATransferCD>>>>.Select(graph, sourceNbr);

                    throw new PXRedirectRequiredException(lCATransferGraph, true, "ViewSource") { Mode = PXBaseRedirectException.WindowMode.NewWindow };

                case WaybillSourceType.InventoryTransfer:
                    var inventoryTransferGraph = PXGraph.CreateInstance<INTransferEntry>();

                    inventoryTransferGraph.transfer.Current = PXSelect<INRegister,
                      Where<INRegister.refNbr, Equal<Required<INRegister.refNbr>>,
                      And<INRegister.docType, Equal<INDocType.transfer>>>>.Select(graph, sourceNbr);

                    throw new PXRedirectRequiredException(inventoryTransferGraph, true, "ViewSource") { Mode = PXBaseRedirectException.WindowMode.NewWindow };

            }
        }
    }
}

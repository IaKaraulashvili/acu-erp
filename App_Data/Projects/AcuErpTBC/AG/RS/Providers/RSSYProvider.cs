﻿using System;
using System.Collections.Generic;
using System.Linq;
using PX.Api;
using PX.Data;
using global::RS.Services.Implementation;
using global::RS.Services.Domain;
using AG.RS.DAC;
using AG.RS;
using AG.RS.Helpers;
using RS.Services;
using AG.Utils.Extensions;
using global::RS.Services.Helpers;
using AG.RS.Shared;
using static AG.RS.Shared.TaxInvoiceStatusAttribute;

namespace PX.DataSync.Custom
{
    public class RSSYProvider : PXSYBaseProvider, IPXSYProvider
    {
        #region Consts
        private const string BarCode = "BarCode";
        private const string ErrorCode = "ErrorCode";
        private const string ReceivedWaybill = "ReceivedWaybill";
        private const string ReceivedWaybillCorrected = "ReceivedWaybillCorrected";
        private const string Waybill = "Waybill";
        private const string UOM = "UOM";
        private const string Excise = "Excise";
        private const string ReceivedTaxInvoiceRequest = "ReceivedTaxInvoiceRequest";
        private const string TaxInvoiceRequest = "TaxInvoiceRequest";
        private const string DeleteReceivedTaxInvoice = "DeleteReceivedTaxInvoice";
        private const string ReceivedTaxInvoice = "ReceivedTaxInvoice";

        private const string SentTaxInvoice = "SentTaxInvoice";

        // Parameters - Received tax invoice
        private const string ReceivedTaxInvoiceSyncStartDate = "ReceivedTaxInvoiceSyncStartDate";
        private const string ReceivedTaxInvoiceSyncEndDate = "ReceivedTaxInvoiceSyncEndDate";
        private const string ReceivedTaxInvoiceSyncHours = "ReceivedTaxInvoiceSyncHours";
        private const string ReceivedTaxInvoiceCanceledSyncDays = "ReceivedTaxInvoiceCanceledSyncDays";


        // Parameters - Received Waybill
        private const string ReceivedWaybillSyncStartDate = "ReceivedWaybillSyncStartDate";
        private const string ReceivedWaybillSyncEndDate = "ReceivedWaybillSyncEndDate";
        private const string ReceivedWaybillSyncHours = "ReceivedWaybillSyncHours";
        private const string ReceivedWaybillCanceledSyncDays = "ReceivedWaybillCanceledSyncDays";

        //parameters Sent Waybill
        private const string WaybillSyncStartDate = "WaybillSyncStartDate";
        private const string WaybillSyncEndDate = "WaybillSyncEndDate";
        private const string WaybillSyncDays = "WaybillSyncDays";

        //// Parameters - Sent Tax Invoice
        private const string SentTaxInvoiceSyncDays = "SentTaxInvoiceSyncDays";



        #endregion


        #region Properties

        //Since you are integrating with Web Service, file extension is not needed.
        public string DefaultFileExtension
        {
            get
            {
                return null;
            }
        }

        //Display Name to be shown in Asseco
        public override string ProviderName
        {
            get
            {
                return PX.Data.PXMessages.Localize("RS Data Provider");
            }
        }

        #endregion

        #region ctor

        public RSSYProvider()
        {

        }

        #endregion

        #region Schema

        //Schema Objects
        public string[] GetSchemaObjects()
        {
            List<string> ret = new List<string>();

            ret.Add(BarCode);
            ret.Add(ErrorCode);
            ret.Add(ReceivedWaybill);
            ret.Add(ReceivedWaybillCorrected);
            ret.Add(Waybill);
            ret.Add(UOM);
            ret.Add(Excise);
            ret.Add(ReceivedTaxInvoiceRequest);
            ret.Add(TaxInvoiceRequest);
            ret.Add(ReceivedTaxInvoice);
            ret.Add(SentTaxInvoice);
            ret.Add(DeleteReceivedTaxInvoice);

            return ret.ToArray();
        }

        //Fields for the Schema object
        public PXFieldState[] GetSchemaFields(String objectName)
        {
            List<PXFieldState> ret = new List<PXFieldState>();

            if (objectName == BarCode)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.BarCodeMaint(), new Type[] { typeof(AG.RS.DAC.RSBarCode) }, false);

                ret.AddRange(fieldStates);
            }
            else if (objectName == ErrorCode)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.RSErrorCodeMaint(), new Type[] { typeof(AG.RS.DAC.RSErrorCode) }, false);

                ret.AddRange(fieldStates);
            }
            else if (objectName == UOM)
            {
                PXFieldState ID = PXStringState.CreateInstance(
                  "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);
                PXFieldState RSName = PXStringState.CreateInstance(
                  "RSName", typeof(string), false, false, 1, 1, 200, null, "RSName", "", "RSName", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(RSName);
            }
            else if (objectName == Excise)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.ExciseMaint(), new Type[] { typeof(AG.RS.DAC.RSExcise) }, false);
                ret.AddRange(fieldStates);

            }
            else if (objectName == SentTaxInvoice)
            {
                PXFieldState ID = PXStringState.CreateInstance(
                "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);
                PXFieldState STATUS = PXStringState.CreateInstance(
                  "STATUS", typeof(Int32), false, false, 1, 0, 15, null, "STATUS", "", "STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(STATUS);
                PXFieldState TAX_STATUS = PXStringState.CreateInstance(
                  "TAX_STATUS", typeof(string), false, false, 1, 0, 15, null, "TAX_STATUS", "", "TAX_STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(TAX_STATUS);
            }

            else if (objectName == ReceivedTaxInvoiceRequest)
            {
                //var fieldStates = PXFieldState.GetFields(new AG.RS.RSReceivedTaxInvoiceRequestEntry(), new Type[] { typeof(AG.RS.DAC.RSReceivedTaxInvoiceRequest) }, false);
                //ret.AddRange(fieldStates);
                PXFieldState ID = PXStringState.CreateInstance(
                "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);
                PXFieldState BAYER_UN_ID = PXStringState.CreateInstance(
                 "BAYER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "BAYER_UN_ID", "", "BAYER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BAYER_UN_ID);
                PXFieldState SELLER_UN_ID = PXStringState.CreateInstance(
                 "SELLER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "SELLER_UN_ID", "", "SELLER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_UN_ID);
                PXFieldState DT = PXStringState.CreateInstance(
                  "DT", typeof(DateTime), false, false, 1, 0, 255, null, "DT", "", "DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DT);
                PXFieldState NOTES = PXStringState.CreateInstance(
                  "NOTES", typeof(string), false, false, 1, 1, 255, null, "NOTES", "", "NOTES", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NOTES);
                PXFieldState STATUS = PXStringState.CreateInstance(
                 "STATUS", typeof(Int32), false, false, 1, 0, 15, null, "STATUS", "", "STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(STATUS);
                PXFieldState REG_DT = PXStringState.CreateInstance(
                  "REG_DT", typeof(DateTime), false, false, 1, 0, 255, null, "REG_DT", "", "REG_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(REG_DT);
                PXFieldState ACSEPT_DT = PXStringState.CreateInstance(
                  "ACSEPT_DT", typeof(DateTime), false, false, 1, 0, 255, null, "ACSEPT_DT", "", "ACSEPT_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ACSEPT_DT);
                PXFieldState SELLER_ORG_NAME = PXStringState.CreateInstance(
                  "SELLER_ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "SELLER_ORG_NAME", "", "SELLER_ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_ORG_NAME);
                PXFieldState BUYER_ORG_NAME = PXStringState.CreateInstance(
                  "BUYER_ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "BUYER_ORG_NAME", "", "BUYER_ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_ORG_NAME);
                PXFieldState SELLER_TIN = PXStringState.CreateInstance(
                  "SELLER_TIN", typeof(string), false, false, 1, 1, 255, null, "SELLER_TIN", "", "SELLER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_TIN);
                PXFieldState BUYER_TIN = PXStringState.CreateInstance(
                  "BUYER_TIN", typeof(string), false, false, 1, 1, 255, null, "BUYER_TIN", "", "BUYER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_TIN);
                PXFieldState READ_DATE = PXStringState.CreateInstance(
                  "READ_DATE", typeof(DateTime), false, false, 1, 0, 255, null, "READ_DATE", "", "READ_DATE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(READ_DATE);
                PXFieldState NOTIFICATION_TEXT = PXStringState.CreateInstance(
                  "NOTIFICATION_TEXT", typeof(string), false, false, 1, 1, 255, null, "NOTIFICATION_TEXT", "", "NOTIFICATION_TEXT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NOTIFICATION_TEXT);
                PXFieldState VIEWED = PXStringState.CreateInstance(
                  "VIEWED", typeof(Boolean), false, false, 1, 0, 1, null, "VIEWED", "", "VIEWED", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(VIEWED);
                PXFieldState SA_IDENT_NO = PXStringState.CreateInstance(
                  "SA_IDENT_NO", typeof(string), false, false, 1, 1, 255, null, "SA_IDENT_NO", "", "SA_IDENT_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SA_IDENT_NO);
                PXFieldState ORG_NAME = PXStringState.CreateInstance(
                  "ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "ORG_NAME", "", "ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ORG_NAME);
                PXFieldState SIDE = PXStringState.CreateInstance(
                  "SIDE", typeof(string), false, false, 1, 1, 255, null, "SIDE", "", "SIDE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SIDE);

            }

            else if (objectName == TaxInvoiceRequest)
            {
                //var fieldStates = PXFieldState.GetFields(new AG.RS.RSTaxInvoiceRequestEntry(), new Type[]{typeof (AG.RS.DAC.RSTaxInvoiceRequest)},false);
                //ret.AddRange(fieldStates);
                PXFieldState ID = PXStringState.CreateInstance(
                 "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);
                PXFieldState BAYER_UN_ID = PXStringState.CreateInstance(
                 "BAYER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "BAYER_UN_ID", "", "BAYER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BAYER_UN_ID);
                PXFieldState SELLER_UN_ID = PXStringState.CreateInstance(
                 "SELLER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "SELLER_UN_ID", "", "SELLER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_UN_ID);
                PXFieldState DT = PXStringState.CreateInstance(
                  "DT", typeof(DateTime), false, false, 1, 0, 255, null, "DT", "", "DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DT);
                PXFieldState NOTES = PXStringState.CreateInstance(
                  "NOTES", typeof(string), false, false, 1, 1, 255, null, "NOTES", "", "NOTES", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NOTES);
                PXFieldState STATUS = PXStringState.CreateInstance(
                 "STATUS", typeof(Int32), false, false, 1, 0, 15, null, "STATUS", "", "STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(STATUS);
                PXFieldState REG_DT = PXStringState.CreateInstance(
                  "REG_DT", typeof(DateTime), false, false, 1, 0, 255, null, "REG_DT", "", "REG_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(REG_DT);
                PXFieldState ACSEPT_DT = PXStringState.CreateInstance(
                  "ACSEPT_DT", typeof(DateTime), false, false, 1, 0, 255, null, "ACSEPT_DT", "", "ACSEPT_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ACSEPT_DT);
                PXFieldState SELLER_ORG_NAME = PXStringState.CreateInstance(
                  "SELLER_ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "SELLER_ORG_NAME", "", "SELLER_ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_ORG_NAME);
                PXFieldState BUYER_ORG_NAME = PXStringState.CreateInstance(
                  "BUYER_ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "BUYER_ORG_NAME", "", "BUYER_ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_ORG_NAME);
                PXFieldState SELLER_TIN = PXStringState.CreateInstance(
                  "SELLER_TIN", typeof(string), false, false, 1, 1, 255, null, "SELLER_TIN", "", "SELLER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_TIN);
                PXFieldState BUYER_TIN = PXStringState.CreateInstance(
                  "BUYER_TIN", typeof(string), false, false, 1, 1, 255, null, "BUYER_TIN", "", "BUYER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_TIN);
                PXFieldState READ_DATE = PXStringState.CreateInstance(
                  "READ_DATE", typeof(DateTime), false, false, 1, 0, 255, null, "READ_DATE", "", "READ_DATE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(READ_DATE);
                PXFieldState NOTIFICATION_TEXT = PXStringState.CreateInstance(
                  "NOTIFICATION_TEXT", typeof(string), false, false, 1, 1, 255, null, "NOTIFICATION_TEXT", "", "NOTIFICATION_TEXT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NOTIFICATION_TEXT);
                PXFieldState VIEWED = PXStringState.CreateInstance(
                  "VIEWED", typeof(Boolean), false, false, 1, 0, 1, null, "VIEWED", "", "VIEWED", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(VIEWED);
                PXFieldState SA_IDENT_NO = PXStringState.CreateInstance(
                  "SA_IDENT_NO", typeof(string), false, false, 1, 1, 255, null, "SA_IDENT_NO", "", "SA_IDENT_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SA_IDENT_NO);
                PXFieldState ORG_NAME = PXStringState.CreateInstance(
                  "ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "ORG_NAME", "", "ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ORG_NAME);
                PXFieldState SIDE = PXStringState.CreateInstance(
                  "SIDE", typeof(string), false, false, 1, 1, 255, null, "SIDE", "", "SIDE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SIDE);
                PXFieldState OVERHEAD_NO = PXStringState.CreateInstance(
                  "OVERHEAD_NO", typeof(string), false, false, 1, 1, 255, null, "OVERHEAD_NO", "", "OVERHEAD_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OVERHEAD_NO);
            }
            else if (objectName == DeleteReceivedTaxInvoice)
            {
                PXFieldState ID = PXStringState.CreateInstance(
                "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);

                PXFieldState F_SERIES = PXStringState.CreateInstance(
               "F_SERIES", typeof(string), false, false, 1, 1, 255, null, "F_SERIES", "", "F_SERIES", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_SERIES);

                PXFieldState F_NUMBER = PXStringState.CreateInstance(
                 "F_NUMBER", typeof(string), false, false, 1, 1, 255, null, "F_NUMBER", "", "F_NUMBER", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_NUMBER);

                PXFieldState STATUS = PXStringState.CreateInstance(
                "STATUS", typeof(string), false, false, 1, 1, 255, null, "STATUS", "", "STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(STATUS);

                PXFieldState EXT_STATUS = PXStringState.CreateInstance(
                "EXT_STATUS", typeof(Int32), false, false, 1, 0, 15, null, "EXT_STATUS", "", "EXT_STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(EXT_STATUS);

            }
            else if (objectName == ReceivedTaxInvoice)
            {
                // var fieldStates = PXFieldState.GetFields(new AG.RS.WaybillEntry(), new Type[] { typeof(AG.RS.DAC.RSWaybill), typeof(AG.RS.DAC.RSWaybillItem) }, false);

                //ret.AddRange(fieldStates);
                //var fieldStates = PXFieldState.GetFields(new AG.RS.ReceivedTaxInvoiceEntry(), new Type[] { typeof(AG.RS.DAC.RSReceivedTaxInvoice), typeof(AG.RS.DAC.RSReceivedTaxInvoiceItem) }, false);
                //ret.AddRange(fieldStates);
                PXFieldState ID = PXStringState.CreateInstance(
               "ID", typeof(Int32), true, false, 1, 0, 15, null, "ID", "", "ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ID);
                PXFieldState F_SERIES = PXStringState.CreateInstance(
                 "F_SERIES", typeof(string), false, false, 1, 1, 255, null, "F_SERIES", "", "F_SERIES", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_SERIES);
                PXFieldState F_NUMBER = PXStringState.CreateInstance(
                 "F_NUMBER", typeof(string), false, false, 1, 1, 255, null, "F_NUMBER", "", "F_NUMBER", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_NUMBER);
                PXFieldState SEQ_NUM_S = PXStringState.CreateInstance(
                 "SEQ_NUM_S", typeof(Int32), false, false, 1, 0, 15, null, "SEQ_NUM_S", "", "SEQ_NUM_S", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SEQ_NUM_S);
                PXFieldState OPERATION_DT = PXStringState.CreateInstance(
                  "OPERATION_DT", typeof(DateTime), false, false, 1, 0, 255, null, "OPERATION_DT", "", "OPERATION_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OPERATION_DT);
                PXFieldState REG_DT = PXStringState.CreateInstance(
                  "REG_DT", typeof(DateTime), false, false, 1, 0, 255, null, "REG_DT", "", "REG_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(REG_DT);
                PXFieldState SELLER_UN_ID = PXStringState.CreateInstance(
                 "SELLER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "SELLER_UN_ID", "", "SELLER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_UN_ID);
                PXFieldState BUYER_UN_ID = PXStringState.CreateInstance(
                  "BUYER_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "BUYER_UN_ID", "", "BUYER_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_UN_ID);
                PXFieldState OVERHEAD_NO = PXStringState.CreateInstance(
                  "OVERHEAD_NO", typeof(string), false, false, 1, 1, 255, null, "OVERHEAD_NO", "", "OVERHEAD_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OVERHEAD_NO);
                PXFieldState STATUS = PXStringState.CreateInstance(
                  "STATUS", typeof(Int32), false, false, 1, 0, 15, null, "STATUS", "", "STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(STATUS);
                PXFieldState USER_ID = PXStringState.CreateInstance(
                  "USER_ID", typeof(Int32), false, false, 1, 0, 15, null, "USER_ID", "", "USER_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(USER_ID);
                PXFieldState S_USER_ID = PXStringState.CreateInstance(
                  "S_USER_ID", typeof(Int32), false, false, 1, 0, 15, null, "S_USER_ID", "", "S_USER_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(S_USER_ID);
                PXFieldState K_ID = PXStringState.CreateInstance(
                  "K_ID", typeof(Int32), false, false, 1, 0, 15, null, "K_ID", "", "K_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(K_ID);
                PXFieldState K_TYPE = PXStringState.CreateInstance(
                  "K_TYPE", typeof(string), false, false, 1, 1, 255, null, "K_TYPE", "", "K_TYPE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(K_TYPE);
                PXFieldState R_UN_ID = PXStringState.CreateInstance(
                  "R_UN_ID", typeof(Int32), false, false, 1, 0, 15, null, "R_UN_ID", "", "R_UN_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(R_UN_ID);
                PXFieldState NO_STATUS = PXStringState.CreateInstance(
                  "NO_STATUS", typeof(Int32), false, false, 1, 0, 15, null, "NO_STATUS", "", "NO_STATUS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NO_STATUS);
                PXFieldState NO_TEXT = PXStringState.CreateInstance(
                  "NO_TEXT", typeof(string), false, false, 1, 1, 255, null, "NO_TEXT", "", "NO_TEXT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NO_TEXT);
                PXFieldState WAS_REF = PXStringState.CreateInstance(
                  "WAS_REF", typeof(Boolean), false, false, 1, 0, 1, null, "WAS_REF", "", "WAS_REF", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(WAS_REF);
                PXFieldState SEQ_NUM_B = PXStringState.CreateInstance(
                  "SEQ_NUM_B", typeof(Int32), false, false, 1, 0, 15, null, "SEQ_NUM_B", "", "SEQ_NUM_B", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SEQ_NUM_B);
                PXFieldState INVOICE_ID = PXStringState.CreateInstance(
                  "INVOICE_ID", typeof(Int32), false, false, 1, 0, 15, null, "INVOICE_ID", "", "INVOICE_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(INVOICE_ID);
                PXFieldState DOC_MOS_NOM_S = PXStringState.CreateInstance(
                  "DOC_MOS_NOM_S", typeof(string), false, false, 1, 1, 255, null, "DOC_MOS_NOM_S", "", "DOC_MOS_NOM_S", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DOC_MOS_NOM_S);
                PXFieldState DOC_MOS_NOM_B = PXStringState.CreateInstance(
                  "DOC_MOS_NOM_B", typeof(string), false, false, 1, 1, 255, null, "DOC_MOS_NOM_B", "", "DOC_MOS_NOM_B", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DOC_MOS_NOM_B);
                PXFieldState OVERHEAD_DT = PXStringState.CreateInstance(
                  "OVERHEAD_DT", typeof(DateTime), false, false, 1, 0, 255, null, "OVERHEAD_DT", "", "OVERHEAD_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OVERHEAD_DT);
                PXFieldState B_S_USER_ID = PXStringState.CreateInstance(
                  "B_S_USER_ID", typeof(Int32), false, false, 1, 0, 15, null, "B_S_USER_ID", "", "B_S_USER_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(B_S_USER_ID);
                PXFieldState SA_IDENT_NO = PXStringState.CreateInstance(
                  "SA_IDENT_NO", typeof(string), false, false, 1, 1, 255, null, "SA_IDENT_NO", "", "SA_IDENT_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SA_IDENT_NO);
                PXFieldState ORG_NAME = PXStringState.CreateInstance(
                  "ORG_NAME", typeof(string), false, false, 1, 1, 255, null, "ORG_NAME", "", "ORG_NAME", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(ORG_NAME);
                PXFieldState NOTES = PXStringState.CreateInstance(
                  "NOTES", typeof(string), false, false, 1, 1, 255, null, "NOTES", "", "NOTES", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(NOTES);
                PXFieldState TANXA = PXStringState.CreateInstance(
                  "TANXA", typeof(decimal), false, false, 1, 0, 2, null, "TANXA", "", "TANXA", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(TANXA);
                PXFieldState AGREE_DATE = PXStringState.CreateInstance(
                  "AGREE_DATE", typeof(DateTime), false, false, 1, 0, 255, null, "AGREE_DATE", "", "AGREE_DATE", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(AGREE_DATE);
                PXFieldState OP_DT = PXStringState.CreateInstance(
                  "OP_DT", typeof(DateTime), false, false, 1, 0, 255, null, "OP_DT", "", "OP_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OP_DT);
                PXFieldState DT = PXStringState.CreateInstance(
                  "DT", typeof(DateTime), false, false, 1, 0, 255, null, "DT", "", "DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DT);
                PXFieldState BUYER_TIN = PXStringState.CreateInstance(
                  "BUYER_TIN", typeof(string), false, false, 1, 1, 255, null, "BUYER_TIN", "", "BUYER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_TIN);
                PXFieldState SELLER_TIN = PXStringState.CreateInstance(
                  "SELLER_TIN", typeof(string), false, false, 1, 1, 255, null, "SELLER_TIN", "", "SELLER_TIN", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_TIN);
                PXFieldState BUYER_DESC = PXStringState.CreateInstance(
                  "BUYER_DESC", typeof(string), false, false, 1, 1, 255, null, "BUYER_DESC", "", "BUYER_DESC", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(BUYER_DESC);
                PXFieldState SELLER_DESC = PXStringState.CreateInstance(
                  "SELLER_DESC", typeof(string), false, false, 1, 1, 255, null, "SELLER_DESC", "", "SELLER_DESC", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SELLER_DESC);
                PXFieldState F_SERIES_K = PXStringState.CreateInstance(
                  "F_SERIES_K", typeof(string), false, false, 1, 1, 255, null, "F_SERIES_K", "", "F_SERIES_K", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_SERIES_K);
                PXFieldState F_NUMBER_K = PXStringState.CreateInstance(
                  "F_NUMBER_K", typeof(string), false, false, 1, 1, 255, null, "F_NUMBER_K", "", "F_NUMBER_K", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(F_NUMBER_K);
                PXFieldState OP_DT_K = PXStringState.CreateInstance(
                  "OP_DT_K", typeof(DateTime), false, false, 1, 0, 255, null, "OP_DT_K", "", "OP_DT_K", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(OP_DT_K);
                //items
                PXFieldState TAXINVOICEITEM_ID = PXStringState.CreateInstance(
                  "ID", typeof(Int32), false, false, 1, 0, 15, null, "TAXINVOICEITEM_ID", "", "TAXINVOICEITEM_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(TAXINVOICEITEM_ID);
                PXFieldState INV_ID = PXStringState.CreateInstance(
                  "INV_ID", typeof(Int32), false, false, 1, 0, 15, null, "INV_ID", "", "INV_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(INV_ID);
                PXFieldState GOODS = PXStringState.CreateInstance(
                  "GOODS", typeof(string), false, false, 1, 1, 255, null, "GOODS", "", "GOODS", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(GOODS);
                PXFieldState G_UNIT = PXStringState.CreateInstance(
                  "G_UNIT", typeof(string), false, false, 1, 1, 255, null, "G_UNIT", "", "G_UNIT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(G_UNIT);
                PXFieldState G_NUMBER = PXStringState.CreateInstance(
                  "G_NUMBER", typeof(decimal), false, false, 1, 0, 2, null, "G_NUMBER", "", "G_NUMBER", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(G_NUMBER);
                PXFieldState FULL_AMOUNT = PXStringState.CreateInstance(
                  "FULL_AMOUNT", typeof(decimal), false, false, 1, 0, 2, null, "FULL_AMOUNT", "", "FULL_AMOUNT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(FULL_AMOUNT);
                PXFieldState DRG_AMOUNT = PXStringState.CreateInstance(
                  "DRG_AMOUNT", typeof(decimal), false, false, 1, 0, 2, null, "DRG_AMOUNT", "", "DRG_AMOUNT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(DRG_AMOUNT);
                PXFieldState AQCIZI_AMOUNT = PXStringState.CreateInstance(
                  "AQCIZI_AMOUNT", typeof(decimal), false, false, 1, 0, 2, null, "AQCIZI_AMOUNT", "", "AQCIZI_AMOUNT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(AQCIZI_AMOUNT);
                //end items

                PXFieldState AKCIS_ID = PXStringState.CreateInstance(
                  "AKCIS_ID", typeof(Int32), false, false, 1, 0, 15, null, "AKCIS_ID", "", "AKCIS_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(AKCIS_ID);
                PXFieldState WAYBILL_ID = PXStringState.CreateInstance(
                  "WAYBILL_ID", typeof(Int32), false, false, 1, 0, 15, null, "WAYBILL_ID", "", "WAYBILL_ID", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(WAYBILL_ID);
                PXFieldState SDRG_AMOUNT = PXStringState.CreateInstance(
                  "SDRG_AMOUNT", typeof(string), false, false, 1, 1, 255, null, "SDRG_AMOUNT", "", "SDRG_AMOUNT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(SDRG_AMOUNT);

                PXFieldState RowId = PXStringState.CreateInstance(
            "RowId", typeof(Int32), false, false, 1, 0, 15, null, "RowId", "", "RowId", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(RowId);

                PXFieldState WAYBILL_NO = PXStringState.CreateInstance(
             "WAYBILL_NO", typeof(string), false, false, 1, 1, 255, null, "WAYBILL_NO", "", "WAYBILL_NO", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(WAYBILL_NO);

                PXFieldState WAYBILL_DT = PXStringState.CreateInstance(
               "WAYBILL_DT", typeof(DateTime), false, false, 1, 0, 255, null, "WAYBILL_DT", "", "WAYBILL_DT", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(WAYBILL_DT);

                PXFieldState PrePayment = PXStringState.CreateInstance(
                "PrePayment", typeof(Boolean), false, false, 1, 0, 1, null, "PrePayment", "", "PrePayment", "", PXErrorLevel.Error, true, true, false, PXUIVisibility.Visible, "", null, null);
                ret.Add(PrePayment);

            }
            else if (objectName == Waybill)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.WaybillEntry(), new Type[] { typeof(AG.RS.DAC.RSWaybill), typeof(AG.RS.DAC.RSWaybillItem) }, false);

                ret.AddRange(fieldStates);
            }
            else if (objectName == ReceivedWaybill)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.ReceivedWaybillEntry(), new Type[] { typeof(AG.RS.DAC.RSReceivedWaybill), typeof(AG.RS.DAC.RSReceivedWaybillItem) }, false);

                ret.AddRange(fieldStates);
            }
            else if (objectName == ReceivedWaybillCorrected)
            {
                var fieldStates = PXFieldState.GetFields(new AG.RS.WaybillHistoryEntry(), new Type[] { typeof(AG.RS.DAC.RSWaybillHistory), typeof(AG.RS.DAC.RSWaybillItemHistory) }, false);

                ret.AddRange(fieldStates);
            }

            return ret.ToArray();
        }

        protected override List<PXStringState> FillParameters()
        {
            return new List<PXStringState>
            {
                CreateParameter(ReceivedWaybillSyncStartDate, "Received Waybill Sync Start Date"),
                CreateParameter(ReceivedWaybillSyncEndDate, "Received Waybill Sync End Date"),
                CreateParameter(ReceivedWaybillSyncHours, "Received Waybill Sync Hours", "24"),
                CreateParameter(ReceivedWaybillCanceledSyncDays, "Received Waybill Canceled Sync Days", "30"),
                CreateParameter(WaybillSyncStartDate, "Sent Waybill Sync Start Date"),
                CreateParameter(WaybillSyncEndDate, "Sent Waybill Sync End Date"),
                CreateParameter(WaybillSyncDays, "Sent Waybill Sync Days", "30"),
                CreateParameter(SentTaxInvoiceSyncDays, "Sent Tax Invoice Sync Days", "30"),
                CreateParameter(ReceivedTaxInvoiceSyncStartDate, "Received TaxInvoice Sync Start Date"),
                CreateParameter(ReceivedTaxInvoiceSyncEndDate, "Received TaxInvoice Sync End Date"),
                CreateParameter(ReceivedTaxInvoiceSyncHours, "Received TaxInvoice Sync  Hours","24"),
                CreateParameter(ReceivedTaxInvoiceCanceledSyncDays, "Received TaxInvoice Canceled Sync Days", "30"),


            };
        }

        #endregion

        #region Import

        public PXSYTable Import(String objectName, String[] fieldNames, PXSYFilterRow[] filters, String lastTimeStamp, PXSYSyncTypes syncType)
        {
            string url = string.Empty, login = string.Empty, pwd = string.Empty;

            GetWaybillParameters(ref url, ref login, ref pwd);

            PXSYTable ret = new PXSYTable(fieldNames);
            ret.TimeStamp = lastTimeStamp;
            ret.ObjectName = objectName;

            if (objectName == ErrorCode)
            {
                ERROR_CODES errorCodes = null;
                using (var ws = new WaybillService(login, pwd, url, 10000))
                {
                    errorCodes = ws.GetErrorCodes();
                }
                for (int i = 0; i < errorCodes.Items.Length; i++)
                {
                    var row = new PXSYRow(ret);
                    row.SetItem(fieldNames[0], new PXSYItem(errorCodes.Items[i].ID));
                    row.SetItem(fieldNames[1], new PXSYItem(errorCodes.Items[i].TEXT));
                    row.SetItem(fieldNames[2], new PXSYItem(errorCodes.Items[i].TYPE));
                    ret.Add(row);
                }
            }
            else if (objectName == BarCode)
            {
                BAR_CODES barCodes = null;
                using (var ws = new WaybillService(login, pwd, url, 10000))
                {
                    barCodes = ws.GetBarCodes("");
                }

                for (int i = 0; i < barCodes.Items.Length; i++)
                {
                    var row = new PXSYRow(ret);

                    row.SetItem(fieldNames[0], new PXSYItem(barCodes.Items[i].CODE));
                    row.SetItem(fieldNames[1], new PXSYItem(barCodes.Items[i].NAME));
                    row.SetItem(fieldNames[2], new PXSYItem(barCodes.Items[i].UNIT_TXT));
                    row.SetItem(fieldNames[2], new PXSYItem(barCodes.Items[i].UNIT_ID));
                    row.SetItem(fieldNames[3], new PXSYItem(barCodes.Items[i].UNIT_TXT));
                    ret.Add(row);
                }
            }
            else if (objectName == UOM)
            {
                WAYBILL_UNITSWAYBILL_UNIT[] uOM = null;
                using (var ws = new WaybillService(login, pwd, url, 10000))
                {
                    uOM = ws.GetWaybillUnitTypes();
                }
                for (int i = 0; i < uOM.Length; i++)
                {
                    var row = new PXSYRow(ret);
                    row.SetItem(fieldNames[0], new PXSYItem(uOM[i].ID));
                    row.SetItem(fieldNames[1], new PXSYItem(uOM[i].NAME));
                    ret.Add(row);
                }
            }
            else if (objectName == Excise)
            {
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);

                TaxInvoiceExcise[] excises = null;
                using (var tis = new TaxInvoiceService(login, pwd, url, 10000))
                {
                    excises = tis.get_akciz("").ToArray();
                }

                for (int i = 0; i < excises.Length; i++)
                {
                    var row = new PXSYRow(ret);

                    row.SetItem(fieldNames[0], new PXSYItem(excises[i].ID.ToString()));
                    row.SetItem(fieldNames[1], new PXSYItem(excises[i].TITLE));
                    row.SetItem(fieldNames[2], new PXSYItem(excises[i].MEASUREMENT));
                    row.SetItem(fieldNames[3], new PXSYItem(excises[i].SAKON_KODI));
                    row.SetItem(fieldNames[4], new PXSYItem(excises[i].AKCIS_GANAKV));
                    ret.Add(row);
                }
            }
            else if (objectName == ReceivedTaxInvoiceRequest)
            {
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);
                TaxInvoiceRequest[] tiRequests = null;
                using (var tis = new TaxInvoiceService(login, pwd, url, 1000))
                {
                    int tin = tis.get_un_id_from_tin("206322102");
                    tiRequests = tis.get_requested_invoices(tin).ToArray();
                }
                for (int i = 0; i < tiRequests.Length; i++)
                {
                    var row = new PXSYRow(ret);
                    row.SetItem(fieldNames[1], new PXSYItem(tiRequests[i].STATUS.ToString()));
                    row.SetItem(fieldNames[6], new PXSYItem(tiRequests[i].ID.ToString()));
                    row.SetItem(fieldNames[7], new PXSYItem(tiRequests[i].REG_DT.ToString()));
                    row.SetItem(fieldNames[8], new PXSYItem(tiRequests[i].DT.ToString()));
                    row.SetItem(fieldNames[9], new PXSYItem(tiRequests[i].READ_DATE.ToString()));
                    row.SetItem(fieldNames[10], new PXSYItem(tiRequests[i].ACSEPT_DT.ToString()));


                    ret.Add(row);
                }
            }
            else if (objectName == TaxInvoiceRequest)
            {
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);
                TaxInvoiceRequest[] tiRequests = null;
                using (var tis = new TaxInvoiceService(login, pwd, url, 1000))
                {
                    int tin = tis.get_un_id_from_tin("206322102");
                    tiRequests = tis.get_invoice_requests(tin).ToArray();
                }
                for (int i = 0; i < tiRequests.Length; i++)
                {
                    var row = new PXSYRow(ret);
                    row.SetItem(fieldNames[2], new PXSYItem(tiRequests[i].STATUS.ToString()));
                    row.SetItem(fieldNames[4], new PXSYItem(tiRequests[i].BAYER_UN_ID.ToString()));
                    row.SetItem(fieldNames[6], new PXSYItem(tiRequests[i].NOTIFICATION_TEXT));
                    row.SetItem(fieldNames[7], new PXSYItem(tiRequests[i].REG_DT.ToString()));
                    row.SetItem(fieldNames[8], new PXSYItem(tiRequests[i].DT.ToString()));
                    row.SetItem(fieldNames[9], new PXSYItem(tiRequests[i].READ_DATE.ToString()));
                    row.SetItem(fieldNames[10], new PXSYItem(tiRequests[i].ACSEPT_DT.ToString()));
                    row.SetItem(fieldNames[11], new PXSYItem(tiRequests[i].BUYER_TIN.ToString()));
                    row.SetItem(fieldNames[14], new PXSYItem(tiRequests[i].BUYER_ORG_NAME));
                    ret.Add(row);
                }
            }
            else if (objectName == ReceivedTaxInvoice)
            {
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);
                TaxInvoice[] taxInvoices = null;
                using (var tis = new TaxInvoiceService(login, pwd, url, 1000))
                {
                    int un_id = tis.get_un_id_from_user_id();

                    //tis.get_buyer_invoices(un_id,,,,,)


                    taxInvoices = tis.get_buyer_invoices_r(un_id, -2).ToArray();

                    taxInvoices = FilterTaxInvoices(taxInvoices);

                    RSUnitMaint UomGraph = PXGraph.CreateInstance<RSUnitMaint>();


                    var UOMs = PXSelectReadonly<RSUnit>.Select(UomGraph).FirstTableItems;


                    for (int i = 0; i < taxInvoices.Length; i++)
                    {
                        int InvoiceId = taxInvoices[i].ID.Value;
                        List<TaxInvoiceItem> TaxInvoiceItems = tis.get_invoice_desc(InvoiceId);
                        List<TaxInvoiceWaybill> TaxInvoiceWaybills = tis.get_ntos_invoices_inv_nos(InvoiceId);

                        var ItemCount = TaxInvoiceItems.Count;
                        var WaybillCount = TaxInvoiceWaybills.Count;
                        var ListCountMax = Math.Max(ItemCount, WaybillCount);

                        var RowId = 0;

                        if (TaxInvoiceItems.Count() > 0)
                        {
                            for (int li = 0; li < ListCountMax; li++)
                            {
                                RowId++;

                                var row = new PXSYRow(ret);
                                row.SetItem(fieldNames[0], new PXSYItem(taxInvoices[i].ID.ToString()));
                                //უნიკალურია ეს კომბინაცია
                                row.SetItem(fieldNames[1], new PXSYItem(taxInvoices[i].F_SERIES.ToString()));
                                row.SetItem(fieldNames[2], new PXSYItem(taxInvoices[i].F_NUMBER.ToString()));
                                //--------------

                                row.SetItem(fieldNames[3], new PXSYItem(taxInvoices[i].SEQ_NUM_S?.ToString()));
                                row.SetItem(fieldNames[4], new PXSYItem(taxInvoices[i].OPERATION_DT?.ToString("MMyyyy")));
                                //row.SetItem(fieldNames[4], new PXSYItem("02-2017"));
                                row.SetItem(fieldNames[5], new PXSYItem(taxInvoices[i].REG_DT.ToString()));
                                row.SetItem(fieldNames[6], new PXSYItem(taxInvoices[i].SELLER_UN_ID?.ToString()));
                                row.SetItem(fieldNames[7], new PXSYItem(taxInvoices[i].BUYER_UN_ID?.ToString()));
                                row.SetItem(fieldNames[8], new PXSYItem(taxInvoices[i].OVERHEAD_NO?.ToString()));
                                row.SetItem(fieldNames[9], new PXSYItem(taxInvoices[i].STATUS?.ToString()));

                                row.SetItem(fieldNames[10], new PXSYItem(taxInvoices[i].USER_ID?.ToString()));
                                row.SetItem(fieldNames[11], new PXSYItem(taxInvoices[i].S_USER_ID?.ToString()));

                                row.SetItem(fieldNames[12], new PXSYItem(taxInvoices[i].K_ID?.ToString()));

                                row.SetItem(fieldNames[13], new PXSYItem(taxInvoices[i].K_TYPE?.ToString()));

                                row.SetItem(fieldNames[15], new PXSYItem(taxInvoices[i].R_UN_ID?.ToString()));
                                row.SetItem(fieldNames[16], new PXSYItem(taxInvoices[i].NO_STATUS?.ToString()));
                                //row.SetItem(fieldNames[17], new PXSYItem(taxInvoices[i].NO_TEXT?.ToString()));

                                row.SetItem(fieldNames[17], new PXSYItem(taxInvoices[i].WAS_REF?.ToString()));

                                row.SetItem(fieldNames[18], new PXSYItem(taxInvoices[i].SEQ_NUM_B?.ToString()));

                                row.SetItem(fieldNames[19], new PXSYItem(taxInvoices[i].INVOICE_ID?.ToString()));
                                row.SetItem(fieldNames[20], new PXSYItem(taxInvoices[i].DOC_MOS_NOM_S?.ToString()));
                                row.SetItem(fieldNames[21], new PXSYItem(taxInvoices[i].DOC_MOS_NOM_B?.ToString()));

                                row.SetItem(fieldNames[22], new PXSYItem(taxInvoices[i].OVERHEAD_DT?.ToString()));
                                row.SetItem(fieldNames[23], new PXSYItem(taxInvoices[i].B_S_USER_ID?.ToString()));
                                row.SetItem(fieldNames[24], new PXSYItem(taxInvoices[i].SA_IDENT_NO?.ToString()));
                                row.SetItem(fieldNames[25], new PXSYItem(taxInvoices[i].ORG_NAME?.ToString()));
                                row.SetItem(fieldNames[26], new PXSYItem(taxInvoices[i].NOTES?.ToString()));
                                row.SetItem(fieldNames[27], new PXSYItem(taxInvoices[i].TANXA?.ToString()));
                                row.SetItem(fieldNames[28], new PXSYItem(taxInvoices[i].AGREE_DATE?.ToString()));
                                row.SetItem(fieldNames[29], new PXSYItem(taxInvoices[i].OP_DT?.ToString()));
                                row.SetItem(fieldNames[30], new PXSYItem(taxInvoices[i].DT?.ToString()));
                                row.SetItem(fieldNames[31], new PXSYItem(taxInvoices[i].BUYER_TIN?.ToString()));
                                row.SetItem(fieldNames[32], new PXSYItem(taxInvoices[i].SELLER_TIN?.ToString()));
                                row.SetItem(fieldNames[33], new PXSYItem(taxInvoices[i].BUYER_DESC?.ToString()));
                                row.SetItem(fieldNames[34], new PXSYItem(taxInvoices[i].SELLER_DESC?.ToString()));
                                row.SetItem(fieldNames[35], new PXSYItem(taxInvoices[i].F_SERIES_K?.ToString()));
                                row.SetItem(fieldNames[36], new PXSYItem(taxInvoices[i].F_NUMBER_K?.ToString()));
                                row.SetItem(fieldNames[37], new PXSYItem(taxInvoices[i].OP_DT_K?.ToString()));


                                var item = TaxInvoiceItems[Math.Min(li, ItemCount - 1)];
                                TaxInvoiceWaybill waybill = new TaxInvoiceWaybill();

                                if (WaybillCount > 0)
                                    waybill = TaxInvoiceWaybills[Math.Min(li, WaybillCount - 1)];

                                var unit = UOMs.Where(x => x.Name.Equals(item.G_UNIT?.ToString())).FirstOrDefault();

                                row.SetItem(fieldNames[38], new PXSYItem(item.ID?.ToString()));
                                row.SetItem(fieldNames[39], new PXSYItem(item.INV_ID?.ToString()));
                                row.SetItem(fieldNames[40], new PXSYItem(item.GOODS?.ToString()));
                                //G_UNIT
                                row.SetItem(fieldNames[41], new PXSYItem(unit?.ID.ToString()));
                                //G_NUMBER
                                string G_NUMBER = item.G_NUMBER?.ToString();
                                row.SetItem(fieldNames[42], new PXSYItem(string.IsNullOrEmpty(G_NUMBER) ? "1" : G_NUMBER));
                                //FULL_AMOUNT
                                row.SetItem(fieldNames[43], new PXSYItem(item.FULL_AMOUNT?.ToString()));
                                //DRG_AMOUNT
                                row.SetItem(fieldNames[44], new PXSYItem(item.DRG_AMOUNT?.ToString()));
                                //AQCIZI_AMOUNT
                                row.SetItem(fieldNames[45], new PXSYItem(item.AQCIZI_AMOUNT?.ToString()));
                                //WAYBILL_ID
                                if (WaybillCount > 0)
                                {
                                    row.SetItem(fieldNames[47], new PXSYItem(waybill.ID?.ToString()));
                                    row.SetItem(fieldNames[50], new PXSYItem(waybill.OVERHEAD_NO?.ToString()));
                                    row.SetItem(fieldNames[51], new PXSYItem(waybill.OVERHEAD_DT?.ToString()));
                                }

                                //RowId
                                row.SetItem(fieldNames[49], new PXSYItem(RowId.ToString()));
                                

                                row.SetItem(fieldNames[52], new PXSYItem(taxInvoices[i].F_SERIES?.StartsWith("ავ").ToString()));

                                ret.Add(row);
                            }
                        }

                    }
                }
            }
            else if (objectName == DeleteReceivedTaxInvoice)
            {
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);
                List<TaxInvoice> taxInvoices = null;
                using (var tis = new TaxInvoiceService(login, pwd, url, 1000))
                {
                    int un_id = tis.get_un_id_from_user_id();

                    ReceivedTaxInvoiceEntry g = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();
                    var TaxInvoicesInSystem = g.ReceivedTaxInvoice.Select().FirstTableItems.ToList();

                    taxInvoices = tis.get_buyer_invoices_r(un_id, -2).ToList();

                    TaxInvoicesInSystem = TaxInvoicesInSystem.Where(x => !taxInvoices.Exists(y => y.ID == x.TaxInvoiceID)).ToList();

                    for (int i = 0; i < TaxInvoicesInSystem.Count; i++)
                    {
                        var row = new PXSYRow(ret);
                        row.SetItem(fieldNames[0], new PXSYItem(TaxInvoicesInSystem[i].TaxInvoiceID.ToString()));
                        //უნიკალურია ეს კომბინაცია
                        row.SetItem(fieldNames[1], new PXSYItem(TaxInvoicesInSystem[i].TaxInvoiceSeries.ToString()));
                        row.SetItem(fieldNames[2], new PXSYItem(TaxInvoicesInSystem[i].TaxInvoiceNumber.ToString()));
                        //--------------
                        row.SetItem(fieldNames[3], new PXSYItem("D"));
                        row.SetItem(fieldNames[4], new PXSYItem(RecivedTaxInvoiceStatusAttribute.ExtStatus.Canceled.ToString()));

                        ret.Add(row);
                    }
                }

            }
            else if (objectName == SentTaxInvoice)
            {
                DateTime createDate;
                GetSentTaxInvoiceDate(out createDate);

                TaxInvoiceEntry sentTaxInvoice = PXGraph.CreateInstance<TaxInvoiceEntry>();
                var taxInvoice = PXSelect<RSTaxInvoice, Where<RSTaxInvoice.status, Equal<statusSent>, And<RSTaxInvoice.createDate, GreaterEqual<Required<RSTaxInvoice.createDate>>>>>.Select(sentTaxInvoice, createDate);
                GetTaxInvoiceServiceParameters(ref url, ref login, ref pwd);
                foreach (RSTaxInvoice item in taxInvoice)
                {
                    using (var context = new TaxInvoiceService(login, pwd, url, 1000))
                    {
                        string f_series;
                        int f_number;
                        DateTime operation_dt;
                        DateTime reg_dt;
                        int seller_un_id;
                        int buyer_un_id;
                        string overhead_no;
                        DateTime overhead_dt;
                        int status;
                        string seq_num_s;
                        string seq_num_b;
                        int k_id;
                        int r_un_id;
                        int k_type;
                        int b_s_user_id;
                        int dec_status;

                        var complete_successfully = context.get_invoice(item.TaxInvoiceID.Value, out f_series, out f_number, out operation_dt, out reg_dt, out seller_un_id,
                        out buyer_un_id, out overhead_no, out overhead_dt, out status, out seq_num_s, out seq_num_b, out k_id, out r_un_id, out k_type, out b_s_user_id,
                        out dec_status);

                        if (status == SentTaxInvoiceStatuses.Confirmed || status == SentTaxInvoiceStatuses.Rejected || status == SentTaxInvoiceStatuses.CorrectedConfirmed)

                        {

                            var row = new PXSYRow(ret);
                            row.SetItem(fieldNames[0], new PXSYItem(item.TaxInvoiceID.ToString()));
                            row.SetItem(fieldNames[2], new PXSYItem(status.ToString()));
                            if (status == SentTaxInvoiceStatuses.Confirmed || status == SentTaxInvoiceStatuses.CorrectedConfirmed)
                                row.SetItem(fieldNames[1], new PXSYItem(TaxInvoiceStatusAttribute.Statuses.Approved));
                            else
                                row.SetItem(fieldNames[1], new PXSYItem(TaxInvoiceStatusAttribute.Statuses.Rejected));
                            ret.Add(row);
                        }
                    }
                }
            }
            else if (objectName == Waybill)
            {
                DateTime startDate;
                DateTime endDate;
                GetWaybillDates(out startDate, out endDate);

                var graph = PXGraph.CreateInstance<AG.RS.WaybillEntry>();

                var sourceWaybills = PXSelectReadonly<AG.RS.DAC.RSWaybill,
                    Where2<
                        Where<AG.RS.DAC.RSWaybill.waybillID, IsNotNull,
                            And<AG.RS.DAC.RSWaybill.createdDateTime, Between<Required<AG.RS.DAC.RSWaybill.createdDateTime>, Required<AG.RS.DAC.RSWaybill.createdDateTime>>>>,
                        And<Where<AG.RS.DAC.RSWaybill.waybillState, IsNotNull,
                            And<AG.RS.DAC.RSWaybill.waybillState, NotEqual<WaybillStates.received>,
                            Or<AG.RS.DAC.RSWaybill.waybillStatus, Equal<WaybillExtStatus.sentToCarrier>>>>>>>
                    .Select(graph, startDate, endDate).FirstTableItems;

                List<WAYBILL> waybills = new List<WAYBILL>();
                using (var ws = new WaybillService(login, pwd, url, 10000))
                {
                    foreach (var item in sourceWaybills)
                    {
                        try
                        {
                            waybills.Add(ws.GetWaybill(int.Parse(item.WaybillID)));
                        }
                        catch (WaybillResultException ex)
                        {
                            throw new PXException(graph.GetGEErrorText(ex.ErrorCode));
                        }
                    }
                }

                FillWaybillTable(fieldNames, ret, waybills.ToArray());
            }
            else if (objectName == ReceivedWaybill)
            {
                DateTime startDate;
                DateTime endDate;
                DateTime canceledStartDate;


                GetReceivedWaybillDates(out startDate, out endDate, out canceledStartDate);

                WAYBILL[] waybills = null;
                var graph = PXGraph.CreateInstance<AG.RS.ReceivedWaybillEntry>();

                try
                {
                    using (var ws = new WaybillService(login, pwd, url, 10000))
                    {
                        waybills = ws.GetReceivedWaybillsFull(startDate, endDate, canceledStartDate);
                    }
                }
                catch (WaybillResultException ex)
                {
                    throw new PXException(graph.GetGEErrorText(ex.ErrorCode));
                }

                List<WAYBILLGOODS_LISTGOODS> goodsList = null;

                if (waybills != null)
                {
                    foreach (var wb in waybills.Where(x => x.GOODS_LIST != null && x.GOODS_LIST.Length > 0))
                    {
                        goodsList = new List<WAYBILLGOODS_LISTGOODS>();

                        var itemsInSystem = PXSelectReadonly2<AG.RS.DAC.RSReceivedWaybillItem,
                            InnerJoin<AG.RS.DAC.RSReceivedWaybill, On<AG.RS.DAC.RSReceivedWaybillItem.waybillNbr, Equal<AG.RS.DAC.RSReceivedWaybill.waybillNbr>>>,
                                Where<AG.RS.DAC.RSReceivedWaybill.waybillID, Equal<Required<AG.RS.DAC.RSReceivedWaybill.waybillID>>,
                                    And<AG.RS.DAC.RSReceivedWaybillItem.itemRowStatus, NotEqual<AG.RS.DAC.RSWaybillItemStatuses.deleted>>>>
                            .Select(graph, wb.ID).FirstTableItems;

                        foreach (var item in itemsInSystem)
                        {
                            var srcItem = wb.GOODS_LIST.FirstOrDefault(x => x.ID == item.ItemRowID);

                            if (srcItem == null) //Set line number for identification
                            {
                                srcItem = item.GetExternalWaybillItem();
                                srcItem.STATUS = AG.RS.DAC.RSWaybillItemStatuses.Deleted.ToString();
                            }

                            goodsList.Add(srcItem);
                        }

                        var newItems = wb.GOODS_LIST.Where(x => !goodsList.Any(y => x.ID == y.ID));

                        //Add new items
                        goodsList.AddRange(newItems);

                        wb.GOODS_LIST = goodsList.ToArray();
                    }

                    FillWaybillAndItemsTable(fieldNames, ret, waybills, typeof(AG.RS.DAC.RSReceivedWaybillItem));
                }
            }
            else if (objectName == ReceivedWaybillCorrected)
            {
                DateTime startDate;
                DateTime endDate;
                DateTime canceledStartDate;

                GetReceivedWaybillDates(out startDate, out endDate, out canceledStartDate);

                WAYBILL[] waybills = null;
                var graph = PXGraph.CreateInstance<AG.RS.WaybillHistoryEntry>();

                try
                {
                    using (var ws = new WaybillService(login, pwd, url, 10000))
                    {
                        waybills = ws.GetCorrectedWaybills(startDate, endDate);
                    }
                }
                catch (WaybillResultException ex)
                {
                    throw new PXException(graph.GetGEErrorText(ex.ErrorCode));
                }

                List<WAYBILLGOODS_LISTGOODS> goodsList = null;

                if (waybills != null)
                {
                    foreach (var wb in waybills)
                    {
                        RSReceivedWaybill wbInSystem = PXSelect<RSReceivedWaybill,
                            Where<RSReceivedWaybill.waybillNumber, Equal<Required<RSReceivedWaybill.waybillNumber>>>>.Select(graph, wb.WAYBILL_NUMBER).FirstOrDefault();

                        if (wbInSystem == null)
                        {
                            continue;
                        }

                        wb.WaybillNbr = wbInSystem.WaybillNbr;

                        goodsList = new List<WAYBILLGOODS_LISTGOODS>();

                        var itemsInSystem = PXSelectReadonly2<AG.RS.DAC.RSWaybillItemHistory,
                            InnerJoin<AG.RS.DAC.RSWaybillHistory, On<AG.RS.DAC.RSWaybillItemHistory.waybillHistoryID, Equal<AG.RS.DAC.RSWaybillHistory.waybillHistoryID>>>,
                                Where<AG.RS.DAC.RSWaybillHistory.waybillID, Equal<Required<AG.RS.DAC.RSWaybillHistory.waybillID>>,
                                    And<RSWaybillHistory.classID, Equal<WaybillClass.received>,
                                        And<AG.RS.DAC.RSWaybillItemHistory.itemRowStatus, NotEqual<AG.RS.DAC.RSWaybillItemStatuses.deleted>>>>>
                            .Select(graph, wb.ID).FirstTableItems;

                        foreach (var item in itemsInSystem)
                        {
                            var srcItem = wb.GOODS_LIST.FirstOrDefault(x => x.ID == item.ItemRowID);

                            if (srcItem == null) //Set line number for identification
                            {
                                srcItem = item.GetExternalWaybillItem();
                                srcItem.STATUS = AG.RS.DAC.RSWaybillItemStatuses.Deleted.ToString();
                            }

                            goodsList.Add(srcItem);
                        }

                        var newItems = wb.GOODS_LIST.Where(x => !goodsList.Any(y => x.ID == y.ID));

                        //Add new items
                        goodsList.AddRange(newItems);

                        wb.GOODS_LIST = goodsList.ToArray();
                    }

                    FillWaybillAndItemsTable(fieldNames, ret, waybills.Where(x => x.WaybillNbr != null).ToArray(), typeof(AG.RS.DAC.RSWaybillHistory));
                }
            }


            return ret;
        }

        private TaxInvoice[] FilterTaxInvoices(TaxInvoice[] RsTaxInvoices)
        {
            ReceivedTaxInvoiceEntry g = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();
            var TaxInvoicesInSystem = g.ReceivedTaxInvoice.Select().FirstTableItems;
            DateTime StartDate;
            DateTime EndDate;
            DateTime CancelDate;
            GetReceivedTaxInvoiceDates(out StartDate, out EndDate, out CancelDate);

            var RsTaxInvoicesList = RsTaxInvoices.ToList().Where(y => y.REG_DT.HasValue && y.REG_DT.Value > StartDate
                        && y.REG_DT.Value < EndDate
                        ).ToList();
            var TaxInvoicesInSystemList = TaxInvoicesInSystem.ToList();





            //RsTaxInvoicesList.RemoveAll(x => TaxInvoicesInSystemList.Exists
            //            (y => y.TaxInvoiceSeries != x.F_SERIES & y.TaxInvoiceNumber != x.F_NUMBER)

            //            && (x.OPERATION_DT.HasValue && x.OPERATION_DT.Value > StartDate
            //            && x.OPERATION_DT.Value < EndDate
            //            ));


            //NewTaxInvoices = RsTaxInvoicesList.ToArray();
            //return NewTaxInvoices;


            foreach (var tax in TaxInvoicesInSystemList)
            {
                var RsTax = RsTaxInvoicesList.Where(y => y.F_SERIES == tax.TaxInvoiceSeries & y.F_NUMBER == tax.TaxInvoiceNumber).FirstOrDefault();

                RsTaxInvoicesList.Remove(RsTax);

                //if (RsTax != null)
                //{
                //    yield return RsTax;

                //}
            }
            return RsTaxInvoicesList.ToArray();
        }

        protected void FillWaybillAndItemsTable(String[] fieldNames, PXSYTable table, WAYBILL[] waybills, Type itemType)
        {
            if (waybills == null)
            {
                return;
            }

            var itemPrefix = itemType.GetType().Name;

            for (int i = 0; i < waybills.Length; i++)
            {
                var wb = waybills[i];

                if (wb.GOODS_LIST == null)
                {
                    continue;
                }

                for (int k = 0; k < wb.GOODS_LIST.Length; k++)
                {
                    var row = new PXSYRow(table);

                    for (int j = 0; j < fieldNames.Length; j++)
                    {
                        PXSYItem item = GetPXSYItem(fieldNames[j], wb, k);

                        if (item != null)
                        {
                            row.SetItem(fieldNames[j], item);
                        }
                    }

                    table.Add(row);
                }
            }
        }

        protected void FillWaybillTable(String[] fieldNames, PXSYTable table, WAYBILL[] waybills)
        {
            if (waybills == null)
            {
                return;
            }

            for (int i = 0; i < waybills.Length; i++)
            {
                var wb = waybills[i];

                var row = new PXSYRow(table);

                for (int j = 0; j < fieldNames.Length; j++)
                {
                    PXSYItem item = GetPXSYItem(fieldNames[j], wb);

                    if (item != null)
                    {
                        row.SetItem(fieldNames[j], item);
                    }
                }

                table.Add(row);
            }
        }

        protected PXSYItem GetPXSYItem(string fieldName, WAYBILL wb, int wbItemIndex = 0)
        {
            PXSYItem item = null;

            #region Field Mapping

            switch (fieldName)
            {
                case "WaybillID":
                    item = new PXSYItem(wb.ID);
                    break;
                case "WaybillNbr":
                    item = new PXSYItem(wb.WaybillNbr);
                    break;
                case "WaybillType":
                    item = new PXSYItem(wb.TYPE);
                    break;
                case "ParentWaybillID":
                    item = new PXSYItem(wb.PAR_ID);
                    break;
                case "WaybillNumber":
                    item = new PXSYItem(wb.WAYBILL_NUMBER);
                    break;
                case "WaybillStatus":
                    item = new PXSYItem(wb.STATUS);
                    break;
                case "WaybillState":
                    item = new PXSYItem(wb.IS_CONFIRMED);
                    break;
                case "WaybillCreateDate":
                    item = new PXSYItem(wb.CREATE_DATE);
                    break;
                case "WaybillActivationDate":
                    item = new PXSYItem(wb.ACTIVATE_DATE);
                    break;
                case "WaybillCloseDate":
                    item = new PXSYItem(wb.CLOSE_DATE);
                    break;
                case "WaybillCorrectionDate":
                    item = new PXSYItem(wb.CORRECTION_DATE);
                    break;
                case "WaybillCategory":
                    item = new PXSYItem("0");
                    break;
                case "RecipientName":
                    item = new PXSYItem(wb.BUYER_NAME);
                    break;
                case "RecipientTaxRegistrationID":
                    item = new PXSYItem(wb.BUYER_TIN);
                    break;
                case "RecipientIsForeignCitizen": //TODO: check logic for RecipientIsForeignCitizen
                    item = new PXSYItem((wb.CHEK_BUYER_TIN != "1").ToString());
                    break;
                case "RecipientInfo":
                    item = new PXSYItem(wb.RECEIVER_INFO);
                    break;
                case "RecipientStatus":
                    item = new PXSYItem("0");
                    break;
                case "SourceAddress":
                    item = new PXSYItem(wb.START_ADDRESS);
                    break;
                case "DestinationAddress":
                    item = new PXSYItem(wb.END_ADDRESS);
                    break;
                case "ShippingCost":
                    item = new PXSYItem(wb.TRANSPORT_COAST.ToDecimalRS()?.ToString());
                    break;
                case "SupplierTaxRegistrationID":
                    item = new PXSYItem(wb.SELLER_TIN);
                    break;
                case "SupplierName":
                    item = new PXSYItem(wb.SELLER_NAME);
                    break;
                case "SupplierInfo":
                    item = new PXSYItem(wb.RECEPTION_INFO);
                    break;
                case "DriverIsForeignCitizen": //TODO: check logic for DriverIsForeignCitizen
                    item = new PXSYItem((wb.CHEK_DRIVER_TIN != "1").ToString());
                    break;
                case "DriverName":
                    item = new PXSYItem(wb.DRIVER_NAME);
                    break;
                case "DriverUID":
                    item = new PXSYItem(wb.DRIVER_TIN);
                    break;
                case "Trailer":
                    item = new PXSYItem(wb.Trailer);
                    break;
                case "CarRegistrationNumber":
                    item = new PXSYItem(wb.CAR_NUMBER);
                    break;
                case "TransStartDate":
                    DateTime? transDate = null;
                    if (!string.IsNullOrEmpty(wb.BEGIN_DATE))
                    {
                        var transStartDate = DateTime.Parse(wb.BEGIN_DATE);

                        transDate = transStartDate.Date;
                    }
                    item = new PXSYItem(transDate.HasValue ? transDate.Value.ToString("d") : null);
                    break;
                case "TransStartTime":
                    TimeSpan? transTime = null;
                    if (!string.IsNullOrEmpty(wb.BEGIN_DATE))
                    {
                        var transStartDate = DateTime.Parse(wb.BEGIN_DATE);

                        transTime = transStartDate.TimeOfDay;
                    }
                    item = new PXSYItem(transTime.HasValue ? transTime.Value.ToString("t") : null);
                    break;
                case "ShippingCostPayer":
                    item = new PXSYItem(wb.TRAN_COST_PAYER);
                    break;
                case "TransportationType":
                    item = new PXSYItem(wb.TRANS_ID);
                    break;
                case "OtherTransportationTypeDescription":
                    item = new PXSYItem(wb.OtherTransText);
                    break;
                case "DeliveryDate":
                    DateTime? diliverDate = null;
                    if (!string.IsNullOrEmpty(wb.DELIVERY_DATE))
                    {
                        var deliveryDate = DateTime.Parse(wb.DELIVERY_DATE);

                        diliverDate = deliveryDate.Date;
                    }
                    item = new PXSYItem(diliverDate.HasValue ? diliverDate.Value.ToString("d") : null);
                    break;
                case "DeliveryTime":
                    TimeSpan? deliveryTime = null;
                    if (!string.IsNullOrEmpty(wb.DELIVERY_DATE))
                    {
                        var deliveryDate = DateTime.Parse(wb.DELIVERY_DATE);

                        deliveryTime = deliveryDate.TimeOfDay;
                    }
                    item = new PXSYItem(deliveryTime.HasValue ? deliveryTime.Value.ToString("t") : null);
                    break;
                case "Comment":
                    item = new PXSYItem(wb.COMMENT);
                    break;
                case "CarrierTaxRegistrationID":
                    item = new PXSYItem(wb.TRANSPORTER_TIN);
                    break;
                case "CarrierInfo":
                    item = new PXSYItem(wb.TRANSPORTER_NAME);
                    break;
                case "RSReceivedWaybillItem__WaybillItemNbr":
                case "RSWaybillItemHistory__WaybillItemNbr":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].WaybillItemNbr.ToString());
                    break;
                case "RSReceivedWaybillItem__ItemRowID":
                case "RSWaybillItemHistory__ItemRowID":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].ID);
                    break;
                case "RSReceivedWaybillItem__ItemRowStatus":
                case "RSWaybillItemHistory__ItemRowStatus":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].STATUS);
                    break;
                case "RSReceivedWaybillItem__ItemName":
                case "RSWaybillItemHistory__ItemName":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].W_NAME);
                    break;
                case "RSReceivedWaybillItem__ItemCode":
                case "RSWaybillItemHistory__ItemCode":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].BAR_CODE);
                    break;
                case "RSReceivedWaybillItem__ExtUOM":
                case "RSWaybillItemHistory__ExtUOM":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].UNIT_ID);
                    break;
                case "RSReceivedWaybillItem__OtherUOMDescription":
                case "RSWaybillItemHistory__OtherUOMDescription":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].UNIT_TXT);
                    break;
                case "RSReceivedWaybillItem__ItemQty":
                case "RSWaybillItemHistory__ItemQty":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].QUANTITY.ToDecimalRS()?.ToString());
                    break;
                case "RSReceivedWaybillItem__ItemExtraQty":
                case "RSWaybillItemHistory__ItemExtraQty":
                    item = new PXSYItem("0");
                    break;
                case "RSReceivedWaybillItem__UnitPrice":
                case "RSWaybillItemHistory__UnitPrice":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].PRICE.ToDecimalRS()?.ToString());
                    break;
                case "RSReceivedWaybillItem__ItemAmt":
                case "RSWaybillItemHistory__ItemAmt":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].AMOUNT.ToDecimalRS()?.ToString());
                    break;
                case "RSReceivedWaybillItem__ExciseID":
                case "RSWaybillItemHistory__ExciseID":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].A_ID);
                    break;
                case "RSReceivedWaybillItem__TaxType":
                case "RSWaybillItemHistory__TaxType":
                    item = new PXSYItem(wb.GOODS_LIST[wbItemIndex].VAT_TYPE);
                    break;
                default:
                    break;
            }

            #endregion

            return item;
        }

        protected void GetReceivedTaxInvoiceDates(out DateTime startDate, out DateTime endDate, out DateTime canceledStartDate)
        {
            endDate = DateTime.Now;
            startDate = endDate.AddHours(-24);
            canceledStartDate = endDate.AddDays(-30);

            var parameters = GetParameters();

            if (IEnumerableExtensions.IsNullOrEmpty(parameters))
            {
                return;
            }

            string val = null;

            Func<string, bool> tryGetValue = (string name) =>
            {
                val = parameters.FirstOrDefault(x => x.Name == name)?.Value;

                return !string.IsNullOrEmpty(val);
            };

            if (tryGetValue(ReceivedTaxInvoiceSyncStartDate))
            {
                startDate = val.ToDateTime().Value;

                if (tryGetValue(ReceivedTaxInvoiceSyncEndDate))
                {
                    endDate = val.ToDateTime().Value;
                }
            }
            else if (tryGetValue(ReceivedTaxInvoiceSyncHours))
            {
                startDate = endDate.AddHours(-int.Parse(val));
            }

            if (tryGetValue(ReceivedTaxInvoiceCanceledSyncDays))
            {
                canceledStartDate = startDate.AddDays(-int.Parse(val));
            }
        }

        protected void GetReceivedWaybillDates(out DateTime startDate, out DateTime endDate, out DateTime canceledStartDate)
        {
            endDate = DateTime.Now;
            startDate = endDate.AddHours(-24);
            canceledStartDate = endDate.AddDays(-30);

            var parameters = GetParameters();

            if (IEnumerableExtensions.IsNullOrEmpty(parameters))
            {
                return;
            }

            string val = null;

            Func<string, bool> tryGetValue = (string name) =>
            {
                val = parameters.FirstOrDefault(x => x.Name == name)?.Value;

                return !string.IsNullOrEmpty(val);
            };

            if (tryGetValue(ReceivedWaybillSyncStartDate))
            {
                startDate = val.ToDateTime().Value;

                if (tryGetValue(ReceivedWaybillSyncEndDate))
                {
                    endDate = val.ToDateTime().Value;
                }
            }
            else if (tryGetValue(ReceivedWaybillSyncHours))
            {
                startDate = endDate.AddHours(-int.Parse(val));
            }

            if (tryGetValue(ReceivedWaybillCanceledSyncDays))
            {
                canceledStartDate = startDate.AddDays(-int.Parse(val));
            }
        }

        protected void GetSentTaxInvoiceDate(out DateTime createDate)
        {
            createDate = DateTime.Now;

            var parameters = GetParameters();
            if (IEnumerableExtensions.IsNullOrEmpty(parameters))
            {
                return;
            }

            string val = null;

            Func<string, bool> tryGetValue = (string name) =>
            {
                val = parameters.FirstOrDefault(x => x.Name == name)?.Value;

                return !string.IsNullOrEmpty(val);
            };

            if (tryGetValue(SentTaxInvoiceSyncDays))
            {
                createDate = createDate.AddDays(-int.Parse(val));
            }

        }


        protected void GetWaybillDates(out DateTime startDate, out DateTime endDate)
        {
            endDate = DateTime.Now;
            //Defaults: 30 days period
            startDate = endDate.AddDays(-30);

            var parameters = GetParameters();

            if (IEnumerableExtensions.IsNullOrEmpty(parameters))
            {
                return;
            }

            string val = null;

            Func<string, bool> tryGetValue = (string name) =>
            {
                val = parameters.FirstOrDefault(x => x.Name == name)?.Value;

                return !string.IsNullOrEmpty(val);
            };

            if (tryGetValue(WaybillSyncStartDate))
            {
                startDate = val.ToDateTime().Value;

                if (tryGetValue(WaybillSyncEndDate))
                {
                    endDate = val.ToDateTime().Value;
                }
            }
            else if (tryGetValue(WaybillSyncDays))
            {
                startDate = endDate.AddDays(-int.Parse(val));
            }
        }


        #endregion

        #region Export

        public void Export(String objectName, PXSYTable table, bool breakOnError, Action<SyProviderRowResult> callback)
        {
            throw new NotImplementedException();
        }

        public void GetWaybillParameters(ref string url, ref string login, ref string pwd)
        {
            var graph = PXGraph.CreateInstance<RSSetupMaint>();
            var setup = PXSelect<RSSetup>.Select(graph).FirstTableItems;
            login = setup.First().WaybillAccount;
            url = setup.First().WaybillUrl;
            pwd = setup.First().WaybillLicence;
        }

        public void GetTaxInvoiceServiceParameters(ref string url, ref string login, ref string pwd)
        {
            var graph = PXGraph.CreateInstance<RSSetupMaint>();
            var setup = PXSelect<RSSetup>.Select(graph).FirstTableItems;
            login = setup.First().TaxInvoiceAccount;
            url = setup.First().TaxInvoiceUri;
            pwd = setup.First().TaxInvoiceLicence;

        }

        #endregion
    }
}
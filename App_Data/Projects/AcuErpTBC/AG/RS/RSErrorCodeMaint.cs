using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;

namespace AG.RS
{
    public class RSErrorCodeMaint : AGGraph<RSErrorCodeMaint, RSErrorCode>
    {
        public PXSelect<RSErrorCode> ErrorCodes;
    }
}
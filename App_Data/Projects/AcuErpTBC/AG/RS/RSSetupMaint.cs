using System;
using System.Collections;
using System.Collections.Generic;

using PX.Data;
using AG.RS.DAC;
using AG.RS.Descriptor;


namespace AG.RS
{

    public class RSSetupMaint : AGGraph<RSSetupMaint>
    {
        public PXSelect<RSSetup> Setup;
        public PXSave<RSSetup> Save;
        public PXCancel<RSSetup> Cancel;

        public PXAction<RSSetup> waybillTest;
        [PXUIField(DisplayName = Messages.TestConnection)]
        [PXButton(CommitChanges = true,ImageKey = PX.Web.UI.Sprite.Main.World)]
        public void WaybillTest()
        {
            string msg = string.Format(Descriptor.Messages.ConnectionToWaybillSuccessfull);
            try
            {
                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    if (context.CheckServiceUser())
                        Setup.Ask(Setup.Current, "Setup", Messages.ConnectionToWaybillSuccessfull, MessageButtons.OK, MessageIcon.Information);
                    else throw new PXException(Messages.ConnectionToWaybillFailed);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == msg)
                    throw;
                else throw new PXException(string.Format("{0} - {1}", Messages.ConnectionToWaybillFailed, ex.Message));
            }

        }

        public PXAction<RSSetup> taxInvoiceTest;
        [PXUIField(DisplayName = Messages.TestConnection)]
        [PXButton(CommitChanges = true,ImageKey = PX.Web.UI.Sprite.Main.World)]
        public void TaxInvoiceTest()
        {
            string msg = string.Format(Descriptor.Messages.ConnectionToTaxInvoiceSuccessfull);
            try
            {
                using (var context = new global::RS.Services.Implementation.TaxInvoiceService(Setup.Current.TaxInvoiceAccount,Setup.Current.TaxInvoiceLicence, Setup.Current.TaxInvoiceUri, Setup.Current.TaxInvoiceTimeout))
                {
                    if (context.check())
                        Setup.Ask(Setup.Current, "Setup", msg, MessageButtons.OK, MessageIcon.Information);
                    else throw new PXException(Messages.ConnectionToTaxInvoiceFailed);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == msg)
                    throw;
                else throw new PXException(string.Format("{0} - {1}", Messages.ConnectionToTaxInvoiceFailed, ex.Message));
            }
        }

        public static bool TaxInvocieIsActive(PXGraph graph)
        {
            
            RSSetup RSSetup = PXSelect<RSSetup>.Select(graph);
            if (RSSetup != null && RSSetup.TaxInvoiceIsActive == true)
            {
                return true;
            }

            return false;
        }

        public static bool WaybillIsActive(PXGraph graph)
        {
            //if (!PXAccess.FeatureInstalled<FeaturesSet.GE>()) \\TODO
            //    return false;

            RSSetup RSSetup = PXSelect<RSSetup>.Select(graph);
            if (RSSetup != null && RSSetup.WaybillIsActive == true)
            {
                return true;
            }

            return false;
        }
    }
}
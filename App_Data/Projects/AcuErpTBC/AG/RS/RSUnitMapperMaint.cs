using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;


namespace AG.RS
{
    public class RSUnitMapperMaint : AGGraph<RSUnitMapperMaint>
    {
        public PXSelect<RSUOMMapper> Mapper;
        public PXSave<RSUOMMapper> Save;
        public PXCancel<RSUOMMapper> Cancel;

        protected virtual void RSUOMMapper_Unit_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSUOMMapper)e.Row;
            foreach (RSUOMMapper line in Mapper.Select())
            {
                if (row.UomID == line.UomID)
                    continue;
                if (row.Unit == line.Unit)
                    throw new PXException("System UOM already exists!");
            }
        }
    }
}
﻿using System;
using System.Collections;
using PX.Data;
using PX.Objects.PO;
using AG.RS.DAC;
using System.Linq;
using PX.Objects.AP;
using PX.Objects.IN;
using AG.RS.Helpers;
using static AG.RS.Shared.RecivedTaxInvoiceStatusAttribute;
using AG.RS.Shared;

namespace AG.RS
{
    public class ReceivedTaxInvoiceEntry : AGGraph<ReceivedTaxInvoiceEntry, RSReceivedTaxInvoice>
    {
        #region Data Views

        public PXSelectJoin<RSReceivedTaxInvoice,
            LeftJoin<RSReceivedTaxInvoiceCorrected, On<RSReceivedTaxInvoice.taxInvoiceNbr,
                Equal<RSReceivedTaxInvoiceCorrected.correctedRefNbr>>>>
                    ReceivedTaxInvoice;

        public PXSetup<RSSetup> RSSetup;

        public PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>> InventoryItems;

        public PXSelect<RSReceivedTaxInvoiceItem,
            Where<RSReceivedTaxInvoiceItem.itemRowStatus, NotEqual<AG.RS.DAC.RSTaxInvoiceItem.RSTaxInvoiceItemStatuses.deleted>,
                And<RSReceivedTaxInvoiceItem.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoice.taxInvoiceNbr>>>>> ReceivedTaxInvoiceItems;

        public PXSelectJoin<RSReceivedTaxInvoiceBill,
             InnerJoin<APInvoice, On<APInvoice.docType, Equal<RSReceivedTaxInvoiceBill.invoiceDocType>,
             And<APInvoice.refNbr, Equal<RSReceivedTaxInvoiceBill.aPInvoiceNbr>>>,
                 LeftJoin<APTran, On<APTran.refNbr, Equal<APInvoice.refNbr>>,
                     LeftJoin<POReceipt, On<POReceipt.receiptNbr, Equal<APTran.receiptNbr>>>
                     >>,

             Where<RSReceivedTaxInvoiceBill.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoiceBill.taxInvoiceNbr>>>> BillsAndAdjustments;

        public PXSelectJoin<RSReceivedTaxInvoiceWaybill, LeftJoin<RSReceivedWaybill,
                On<RSReceivedTaxInvoiceWaybill.wayBillNbr, Equal<RSReceivedWaybill.waybillNbr>>>,
                Where<RSReceivedTaxInvoiceWaybill.taxInvoiceNbr,
                Equal<Current<RSReceivedTaxInvoice.taxInvoiceNbr>>>> ReceivedTaxInvoiceWaybills;

        public PXSelect<RSUnit> UOMs;

        #endregion

        #region Constructor

        public ReceivedTaxInvoiceEntry()
        {
            var cur = RSSetup.Current;
            actionsMenu.AddMenuAction(confirmTaxInvoice);
            actionsMenu.AddMenuAction(rejectTaxInvoice);
            actionsMenu.AddMenuAction(CreateBill);
        }

        #endregion

        #region Page Event Handlers

        protected virtual void RSReceivedTaxInvoice_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        protected virtual void RSReceivedTaxInvoice_BranchID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                e.NewValue = Accessinfo.BranchID;
                row.BranchID = Accessinfo.BranchID;
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        protected virtual void RSReceivedTaxInvoice_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSReceivedTaxInvoice)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.initialTaxInvoiceNbr>(sender, row, row.TaxInvoiceStatus == ExtStatus.Corrected);
            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.initialTaxInvoiceID>(sender, row, false);
            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.correctionType>(sender, row, row.TaxInvoiceStatus == ExtStatus.Corrected);
            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.correctionDate>(sender, row, row.TaxInvoiceStatus == ExtStatus.Corrected);

            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.taxInvoiceNumber>(sender, row, false);
            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.taxInvoiceSeries>(sender, row, false);
            PXUIFieldAttribute.SetVisible<RSReceivedTaxInvoice.requestDate>(sender, row, false);

            confirmTaxInvoice.SetEnabled(row.CanConfirmTaxInvoice());
            rejectTaxInvoice.SetEnabled(row.CanRejectTaxInvoice());
            //TaxInvoiceCorrectAction.SetEnabled(ReceivedTaxInvoice.Current.CanCreateBillAndCorrect());
            CreateBill.SetEnabled(row.CanCreateBillAndCorrect());
            SetDisplayNameInventoryGridCol(row);

            ReceivedTaxInvoice.Cache.AllowDelete = false;
        }

        protected virtual void RSReceivedTaxInvoice_Hold_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = RSSetup.Current.TaxInvoiceHoldOnEntry;
        }

        protected virtual void RSReceivedTaxInvoice_Status_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = RSSetup.Current.TaxInvoiceHoldOnEntry == true ? WaybillStatus.Hold : WaybillStatus.Open;
        }

        #endregion

        #region Buttons

        public PXAction<RSReceivedTaxInvoice> prepaymentTaxInvoiceAction;
        [PXUIField(DisplayName = "Text")]
        [PXButton()]
        protected virtual IEnumerable PrepaymentTaxInvoiceAction(PXAdapter adapter)
        {
            var row = adapter.Get<RSReceivedTaxInvoice>().FirstOrDefault();

            SetDisplayNameInventoryGridCol(row);

            return adapter.Get();
        }

        public PXAction<RSReceivedTaxInvoice> DeleteTaxInvoiceItems;
        [PXButton]
        protected virtual void deleteTaxInvoiceItems()
        {
            var inserted = ReceivedTaxInvoiceItems.Cache.Inserted.RowCast<RSReceivedTaxInvoiceItem>();
            var updated = ReceivedTaxInvoiceItems.Cache.Updated.RowCast<RSReceivedTaxInvoiceItem>();
            RSReceivedTaxInvoiceItem row;

            if (inserted.Count() > 0)
                row = inserted.FirstOrDefault();
            else
                row = updated?.FirstOrDefault();


            if (row == null)
                return;

            var RowId = row.RowID;
            var ItemRowId = row.ItemRowID;

            //პირველივე აითემის დამატების/განახლების დროს ვშლით ყლველას
            //რადგან არ მოხდეს დუბლირება აითემების
            if (RowId == 1)
                foreach (var item in ReceivedTaxInvoiceItems.Select())
                {
                    if (((RSReceivedTaxInvoiceItem)item).ItemRowID != ItemRowId)
                        ReceivedTaxInvoiceItems.Delete(item);
                }
        }

        public PXAction<RSReceivedTaxInvoice> TaxInvoiceCorrectAction;
        [PXButton()]
        [PXUIField(DisplayName = "Correct Action")]
        protected virtual void taxInvoiceCorrectAction()
        {

            var Current = ReceivedTaxInvoice.Current;

            if (Current != null)
            {
                RSReceivedTaxInvoice PrevTaxInvoice = PXSelectReadonly<RSReceivedTaxInvoice,
                Where<RSReceivedTaxInvoice.taxInvoiceID, Equal<Required<RSReceivedTaxInvoice.taxInvoiceID>>
                    // And<RSReceivedTaxInvoice.taxInvoiceNumber, Equal<Required<RSReceivedTaxInvoice.taxInvoiceNumber>>
                    //Get rows except itself
                    , And<RSReceivedTaxInvoice.taxInvoiceNbr, NotEqual<Required<RSReceivedTaxInvoice.taxInvoiceNbr>>
                    >>,
                    OrderBy<Desc<RSReceivedTaxInvoice.createdDateTime>>>
                .Select(this, Current.InitialTaxInvoiceID, Current.TaxInvoiceNbr).FirstTableItems.FirstOrDefault();


                //determine existance of the recived tax invoice document
                if (PrevTaxInvoice != null)
                {
                    if (PrevTaxInvoice.Status == Status.Approved)
                    {
                        ReceivedTaxInvoiceEntry g = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();
                        PrevTaxInvoice.Status = Status.Corrected;
                        //შესამოწმებელია მოხვდება თუ არა პირველადი დოკუმენტი დასასინქრონიზებელ სიაში
                        //თუ არა მაშინ აქ უნდა შევცვალოთ რს -ის სტატუსი
                        PrevTaxInvoice.TaxInvoiceStatus = ExtStatus.Initial;

                        Current.CorrectedRefNbr = PrevTaxInvoice.TaxInvoiceNbr;
                        using (PXConnectionScope cs = new PXConnectionScope())
                        {
                            using (PXTransactionScope TScope = new PXTransactionScope())
                            {

                                g.ReceivedTaxInvoice.Cache.Update(PrevTaxInvoice);
                                g.ReceivedTaxInvoice.Cache.Update(Current);

                                g.ReceivedTaxInvoice.Cache.Persist(PXDBOperation.Update);
                                g.ReceivedTaxInvoice.Cache.Persisted(false);

                                TScope.Complete(g);
                            }
                        }
                    }
                }
            }
        }

        public PXAction<RSReceivedTaxInvoice> ViewPrevTaxInvoice;
        [PXButton]
        protected virtual void viewPrevTaxInvoice()
        {
            var current = ReceivedTaxInvoice.Current;
            if (current != null)
            {

                ViewTaxInvoice(current.CorrectedRefNbr);

            }
        }

        public PXAction<RSReceivedTaxInvoice> ViewNextTaxInvoice;
        [PXButton]
        protected virtual void viewNextTaxInvoice()
        {
            var current = ReceivedTaxInvoice.Current;
            if (current != null)
            {
                var JoinedCorrectedDAC = PXSelectReadonly<RSReceivedTaxInvoiceCorrected, Where<RSReceivedTaxInvoiceCorrected.correctedRefNbr, Equal<Current<RSReceivedTaxInvoice.taxInvoiceNbr>>>>.Select(this).FirstTableItems.FirstOrDefault();
                if (JoinedCorrectedDAC == null)
                    return;

                ViewTaxInvoice(JoinedCorrectedDAC.TaxInvoiceNbr);
            }
        }

        private void ViewTaxInvoice(string TaxInvoiceNbr)
        {
            ReceivedTaxInvoiceEntry graph = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();

            var result = PXSelectReadonly<RSReceivedTaxInvoice,
                               Where<RSReceivedTaxInvoice.taxInvoiceNbr,
                               Equal<Required<RSReceivedTaxInvoice.taxInvoiceNbr>>>>.Select(graph, TaxInvoiceNbr).FirstTableItems;

            graph.ReceivedTaxInvoice.Current = result.FirstOrDefault();

            if (graph.ReceivedTaxInvoice.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.New };
            }
        }

        public PXAction<RSReceivedTaxInvoice> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        public PXAction<RSReceivedTaxInvoice> confirmTaxInvoice;
        [PXUIField(DisplayName = "Accept TaxInvoice", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable ConfirmTaxInvoice(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    ConfirmTI();
                    PXProcessing<RSReceivedTaxInvoice>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSReceivedTaxInvoice> rejectTaxInvoice;
        [PXUIField(DisplayName = "Reject TaxInvoice", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable RejectTaxInvoice(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    RejectTI();
                    PXProcessing<RSReceivedTaxInvoice>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSReceivedTaxInvoice> CreateBill;
        [PXProcessButton]
        [PXUIField(DisplayName = "Create Bill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        protected void createBill()
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                var current = ReceivedTaxInvoice.Current;
                if (current != null)
                {
                    APInvoiceEntry bill = PXGraph.CreateInstance<APInvoiceEntry>();
                    var api = bill.Document.Insert();
                    api.CreatedDateTime = current.CreatedDateTime;
                    api.FinPeriodID = current.PostPeriod;
                    bill.Document.Current = api;
                    bill.Caches<APInvoice>().SetValueExt<APInvoice.vendorID>(api, current.VendorID);

                    APInvoiceEntry_Extension invExt = bill.GetExtension<APInvoiceEntry_Extension>();
                    RSAPBillReceivedTaxInvoice tax = new RSAPBillReceivedTaxInvoice();
                    tax.TaxInvoiceID = current.TaxInvoiceID;
                    tax.TaxInvoiceStatus = current.Status;
                    invExt.ReceivedTaxInvoices.Cache.Insert(tax);


                    APTran billTran;

                    foreach (RSReceivedTaxInvoiceItem item in ReceivedTaxInvoiceItems.Select())
                    {
                        if (item.InventoryID != null)
                        {
                            billTran = bill.Transactions.Insert();
                            billTran.InventoryID = item.InventoryID;
                            billTran.TranDesc = item.ItemName;

                            RSUOMMapper uom = PXSelect<RSUOMMapper, Where<RSUOMMapper.rSUOMID, Equal<Required<RSUOMMapper.rSUOMID>>>>.Select(this, item.ExtUOM);

                            if (uom != null)
                            {
                                var itemUom = PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                                                                    And<Where<InventoryItem.baseUnit, Equal<Required<InventoryItem.baseUnit>>,
                                                                         Or<InventoryItem.salesUnit, Equal<Required<InventoryItem.salesUnit>>,
                                                                             Or<InventoryItem.purchaseUnit, Equal<Required<InventoryItem.purchaseUnit>>>>>>>>.Select(this, item.InventoryID, uom.Unit, uom.Unit, uom.Unit);
                                if (itemUom.Count > 0)
                                    bill.Caches<APTran>().SetValueExt<APTran.uOM>(billTran, uom.Unit);
                            }

                            bill.Caches<APTran>().SetValue<APTran.curyUnitCost>(billTran, item.UnitPrice);
                            bill.Caches<APTran>().SetValue<APTran.qty>(billTran, item.ItemQty);
                            bill.Transactions.Update(billTran);
                        }

                    }

                    throw new PXRedirectRequiredException(bill, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.InlineWindow };
                }
            });
        }

        #endregion

        #region Other

        private void SetDisplayNameInventoryGridCol(RSReceivedTaxInvoice row)
        {
            if (row == null)
                return;

            PXCache c;
            if (Caches.TryGetValue(typeof(RSReceivedTaxInvoiceItem), out c))
            {
                var FieldDisplayName = (row.PrepaymentTaxInvoice.HasValue && row.PrepaymentTaxInvoice.Value) ?
                        Descriptor.Messages.ReceivedTaxInvoicFullAmtDisplayName
                        : Descriptor.Messages.ReceivedTaxInvoicFullAmtDefaultDisplayName;


                PXUIFieldAttribute.SetDisplayName<RSReceivedTaxInvoiceItem.fullAmt>(c, FieldDisplayName);
            }
        }

        public void RejectTI()
        {
            var taxInvoice = ReceivedTaxInvoice.Current;
            using (var context = new global::RS.Services.Implementation.TaxInvoiceService(RSSetup.Current.TaxInvoiceAccount, RSSetup.Current.TaxInvoiceLicence, RSSetup.Current.TaxInvoiceUri, RSSetup.Current.TaxInvoiceTimeout))
            {
                try
                {

                    bool rejected = context.ref_invoice_status(taxInvoice.TaxInvoiceID.GetValueOrDefault(), string.Empty);
                    if (rejected)
                    {
                        taxInvoice.Status = Status.Rejected;
                        taxInvoice.TaxInvoiceStatus = ExtStatus.Rejected;
                        taxInvoice.ConfirmDate = DateTime.Now;
                        taxInvoice.ConfirmedByID = Accessinfo.UserID;
                        ReceivedTaxInvoice.Update(taxInvoice);
                        Actions.PressSave();
                    }
                    else
                    {
                        throw new PXException(AG.RS.Descriptor.Messages.CanNotReject);
                    }
                }
                catch (Exception e)
                {
                    throw new PXException(e.Message);
                }

            }

        }

        public void ConfirmTI()
        {
            var taxInvoice = ReceivedTaxInvoice.Current;

            if (!taxInvoice.CanConfirmTaxInvoice())
            {
                throw new PXException(AG.RS.Descriptor.Messages.CanNotConfirm);
            }

            var vendorExists = PXGraphHelpers.GetVendorIDByTaxRegistrationID(this, taxInvoice.VendorTaxRegistrationID).HasValue;

            if (!vendorExists)
            {
                throw new PXException(AG.RS.Descriptor.Messages.VendorDoesNotExist);
            }

            using (var context = new global::RS.Services.Implementation.TaxInvoiceService(RSSetup.Current.TaxInvoiceAccount, RSSetup.Current.TaxInvoiceLicence, RSSetup.Current.TaxInvoiceUri, RSSetup.Current.TaxInvoiceTimeout))
            {
                try
                {
                    var taxInvoiceStatus = TaxInvoiceStatusAttribute.SentTaxInvoiceStatuses.Confirmed;
                    if (!string.IsNullOrEmpty(taxInvoice.CorrectedRefNbr))
                        taxInvoiceStatus = TaxInvoiceStatusAttribute.SentTaxInvoiceStatuses.CorrectedConfirmed;

                    bool confirmed = context.accept_invoice_status(taxInvoice.TaxInvoiceID.GetValueOrDefault(), taxInvoiceStatus);
                    if (confirmed)
                    {
                        taxInvoice.Status = Status.Approved;
                        taxInvoice.TaxInvoiceStatus = ExtStatus.Confirmed;
                        taxInvoice.ConfirmDate = DateTime.Now;
                        taxInvoice.ConfirmedByID = Accessinfo.UserID;
                        ReceivedTaxInvoice.Update(taxInvoice);
                        Actions.PressSave();
                    }
                    else
                    {
                        throw new PXException(AG.RS.Descriptor.Messages.CanNotConfirm);
                    }
                }
                catch (Exception e)
                {
                    throw new PXException(e.Message);
                }

            }
        } 

        #endregion
    }
}
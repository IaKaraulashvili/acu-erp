using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.SO;
using PX.Objects.GL;
using PX.Objects.AP;
using PX.Objects.AR;
using AG.RS.Helpers;
using PX.Objects.CR;
using System.Linq;

namespace AG.RS
{
    public class ReceivedTaxInvoiceRequestEntry : AGGraph<ReceivedTaxInvoiceRequestEntry, RSReceivedTaxInvoiceRequest>
    {

        #region DataViews

        public PXSelect<RSReceivedTaxInvoiceRequest> ReceivedTaxInvoiceRequest;

        public PXSetup<RSSetup> RSSetup;

        public PXSelectJoin<RSWaybill,
            InnerJoin<RSWaybillSource, On<RSWaybill.waybillNbr, Equal<RSWaybillSource.waybillNbr>>,
            InnerJoin<SOOrderShipment, On<SOOrderShipment.shipmentNbr, Equal<RSWaybillSource.sourceNbr>,
                                      And<RSWaybillSource.sourceType, Equal<RSWaybillSource.SourceTypes.shipment>>>,
            InnerJoin<ARInvoice, On<SOOrderShipment.invoiceType, Equal<ARInvoice.docType>,
                                      And<SOOrderShipment.invoiceNbr, Equal<ARInvoice.refNbr>>>,
            InnerJoin<RSReceivedTaxInvoiceRequestInvoice,On<ARInvoice.docType, Equal<RSReceivedTaxInvoiceRequestInvoice.invoiceDocType>,
                                And<ARInvoice.refNbr, Equal<RSReceivedTaxInvoiceRequestInvoice.invoiceNbr>>>>>>>,
            Where<RSReceivedTaxInvoiceRequestInvoice.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoiceRequest.taxInvoiceNbr>>>> Waybills;

        public PXSelectJoin<RSReceivedTaxInvoiceRequestInvoice,
            InnerJoin<ARInvoice, On<ARInvoice.docType, Equal<RSReceivedTaxInvoiceRequestInvoice.invoiceDocType>,
                                And<ARInvoice.refNbr, Equal<RSReceivedTaxInvoiceRequestInvoice.invoiceNbr>>>>,
            Where<RSReceivedTaxInvoiceRequestInvoice.taxInvoiceNbr, Equal<Current<RSReceivedTaxInvoiceRequest.taxInvoiceNbr>>>> InvoiceAndMemos;

        #endregion

        #region Constructor

        public ReceivedTaxInvoiceRequestEntry()
        {
            var cur = RSSetup.Current;
        }

        #endregion

        #region Page Events

        protected virtual void RSReceivedTaxInvoiceRequest_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var received = (RSReceivedTaxInvoiceRequest)e.Row;
            if (received.Hold == true)
            {
                received.Status = RSReceivedTaxInvoiceRequest.TaxInvoiceStatus.OnHold;
            }
            else
            {
                received.Status = RSReceivedTaxInvoiceRequest.TaxInvoiceStatus.Received;
            }

        }

        protected virtual void RSReceivedTaxInvoiceRequest_CustomerID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSReceivedTaxInvoiceRequest)e.Row;

            if (row != null)
            {
                string name = null;
                string taxRegID = null;

                if (row.CustomerID.HasValue)
                {
                    PXResult<Customer, PX.Objects.CR.Location> result = (PXResult<Customer, PX.Objects.CR.Location>)PXSelectJoin<Customer,
                        LeftJoin<PX.Objects.CR.Location, On<Customer.defLocationID, Equal<PX.Objects.CR.Location.locationID>>>,
                        Where<Customer.bAccountID, Equal<Required<RSReceivedTaxInvoiceRequest.customerID>>>>.Select(this, row.CustomerID);

                    Customer customer = result;

                    if (customer != null)
                    {
                        name = customer.AcctName;

                        PX.Objects.CR.Location location = result;

                        if (location != null)
                        {
                            taxRegID = location.TaxRegistrationID;
                        }
                    }
                }

                row.CustomerName = name;
                row.CustomerTaxRegistrationID = taxRegID;
            }
        }

        protected virtual void RSReceivedTaxInvoiceRequest_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }



        protected virtual void RSReceivedTaxInvoiceRequest_BranchID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                e.NewValue = Accessinfo.BranchID;
                row.BranchID = Accessinfo.BranchID;
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        protected virtual void RSReceivedTaxInvoiceRequest_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RSReceivedTaxInvoiceRequest row = (RSReceivedTaxInvoiceRequest)e.Row;
            if (row.CustomerID != null)
            {
                if (InvoiceAndMemos.View.SelectSingleBound(new object[] { e.Row }) != null)
                {
                    PXUIFieldAttribute.SetEnabled<RSReceivedTaxInvoiceRequest.customerID>(sender, row, false);
                }
                else
                {
                    PXUIFieldAttribute.SetEnabled<RSReceivedTaxInvoiceRequest.customerID>(sender, row, true);
                }
            }
        }

        #endregion


    }
}
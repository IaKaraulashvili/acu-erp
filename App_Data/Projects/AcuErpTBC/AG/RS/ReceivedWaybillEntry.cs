﻿using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using PX.Objects.IN;
using global::RS.Services.Domain;
using AG.RS.DAC;
using System.Linq;
using AG.RS.Helpers;
using PX.Objects.PO;
using AG.RS.Shared;

namespace AG.RS
{
    public class ReceivedWaybillEntry : AGGraph<ReceivedWaybillEntry, RSReceivedWaybill>
    {
        #region DataViews

        public PXSelect<RSReceivedWaybill> Waybills;

        public PXSelect<RSReceivedWaybill,
            Where<RSReceivedWaybill.waybillNbr, Equal<Current<RSReceivedWaybill.waybillNbr>>>> WaybillDetails;

        public PXSelect<RSReceivedWaybillItem,
            Where<RSReceivedWaybillItem.itemRowStatus, NotEqual<RSWaybillItemStatuses.deleted>,
                And<RSReceivedWaybillItem.waybillNbr, Equal<Current<RSReceivedWaybill.waybillNbr>>>>,
                OrderBy<Desc<RSWaybillItem.createdDateTime>>> WaybillItems;

        public PXSelectJoin<RSReceivedWaybill,
            InnerJoin<PX.Objects.PO.POReceipt, On<Current<RSReceivedWaybill.pOReceiptNbr>, Equal<PX.Objects.PO.POReceipt.receiptNbr>>>,
            Where<RSReceivedWaybill.waybillNbr, Equal<Current<RSReceivedWaybill.waybillNbr>>>> POReceipt;

        public PXSelect<InventoryItem, Where<InventoryItem.inventoryCD, Equal<Required<InventoryItem.inventoryCD>>>> InventoryItems;

        public PXSelectReadonly<RSWaybillHistory,
          Where<RSWaybillHistory.waybillNbr, Equal<Current<RSReceivedWaybill.waybillNbr>>, And<RSWaybillHistory.classID, Equal<WaybillClass.received>>>,
          OrderBy<Desc<RSWaybillHistory.createdDateTime>>> HistoryItems;

        public PXSelect<RSErrorCode> ErrorCodes;

        public PXSetup<RSSetup> Setup;
        public PXSelect<RSTransportType> TransportTypes;
        public PXSelect<RSUnit> UOMs;

        #endregion

        #region ctor

        public ReceivedWaybillEntry()
        {
            var cur = Setup.Current;

            actionsMenu.AddMenuAction(confirmWaybill);
            actionsMenu.AddMenuAction(rejectWaybill);
            reportsMenu.AddMenuAction(printWB);
        }

        #endregion

        #region Page Events

        protected virtual void RSReceivedWaybill_SupplierDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RSReceivedWaybill row = (RSReceivedWaybill)e.Row;
            if (row != null)
                row.DestinationAddress = row.SupplierDestinationAddress;
        }

        protected virtual void RSReceivedWaybill_RecipientDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RSReceivedWaybill row = (RSReceivedWaybill)e.Row;
            if (row != null)
                row.DestinationAddress = row.RecipientDestinationAddress;
        }

        protected virtual void RSReceivedWaybill_SourceAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSReceivedWaybill)e.Row;

            if (row != null)
            {
                if (row.WaybillType == WaybillType.WithoutTransporation && row.SourceAddress != row.DestinationAddress)
                {
                    sender.SetValueExt<RSReceivedWaybill.recipientDestinationAddress>(row, row.SourceAddress);
                }
            }
        }

        protected virtual void RSReceivedWaybill_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSReceivedWaybill)e.Row;

            PXGraphHelpers.SetWaybillStatus(row);
        }

        protected virtual void RSReceivedWaybill_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {
            var row = (RSReceivedWaybill)e.Row;
            if (row != null)
            {
                //row.DriverIsForeignCitizen = !row.DriverIsForeignCitizen;
                //row.RecipientIsForeignCitizen = !row.RecipientIsForeignCitizen;

                if (!string.IsNullOrEmpty(row.DestinationAddress))
                {
                    if (row.WaybillType == WaybillType.Internal && string.IsNullOrEmpty(row.SupplierDestinationAddress))
                        row.SupplierDestinationAddress = row.DestinationAddress;
                    else if (row.WaybillType != WaybillType.Internal && string.IsNullOrEmpty(row.RecipientDestinationAddress))
                        row.RecipientDestinationAddress = row.DestinationAddress;
                }
            }
        }

        protected virtual void RSReceivedWaybill_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSReceivedWaybill)e.Row;

            if (row == null) return;

            bool isInternal = row.WaybillType == WaybillType.Internal;
            bool isWithoutTrans = row.WaybillType == WaybillType.WithoutTransporation;
            bool isDistribution = row.WaybillType == WaybillType.Distribution;
            var isCarrierVehicle = row.TransportationType == TransportationType.Carrier_Vehicle;

            bool needDriverInfo =
                !isWithoutTrans
                && (
                    row.TransportationType == TransportationType.Vehicle
                    || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                    || isCarrierVehicle
                );

            var needShippingCostPayer =
                isInternal && row.TransportationType == TransportationType.Vehicle_ForeignCountry
                || (
                        row.WaybillType == WaybillType.Transporation
                        || row.WaybillType == WaybillType.Distribution
                        || row.WaybillType == WaybillType.Rtrn
                    )
                    && (
                        row.TransportationType == TransportationType.Vehicle
                        || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                        || isCarrierVehicle
                       );

            bool needCarrierInfo = !isWithoutTrans && isCarrierVehicle;

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.status>(sender, row, false);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.hold>(sender, row, false);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.isCorrected>(sender, row, row.IsCorrected.GetValueOrDefault());

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.driverUID>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.driverName>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.driverIsForeignCitizen>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.shippingCost>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.carRegistrationNumber>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.trailer>(sender, row, needDriverInfo);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.transportationType>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.otherTransportationTypeDescription>(sender, row, row.TransportationType == TransportationType.Other);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.supplierDestinationAddress>(sender, row, isInternal);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.shippingCostPayer>(sender, row, needShippingCostPayer);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.waybillCloseDate>(sender, row, row.WaybillCloseDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.waybillCancelDate>(sender, row, row.WaybillCancelDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.waybillCorrectionDate>(sender, row, row.WaybillCorrectionDate.HasValue);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.acceptedByID>(sender, row, row.AcceptedByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.acceptedDate>(sender, row, row.AcceptedDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.rejectedByID>(sender, row, row.RejectedByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.rejectedDate>(sender, row, row.RejectedDate.HasValue);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.carrierTaxRegistrationID>(sender, row, needCarrierInfo);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.carrierInfo>(sender, row, needCarrierInfo);

            // Hide Buyer (Recipient)
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.recipientIsForeignCitizen>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.recipientName>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.recipientTaxRegistrationID>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.recipientDestinationAddress>(sender, row, !isInternal && !isDistribution && !isWithoutTrans);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.transStartDate>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.transStartTime>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.deliveryDate>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.deliveryTime>(sender, row, !isWithoutTrans);

            PXUIFieldAttribute.SetVisible<RSReceivedWaybill.linkedWaybillNbr>(sender, row, isInternal);

            PXUIFieldAttribute.SetEnabled<RSReceivedWaybill.waybillType>(sender, row, sender.GetStatus(row) == PXEntryStatus.Inserted);

            confirmWaybill.SetEnabled(row.CanConfirm());
            rejectWaybill.SetEnabled(row.CanReject());

            Insert.SetVisible(false);
            Delete.SetVisible(false);
        }

        protected virtual void RSReceivedWaybill_Hold_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = Setup.Current.WaybillHoldOnEntry;
        }

        protected virtual void RSReceivedWaybill_Status_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = Setup.Current.WaybillHoldOnEntry == true ? WaybillStatus.Hold : WaybillStatus.Open;
        }

        #endregion

        #region Buttons

        public PXAction<RSReceivedWaybill> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        public PXAction<RSReceivedWaybill> confirmWaybill;
        [PXUIField(DisplayName = "Accept Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable ConfirmWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    ConfirmWB();
                    PXProcessing<RSReceivedWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSReceivedWaybill> rejectWaybill;
        [PXUIField(DisplayName = "Reject Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable RejectWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    RejectWB();
                    PXProcessing<RSReceivedWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }



        public PXAction<RSReceivedWaybill> reportsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Reports")]
        protected virtual void ReportsMenu()
        {

        }

        public PXAction<RSReceivedWaybill> printWB;
        [PXUIField(DisplayName = "Print Waybill", MapViewRights = PXCacheRights.Select, MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        public virtual IEnumerable PrintWB(PXAdapter adapter)
        {
            if (Waybills.Current != null)
            {
                Dictionary<string, string> info = new Dictionary<string, string>();
                info["WaybillNbr"] = Waybills.Current.WaybillNbr.ToString();

                throw new PXReportRequiredException(info, "RS602000", "Print Received Waybill");
            }
            return adapter.Get();
        }

        public PXAction<RSReceivedWaybill> viewPOReceipt;
        [PXUIField(DisplayName = Descriptor.Messages.ViewPOReceipt, MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select, Visible = true)]
        [PXLookupButton]
        public virtual IEnumerable ViewPOReceipt(PXAdapter adapter)
        {
            if (this.POReceipt.Current != null)
            {
                RSReceivedWaybill row = this.POReceipt.Current;
                var graph = PXGraph.CreateInstance<POReceiptEntry>();
                graph.Document.Current = graph.Document.Search<POReceipt.receiptNbr>(row.POReceiptNbr);
                throw new PXRedirectRequiredException(graph, true, PX.Objects.PO.Messages.Document) { Mode = PXBaseRedirectException.WindowMode.NewWindow };
            }
            return adapter.Get();
        }

        public PXAction<RSReceivedWaybill> viewHistory;
        [PXUIField(MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton]
        public virtual void ViewHistory()
        {
            var row = HistoryItems.Current;

            if (row != null)
            {
                var graph = PXGraph.CreateInstance<WaybillHistoryEntry>();

                graph.Waybills.Current = row;

                throw new PXPopupRedirectException(graph, "ViewHistory", true);
            }
        }

        #endregion

        #region Actions

        public void ConfirmWB()
        {
            var waybill = Waybills.Current;

            if (!waybill.CanConfirm())
            {
                throw new PXException(AG.RS.Descriptor.Messages.CanNotConfirm);
            }

            if (waybill.WaybillType != WaybillType.Internal)
            {
                var vendorExists = PXGraphHelpers.GetVendorIDByTaxRegistrationID(this, waybill.SupplierTaxRegistrationID).HasValue;

                if (!vendorExists)
                {
                    throw new PXException(AG.RS.Descriptor.Messages.VendorDoesNotExist);
                }
            }

            using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
            {
                context.CheckServiceUser();

                if (!context.ConfirmWaybill(int.Parse(waybill.WaybillID)))
                {
                    throw new PXException(AG.RS.Descriptor.Messages.CanNotConfirm);
                }

                UpdateWaybillState();
            }
        }

        public void RejectWB()
        {
            var waybill = Waybills.Current;

            if (!waybill.CanReject())
            {
                throw new PXException(AG.RS.Descriptor.Messages.CanNotReject);
            }

            using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
            {
                context.CheckServiceUser();

                if (!context.RejectWaybill(int.Parse(waybill.WaybillID)))
                {
                    throw new PXException(AG.RS.Descriptor.Messages.CanNotReject);
                }

                UpdateWaybillState();
            }
        }

        public void CorrectWB()
        {
            RSReceivedWaybill waybill = Waybills.Current;

            var waybillItems = WaybillItems.Select().FirstTableItems;

            ReceivedWaybillEntry graph = PXGraph.CreateInstance<ReceivedWaybillEntry>();

            var newWaybill = graph.Waybills.Insert();
            newWaybill.CopyFrom(waybill);

            graph.Waybills.Cache.SetDefaultExt<RSReceivedWaybill.hold>(newWaybill);
            graph.Waybills.Cache.SetDefaultExt<RSReceivedWaybill.status>(newWaybill);

            graph.Waybills.Current = newWaybill;

            foreach (var item in waybillItems)
            {
                var newItem = graph.WaybillItems.Insert();
                newItem.CopyFrom(item);
            }

            if (graph.Waybills.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.Same };
            }
        }

        #endregion

        #region Helper Methods

        protected WAYBILL GetExternalWB()
        {
            var waybillID = Waybills.Current.WaybillID;
            WAYBILL waybill = null;

            if (string.IsNullOrEmpty(waybillID))
            {
                return null;
            }

            try
            {
                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    waybill = context.GetWaybill(int.Parse(waybillID));
                }
            }
            catch (global::RS.Services.WaybillResultException ex)
            {
                throw new PXException(GetErrorText(ex.ErrorCode));
            }

            return waybill;
        }

        protected void UpdateWaybillState()
        {
            var extWB = GetExternalWB();

            if (extWB != null)
            {
                using (var tran = new PXTransactionScope())
                {
                    var waybill = Waybills.Current;

                    PXGraphHelpers.SetWaybillState(waybill, extWB.IS_CONFIRMED);

                    if (waybill.WaybillState == WaybillStates.Received)
                    {
                        waybill.AcceptedByID = Accessinfo.UserID;
                        waybill.AcceptedDate = DateTime.Now;
                    }
                    else if (waybill.WaybillState == WaybillStates.Rejected)
                    {
                        waybill.RejectedByID = Accessinfo.UserID;
                        waybill.RejectedDate = DateTime.Now;
                    }

                    Waybills.Update(waybill);

                    Actions.PressSave();

                    var graph = PXGraph.CreateInstance<WaybillEntry>();

                    var sentWB = PXSelect<RSWaybill, Where<RSWaybill.waybillID, Equal<Required<RSWaybill.waybillID>>>>
                        .Select(graph, waybill.WaybillID).FirstTableItems.FirstOrDefault();

                    if (sentWB != null)
                    {
                        graph.Waybills.Current = sentWB;

                        sentWB.WaybillState = waybill.WaybillState;

                        graph.Waybills.Update(sentWB);
                        graph.Actions.PressSave();
                    }

                    tran.Complete();
                }
            }
        }

        protected string GetErrorText(string statusID)
        {
            string errors = string.Empty;
            foreach (var error in ErrorCodes.Search<RSErrorCode.iD>(statusID).FirstTableItems)
            {
                errors += "; " + error.Name;
            }
            if (errors.Length > 2)
                errors = errors.Substring(2);
            return errors;
        }

        public RSReceivedWaybill GetByWaybillNumber(string waybillNumber)
        {
            return PXSelect<RSReceivedWaybill,
                Where<RSReceivedWaybill.waybillNumber, Equal<Required<RSReceivedWaybill.waybillNumber>>>>.Select(this, waybillNumber);
        }

        #endregion
    }
}
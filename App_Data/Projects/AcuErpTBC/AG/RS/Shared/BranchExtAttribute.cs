﻿using PX.Data;
using PX.Objects.GL;
using PX.Objects.CR;
using System;

namespace AG.RS.Shared
{
    public class BranchExtAttribute : BranchAttribute
    {
        public BranchExtAttribute()
            : base()
        {
            Type searchType = typeof(Search2<Branch.branchID,
                LeftJoin<BAccountExt, On<BAccountExt.bAccountID, Equal<Branch.bAccountID>>,
                LeftJoin<Ledger, On<Ledger.defBranchID, Equal<Branch.branchID>>>>,
                    Where2<MatchWithBranch<Branch.branchID>, And<Ledger.ledgerID, IsNull>>>);
            PXDimensionSelectorAttribute attr =
                new PXDimensionSelectorAttribute(_DimensionName, searchType, typeof(Branch.branchCD),
                    new Type[] { typeof(Branch.branchCD), typeof(Branch.ledgerID), typeof(Branch.acctName), typeof(BAccountExt.taxRegistrationID) });
            attr.ValidComboRequired = true;
            attr.DescriptionField = typeof(Branch.acctName);
            _Attributes[_SelAttrIndex] = attr;
        }

        public BranchExtAttribute(Type sourceType)
            : base(sourceType)
        {
        }

        public class BAccountExt : BAccount
        {
            #region BAccountID
            public abstract class bAccountID : PX.Data.IBqlField
            {
            }
            #endregion
            #region TaxRegistrationID
            public abstract class taxRegistrationID : PX.Data.IBqlField
            {
            }
            #endregion
        }
    }
}

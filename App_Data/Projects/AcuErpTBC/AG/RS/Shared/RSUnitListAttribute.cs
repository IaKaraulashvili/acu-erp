﻿using AG.RS.DAC;
using PX.Data;
using System.Collections.Generic;

namespace AG.RS.Shared
{
    public class RSUnitListAttribute : PXIntListAttribute
    {
        public RSUnitListAttribute() : base() { }
        public override void CacheAttached(PXCache cache)
        {
            var data = PXSelect<RSUnit>.Select(cache.Graph);
            List<int> values = new List<int>();
            List<string> labels = new List<string>();
            foreach (var item in data.FirstTableItems)
            {
                values.Add(item.ID.Value);
                labels.Add(item.Name);
            }
            this._AllowedValues = values.ToArray();
            this._AllowedLabels = labels.ToArray();
        }
    }
}

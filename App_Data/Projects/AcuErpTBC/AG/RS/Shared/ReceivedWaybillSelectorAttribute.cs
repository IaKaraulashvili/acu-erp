﻿using AG.RS.DAC;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class ReceivedWaybillSelectorAttribute : PXSelectorAttribute
    {
        /// <summary>
        /// Default Ctor
        /// </summary>
        /// <param name="SearchType"> Must be IBqlSearch type, pointing to refNbr</param>
        public ReceivedWaybillSelectorAttribute()
            : base(typeof(Search<RSReceivedWaybill.waybillNbr>),
            typeof(RSReceivedWaybill.waybillNbr),
            typeof(RSReceivedWaybill.waybillType),
            typeof(RSReceivedWaybill.transportationType),
            typeof(RSReceivedWaybill.status),
            typeof(RSReceivedWaybill.waybillNumber),
            typeof(RSReceivedWaybill.waybillState),
            typeof(RSReceivedWaybill.waybillStatus),
            typeof(RSReceivedWaybill.waybillID),
            typeof(RSReceivedWaybill.waybillActivationDate),
            typeof(RSReceivedWaybill.waybillCloseDate),
            typeof(RSReceivedWaybill.waybillCreateDate),
            typeof(RSReceivedWaybill.deliveryDateTime),
            typeof(RSWaybill.waybillCorrectionDate))
        {

        }
    }
}

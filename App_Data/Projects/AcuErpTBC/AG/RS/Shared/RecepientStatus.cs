﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class RecepientStatus
    {
        public const int Without = 0;
        public class without : Constant<Int32>
        {
            public without()
                : base(Without)
            {
            }
        }
        public const int MicroBusiness = 1;
        public class microBusiness : Constant<Int32>
        {
            public microBusiness()
                : base(MicroBusiness)
            {
            }
        }
        public const int SME = 2;
        public class sme : Constant<Int32>
        {
            public sme()
                : base(SME)
            {
            }
        }

        public class UI
        {
            public const string Without = "Without";
            public const string MicroBusiness = "MicroBusiness";
            public const string SME = "SME";
        }
    }
}

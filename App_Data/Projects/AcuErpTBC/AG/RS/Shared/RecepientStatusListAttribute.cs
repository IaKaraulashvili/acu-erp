﻿using PX.Data;

namespace AG.RS.Shared
{
    public class RecepientStatusListAttribute : PXIntListAttribute
    {
        public RecepientStatusListAttribute() : base(
            new int[]{
                RecepientStatus.Without,
                RecepientStatus.MicroBusiness,
                RecepientStatus.SME
            },
            new string[]{
              RecepientStatus.UI.Without,
              RecepientStatus.UI.MicroBusiness,
              RecepientStatus.UI.SME
            })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class RecivedTaxInvoiceStatusAttribute
        : PXStringListAttribute
    {
        public RecivedTaxInvoiceStatusAttribute() : base(new string[]
        {
                Status.Open,
                Status.Initial,
                Status.Corrected,
                Status.Approved,
                Status.Deleted,
                Status.Rejected

        },
        new string[]
        {
               "Open",
               "Initial",
               "Corrected",
               "Approved",
               "Deleted",
               "Rejected"

        })
        { }

        #region Statuses

        public class Status
        {
            public const string Open = "O";
            public const string Initial = "I";
            public const string Corrected = "R";
            public const string Approved = "A";
            public const string Deleted = "D";
            public const string Rejected = "J";
        }

        public class ExtStatus
        {
            public const int Pending = -1;
            public const int Rejected = 0;
            public const int Confirmed = 2;
            public const int Initial = 3;
            public const int Corrected = 4;
            public const int Canceled = 7;
            public const int WaitingApprove = 1;
            public const int ApprovedByVendor = 5;//კორექტირების ანგარიშ/ფაქტურა გადაგზავნილი, დასადასტურებელი;
        }

        public class CorrectionTypeStatus
        {
            public const int Canceled = 1;
            public const int TypeChange = 2;
            public const int AmountChange = 3;
            public const int Return = 4;

        }

        #endregion
    }
}
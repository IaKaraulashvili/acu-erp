﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class ShippingCostPayer
    {
        public const int Buyer = 1;
        public class buyer : Constant<Int32>
        {
            public buyer()
                : base(Buyer)
            {
            }
        }
        public const int Seller = 2;
        public class seller : Constant<Int32>
        {
            public seller()
                : base(Seller)
            {
            }
        }

        public class UI
        {
            public const string Buyer = "Buyer";
            public const string Seller = "Seller";
        }
    }
}

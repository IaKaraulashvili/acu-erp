﻿using PX.Data;

namespace AG.RS.Shared
{
    public class ShippingCostPayerListAttribute : PXIntListAttribute
    {
        public ShippingCostPayerListAttribute() : base(
            new int[]{
                ShippingCostPayer.Buyer,
                ShippingCostPayer.Seller
            },
            new string[]{
              ShippingCostPayer.UI.Buyer,
              ShippingCostPayer.UI.Seller
            })
        { }
    }
}

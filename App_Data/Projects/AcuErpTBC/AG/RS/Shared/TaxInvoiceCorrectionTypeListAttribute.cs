﻿using PX.Data;
using static AG.RS.Shared.TaxInvoiceStatusAttribute;

namespace AG.RS.Shared
{
    public class TaxInvoiceCorrectionTypeListAttribute : PXIntListAttribute
    {
        public TaxInvoiceCorrectionTypeListAttribute() : base(
        new int[]{
                CorrectionTypes.Canceled,
                CorrectionTypes.VATTypeChange,
                CorrectionTypes.PriceChange,
                CorrectionTypes.Return
        },
            new string[]{
                "Canceled",
                "VAT Type Change",
                "Price Change",
                "Return"
            })
        { }
    }
}

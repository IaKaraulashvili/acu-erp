﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class TaxInvoiceStatusAttribute : PXStringListAttribute
    {
        public TaxInvoiceStatusAttribute() : base(
             new string[]{

                Statuses.OnHold,
                Statuses.Open,
                Statuses.Sent,
                Statuses.Corrected,
                Statuses.Deleted,
                Statuses.Rejected,
                Statuses.Approved
            },
            new string[]{
                "On Hold",
                "Open",
                "Sent",
                "Corrected",
                "Deleted",
                "Rejected",
                "Approved"
            }
            )
        { }

        #region Statuses

        public static class CorrectionTypes
        {
            public const int Canceled = 1;
            public const int VATTypeChange = 2;
            public const int PriceChange = 3;
            public const int Return = 4;
        }

        public static class Statuses
        {
            public const string OnHold = "H";
            public const string Open = "O";
            public const string Sent = "S";
            public const string Corrected = "C";
            public const string Deleted = "D";
            public const string Rejected = "R";
            public const string Approved = "A";
            //public const string Sending = "SE";
            //public const string Deleting = "DE";
        }

        public static class SentTaxInvoiceStatuses
        {
            public const int Deleted = -1;
            public const int Rejected = 0;
            public const int Sent = 1;
            public const int Confirmed = 2;
            public const int Initial = 3;
            public const int CorrectedNew = 4;
            public const int CorrectedSent = 5;
            public const int ToBeCanceled = 6;
            public const int Canceled = 7;
            public const int CorrectedConfirmed = 8;
            public const int Replaced = 9;
            public const int CanceledByGE = 10;
            //public const int Rejected = "Rj";
        }

        public class statusSent : Constant<string>
        {
            public statusSent() : base(TaxInvoiceStatusAttribute.Statuses.Sent) { }
        }

        #endregion
    }
}

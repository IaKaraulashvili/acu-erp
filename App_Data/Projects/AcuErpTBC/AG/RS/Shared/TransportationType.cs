﻿using PX.Data;
using System;

namespace AG.RS.Shared
{
    public static class TransportationType
    {
        public const int Vehicle = 1;
        public class vehicle : Constant<Int32>
        {
            public vehicle()
                : base(Vehicle)
            {
            }
        }
        public const int Railway = 2;
        public class railway : Constant<Int32>
        {
            public railway()
                : base(Railway)
            {
            }
        }
        public const int Airplane = 3;
        public class airplane : Constant<Int32>
        {
            public airplane()
                : base(Airplane)
            {
            }
        }
        public const int Other = 4;
        public class other : Constant<Int32>
        {
            public other()
                : base(Other)
            {
            }
        }
        public const int Vehicle_ForeignCountry = 6;
        public class vehicle_ForeignCountry : Constant<Int32>
        {
            public vehicle_ForeignCountry()
                : base(Vehicle_ForeignCountry)
            {
            }
        }
        public const int Carrier_Vehicle = 7;
        public class carrier_Vehicle : Constant<Int32>
        {
            public carrier_Vehicle()
                : base(Carrier_Vehicle)
            {
            }
        }

        public static class UI
        {
            public const string Vehicle = "Vehicle";
            public const string Railway = "Railway";
            public const string Airplane = "Airplane";
            public const string Other = "Other";
            public const string Vehicle_ForeignCountry = "Vehicle – Foreign Citizen";
            public const string Carrier_Vehicle = "Carrier - Vehicle";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillCategory
    {
        public const int Normal = 0;
        public class normal : Constant<Int32>
        {
            public normal()
                : base(Normal)
            {
            }
        }
        public const int Timber = 1;
        public class timber : Constant<Int32>
        {
            public timber()
                : base(Timber)
            {
            }
        }
        public class UI
        {
            public const string Normal = "Normal";
            public const string Timber = "Timber";
        }
    }
}

﻿using PX.Data;

namespace AG.RS.Shared
{
    public class WaybillCategoryListAttribute : PXIntListAttribute
    {
        public WaybillCategoryListAttribute() : base(new int[]{
            WaybillCategory.Normal,
            WaybillCategory.Timber
       },
           new string[]{
            WaybillCategory.UI.Normal,
            WaybillCategory.UI.Timber
       })
        { }
    }
}

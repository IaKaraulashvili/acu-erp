﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillClass
    {
        public const string Sent = "S";
        public class sent : Constant<String>
        {
            public sent()
                : base(Sent)
            {
            }
        }
        public const string Received = "R";
        public class received : Constant<String>
        {
            public received()
                : base(Received)
            {
            }
        }

        public class UI
        {
            public const string Sent = "Sent";
            public const string Received = "Received";
        }

    }
}

﻿using PX.Data;

namespace AG.RS.Shared
{
    public class WaybillClassListAttribute : PXStringListAttribute
    {
        public WaybillClassListAttribute() : base(
            new string[]{
                WaybillClass.Sent,
                WaybillClass.Received
            },
            new string[]{
              WaybillClass.UI.Sent,
              WaybillClass.UI.Received
            })
        { }
    }
}

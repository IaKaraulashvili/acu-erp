﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillExtStatus
    {
        public const int Saved = 0;
        public class saved : Constant<Int32>
        {
            public saved()
                : base(Saved)
            {
            }
        }
        public const int Activated = 1;
        public class activated : Constant<Int32>
        {
            public activated()
                : base(Activated)
            {
            }
        }
        public const int Closed = 2;
        public class closed : Constant<Int32>
        {
            public closed()
                : base(Closed)
            {
            }
        }
        public const int Canceled = -2;
        public class canceled : Constant<Int32>
        {
            public canceled()
                : base(Canceled)
            {
            }
        }
        public const int SentToCarrier = 8;
        public class sentToCarrier : Constant<Int32>
        {
            public sentToCarrier()
                : base(SentToCarrier)
            {
            }
        }

        public class UI
        {
            public const string Saved = "Saved";
            public const string Activated = "Activated";
            public const string Closed = "Closed";
            public const string Canceled = "Canceled";
            public const string SentToCarrier = "Sent To Carrier";


        }
    }
}

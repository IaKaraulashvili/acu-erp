﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillExtStatusListAttribute : PXIntListAttribute
    {
        public WaybillExtStatusListAttribute() : base(
            new int[]{
                WaybillExtStatus.Saved,
                WaybillExtStatus.Activated,
                WaybillExtStatus.Closed,
                WaybillExtStatus.Canceled,
                WaybillExtStatus.SentToCarrier
            },
            new string[]{
              WaybillExtStatus.UI.Saved,
              WaybillExtStatus.UI.Activated,
              WaybillExtStatus.UI.Closed,
              WaybillExtStatus.UI.Canceled,
              WaybillExtStatus.UI.SentToCarrier
            })
        { }
    }
}

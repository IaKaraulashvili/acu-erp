﻿using AG.RS.DAC;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillHistorySelectorAttribute : PXSelectorAttribute
    {
        /// <summary>
        /// Default Ctor
        /// </summary>
        /// <param name="SearchType"> Must be IBqlSearch type, pointing to refNbr</param>
        public WaybillHistorySelectorAttribute()
            : base(typeof(Search<RSWaybillHistory.waybillHistoryNbr,
                Where<RSWaybillHistory.waybillNbr, Equal<Current<RSWaybillHistory.waybillNbr>>,
                    And<RSWaybillHistory.classID, Equal<Current<RSWaybillHistory.classID>>>>,
                        OrderBy<Desc<RSWaybillHistory.waybillHistoryNbr>>>),
            typeof(RSWaybillHistory.waybillNbr),
            typeof(RSWaybillHistory.waybillType),
            typeof(RSWaybillHistory.transportationType),
            typeof(RSWaybillHistory.status),
            typeof(RSWaybillHistory.waybillNumber),
            typeof(RSWaybillHistory.waybillState),
            typeof(RSWaybillHistory.waybillStatus),
            typeof(RSWaybillHistory.waybillID),
            typeof(RSWaybillHistory.waybillActivationDate),
            typeof(RSWaybillHistory.waybillCloseDate),
            typeof(RSWaybillHistory.waybillCreateDate))
        {
            Filterable = true;
        }
    }
}

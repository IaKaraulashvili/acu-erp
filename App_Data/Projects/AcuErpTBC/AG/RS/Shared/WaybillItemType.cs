﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillItemType
    {
        public const int Inventory = 1;
        public class inventory : Constant<Int32>
        {
            public inventory()
                : base(Inventory)
            {
            }
        }
        public const int FixedAsset = 2;
        public class fixedAsset : Constant<Int32>
        {
            public fixedAsset()
                : base(FixedAsset)
            {
            }
        }

        public const int EnterpriseAsset = 3;
        public class enterpriseAsset : Constant<Int32>
        {
            public enterpriseAsset()
                : base(EnterpriseAsset)
            {
            }
        }

        public class UI
        {
            public const string Inventory = "Inventory";
            public const string FixedAsset = "Fixed Asset";
            public const string EnterpriseAsset = "Enterprise Asset";
        }
    }
}

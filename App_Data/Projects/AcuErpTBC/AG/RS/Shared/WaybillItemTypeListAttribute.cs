﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillItemTypeListAttribute : PXIntListAttribute
    {
        public WaybillItemTypeListAttribute() : base(new int[]{
            WaybillItemType.Inventory,
            WaybillItemType.FixedAsset,
            WaybillItemType.EnterpriseAsset
       },
           new string[]{
            WaybillItemType.UI.Inventory,
            WaybillItemType.UI.FixedAsset,
            WaybillItemType.UI.EnterpriseAsset
       })
        { }
    }
}

﻿using AG.RS.DAC;
using PX.Data;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.IN;
using System;
using System.Collections;
using System.Linq;

namespace AG.RS.Shared
{
    public class WaybillItemTypeObjectSelectorAttribute : PXCustomSelectorAttribute
    {
        [Serializable]
        public class InventoryAssetData : IBqlTable
        {

            #region CD
            [PXString(60, IsUnicode = true, InputMask = "", IsKey = true)]
            [PXUIField(DisplayName = "ID", Visibility = PXUIVisibility.SelectorVisible)]
            public string CD { get; set; }

            public class cD : IBqlField { }
            #endregion
            #region Id

            [PXInt()]
            [PXUIField(Visibility = PXUIVisibility.Invisible)]
            public int? ID { get; set; }

            public class iD : IBqlField { }

            #endregion

            #region Description

            [PXString(60, IsUnicode = true, InputMask = "")]
            [PXUIField(DisplayName = "Description", Visibility = PXUIVisibility.SelectorVisible)]
            public string Description { get; set; }

            public class description : IBqlField { }

            #endregion

            #region ClassId

            [PXString(60, IsUnicode = true, InputMask = "")]
            [PXUIField(DisplayName = "Class Id", Visibility = PXUIVisibility.SelectorVisible)]
            public int? ClassId { get; set; }

            public class classId : IBqlField { }

            #endregion

            #region Status
            [PXString(60, IsUnicode = true, InputMask = "")]
            [PXUIField(DisplayName = "Status", Visibility = PXUIVisibility.SelectorVisible)]
            public string Status { get; set; }

            public class status : IBqlField { }
            #endregion



        }

        private Type _waybillItemType;

        public WaybillItemTypeObjectSelectorAttribute(Type waybillItemType) : base(typeof(InventoryAssetData.iD))
        {
            if (waybillItemType == null)
                throw new ArgumentNullException("waybillItemType");

            this._waybillItemType = waybillItemType;

        }

        protected virtual IEnumerable GetRecords()
        {
            PXCache cache = this._Graph.Caches[BqlCommand.GetItemType(this._waybillItemType)];
            var waybillItem = (RSWaybillItem)cache.Current;
            var waybillItemType = waybillItem?.WaybillItemType;

            switch (waybillItemType)
            {
                case WaybillItemType.Inventory:
                default:
                    {

                        var inventoryItems = PXSelect<InventoryItem,
                                          Where<InventoryItem.itemType, Equal<INItemTypes.subAssembly>,
                                          Or<InventoryItem.itemType, Equal<INItemTypes.finishedGood>,
                                          Or<InventoryItem.itemType, Equal<INItemTypes.component>>>>>.Select(_Graph).FirstTableItems;

                        return inventoryItems.Select(m => new InventoryAssetData { ID = m.InventoryID, Description = m.Descr, ClassId = m.ClassID, Status = m.ItemStatus, CD = m.InventoryCD });
                    }
                case WaybillItemType.FixedAsset:

                    {

                        var fixedAssetItems = PXSelectJoin<FixedAsset,
                                LeftJoin<FADetails, On<FADetails.assetID, Equal<FixedAsset.assetID>>,
                                LeftJoin<FALocationHistory, On<FALocationHistory.assetID, Equal<FixedAsset.assetID>,
                                    And<FALocationHistory.revisionID, Equal<FADetails.locationRevID>>>,
                                LeftJoin<Branch, On<Branch.branchID, Equal<FALocationHistory.locationID>>,
                                LeftJoin<PX.Objects.EP.EPEmployee, On<PX.Objects.EP.EPEmployee.bAccountID, Equal<FALocationHistory.employeeID>>>>>>>.Select(_Graph).FirstTableItems;

                        return fixedAssetItems.Select(m => new InventoryAssetData { ID = m.AssetID, Description = m.Description, ClassId = m.ClassID, Status = m.Status, CD = m.AssetCD });
                    }

            }


        }
    }
}

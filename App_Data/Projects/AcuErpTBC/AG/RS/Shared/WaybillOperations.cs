﻿namespace AG.RS.Shared
{
    public static class WaybillOperations
    {
        public enum Operations
        {
            Close,
            Cancel,
            Delete,
            Activate
        }

        public static string GenarateStatus(int status, string waybillID, Operations type)
        {
            string statusMessage = string.Empty;
            if (type == Operations.Close)
            {
                switch (status)
                {
                    case 1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " დაიხურა.";
                        }
                        break;
                    case -1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " არ დაიხურა.";
                        }
                        break;

                    case -101:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " სხვისი ზედნადებია და შეუძლებელია დახურვა.";
                        }
                        break;
                    case -100:
                        {
                            statusMessage = "სერვისი მომხმარებლის სახელი ან პაროლი არასწორია.";
                        }
                        break;
                }
            }
            else if (type == Operations.Cancel)
            {
                switch (status)
                {
                    case 1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " გაუქმდა.";
                        }
                        break;
                    case -1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " არ გაუქმდა.";
                        }
                        break;

                    case -101:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " სხვისი ზედნადებია და შეუძლებელია გაუქმება.";
                        }
                        break;
                    case -100:
                        {
                            statusMessage = "სერვისი მომხმარებლის სახელი ან პაროლი არასწორია.";
                        }
                        break;
                }
            }

            else if (type == Operations.Delete)
            {
                switch (status)
                {
                    case 1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " წაიშალა.";
                        }
                        break;
                    case -1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " არ წაიშალა.";
                        }
                        break;

                    case -101:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " სხვისი ზედნადებია და შეუძლებელია წაშლა.";
                        }
                        break;
                    case -100:
                        {
                            statusMessage = "სერვისი მომხმარებლის სახელი ან პაროლი არასწორია.";
                        }
                        break;
                }
            }
            else if (type == Operations.Activate)
            {
                switch (status)
                {
                    case 1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " გააქტიურდა.";
                        }
                        break;
                    case -1:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " არ გააქტიურდა.";
                        }
                        break;

                    case -101:
                        {
                            statusMessage = "ზედნადები ნომრით " + waybillID + " სხვისი ზედნადებია და შეუძლებელია გააქტიურება.";
                        }
                        break;
                    case -100:
                        {
                            statusMessage = "სერვისი მომხმარებლის სახელი ან პაროლი არასწორია.";
                        }
                        break;
                }
            }
            return statusMessage;
        }
    }
}

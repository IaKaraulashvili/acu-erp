﻿using AG.RS.DAC;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillSelectorAttribute : PXSelectorAttribute
    {
        /// <summary>
        /// Default Ctor
        /// </summary>
        /// <param name="SearchType"> Must be IBqlSearch type, pointing to refNbr</param>
        public WaybillSelectorAttribute()
            : base(typeof(Search<RSWaybill.waybillNbr>),
            typeof(RSWaybill.waybillNbr),
            typeof(RSWaybill.waybillType),
            typeof(RSWaybill.transportationType),
            typeof(RSWaybill.status),
            typeof(RSWaybill.waybillNumber),
            typeof(RSWaybill.waybillState),
            typeof(RSWaybill.waybillStatus),
            typeof(RSWaybill.waybillID),
            typeof(RSWaybill.waybillActivationDate),
            typeof(RSWaybill.waybillCloseDate),
            typeof(RSWaybill.waybillCreateDate),
            typeof(RSWaybill.deliveryDateTime),
            typeof(RSWaybill.waybillCorrectionDate))
        {
        }
    }
}

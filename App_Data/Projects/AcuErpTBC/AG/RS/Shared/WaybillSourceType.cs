﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillSourceType
    {
        public const int LCABulkRegistration = 1;
        public class lCABulkRegistration : Constant<Int32>
        {
            public lCABulkRegistration()
                : base(LCABulkRegistration)
            {
            }
        }
        public const int LCABulkTransfer = 2;
        public class lCABulkTransfer : Constant<Int32>
        {
            public lCABulkTransfer()
                : base(LCABulkTransfer)
            {
            }
        }

        public const int InventoryTransfer = 3;
        public class inventoryTransfer : Constant<Int32>
        {
            public inventoryTransfer()
                : base(InventoryTransfer)
            {
            }
        }

        public const int InventoryIssues = 4;
        public class inventoryIssues : Constant<Int32>
        {
            public inventoryIssues()
                : base(InventoryIssues)
            {
            }
        }

        public class UI
        {
            public const string LCABulkRegistration = "LCA Bulk Registration";
            public const string LCABulkTransfer = "LCA Bulk Transfer";
            public const string InventoryTransfer = "Inventory Transfer";
            public const string InventoryIssues = "Inventory Issues";
        }
    }
}

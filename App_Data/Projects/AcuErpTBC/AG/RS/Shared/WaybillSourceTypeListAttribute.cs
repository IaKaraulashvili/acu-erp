﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillSourceTypeListAttribute : PXIntListAttribute
    {
        public WaybillSourceTypeListAttribute() : base(new int[]{
            WaybillSourceType.LCABulkRegistration,
            WaybillSourceType.LCABulkTransfer,
            WaybillSourceType.InventoryTransfer,
            WaybillSourceType.InventoryIssues
       },
           new string[]{
            WaybillSourceType.UI.LCABulkRegistration,
            WaybillSourceType.UI.LCABulkTransfer,
            WaybillSourceType.UI.InventoryTransfer,
            WaybillSourceType.UI.InventoryIssues
       })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillStates
    {
        public const int Receivable = 0;
        public class receivable : Constant<Int32>
        {
            public receivable()
                : base(Receivable)
            {
            }
        }
        public const int Received = 1;
        public class received : Constant<Int32>
        {
            public received()
                : base(Received)
            {
            }
        }
        public const int Rejected = -1;
        public class rejected : Constant<Int32>
        {
            public rejected()
                : base(Rejected)
            {
            }
        }

        public class UI
        {
            public const string Receivable = "Receivable";
            public const string Received = "Received";
            public const string Rejected = "Rejected";
        }

    }
}

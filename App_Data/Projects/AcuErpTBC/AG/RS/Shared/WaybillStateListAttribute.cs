﻿using PX.Data;

namespace AG.RS.Shared
{
    public class WaybillStateListAttribute : PXIntListAttribute
    {
        public WaybillStateListAttribute() : base(new int[]{
            WaybillStates.Receivable,
            WaybillStates.Received,
            WaybillStates.Rejected

       },
           new string[]{
            WaybillStates.UI.Receivable,
            WaybillStates.UI.Received,
            WaybillStates.UI.Rejected

       })
        { }
    }
}

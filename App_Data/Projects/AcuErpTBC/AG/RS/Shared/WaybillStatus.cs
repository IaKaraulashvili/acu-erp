﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillStatus
    {
        public const string Hold = "H";
        public class hold : Constant<String>
        {
            public hold()
                : base(Hold)
            {
            }
        }
        public const string Open = "O";
        public class open : Constant<String>
        {
            public open()
                : base(Open)
            {
            }
        }
        public const string Posted = "P";
        public class posted : Constant<String>
        {
            public posted()
                : base(Posted)
            {
            }
        }

        public class UI
        {
            public const string Hold = "On Hold";
            public const string Open = "Open";
            public const string Sent = "Sent";
        }

    }
}

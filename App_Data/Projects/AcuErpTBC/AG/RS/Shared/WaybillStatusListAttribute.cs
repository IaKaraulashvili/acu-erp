﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public class WaybillStatusListAttribute : PXStringListAttribute
    {
        public WaybillStatusListAttribute() : base(
            new string[]{
                WaybillStatus.Hold,
                WaybillStatus.Open,
                WaybillStatus.Posted
            },
            new string[]{
                WaybillStatus.UI.Hold,
                WaybillStatus.UI.Open,
                WaybillStatus.UI.Sent
            })
        { }
    }
}

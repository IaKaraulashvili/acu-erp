﻿using AG.RS.DAC;
using PX.Data;
using System.Collections.Generic;
using System.Linq;

namespace AG.RS.Shared
{
    public class WaybillTransportationTypeListAttribute : PXIntListAttribute
    {
        public WaybillTransportationTypeListAttribute() : base(new int[]{
                TransportationType.Vehicle,
                TransportationType.Railway,
                TransportationType.Airplane,
                TransportationType.Other,
                TransportationType.Vehicle_ForeignCountry,
                TransportationType.Carrier_Vehicle
           },
            new string[]{
                TransportationType.UI.Vehicle,
                TransportationType.UI.Railway,
                TransportationType.UI.Airplane,
                TransportationType.UI.Other,
                TransportationType.UI.Vehicle_ForeignCountry,
                TransportationType.UI.Carrier_Vehicle
           })
        { }

        public override void FieldSelecting(PXCache cache, PXFieldSelectingEventArgs e)
        {
            if (e.Row != null)
            {
                var current = e.Row as IRSWaybill;

                List<int> values = this._AllowedValues.ToList();
                List<string> labels = this._AllowedLabels.ToList();

                if (current.WaybillType == WaybillType.Rtrn
                    || current.WaybillType == WaybillType.Distribution)
                {
                    values.Remove(TransportationType.Carrier_Vehicle);
                    labels.Remove(TransportationType.UI.Carrier_Vehicle);
                }

                this._AllowedValues = values.ToArray();
                this._AllowedLabels = labels.ToArray();
            }

            base.FieldSelecting(cache, e);
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.RS.Shared
{
    public static class WaybillType
    {
        public const int Internal = 1;
        public class @internal : Constant<Int32>
        {
            public @internal()
                : base(Internal)
            {
            }
        }
        public const int Transporation = 2;
        public class transporation : Constant<Int32>
        {
            public transporation()
                : base(Transporation)
            {
            }
        }

        public const int WithoutTransporation = 3;
        public class withoutTransporation : Constant<Int32>
        {
            public withoutTransporation()
                : base(WithoutTransporation)
            {
            }
        }

        public const int Distribution = 4;
        public class distribution : Constant<Int32>
        {
            public distribution()
                : base(Distribution)
            {
            }
        }

        public const int Rtrn = 5;
        public class rtrn : Constant<Int32>
        {
            public rtrn()
                : base(Rtrn)
            {
            }
        }

        public const int SubWaybill = 6;
        public class subWaybill : Constant<Int32>
        {
            public subWaybill()
                : base(SubWaybill)
            {
            }
        }

        public class UI
        {
            public const string Internal = "Internal";
            public const string Transporation = "Trans.";
            public const string WithoutTransporation = "Without Trans.";
            public const string Distribution = "Distribution";
            public const string Rtrn = "Return";
            public const string SubWaybill = "SubWaybill";
        }

    }
}

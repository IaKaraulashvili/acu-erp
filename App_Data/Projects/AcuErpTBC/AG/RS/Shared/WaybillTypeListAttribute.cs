﻿using PX.Data;

namespace AG.RS.Shared
{
    public class WaybillTypeListAttribute : PXIntListAttribute
    {
        public WaybillTypeListAttribute() : base(new int[]{
            WaybillType.Internal,
            WaybillType.Transporation,
            WaybillType.WithoutTransporation,
            WaybillType.Distribution,
            WaybillType.Rtrn
       },
           new string[]{
            WaybillType.UI.Internal,
            WaybillType.UI.Transporation,
            WaybillType.UI.WithoutTransporation,
            WaybillType.UI.Distribution,
            WaybillType.UI.Rtrn
       })
        { }
    }
}

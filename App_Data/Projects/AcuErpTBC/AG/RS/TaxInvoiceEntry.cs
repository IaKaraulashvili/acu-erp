﻿using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using System.Linq;
using PX.Objects.AR;
using PX.Objects.IN;
using PX.Objects.GL;
using PX.Objects.CR;
using global::RS.Services.Domain;
using PX.Objects.TX;
using PX.Objects.SO;
using static AG.RS.Shared.TaxInvoiceStatusAttribute;
using AG.RS.Shared;

namespace AG.RS
{
    public class TaxInvoiceEntry : AGGraph<TaxInvoiceEntry, RSTaxInvoice>
    {
        #region Data Views
        //public PXSelect<RSTaxInvoice> TaxInvoices;

        public PXSelectJoin<RSTaxInvoice,
                LeftJoin<RSTaxInvoiceCorrected, On<RSTaxInvoice.taxInvoiceNbr,
                    Equal<RSTaxInvoiceCorrected.correctedRefNbr>>>>
                        TaxInvoices;



        public PXSelect<RSTaxInvoiceItem, Where<RSTaxInvoiceItem.taxInvoiceNbr, Equal<Current<RSTaxInvoice.taxInvoiceNbr>>>> TaxInvoiceItems;

        public PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>> InventoryItems;

        public PXSelectJoin<RSTaxInvoiceWaybill, InnerJoin<RSWaybill, On<RSTaxInvoiceWaybill.waybillNbr, Equal<RSWaybill.waybillNbr>>>,
                Where<RSTaxInvoiceWaybill.taxInvoiceNbr, Equal<Current<RSTaxInvoice.taxInvoiceNbr>>>> TaxInvoiceWaybills;


        //public PXSelectReadonly<RSWaybill, Where<RSWaybill.taxInvoiceNbr, Equal<Current<RSTaxInvoice.taxInvoiceNbr>>>> TaxInvoiceWaybills;

        public PXSelect<RSTaxInvoiceMemo, Where<RSTaxInvoiceMemo.taxInvoiceNbr, Equal<Current<RSTaxInvoice.taxInvoiceNbr>>>> InvoiceAndMemos;


        public PXSelect<RSUnit> UOMs;



        public PXSetup<RSSetup> RSSetup;

        public PXFilter<TaxInvoiceCorrectionPopup> CorrectionTypes;

        //public PXSelectJoin<ARInvoice,
        //        InnerJoin<ARTran, On<ARInvoice.docType, Equal<ARTran.tranType>, And<ARInvoice.refNbr, Equal<ARTran.refNbr>>>,
        //        LeftJoin<ARTaxTran, On<ARTran.sOShipmentType, Equal<RSWaybillSource.sourceType>, And<ARTran.sOShipmentNbr, Equal<RSWaybillSource.sourceNbr>>>
        //        >>,
        //        Where<ARInvoice.docType, Equal<Optional<ARInvoice.docType>>, And<ARInvoice.refNbr, Equal<Optional<ARInvoice.refNbr>>>>> arInvoiceAndTran;

        public PXSelectJoin<ARInvoice,
                InnerJoin<ARTran, On<ARInvoice.docType, Equal<ARTran.tranType>, And<ARInvoice.refNbr, Equal<ARTran.refNbr>>>,
                LeftJoin<RSWaybillSource, On<ARTran.sOShipmentType, Equal<RSWaybillSource.sourceType>, And<ARTran.sOShipmentNbr, Equal<RSWaybillSource.sourceNbr>>>
                >>,
                Where<RSWaybillSource.waybillNbr, IsNull,
                And<ARInvoice.docType, Equal<ARDocType.invoice>,
                    And<ARInvoice.branchID, Equal<Optional<ARInvoiceListFilter.branchID>>,
                    And<ARInvoice.customerID, Equal<Optional<ARInvoiceListFilter.customerID>>,
                    And<ARInvoice.status, Equal<Optional<ARInvoiceListFilter.status>>
                    >>>>
                >> arInvoiceList;
        public PXFilter<ARInvoiceListFilter> arInvoiceListFilter;

        public virtual IEnumerable ARInvoiceList()
        {
            List<PXResult<ARInvoice, ARTran, RSWaybillSource>> list = new List<PXResult<ARInvoice, ARTran, RSWaybillSource>>();
            if (arInvoiceListFilter.Current != null)
                foreach (PXResult<ARInvoice, ARTran, RSWaybillSource> res in PXSelectJoin<ARInvoice,
                InnerJoin<ARTran, On<ARInvoice.docType, Equal<ARTran.tranType>, And<ARInvoice.refNbr, Equal<ARTran.refNbr>>>,
                LeftJoin<RSWaybillSource, On<ARTran.sOShipmentType, Equal<RSWaybillSource.sourceType>, And<ARTran.sOShipmentNbr, Equal<RSWaybillSource.sourceNbr>>>
                >>,
                Where<RSWaybillSource.waybillNbr, IsNull,
                And<ARInvoice.docType, Equal<ARDocType.invoice>,
                    And<ARInvoice.branchID, Equal<Optional<ARInvoiceListFilter.branchID>>,
                    And<ARInvoice.customerID, Equal<Optional<ARInvoiceListFilter.customerID>>,
                    And<ARInvoice.status, Equal<Optional<ARInvoiceListFilter.status>>
                    >>>>
                >>.Select(this, arInvoiceListFilter.Current.BranchID, arInvoiceListFilter.Current.CustomerID, arInvoiceListFilter.Current.Status))
                {
                    list.Add(res);
                }
            return list;
        }

        #endregion

        #region ctor

        public TaxInvoiceEntry()
        {
            var cur = RSSetup.Current;

            ActionsMenu.AddMenuAction(postTaxInvoice);
            ActionsMenu.AddMenuAction(deleteTaxInvoice);
            ActionsMenu.AddMenuAction(TaxInvoiceCorrectAction);


        }

        #endregion

        #region Page Events
        // ----------------------------- branchis shesavsebi -------------------------------------------------------------
        protected virtual void RSTaxInvoice_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }



        protected virtual void RSTaxInvoice_BranchID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                e.NewValue = Accessinfo.BranchID;
                row.BranchID = Accessinfo.BranchID;
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        //                     RSTaxInvoiceItem_InventoryID
        protected virtual void RSTaxInvoiceItem_InventoryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RSTaxInvoiceItem row = e.Row as RSTaxInvoiceItem;
            if (row != null)
            {
                if (!string.IsNullOrEmpty(row.TaxInvoiceNbr))
                {
                    InventoryItem item = InventoryItems.Select(row.InventoryID);

                    if (item != null)
                    {
                        row.ItemName = item.Descr;

                        //row.UnitPrice = item.StdCost;
                        //row.UnitPrice = item.BasePrice;
                        //  row.ExtUOM = item.BaseUnit;
                        if (item.ItemStatus == "AC")
                        {
                            row.ItemRowStatus = 1;
                        }
                        else { row.ItemRowStatus = -1; }
                        sender.SetDefaultExt<RSTaxInvoiceItem.itemAmt>(row);
                    }
                }
            }
        }


        protected virtual void RSTaxInvoice_CustomerID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSTaxInvoice)e.Row;

            if (row != null)
            {
                string name = null;
                string taxRegID = null;

                if (row.CustomerID.HasValue)
                {
                    PXResult<Customer, PX.Objects.CR.Location> result = (PXResult<Customer, PX.Objects.CR.Location>)PXSelectJoin<Customer,
                        LeftJoin<PX.Objects.CR.Location, On<Customer.defLocationID, Equal<PX.Objects.CR.Location.locationID>>>,
                        Where<Customer.bAccountID, Equal<Required<RSTaxInvoice.customerID>>>>.Select(this, row.CustomerID);

                    Customer customer = result;

                    if (customer != null)
                    {
                        name = customer.AcctName;

                        PX.Objects.CR.Location location = result;

                        if (location != null)
                        {
                            taxRegID = location.TaxRegistrationID;
                        }
                    }
                }

                row.CustomerName = name;
                row.CustomerTaxRegistrationID = taxRegID;
            }
        }

        // hold check box changes status to hold
        //protected virtual void RSTaxInvoice_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        //{

        //    var row = (RSTaxInvoice)e.Row;

        //    if (row.Hold == true)
        //    {
        //        row.Status = Statuses.OnHold;
        //    }
        //    else
        //    {
        //        row.Status = Statuses.Open;
        //    }
        //}


        protected virtual void RSTaxInvoiceMemo_ArInvoiceNbr_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            var row = (RSTaxInvoiceMemo)e.Row;

            var arInvoice = PXSelect<ARInvoice,
                          Where<ARInvoice.refNbr, Equal<Required<RSTaxInvoiceMemo.arInvoiceNbr>>>>.Select(this, row.ArInvoiceNbr).FirstTableItems.First();

            row.InvoiceAndMemoDate = arInvoice.DiscDate;

        }


        protected virtual void RSTaxInvoiceMemo_ShipmentNbr_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            var row = (RSTaxInvoiceMemo)e.Row;

            var shipment = PXSelect<SOShipment,
                          Where<SOShipment.shipmentNbr, Equal<Required<RSTaxInvoiceMemo.shipmentNbr>>>>.Select(this, row.ShipmentNbr).FirstTableItems.First();

            row.ShipmentDate = shipment.ShipDate;

        }

        protected virtual void RSTaxInvoice_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSTaxInvoice)e.Row;
            if (row == null) return;


            //PXUIFieldAttribute.SetVisible<RSTaxInvoice.initialTaxInvoiceNbr>(sender, row, row.Status == Statuses.Corrected);
            PXUIFieldAttribute.SetVisible<RSTaxInvoice.correctionType>(sender, row, row.Status == Statuses.Corrected);
            PXUIFieldAttribute.SetVisible<RSTaxInvoice.correctionDate>(sender, row, row.Status == Statuses.Corrected);

            PXUIFieldAttribute.SetVisible<RSTaxInvoice.prepaymentTaxInvoice>(sender, row, string.IsNullOrEmpty(row.CorrectedRefNbr));

            TaxInvoices.Cache.AllowDelete = row.Status == Statuses.Open;

            SetDisplayNameInventoryGridCol(row);



            deleteTaxInvoice.SetEnabled(TaxInvoices.Current.CanDeleteTaxInvoice());
            postTaxInvoice.SetEnabled(TaxInvoices.Current.CanPostTaxInvoice());
            TaxInvoiceCorrectAction.SetEnabled(TaxInvoices.Current.CanCorrectTaxInvoice());
        }

        private void SetDisplayNameInventoryGridCol(RSTaxInvoice row)
        {
            if (row == null)
                return;

            PXCache c;
            if (Caches.TryGetValue(typeof(RSTaxInvoiceItem), out c))
            {
                var FieldDisplayName = (row.PrepaymentTaxInvoice.HasValue && row.PrepaymentTaxInvoice.Value) ?
                        Descriptor.Messages.ReceivedTaxInvoicFullAmtDisplayName
                        : Descriptor.Messages.ReceivedTaxInvoicFullAmtDefaultDisplayName;


                PXUIFieldAttribute.SetDisplayName<RSTaxInvoiceItem.fullAmt>(c, FieldDisplayName);
            }
        }

        protected virtual void RSTaxInvoice_Hold_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = RSSetup.Current.TaxInvoiceHoldOnEntry;
        }

        protected virtual void RSTaxInvoice_Status_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = RSSetup.Current.TaxInvoiceHoldOnEntry == true ? WaybillStatus.Hold : WaybillStatus.Open;
        }

        #endregion

        #region Actions

        public PXAction<RSTaxInvoice> prepaymentTaxInvoiceAction;
        [PXUIField(DisplayName = "Text")]
        [PXButton()]
        protected virtual IEnumerable PrepaymentTaxInvoiceAction(PXAdapter adapter)
        {
            var row = adapter.Get<RSTaxInvoice>().FirstOrDefault();

            SetDisplayNameInventoryGridCol(row);

            return adapter.Get();
        }


        public PXAction<RSTaxInvoice> TaxInvoiceCorrectAction;
        [PXButton()]
        [PXUIField(DisplayName = "Correct Action")]
        protected virtual void taxInvoiceCorrectAction()
        {

            var Current = TaxInvoices.Current;
            //this.Save.se = false;

            if (Current != null && Current.CanCorrectTaxInvoice())
            {
                var g = PXGraph.CreateInstance<TaxInvoiceEntry>();

                var AlreadyExistCorrectedDoc =
                        PXSelectReadonly<RSTaxInvoice,
                        Where<RSTaxInvoice.correctedRefNbr, Equal<Required<RSTaxInvoice.taxInvoiceNbr>>>>.
                        Select(this, Current.TaxInvoiceNbr).FirstTableItems;

                if (AlreadyExistCorrectedDoc.Count() > 0)
                {
                    g.TaxInvoices.Current = AlreadyExistCorrectedDoc.First();
                    throw new PXRedirectRequiredException(g, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.Same };
                }

                if (CorrectionTypes.AskExt(true) != WebDialogResult.OK) return;

                //Cancel.Press();
                // g.Caches.Clear();


                var TaxToCopy = PXCache<RSTaxInvoice>.CreateCopy(Current);

                TaxToCopy.TaxInvoiceNbr = null;
                TaxToCopy.TaxInvoiceID = null;
                TaxToCopy.TaxInvoiceNumber = null;
                TaxToCopy.Status = Statuses.OnHold;
                TaxToCopy.TaxInvoiceStatus = null;
                TaxToCopy.CorrectionType = CorrectionTypes.Current.CorrectionType;
                TaxToCopy.CorrectedRefNbr = Current.TaxInvoiceNbr;
                var CreatedTax = g.TaxInvoices.Insert(TaxToCopy);
                //g.Persist();
                g.TaxInvoices.Insert(TaxToCopy);

                var i = 0;
                foreach (var item in TaxInvoiceItems.Select())
                {
                    i++;
                    var ItemToCopy = (RSTaxInvoiceItem)item;
                    ItemToCopy.TaxInvoiceItemNbr = i;
                    ItemToCopy.TaxInvoiceNbr = CreatedTax.TaxInvoiceNbr;
                    ItemToCopy.ItemRowID = 0;

                    g.TaxInvoiceItems.Insert(ItemToCopy);
                }
                i = 0;
                foreach (var item in TaxInvoiceWaybills.Select())
                {
                    i++;
                    var ItemToCopy = (RSTaxInvoiceWaybill)item;
                    ItemToCopy.TaxInvoiceNbr = CreatedTax.TaxInvoiceNbr;
                    g.TaxInvoiceWaybills.Insert(ItemToCopy);
                }

                //g.Persist();

                //g.Last.Press();
                //Cancel.Press();
                throw new PXRedirectRequiredException(g, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.NewWindow };

                //throw new PXPopupRedirectException(g, "", true);

            }

        }

        #region Buttons

        public PXAction<RSTaxInvoice> ViewPrevTaxInvoice;
        [PXButton]
        protected virtual void viewPrevTaxInvoice()
        {
            var current = TaxInvoices.Current;
            if (current != null)
            {

                ViewTaxInvoice(current.CorrectedRefNbr);

            }
        }

        public PXAction<RSTaxInvoice> ViewNextTaxInvoice;
        [PXButton]
        protected virtual void viewNextTaxInvoice()
        {
            var current = TaxInvoices.Current;
            if (current != null)
            {
                var JoinedCorrectedDAC = PXSelectReadonly<RSTaxInvoiceCorrected,
                        Where<RSTaxInvoiceCorrected.correctedRefNbr, Equal<Current<RSTaxInvoice.taxInvoiceNbr>>>>
                            .Select(this).FirstTableItems.FirstOrDefault();
                if (JoinedCorrectedDAC == null)
                    return;

                ViewTaxInvoice(JoinedCorrectedDAC.TaxInvoiceNbr);
            }
        }

        private void ViewTaxInvoice(string TaxInvoiceNbr)
        {
            TaxInvoiceEntry graph = PXGraph.CreateInstance<TaxInvoiceEntry>();

            var result = PXSelectReadonly<RSTaxInvoice,
                               Where<RSTaxInvoice.taxInvoiceNbr,
                               Equal<Required<RSTaxInvoice.taxInvoiceNbr>>>>.Select(graph, TaxInvoiceNbr).FirstTableItems;

            graph.TaxInvoices.Current = result.FirstOrDefault();

            if (graph.TaxInvoices.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, string.Empty) { Mode = PXBaseRedirectException.WindowMode.New };
            }
        }



        public PXAction<RSTaxInvoice> ActionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void actionsMenu()
        {
        }

        public PXAction<RSTaxInvoice> AddARInvoice;
        [PXUIField(DisplayName = AG.RS.Descriptor.Messages.AddInvoiceAndMemo, MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select, Visible = true, FieldClass = "DISTR")]
        [PXLookupButton]
        public virtual IEnumerable addARInvoice(PXAdapter adapter)
        {
            if (this.arInvoiceList.AskExt() == WebDialogResult.OK)
            {
                return addARInvoice2(adapter);
            }
            return adapter.Get();
        }

        public PXAction<RSTaxInvoice> AddARInvoice2;
        [PXUIField(DisplayName = AG.RS.Descriptor.Messages.Add, MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select, Visible = false)]
        [PXLookupButton]
        public virtual IEnumerable addARInvoice2(PXAdapter adapter)
        {
            if (this.TaxInvoices.Current != null)
            {
                foreach (ARInvoice rc in arInvoiceList.Cache.Updated)
                {
                    if (rc.Selected == true)
                    {

                    }
                }
            }
            arInvoiceList.View.Clear();
            arInvoiceList.Cache.Clear();
            return adapter.Get();
        }

        public PXAction<RSTaxInvoice> postTaxInvoice;
        [PXUIField(DisplayName = "Post TaxInvoice", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable PostTaxInvoice(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    //adapter.Get<taxinvo>
                    PostTI();
                    PXProcessing<RSTaxInvoice>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSTaxInvoice> deleteTaxInvoice;
        [PXUIField(DisplayName = "Delete TaxInvoice", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable DeleteTaxInvoice(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    DeleteTI();
                    PXProcessing<RSTaxInvoice>.SetInfo(ActionsMessages.GIRecordHasBeenDeleted);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        #endregion

        #region Methods


        public void PostTI()
        {

            var taxInvoice = this.TaxInvoices.Current;
            int invois_id = 0;



            var postPeriod = PXSelect<TaxPeriod, Where<TaxPeriod.taxPeriodID, Equal<Required<RSTaxInvoice.postPeriod>>>>.Select(this, taxInvoice.PostPeriod).FirstTableItems;
            using (var context = new global::RS.Services.Implementation.TaxInvoiceService(RSSetup.Current.TaxInvoiceAccount, RSSetup.Current.TaxInvoiceLicence, RSSetup.Current.TaxInvoiceUri, RSSetup.Current.TaxInvoiceTimeout))
            {
                var IsPrepayment = taxInvoice.PrepaymentTaxInvoice.HasValue && taxInvoice.PrepaymentTaxInvoice.Value;
                bool complete_successfully = false;


                var IsCorrected = !string.IsNullOrEmpty(taxInvoice.CorrectedRefNbr);

                if (IsCorrected)
                {
                    RSTaxInvoice CorrectedTaxInv = PXSelectReadonly<RSTaxInvoice,
                        Where<RSTaxInvoice.taxInvoiceNbr, Equal<Required<RSTaxInvoice.taxInvoiceNbr>>>>.
                                Select(this, taxInvoice.CorrectedRefNbr).FirstTableItems.First();



                    context.k_invoice(CorrectedTaxInv.TaxInvoiceID.Value, taxInvoice.CorrectionType.Value, out invois_id);

                    //clean invoce from items
                    var inv_items = context.get_invoice_desc(invois_id);
                    foreach (var item in inv_items)
                    {
                        context.delete_invoice_desc(item.ID.Value, invois_id);
                    }

                }

                if (IsPrepayment)
                    complete_successfully = context.save_seller_invoice_a(ref invois_id, postPeriod.First().StartDate.Value, taxInvoice.CustomerTaxRegistrationID);
                else
                    complete_successfully = context.save_seller_invoice(ref invois_id, postPeriod.First().StartDate.Value, taxInvoice.CustomerTaxRegistrationID);

                if (complete_successfully)
                {
                    taxInvoice.TaxInvoiceID = invois_id;
                    //TODO: remove comments
                    foreach (PXResult<RSTaxInvoiceWaybill, RSWaybill> tiitem in TaxInvoiceWaybills.Select())
                    {

                        complete_successfully = context.save_ntos_invoices_inv_nos(invois_id, ((RSWaybill)tiitem).WaybillNumber, ((RSWaybill)tiitem).WaybillCreateDate.Value);
                        if (!complete_successfully)
                        {
                            complete_successfully = context.change_invoice_status(invois_id, SentTaxInvoiceStatuses.Deleted);
                            if (complete_successfully)
                                throw new PXException(string.Format("Cannot Process {0} Waybill.", ((RSWaybill)tiitem).WaybillNumber));
                            else
                                throw new PXException(string.Format("Partially Process {0} Waybill And Cannot Delete Tax Invoice To GE", ((RSWaybill)tiitem).WaybillNumber));
                        }
                    }
                    foreach (var tiitem in TaxInvoiceItems.Select().FirstTableItems)
                    {
                        int id = tiitem.ItemRowID ?? 0;

                        //if(IsCorrected)
                        //{
                        //    context.get_invoice_desc(invois_id)
                        //}
                        //context.delete_invoice_desc()
                        complete_successfully = context.save_invoice_desc(ref id, invois_id, tiitem.ItemName, tiitem.ExtUOM.ToString(), tiitem.ItemQty.Value, tiitem.FullAmt.Value, tiitem.VATAmt.Value, tiitem.ExciseAmt.Value, 0);
                        if (complete_successfully)
                        {
                            tiitem.ItemRowID = id;
                            TaxInvoiceItems.Update(tiitem);
                        }
                        else
                        {
                            complete_successfully = context.change_invoice_status(invois_id, SentTaxInvoiceStatuses.Deleted);
                            if (complete_successfully)
                                throw new PXException(string.Format("Cannot Process {0} Item.", tiitem.ItemName));
                            else
                                throw new PXException(string.Format("Partially Process {0} Item And Cannot Delete Tax Invoice To GE", tiitem.ItemName));
                        }
                    }

                    int status = SentTaxInvoiceStatuses.Sent;

                    if (IsCorrected)
                        status = SentTaxInvoiceStatuses.CorrectedSent;

                    complete_successfully = context.change_invoice_status(invois_id, status);

                    if (complete_successfully)
                    {
                        UpdateTaxInvoiceFromGE(context, ref taxInvoice);
                        TaxInvoices.Update(taxInvoice);
                        Actions.PressSave();
                    }
                    else
                        throw new PXException("Cannot Post Tax Invoice. Cannot Change Status!");
                }
                else
                {
                    throw new PXException("Cannot Post Tax Invoice");
                }
            }
        }

        public void DeleteTI()
        {
            var taxInvoice = TaxInvoices.Current;
            using (var context = new global::RS.Services.Implementation.TaxInvoiceService(RSSetup.Current.TaxInvoiceAccount, RSSetup.Current.TaxInvoiceLicence, RSSetup.Current.TaxInvoiceUri, RSSetup.Current.TaxInvoiceTimeout))
            {
                // bool deleted = context.ref_invoice_status(taxInvoice.TaxInvoiceID.GetValueOrDefault(), string.Empty);
                bool deleted = context.change_invoice_status(taxInvoice.TaxInvoiceID.GetValueOrDefault(), SentTaxInvoiceStatuses.Deleted);

                if (!deleted)
                    throw new PXException(string.Format("Cannot be deleted"));

                taxInvoice.Status = Statuses.Deleted;
                taxInvoice.TaxInvoiceStatus = SentTaxInvoiceStatuses.Deleted;
                TaxInvoices.Update(taxInvoice);
                Actions.PressSave();
            }

        }

        #endregion

        #endregion

        #region Helper Methods

        private TaxInvoice GetExternalTaxInvoice(IEnumerable<RSTaxInvoiceItem> items = null)
        {
            // TO DO -- Test Mappings
            TaxInvoice rstaxInvoice = new TaxInvoice();
            List<TaxInvoiceItem> itemList = new List<TaxInvoiceItem>();

            var taxInvoice = this.TaxInvoices.Current;
            rstaxInvoice.AGREE_DATE = taxInvoice.ConfirmDate;
            //rstaxInvoice.B_S_USER_ID = taxInvoice.;
            rstaxInvoice.BUYER_DESC = taxInvoice.CustomerName;
            rstaxInvoice.BUYER_TIN = taxInvoice.CustomerTaxRegistrationID;
            //rstaxInvoice.BUYER_UN_ID = taxInvoice.;
            rstaxInvoice.DT = taxInvoice.CreateDate;
            rstaxInvoice.F_NUMBER = taxInvoice.TaxInvoiceNumber.Substring(0, 5);
            rstaxInvoice.F_SERIES = taxInvoice.TaxInvoiceNumber.Substring(6);
            rstaxInvoice.ID = taxInvoice.TaxInvoiceID;
            //rstaxInvoice.K_ID = taxInvoice.InitialTaxInvoiceNbr;
            //rstaxInvoice.K_TYPE = taxInvoice.CorrectionType;
            rstaxInvoice.NOTES = taxInvoice.Note;

            //rstaxInvoice.OP_DT = taxInvoice.PostPeriod;
            //rstaxInvoice.OPERATION_DT = taxInvoice.PostPeriod;

            rstaxInvoice.ORG_NAME = taxInvoice.BranchName;
            rstaxInvoice.REG_DT = taxInvoice.RegistrationDate;
            rstaxInvoice.SA_IDENT_NO = taxInvoice.CustomerTaxRegistrationID;
            rstaxInvoice.SELLER_DESC = taxInvoice.BranchName;
            rstaxInvoice.SELLER_TIN = taxInvoice.BranchTaxRegistrationID;
            //rstaxInvoice.SELLER_UN_ID = taxInvoice.;
            //rstaxInvoice.STATUS = taxInvoice.TaxInvoiceStatus;
            //rstaxInvoice.TANXA = taxInvoice.

            decimal fullAmount = 0;
            foreach (var item in items ?? TaxInvoiceItems.Select().FirstTableItems)
            {
                var tiitem = new TaxInvoiceItem();
                tiitem.AQCIZI_AMOUNT = item.ExciseAmt;
                tiitem.DRG_AMOUNT = item.VATAmt;
                tiitem.FULL_AMOUNT = item.FullAmt;
                tiitem.G_NUMBER = item.ItemQty;
                //tiitem.G_UNIT = item.ExtUOM;
                tiitem.GOODS = item.ItemName;
                tiitem.ID = item.InventoryID;
                tiitem.INV_ID = taxInvoice.TaxInvoiceID;
                //tiitem.SDRG_AMOUNT = item.TaxType;

                itemList.Add(tiitem);

                if (item.ItemAmt.HasValue)
                    fullAmount += item.ItemAmt.Value;
            }
            rstaxInvoice.TANXA = fullAmount;


            return rstaxInvoice;
        }


        private void UpdateTaxInvoiceFromGE(global::RS.Services.Implementation.TaxInvoiceService context, ref RSTaxInvoice taxInvoice)
        {
            string f_series;
            int f_number;
            DateTime operation_dt;
            DateTime reg_dt;
            int seller_un_id;
            int buyer_un_id;
            string overhead_no;
            DateTime overhead_dt;
            int status;
            string seq_num_s;
            string seq_num_b;
            int k_id;
            int r_un_id;
            int k_type;
            int b_s_user_id;
            int dec_status;

            var complete_successfully = context.get_invoice(taxInvoice.TaxInvoiceID.Value, out f_series, out f_number, out operation_dt, out reg_dt, out seller_un_id,
                out buyer_un_id, out overhead_no, out overhead_dt, out status, out seq_num_s, out seq_num_b, out k_id, out r_un_id, out k_type, out b_s_user_id,
                out dec_status);

            taxInvoice.TaxInvoiceNumber = string.Concat(f_series, " ", f_number.ToString());
            taxInvoice.RegistrationDate = reg_dt;
            taxInvoice.CreateDate = reg_dt;
            taxInvoice.TaxInvoiceStatus = status;
            taxInvoice.CorrectionType = k_type;
            taxInvoice.InitialTaxInvoiceID = k_id;
            taxInvoice.Status = Statuses.Sent;
            // TODO: dec_status

        }

        #endregion

        //public override bool IsDirty
        //{
        //    get
        //    {
        //        return false;
        //    }
        //}

        public override void Persist()
        {
            var Current = TaxInvoices.Current;
            if (Current != null)
            {
                if (Current.Status == Statuses.OnHold)
                {
                    Current.Status = Statuses.Open;
                    if (!string.IsNullOrEmpty(Current.CorrectedRefNbr))
                    {
                        var initialTaxList = PXSelectReadonly<RSTaxInvoice,
                            Where<RSTaxInvoice.taxInvoiceNbr, Equal<Required<RSTaxInvoice.correctedRefNbr>>>>
                                .Select(this, Current.CorrectedRefNbr).FirstTableItems;

                        var initialTaxItem = initialTaxList.FirstOrDefault();
                        if (initialTaxItem != null)
                        {
                            initialTaxItem.Status = Statuses.Corrected;

                            var g = PXGraph.CreateInstance<TaxInvoiceEntry>();
                            g.TaxInvoices.Cache.Update(initialTaxItem);
                            g.Actions.PressSave();
                        }
                    }
                }
                //var 
            }

            base.Persist();
        }


        #region Custom Classes

        [Serializable()]
        public partial class ARInvoiceListFilter : IBqlTable
        {
            #region Status
            public new abstract class status : PX.Data.IBqlField
            {
            }
            string _Status;
            /// <summary>
            /// The status of the document.
            /// The value of the field is determined by the values of the status flags. Can't be changed directly.
            /// (The status flags are: <see cref="Open"/>, <see cref="Closed"/>.)
            /// </summary>
            /// <value>
            /// Possible values are:
            /// <c>"O"</c> - Open,
            /// <c>"C"</c> - Closed
            /// Defaults to Hold (<c>"O"</c>).
            /// </value>
            [PXString(1, IsFixed = true)]
            [PXDefault(ARDocStatus.Open)]
            [PXUIField(DisplayName = "Status", Visibility = PXUIVisibility.SelectorVisible)]
            [PXStringList(new string[]{
                ARDocStatus.Open,
                ARDocStatus.Closed
                     },
                 new string[]{
                    "Open",
                    "Closed"})]
            public virtual String Status
            {
                get
                {
                    return this._Status;
                }
                set
                {
                    this._Status = value;
                }
            }
            #endregion
            #region BranchID
            public new abstract class branchID : PX.Data.IBqlField
            {
            }
            int? _BranchID;
            /// <summary>
            /// Identifier of the <see cref="Branch"/>, to which the document belongs.
            /// </summary>
            /// <value>
            /// Corresponds to the <see cref="Branch.BranchID"/> field.
            /// </value>
            [Branch(typeof(Coalesce<
                Search<Location.cBranchID, Where<Location.bAccountID, Equal<Current<ARRegister.customerID>>, And<Location.locationID, Equal<Current<ARRegister.customerLocationID>>>>>,
                Search<PX.Objects.GL.Branch.branchID, Where<PX.Objects.GL.Branch.branchID, Equal<Current<AccessInfo.branchID>>>>>), IsDetail = false)]
            public virtual Int32? BranchID
            {
                get
                {
                    return this._BranchID;
                }
                set
                {
                    this._BranchID = value;
                }
            }
            #endregion
            #region CustomerID
            public new abstract class customerID : PX.Data.IBqlField
            {
            }
            int? _CustomerID;
            /// <summary>
            /// Identifier of the <see cref="Customer"/>, whom the document belongs.
            /// </summary>
            /// <value>
            /// Corresponds to the <see cref="BAccount.BAccountID"/> field.
            /// </value>
            //[CustomerCredit(typeof(ARInvoice.hold), typeof(ARInvoice.released), Visibility = PXUIVisibility.SelectorVisible, DescriptionField = typeof(Customer.acctName), Filterable = true, TabOrder = 2)]
            [PXUIField(DisplayName = "Customer")]
            [PXDefault()]
            public virtual Int32? CustomerID
            {
                get
                {
                    return this._CustomerID;
                }
                set
                {
                    this._CustomerID = value;
                }
            }
            #endregion
        }

        #endregion
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using AG.RS.DAC;
using global::RS.Services.Domain;
using PX.Objects.PO;
using AG.RS.Helpers;
using System.Linq;
using PX.Objects.AP;
using AR.RS.DAC;

namespace AG.RS
{
    public class TaxInvoiceRequestEntry : AGGraph<TaxInvoiceRequestEntry, RSTaxInvoiceRequest>
    {
        #region DataViews
        public PXSelect<RSTaxInvoiceRequest> TaxInvoiceRequest;

        public PXSelectJoin<RSReceivedWaybill,
            InnerJoin<APTran, On<RSReceivedWaybill.pOReceiptNbr, Equal<APTran.receiptNbr>>,
            InnerJoin<RSTaxInvoiceRequestBill, On<APTran.refNbr, Equal<RSTaxInvoiceRequestBill.aPInvoiceNbr>,
                And<APTran.tranType, Equal<RSTaxInvoiceRequestBill.invoiceDocType>>>>>,
            Where<RSTaxInvoiceRequestBill.taxInvoiceNbr, Equal<Current<RSTaxInvoiceRequest.taxInvoiceNbr>>>> Waybills;

        public PXSetup<RSSetup> RSSetup;

        public PXSelectJoin<RSTaxInvoiceRequestBill,
              InnerJoin<APInvoice, On<APInvoice.docType, Equal<RSTaxInvoiceRequestBill.invoiceDocType>,
              And<APInvoice.refNbr, Equal<RSTaxInvoiceRequestBill.aPInvoiceNbr>>>>,
              Where<RSTaxInvoiceRequestBill.taxInvoiceNbr, Equal<Current<RSTaxInvoiceRequest.taxInvoiceNbr>>>> BillsAndAdjustments; 
        #endregion

        #region ctor

        public TaxInvoiceRequestEntry()
        {
            var cur = RSSetup.Current;
        } 
        #endregion

        #region Page Events

        protected virtual void RSTaxInvoiceRequest_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var sent = (RSTaxInvoiceRequest)e.Row;

            if (sent.Hold == true)
            {
                sent.Status = RSTaxInvoiceRequest.TaxInvoiceStatus.Hold;
            }
            else if (sent.Status == RSTaxInvoiceRequest.TaxInvoiceStatus.Hold && sent.Hold == false)
            {
                sent.Status = RSTaxInvoiceRequest.TaxInvoiceStatus.Open;
            }
        }

        protected virtual void RSTaxInvoiceRequest_VendorID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSTaxInvoiceRequest)e.Row;

            if (row != null)
            {
                string name = null;
                string taxRegID = null;

                if (row.VendorID.HasValue)
                {
                    PXResult<VendorLocation, PX.Objects.CR.Location> result = (PXResult<VendorLocation, PX.Objects.CR.Location>)PXSelectJoin<VendorLocation,
                        LeftJoin<PX.Objects.CR.Location, On<VendorLocation.locationID, Equal<PX.Objects.CR.Location.locationID>>>,
                        Where<VendorLocation.bAccountID, Equal<Required<RSTaxInvoiceRequest.vendorID>>>>.Select(this, row.VendorID);

                    VendorLocation vendor = result;

                    if (vendor != null)
                    {
                        name = vendor.AcctCD;

                        PX.Objects.CR.Location location = result;

                        if (location != null)
                        {
                            taxRegID = location.TaxRegistrationID;
                        }
                    }
                }

                row.VendorName = name;
                row.VendorTaxRegistrationID = taxRegID;
            }
        }

        protected virtual void RSTaxInvoiceRequest_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        protected virtual void RSTaxInvoiceRequest_BranchID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (IRSBranch)e.Row;
            if (row != null)
            {
                e.NewValue = Accessinfo.BranchID;
                row.BranchID = Accessinfo.BranchID;
                Helpers.PXGraphHelpers.SetBranch(this, row);
            }
        }

        protected virtual void RSTaxInvoiceRequest_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RSTaxInvoiceRequest row = (RSTaxInvoiceRequest)e.Row;
            if (row.VendorID != null)
            {
                if (BillsAndAdjustments.View.SelectSingleBound(new object[] { e.Row }) != null)
                {
                    PXUIFieldAttribute.SetEnabled<RSTaxInvoiceRequest.vendorID>(sender, row, false);
                }
                else
                {
                    PXUIFieldAttribute.SetEnabled<RSTaxInvoiceRequest.vendorID>(sender, row, true);
                }
            }
        }

        #endregion
    }
}
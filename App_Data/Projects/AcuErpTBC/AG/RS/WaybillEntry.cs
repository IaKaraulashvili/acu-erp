﻿using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using PX.Objects.IN;
using global::RS.Services.Domain;
using global::RS.Services.Helpers;
using AG.RS.DAC;
using System.Linq;
using PX.Objects.AR;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using AG.RS.Helpers;
using System.Globalization;
using PX.Objects.FA;
using AG.Utils.Extensions;
using AG.RS.Shared;
using AG.EA.DAC;
using AG.EA;

namespace AG.RS
{
    public class WaybillEntry : AGGraph<WaybillEntry, RSWaybill>
    {
        #region DataViews

        public PXSelect<RSWaybill> Waybills;

        public PXSelect<RSWaybill,
           Where<RSWaybill.waybillNbr, Equal<Current<RSWaybill.waybillNbr>>>> WaybillDetails;

        public PXSelect<RSWaybillItem,
            Where<RSWaybillItem.waybillNbr, Equal<Current<RSWaybill.waybillNbr>>>> WaybillItems;

        public PXSelectReadonly<RSWaybillHistory,
                  Where<RSWaybillHistory.waybillNbr, Equal<Current<RSWaybill.waybillNbr>>, And<RSWaybillHistory.classID, Equal<WaybillClass.sent>>>,
                  OrderBy<Desc<RSWaybillHistory.createdDateTime>>> HistoryItems;

        public PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>> InventoryItems;

        public PXSelect<RSWaybillSource, Where<RSWaybillSource.waybillNbr, Equal<Current<RSWaybill.waybillNbr>>>> WaybillSource;

        public PXSetup<RSSetup> Setup;
        public PXSelect<RSTransportType> TransportTypes;
        public PXSelect<RSUnit> UOMs;
        public PXSetup<FeaturesSet> FeaturesSet;

        #endregion

        #region ctor

        public WaybillEntry()
        {
            var cur = Setup.Current;
            var fs = FeaturesSet.Current;

            actionsMenu.AddMenuAction(postWaybill);
            actionsMenu.AddMenuAction(sendToCarrier);
            actionsMenu.AddMenuAction(postAndCloseWaybill);
            actionsMenu.AddMenuAction(cancelWaybill);
            actionsMenu.AddMenuAction(closeWaybill);
            actionsMenu.AddMenuAction(correctWaybill);
            actionsMenu.AddMenuAction(postCorrectedWaybill);
            actionsMenu.AddMenuAction(undoCorrection);
            reportsMenu.AddMenuAction(printWB);
        }

        #endregion

        #region Page Events

        protected virtual void RSWaybill_SupplierDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row != null)
                row.DestinationAddress = row.SupplierDestinationAddress;
        }

        protected virtual void RSWaybill_RecipientDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row != null)
            {
                row.DestinationAddress = row.RecipientDestinationAddress;

                if (row.WaybillType == WaybillType.WithoutTransporation && row.SourceAddress != row.DestinationAddress)
                {
                    sender.SetValueExt<RSWaybill.sourceAddress>(row, row.RecipientDestinationAddress);
                }
            }
        }

        protected virtual void RSWaybill_SourceAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row != null)
            {
                if (row.WaybillType == WaybillType.WithoutTransporation && row.SourceAddress != row.DestinationAddress)
                {
                    sender.SetValueExt<RSWaybill.recipientDestinationAddress>(row, row.SourceAddress);
                }
            }
        }

        protected virtual void RSWaybill_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            PXGraphHelpers.SetWaybillStatus(row);
        }

        protected virtual void RSWaybill_BranchID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            PXGraphHelpers.SetBranchInfo(this, row);
        }

        protected virtual void RSWaybill_CustomerID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;
            if (row != null)
            {
                var customer = (Customer)PXSelect<Customer, Where<Customer.bAccountID, Equal<Required<Customer.bAccountID>>>>.Select(this, row.CustomerID);
                if (customer != null)
                {
                    var baccount = (BAccount2)PXSelect<BAccount2, Where<BAccount2.bAccountID, Equal<Required<BAccount2.bAccountID>>>>
                                    .Select(this, customer.BAccountID);
                    if (baccount != null)
                    {
                        var contact = (Contact)PXSelect<Contact, Where<Contact.bAccountID, Equal<Required<BAccount.bAccountID>>, And<Contact.contactID, Equal<Required<BAccount.defContactID>>>>>
                            .Select(this, baccount.BAccountID, baccount.DefContactID);

                        if (contact != null)
                        {
                            row.RecipientIsForeignCitizen = PXCache<Contact>.GetExtension<ContactExt>(contact).UsrIsNotResident;
                            row.RecipientName = contact.FullName;
                        }
                        var address = (Address)PXSelect<Address, Where<Address.bAccountID, Equal<Required<Address.bAccountID>>,
                            And<Address.addressID, Equal<Required<Address.addressID>>>>>.Select(this, baccount.BAccountID, baccount.DefAddressID);
                        if (address != null && row.WaybillType != WaybillType.Internal)
                        {
                            if (!string.IsNullOrEmpty(address.AddressLine1))
                            {
                                sender.SetValueExt<RSWaybill.recipientDestinationAddress>(row, address.AddressLine1);
                            }
                        }
                        var location = (LocationExtAddress)PXSelect<LocationExtAddress, Where<LocationExtAddress.locationBAccountID, Equal<Required<BAccount.bAccountID>>, And<LocationExtAddress.locationID, Equal<Required<BAccount.defLocationID>>>>>
                            .Select(this, baccount.BAccountID, baccount.DefLocationID);
                        if (location != null)
                        {
                            if (row.WaybillType != WaybillType.Internal)
                            {
                                row.RecipientTaxRegistrationID = location.TaxRegistrationID;
                            }
                        }
                    }
                }
            }
        }

        protected virtual void RSWaybill_BranchID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (RSWaybill)e.Row;
            e.NewValue = Accessinfo.BranchID;
            row.BranchID = Accessinfo.BranchID;
            PXGraphHelpers.SetBranchInfo(this, row);
        }

        protected virtual void RSWaybill_TransStartTime_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row == null) return;
            e.NewValue = (int)DateTime.Now.TimeOfDay.TotalMinutes;

        }

        protected virtual void RSWaybill_DriverUID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row == null) return;

            //If import scenario is running cancel that event TODO: AllowTransporterEntry name is not meaningful
            if (row.AllowTransporterEntry.GetValueOrDefault()) return;

            RSDriver driver = PXGraphHelpers.GetDriverByUID(this, row.DriverUID);

            if (driver != null)
            {
                row.DriverName = driver.DriverName;
                row.DriverIsForeignCitizen = driver.IsForeignCitizen;
                row.CarRegistrationNumber = driver.DefaultCarNumber;
            }
            else
            {
                //clear
                sender.SetDefaultExt<RSWaybill.driverName>(row);
                sender.SetDefaultExt<RSWaybill.carRegistrationNumber>(row);
            }
        }

        protected virtual void RSWaybill_RowUpdating(PXCache sender, PXRowUpdatingEventArgs e)
        {
            var row = (RSWaybill)e.NewRow;
            var oldRow = (RSWaybill)e.Row;

            if (row != null)
            {
                Waybills.Current.DriverUID = row.DriverUID;
                if (row.DriverIsForeignCitizen == false && row.DriverUID != null && row.DriverUID.Length != 11)
                {
                    sender.RaiseExceptionHandling<RSWaybill.driverUID>(
                        row, row.DriverUID,
                        new PXSetPropertyException(AG.RS.Descriptor.Messages.WaybillDriverUIDLength));
                }
                if (row.WaybillType != WaybillType.Internal && row.RecipientIsForeignCitizen == false && row.RecipientTaxRegistrationID != null
                    && row.RecipientTaxRegistrationID.Length != 11
                    )
                {
                    //sender.RaiseExceptionHandling<RSWaybill.recipientTaxRegistrationID>(
                    //    row, row.RecipientTaxRegistrationID,
                    //    new PXSetPropertyException(AG.RS.Descriptor.Messages.WaybillRecipientTaxIDLength));
                }
            }
        }

        protected virtual void RSWaybill_RowUpdated(PXCache sender, PXRowUpdatedEventArgs e)
        {
            var row = (RSWaybill)e.Row;
            var oldRow = (RSWaybill)e.OldRow;

            if (row != null)
            {
                bool waybillTypeChanged = !sender.ObjectsEqual<RSWaybill.waybillType>(row, oldRow);
                if (waybillTypeChanged)
                {
                    ClearWaybillForm(sender, row);
                    return;
                }

                bool transportationTypeChanged = !sender.ObjectsEqual<RSWaybill.transportationType>(row, oldRow);
                if (transportationTypeChanged)
                {
                    ClearDriverForm(sender, row);
                }
            }
        }

        protected virtual void RSWaybill_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {
            var row = (RSWaybill)e.Row;
            if (row != null)
            {
                if (!string.IsNullOrEmpty(row.DestinationAddress))
                {
                    if (row.WaybillType == WaybillType.Internal && string.IsNullOrEmpty(row.SupplierDestinationAddress))
                        row.SupplierDestinationAddress = row.DestinationAddress;
                    else if (row.WaybillType != WaybillType.Internal && string.IsNullOrEmpty(row.RecipientDestinationAddress))
                        row.RecipientDestinationAddress = row.DestinationAddress;
                }
            }
        }

        protected virtual void RSWaybill_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSWaybill)e.Row;

            if (row == null) return;

            var isInternal = row.WaybillType == WaybillType.Internal;
            var isWithoutTrans = row.WaybillType == WaybillType.WithoutTransporation;
            var isDistribution = row.WaybillType == WaybillType.Distribution;
            var isCarrierVehicle = row.IsCarrierVehicle();

            bool needDriverInfo =
                !isWithoutTrans
                && (
                    row.TransportationType == TransportationType.Vehicle
                    || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                    || isCarrierVehicle
                );

            var needShippingCostPayer =
                isInternal && row.TransportationType == TransportationType.Vehicle_ForeignCountry
                || (
                        row.WaybillType == WaybillType.Transporation
                        || row.WaybillType == WaybillType.Distribution
                        || row.WaybillType == WaybillType.Rtrn
                    )
                    && (
                        row.TransportationType == TransportationType.Vehicle
                        || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                        || isCarrierVehicle
                       );

            bool needCarrierInfo = !isWithoutTrans && isCarrierVehicle;

            PXUIFieldAttribute.SetVisible<RSWaybill.isCorrected>(sender, row, row.IsCorrected.GetValueOrDefault());

            PXUIFieldAttribute.SetVisible<RSWaybill.driverUID>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybill.driverName>(sender, row, needDriverInfo);
            getDriverName.SetVisible(needDriverInfo);

            PXUIFieldAttribute.SetVisible<RSWaybill.closedByID>(sender, row, row.ClosedByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybill.waybillCloseDate>(sender, row, row.WaybillCloseDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybill.canceledByID>(sender, row, row.CanceledByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybill.waybillCancelDate>(sender, row, row.WaybillCancelDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybill.correctedByID>(sender, row, row.CorrectedByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybill.waybillCorrectionDate>(sender, row, row.WaybillCorrectionDate.HasValue);

            PXUIFieldAttribute.SetVisible<RSWaybill.driverIsForeignCitizen>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybill.shippingCost>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybill.carRegistrationNumber>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybill.trailer>(sender, row, needDriverInfo);

            PXUIFieldAttribute.SetVisible<RSWaybill.otherTransportationTypeDescription>(sender, row, row.TransportationType == TransportationType.Other);
            PXUIFieldAttribute.SetVisible<RSWaybill.supplierDestinationAddress>(sender, row, isInternal);
            PXUIFieldAttribute.SetVisible<RSWaybill.shippingCostPayer>(sender, row, needShippingCostPayer);

            PXUIFieldAttribute.SetVisible<RSWaybill.carrierTaxRegistrationID>(sender, row, needCarrierInfo);
            PXUIFieldAttribute.SetVisible<RSWaybill.carrierInfo>(sender, row, needCarrierInfo);

            // Hide Buyer (Recipient)
            PXUIFieldAttribute.SetVisible<RSWaybill.recipientInfo>(sender, row, !isInternal);
            PXUIFieldAttribute.SetVisible<RSWaybill.recipientIsForeignCitizen>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybill.recipientName>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybill.customerID>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybill.recipientTaxRegistrationID>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybill.recipientDestinationAddress>(sender, row, !isInternal && !isDistribution && !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybill.transportationType>(sender, row, !isWithoutTrans);

            PXUIFieldAttribute.SetVisible<RSWaybill.transStartDate>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybill.transStartTime>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybill.deliveryTime>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybill.deliveryDate>(sender, row, !isWithoutTrans);

            bool hasMultiBranch = FeaturesSet.Current.Branch ?? false;
            PXUIFieldAttribute.SetVisible<RSWaybill.branchID>(sender, row, hasMultiBranch);

            PXUIFieldAttribute.SetVisible<RSWaybill.linkedWaybillNbr>(sender, row, isInternal);

            // Set Enabled

            var onHold = row.Status == WaybillStatus.Hold;
            var isOpen = row.Status == WaybillStatus.Open;
            var isPosted = row.Status == WaybillStatus.Posted;
            var isCorrected = row.IsCorrected.GetValueOrDefault();
            var isClosed = row.WaybillStatus == WaybillExtStatus.Closed;
            var isCanceled = row.WaybillStatus == WaybillExtStatus.Canceled;
            var isActivated = row.WaybillStatus == WaybillExtStatus.Activated;
            var allowTransporterEntry = row.AllowTransporterEntry.GetValueOrDefault();
            var enableDriverEntry = !isPosted && !isCarrierVehicle || allowTransporterEntry;
            var isCarrierVehicleActivated = isCarrierVehicle && !row.WaybillNumber.IsNullOrEmpty();

            PXUIFieldAttribute.SetEnabled<RSWaybill.waybillType>(sender, row, sender.GetStatus(row) == PXEntryStatus.Inserted && row.SourceType != WaybillSourceType.LCABulkTransfer);
            PXUIFieldAttribute.SetEnabled<RSWaybill.hold>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.branchID>(sender, row, !isPosted && !isCorrected);
            PXUIFieldAttribute.SetEnabled<RSWaybill.waybillCategory>(sender, row, !isPosted && !isCorrected);

            PXUIFieldAttribute.SetEnabled<RSWaybill.transStartDate>(sender, row, !isPosted && !isWithoutTrans || allowTransporterEntry);
            PXUIFieldAttribute.SetEnabled<RSWaybill.transStartTime>(sender, row, !isPosted && !isWithoutTrans || allowTransporterEntry);

            PXUIFieldAttribute.SetEnabled<RSWaybill.driverName>(sender, row, enableDriverEntry);
            PXUIFieldAttribute.SetEnabled<RSWaybill.driverUID>(sender, row, enableDriverEntry);
            PXUIFieldAttribute.SetEnabled<RSWaybill.carRegistrationNumber>(sender, row, enableDriverEntry);
            PXUIFieldAttribute.SetEnabled<RSWaybill.driverIsForeignCitizen>(sender, row, enableDriverEntry);
            PXUIFieldAttribute.SetEnabled<RSWaybill.trailer>(sender, row, !isPosted && !isCarrierVehicle || allowTransporterEntry);
            getDriverName.SetEnabled(!isPosted && !isCarrierVehicle && (row.DriverIsForeignCitizen == true || (row.DriverIsForeignCitizen == false && row.DriverUID != null && row.DriverUID.Length == 11)));

            PXUIFieldAttribute.SetEnabled<RSWaybill.sourceAddress>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.transportationType>(sender, row, !isPosted && !isCarrierVehicleActivated);
            PXUIFieldAttribute.SetEnabled<RSWaybill.carrierInfo>(sender, row, !isPosted && !isCarrierVehicleActivated);
            PXUIFieldAttribute.SetEnabled<RSWaybill.carrierTaxRegistrationID>(sender, row, !isPosted && !isCarrierVehicleActivated);
            PXUIFieldAttribute.SetEnabled<RSWaybill.shippingCost>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.shippingCostPayer>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.otherTransportationTypeDescription>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.customerID>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.recipientIsForeignCitizen>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.supplierDestinationAddress>(sender, row, !isPosted);
            PXUIFieldAttribute.SetEnabled<RSWaybill.recipientDestinationAddress>(sender, row, !isPosted);

            PXUIFieldAttribute.SetEnabled<RSWaybill.supplierInfo>(sender, row, !isCanceled && (isCorrected && isClosed || isPosted && !isClosed || isWithoutTrans));
            PXUIFieldAttribute.SetEnabled<RSWaybill.recipientInfo>(sender, row, !isCanceled && (isCorrected && isClosed || isPosted && !isClosed || isWithoutTrans));
            PXUIFieldAttribute.SetEnabled<RSWaybill.transStartDate>(sender, row, !isCanceled && !isCarrierVehicleActivated);
            PXUIFieldAttribute.SetEnabled<RSWaybill.transStartTime>(sender, row, !isCanceled && !isCarrierVehicleActivated);
            PXUIFieldAttribute.SetEnabled<RSWaybill.deliveryDate>(sender, row, !isCanceled && (isCorrected && isClosed || isPosted && !isClosed));
            PXUIFieldAttribute.SetEnabled<RSWaybill.deliveryTime>(sender, row, !isCanceled && (isCorrected && isClosed || isPosted && !isClosed));

            //PXUIFieldAttribute.SetEnabled<RSWaybill.comment>(sender, row, !isPosted);

            // Conditionally required fields
            PXDefaultAttribute.SetPersistingCheck<RSWaybill.shippingCostPayer>(sender, null, needShippingCostPayer ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
            PXDefaultAttribute.SetPersistingCheck<RSWaybill.driverUID>(sender, null, needDriverInfo && enableDriverEntry ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
            PXDefaultAttribute.SetPersistingCheck<RSWaybill.driverName>(sender, null, needDriverInfo && enableDriverEntry ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);

            Waybills.Cache.AllowDelete = !isPosted && !isCorrected;
            WaybillItems.Cache.AllowDelete = !isPosted;
            WaybillItems.Cache.AllowInsert = !isPosted;
            WaybillItems.Cache.AllowUpdate = !isPosted;

            // Actions Menu
            PXButtonState actionsMenuState = actionsMenu.GetState(row) as PXButtonState;
            List<ButtonMenu> menu = new List<ButtonMenu>();

            foreach (ButtonMenu button in actionsMenuState.Menus)
            {
                if (button.Command == (postWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Visible = !isWithoutTrans && !isCarrierVehicle;
                    button.Enabled = !Waybills.Cache.IsDirty && isOpen && row.WaybillStatus == null && !isWithoutTrans && !isCarrierVehicle && sender.GetStatus(row) != PXEntryStatus.Inserted;
                }
                else if (button.Command == (sendToCarrier.GetState(row) as PXButtonState).Name)
                {
                    button.Visible = !isWithoutTrans && isCarrierVehicle;
                    button.Enabled = !Waybills.Cache.IsDirty && isOpen && row.WaybillStatus == null && !isWithoutTrans && isCarrierVehicle;
                }
                else if (button.Command == (postAndCloseWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Visible = isWithoutTrans;
                    button.Enabled = !Waybills.Cache.IsDirty && isOpen && row.WaybillStatus == null && isWithoutTrans;
                }
                else if (button.Command == (cancelWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Enabled = !Waybills.Cache.IsDirty && isPosted && !isCanceled;
                }
                else if (button.Command == (closeWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Visible = !isWithoutTrans;
                    button.Enabled = !Waybills.Cache.IsDirty && isPosted && isActivated && !isWithoutTrans && (row.WaybillType == WaybillType.Internal || !isCarrierVehicle);
                }
                else if (button.Command == (correctWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Enabled = !Waybills.Cache.IsDirty && isPosted && !isCanceled;
                }
                else if (button.Command == (postCorrectedWaybill.GetState(row) as PXButtonState).Name)
                {
                    button.Enabled = !Waybills.Cache.IsDirty && isCorrected && isOpen;
                }
                else if (button.Command == (undoCorrection.GetState(row) as PXButtonState).Name)
                {
                    button.Enabled = isOpen && isCorrected;
                }

                menu.Add(button);
            }

            actionsMenu.SetMenu(menu.ToArray());
        }

        protected virtual void RSWaybillItem_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSWaybillItem)e.Row;

            if (row != null)
            {
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.waybillItemType>(sender, row, Waybills.Current.SourceType != WaybillSourceType.LCABulkTransfer);
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.inventoryID>(sender, row, row.WaybillItemType == WaybillItemType.Inventory);
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.fixedAssetID>(sender, row, row.WaybillItemType == WaybillItemType.FixedAsset);
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.eAAssetID>(sender, row, row.WaybillItemType == WaybillItemType.EnterpriseAsset);
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.otherUOMDescription>(sender, row, row.ExtUOM == 99 /* Other */);
            }
        }

        protected virtual void RSWaybillItem_RowUpdated(PXCache sender, PXRowUpdatedEventArgs e)
        {
            var row = (RSWaybillItem)e.Row;

            if (row != null)
            {
                //PXSelectorAttribute.SetColumns<RSWaybillItem.inventoryID>(sender, new Type[] { typeof(InventoryItem.invtSubID) }, new string[] { PXUIFieldAttribute.GetDisplayName<InventoryItem.invtSubID>(sender) });
                PXUIFieldAttribute.SetEnabled<RSWaybillItem.otherUOMDescription>(sender, row, row.ExtUOM == 99 /* Other */);

                if (!sender.ObjectsEqual<RSWaybillItem.waybillItemType>(row, e.OldRow))
                {
                    ClearWaybillItemRow(sender, row);
                }

            }
        }

        protected virtual void RSWaybillItem_ItemQty_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybillItem)e.Row;
            if (row != null)
            {
                row.ItemAmt = row.UnitPrice * (row.ItemQty + row.ItemExtraQty);
            }

        }

        protected virtual void RSWaybillItem_InventoryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            RSWaybillItem row = e.Row as RSWaybillItem;
            if (row != null)
            {
                var uoms = UOMs.Select().FirstTableItems.ToList();
                InventoryItem inventoryItem = InventoryItems.Select(row.InventoryID);
                if (inventoryItem != null)
                {
                    var rSUOMId = PXSelect<RSUOMMapper, Where<RSUOMMapper.unit, Equal<Required<RSUOMMapper.unit>>>>.Select(this, inventoryItem.SalesUnit).FirstTableItems?.FirstOrDefault()?.RSUOMID;
                    row.ItemName = inventoryItem.Descr;
                    row.UnitPrice = inventoryItem.StdCost;
                    row.ItemCode = inventoryItem.InventoryCD;
                    sender.SetValueExt<RSWaybillItem.extUOM>(row, rSUOMId);
                    sender.SetDefaultExt<RSWaybillItem.itemAmt>(row);
                }
            }
        }

        protected virtual void RSWaybillItem_FixedAssetID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            RSWaybillItem row = e.Row as RSWaybillItem;
            if (row != null)
            {
                var uoms = UOMs.Select().FirstTableItems.ToList();
                FixedAsset fixssetAssetItem = PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.assetID>>>>.Select(this, row.FixedAssetID);
                if (fixssetAssetItem != null)
                {
                    FixedAssetExt fixssetAssetItemExt = PXCache<FixedAsset>.GetExtension<FixedAssetExt>(fixssetAssetItem);
                    var rSUOMId = PXSelect<RSUOMMapper, Where<RSUOMMapper.unit, Equal<Required<RSUOMMapper.unit>>>>.Select(this, fixssetAssetItemExt?.UsrUnitOfMeasure).FirstTableItems?.FirstOrDefault()?.RSUOMID;
                    row.ItemName = fixssetAssetItem.Description;
                    row.ItemCode = fixssetAssetItem.AssetCD;
                    sender.SetDefaultExt<RSWaybillItem.unitPrice>(row);
                    sender.SetValueExt<RSWaybillItem.extUOM>(row, rSUOMId);
                    sender.SetDefaultExt<RSWaybillItem.itemAmt>(row);
                }
            }
        }

        protected virtual void RSWaybillItem_EAAssetID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            RSWaybillItem row = e.Row as RSWaybillItem;
            if (row != null)
            {
                var uoms = UOMs.Select().FirstTableItems.ToList();
                EAAsset asset = PXSelect<EAAsset, Where<EAAsset.assetID, Equal<Required<FixedAsset.assetID>>>>.Select(this, row.EAAssetID);
                if (asset != null)
                {
                    var rSUOMId = PXSelect<RSUOMMapper, Where<RSUOMMapper.unit, Equal<Required<RSUOMMapper.unit>>>>.Select(this, asset.UOM).FirstTableItems?.FirstOrDefault()?.RSUOMID;
                    row.ItemName = asset.Description;
                    row.ItemCode = asset.AssetCD;
                    row.ItemQty = asset.Qty;
                    sender.SetDefaultExt<RSWaybillItem.unitPrice>(row);
                    sender.SetValueExt<RSWaybillItem.extUOM>(row, rSUOMId);
                    sender.SetDefaultExt<RSWaybillItem.itemAmt>(row);
                }
            }
        }

        protected virtual void RSWaybillItem_ExtUOM_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RSWaybillItem waybill = e.Row as RSWaybillItem;
            bool found = false;
            if (waybill != null && waybill.ExtUOM.HasValue)
            {
                foreach (var item in UOMs.Select().FirstTableItems)
                {
                    if (item.ID == waybill.ExtUOM.Value)
                    {
                        found = true;
                        waybill.OtherUOMDescription = item.Name;
                        break;
                    }
                }
            }
            if (!found)
            {
                waybill.OtherUOMDescription = null;
            }
        }

        protected virtual void RSWaybillSource_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSWaybillSource)e.Row;
            if (row != null)
            {
                PXUIFieldAttribute.SetEnabled<RSWaybillSource.sourceNbr>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<RSWaybillSource.sourceType>(sender, row, false);
                PXUIFieldAttribute.SetEnabled<RSWaybillSource.waybillNbr>(sender, row, false);
            }
        }

        protected virtual void RSWaybill_Hold_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = Setup.Current.WaybillHoldOnEntry;
        }

        protected virtual void RSWaybill_Status_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.Cancel = true;
            e.NewValue = Setup.Current.WaybillHoldOnEntry == true ? WaybillStatus.Hold : WaybillStatus.Open;
        }

        #endregion

        #region Buttons
        public PXAction<RSWaybill> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        public PXAction<RSWaybill> viewSource;
        [PXUIField(DisplayName = "View Source", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton]
        public virtual void ViewSource()
        {
            var row = Waybills.Current;
            if (row == null || row.SourceNbr.IsNullOrEmpty()) return;
            PXGraphHelpers.ViewWaybillSource(this, row.SourceType, row.SourceNbr);
        }


        public PXAction<RSWaybill> postWaybill;
        [PXUIField(DisplayName = "Post Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable PostWaybill(PXAdapter adapter)
        {


            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    PostWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> postAndCloseWaybill;
        [PXUIField(DisplayName = "Post & Close Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable PostAndCloseWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    PostWB();
                    CloseWB();

                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> cancelWaybill;
        [PXUIField(DisplayName = "Cancel Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable CancelWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    CancelWB();

                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> closeWaybill;
        [PXUIField(DisplayName = "Close Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable CloseWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    CloseWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> correctWaybill;
        [PXUIField(DisplayName = "Correct Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable CorrectWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    CorrectWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> postCorrectedWaybill;
        [PXUIField(DisplayName = "Post Corrected Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable PostCorrectedWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    PostCorrectedWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> sendToCarrier;
        [PXUIField(DisplayName = "Send To Carrier", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable SendToCarrier(PXAdapter adapter)
        {


            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    PostWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> undoCorrection;
        [PXUIField(DisplayName = "Undo Correction", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable UndoCorrection(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                try
                {
                    UndoCorrectedWB();
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<RSWaybill> viewHistory;
        [PXUIField(MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton]
        public virtual void ViewHistory()
        {
            var row = HistoryItems.Current;

            if (row != null)
            {
                var graph = PXGraph.CreateInstance<WaybillHistoryEntry>();

                graph.Waybills.Current = row;

                throw new PXPopupRedirectException(graph, "ViewHistory", true);
            }
        }



        public PXAction<RSWaybill> reportsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Reports")]
        protected virtual void ReportsMenu()
        {

        }

        public PXAction<RSWaybill> printWB;
        [PXUIField(DisplayName = "Print Waybill", MapViewRights = PXCacheRights.Select, MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        public virtual IEnumerable PrintWB(PXAdapter adapter)
        {
            if (Waybills.Current != null)
            {
                Dictionary<string, string> info = new Dictionary<string, string>();
                info["WaybillNbr"] = Waybills.Current.WaybillNbr.ToString();
                //info["WaybillType"] = WaybillDetails.Current.WaybillType.Value.ToString();

                throw new PXReportRequiredException(info, "RS601000", "Print Waybill");
            }
            return adapter.Get();
        }

        public PXAction<RSWaybill> getDriverName;
        [PXUIField(DisplayName = AG.RS.Descriptor.Messages.GetDriverName)]
        [PXButton]
        public void GetDriverName()
        {
            var waybill = Waybills.Current;
            try
            {
                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    if (waybill.DriverUID != null && waybill.DriverUID.Length > 0)
                    {
                        waybill.DriverName = context.GetNameByUID(waybill.DriverUID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }

        }

        public PXAction<RSWaybill> viewInventory;
        [PXButton]
        protected virtual void ViewInventory()
        {
            if (WaybillItems.Current != null)
            {
                InventoryItemMaint graph = PXGraph.CreateInstance<InventoryItemMaint>();

                graph.Item.Current = graph.Item.Search<InventoryItem.inventoryID>(WaybillItems.Current.InventoryID);
                //graph.Item.Current = PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>.Select(graph, WaybillItems.Current.InventoryID);

                if (graph.ItemSettings.Current != null)
                {
                    throw new PXRedirectRequiredException(graph, true, "InventoryItem") { Mode = PXBaseRedirectException.WindowMode.NewWindow };
                }
            }
        }

        #endregion

        #region Actions

        public void PostWB()
        {
            using (var scope = new PXTransactionScope())
            {
                RSWaybill waybill = Waybills.Current;

                if (waybill.Status == WaybillStatus.Hold)
                    throw new PXException(Descriptor.Messages.CannotProcessHoldStatus);

                if (!(waybill.Status == WaybillStatus.Open && waybill.WaybillStatus == null && waybill.WaybillState == null && waybill.WaybillCreateDate == null && waybill.WaybillActivationDate == null))
                    throw new PXException(Descriptor.Messages.WaybillAlreadyPosted);

                if (Setup == null || !(Setup.Current.WaybillIsActive ?? false))
                    throw new PXException(Descriptor.Messages.RSIsNotConfigured);

                string waybillID = waybill.WaybillID;

                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    context.CheckServiceUser();

                    var wb = GetExternalWaybill();

                    RESULT result = context.SaveWaybill(wb);

                    if (int.Parse(result.STATUS) >= 0)
                    {
                        waybill.Status = WaybillStatus.Posted;
                        waybill.WaybillStatus = WaybillExtStatus.Saved;
                        waybill.WaybillState = WaybillStates.Receivable;
                        waybill.WaybillID = result.ID;
                        waybillID = result.ID;
                        waybill.PostedByID = Accessinfo.UserID;

                        var wb2 = context.GetWaybill(int.Parse(waybillID));

                        UpdateWaybill(wb2);

                        Actions.PressSave();

                    }
                    else
                    {
                        if (int.Parse(result.STATUS) < 0)
                        {
                            string errors = this.GetGEErrorText(result.STATUS);

                            throw new PXException(errors);
                        }
                    }
                }

                scope.Complete();
            }
        }

        public void CancelWB()
        {
            using (var scope = new PXTransactionScope())
            {
                if (Setup == null || !(Setup.Current.WaybillIsActive ?? false))
                    throw new PXException(Descriptor.Messages.RSIsNotConfigured);

                RSWaybill waybill = Waybills.Current;

                if (waybill.WaybillStatus == WaybillExtStatus.Canceled)
                    throw new PXException(Descriptor.Messages.WaybillAlreadyCanceled);

                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    int result = context.CancelWaybills(string.IsNullOrEmpty(waybill.WaybillID) ? 0 : int.Parse(waybill.WaybillID));
                    string message = WaybillOperations.GenarateStatus(result, waybill.WaybillID, WaybillOperations.Operations.Cancel);
                    if (result < 0)
                    {
                        throw new PXException(message);
                    }

                    waybill.WaybillCancelDate = DateTime.Now;
                    waybill.CanceledByID = Accessinfo.UserID;

                    var wb2 = context.GetWaybill(int.Parse(waybill.WaybillID));

                    UpdateWaybill(wb2);

                    Actions.PressSave();
                }

                scope.Complete();
            }
        }

        public void CloseWB()
        {
            using (var scope = new PXTransactionScope())
            {
                if (Setup == null || !(Setup.Current.WaybillIsActive ?? false))
                    throw new PXException(Descriptor.Messages.RSIsNotConfigured);

                RSWaybill waybill = Waybills.Current;

                if (waybill.WaybillStatus != WaybillExtStatus.Activated && waybill.WaybillType != WaybillType.WithoutTransporation)
                {
                    throw new PXException(Descriptor.Messages.WaybillIsNotActivated);
                }

                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    int result = context.CloseWaybills(string.IsNullOrEmpty(waybill.WaybillID) ? 0 : int.Parse(waybill.WaybillID), waybill.DeliveryDateTime);

                    if (result < 0)
                    {
                        string message = WaybillOperations.GenarateStatus(result, waybill.WaybillID, WaybillOperations.Operations.Close);
                        if (string.IsNullOrEmpty(message))
                            message = this.GetGEErrorText(result.ToString());
                        throw new PXException(message);
                    }

                    //TODO:  Close Correction Case
                    //var wb = context.GetWaybill(int.Parse(waybill.WaybillID));
                    //wb.RECEIVER_INFO = waybill.RecipientInfo;
                    //wb.RECEPTION_INFO = waybill.SupplierInfo;
                    //RESULT result2 = context.SaveWaybill(wb); 

                    waybill.ClosedByID = Accessinfo.UserID;

                    var wb2 = context.GetWaybill(int.Parse(waybill.WaybillID));

                    UpdateWaybill(wb2);

                    Actions.PressSave();
                }

                scope.Complete();
            }
        }

        public void CorrectWB()
        {
            using (var scope = new PXTransactionScope())
            {
                RSWaybill waybill = Waybills.Current;

                var cache = Waybills.Cache;

                cache.SetDefaultExt<RSWaybill.hold>(waybill);
                cache.SetDefaultExt<RSWaybill.status>(waybill);

                var historyGraph = PXGraph.CreateInstance<WaybillHistoryEntry>();

                var wbHistory = historyGraph.Waybills.Insert();
                wbHistory.CopyFrom(waybill);
                wbHistory.WaybillID = null;
                wbHistory.Hold = false;
                wbHistory.Status = WaybillStatus.Open;
                wbHistory.WaybillCorrectionDate = null;
                wbHistory.CorrectedByID = null;
                wbHistory.ClassID = WaybillClass.Sent;
                wbHistory.TotalAmount = 0;
                historyGraph.Waybills.Current = wbHistory;

                var items = WaybillItems.Select().FirstTableItems;

                foreach (RSWaybillItem item in items)
                {
                    var historyItem = new RSWaybillItemHistory();
                    historyItem.CopyFrom(item);
                    historyGraph.WaybillItems.Insert(historyItem);
                }

                historyGraph.Persist();

                waybill.IsCorrected = true;

                Waybills.Cache.Update(waybill);

                Actions.PressSave();

                scope.Complete();
            }
        }

        public void PostCorrectedWB()
        {
            using (var scope = new PXTransactionScope())
            {
                RSWaybill waybill = Waybills.Current;

                if (waybill.Status == WaybillStatus.Hold)
                    throw new PXException(Descriptor.Messages.CannotProcessHoldStatus);

                if (Setup == null || !(Setup.Current.WaybillIsActive ?? false))
                    throw new PXException(Descriptor.Messages.RSIsNotConfigured);

                string waybillID = waybill.WaybillID;

                using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
                {
                    context.CheckServiceUser();

                    var historyGraph = PXGraph.CreateInstance<WaybillHistoryEntry>();

                    var lastHistoryWb = historyGraph.GetLastCorrectedSentWaybillByWaybillNbr(waybill.WaybillNbr);

                    historyGraph.Waybills.Current = lastHistoryWb;

                    var deletedItems = new List<RSWaybillItem>();
                    var curItems = WaybillItems.Select().FirstTableItems;

                    foreach (var item in historyGraph.WaybillItems.Select().FirstTableItems)
                    {
                        if (!curItems.Any(x => x.ItemRowID == item.ItemRowID))
                        {
                            deletedItems.Add(new RSWaybillItem
                            {
                                ItemRowID = item.ItemRowID,
                                ItemRowStatus = RSWaybillItemStatuses.Deleted
                            });
                        }
                    }

                    var wb = GetExternalWaybill(curItems.Union(deletedItems));
                    RESULT result = context.SaveWaybill(wb);

                    if (int.Parse(result.STATUS) < 0)
                    {
                        string errors = this.GetGEErrorText(result.STATUS);

                        throw new PXException(errors);
                    }

                    // Update current waybill
                    var wb2 = context.GetWaybill(int.Parse(waybillID));

                    UpdateWaybill(wb2);

                    waybill.Status = WaybillStatus.Posted;
                    waybill.CorrectedByID = Accessinfo.UserID;

                    var adjustedWaybill = context.GetLastCorrectedWaybill(int.Parse(waybill.WaybillID));

                    if (adjustedWaybill != null)
                    {
                        waybill.WaybillCorrectionDate = adjustedWaybill.DT.ToDateTimeRS();
                    }

                    Waybills.Update(waybill);
                    Actions.PressSave();

                    // Update history waybill
                    lastHistoryWb.Status = WaybillStatus.Posted;
                    lastHistoryWb.Hold = false;
                    lastHistoryWb.CorrectedByID = Accessinfo.UserID;

                    if (adjustedWaybill != null)
                    {
                        lastHistoryWb.WaybillID = adjustedWaybill.ID;
                        lastHistoryWb.WaybillCorrectionDate = waybill.WaybillCorrectionDate;
                    }

                    historyGraph.Waybills.Update(lastHistoryWb);
                    historyGraph.Persist();
                }

                scope.Complete();
            }
        }

        public void UndoCorrectedWB()
        {
            using (var scope = new PXTransactionScope())
            {
                var graph = this;
                var waybill = graph.Waybills.Current;
                if (waybill == null) return;

                var historyGraph = PXGraph.CreateInstance<WaybillHistoryEntry>();

                var oldWaybill = historyGraph.GetLastCorrectedSentWaybillByWaybillNbr(waybill.WaybillNbr);
                historyGraph.Waybills.Current = oldWaybill;

                var waybillItems = graph.WaybillItems.Select().FirstTableItems;
                var waybillItemsHistory = historyGraph.WaybillItems.Select().FirstTableItems;

                var toRemove = waybillItems.Where(x => !waybillItemsHistory.Any(y => y.WaybillItemNbr == x.WaybillItemNbr));

                foreach (var item in toRemove)
                {
                    WaybillItems.Delete(item);
                }

                foreach (var item in waybillItemsHistory)
                {
                    if (waybillItems.Any(x => x.WaybillItemNbr == item.WaybillItemNbr))
                    {
                        var waybillItem = waybillItems.First(x => x.WaybillItemNbr == item.WaybillItemNbr);
                        waybillItem.CopyFrom(item);
                        WaybillItems.Update(waybillItem);
                    }
                    else
                    {
                        var waybillItem = graph.WaybillItems.Insert();
                        waybillItem.CopyFrom(item);
                    }
                }

                var tempWaybillId = waybill.WaybillID;
                var tempCorrectionDate = waybill.WaybillCorrectionDate;
                var tempCorrectedByID = waybill.CorrectedByID;

                waybill.CopyFrom(oldWaybill);
                waybill.Status = WaybillStatus.Posted;
                waybill.Hold = false;
                waybill.WaybillID = tempWaybillId;
                waybill.WaybillCorrectionDate = tempCorrectionDate;
                waybill.CorrectedByID = tempCorrectedByID;

                historyGraph.Waybills.Delete(oldWaybill);
                historyGraph.Persist();

                graph.Waybills.Update(waybill);
                graph.Actions.PressSave();

                scope.Complete();
            }
        }

        public void RefreshWB()
        {
            RSWaybill waybill = Waybills.Current;

            using (var context = new global::RS.Services.Implementation.WaybillService(Setup.Current.WaybillAccount, Setup.Current.WaybillLicence, Setup.Current.WaybillUrl, Setup.Current.WaybillTimeout))
            {
                WAYBILL result = context.GetWaybill(waybill.WaybillID.ToInt(0).Value);

                if (int.Parse(result.STATUS) < 0)
                {
                    string errors = this.GetGEErrorText(result.STATUS);
                    throw new PXException(errors);
                }

                UpdateWaybill(result);

                Actions.PressSave();
            }
        }

        #endregion

        #region Overrides

        protected virtual void RSWaybill_RowDeleted(PXCache sender, PXRowDeletedEventArgs e)
        {
            var row = Waybills.Current;
            if (row == null) return;

            UpdateSource(row, PXEntryStatus.Deleted);
        }


        public override void Persist()
        {
            var row = Waybills.Current;
            if (row == null) return;

            var entryStatus = this.Waybills.Cache.GetStatus(row);

            base.Persist();

            UpdateSource(row, entryStatus);

            SaveDriver();
        }
        #endregion

        #region Helper Methods

        public void UpdateWaybill(WAYBILL rsWaybill)
        {
            try
            {
                var waybill = Waybills.Current;

                waybill.WaybillID = rsWaybill.ID.NullIfEmpty();
                waybill.ParentWaybillID = rsWaybill.PAR_ID.NullIfEmpty();
                waybill.WaybillNumber = rsWaybill.WAYBILL_NUMBER.NullIfEmpty();
                waybill.WaybillType = rsWaybill.TYPE.ToInt();
                waybill.WaybillStatus = rsWaybill.STATUS.ToInt();
                PXGraphHelpers.SetWaybillState(waybill, rsWaybill.IS_CONFIRMED);
                waybill.WaybillCreateDate = rsWaybill.CREATE_DATE.ToDateTimeRS();
                waybill.WaybillActivationDate = rsWaybill.ACTIVATE_DATE.ToDateTimeRS();
                waybill.WaybillCloseDate = rsWaybill.CLOSE_DATE.ToDateTimeRS();

                waybill.RecipientName = rsWaybill.BUYER_NAME.NullIfEmpty();
                waybill.RecipientTaxRegistrationID = rsWaybill.BUYER_TIN.NullIfEmpty();
                waybill.RecipientIsForeignCitizen = rsWaybill.CHEK_BUYER_TIN != "1";
                waybill.RecipientInfo = rsWaybill.RECEIVER_INFO.NullIfEmpty();
                waybill.SourceAddress = rsWaybill.START_ADDRESS.NullIfEmpty();
                waybill.ShippingCost = rsWaybill.TRANSPORT_COAST.ToDecimalRS();
                waybill.SupplierTaxRegistrationID = rsWaybill.SELLER_TIN.NullIfEmpty();
                waybill.SupplierName = rsWaybill.SELLER_NAME.NullIfEmpty();
                waybill.SupplierInfo = rsWaybill.RECEPTION_INFO.NullIfEmpty();
                waybill.DestinationAddress = rsWaybill.END_ADDRESS.NullIfEmpty();
                waybill.DriverIsForeignCitizen = rsWaybill.CHEK_DRIVER_TIN != "1";

                if (!rsWaybill.DELIVERY_DATE.IsNullOrEmpty())
                {
                    var deliveryDate = DateTime.Parse(rsWaybill.DELIVERY_DATE);

                    waybill.DeliveryDate = deliveryDate.Date;
                    waybill.DeliveryTime = (int)deliveryDate.TimeOfDay.TotalMinutes;
                }

                waybill.DriverName = rsWaybill.DRIVER_NAME.NullIfEmpty();
                if (!rsWaybill.DRIVER_TIN.IsNullOrEmpty()) { waybill.DriverUID = rsWaybill.DRIVER_TIN; }
                waybill.Trailer = rsWaybill.Trailer.NullIfEmpty();
                waybill.CarRegistrationNumber = rsWaybill.CAR_NUMBER.NullIfEmpty();
                waybill.CarrierTaxRegistrationID = rsWaybill.TRANSPORTER_TIN.NullIfEmpty();
                waybill.CarrierInfo = rsWaybill.TRANSPORTER_NAME.NullIfEmpty();

                if (!rsWaybill.BEGIN_DATE.IsNullOrEmpty())
                {
                    var transportationStartDate = DateTime.Parse(rsWaybill.BEGIN_DATE);
                    waybill.TransStartDate = transportationStartDate.Date;
                    waybill.TransStartTime = (int)transportationStartDate.TimeOfDay.TotalMinutes;
                }

                waybill.ShippingCostPayer = rsWaybill.TRAN_COST_PAYER.ToInt();
                waybill.TransportationType = rsWaybill.TRANS_ID.ToInt();
                waybill.OtherTransportationTypeDescription = rsWaybill.OtherTransText.NullIfEmpty();
                waybill.Comment = rsWaybill.COMMENT.NullIfEmpty();

                Waybills.Cache.Update(waybill);

                var waybillItems = WaybillItems.Select().FirstTableItems;

                foreach (var item in rsWaybill.GOODS_LIST)
                {
                    var wbItem = waybillItems.FirstOrDefault(x => x.ItemCode.Trim() == item.BAR_CODE);

                    if (wbItem != null)
                        wbItem.ItemRowID = item.ID;
                    else
                        wbItem = (RSWaybillItem)WaybillItems.Cache.Insert();

                    wbItem.ItemRowID = item.ID.NullIfEmpty();
                    wbItem.ItemName = item.W_NAME.NullIfEmpty();
                    wbItem.ItemRowStatus = item.STATUS.ToInt(0);
                    wbItem.ItemCode = item.BAR_CODE.NullIfEmpty();
                    wbItem.ExtUOM = item.UNIT_ID.ToInt(0);
                    wbItem.OtherUOMDescription = item.UNIT_TXT.NullIfEmpty();
                    wbItem.ItemQty = item.QUANTITY.ToDecimalRS();
                    wbItem.UnitPrice = item.PRICE.ToDecimalRS();
                    wbItem.ItemAmt = item.AMOUNT.ToDecimalRS();
                    wbItem.ExciseID = item.A_ID.ToInt(0);
                    wbItem.TaxType = item.VAT_TYPE.ToInt(0);

                    WaybillItems.Cache.Update(wbItem);
                }

                PXGraphHelpers.SetNote(this, waybill);
            }
            catch (Exception ex)
            {
                string message = string.Format("Error During Converting Values.\r\n{0}", ex.Message);

                throw new PXException(message);
            }
        }

        private WAYBILL GetExternalWaybill(IEnumerable<RSWaybillItem> items = null)
        {
            WAYBILL wb = new WAYBILL();
            List<WAYBILLGOODS_LISTGOODS> goodsList = new List<WAYBILLGOODS_LISTGOODS>();

            var waybill = Waybills.Current;
            const string DateFormat = "yyyy-MM-ddTHH:mm:ss";
            var culture = CultureInfo.InvariantCulture;

            wb.ID = string.IsNullOrEmpty(waybill.WaybillID) ? "0" : waybill.WaybillID;
            wb.PAR_ID = waybill.ParentWaybillID;
            wb.WAYBILL_NUMBER = string.IsNullOrEmpty(waybill.WaybillNumber) ? "0" : waybill.WaybillNumber;
            wb.TYPE = waybill.WaybillType.HasValue ? waybill.WaybillType.Value.ToString() : "";
            wb.STATUS = waybill.GenerateExternalWaybillStatus();
            wb.CREATE_DATE = waybill.WaybillCreateDate.HasValue ? waybill.WaybillCreateDate.Value.ToString(DateFormat) : "";
            wb.ACTIVATE_DATE = waybill.WaybillActivationDate.HasValue ? waybill.WaybillActivationDate.Value.ToString(DateFormat) : "";
            wb.CLOSE_DATE = waybill.WaybillCloseDate.HasValue ? waybill.WaybillCloseDate.Value.ToString(DateFormat) : "";
            wb.BUYER_NAME = waybill.RecipientName;
            wb.BUYER_TIN = waybill.RecipientTaxRegistrationID;
            wb.CHEK_BUYER_TIN = waybill.RecipientIsForeignCitizen.GetValueOrDefault() ? "0" : "1";
            wb.RECEIVER_INFO = waybill.RecipientInfo;
            wb.START_ADDRESS = waybill.SourceAddress;
            wb.TRANSPORT_COAST = waybill.ShippingCost?.ToString(culture);
            wb.SELLER_TIN = waybill.SupplierTaxRegistrationID;
            wb.SELLER_NAME = waybill.SupplierName;
            wb.RECEPTION_INFO = waybill.SupplierInfo;
            wb.END_ADDRESS = waybill.DestinationAddress;
            wb.CHEK_DRIVER_TIN = waybill.DriverIsForeignCitizen.GetValueOrDefault() ? "0" : "1";
            wb.DELIVERY_DATE = waybill.DeliveryDateTime.HasValue ? waybill.DeliveryDateTime.Value.ToString(DateFormat) : "";
            wb.DRIVER_NAME = waybill.DriverName;
            wb.DRIVER_TIN = waybill.DriverUID;
            wb.CAR_NUMBER = waybill.CarRegistrationNumber;
            wb.BEGIN_DATE = waybill.TransportationStartDateTime.HasValue ? waybill.TransportationStartDateTime.Value.ToString(DateFormat) : "";
            wb.TRAN_COST_PAYER = waybill.ShippingCostPayer.HasValue ? waybill.ShippingCostPayer.Value.ToString() : "";
            wb.TRANS_ID = waybill.TransportationType.HasValue ? waybill.TransportationType.Value.ToString() : "";
            wb.OtherTransText = waybill.OtherTransportationTypeDescription;
            wb.Trailer = waybill.Trailer;
            wb.COMMENT = waybill.Comment;
            wb.TRANSPORTER_TIN = waybill.CarrierTaxRegistrationID;
            wb.TRANSPORTER_NAME = waybill.CarrierInfo;

            decimal fullAmount = 0;

            foreach (var item in items ?? WaybillItems.Select().FirstTableItems)
            {
                var wbitem = new WAYBILLGOODS_LISTGOODS();
                wbitem.ID = item.ItemRowID;
                wbitem.W_NAME = item.ItemName;
                wbitem.STATUS = item.ItemRowStatus?.ToString() ?? RSWaybillItemStatuses.Active.ToString();
                wbitem.BAR_CODE = item.ItemCode;
                wbitem.UNIT_ID = item.ExtUOM?.ToString() ?? "";
                wbitem.UNIT_TXT = item.OtherUOMDescription;
                wbitem.QUANTITY = item.ItemQty?.ToString(culture) ?? "";
                wbitem.PRICE = item.UnitPrice?.ToString(culture) ?? "";
                wbitem.AMOUNT = item.ItemAmt?.ToString(culture) ?? "";
                wbitem.A_ID = item.ExciseID?.ToString() ?? "0";
                wbitem.VAT_TYPE = item.TaxType?.ToString() ?? "";
                goodsList.Add(wbitem);

                if (item.ItemAmt.HasValue)
                    fullAmount += item.ItemAmt.Value;
            }
            wb.GOODS_LIST = goodsList.ToArray();
            wb.FULL_AMOUNT = fullAmount.ToString(culture);

            return wb;
        }

        private void CreateReceivedWaybill(RSWaybill waybill)
        {
            if (waybill.WaybillType == WaybillType.Internal && !waybill.IsCarrierVehicle())
            {
                var recievedGraph = PXGraph.CreateInstance<ReceivedWaybillEntry>();
                var receivedWaybill = recievedGraph.Waybills.Insert();

                receivedWaybill.CopyFrom(waybill);
                recievedGraph.Waybills.Current = receivedWaybill;

                foreach (RSWaybillItem item in WaybillItems.Select())
                {
                    var ReceivedWaybillItem = recievedGraph.WaybillItems.Insert();
                    ReceivedWaybillItem.CopyFrom(item);
                }

                PXGraphHelpers.SetNote(recievedGraph, receivedWaybill);

                recievedGraph.Actions.PressSave();
            }
        }

        private void SaveDriver()
        {
            var waybill = Waybills.Current;
            if (waybill == null) return;

            if (!string.IsNullOrEmpty(waybill.DriverUID))
            {
                DriverMaint driverMaintGraph = PXGraph.CreateInstance<DriverMaint>();
                RSDriver driver = PXGraphHelpers.GetDriverByUID(this, waybill.DriverUID);

                if (driver == null)
                    driverMaintGraph.Drivers.Insert(new RSDriver
                    {
                        DriverName = waybill.DriverName,
                        DriverTaxID = waybill.DriverUID,
                        DefaultCarNumber = waybill.CarRegistrationNumber,
                        IsForeignCitizen = waybill.DriverIsForeignCitizen
                    });
                else
                {
                    driver.DriverName = waybill.DriverName;
                    driver.DriverTaxID = waybill.DriverUID;
                    driver.DefaultCarNumber = waybill.CarRegistrationNumber;
                    driver.IsForeignCitizen = waybill.DriverIsForeignCitizen;
                    driverMaintGraph.Drivers.Update(driver);
                }
                driverMaintGraph.Actions.PressSave();
            }
        }

        private void ClearWaybillForm(PXCache cache, RSWaybill row)
        {
            cache.SetDefaultExt<RSWaybill.transportationType>(row);
            cache.SetDefaultExt<RSWaybill.sourceAddress>(row);
            cache.SetDefaultExt<RSWaybill.destinationAddress>(row);
            cache.SetDefaultExt<RSWaybill.customerID>(row);
            cache.SetDefaultExt<RSWaybill.recipientDestinationAddress>(row);
            cache.SetDefaultExt<RSWaybill.recipientName>(row);
            cache.SetDefaultExt<RSWaybill.recipientInfo>(row);
            cache.SetDefaultExt<RSWaybill.shippingCostPayer>(row);
            cache.SetDefaultExt<RSWaybill.recipientIsForeignCitizen>(row);
            cache.SetDefaultExt<RSWaybill.supplierInfo>(row);
            cache.SetDefaultExt<RSWaybill.supplierDestinationAddress>(row);
            cache.SetDefaultExt<RSWaybill.deliveryDate>(row);
            cache.SetDefaultExt<RSWaybill.comment>(row);
            ClearDriverForm(cache, row);
        }

        private void ClearDriverForm(PXCache cache, RSWaybill row)
        {
            cache.SetDefaultExt<RSWaybill.driverUID>(row);
            cache.SetDefaultExt<RSWaybill.driverName>(row);
            cache.SetDefaultExt<RSWaybill.carRegistrationNumber>(row);
            cache.SetDefaultExt<RSWaybill.trailer>(row);
            cache.SetDefaultExt<RSWaybill.driverIsForeignCitizen>(row);
            cache.SetDefaultExt<RSWaybill.shippingCost>(row);
            cache.SetDefaultExt<RSWaybill.shippingCostPayer>(row);
            cache.SetDefaultExt<RSWaybill.otherTransportationTypeDescription>(row);
            cache.SetDefaultExt<RSWaybill.carrierInfo>(row);
            cache.SetDefaultExt<RSWaybill.carrierTaxRegistrationID>(row);
        }

        private void ClearWaybillItemRow(PXCache cache, RSWaybillItem row)
        {
            cache.SetDefaultExt<RSWaybillItem.itemExtraQty>(row);
            cache.SetDefaultExt<RSWaybillItem.itemQty>(row);
            cache.SetDefaultExt<RSWaybillItem.itemCode>(row);
            cache.SetDefaultExt<RSWaybillItem.inventoryID>(row);
            cache.SetDefaultExt<RSWaybillItem.fixedAssetID>(row);
            cache.SetDefaultExt<RSWaybillItem.itemName>(row);
            cache.SetDefaultExt<RSWaybillItem.unitPrice>(row);
            cache.SetDefaultExt<RSWaybillItem.itemAmt>(row);
            cache.SetDefaultExt<RSWaybillItem.taxType>(row);
            cache.SetValueExt<RSWaybillItem.extUOM>(row, null);
        }

        private void UpdateSource(RSWaybill waybill, PXEntryStatus entryStatus)
        {
            switch (waybill.SourceType)
            {
                case WaybillSourceType.LCABulkRegistration:

                    var lcaRegGraph = PXGraph.CreateInstance<LCARegistrationEntry>();
                    lcaRegGraph.LCARegistration.Current = PXSelect<EALCARegistration,
                      Where<EALCARegistration.lCARegistrationCD, Equal<Required<EALCARegistration.lCARegistrationCD>>>>.Select(this, waybill.SourceNbr);

                    if (entryStatus == PXEntryStatus.Inserted && lcaRegGraph.LCARegistration.Current != null)
                    {
                        lcaRegGraph.LCARegistration.Current.WaybillRefNbr = waybill.WaybillNbr;
                        lcaRegGraph.LCARegistration.Update(lcaRegGraph.LCARegistration.Current);
                        lcaRegGraph.Save.Press();
                    }

                    else if (entryStatus == PXEntryStatus.Deleted && lcaRegGraph.LCARegistration.Current != null)
                    {
                        lcaRegGraph.LCARegistration.Current.WaybillRefNbr = null;
                        lcaRegGraph.LCARegistration.Update(lcaRegGraph.LCARegistration.Current);
                        lcaRegGraph.Save.Press();
                    }

                    break;
                case WaybillSourceType.LCABulkTransfer:

                    var lCATransferGraph = PXGraph.CreateInstance<LCATransferEntry>();
                    lCATransferGraph.Document.Current = PXSelect<EALCATransfer,
                      Where<EALCATransfer.lCATransferCD, Equal<Required<EALCATransfer.lCATransferCD>>>>.Select(this, waybill.SourceNbr);

                    if (entryStatus == PXEntryStatus.Inserted && lCATransferGraph.Document.Current != null)
                    {
                        lCATransferGraph.Document.Current.WaybillRefNbr = waybill.WaybillNbr;
                        lCATransferGraph.Document.Update(lCATransferGraph.Document.Current);

                        var assetGraph = PXGraph.CreateInstance<AssetEntry>();
                        foreach (EALCATransferDetail item in lCATransferGraph.Details.Select())
                        {
                            assetGraph.Clear();

                            assetGraph.Assets.Current = assetGraph.FindById(item.AssetID.Value);
                            if (assetGraph.Assets.Current != null)
                            {
                                var assetWaybill = assetGraph.Waybills.Insert();
                                assetWaybill.WaybillCD = waybill.WaybillNbr;
                                assetGraph.Save.Press();
                            }
                        }

                        lCATransferGraph.Save.Press();
                    }
                    else if (entryStatus == PXEntryStatus.Deleted && lCATransferGraph.Document.Current != null)
                    {
                        lCATransferGraph.Document.Current.WaybillRefNbr = null;
                        lCATransferGraph.Document.Update(lCATransferGraph.Document.Current);

                        var assetGraph = PXGraph.CreateInstance<AssetEntry>();
                        foreach (EALCATransferDetail item in lCATransferGraph.Details.Select())
                        {
                            assetGraph.Clear();

                            assetGraph.Assets.Current = assetGraph.FindById(item.AssetID.Value);
                            if (assetGraph.Assets.Current != null)
                            {
                                var assetWaybill = assetGraph.Waybills.Select().FirstTableItems.FirstOrDefault(m => m.WaybillCD == waybill.WaybillNbr);
                                if (assetWaybill != null)
                                {
                                    assetGraph.Waybills.Delete(assetWaybill);
                                    assetGraph.Save.Press();
                                }

                            }
                        }
                        lCATransferGraph.Save.Press();
                    }
                    break;
                case WaybillSourceType.InventoryTransfer:

                    var inventoryTransferGraph = PXGraph.CreateInstance<INTransferEntry>();

                    inventoryTransferGraph.transfer.Current = PXSelect<INRegister,
                      Where<INRegister.refNbr, Equal<Required<INRegister.refNbr>>,
                      And<INRegister.docType, Equal<INDocType.transfer>>>>.Select(this, waybill.SourceNbr);

                    if (entryStatus == PXEntryStatus.Inserted && inventoryTransferGraph.transfer.Current != null)
                    {
                        var extTransfer = inventoryTransferGraph.transfer.Current.GetExtension<INRegisterExt>();
                        extTransfer.UsrWaybillRefNbr = waybill.WaybillNbr;
                        inventoryTransferGraph.transfer.Update(inventoryTransferGraph.transfer.Current);
                        inventoryTransferGraph.Save.Press();

                    }
                    else if (entryStatus == PXEntryStatus.Deleted && inventoryTransferGraph.transfer.Current != null)
                    {
                        var extTransfer = inventoryTransferGraph.transfer.Current.GetExtension<INRegisterExt>();
                        extTransfer.UsrWaybillRefNbr = null;
                        inventoryTransferGraph.transfer.Update(inventoryTransferGraph.transfer.Current);
                        inventoryTransferGraph.Save.Press();
                    }
                    break;

                case WaybillSourceType.InventoryIssues:

                    var inventoryIssueGraph = PXGraph.CreateInstance<INIssueEntry>();

                    inventoryIssueGraph.issue.Current = PXSelect<INRegister,
                      Where<INRegister.refNbr, Equal<Required<INRegister.refNbr>>,
                      And<INRegister.docType, Equal<INDocType.issue>>>>.Select(this, waybill.SourceNbr);

                    if (entryStatus == PXEntryStatus.Inserted && inventoryIssueGraph.issue.Current != null)
                    {
                        var extTransfer = inventoryIssueGraph.issue.Current.GetExtension<INRegisterExt>();
                        extTransfer.UsrWaybillRefNbr = waybill.WaybillNbr;
                        inventoryIssueGraph.issue.Update(inventoryIssueGraph.issue.Current);
                        inventoryIssueGraph.Save.Press();
                    }
                    else if (entryStatus == PXEntryStatus.Deleted && inventoryIssueGraph.issue.Current != null)
                    {
                        var extTransfer = inventoryIssueGraph.issue.Current.GetExtension<INRegisterExt>();
                        extTransfer.UsrWaybillRefNbr = null;
                        inventoryIssueGraph.issue.Update(inventoryIssueGraph.issue.Current);
                        inventoryIssueGraph.Save.Press();
                    }
                    break;
            }
        }
        #endregion
    }
}
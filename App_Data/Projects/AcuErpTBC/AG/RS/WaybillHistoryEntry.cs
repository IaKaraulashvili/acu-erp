using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.CS;
using AG.RS.Helpers;
using System.Linq;
using AG.RS.Shared;
using AG.Utils.Extensions;
using AG.EA;
using AG.EA.DAC;
using PX.Objects.IN;

namespace AG.RS
{
    public class WaybillHistoryEntry : AGGraph<WaybillHistoryEntry, RSWaybillHistory>
    {
        public PXSetup<RSSetup> Setup;

        public PXSelect<RSWaybillHistory, Where<RSWaybillHistory.waybillNbr, Equal<Optional<RSWaybillHistory.waybillNbr>>,
            And<RSWaybillHistory.classID, Equal<Optional<RSWaybillHistory.classID>>>>> Waybills;

        // public PXSelect<RSWaybillHistory> WaybillHistory;

        public PXSelect<RSWaybillItemHistory,
            Where<RSWaybillItemHistory.waybillHistoryID, Equal<Current<RSWaybillHistory.waybillHistoryID>>>> WaybillItems;
        public PXSetup<FeaturesSet> FeaturesSet;

        public WaybillHistoryEntry()
        {
            var cur = Setup.Current;
        }

        #region Page Events

        protected virtual void RSWaybillHistory_SupplierDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybillHistory)e.Row;

            if (row != null)
                row.DestinationAddress = row.SupplierDestinationAddress;
        }

        protected virtual void RSWaybillHistory_RecipientDestinationAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybillHistory)e.Row;

            if (row != null)
            {
                row.DestinationAddress = row.RecipientDestinationAddress;

                if (row.WaybillType == WaybillType.WithoutTransporation && row.SourceAddress != row.DestinationAddress)
                {
                    sender.SetValueExt<RSWaybillHistory.sourceAddress>(row, row.RecipientDestinationAddress);
                }
            }
        }

        protected virtual void RSWaybillHistory_SourceAddress_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (RSWaybillHistory)e.Row;

            if (row != null)
            {
                if (row.WaybillType == WaybillType.WithoutTransporation && row.SourceAddress != row.DestinationAddress)
                {
                    sender.SetValueExt<RSWaybillHistory.recipientDestinationAddress>(row, row.SourceAddress);
                }
            }
        }

        protected virtual void RSWaybillHistory_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {
            var row = (RSWaybillHistory)e.Row;
            SetDestinationAddress(row);
        }

        protected virtual void RSWaybillHistory_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (RSWaybillHistory)e.Row;

            if (row == null) return;

            SetDestinationAddress(row);

            var isInternal = row.WaybillType == WaybillType.Internal;
            var isWithoutTrans = row.WaybillType == WaybillType.WithoutTransporation;
            var isDistribution = row.WaybillType == WaybillType.Distribution;
            var isCarrierVehicle = row.TransportationType == TransportationType.Carrier_Vehicle;

            bool needDriverInfo =
                           !isWithoutTrans
                           && (
                               row.TransportationType == TransportationType.Vehicle
                               || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                               || isCarrierVehicle
                           );

            var needShippingCostPayer =
                isInternal && row.TransportationType == TransportationType.Vehicle_ForeignCountry
                || (
                        row.WaybillType == WaybillType.Transporation
                        || row.WaybillType == WaybillType.Distribution
                        || row.WaybillType == WaybillType.Rtrn
                    )
                    && (
                        row.TransportationType == TransportationType.Vehicle
                        || row.TransportationType == TransportationType.Vehicle_ForeignCountry
                        || isCarrierVehicle
                       );

            bool needCarrierInfo = !isWithoutTrans && isCarrierVehicle;

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.status>(sender, row, row.ClassID == WaybillClass.Sent);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.hold>(sender, row, false);


            PXUIFieldAttribute.SetVisible<RSWaybillHistory.driverUID>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.driverName>(sender, row, needDriverInfo);

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.waybillCloseDate>(sender, row, row.WaybillCloseDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.closedByID>(sender, row, row.ClosedByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.waybillCancelDate>(sender, row, row.WaybillCancelDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.canceledByID>(sender, row, row.CanceledByID.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.waybillCorrectionDate>(sender, row, row.WaybillCorrectionDate.HasValue);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.correctedByID>(sender, row, row.CorrectedByID.HasValue);

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.driverIsForeignCitizen>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.shippingCost>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.carRegistrationNumber>(sender, row, needDriverInfo);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.trailer>(sender, row, needDriverInfo);

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.otherTransportationTypeDescription>(sender, row, row.TransportationType == TransportationType.Other);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.supplierDestinationAddress>(sender, row, isInternal);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.shippingCostPayer>(sender, row, needShippingCostPayer);

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.carrierTaxRegistrationID>(sender, row, needCarrierInfo);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.carrierInfo>(sender, row, needCarrierInfo);

            // Hide Buyer(Recipient) Panel
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.recipientInfo>(sender, row, !isInternal);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.recipientIsForeignCitizen>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.recipientName>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.customerID>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.recipientTaxRegistrationID>(sender, row, !isInternal && !isDistribution);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.recipientDestinationAddress>(sender, row, !isInternal && !isDistribution && !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.transportationType>(sender, row, !isWithoutTrans);

            PXUIFieldAttribute.SetVisible<RSWaybillHistory.transStartDate>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.transStartTime>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.deliveryTime>(sender, row, !isWithoutTrans);
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.deliveryDate>(sender, row, !isWithoutTrans);

            bool hasMultiBranch = FeaturesSet.Current.Branch ?? false;
            PXUIFieldAttribute.SetVisible<RSWaybillHistory.branchID>(sender, row, hasMultiBranch);

            Insert.SetVisible(false);
            Save.SetVisible(false);
            Delete.SetVisible(false);
        }

        #endregion

        public override void Persist()
        {
            var row = Waybills.Current;
            if (row == null || row.ClassID == WaybillClass.Sent)
            {
                base.Persist(); return;
            }

            using (var scope = new PXTransactionScope())
            {
                var graph = PXGraph.CreateInstance<ReceivedWaybillEntry>();

                var rwb = graph.GetByWaybillNumber(row.WaybillNumber);

                if (rwb == null)
                {
                    throw new PXException(AG.RS.Descriptor.Messages.ReceivedWaybillDoesNotExist);
                }

                graph.Waybills.Current = rwb;

                if (rwb.WaybillCorrectionDate == null || rwb.WaybillCorrectionDate < row.WaybillCorrectionDate)
                {
                    rwb.WaybillCorrectionDate = row.WaybillCorrectionDate;
                    rwb.IsCorrected = true;

                    graph.Waybills.Update(rwb);

                    graph.Actions.PressSave();
                }

                base.Persist();

                scope.Complete();
            }
        }
        #region Actions
        public PXAction<RSWaybillHistory> viewSource;
        [PXUIField(DisplayName = "View Source", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXButton]
        public virtual void ViewSource()
        {
            var row = Waybills.Current;
            if (row == null || row.SourceNbr.IsNullOrEmpty()) return;
            PXGraphHelpers.ViewWaybillSource(this, row.SourceType, row.SourceNbr);
        }
        #endregion
        #region Helpers

        private void SetDestinationAddress(RSWaybillHistory row)
        {
            if (row != null)
            {
                if (!string.IsNullOrEmpty(row.DestinationAddress))
                {
                    if (row.WaybillType == WaybillType.Internal && string.IsNullOrEmpty(row.SupplierDestinationAddress))
                        row.SupplierDestinationAddress = row.DestinationAddress;
                    else if (row.WaybillType != WaybillType.Internal && string.IsNullOrEmpty(row.RecipientDestinationAddress))
                        row.RecipientDestinationAddress = row.DestinationAddress;
                }
            }
        }

        public RSWaybillHistory GetLastCorrectedSentWaybillByWaybillNbr(string waybillNbr)
        {
            return PXSelect<RSWaybillHistory,
                        Where<RSWaybillHistory.waybillNbr, Equal<Required<RSWaybill.waybillNbr>>,
                            And<RSWaybillHistory.classID, Equal<WaybillClass.sent>>>,
                        OrderBy<Desc<RSWaybillHistory.createdDateTime>>>
                        .Select(this, waybillNbr).FirstTableItems.FirstOrDefault();
        }

        #endregion
    }
}
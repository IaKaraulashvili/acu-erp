﻿using PX.Data;

namespace AG.Common
{
    public static class DACMapper
    {
        public static void Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            var srcType = typeof(TSource);

            var destType = typeof(TDestination);
            var propInfos = destType.GetProperties();

            foreach (var item in propInfos) 
            {
                var prop = destType.GetProperty(item.Name);
                var attrs = prop.GetCustomAttributes(false);
                var canCopy = true;

                for (int i = 0; i < attrs.Length; i++)
                {
                    var attr = attrs[i];

                    if (attr is PXDBIdentityAttribute
                       || attr is PXDBDefaultAttribute
                       || attr is PX.Objects.CS.AutoNumberAttribute
                       || attr is PXDBTimestampAttribute
                       || attr is PXDBCreatedByIDAttribute
                       || attr is PXDBCreatedByScreenIDAttribute
                       || attr is PXDBCreatedDateTimeAttribute
                       || attr is PXDBLastModifiedByIDAttribute
                       || attr is PXDBLastModifiedByScreenIDAttribute
                       || attr is PXDBLastModifiedDateTimeAttribute
                       || attr is PXNoteAttribute)
                    {
                        canCopy = false;
                        break;
                    }
                }

                if (canCopy)
                {
                    var srcProp = srcType.GetProperty(item.Name);

                    if (srcProp != null)
                    {
                        if (prop.CanWrite)
                            prop.SetValue(destination, srcProp.GetValue(source, null), null);
                    }
                }
            }
        }
    }
}

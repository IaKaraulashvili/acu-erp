﻿using PX.Common;

namespace AG.Common
{
    [PXLocalizable(Messages.Prefix)]
    public static class Messages
    {
        public const string Prefix = "";
        public const string OnlyGELIsAllowed = "Only GEL is allowed!";
        public const string LegalFormID = "This Legal Form ID already exists in the system!";
        public const string ContractNbrAlreadyExists = "Contract with this number already exists";
        public const string CurrenciesDoNotMatch = "Currencies do not match";
        public const string LCALowCostAssetError = " LCA Cannot be issued with stock";
        public const string AssetIsNotOpen = "Couldn't release! Asset status is not open.";
        public const string AssetInappropriateStatus = "Can't perform this action, asset status is inappropriate!";
        public const string FARelatedEmpty = "Add FixedAsset record!";
        public const string InappropriateStatusForAction = "Can't perform this action, asset status is inappropriate!";

        public const string TaxRegistrationID = "This user already exists in the system";
        public const string DefaultAccount = "IBAN Account with such name already exists";
        public const string OneDefaultIBANAccount = "You can have only one default IBAN Account.";
        public const string TaxRegistrationIDCannotBeEmpty = "Tax Registration ID cannot be empty.";
        public const string LegalFormIDNotCorrespondCountry = "Legal Form not correspond to the country!";
        public const string INIssueSourseAddress = "Source Address Could Not Be Different, Please Select Warehouse Location Source Address";
        public const string DetailsAreEmpty = "Details are empty!";
        public const string WaybillAlreadyCreatedFromLCABulkReg = "Waybill already created from LCA Bulk Registraiton";
        public const string POOrderPrepaymentAmountZero = "Prepayment Amount Cannot be zero!";
        public const string POOrderPrepaymentAmountMore = "Prepayment Amount Cannot be more than Order Total!";
        public const string APInvoicePrepaymentAmount = "AP Balance Cannot be more than Prepayment Amount!";

        public const string BugetOwnerDepartment = "Buget Owner Department cannot be empty!";

    }
}

using AG.Extensions.AP.DAC;
using PX.Data;
using PX.Objects.CS;

namespace AG.Extensions.AP
{
    public class AGBankMaint : AGGraph<AGBankMaint>
    {
        public PXSave<AGBank> Save;
        public PXCancel<AGBank> Cancel;

        public PXSelect<AGBank> Banks;



        public PXAction<AGBank> ViewCountry;
        [PXButton]
        public virtual void viewCountry()
        {
            AGBank row = Banks.Current;
            CountryMaint grap = CreateInstance<CountryMaint>();
            var ss = grap.Country.Search<Country.countryID>(row.CountryID);
            grap.Country.Current = grap.Country.Search<Country.countryID>(row.CountryID);
            if (grap.Country.Current != null)
                throw new PXRedirectRequiredException(grap, true, "Country");
        }

        protected virtual void AGBank_SwiftCode_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (AGBank)e.Row;
            foreach (AGBank line in Banks.Select())
            {
                if (row.BankID == line.BankID)
                    continue;

                if (row.SwiftCode == line.SwiftCode)
                     throw new PXException("This bank already exists in the system");
            }
        }
    }
}
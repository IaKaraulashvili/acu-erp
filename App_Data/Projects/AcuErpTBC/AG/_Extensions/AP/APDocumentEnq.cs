using System.Collections;
using PX.Data;
using static PX.Objects.AP.APDocumentEnq;
using AG.Extensions.PO.DAC;

namespace PX.Objects.AP
{
    public class APDocumentEnq_Extension : PXGraphExtension<APDocumentEnq>
    {
        #region Event Handlers
        [PXFilterable]
        public PXSelectJoinOrderBy<
            APDocumentResult,
            LeftJoin<APInvoice,
                On<APDocumentResult.docType, Equal<APInvoice.docType>,
                And<APDocumentResult.refNbr, Equal<APInvoice.refNbr>>>,
                LeftJoin<AGContract, On<APRegisterExt.usrContractID, Equal<AGContract.contractID>>>>,
            OrderBy<Desc<APDocumentResult.docDate>>> Documents;

        public virtual IEnumerable documents()
        {
            var filterExt = Base.Filter.Current.GetExtension<APDocumentFilterExt>();

            foreach (var item in Base.Documents.Select())
            {
                if(filterExt.UsrContractID != null)
                {
                    if (PXCache<APRegister>.GetExtension<APRegisterExt>(item).UsrContractID != filterExt.UsrContractID)
                        continue;
                }                  
                yield return item;
            }
        }

        protected virtual void APDocumentFilter_VendorID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (APDocumentFilter)e.Row;
            if (row == null) return;

            var rowExt = row.GetExtension<APDocumentFilterExt>();
            rowExt.UsrContractID = null;
        }
        #endregion
    }
}
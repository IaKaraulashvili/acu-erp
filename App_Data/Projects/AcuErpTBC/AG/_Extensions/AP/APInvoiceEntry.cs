﻿using AG.Extensions.AP.Shared;
using AG.RS;
using AG.RS.DAC;
using AG.RS.Shared;
using PX.Data;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.TX;
using System;
using System.Collections;
using System.Linq;
using AG.Shared;
using AG.DAC;
using AG.Extensions.AP.DAC;

namespace PX.Objects.AP
{
    public class APInvoiceEntry_Extension : PXGraphExtension<APInvoiceEntry>
    {
        #region Data Views

        public PXSelectJoin<RSAPBillReceivedTaxInvoice, InnerJoin<RSReceivedTaxInvoice, On<RSAPBillReceivedTaxInvoice.taxInvoiceID, Equal<RSReceivedTaxInvoice.taxInvoiceID>>>,
                 Where<RSAPBillReceivedTaxInvoice.aPBillRefNbr, Equal<Current<APInvoice.refNbr>>>> ReceivedTaxInvoices;

        public PXSelect<RSReceivedTaxInvoice,
                                        Where<RSReceivedTaxInvoice.vendorID, Equal<Current<APInvoice.vendorID>>,
                                               And<Current<TaxInvoiceFilter.status>, IsNull, Or<RSReceivedTaxInvoice.status, Equal<Current<TaxInvoiceFilter.status>>,
                                               And<Current<TaxInvoiceFilter.financePeriod>, IsNull, Or<RSReceivedTaxInvoice.postPeriod, Equal<Current<TaxInvoiceFilter.financePeriod>>,
                                               And<Current<TaxInvoiceFilter.amountFrom>, IsNull, Or<RSReceivedTaxInvoice.fullAmt, Equal<Current<TaxInvoiceFilter.amountFrom>>,
                                               And<Current<TaxInvoiceFilter.startDate>, IsNull, And<Current<TaxInvoiceFilter.endDate>, IsNull,
                                                     Or<RSReceivedTaxInvoice.createDate, Between<Current<TaxInvoiceFilter.startDate>, Current<TaxInvoiceFilter.endDate>>>>>>>>>>>>> TaxInvoices;

        public IEnumerable taxInvoices()
        {
            var s = Filter.Current.VendorID;

            PXSelectBase<RSReceivedTaxInvoice> query = new PXSelect<RSReceivedTaxInvoice, Where<RSReceivedTaxInvoice.vendorID, Equal<Current<APInvoice.vendorID>>>>(Base);

            if (Filter.Current.CheckAPBill != null && Filter.Current.CheckAPBill == true)
                query = new PXSelectJoin<RSReceivedTaxInvoice, LeftJoin<RSAPBillReceivedTaxInvoice, On<RSReceivedTaxInvoice.taxInvoiceID, Equal<RSAPBillReceivedTaxInvoice.taxInvoiceID>>>,
                                                Where<RSReceivedTaxInvoice.vendorID, Equal<Current<APInvoice.vendorID>>, And<RSAPBillReceivedTaxInvoice.taxInvoiceID, IsNull>>>(Base);


            if (Filter.Current.PrepaymentTaxInvoice.GetValueOrDefault())
                query.WhereAnd<Where<RSReceivedTaxInvoice.prepaymentTaxInvoice, Equal<True>>>();

            if (Filter.Current.Status != null)
                query.WhereAnd<Where<RSReceivedTaxInvoice.status, Equal<Current<TaxInvoiceFilter.status>>>>();

            if (Filter.Current.FinancePeriod != null)
                query.WhereAnd<Where<RSReceivedTaxInvoice.postPeriod, Equal<Current<TaxInvoiceFilter.financePeriod>>>>();

            if (Filter.Current.StartDate != null && Filter.Current.EndDate != null)
                query.WhereAnd<Where<RSReceivedTaxInvoice.createDate, Between<Current<TaxInvoiceFilter.startDate>, Current<TaxInvoiceFilter.endDate>>>>();

            if (Filter.Current.AmountFrom != null && Filter.Current.AmountTo != null)
                query.WhereAnd<Where<RSReceivedTaxInvoice.totalAmt, Between<Current<TaxInvoiceFilter.amountFrom>, Current<TaxInvoiceFilter.amountTo>>>>();

            return query.Select();
        }

        public PXFilter<TaxInvoiceFilter> Filter;

        public PXSelect<AGFARelated,
                    Where<AGFARelated.refNbr, Equal<Current<APInvoice.refNbr>>, And<AGFARelated.type, Equal<AGFARelatedTypes.aPInvoice>>>> FARelateds;

        #endregion

        #region APInvoice event handlers

        protected virtual void TaxInvoiceFilter_PrepaymentTaxInvoice_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            APInvoice apInvoice = Base.Document.Current;
            TaxInvoiceFilter row = e.Row as TaxInvoiceFilter;
            if (apInvoice == null || row == null) return;

            row.PrepaymentTaxInvoice = apInvoice.DocType == APDocType.Prepayment;
        }

        protected virtual void APInvoice_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            APInvoice row = e.Row as APInvoice;
            if (row == null) return;

            var rowExt = row.GetExtension<APRegisterExt>();

            PXUIFieldAttribute.SetVisible<APRegisterExt.usrPrepaymentCloseDate>(cache, row, row.DocType == APDocType.Prepayment);
            PXDefaultAttribute.SetPersistingCheck<APRegisterExt.usrPrepaymentCloseDate>(cache, null, row.DocType == APDocType.Prepayment ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);

            SetVisiblePOSection(cache, row);
        }

        protected virtual void APInvoice_VendorID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (APRegister)e.Row;
            if (row == null) return;

            /* APInvoice has CacheAttached event on VendorID, that replaces all attributes 
               added by default or added by customization. It prevents any logic to be eecuted.
               Here we call FieldUpdated of the attribute manually. */
            new VendorBankAccountCheckAttribute().FieldUpdated(cache, e);

            var rowExt = PXCache<APRegister>.GetExtension<APRegisterExt>(row);
            rowExt.UsrContractID = null;
        }

        protected virtual void APInvoice_UsrAGBankAccountID_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = Base.Document.Current;
            if (row == null) return;
            if (e.NewValue == null) return;

            var bankAccount = (AGBankAccount)PXSelectReadonly<AGBankAccount, Where<AGBankAccount.bankAccountID, Equal<Required<AGBankAccount.bankAccountID>>>>
                 .Select(Base, e.NewValue);

            if (bankAccount?.CurrencyID != row.CuryID)
            {
                e.NewValue = null;
                throw new PXSetPropertyException(AG.Common.Messages.CurrenciesDoNotMatch);
            }
        }

        protected virtual void APInvoice_UsrEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (APRegister)e.Row;
            sender.SetDefaultExt<APRegisterExt.usrDepartment>(row);
        }
        #endregion

        #region FARelated event handlers

        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "RefNbr")]
        [PXDBDefault(typeof(APInvoice.refNbr))]
        protected void AGFARelated_RefNbr_CacheAttached(PXCache cache)
        {

        }

        protected virtual void AGFARelated_AssetCD_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (AGFARelated)e.Row;
            if (row == null) return;

            PXResult<PX.Objects.FA.FixedAsset> asset = PXSelectJoin<FixedAsset,
                        LeftJoin<FADetails, On<FixedAsset.assetID, Equal<FADetails.assetID>>,
                        LeftJoin<FALocationHistory, On<FixedAsset.assetID, Equal<FALocationHistory.assetID>>>>,
                            Where<FixedAsset.assetCD, Equal<Required<FixedAsset.assetCD>>>>.Select(Base, row.AssetCD).FirstOrDefault();


            if (asset != null)
            {

                row.Description = asset.GetItem<FixedAsset>().Description;
                row.AssetClass = asset.GetItem<FixedAsset>().ClassID;
                row.Status = asset.GetItem<FixedAsset>().Status;
                row.UsefulLifeYears = asset.GetItem<FixedAsset>().UsefulLife;
                row.ReceiptDate = asset.GetItem<FADetails>().ReceiptDate;
                row.PlacedInServiceDate = asset.GetItem<FADetails>().DepreciateFromDate;
                row.OrigAcquisitionCost = asset.GetItem<FADetails>().AcquisitionCost;
                row.Department = asset.GetItem<FALocationHistory>().Department;
            }
        }

        #endregion

        #region Actions

        public delegate IEnumerable ReleaseDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable Release(PXAdapter adapter, ReleaseDelegate baseMethod)
        {
            var row = Base.Document.Current;
            var rowExt = Base.Document.Cache.GetExtension<APRegisterExt>(row);
            if (rowExt.UsrPONumber != null && row.CuryLineTotal > rowExt.UsrPOPrepaymentAmt)
                throw new PXException(AG.Common.Messages.APInvoicePrepaymentAmount);

            return baseMethod(adapter);
        }

        public delegate IEnumerable VoidDocumentDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable VendorDocuments(PXAdapter adapter, VoidDocumentDelegate baseMethod)
        {
            if (Base.vendor.Current != null)
            {
                APDocumentEnq graph = PXGraph.CreateInstance<APDocumentEnq>();
                graph.Filter.Current.VendorID = Base.vendor.Current.BAccountID;
                var row = Base.Document.Current;
                if (row != null)
                {
                    var rowExt = Base.Document.Cache.GetExtension<APRegisterExt>(row);
                    graph.Filter.Current.GetExtension<APDocumentFilterExt>().UsrContractID = rowExt?.UsrContractID;
                }

                graph.Filter.Select();
                throw new PXRedirectRequiredException(graph, "Vendor Details");
            }
            return adapter.Get();
        }

        public PXAction<APInvoice> AddReceivedTaxInvoice;
        [PXButton]
        protected void addReceivedTaxInvoice()
        {
            var a = Base.Document.Current;
            Filter.Current.VendorID = a.VendorID;

            if (Filter.AskExt((graph, view) =>
            {
                Filter.Cache.ClearQueryCache();
                Filter.View.Clear();
                Filter.Cache.Clear();

                TaxInvoices.Cache.ClearQueryCache();
                TaxInvoices.View.Clear();
                TaxInvoices.Cache.Clear();
            }, true) == WebDialogResult.OK)
            {
                foreach (var tax in TaxInvoices.Cache.Cached)
                {
                    Filter.Current.VendorID = a.VendorID;
                    RSAPBillReceivedTaxInvoice apibilltax = new RSAPBillReceivedTaxInvoice();
                    if (((AG.RS.DAC.RSReceivedTaxInvoice)tax).Selected == true)
                    {
                        apibilltax.TaxInvoiceID = ((AG.RS.DAC.RSReceivedTaxInvoice)tax).TaxInvoiceID;
                        apibilltax.TaxInvoiceStatus = ((AG.RS.DAC.RSReceivedTaxInvoice)tax).Status;
                        ReceivedTaxInvoices.Cache.Insert(apibilltax);

                        ReceivedTaxInvoiceEntry taxInvoice = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();
                        var Current = ReceivedTaxInvoices.Current;
                        RSReceivedTaxInvoice i = PXSelect<RSReceivedTaxInvoice, Where<RSReceivedTaxInvoice.taxInvoiceID, Equal<Required<RSReceivedTaxInvoice.taxInvoiceID>>>>.Select(Base, Current.TaxInvoiceID);
                        i.Status = "C";
                        taxInvoice.ReceivedTaxInvoice.Update(i);
                    }
                }
            }

        }

        public PXAction<APInvoice> RemoveTaxInvoice;
        [PXButton]
        protected void removeTaxInvoice()
        {
            ReceivedTaxInvoiceEntry taxInvoice = PXGraph.CreateInstance<ReceivedTaxInvoiceEntry>();
            var Current = ReceivedTaxInvoices.Current;
            RSReceivedTaxInvoice i = PXSelectReadonly<RSReceivedTaxInvoice, Where<RSReceivedTaxInvoice.taxInvoiceID, Equal<Required<RSReceivedTaxInvoice.taxInvoiceID>>>>.Select(Base, Current.TaxInvoiceID);
            i.Status = ReceivedTaxInvoices.Current.TaxInvoiceStatus;
            taxInvoice.ReceivedTaxInvoice.Update(i);
            taxInvoice.Save.Press();

            ReceivedTaxInvoices.Delete(ReceivedTaxInvoices.Current);
            Base.Actions.PressSave();
        }


        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var row = Base.Document.Current;

            if (row != null)
            {
                var rowExt = Base.Document.Cache.GetExtension<APRegisterExt>(row);

                if ((row.CreatedByScreenID != "PO302000" && row.CreatedByScreenID != "PO501000") && !row.Hold.GetValueOrDefault() && rowExt.UsrDepartment == null)
                {
                    PXCache cache = Base.Caches[typeof(APRegisterExt)];
                    cache.RaiseExceptionHandling<APRegisterExt.usrDepartment>(row, rowExt.UsrDepartment,
                     new PXSetPropertyException(AG.Common.Messages.BugetOwnerDepartment, PXErrorLevel.Error));
                }

                var curTransaction = Base.Transactions.Current;

                if ((row.CreatedByScreenID == "PO302000" || row.CreatedByScreenID == "PO501000") && curTransaction != null)
                {
                    POReceipt receipt = PXSelect<POReceipt, Where<POReceipt.receiptNbr, Equal<Required<POReceipt.receiptNbr>>>>.Select(Base, curTransaction.ReceiptNbr);

                    if (receipt != null && receipt.AutoCreateInvoice.GetValueOrDefault())
                    {
                        var extreceipt = receipt.GetExtension<POReceiptExt>();

                        if (Base.Document.Cache.GetStatus(row) == PXEntryStatus.Inserted)
                        {
                            rowExt.UsrDepartment = extreceipt.UsrDepartment;
                            rowExt.UsrEmployeeID = extreceipt.UsrEmployeeID;
                        }

                        rowExt.UsrContractID = extreceipt.UsrContractID;
                    }

                    var poReceipt = PXSelect<AGFARelated, Where<AGFARelated.refNbr, Equal<Required<POReceipt.receiptNbr>>, And<AGFARelated.type, Equal<AGFARelatedTypes.pOReceipt>>>>.Select(Base, curTransaction.ReceiptNbr);

                    if (poReceipt != null && poReceipt.Count > 0 && FARelateds.Cache.Current == null)
                    {
                        rowExt.UsrFARelated = true;

                        foreach (AGFARelated item in poReceipt)
                        {
                            AGFARelated faRelated = new AGFARelated();
                            faRelated.AssetCD = item.AssetCD;
                            faRelated.RefNbr = row.RefNbr;
                            faRelated.ExpenseAmount = item.ExpenseAmount;
                            FARelateds.Cache.Insert(faRelated);
                        }
                    }
                }

                if (rowExt.UsrFARelated != null)
                {
                    if (rowExt.UsrFARelated.Value && FARelateds.Select().Count == 0)
                        throw new PXException(AG.Common.Messages.FARelatedEmpty);
                    if (!rowExt.UsrFARelated.Value && FARelateds.Select().Count > 0)
                    {
                        foreach (AGFARelated item in FARelateds.Select())
                            FARelateds.Delete(item);
                    }
                }

                if (rowExt?.UsrAGBankAccountID != null)
                {
                    var bankAccount = (AGBankAccount)PXSelectReadonly<AGBankAccount, Where<AGBankAccount.bankAccountID, Equal<Required<AGBankAccount.bankAccountID>>>>
                        .Select(Base, rowExt.UsrAGBankAccountID);

                    if (bankAccount?.CurrencyID != row.CuryID)
                    {
                        rowExt.UsrAGBankAccountID = null;
                        Base.CurrentDocument.Cache.RaiseExceptionHandling<APRegisterExt.usrAGBankAccountID>(row, rowExt.UsrAGBankAccountID, new PXSetPropertyException(AG.Common.Messages.CurrenciesDoNotMatch, PXErrorLevel.Error));
                    }
                }
            }

            baseMethod();
        }

        #endregion

        #region DAC

        [Serializable]
        public class TaxInvoiceFilter : IBqlTable
        {
            #region VendorID
            public abstract class vendorID : IBqlField
            {
            }
            [PXInt]
            [PXDefault]
            public virtual int? VendorID { get; set; }
            #endregion

            #region Finance Period
            public abstract class financePeriod : IBqlField
            {
            }
            protected string _FinancePeriod;
            [FinPeriodID()]
            [PXUIField(DisplayName = "Finance Period")]//
            [PXSelector(typeof(Search<TaxPeriod.taxPeriodID, Where<TaxPeriod.vendorID,
                               Equal<Current<APInvoice.vendorID>>>>), DirtyRead = true)]
            public virtual string FinancePeriod { get; set; }
            #endregion

            #region StartDate
            public abstract class startDate : PX.Data.IBqlField
            {
            }
            protected DateTime? _StartDate;
            [PXDate()]
            [PXUIField(DisplayName = "Start Date", Visibility = PXUIVisibility.Visible)]
            public virtual DateTime? StartDate { get; set; }
            #endregion

            #region EndDate
            public abstract class endDate : PX.Data.IBqlField
            {
            }
            protected DateTime? _EndDate;
            [PXDate()]
            [PXUIField(DisplayName = "End Date", Visibility = PXUIVisibility.Visible)]
            public virtual DateTime? EndDate { get; set; }
            #endregion

            #region AmountFrom
            public abstract class amountFrom : PX.Data.IBqlField
            {
            }
            protected Decimal? _AmountFrom;
            [PXDBDecimal(6)]
            [PXUIField(DisplayName = "Amount From", Visibility = PXUIVisibility.Visible)]
            public virtual Decimal? AmountFrom { get; set; }
            #endregion

            #region AmountTo
            public abstract class amountTo : PX.Data.IBqlField
            {
            }
            protected Decimal? _AmountTo;
            [PXDBDecimal(6)]
            [PXUIField(DisplayName = "Amount To", Visibility = PXUIVisibility.Visible)]
            public virtual Decimal? AmountTo { get; set; }
            #endregion

            #region Status
            public abstract class status : PX.Data.IBqlField
            {
            }
            protected string _Status;
            [PXString(1, IsFixed = true)]
            [PXUIField(DisplayName = "Status")]
            [PXStringList(
                new string[]
                {
                    RecivedTaxInvoiceStatusAttribute.Status.Open,
                    RecivedTaxInvoiceStatusAttribute.Status.Initial,
                    RecivedTaxInvoiceStatusAttribute.Status.Corrected,
                    RecivedTaxInvoiceStatusAttribute.Status.Approved,
                    RecivedTaxInvoiceStatusAttribute.Status.Deleted,
                    RecivedTaxInvoiceStatusAttribute.Status.Rejected

                },
                new string[]
                {
                   "Open",
                   "Initial",
                   "Corrected",
                   "Approved",
                   "Deleted",
                   "Rejected"

                })]
            [PXDefault(RecivedTaxInvoiceStatusAttribute.Status.Open)]
            public virtual string Status
            {
                get
                {
                    return this._Status;
                }
                set
                {
                    this._Status = value;
                }
            }
            #endregion

            #region CheckAPBill
            public abstract class checkAPBill : PX.Data.IBqlField
            {
            }
            protected Boolean? _CheckAPBill;
            [PXBool]
            [PXUIField(DisplayName = "Hide linked with AP Bill", Visibility = PXUIVisibility.Visible)]
            public virtual Boolean? CheckAPBill { get; set; }
            #endregion

            #region PrepaymentTaxInvoice
            public abstract class prepaymentTaxInvoice : PX.Data.IBqlField
            {
            }
            protected Boolean? _PrepaymentTaxInvoice;
            [PXBool]
            [PXUIField(DisplayName = "Prepayment Tax Invoice", Visibility = PXUIVisibility.Visible)]
            public virtual Boolean? PrepaymentTaxInvoice { get; set; }
            #endregion
        }

        #endregion

        private void SetVisiblePOSection(PXCache cache, APRegister register)
        {
            var rowExt = PXCache<APRegister>.GetExtension<APRegisterExt>(register);
            if (!(Base.Document.Current.DocType == APDocType.Prepayment && rowExt.UsrPONumber != null))
            {
                PXUIFieldAttribute.SetVisible<APRegisterExt.usrPONumber>(cache, register, false);
                PXUIFieldAttribute.SetVisible<APRegisterExt.usrPOPrepaymentAmt>(cache, register, false);
            }
        }
    }
}

using AG.Extensions.AP.DAC;
using PX.Data;

namespace PX.Objects.AP
{
    public class APPaymentEntry_Extension : PXGraphExtension<APPaymentEntry>
    {
        #region Event Handlers

        public delegate void CreatePaymentDelegate(APInvoice apdoc);
        [PXOverride]
        public void CreatePayment(APInvoice apdoc, CreatePaymentDelegate baseMethod)
        {
            baseMethod(apdoc);

            var row = Base.Document.Current;
            var ext = row.GetExtension<APRegisterExt>();

            ext.UsrAGBankAccountID = apdoc.GetExtension<APRegisterExt>().UsrAGBankAccountID;
        }

        protected virtual void APPayment_UsrAGBankAccountID_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = Base.Document.Current;
            if (row == null) return;
            if (e.NewValue == null) return;

            var bankAccount = (AGBankAccount)PXSelectReadonly<AGBankAccount, Where<AGBankAccount.bankAccountID, Equal<Required<AGBankAccount.bankAccountID>>>>
                 .Select(Base, e.NewValue);

            if (bankAccount?.CurrencyID != row.CuryID)
            {
                e.NewValue = null;
                throw new PXSetPropertyException(AG.Common.Messages.CurrenciesDoNotMatch);
            }
        }

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var row = Base.Document.Current;
            if (row == null) return;

            var rowExt = Base.Document.Cache.GetExtension<APRegisterExt>(row);

            if (rowExt.UsrAGBankAccountID != null)
            {
                var bankAccount = (AGBankAccount)PXSelectReadonly<AGBankAccount, Where<AGBankAccount.bankAccountID, Equal<Required<AGBankAccount.bankAccountID>>>>
                    .Select(Base, rowExt.UsrAGBankAccountID);

                if (bankAccount?.CurrencyID != row.CuryID)
                {
                    rowExt.UsrAGBankAccountID = null;
                    Base.CurrentDocument.Cache.RaiseExceptionHandling<APRegisterExt.usrAGBankAccountID>(row, rowExt.UsrAGBankAccountID, new PXSetPropertyException(AG.Common.Messages.CurrenciesDoNotMatch, PXErrorLevel.Error));
                }
            }

            baseMethod();
        }

        #endregion
    }
}
﻿namespace AG.Extensions.AP.DAC
{
    using PX.Data;
    using PX.Objects.CS;

    [System.SerializableAttribute()]
	public class AGBank : PX.Data.IBqlTable
	{
		#region BankID
		public abstract class bankID : PX.Data.IBqlField
		{
		}
		protected int? _BankID;
        [PXDBIdentity(IsKey =true)]
        [PXUIField(Enabled = false)]
		public virtual int? BankID
		{
			get
			{
				return this._BankID;
			}
			set
			{
				this._BankID = value;
			}
		}
		#endregion
		#region SwiftCode
		public abstract class swiftCode : PX.Data.IBqlField
		{
		}
		protected string _SwiftCode;
		[PXDBString(15, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Swift Code")]
		public virtual string SwiftCode
		{
			get
			{
				return this._SwiftCode;
			}
			set
			{
				this._SwiftCode = value;
			}
		}
		#endregion
		#region BankName
		public abstract class bankName : PX.Data.IBqlField
		{
		}
		protected string _BankName;
		[PXDBString(60, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Bank Name")]
		public virtual string BankName
		{
			get
			{
				return this._BankName;
			}
			set
			{
				this._BankName = value;
			}
		}
		#endregion
		#region CountryID
		public abstract class countryID : PX.Data.IBqlField
		{
		}
		protected string _CountryID;
		[PXDBString(2, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "CountryID")]
        [PXSelector(typeof(Country.countryID), CacheGlobal = true, DescriptionField = typeof(Country.description))]
        public virtual string CountryID
		{
			get
			{
				return this._CountryID;
			}
			set
			{
				this._CountryID = value;
			}
		}
		#endregion
		#region Address
		public abstract class address : PX.Data.IBqlField
		{
		}
		protected string _Address;
		[PXDBString(255, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Address")]
		public virtual string Address
		{
			get
			{
				return this._Address;
			}
			set
			{
				this._Address = value;
			}
		}
		#endregion
	}
}

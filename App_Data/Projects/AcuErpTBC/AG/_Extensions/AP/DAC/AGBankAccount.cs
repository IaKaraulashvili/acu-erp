﻿namespace AG.Extensions.AP.DAC
{
    using PX.Data;
    using PX.Objects.CM;
    using PX.Objects.AP;
    
    [System.SerializableAttribute()]
	public class AGBankAccount : PX.Data.IBqlTable
	{
		#region BankAccountID
		public abstract class bankAccountID : PX.Data.IBqlField
		{
		}
		protected int? _BankAccountID;
		[PXDBIdentity(IsKey = true)]
		public virtual int? BankAccountID
		{
			get
			{
				return this._BankAccountID;
			}
			set
			{
				this._BankAccountID = value;
			}
		}
        #endregion

        #region BAccountID

        public abstract class bAccountID : PX.Data.IBqlField
		{
		}
		protected int? _BAccountID;
		[PXDBInt()]
        [PXDBDefault(typeof(Vendor.bAccountID))]
        [PXParent(typeof(Select<Vendor, 
                                 Where<Vendor.bAccountID, Equal<Current<AGBankAccount.bAccountID>>>>))]
		public virtual int? BAccountID
        {
			get
			{
				return this._BAccountID;
			}
			set
			{
				this._BAccountID = value;
			}
		}
		#endregion

		#region IBANAccount
		public abstract class iBANAccount : PX.Data.IBqlField
		{
		}
		protected string _IBANAccount;
		[PXDBString(22, IsUnicode = true)]
		[PXDefault]
		[PXUIField(DisplayName = "IBAN Account")]
		public virtual string IBANAccount
		{
			get
			{
				return this._IBANAccount;
			}
			set
			{
				this._IBANAccount = value;
			}
		}
		#endregion

		#region AccountName
		public abstract class accountName : PX.Data.IBqlField
		{
		}
		protected string _AccountName;
		[PXDBString(60, IsUnicode = true)]
	
		[PXUIField(DisplayName = "Account Name")]
		public virtual string AccountName
		{
			get
			{
				return this._AccountName;
			}
			set
			{
				this._AccountName = value;
			}
		}
		#endregion

		#region AGBankID
		public abstract class bankID : PX.Data.IBqlField
		{
		}
		protected int? _BankID;
		[PXDBInt()]
		[PXDefault]
		[PXUIField(DisplayName = "Bank Code")]

        [PXSelector(
            typeof(AGBank.bankID),
            typeof(AGBank.swiftCode),
            typeof(AGBank.bankName),
            SubstituteKey = typeof(AGBank.swiftCode))]
        public virtual int? BankID
		{
			get
			{
				return this._BankID;
			}
			set
			{
				this._BankID = value;
			}
		}

		#endregion  

        #region BankRegistrationID
        public abstract class bankRegistrationID : PX.Data.IBqlField
        {
        }
        protected string _BankRegistrationID;
        [PXDBString(60, IsUnicode = true)]
      //  [PXDefault()]
        [PXUIField(DisplayName = "Bank Registration ID")]
        public virtual string BankRegistrationID
        {
            get
            {
                return this._BankRegistrationID;
            }
            set
            {
                this._BankRegistrationID = value;
            }
        }
        #endregion

		#region CurrencyID
		public abstract class currencyID : PX.Data.IBqlField
		{
		}
		protected string _CurrencyID;
		[PXDBString(5, IsUnicode = true, InputMask = ">LLLLL")]
		[PXDefault()]
        [PXUIField(DisplayName = "Currency ID", Visibility = PXUIVisibility.SelectorVisible)]
        [PXSelector(typeof(CurrencyList.curyID),
            typeof(CurrencyList.curyID))]
        public virtual string CurrencyID
		{
			get
			{
				return this._CurrencyID;
			}
			set
			{
				this._CurrencyID = value;
			}
		}
		#endregion

		#region IsDefault
		public abstract class isDefault : PX.Data.IBqlField
		{
		}
		protected bool? _IsDefault;
		[PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Is Default")]
		public virtual bool? IsDefault
		{
			get
			{
				return this._IsDefault;
			}
			set
			{
				this._IsDefault = value;
			}
		}
        #endregion

        #region IntermediaryBankSwift
        public abstract class intermediaryBankID : PX.Data.IBqlField
        {
        }
        protected int? _IntermediaryBankID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Intermediary Bank Swift")]
        [PXSelector(
            typeof(AGBank.bankID),
            typeof(AGBank.swiftCode),
            typeof(AGBank.bankName),
            SubstituteKey = typeof(AGBank.swiftCode))]
        public virtual int? IntermediaryBankID
        {
            get
            {
                return this._IntermediaryBankID;
            }
            set
            {
                this._IntermediaryBankID = value;
            }
        }
        #endregion

        #region IntermediaryBankName
        public abstract class intermediaryBankName : PX.Data.IBqlField
        {
        }
        protected string _IntermediaryBankName;
        [PXDBString(100, IsUnicode = true)]
        [PXUIField(DisplayName = "Intermediary Bank Name", Enabled = false)]
        public virtual string IntermediaryBankName
        {
            get
            {
                return this._IntermediaryBankName;
            }
            set
            {
                this._IntermediaryBankName = value;
            }
        }
        #endregion
    }
}

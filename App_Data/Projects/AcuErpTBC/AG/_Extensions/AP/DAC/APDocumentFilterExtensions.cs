using AG.Extensions.PO.Shared;
using PX.Data;

namespace PX.Objects.AP
{
    public class APDocumentFilterExt : PXCacheExtension<PX.Objects.AP.APDocumentEnq.APDocumentFilter>
  {
    #region UsrContractID
    [PXInt]
    [PXUIField(DisplayName="Contract")]
    [AGContractSelector(typeof(PX.Objects.AP.APDocumentEnq.APDocumentFilter.vendorID))]
    public virtual int? UsrContractID { get; set; }
    public abstract class usrContractID : IBqlField { }
    #endregion
  }
}
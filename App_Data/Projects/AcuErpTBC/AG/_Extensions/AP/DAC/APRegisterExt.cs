﻿using AG.Extensions.AP.DAC;
using AG.Extensions.AP.Shared;
using AG.Extensions.PO.Shared;
using PX.Data;
using PX.Objects.EP;
using System;

namespace PX.Objects.AP
{
    public class APRegisterExt : PXCacheExtension<PX.Objects.AP.APRegister>
    {
        #region UsrAGBankAccountID
        [PXDBInt()]
        [AGBankAccountsSelector(typeof(Where<AGBankAccount.bAccountID, Equal<Current<APRegister.vendorID>>,
            And<AGBankAccount.currencyID, Equal<Current<APRegister.curyID>>>>))]
        [PXUIField(DisplayName = "Bank Account")]

        public virtual int? UsrAGBankAccountID { get; set; }
        public abstract class usrAGBankAccountID : IBqlField { }
        #endregion

        #region UsrPrepaymentCloseDate
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [PXDBDate]
        [PXUIField(DisplayName = "Prepayment Close Date")]
        public virtual DateTime? UsrPrepaymentCloseDate { get; set; }
        public abstract class usrPrepaymentCloseDate : IBqlField { }
        #endregion

        #region UsrContractID
        [PXDBInt]
        [PXUIField(DisplayName = "Contract")]
        [AGContractSelector(typeof(APRegister.vendorID))]
        public virtual int? UsrContractID { get; set; }
        public abstract class usrContractID : IBqlField { }
        #endregion

        #region UsrFARelated
        [PXDBBool]
        [PXUIField(DisplayName = "FA Related")]
        public virtual bool? UsrFARelated { get; set; }
        public abstract class usrFARelated : IBqlField { }
        #endregion

        #region UsrPONumber
        [PXDBString(20)]
        [PXUIField(DisplayName = "PO Number",Enabled =false)]
        public virtual string UsrPONumber { get; set; }
        public abstract class usrPONumber : IBqlField { }
        #endregion

        #region UsrPOPrepaymentAmt
        [PXDBDecimal]
        [PXUIField(DisplayName = "PO Prepayment Amount",Enabled =false)]
        public virtual Decimal? UsrPOPrepaymentAmt { get; set; }
        public abstract class usrPOPrepaymentAmt : IBqlField { }
        #endregion

        #region UsrEmployeeID
        [PXDBInt]
        [PXUIField(DisplayName = "Budget Owner Emp.")]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        public virtual int? UsrEmployeeID { get; set; }
        public abstract class usrEmployeeID : IBqlField { }
        #endregion

        #region UsrDepartment
        [PXDBString(10)]
        [PXUIField(DisplayName = "Budget Owner Dep.")]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<APRegisterExt.usrEmployeeID>>>>),PersistingCheck =PXPersistingCheck.Nothing)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        public virtual string UsrDepartment { get; set; }
        public abstract class usrDepartment : IBqlField { }
        #endregion
    }
}

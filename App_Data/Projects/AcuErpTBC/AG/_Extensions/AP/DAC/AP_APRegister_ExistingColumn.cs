using AG.Extensions.AP.Shared;
using PX.Data;

namespace PX.Objects.AP
{
    [PXNonInstantiatedExtension]
    public class AP_APRegister_ExistingColumn : PXCacheExtension<PX.Objects.AP.APRegister>
    {
        #region VendorID  
        [PXMergeAttributes(Method = MergeMethod.Append)]
        [VendorBankAccountCheck()]
        public int? VendorID { get; set; }
        #endregion
    }
}
using PX.Data;
using AG.Extensions.CS.DAC;

namespace PX.Objects.AP
{
    public class VendorClassExt : PXCacheExtension<PX.Objects.AP.VendorClass>
  {
      #region UsrLegalFormID
      [PXDBString(15, IsUnicode = true)]
      [PXUIField(DisplayName="Legal Form")]
      [PXSelector(typeof(Search<AGLegalForm.legalFormID, Where<Current<VendorClass.countryID>, IsNull, Or<AGLegalForm.countryID, Equal<Current<VendorClass.countryID>>,
          Or<AGLegalForm.countryID, IsNull>>>>),
                  typeof(AGLegalForm.legalFormID),typeof(AGLegalForm.description),
                  typeof(AGLegalForm.countryID), SubstituteKey = typeof(AGLegalForm.legalFormID),CacheGlobal =true)]
      public virtual string UsrLegalFormID { get; set; }
      public abstract class usrLegalFormID : IBqlField { }
      #endregion
  }
}
using AG.Extensions.CS.DAC;
using PX.Data;
using PX.Objects.CR;

namespace PX.Objects.AP
{
    public class VendorExt : PXCacheExtension<PX.Objects.AP.Vendor>
    {
        #region UsrLegalFormID
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Legal Form")]
        [PXSelector(typeof(Search<AGLegalForm.legalFormID,
                          Where<AGLegalForm.countryID, Equal<Current<Address.countryID>>,
                              Or<AGLegalForm.countryID, IsNull>>>),
                    typeof(AGLegalForm.legalFormID), typeof(AGLegalForm.description),
                    typeof(AGLegalForm.countryID), SubstituteKey = typeof(AGLegalForm.legalFormID), CacheGlobal = true)]
        public virtual string UsrLegalFormID { get; set; }
        public abstract class usrLegalFormID : IBqlField { }
        #endregion
    }
}
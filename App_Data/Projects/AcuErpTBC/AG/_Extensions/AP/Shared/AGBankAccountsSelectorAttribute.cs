﻿using AG.Extensions.AP.DAC;
using PX.Data;
using System;

namespace AG.Extensions.AP.Shared
{
    public class AGBankAccountsSelectorAttribute : PXSelectorAttribute
    {
        public AGBankAccountsSelectorAttribute(Type where)
            : base(typeof(Search2<AGBankAccount.bankAccountID,
                            LeftJoin<AGBank, On<AGBankAccount.bankID, Equal<AGBank.bankID>>>>),
                  typeof(AGBankAccount.iBANAccount),
                  typeof(AGBankAccount.currencyID),
                  typeof(AGBankAccount.accountName),
                  typeof(AGBank.bankName),
                  typeof(AGBank.countryID))
        {
            SubstituteKey = typeof(AGBankAccount.iBANAccount);
            DescriptionField = typeof(AGBankAccount.accountName);

            _Select = _Select.WhereAnd(where);
        }
    }
}

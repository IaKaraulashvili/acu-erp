﻿using AG.Extensions.AP.DAC;
using PX.Data;
using PX.Objects.AP;
using System.Linq;

namespace AG.Extensions.AP.Shared
{
    public class VendorBankAccountCheckAttribute : PXEventSubscriberAttribute, IPXFieldUpdatedSubscriber
    {
        public void FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (APRegister)e.Row;

            if (row != null)
            {
                var registerExt = cache.GetExtension<APRegisterExt>(row);

                int? bankAccountID = null;

                if (row.VendorID != null)
                {
                    var bankAccounts = PXSelect<AGBankAccount,
                        Where<AGBankAccount.bAccountID, Equal<Required<APRegister.vendorID>>, 
                            And<AGBankAccount.isDefault, Equal<True>>>>.Select(cache.Graph, row.VendorID).FirstTableItems;

                    if (bankAccounts != null)
                    {
                        bankAccountID = bankAccounts.FirstOrDefault()?.BankAccountID;
                    }
                }

                cache.SetValueExt<APRegisterExt.usrAGBankAccountID>(row, bankAccountID);
            }
        }
    }
}

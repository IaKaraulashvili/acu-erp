﻿using PX.Data;
using PX.Objects.AP;
using PX.Objects.CR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.Extensions.AP.Shared
{
    public class VendorSelectorAttribute : PXSelectorAttribute
    {
        public VendorSelectorAttribute(Type where)
            : base(
                typeof(Search2<Vendor.bAccountID,
                    InnerJoin<LocationExtAddress, On<Vendor.bAccountID, Equal<LocationExtAddress.bAccountID>>,
                        LeftJoin<Contact, On<Vendor.defContactID, Equal<Contact.contactID>>>>>),
                typeof(Vendor.acctCD),
                typeof(Vendor.acctName),
                typeof(LocationExtAddress.addressLine1),
                typeof(LocationExtAddress.addressLine2),
                typeof(LocationExtAddress.postalCode),
                typeof(Contact.phone1),
                typeof(LocationExtAddress.city),
                typeof(LocationExtAddress.countryID),
                typeof(LocationExtAddress.taxRegistrationID),
                typeof(Vendor.curyID),
                typeof(Contact.salutation),
                typeof(Vendor.vendorClassID),
                typeof(Vendor.status))
        {
            SubstituteKey = typeof(Vendor.acctCD);
            DescriptionField = typeof(Vendor.acctName);

            _Select = _Select.WhereAnd(where);
        }
    }
}

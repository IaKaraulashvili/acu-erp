﻿using PX.Data;
using PX.Objects.AP;
using PX.Objects.CR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.Extensions.AP.Shared
{
    public class VendorUnboundAttribute : PXDBScalarAttribute
    {
        public VendorUnboundAttribute(Type search)
            : base(typeof(Search2<Vendor.bAccountID,
                InnerJoin<LocationExtAddress, On<Vendor.bAccountID, Equal<LocationExtAddress.bAccountID>>>>))
        {
            _Search = _Search.WhereAnd(search);
        }
    }
}
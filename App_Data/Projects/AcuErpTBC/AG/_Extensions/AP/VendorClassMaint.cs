using AG.Extensions.CS.DAC;
using PX.Data;
using PX.Objects.CR;
using PX.Objects.CS;

namespace PX.Objects.AP
{

    public class VendorClassMaint_Extension : PXGraphExtension<VendorClassMaint>
    {

        public override void Initialize()
        {
            PXDBAttributeAttribute.Activate(Base.VendorClassRecord.Cache);
        }

        [Country]
        [PXDBString(100)]
        [PXUIField(DisplayName = "Country")]
        protected void VendorClass_CountryID_CacheAttached(PXCache cache)
        {

        }

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var current = Base.CurVendorClassRecord.Current;
            if (current != null)
            {
                var vendorClassExt = current.GetExtension<VendorClassExt>();
                var legalForm = PXSelect<AGLegalForm, Where<AGLegalForm.legalFormID, Equal<Required<AGLegalForm.legalFormID>>, And<Where<AGLegalForm.countryID,
                                IsNull, Or<AGLegalForm.countryID, Equal<Required<VendorClass.countryID>>>>>>>.Select(Base, vendorClassExt.UsrLegalFormID, current.CountryID);

                if (legalForm.Count == 0)
                {
                    PXCache cache = Base.Caches[typeof(VendorClassExt)];
                    cache.RaiseExceptionHandling<VendorClassExt.usrLegalFormID>(current, vendorClassExt.UsrLegalFormID,
                     new PXSetPropertyException(AG.Common.Messages.LegalFormIDNotCorrespondCountry, PXErrorLevel.Error));
                }
            }
            baseMethod();
        }
    }
}
using System.Linq;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using AG.Extensions.AP.DAC;
using AG.Extensions.AP;
using AG.Extensions.CS.DAC;

namespace PX.Objects.AP
{

    public class VendorMaint_Extension : PXGraphExtension<VendorMaint>
    {
        #region Selects
        public PXSelectJoin<AGBankAccount, LeftJoin<AGBank, On<AGBank.bankID, Equal<AGBankAccount.bankID>>>,
            Where<AGBankAccount.bAccountID, Equal<Current<Vendor.bAccountID>>>> BankAccounts;



        #endregion

        #region Event Handlers

        protected virtual void AGBankAccount_IBANAccount_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            var row = (AGBankAccount)e.Row;
            foreach (AGBankAccount line in BankAccounts.Select())
            {
                if (row.BankAccountID == line.BankAccountID)
                    continue;

                if (row.IBANAccount == line.IBANAccount)
                {
                    sender.RaiseExceptionHandling<AGBankAccount.iBANAccount>(row, row.IBANAccount,
                        new PXSetPropertyException(AG.Common.Messages.DefaultAccount, PXErrorLevel.Warning));
                    break;
                }
            }
        }

        protected virtual void AGBankAccount_IntermediaryBankID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (AGBankAccount)e.Row;
            if (row == null) return;
            AGBank bank = PXSelect<AGBank, Where<AGBank.bankID, Equal<Required<AGBankAccount.intermediaryBankID>>>>.Select(Base, row.IntermediaryBankID).FirstOrDefault();
            if (bank != null && row.IntermediaryBankID != null)
                row.IntermediaryBankName = bank.BankName;
            else
                row.IntermediaryBankName = null;
        }

        protected virtual void Vendor_RowUpdated(PXCache cache, PXRowUpdatedEventArgs e)
        {
            var row = (VendorR)e.Row;

            if (row == null) return;

            if (!cache.ObjectsEqual<Vendor.vendorClassID>(e.OldRow, e.Row))
            {
                var vendorClass = Base.VendorClass.Current;
                string legalFormID = null;

                if (vendorClass != null)
                {
                    legalFormID = Base.VendorClass.Cache.GetExtension<VendorClassExt>(vendorClass).UsrLegalFormID;
                }

                cache.GetExtension<VendorExt>(row).UsrLegalFormID = legalFormID;
            }
        }


        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {

            var defaultBankAccounts = BankAccounts.Select().FirstTableItems.Where(m => m.IsDefault.GetValueOrDefault()).ToList();
            if (defaultBankAccounts.Count() > 1)
            {
                PXCache cache = Base.Caches[typeof(AGBankAccount)];
                defaultBankAccounts.ForEach(bankAccount =>
                {
                    cache.RaiseExceptionHandling<AGBankAccount.isDefault>(bankAccount, bankAccount.IsDefault,
                     new PXSetPropertyException(AG.Common.Messages.OneDefaultIBANAccount, PXErrorLevel.Error));
                });
                throw new PXException(AG.Common.Messages.OneDefaultIBANAccount);
            }
            var defLocation = (CR.LocationExtAddress)Base.DefLocation.Select();
            if (defLocation != null)
            {
                defLocation.TaxRegistrationID = Base.BAccount.Current.AcctCD;
            }
            // Base.DefLocation.Current.TaxRegistrationID = Base.BAccount.Current.AcctCD;
            //Contact contact = Base.DefContact.Current;
            //if (contact != null)
            //{
            //    PXCache contactCache = Base.DefContact.Cache;
            //    ContactExt contactExt = contactCache.GetExtension<ContactExt>(contact);
            //    if (String.IsNullOrEmpty(contactExt.UsrTaxRegistrationID))
            //        throw new  PXException(Messages.TaxRegistrationIDCannotBeEmpty);

            //}

            var current = Base.CurrentVendor.Current;

            if (current != null)
            {
                var address = Base.Addresses.Current;
                var vendorExt = current.GetExtension<VendorExt>();

                if (address != null && vendorExt.UsrLegalFormID != null)
                {
                    var legalForm = PXSelect<AGLegalForm, Where<AGLegalForm.legalFormID, Equal<Required<AGLegalForm.legalFormID>>, And<Where<AGLegalForm.countryID,
                                    IsNull, Or<AGLegalForm.countryID, Equal<Required<Address.countryID>>>>>>>.Select(Base, vendorExt.UsrLegalFormID, address.CountryID);

                    if (legalForm.Count == 0)
                    {
                        PXCache cache = Base.Caches[typeof(VendorExt)];
                        cache.RaiseExceptionHandling<VendorExt.usrLegalFormID>(current, vendorExt.UsrLegalFormID,
                         new PXSetPropertyException(AG.Common.Messages.LegalFormIDNotCorrespondCountry, PXErrorLevel.Error));
                    }
                }
            }
            baseMethod();
        }

        #endregion

        #region Actions


        public PXAction<VendorR> ViewCurrency;
        [PXButton]
        public virtual void viewCurrency()
        {
            AGBankAccount row = BankAccounts.Current;
            CurrencyMaint graph = PXGraph.CreateInstance<CurrencyMaint>();
            graph.CuryListRecords.Current = graph.CuryListRecords.Search<CurrencyList.curyID>(row.CurrencyID);
            if (graph.CuryListRecords.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, "Currency");
            }
        }

        public PXAction<VendorR> ViewBank;
        [PXButton]
        public virtual void viewBank()
        {
            AGBankAccount row = BankAccounts.Current;
            AGBankMaint graph = PXGraph.CreateInstance<AGBankMaint>();
            graph.Banks.Current = graph.Banks.Search<AGBank.bankID>(row.BankID);
            if (graph.Banks.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, "Bank");
            }
        }

        #endregion

    }
}
﻿using PX.Data;
using PX.Objects.AR;
using PX.Objects.TX;
using AG.RS.DAC;
using System;
using System.Linq;
using static AG.RS.Shared.TaxInvoiceStatusAttribute;
using PX.Api;
using System.Collections;
using PX.DataSync;
using CustomSYProvider.Services;
using System.Collections.Generic;

namespace AG.RS
{
    public class ARInvoiceEntryExt : PXGraphExtension<ARInvoiceEntry>
    {
        #region ctor
        public override void Initialize()
        {
            Base.action.AddMenuAction(PostTaxInvoice);
        }
        #endregion

        #region Page Events
        protected virtual void ARInvoice_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (ARInvoice)e.Row;
            if (row != null)
            {
                var isEnabled = row.Status == ARDocStatus.Open || row.Status == ARDocStatus.Closed && row.DocType == ARInvoiceType.Invoice;
                //var isGroupPayment = this.Base.customer.Select(row.CustomerID).FirstTableItems.SingleOrDefault();

                PostTaxInvoice.SetEnabled(isEnabled);
            }
        }

        #endregion

        #region Buttons
        public PXAction<ARInvoice> PostTaxInvoice;
        [PXUIField(DisplayName = "Create Tax Invoice", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        public virtual void postTaxInvoice()
        {
            PXLongOperation.StartOperation(Base, delegate ()
            {
                try
                {
                    CreateTaxInvoice();
                    PXProcessing<ARInvoice>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });
        }

        public PXAction<ARInvoice> Export;
        [PXUIField(DisplayName = "Export", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable export(PXAdapter adapter)
        {
            var current = Base.Document.Current;
            string exportScenarioName = "AR Invoices Export";

            PXLongOperation.StartOperation(Base, delegate ()
            {
                WordPDFSYExportProcessHelper.RunScenarion(
                    Base,
                    exportScenarioName,
                    current.NoteID.ToString(),
                    current.RefNbr,
                    current.DocType
                    );
            });

            return adapter.Get();
        }

        #endregion

        #region Methods

        void CreateTaxInvoice()
        {
            TaxInvoiceEntry graph = PXGraph.CreateInstance<TaxInvoiceEntry>();
            var arInvoice = Base.Document.Current;
            PXResultset<ARTran> arInvoiceItems = Base.Transactions.Select();
            var taxes = PXSelect<Tax>.Select(graph).FirstTableItems;
            var uOM = PXSelect<RSUOMMapper>.Select(graph).FirstTableItems;

            var postPeriod = PXSelect<TaxPeriod,
                Where<TaxPeriod.startDate, LessEqual<Required<TaxPeriod.startDate>>,
                And<TaxPeriod.endDate, GreaterEqual<Required<TaxPeriod.endDate>>>>>.Select(graph, arInvoice.DocDate, arInvoice.DocDate).FirstTableItems.FirstOrDefault();


            RSTaxInvoice rsTI = graph.TaxInvoices.Insert();

            rsTI.PostPeriod = postPeriod.TaxPeriodID;
            rsTI.Hold = false;
            rsTI.ItemCntr = 0;
            rsTI.Status = Statuses.Open;

            graph.TaxInvoices.Cache.SetValueExt<RSTaxInvoice.branchID>(rsTI, arInvoice.BranchID);
            graph.TaxInvoices.Cache.SetValueExt<RSTaxInvoice.customerID>(rsTI, arInvoice.CustomerID);

            foreach (var item in arInvoiceItems)
            {
                var arItem = (ARTran)item;
                RSTaxInvoiceItem taxInvoiceItem = graph.TaxInvoiceItems.Insert();
                taxInvoiceItem.InventoryID = arItem.InventoryID;
                taxInvoiceItem.ItemName = arItem.TranDesc;
                taxInvoiceItem.ExtUOM = (from uom in uOM where uom.Unit == arItem.UOM select uom.RSUOMID).FirstOrDefault();
                taxInvoiceItem.ItemQty = arItem.Qty;
                taxInvoiceItem.UnitPrice = arItem.Qty == 0 ? 0 : arItem.CuryTranAmt / arItem.Qty;
                taxInvoiceItem.VATAmt = arItem.CuryTaxAmt;
                taxInvoiceItem.ItemAmt = arItem.CuryTranAmt;
                taxInvoiceItem.ExciseAmt = 0;
                taxInvoiceItem.FullAmt = taxInvoiceItem.VATAmt + taxInvoiceItem.ItemAmt + taxInvoiceItem.ExciseAmt;
                var taxItem = (from t in taxes where t.TaxID == arItem.TaxID select t).FirstOrDefault();
                taxInvoiceItem.TaxType = PXCache<Tax>.GetExtension<TaxExt>(taxItem).UsrRSVatType;
                //graph.SetValueExt("TaxInvoiceItems", taxInvoiceItem, "UnitPrice", taxInvoiceItem.UnitPrice);
            }
            bool post = SaveOrPost(graph);
            if (post)
            {
                using (var tran = new PXTransactionScope())
                {
                    graph.Actions.PressSave();
                    graph.PostTI();
                    // TODO
                    graph.Actions.PressSave();
                    tran.Complete(graph);
                }
            }
            else
            {
                graph.Actions.PressSave();
            }
        }

        bool SaveOrPost(TaxInvoiceEntry graph)
        {
            var setup = PXSelect<RSSetup>.Select(graph).FirstTableItems;
            if (setup.Count() > 0)
            {
                return (bool)setup.First().TaxInvoicePostOnInvoiceAndMemo;
            }
            return false;
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using PX.Data;
using PX.Objects.GL;
using PX.Objects;
using PX.Objects.CM;

namespace PX.Objects.CM
{
    public class CMSetupMaint_Extension : PXGraphExtension<CMSetupMaint>
    {
        #region Event Handlers

        protected void CMSetup_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (CMSetup)e.Row;

            if (row == null)
            {
                return;
            }

            var ext = Base.cmsetup.Cache.GetExtension<CMSetupExt>(row);

            PXUIFieldAttribute.SetEnabled<CMSetupExt.usrNBGServiceUrl>(cache, row, ext.UsrUseNBGService.GetValueOrDefault());
        }

        #endregion
    }
}
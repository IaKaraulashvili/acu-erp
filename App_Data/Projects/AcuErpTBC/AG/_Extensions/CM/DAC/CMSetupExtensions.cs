using PX.Data;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.CM
{
    public class CMSetupExt : PXCacheExtension<PX.Objects.CM.CMSetup>
    {
        #region UsrUseNBGService
        [PXDBBool]
        [PXUIField(DisplayName = "Use Service")]
        public virtual bool? UsrUseNBGService { get; set; }
        public abstract class usrUseNBGService : IBqlField { }
        #endregion
        #region UsrNBGServiceUrl
        [PXDBString]
        [PXUIField(DisplayName = "Service Url")]
        public virtual string UsrNBGServiceUrl { get; set; }
        public abstract class usrNBGServiceUrl : IBqlField { }
        #endregion
    }
}
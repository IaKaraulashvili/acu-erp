using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.AP;
using PX.Objects.CS;
using System.Net;
using System.Linq;
using PX.Objects;
using PX.Objects.CM;

namespace PX.Objects.CM
{
    public class RefreshCurrencyRates_Extension : PXGraphExtension<RefreshCurrencyRates>
    {
        #region Views

        public PXSetup<CMSetup> Setup;

        #endregion

        #region Event Handlers

        protected void RefreshFilter_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var setup = Setup.Current;

            var ext = Setup.Cache.GetExtension<CMSetupExt>(setup);

            if (!ext.UsrUseNBGService.GetValueOrDefault())
            {
                return;
            }

            RefreshFilter filter = (RefreshFilter)e.Row;
            if (filter != null)
            {
                Base.CurrencyRateList.SetProcessDelegate(
                    delegate (List<RefreshRate> list)
                    {
                        RefreshRatesNBG(filter, list, ext.UsrNBGServiceUrl);
                    }
                );
            }
        }

        #endregion

        #region Other

        public static void RefreshRatesNBG(RefreshFilter filter, List<RefreshRate> list, string serviceUrl)
        {
            if (filter.CuryID != "GEL")
            {
                throw new PXException(AG.Common.Messages.OnlyGELIsAllowed);
            }

            var date = filter.CuryEffDate.Value;

            List<NBG.Services.CurrencyRate> rates;

            using (var service = new NBG.Services.CurrencyService(serviceUrl))
            {
                rates = service.GetRates(date);
            }

            CuryRateMaint graph = PXGraph.CreateInstance<CuryRateMaint>();
            graph.Filter.Current.ToCurrency = filter.CuryID;
            graph.Filter.Current.EffDate = date;

            bool hasError = false;

            for (int i = 0; i < list.Count; i++)
            {
                RefreshRate rr = list[i];

                var rate = rates.Where(p => p.Code == rr.FromCuryID).FirstOrDefault();

                if (rate == null)
                {
                    PXProcessing<RefreshRate>.SetError(i, PXMessages.LocalizeFormatNoPrefixNLA(Messages.NoOnlyRatesFoundForCurrency, rr.FromCuryID));
                    hasError = true;
                }
                else
                {
                    CurrencyRate curyRate = (CurrencyRate)graph.CuryRateRecordsEntry.Insert();
                    curyRate.FromCuryID = rr.FromCuryID;
                    curyRate.ToCuryID = filter.CuryID;
                    curyRate.CuryRateType = rr.CuryRateType;
                    curyRate.CuryRate = (1 / rate.Rate) * (1 + rr.OnlineRateAdjustment.GetValueOrDefault(0) / 100);
                    curyRate.CuryMultDiv = "D";
                    rr.CuryRate = curyRate.CuryRate;
                    graph.CuryRateRecordsEntry.Update(curyRate);

                    PXProcessing<RefreshRate>.SetInfo(i, ActionsMessages.RecordProcessed);
                }
            }

            graph.Actions.PressSave();

            if (hasError)
            {
                throw new PXOperationCompletedException(Messages.CurrencyRateFailedToRefresh);
            }
        }

        #endregion
    }
}
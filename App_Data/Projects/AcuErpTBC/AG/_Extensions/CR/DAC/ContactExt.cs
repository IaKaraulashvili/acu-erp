using PX.Data;


namespace PX.Objects.CR
{
    public class ContactExt : PXCacheExtension<PX.Objects.CR.Contact>
  {   
       #region UsrIsNotResident
        [PXDBBool]
        [PXUIField(DisplayName = "Recipient Is Foreign Citizen")]
        public virtual bool? UsrIsNotResident { get; set; }
        public abstract class usrIsNotResident : IBqlField { }
        #endregion
    }
}
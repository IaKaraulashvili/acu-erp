using PX.Data;
using AG.Extensions.CS.DAC;

namespace AG.Extensions.CS
{
    public class AGLegalFormMaint : PXGraph<AGLegalFormMaint>
    {
        public PXSave<AGLegalForm> Save;
        public PXCancel<AGLegalForm> Cancel;

        public PXSelect<AGLegalForm> LegalForms;


        protected virtual void AGLegalForm_LegalFormID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (AGLegalForm)e.Row;
            foreach (AGLegalForm line in LegalForms.Select())
            {
                if (row.ID == line.ID)
                    continue;

                if (row.LegalFormID == line.LegalFormID)
                    throw new PXException(AG.Common.Messages.LegalFormID);
            }
        }
    }
}
﻿namespace AG.Extensions.CS.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.CS;

    [System.SerializableAttribute()]
	public class AGLegalForm : PX.Data.IBqlTable
	{
		#region ID
		public abstract class iD : PX.Data.IBqlField
		{
		}
		protected int? _ID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				this._ID = value;
			}
		}
		#endregion
		#region LegalFormID
		public abstract class legalFormID : PX.Data.IBqlField
		{
		}
		protected string _LegalFormID;
		[PXDBString(15, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "ID")]
		public virtual string LegalFormID
		{
			get
			{
				return this._LegalFormID;
			}
			set
			{
				this._LegalFormID = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(250, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Description")]
		public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
		#endregion
		#region CountryID
		public abstract class countryID : PX.Data.IBqlField
		{
		}
		protected string _CountryID;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Country")]
        [PXSelector(typeof(Country.countryID), CacheGlobal = true, DescriptionField = typeof(Country.description))]
        public virtual string CountryID
		{
			get
			{
				return this._CountryID;
			}
			set
			{
				this._CountryID = value;
			}
		}
		#endregion
	}
}

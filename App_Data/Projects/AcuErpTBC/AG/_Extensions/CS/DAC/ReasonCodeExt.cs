﻿using AG._Extensions.CS.Shared;
using AG.EA.Shared;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PX.Objects.CS
{
    public class ReasonCodeExt : PXCacheExtension<ReasonCode>
    {
        #region SubMask
        public abstract class subMask : PX.Data.IBqlField
        {
        }
        protected String _SubMask;
        [PXDefault()]
        [PXDBString(IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Combine Sub From Combined", Visible = false)]//required for Copy-Paste
        public virtual String SubMask
        {
            get
            {
                return this._SubMask;
            }
            set
            {
                this._SubMask = value;
            }
        }
        #endregion

        #region SubMaskInventory
        public abstract class subMaskInventory : PX.Data.IBqlField
        {
        }
        [PXDefault()]
        [PXString(30, IsUnicode = true, InputMask = "")]
        [AG._Extensions.CS.Shared.ReasonCodeSubAccountMaskAttribute(DisplayName = "Combine Sub from")]
        public virtual String SubMaskInventory
        {
            get
            {
                return this._SubMask;
            }
            set
            {
                this._SubMask = value;
            }
        }
        #endregion

        #region UsrCOGSExpenseAccountFrom
        [PXDBString(1)]
        [PXUIField(DisplayName = "Use COGS/Expense Account From")]
        [UsrCOGSExpenseAccountFromTypeList]
        [PXDefault]
        public virtual string UsrCOGSExpenseAccountFrom { get; set; }
        public abstract class usrCOGSExpenseAccountFrom : IBqlField { }
        #endregion
    }
}

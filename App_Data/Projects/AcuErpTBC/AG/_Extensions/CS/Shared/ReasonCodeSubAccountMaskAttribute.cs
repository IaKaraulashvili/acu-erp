﻿using PX.Data;
using PX.Objects.CS;
using PX.Objects.GL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG._Extensions.CS.Shared
{
    [PXUIField(DisplayName = "Subaccount Mask", Visibility = PXUIVisibility.Visible, FieldClass = _DimensionName)]
    public sealed class ReasonCodeSubAccountMaskAttribute : AcctSubAttribute
    {
        public const string ReasonCode = "R";
        public const string InventoryItem = "I";
        public const string PostingClass = "P";
        public const string Warehouse = "W";
        public const string Employee = "E";
        public const string Department = "D";

        private static readonly string[] writeOffValues = new string[] { ReasonCode, InventoryItem, Warehouse, PostingClass, Employee, Department };
        private static readonly string[] writeOffLabels = new string[] { PX.Objects.CS.Messages.ReasonCode, PX.Objects.IN.Messages.InventoryItem, PX.Objects.IN.Messages.Warehouse, PX.Objects.IN.Messages.PostingClass, PX.Objects.EP.Messages.MaskEmployee, PX.Objects.EP.Messages.Department};

        private const string _DimensionName = "SUBACCOUNT";
        private const string _MaskName = "ReasonCodeIN";
        public ReasonCodeSubAccountMaskAttribute()
            : base()
        {
            PXDimensionMaskAttribute attr = new PXDimensionMaskAttribute(_DimensionName, _MaskName, ReasonCode, writeOffValues, writeOffLabels);
            attr.ValidComboRequired = false;
            _Attributes.Add(attr);
            _SelAttrIndex = _Attributes.Count - 1;
        }

        public static string MakeSub<Field>(PXGraph graph, string mask, object[] sources, Type[] fields)
            where Field : IBqlField
        {
            try
            {
                return PXDimensionMaskAttribute.MakeSub<Field>(graph, mask, writeOffValues, sources);
            }
            catch (PXMaskArgumentException ex)
            {
                PXCache cache = graph.Caches[BqlCommand.GetItemType(fields[ex.SourceIdx])];
                string fieldName = fields[ex.SourceIdx].Name;
                throw new PXMaskArgumentException(writeOffLabels[ex.SourceIdx], PXUIFieldAttribute.GetDisplayName(cache, fieldName));
            }
        }
    }
}

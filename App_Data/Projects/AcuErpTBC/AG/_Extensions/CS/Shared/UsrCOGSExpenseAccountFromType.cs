﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG._Extensions.CS.Shared
{
   public static class UsrCOGSExpenseAccountFromType
    {
        public const string ReasonCode = "R";
        public class reasonCode : Constant<String>
        {
            public reasonCode()
                : base(ReasonCode)
            {
            }
        }
        public const string InventoryItem = "I";
        public class inventoryItem : Constant<String>
        {
            public inventoryItem()
                : base(InventoryItem)
            {
            }
        }

        public class UI
        {
            public const string ReasonCode = "Reason Code";
            public const string InventoryItem = "Inventory Item";
        }
    }
}

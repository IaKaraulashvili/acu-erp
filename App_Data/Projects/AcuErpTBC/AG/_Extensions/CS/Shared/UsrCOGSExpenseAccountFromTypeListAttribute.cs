﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG._Extensions.CS.Shared
{
    public class UsrCOGSExpenseAccountFromTypeListAttribute : PXStringListAttribute
    {
        public UsrCOGSExpenseAccountFromTypeListAttribute() : base(
            new string[] {
                UsrCOGSExpenseAccountFromType.ReasonCode,
                UsrCOGSExpenseAccountFromType.InventoryItem
            },
            new string[] {
                UsrCOGSExpenseAccountFromType.UI.ReasonCode,
                UsrCOGSExpenseAccountFromType.UI.InventoryItem
            })
        { }
    }
}

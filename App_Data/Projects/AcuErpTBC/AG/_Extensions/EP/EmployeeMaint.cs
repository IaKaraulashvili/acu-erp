using System.Collections;
using System.Linq;
using PX.Common;
using PX.Objects.AP;
using PX.SM;
using PX.TM;
using System;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CA;
using PX.Objects.CM;
using System.Collections.Generic;
using PX.Data.Maintenance.GI;
using PX.Objects.PM;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects;
using PX.Objects.EP;
using AG.Extensions.AP.DAC;
using AG.Extensions.AP;

namespace PX.Objects.EP
{
    public class EmployeeMaint_Extension : PXGraphExtension<EmployeeMaint>
    {

        #region Selects
        public PXSelectJoin<AGBankAccount, LeftJoin<AGBank, On<AGBank.bankID, Equal<AGBankAccount.bankID>>>,
            Where<AGBankAccount.bAccountID, Equal<Current<EPEmployee.bAccountID>>>> BankAccounts;



        #endregion

        #region Event Handlers

        [PXDBInt()]
        [PXDBDefault(typeof(EPEmployee.bAccountID))]
        [PXParent(typeof(Select<EPEmployee, Where<EPEmployee.bAccountID, Equal<Current<AGBankAccount.bAccountID>>>>))]
        protected void AGBankAccount_BAccountID_CacheAttached(PXCache cache)
        {

        }

        protected virtual void AGBankAccount_IBANAccount_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {

            var row = (AGBankAccount)e.Row;
            foreach (AGBankAccount line in BankAccounts.Select())
            {
                if (row.BankAccountID == line.BankAccountID)
                    continue;

                if (row.IBANAccount == line.IBANAccount)
                {
                    sender.RaiseExceptionHandling<AGBankAccount.iBANAccount>(row, row.IBANAccount,
                        new PXSetPropertyException(AG.Common.Messages.DefaultAccount, PXErrorLevel.Warning));
                    break;
                }
            }
        }

        #endregion

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {

            var defaultBankAccounts = BankAccounts.Select().FirstTableItems.Where(m => m.IsDefault.GetValueOrDefault()).ToList();
            if (defaultBankAccounts.Count() > 1)
            {
                PXCache cache = Base.Caches[typeof(AGBankAccount)];
                defaultBankAccounts.ForEach(bankAccount =>
                {
                    cache.RaiseExceptionHandling<AGBankAccount.isDefault>(bankAccount, bankAccount.IsDefault,
                     new PXSetPropertyException(AG.Common.Messages.OneDefaultIBANAccount, PXErrorLevel.Error));
                });
                throw new PXException(AG.Common.Messages.OneDefaultIBANAccount);
            }
            baseMethod();
        }

        #region Actions
        public PXAction<EPEmployee> ViewCurrency;
        [PXButton]
        public virtual void viewCurrency()
        {
            AGBankAccount row = BankAccounts.Current;
            CurrencyMaint graph = PXGraph.CreateInstance<CurrencyMaint>();
            graph.CuryListRecords.Current = graph.CuryListRecords.Search<CurrencyList.curyID>(row.CurrencyID);
            if (graph.CuryListRecords.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, "Currency");
            }
        }

        public PXAction<EPEmployee> ViewBank;
        [PXButton]
        public virtual void viewBank()
        {
            AGBankAccount row = BankAccounts.Current;
            AGBankMaint graph = PXGraph.CreateInstance<AGBankMaint>();
            graph.Banks.Current = graph.Banks.Search<AGBank.bankID>(row.BankID);
            if (graph.Banks.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, "Bank");
            }
        }

        #endregion
    }
}
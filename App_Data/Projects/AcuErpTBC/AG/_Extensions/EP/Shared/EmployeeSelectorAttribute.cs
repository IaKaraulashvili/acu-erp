﻿using PX.Data;
using PX.Objects.EP;
using System;
using PX.Objects.CR;

namespace AG.Extensions.EP.Shared
{
    public class EmployeeSelectorAttribute : PXDimensionSelectorAttribute
    {
        public EmployeeSelectorAttribute()
            : this(typeof(Search2<EPEmployee.bAccountID,
                    LeftJoin<EPEmployeePosition, On<EPEmployeePosition.employeeID, Equal<EPEmployee.bAccountID>, And<EPEmployeePosition.isActive, Equal<True>>>,
                    LeftJoin<Contact, On<Contact.bAccountID, Equal<EPEmployee.parentBAccountID>, And<Contact.contactID, Equal<EPEmployee.defContactID>>>>>>),
                substituteKey: typeof(EPEmployee.acctCD),
                fieldList: new[]
                {
                   // typeof(EPEmployee.acctCD),
                    typeof(EPEmployee.bAccountID),
                    typeof(EPEmployee.acctCD),
                    typeof(EPEmployee.acctName),
                    typeof(EPEmployee.status),
                    typeof(EPEmployeePosition.positionID),
                    typeof(Contact.eMail),
                    typeof(Contact.phone1),
                    typeof(EPEmployee.departmentID),
                    typeof(EPEmployee.vendorClassID),
                    typeof(EPEmployee.supervisorID)
                })
        {
        }

        public EmployeeSelectorAttribute(params Type[] fieldList)
            : this(typeof(Search2<EPEmployee.bAccountID,
                    LeftJoin<EPEmployeePosition, On<EPEmployeePosition.employeeID, Equal<EPEmployee.bAccountID>, And<EPEmployeePosition.isActive, Equal<True>>>,
                    LeftJoin<Contact, On<Contact.bAccountID, Equal<EPEmployee.parentBAccountID>, And<Contact.contactID, Equal<EPEmployee.defContactID>>>>>>),
                typeof(EPEmployee.acctCD),
                fieldList)
        {
        }

        public EmployeeSelectorAttribute(Type search, Type substituteKey, params Type[] fieldList)
            : base(
                EmployeeRawAttribute.DimensionName,
                search,
                substituteKey,
                fieldList)
        {
            DescriptionField = typeof(EPEmployee.acctName);
        }
    }
}

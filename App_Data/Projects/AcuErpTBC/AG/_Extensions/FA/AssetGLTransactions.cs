using System;
using System.Collections;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.FA.Overrides.AssetProcess;
using PX.Objects.GL;
using PX.Objects.IN;
using System.Linq;
using System.Collections.Generic;

namespace PX.Objects.FA
{
    public class AssetGLTransactions_Extension : PXGraphExtension<AssetGLTransactions>
    {
        #region Event Handlers
        protected virtual void FixedAsset_RowInserting(PXCache sender, PXRowInsertingEventArgs e)
        {
            FixedAsset row = (FixedAsset)e.Row;
            if (row == null) return;

            FAAccrualTran ext = Base.GLTransactions.Current;

            var fixedAssetCache = Base.Caches[typeof(FixedAsset)];
            FixedAssetExt assetExt = row.GetExtension<FixedAssetExt>();
            assetExt.UsrUnitOfMeasure = ext.GLTranUOM;
        }

        #endregion
    }
}
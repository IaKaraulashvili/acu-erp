using System.Collections;
using System.Collections.Generic;
using PX.Data;
using AG.DAC;
using AG.Shared;

namespace PX.Objects.FA
{
    public class AssetMaint_Extension : PXGraphExtension<AssetMaint>
    {
        public override void Initialize()
        {
            base.Initialize();
            this.reportsMenu.AddMenuAction(printFA);
        }

        #region Event Handlers

        protected virtual void FixedAsset_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {
            var r = Base.Asset.Current;
            if (r == null) return;
            printFA.SetEnabled(CanViewRelatedAP());
        }

        protected virtual void FixedAsset_ClassID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (FixedAsset)e.Row;
            var rowExt = PXCache<FixedAsset>.GetExtension<FixedAssetExt>(row);

            if (e.OldValue != null) return;

            var fixedAsset = (FixedAsset)PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.classID>>>>.Select(Base, row.ClassID);

            rowExt.UsrUnitOfMeasure = sender.GetExtension<FixedAssetExt>(fixedAsset).UsrUnitOfMeasure;
        }

        #endregion


        #region Actions

        public PXAction<FixedAsset> reportsMenu;
        [PXUIField(DisplayName = "Reports")]
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        public virtual void ReportsMenu()
        {

        }

        public PXAction<FixedAsset> printFA;
        [PXUIField(DisplayName = "View Related AP Bill", MapViewRights = PXCacheRights.Select, MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        public virtual IEnumerable PrintFA(PXAdapter adapter)
        {
            if (Base.Asset.Current != null)
            {
                Dictionary<string, string> info = new Dictionary<string, string>();
                info["AssetCD"] = Base.Asset.Current.AssetCD.ToString();

                throw new PXReportRequiredException(info, "AG6010FA", "View Related AP Bill");
            }
            return adapter.Get();
        }

        #endregion

        public bool CanViewRelatedAP()
        {
            bool result = false;
            var r = Base.Asset.Current;
            using (new PXConnectionScope())
            {
                var asset = PXSelect<AGFARelated,
                                Where<AGFARelated.assetCD, Equal<Required<FixedAsset.assetCD>>,
                                    And<AGFARelated.type, Equal<AGFARelatedTypes.aPInvoice>>>>.Select(Base, r.AssetCD);
                if (asset.Count > 0)
                    result = true;
            }
            return result;
        }
    }
}
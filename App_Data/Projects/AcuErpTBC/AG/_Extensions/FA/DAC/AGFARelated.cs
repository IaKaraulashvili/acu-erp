﻿namespace AG.DAC
{
    using System;
    using PX.Data;
    using PX.Objects.FA;
    using Shared;

    [System.SerializableAttribute()]
	public class AGFARelated : PX.Data.IBqlTable
	{
		#region FARelatedID
		public abstract class fARelatedID : PX.Data.IBqlField
		{
		}
		protected int? _FARelatedID;
		[PXDBIdentity()]
		[PXUIField(Enabled = false)]
		public virtual int? FARelatedID
		{
			get
			{
				return this._FARelatedID;
			}
			set
			{
				this._FARelatedID = value;
			}
		}
		#endregion
		#region Type
		public abstract class type : PX.Data.IBqlField
		{
		}
		protected string _Type;
		[PXDBString(2, IsFixed = true, IsKey = true)]
		[PXUIField(DisplayName = "Type")]
		[AGFARelatedType]
		public virtual string Type
		{
			get
			{
				return this._Type;
			}
			set
			{
				this._Type = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(256, IsUnicode = true)]
		[PXUIField(DisplayName = "Description", Enabled = false)]
		public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
		#endregion
		#region AssetClass
		public abstract class assetClass : PX.Data.IBqlField
		{
		}
		protected int? _AssetClass;
		[PXDBInt()]
		[PXUIField(DisplayName = "Asset Class", Enabled = false)]
		[PXSelector(typeof(Search<FAClass.assetID, Where<FAClass.recordType, Equal<FARecordType.classType>>>),
            typeof(FAClass.assetCD),
            typeof(FAClass.assetTypeID),
            typeof(FAClass.description),
            typeof(FAClass.usefulLife),
            SubstituteKey = typeof(FAClass.assetCD),
            DescriptionField = typeof(FAClass.description),
            CacheGlobal = true)]
		public virtual int? AssetClass
		{
			get
			{
				return this._AssetClass;
			}
			set
			{
				this._AssetClass = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected string _Status;
		[PXDBString(1, IsFixed = true)]
		[PXUIField(DisplayName = "Status", Enabled = false)]
		[PXStringList(new string[]
                        {
                        FARelatedStatus.Active,
                        FARelatedStatus.Hold,
                        FARelatedStatus.Suspended,
                        FARelatedStatus.FullyDepreciated,
                        FARelatedStatus.Disposed,
                        FARelatedStatus.UnderConstruction,
                        FARelatedStatus.Dekitting,
                        FARelatedStatus.Reversed
                        },
                        new string[]
                        {
                        FARelatedStatus.UI.Active,
                        FARelatedStatus.UI.Hold,
                        FARelatedStatus.UI.Suspended,
                        FARelatedStatus.UI.FullyDepreciated,
                        FARelatedStatus.UI.Disposed,
                        FARelatedStatus.UI.UnderConstruction,
                        FARelatedStatus.UI.Dekitting,
                        FARelatedStatus.UI.Reversed
                        })]
		public virtual string Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region ReceiptDate
		public abstract class receiptDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _ReceiptDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Receipt Date", Enabled = false)]
		public virtual DateTime? ReceiptDate
		{
			get
			{
				return this._ReceiptDate;
			}
			set
			{
				this._ReceiptDate = value;
			}
		}
		#endregion
		#region PlacedInServiceDate
		public abstract class placedInServiceDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _PlacedInServiceDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Placed in Service Date", Enabled = false)]
		public virtual DateTime? PlacedInServiceDate
		{
			get
			{
				return this._PlacedInServiceDate;
			}
			set
			{
				this._PlacedInServiceDate = value;
			}
		}
		#endregion
		#region OrigAcquisitionCost
		public abstract class origAcquisitionCost : PX.Data.IBqlField
		{
		}
		protected decimal? _OrigAcquisitionCost;
		[PXDBDecimal(4)]
		[PXUIField(DisplayName = "Orig. Acquisition Cost", Enabled = false)]
		public virtual decimal? OrigAcquisitionCost
		{
			get
			{
				return this._OrigAcquisitionCost;
			}
			set
			{
				this._OrigAcquisitionCost = value;
			}
		}
		#endregion
		#region UsefulLifeYears
		public abstract class usefulLifeYears : PX.Data.IBqlField
		{
		}
		protected decimal? _UsefulLifeYears;
		[PXDBDecimal(4)]
		[PXUIField(DisplayName = "Useful Life Years", Enabled = false)]
		public virtual decimal? UsefulLifeYears
		{
			get
			{
				return this._UsefulLifeYears;
			}
			set
			{
				this._UsefulLifeYears = value;
			}
		}
		#endregion
		#region Department
		public abstract class department : PX.Data.IBqlField
		{
		}
		protected string _Department;
		[PXDBString(10, IsUnicode = true)]
		[PXUIField(DisplayName = "Department", Enabled = false)]
		public virtual string Department
		{
			get
			{
				return this._Department;
			}
			set
			{
				this._Department = value;
			}
		}
		#endregion
		#region ExpenseAmount
		public abstract class expenseAmount : PX.Data.IBqlField
		{
		}
		protected decimal? _ExpenseAmount;
		[PXUIField(DisplayName = "Expense Amount")]
		[PXDBDecimal(4)]
		public virtual decimal? ExpenseAmount
		{
			get
			{
				return this._ExpenseAmount;
			}
			set
			{
				this._ExpenseAmount = value;
			}
		}
		#endregion
		#region AssetCD
		public abstract class assetCD : PX.Data.IBqlField
		{
		}
		protected string _AssetCD;
		[PXDBString(15, IsUnicode = true, IsKey = true)]
		[PXUIField(DisplayName = "Fixed Asset")]
		[PXSelector(typeof(FixedAsset.assetCD), CacheGlobal = true, DescriptionField = typeof(FixedAsset.assetCD))]
		public virtual string AssetCD
		{
			get
			{
				return this._AssetCD;
			}
			set
			{
				this._AssetCD = value;
			}
		}
		#endregion
		#region RefNbr
		public abstract class refNbr : PX.Data.IBqlField
		{
		}
		protected string _RefNbr;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "RefNbr")]
        [PXDefault("")]
        public virtual string RefNbr
		{
			get
			{
				return this._RefNbr;
			}
			set
			{
				this._RefNbr = value;
			}
		}
		#endregion
	}

    public class FARelatedStatus
    {
        public const string Active = "A";
        public const string Hold = "H";
        public const string Suspended = "S";
        public const string FullyDepreciated = "F";
        public const string Disposed = "D";
        public const string UnderConstruction = "C";
        public const string Dekitting = "K";
        public const string Reversed = "R";

        public class UI
        {
            public const string Active = "Active";
            public const string Hold = "Hold";
            public const string Suspended = "Suspended";
            public const string FullyDepreciated = "FullyDepreciated";
            public const string Disposed = "Disposed";
            public const string UnderConstruction = "UnderConstruction";
            public const string Dekitting = "Dekitting";
            public const string Reversed = "Reversed";
        }
    }
}

using PX.Data.EP;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.FA
{
    public class FixedAssetExt : PXCacheExtension<PX.Objects.FA.FixedAsset>
    {
        #region UsrUnitOfMeasure
       
        [PXDBString(6)]
        [PXUIField(DisplayName = "Unit of Measure")]
        [PXSelector(typeof(Search4<INUnit.fromUnit, Where<INUnit.unitType, Equal<INUnitType.global>>, Aggregate<GroupBy<INUnit.fromUnit>>>))]
        public virtual string UsrUnitOfMeasure { get; set; }
        public abstract class usrUnitOfMeasure : IBqlField { }
        #endregion
    }
}
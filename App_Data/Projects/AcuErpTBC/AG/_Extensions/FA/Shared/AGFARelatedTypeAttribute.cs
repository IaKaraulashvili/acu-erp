﻿

using AG.DAC;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.Shared
{
    public class AGFARelatedTypeAttribute : PXEventSubscriberAttribute, IPXFieldDefaultingSubscriber
    {
        public virtual void FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (e.Row == null) return;
            AGFARelated fa = (AGFARelated)e.Row;
            switch (sender.Graph.GetType().Name)
            {
                case "Cst_POReceiptEntry": fa.Type = AGFARelatedTypes.POReceipt; break;
                case "Cst_APInvoiceEntry": fa.Type = AGFARelatedTypes.APInvoice; break;
                case "Cst_POOrderEntry": fa.Type = AGFARelatedTypes.POOrder; break;
                default:
                    break;
            }
        }
    }
}

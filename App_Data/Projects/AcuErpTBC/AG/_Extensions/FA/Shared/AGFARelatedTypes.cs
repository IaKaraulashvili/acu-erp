﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AG.Shared
{
    public class AGFARelatedTypes
    {
        public const string POReceipt = "PR";
        public class pOReceipt : Constant<String>
        {
            public pOReceipt()
                : base(POReceipt)
            {
            }
        }

        public const string APInvoice = "AB";
        public class aPInvoice : Constant<String>
        {
            public aPInvoice()
                : base(APInvoice)
            {
            }
        }

        public const string POOrder = "PO";
        public class pOOrder : Constant<String>
        {
            public pOOrder()
                : base(POOrder)
            {
            }
        }
    }
}

﻿using System;
using PX.Data;
using PX.Objects.IN;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.EP;

namespace AG.Extensions.FA.Shared
{
    public class FixedAssetSelectorAttribute : PXSelectorAttribute
    {
        public FixedAssetSelectorAttribute() : base(typeof(Search2<FixedAsset.assetID, LeftJoin<FADetails, On<FADetails.assetID, Equal<FixedAsset.assetID>>,
            LeftJoin<FALocationHistory, On<FALocationHistory.assetID, Equal<FADetails.assetID>,
                                        And<FALocationHistory.revisionID, Equal<FADetails.locationRevID>>>,
            LeftJoin<Branch, On<Branch.branchID, Equal<FALocationHistory.locationID>>,
            LeftJoin<EPEmployee, On<EPEmployee.bAccountID, Equal<FALocationHistory.employeeID>>>>>>,
            Where<FixedAsset.recordType, Equal<FARecordType.assetType>>>),
            typeof(FixedAsset.assetCD),
            typeof(FixedAsset.description),
            typeof(FixedAsset.classID),
            typeof(FixedAsset.usefulLife),
            typeof(FixedAsset.assetTypeID),
            typeof(FADetails.status),
            typeof(Branch.branchCD),
            typeof(EPEmployee.acctName),
            typeof(FALocationHistory.department))        
        {
            Filterable = true;
            SubstituteKey = typeof(FixedAsset.assetCD);
            DescriptionField = typeof(FixedAsset.description);
        }
    }
}

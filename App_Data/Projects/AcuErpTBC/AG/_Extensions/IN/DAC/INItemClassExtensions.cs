using PX.Common;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.IN;
using PX.Objects.TX;
using PX.Objects;
using PX.TM;
using System.Collections.Generic;
using System;

namespace PX.Objects.IN
{
    public class INItemClassExt : PXCacheExtension<PX.Objects.IN.INItemClass>
    {
        #region UsrLowCostAsset
        [PXDBBool]
        [PXUIField(DisplayName = "Low Cost Asset")]
        public virtual bool? UsrLowCostAsset { get; set; }
        public abstract class usrLowCostAsset : IBqlField { }
        #endregion
        #region UsrB6Account
        [PXDBString(50)]
        [PXUIField(DisplayName = "B6 Account For NBG")]

        public virtual string UsrB6Account { get; set; }
        public abstract class usrB6Account : IBqlField { }
        #endregion
    }
}
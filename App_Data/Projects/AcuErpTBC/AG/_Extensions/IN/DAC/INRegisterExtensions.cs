using PX.Data;
using PX.Objects.EP;
using PX.Objects.CR;
using AG.Extensions.EP.Shared;
using PX.Data.ReferentialIntegrity.Attributes;
using AG.RS.DAC;
using AG.RS.Shared;

namespace PX.Objects.IN
{
    public class INRegisterExt : PXCacheExtension<PX.Objects.IN.INRegister>
    {
        #region UsrEmployeeID
        [PXDBInt]
        [PXUIField(DisplayName = "Employee")]
        [EmployeeSelector]
        public virtual int? UsrEmployeeID { get; set; }
        public abstract class usrEmployeeID : IBqlField { }
        #endregion

        #region UsrDepartment
        [PXDBString(10, IsUnicode = true)]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<INRegisterExt.usrEmployeeID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        [PXUIField(DisplayName = "Department")]
        public virtual string UsrDepartment { get; set; }
        public abstract class usrDepartment : IBqlField { }
        #endregion

        #region UsrBuildingID
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID>),
            SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXUIField(DisplayName = "Building")]
        public virtual int? UsrBuildingID { get; set; }
        public abstract class usrBuildingID : IBqlField { }
        #endregion

        #region UsrCreateWaybill
        [PXDBBool]
        [PXUIField(DisplayName = "Requires Waybill")]
        public virtual bool? UsrCreateWaybill { get; set; }
        public abstract class usrCreateWaybill : IBqlField { }
        #endregion

        #region UsrSiteID
        [PXDBInt]
        [PXSelector(typeof(Search<INSite.siteID>),
            SubstituteKey = typeof(INSite.descr), DescriptionField = typeof(INSite.descr))]
        [PXUIField(DisplayName = "Warehouse")]
        public virtual int? UsrSiteID { get; set; }
        public abstract class usrSiteID : IBqlField { }
        #endregion

        #region UsrSiteAddress
        [PXDBString(250,IsUnicode =true)]
        [PXUIField(DisplayName = "Address Line 1", Enabled = false)]
        public virtual string UsrSiteAddress { get; set; }
        public abstract class usrSiteAddress : IBqlField { }
        #endregion

        #region UsrWaybillRefNbr
        [PXDBString(15, IsUnicode = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [PXUIField(DisplayName = "Sent Waybill Nbr.", Enabled = false)]
        [WaybillSelector]
        public virtual string UsrWaybillRefNbr { get; set; }
        public abstract class usrWaybillRefNbr : IBqlField { }
        #endregion
    }
}
using PX.Data;
using PX.Objects.EP;
using PX.Objects.CR;
using AG.Extensions.EP.Shared;

namespace PX.Objects.IN
{
    public class INTranExt : PXCacheExtension<PX.Objects.IN.INTran>
    {
        #region UsrLowCostAsset
        [PXDBBool]
        [PXUIField(DisplayName = "Low Cost Asset")]
        [PXDefault(typeof(Search<InventoryItemExt.usrLowCostAsset, Where<InventoryItem.inventoryID, Equal<Current<INTran.inventoryID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual bool? UsrLowCostAsset { get; set; }
        public abstract class usrLowCostAsset : IBqlField { }
        #endregion

        #region UsrEmployeeID
        [PXDBInt]
        [PXUIField(DisplayName = "Employee")]
        [EmployeeSelector]
        public virtual int? UsrEmployeeID { get; set; }
        public abstract class usrEmployeeID : IBqlField { }
        #endregion

        #region UsrDepartment
        [PXUIField(DisplayName = "Department")]
        [PXDBString(10, IsUnicode = true)]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<INTranExt.usrEmployeeID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        public virtual string UsrDepartment { get; set; }
        public abstract class usrDepartment : IBqlField { }
        #endregion

        #region UsrBuildingID
        [PXDBInt]
        [PXSelector(typeof(Search<Building.buildingID>),
         SubstituteKey = typeof(Building.buildingCD), DescriptionField = typeof(Building.description))]
        [PXUIField(DisplayName = "Building")]
        public virtual int? UsrBuildingID { get; set; }
        public abstract class usrBuildingID : IBqlField { }
        #endregion
    }
}
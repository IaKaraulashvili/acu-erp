using System.Collections;
using System.Collections.Generic;
using PX.Data;
using System.Linq;
using AG.EA;
using AG.EA.DAC;
using PX.Objects.CR;
using AG.RS;
using AG.RS.DAC;
using AG.RS.Shared;

namespace PX.Objects.IN
{

    public class INIssueEntry_Extension : PXGraphExtension<INIssueEntry>
    {
        public PXSelect<RSWaybill,
            Where<RSWaybill.waybillNbr, Equal<Current<INRegisterExt.usrWaybillRefNbr>>>> Waybills;

        public PXSetup<EAAssetSetup> Setup;

        public override void Initialize()
        {
            base.Initialize();
            this.actionsMenu.AddMenuAction(createWB);
            this.actionsMenu.AddMenuAction(LCACreateAction);
        }


        #region Event Handlers
        protected virtual void INTran_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (INTran)e.Row;
            if (row == null) return;
            var regExt = Base.issue.Current.GetExtension<INRegisterExt>();
            if (regExt.UsrCreateWaybill.GetValueOrDefault())
                PXUIFieldAttribute.SetEnabled<INTranExt.usrBuildingID>(sender, row, false);
        }

        protected virtual void INRegister_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            LCACreateAction.SetEnabled(!(sender.GetStatus(e.Row) == PXEntryStatus.Inserted));

            //LCACreateAction.SetEnabled(false);
            var row = (INRegister)e.Row;
            if (row == null) return;

            var regExt = row.GetExtension<INRegisterExt>();

            //PXUIFieldAttribute.SetVisible<INRegisterExt.usrSiteID>(sender, row, regExt.UsrCreateWaybill != null && regExt.UsrCreateWaybill.Value);
            //PXUIFieldAttribute.SetVisible<INRegisterExt.usrSiteAddress>(sender, row, regExt.UsrCreateWaybill != null && regExt.UsrCreateWaybill.Value);
            //PXUIFieldAttribute.SetVisible<RSWaybill.waybillNbr>(sender, row, regExt.UsrCreateWaybill != null && regExt.UsrCreateWaybill.Value);
            //PXUIFieldAttribute.SetVisible<RSWaybill.status>(sender, row, regExt.UsrCreateWaybill != null && regExt.UsrCreateWaybill.Value);
            //PXUIFieldAttribute.SetVisible<RSWaybill.linkedWaybillNbr>(sender, row, regExt.UsrCreateWaybill != null && regExt.UsrCreateWaybill.Value);

            using (new PXConnectionScope())
            {
                var actionEnabled = Base.release.GetEnabled();//!row.Status == INDocStatus.Released;

                var lcaRegistration = PXSelectReadonly<EALCARegistration,
                    Where<EALCARegistration.iNRegisterRefNbr, Equal<Required<EALCARegistration.iNRegisterRefNbr>>>>
                        .Select(Base, row.RefNbr).FirstTableItems;


                var transactions = Base.transactions.Select().FirstTableItems;
                bool usrLowCostAsset = false;
                foreach (var tran in transactions)
                {
                    var tranExt = tran.GetExtension<INTranExt>();
                    usrLowCostAsset = tranExt.UsrLowCostAsset.GetValueOrDefault();
                    if (!usrLowCostAsset)
                        break;
                }

                bool isLcaRegistered = lcaRegistration.Count() > 0;
                bool isIssueReleased = row.Status == INDocStatus.Released;

                LCACreateAction.SetEnabled(isIssueReleased & !isLcaRegistered & usrLowCostAsset == true);
                createWB.SetEnabled(regExt.UsrCreateWaybill.GetValueOrDefault() && isIssueReleased && regExt.UsrWaybillRefNbr == null);
            }
        }

        protected virtual void INRegister_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {

        }

        protected virtual void INRegister_UsrEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INRegister)e.Row;
            if (row == null) return;

            sender.SetDefaultExt<INRegisterExt.usrDepartment>(row);
        }

        protected virtual void INTran_UsrEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INTran)e.Row;
            if (row == null) return;

            sender.SetDefaultExt<INTranExt.usrDepartment>(row);
        }

        protected virtual void INRegister_UsrSiteID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INRegister)e.Row;
            if (row == null) return;

            var regExt = row.GetExtension<INRegisterExt>();
            if (regExt.UsrSiteID != null)
                regExt.UsrSiteAddress = GetAddress(regExt.UsrSiteID.Value);
            else
                regExt.UsrSiteAddress = null;
        }

        protected virtual void INRegister_UsrBuildingID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INRegister)e.Row;
            if (row == null) return;

            var regExt = row.GetExtension<INRegisterExt>();
            if (regExt.UsrCreateWaybill.GetValueOrDefault() && regExt.UsrBuildingID != null)
            {
                foreach (INTran item in Base.transactions.Select())
                {
                    item.GetExtension<INTranExt>().UsrBuildingID = regExt.UsrBuildingID;
                    Base.transactions.Update(item);
                }
            }
        }

        protected virtual void INTran_InventoryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INTran)e.Row;
            if (row == null) return;

            sender.SetDefaultExt<INTranExt.usrLowCostAsset>(row);
        }

        protected virtual void INTran_RowInserting(PXCache sender, PXRowInsertingEventArgs e)
        {
            var row = (INTran)e.Row;
            if (row == null) return;

            var issue = Base.issue.Current;
            if (issue == null) return;

            var rowExt = row.GetExtension<INTranExt>();
            var issueExt = issue.GetExtension<INRegisterExt>();

            rowExt.UsrEmployeeID = issueExt.UsrEmployeeID;
            rowExt.UsrDepartment = issueExt.UsrDepartment;
            rowExt.UsrBuildingID = issueExt.UsrBuildingID;
        }

        protected virtual void INTran_SiteID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (INTran)e.Row;
            if (row == null) return;

            var issue = Base.issue.Current;
            if (issue == null) return;

            var issueExt = issue.GetExtension<INRegisterExt>();

            if (issueExt.UsrCreateWaybill.GetValueOrDefault() && row.SiteID != null && issueExt.UsrSiteID == null)
            {
                issueExt.UsrSiteID = row.SiteID;
                issueExt.UsrSiteAddress = GetAddress(issueExt.UsrSiteID.Value);
            }
        }

        #endregion

        public PXAction<INRegister> actionsMenu;
        [PXUIField(DisplayName = "Actions")]
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        public virtual void ActionsMenu()
        {

        }

        public PXAction<INRegister> createWB;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Create Waybill")]
        protected IEnumerable CreateWB(PXAdapter adapter)
        {

            var issue = Base.issue.Current;
            if (issue == null) return null;

            CreateWaybill();

            return adapter.Get();
        }

        public delegate IEnumerable ReleaseDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable Release(PXAdapter adapter, ReleaseDelegate baseMethod)
        {

            var issue = Base.issue.Current;
            var transactions = Base.transactions.Select().FirstTableItems;
            bool? lowCostAsset = transactions.FirstOrDefault()?.GetExtension<INTranExt>()?.UsrLowCostAsset;

            var issueExt = issue.GetExtension<INRegisterExt>();

            var lcaRegistration = (EALCARegistration)PXSelectReadonly<EALCARegistration,
                    Where<EALCARegistration.iNRegisterRefNbr, Equal<Required<EALCARegistration.iNRegisterRefNbr>>>>
                        .Select(Base, issue.RefNbr);

            if (lcaRegistration != null && !string.IsNullOrEmpty(lcaRegistration.WaybillRefNbr))
                throw new PXException(AG.Common.Messages.WaybillAlreadyCreatedFromLCABulkReg);

            else if (issueExt.UsrCreateWaybill.GetValueOrDefault() && ((Base.transactions.Select().Count > 0 && issueExt.UsrSiteID == null) || transactions.Where(x => x.SiteID == issueExt.UsrSiteID).Count() == 0))
            {
                PXCache cacheIn = Base.Caches[typeof(INRegisterExt)];
                cacheIn.RaiseExceptionHandling<INRegisterExt.usrSiteID>(issue, issueExt.UsrSiteID,
                 new PXSetPropertyException(AG.Common.Messages.INIssueSourseAddress, PXErrorLevel.Error));
                return null;
            }

            else if (issueExt.UsrCreateWaybill.GetValueOrDefault() && transactions.Select(x => x.SiteID).Distinct().Count() > 1)
            {
                var wareHouse = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(Base, issueExt.UsrSiteID).FirstTableItems;
                if (Base.issue.Ask(Messages.Warning, "Different source addresses are defined on transaction details. Do you want to processed waybill with <" + wareHouse.First().Descr + "> warehouse?", MessageButtons.YesNo, false) == WebDialogResult.No)
                    return null;
            }


            foreach (INTran item in transactions)
            {
                var itemExt = item.GetExtension<INTranExt>();
                if (itemExt.UsrLowCostAsset.GetValueOrDefault() != lowCostAsset.GetValueOrDefault())
                    throw new PXException(AG.Common.Messages.LCALowCostAssetError);
            }

            PXCache cache = Base.issue.Cache;
            List<INRegister> list = new List<INRegister>();
            foreach (INRegister indoc in adapter.Get<INRegister>())
            {
                if (indoc.Hold == false && indoc.Released == false)
                {
                    cache.Update(indoc);
                    list.Add(indoc);
                }
            }

            if (list.Count == 0)
            {
                throw new PXException(Messages.Document_Status_Invalid);
            }

            Base.Save.Press();

            PXLongOperation.StartOperation(Base, delegate ()
            {
                bool bulkRegAssetAutoCreate = Setup.Current.LCABulkRegistrationAutoCreate.GetValueOrDefault() && lowCostAsset.GetValueOrDefault();
                LCARegistrationEntry assetBulkRegGraph = null;

                using (var tran = new PXTransactionScope())
                {
                    if (bulkRegAssetAutoCreate)
                    {
                        assetBulkRegGraph = CreateLca();
                    }

                    INDocumentRelease.ReleaseDoc(list, false);
                    tran.Complete();
                }

                if (bulkRegAssetAutoCreate)
                {
                    throw new PXRedirectRequiredException(assetBulkRegGraph, true, "") { Mode = PXBaseRedirectException.WindowMode.InlineWindow };
                }

            });

            return list;

        }

        public PXAction<INRegister> LCACreateAction;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Create LCAs")]
        protected IEnumerable lCACreateAction(PXAdapter adapter = null)
        {
            var assetBulkRegGraph = CreateLca();
            throw new PXRedirectRequiredException(assetBulkRegGraph, true, "") { Mode = PXBaseRedirectException.WindowMode.InlineWindow };
        }

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var issue = Base.issue.Current;
            if (issue == null) return;
            var issueExt = issue.GetExtension<INRegisterExt>();
            if (issueExt.UsrCreateWaybill.GetValueOrDefault() && issueExt.UsrBuildingID == null)
            {
                PXCache cache = Base.Caches[typeof(INRegisterExt)];
                cache.RaiseExceptionHandling<INRegisterExt.usrBuildingID>(issue, issueExt.UsrSiteID,
                 new PXSetPropertyException("The value can not be empty", PXErrorLevel.Error));
            }
            baseMethod();
        }

        public IEnumerable<INTranSplit> GetTranSplits(string TranNbr, int? LineNbr)
        {

            var TranSplit = PXSelect<INTranSplit,
                Where<INTranSplit.tranType, Equal<Current<INTran.tranType>>,
                    And<INTranSplit.refNbr, Equal<Required<INTran.refNbr>>,
                        And<INTranSplit.lineNbr, Equal<Required<INTran.lineNbr>>>>>>.Select(Base, TranNbr, LineNbr).FirstTableItems;

            return TranSplit;
        }

        public LCARegistrationEntry CreateLca()
        {

            LCARegistrationEntry assetBulkRegGraph = PXGraph.CreateInstance<LCARegistrationEntry>();

            using (var tran = new PXTransactionScope())
            {
                var issue = Base.issue.Current;
                var transactions = Base.transactions.Select().FirstTableItems;

                EALCARegistration lcaRegDoc = assetBulkRegGraph.LCARegistration.Insert();
                lcaRegDoc.INRegisterRefNbr = issue.RefNbr;

                List<INTranSplit> splits = new List<INTranSplit>();
                //INTranSplit Dac
                foreach (INTran item in transactions)
                {
                    INTranExt tranExt = item.GetExtension<INTranExt>();

                    IEnumerable<INTranSplit> tranSplits = GetTranSplits(item.RefNbr, item.LineNbr);

                    foreach (var splitItem in tranSplits)
                    {
                        var row = assetBulkRegGraph.LCARegistrationDetails.Insert();

                        assetBulkRegGraph.LCARegistrationDetails.SetValueExt<EALCARegistrationDetail.inventoryID>(row, splitItem.InventoryID);
                        assetBulkRegGraph.LCARegistrationDetails.SetValueExt<EALCARegistrationDetail.employeeID>(row, tranExt.UsrEmployeeID);

                        row.LotSerialNbr = splitItem.LotSerialNbr;
                        row.Cost = splitItem.UnitCost;
                        row.Department = tranExt.UsrDepartment;
                        row.BuildingID = tranExt.UsrBuildingID;
                        row.SiteID = item.SiteID;
                    }

                }

                if (Setup.Current.LCABulkRegistrationAutoRelease.GetValueOrDefault())
                {
                    assetBulkRegGraph.Save.Press();
                    assetBulkRegGraph.ReleaseDocument(lcaRegDoc);
                }

                tran.Complete();
            }

            return assetBulkRegGraph;
        }

        public void CreateWaybill()
        {
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            var issue = Base.CurrentDocument.Current;
            PXResultset<INTran> documentDetails = Base.transactions.Select();

            RSWaybill waybill = graph.Waybills.Insert();
            waybill.WaybillType = WaybillType.Internal;
            waybill.WaybillCategory = WaybillCategory.Normal;
            waybill.SourceNbr = issue.RefNbr;
            waybill.SourceType = WaybillSourceType.InventoryIssues;
            var extRegister = issue.GetExtension<INRegisterExt>();
            waybill.SourceAddress = extRegister.UsrSiteAddress;

            var building = PXSelect<Building, Where<Building.buildingID, Equal<Required<Building.buildingID>>>>.Select(Base, extRegister.UsrBuildingID).FirstTableItems;

            waybill.SupplierDestinationAddress = building.First().Description;
            waybill.DestinationAddress= building.First().Description;
            SetBranch(waybill, issue, graph);

            foreach (INTran item in documentDetails)
            {
                RSWaybillItem waybillItem = graph.WaybillItems.Insert();
                graph.WaybillItems.Cache.SetValueExt<RSWaybillItem.inventoryID>(waybillItem, (int)item.InventoryID);
                waybillItem.WaybillItemType = WaybillItemType.Inventory;
                waybillItem.ItemQty = item.Qty;
                waybillItem.ItemQty = item.Qty;
                waybillItem.UnitPrice = item.UnitPrice;
                waybillItem.ItemAmt = item.TranAmt;
                waybillItem.TaxType = GETaxTypes.Normal;
            }

            throw new PXPopupRedirectException(graph, "ViewWaybill", true);
        }

        public void SetBranch(RSWaybill waybill, INRegister transfer, WaybillEntry graph)
        {
            var warehouse = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, transfer.SiteID).FirstTableItems;
            if (warehouse.Count() > 0)
            {
                graph.Waybills.Cache.SetValueExt<RSWaybill.branchID>(waybill, warehouse.First().BranchID);
            }
        }

        public string GetAddress(int siteID)
        {
            string result = "";
            var address = PXSelectJoin<Address, InnerJoin<INSite, On<Address.addressID, Equal<INSite.addressID>>>, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(Base, siteID).FirstTableItems;
            if (address != null && address.Count() > 0)
                result = address.First().AddressLine1;
            return result;
        }
    }
}
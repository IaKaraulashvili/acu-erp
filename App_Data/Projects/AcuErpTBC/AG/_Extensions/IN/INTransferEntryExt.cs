using System;
using System.Collections;
using System.Linq;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using AG.RS;
using AG.RS.Shared;

namespace PX.Objects.IN
{
    public class INTransferEntryExt : PXGraphExtension<INTransferEntry>
    {
        #region Views

        public PXSelect<RSWaybill,
            Where<RSWaybill.waybillNbr, Equal<Current<INRegisterExt.usrWaybillRefNbr>>>> Waybills;

        #endregion

        #region Properties

        public bool Post { get; set; }

        #endregion

        #region Overrides

        public override void Initialize()
        {
            actionsMenu.AddMenuAction(createWB);
        } 

        #endregion

        #region Event Handlers

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Sent Waybill Nbr.")]
        protected void RSWaybill_WaybillNbr_CacheAttached(PXCache sender) { }

        protected void INRegister_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (INRegister)e.Row;
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            if (row != null)
            {
                var ext = row.GetExtension<INRegisterExt>();
                var isEnabled = ext.UsrWaybillRefNbr == null && row.Status == INDocStatus.Released;

                createWB.SetEnabled(isEnabled);
            }
        } 

        #endregion

        #region Actions

        public PXAction<INRegister> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        public PXAction<INRegister> createWB;
        [PXUIField(DisplayName = "Create Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        public virtual IEnumerable CreateWB(PXAdapter adapter)
        {
            CreateWaybill();

            return adapter.Get();
        }

        #endregion

        #region Other Methods

        void CreateWaybill()
        {
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            var transfer = Base.CurrentDocument.Current;
            PXResultset<INTran> documentDetails = Base.transactions.Select();

            RSWaybill waybill = graph.Waybills.Insert();
            waybill.WaybillType = WaybillType.Internal;
            waybill.WaybillCategory = WaybillCategory.Normal;
            waybill.SourceNbr = transfer.RefNbr;
            waybill.SourceType = WaybillSourceType.InventoryTransfer;
            SetBranchInfo(waybill, transfer, graph);
            SetAddresses(waybill, transfer, graph);

            foreach (INTran item in documentDetails)
            {
                RSWaybillItem waybillItem = graph.WaybillItems.Insert();
                graph.WaybillItems.Cache.SetValueExt<RSWaybillItem.inventoryID>(waybillItem, (int)item.InventoryID);
                waybillItem.WaybillItemType = WaybillItemType.Inventory;
                waybillItem.ItemQty = item.Qty;
            }

            throw new PXPopupRedirectException(graph, "ViewWaybill", true);
        }

        void FillReceivedWB(RSWaybill waybill, RSReceivedWaybill receivedWb, ReceivedWaybillEntry receivedWbGraph)
        {
            receivedWb.WaybillType = WaybillType.Internal;
            receivedWb.WaybillCategory = WaybillCategory.Normal;
            receivedWb.SupplierName = waybill.SupplierName;
            receivedWb.SupplierTaxRegistrationID = waybill.SupplierTaxRegistrationID;
            receivedWb.SourceAddress = waybill.SourceAddress;
            receivedWb.DestinationAddress = waybill.DestinationAddress;
            receivedWbGraph.Waybills.Cache.SetValueExt<RSReceivedWaybill.transStartDate>(receivedWb, waybill.TransStartDate);
            receivedWbGraph.Waybills.Cache.SetValueExt<RSReceivedWaybill.transStartTime>(receivedWb, waybill.TransStartTime);
            //waybill.TransportationType = TransportationTypes.Other;
            //waybill.OtherTransportationTypeDescription = "x";
        }

        void FillReceivedWbItems(RSWaybillItem waybillItem, RSReceivedWaybillItem item, ReceivedWaybillEntry receivedWbGraph)
        {
            item.InventoryID = waybillItem.InventoryID;
            item.ItemName = waybillItem.ItemName;
            item.ExtUOM = waybillItem.ExtUOM;
            item.OtherUOMDescription = waybillItem.OtherUOMDescription;
            item.ItemCode = waybillItem.ItemCode;
            item.ItemQty = waybillItem.ItemQty;
            item.UnitPrice = 0;
            item.ItemAmt = 0;
            item.TaxType = GETaxTypes.Normal;
        }

        void FillWaybillSource(WaybillEntry graph, INRegister transfer, RSWaybill waybill)
        {
            RSWaybillSource sourceWaybill = graph.WaybillSource.Insert();
            sourceWaybill.SourceNbr = transfer.RefNbr;
            sourceWaybill.SourceType = AG.RS.DAC.RSWaybillSource.SourceTypes.Transfer;
        }

        void SetBranchInfo(RSWaybill waybill, INRegister transfer, WaybillEntry graph)
        {
            var warehouse = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, transfer.SiteID).FirstTableItems;
            if (warehouse.Count() > 0)
            {
                graph.Waybills.Cache.SetValueExt<RSWaybill.branchID>(waybill, warehouse.First().BranchID);
            }
        }

        void SetAddresses(RSWaybill waybill, INRegister transfer, WaybillEntry graph)
        {
            var warehouseFrom = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, transfer.SiteID).FirstTableItems;
            if (warehouseFrom != null && warehouseFrom.Count() > 0)
            {
                var address = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.Select(graph, warehouseFrom.First().AddressID).FirstTableItems;
                if (address != null && address.Count() > 0)
                {
                    waybill.SourceAddress = address.First().AddressLine1;
                }
            }

            var warehouseTo = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, transfer.ToSiteID).FirstTableItems;
            if (warehouseTo != null && warehouseTo.Count() > 0)
            {
                var address = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.Select(graph, warehouseTo.First().AddressID).FirstTableItems;
                if (address != null && address.Count() > 0)
                {
                    waybill.DestinationAddress = address.First().AddressLine1;
                    waybill.SupplierDestinationAddress = address.First().AddressLine1;
                }
            }
        }

        void SetBarCode(RSWaybillItem waybillItem, WaybillEntry graph, INTran tran)
        {
            var feature = PXSelect<FeaturesSet>.Select(graph).FirstTableItems;
            if (feature.Count() > 0 && feature.First().SubItem == true)
            {
                var order = PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Required<SOOrder.orderNbr>>, And<SOOrder.orderType, Equal<Required<SOOrder.orderType>>>>>.Select(graph, tran.SOOrderNbr, tran.SOOrderType).FirstTableItems;
                if (order != null && order.Count() > 0)
                {
                    var orderline = PXSelect<SOLine, Where<SOLine.orderNbr, Equal<Required<SOLine.orderNbr>>, And<SOLine.orderType,
                        Equal<Required<SOLine.orderType>>>>>.Select(graph, order.First().OrderNbr, order.First().OrderType).FirstTableItems;
                    if (orderline != null && orderline.Count() > 0 && !string.IsNullOrEmpty(orderline.First().AlternateID))
                    {
                        waybillItem.ItemCode = orderline.First().AlternateID;
                    }
                }
            }
            else
            {
                var iNItemXRef = PXSelect<INItemXRef, Where<INItemXRef.inventoryID, Equal<Required<INItemXRef.inventoryID>>>>.Select(graph, tran.InventoryID).FirstTableItems;
                if (iNItemXRef != null && iNItemXRef.Count() > 0)
                {
                    foreach (var item in iNItemXRef)
                    {
                        if (item.AlternateType == "BAR")
                        {
                            waybillItem.ItemCode = item.AlternateID;
                        }
                    }
                }
                else
                {
                    var inventoryItem = PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>.Select(graph, tran.InventoryID).FirstTableItems;
                    if (inventoryItem != null && inventoryItem.Count() > 0)
                    {
                        if (!string.IsNullOrEmpty(inventoryItem.First().InventoryCD))
                        {
                            waybillItem.ItemCode = inventoryItem.First().InventoryCD;
                        }
                    }
                }
            }
        }

        void SetUnitOfMeasure(RSWaybillItem waybillItem, INTran tran, WaybillEntry graph)
        {
            var uOM = PXSelect<RSUOMMapper, Where<RSUOMMapper.unit, Equal<Required<RSUOMMapper.unit>>>>.Select(graph, tran.UOM).FirstTableItems;
            if (uOM != null && uOM.Count() > 0)
            {
                if (!string.IsNullOrEmpty(uOM.First().Unit))
                {
                    waybillItem.ExtUOM = uOM.First().RSUOMID;
                    waybillItem.OtherUOMDescription = Convert.ToString(uOM.First().RSUOMID);
                }
            }
        }

        void SaveOrPost(WaybillEntry graph)
        {
            var setup = PXSelect<RSSetup>.Select(graph).FirstTableItems;
            if (setup.Count() > 0)
            {
                Post = (bool)setup.First().WaybillPostOnTransfer;
            }
        } 

        #endregion
    }
}

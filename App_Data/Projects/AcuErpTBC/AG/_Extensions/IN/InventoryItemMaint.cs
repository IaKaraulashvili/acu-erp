using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.SO;
using PX.SM;
using PX.Web.UI;
using PX.Objects.RUTROT;
using CRLocation = PX.Objects.CR.Standalone.Location;
using ItemStats = PX.Objects.IN.Overrides.INDocumentRelease.ItemStats;
using PX.Objects;
using PX.Objects.IN;

namespace PX.Objects.IN
{
    public class InventoryItemMaint_Extension : PXGraphExtension<InventoryItemMaint>
    {
        #region Event Handlers

        protected void InventoryItem_ItemClassID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (InventoryItem)e.Row;
            if (row == null) return;

            INItemClass ic = Base.ItemClass.Select();
            var icExt = ic.GetExtension<INItemClassExt>();

            var rowExt = row.GetExtension<InventoryItemExt>();
            rowExt.UsrLowCostAsset = icExt?.UsrLowCostAsset;
        }

        #endregion
    }
}
﻿using AG.Extensions.PO.DAC;
using PX.Data;
using AG.Utils.Extensions;

namespace AG.Extensions.PO
{
    public class AGContractMaint : PXGraph<AGContractMaint>
    {
        public PXSave<AGContract> Save;
        public PXCancel<AGContract> Cancel;

        public PXSelect<AGContract> Contracts;
     
        protected virtual void AGContract_ContractNbr_FieldVerifying(PXCache sender, PXFieldVerifyingEventArgs e)
        {
            AGContract row = (AGContract)e.Row;
            if (row == null) return;

            bool exists = !(PXSelect<AGContract, Where<AGContract.contractNbr, Equal<Required<AGContract.contractNbr>>, And<AGContract.contractID, NotEqual<Required<AGContract.contractID>>>>>
                .Select(this, e.NewValue?.ToString(), row.ContractID).FirstTableItems.IsNullOrEmpty());

            if (exists)
                throw new PXSetPropertyException<AGContract.contractNbr>(AG.Common.Messages.ContractNbrAlreadyExists);
        }
    }
}

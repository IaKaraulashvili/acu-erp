using AG.Extensions.PO.Shared;
using PX.Data;
using PX.Objects.EP;

namespace PX.Objects.PO
{
    public class POOrderExt : PXCacheExtension<PX.Objects.PO.POOrder>
    {
        #region UsrContractID
        [PXDBInt]
        [PXUIField(DisplayName = "Contract")]
        [AGContractSelector(typeof(POOrder.vendorID))]
        public virtual int? UsrContractID { get; set; }
        public abstract class usrContractID : IBqlField { }
        #endregion

        #region UsrFARelated
        [PXDBBool]
        [PXUIField(DisplayName = "FA Related")]

        public virtual bool? UsrFARelated { get; set; }
        public abstract class usrFARelated : IBqlField { }
        #endregion

        #region UsrPrepaymentIsRequired
        [PXDBBool]
        [PXUIField(DisplayName = "Prepayment")]

        public virtual bool? UsrPrepaymentIsRequired { get; set; }
        public abstract class usrPrepaymentIsRequired : IBqlField { }
        #endregion

        #region UsrPrepaymentAmt
        [PXDBDecimal]
        [PXUIField(DisplayName = "Prepayment Amount")]

        public virtual decimal? UsrPrepaymentAmt { get; set; }
        public abstract class usrPrepaymentAmt : IBqlField { }
        #endregion

        #region UsrEmployeeID
        [PXDBInt]
        [PXUIField(DisplayName = "Budget Owner Emp.")]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        public virtual int? UsrEmployeeID { get; set; }
        public abstract class usrEmployeeID : IBqlField { }
        #endregion

        #region UsrDepartment
        [PXDBString(10)]
        [PXUIField(DisplayName = "Budget Owner Dep.")]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<POOrderExt.usrEmployeeID>>>>),PersistingCheck =PXPersistingCheck.Nothing)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        public virtual string UsrDepartment { get; set; }
        public abstract class usrDepartment : IBqlField { }
        #endregion
    }
}
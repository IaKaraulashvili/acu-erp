using AG.Extensions.PO.Shared;
using AG.RS.DAC;
using PX.Data;
using PX.Objects.EP;

namespace PX.Objects.PO
{
    public class POReceiptExt : PXCacheExtension<PX.Objects.PO.POReceipt>
    {
        #region UsrContractID
        [PXDBInt]
        [PXUIField(DisplayName = "Contract")]
        [AGContractSelector(typeof(POReceipt.vendorID))]
        public virtual int? UsrContractID { get; set; }
        public abstract class usrContractID : IBqlField { }
        #endregion
        #region UsrFARelated
        [PXDBBool]
        [PXUIField(DisplayName = "FA Related")]

        public virtual bool? UsrFARelated { get; set; }
        public abstract class usrFARelated : IBqlField { }
        #endregion

        #region UsrEmployeeID
        [PXDBInt]
        [PXUIField(DisplayName = "Budget Owner Emp.")]
        [PXSelector(typeof(EPEmployee.bAccountID), SubstituteKey = typeof(EPEmployee.acctCD), DescriptionField = typeof(EPEmployee.acctName))]
        public virtual int? UsrEmployeeID { get; set; }
        public abstract class usrEmployeeID : IBqlField { }
        #endregion

        #region UsrDepartment
        [PXDBString(10)]
        [PXUIField(DisplayName = "Budget Owner Dep.")]
        [PXDefault(typeof(Search<EPEmployee.departmentID, Where<EPEmployee.bAccountID, Equal<Current<POReceiptExt.usrEmployeeID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        [PXSelector(typeof(EPDepartment.departmentID), DescriptionField = typeof(EPDepartment.description))]
        public virtual string UsrDepartment { get; set; }
        public abstract class usrDepartment : IBqlField { }
        #endregion

        #region UsrReceivedWaybillNbr
        [PXDBString(15)]
        [PXUIField(DisplayName = "Waybill Nbr")]
        [PXSelector(typeof(Search<
            RSReceivedWaybill.waybillNbr,
            Where<RSReceivedWaybill.vendorID, Equal<Current<POReceipt.vendorID>>, And<RSReceivedWaybill.pOReceiptNbr, IsNull>>>),
            SubstituteKey = typeof(RSReceivedWaybill.waybillNbr), DescriptionField = typeof(EPEmployee.acctName))]
        public virtual string UsrReceivedWaybillNbr { get; set; }
        public abstract class usrReceivedWaybillNbr : IBqlField { }
        #endregion
    }
}
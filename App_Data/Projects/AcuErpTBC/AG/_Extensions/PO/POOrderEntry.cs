using System;
using System.Collections;
using System.Linq;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.FA;
using AG.DAC;
using AG.Shared;
using PX.Objects.IN;
using System.Collections.Generic;
using PX.Objects.CS;
using PX.Data.Maintenance;
using PX.Common;

namespace PX.Objects.PO
{
    public class POOrderEntry_Extension : PXGraphExtension<POOrderEntry>
    {
        public PXSelect<AGFARelated,
                Where<AGFARelated.refNbr, Equal<Current<POOrder.orderNbr>>, And<AGFARelated.type, Equal<AGFARelatedTypes.pOOrder>>>> FARelateds;




        #region Event Handlers

        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "RefNbr")]
        [PXDBDefault(typeof(POOrder.orderNbr))]
        protected void AGFARelated_RefNbr_CacheAttached(PXCache cache)
        {

        }

        protected virtual void AGFARelated_AssetCD_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (AGFARelated)e.Row;
            if (row == null) return;

            PXResult<PX.Objects.FA.FixedAsset> asset = PXSelectJoin<FixedAsset,
                        LeftJoin<FADetails, On<FixedAsset.assetID, Equal<FADetails.assetID>>,
                        LeftJoin<FALocationHistory, On<FixedAsset.assetID, Equal<FALocationHistory.assetID>>>>,
                            Where<FixedAsset.assetCD, Equal<Required<FixedAsset.assetCD>>>>.Select(Base, row.AssetCD).FirstOrDefault();


            if (asset != null)
            {
                row.Description = asset.GetItem<FixedAsset>().Description;
                row.AssetClass = asset.GetItem<FixedAsset>().ClassID;
                row.Status = asset.GetItem<FixedAsset>().Status;
                row.UsefulLifeYears = asset.GetItem<FixedAsset>().UsefulLife;
                row.ReceiptDate = asset.GetItem<FADetails>().ReceiptDate;
                row.PlacedInServiceDate = asset.GetItem<FADetails>().DepreciateFromDate;
                row.OrigAcquisitionCost = asset.GetItem<FADetails>().AcquisitionCost;
                row.Department = asset.GetItem<FALocationHistory>().Department;
            }
        }

        protected virtual void POOrder_VendorID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            POOrder order = e.Row as POOrder;
            if (order == null) return;

            var rowExt = PXCache<POOrder>.GetExtension<POOrderExt>(order);
            rowExt.UsrContractID = null;
        }

        protected virtual void POOrder_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (POOrder)e.Row;
            if (row == null) return;
            EnableDisablePrepaymentAmt(sender, row);
            bool enableAddInvSite1 = row.VendorID != null;
            addInvBySite1.SetEnabled(enableAddInvSite1);
        }

        protected virtual void POOrder_UsrPrepaymentIsRequired_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (POOrder)e.Row;
            if (row == null) return;
            EnableDisablePrepaymentAmt(sender, row);
            var ext = PXCache<POOrder>.GetExtension<POOrderExt>(row);
            if (!ext.UsrPrepaymentIsRequired.GetValueOrDefault() && ext.UsrPrepaymentAmt != 0)
                ext.UsrPrepaymentAmt = 0;
        }

        protected virtual void POOrder_UsrEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (POOrder)e.Row;
            sender.SetDefaultExt<POOrderExt.usrDepartment>(row);
        }

        #endregion

        public delegate IEnumerable CreatePOReceiptDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable CreatePOReceipt(PXAdapter adapter, CreatePOReceiptDelegate baseMethod)
        {
            if (Base.Document.Current != null &&
                 (Base.Document.Current.OrderType == POOrderType.RegularOrder
                    || Base.Document.Current.OrderType == POOrderType.DropShip))
            {
                POOrder order = (POOrder)Base.Document.Current;
                if (order.Status == POOrderStatus.Open)
                {
                    Base.ValidateLines();
                    bool needsPOReceipt = false;
                    foreach (POLine iLn in Base.Transactions.Select())
                    {
                        if (POOrderEntry.NeedsPOReceipt(iLn, true, Base.POSetup.Current))
                        {
                            needsPOReceipt = true;
                            break;
                        }
                    }
                    if (needsPOReceipt)
                    {
                        Base.Save.Press();

                        PXLongOperation.StartOperation(Base, () =>
                        {
                            POReceiptEntry receiptGraph = POReceiptEntry.CreateInstance<POReceiptEntry>();
                            POReceipt receipt = new POReceipt
                            {
                                ReceiptType = POReceiptType.POReceipt,
                                BranchID = order.BranchID,
                                VendorID = order.VendorID,
                                PayToVendorID = order.PayToVendorID,
                                VendorLocationID = order.VendorLocationID,
                                TermsID = order.TermsID,
                                CuryID = order.CuryID
                            };
                            if (!string.IsNullOrEmpty(order.TaxZoneID))
                            {
                                receipt.TaxZoneID = order.TaxZoneID;
                            }
                            var orderExt = order.GetExtension<POOrderExt>();
                            var receiptExt = receiptGraph.Caches[typeof(POReceipt)].GetExtension<POReceiptExt>(receipt);
                            receiptExt.UsrContractID = orderExt.UsrContractID;
                            var poreceiptExt = receiptGraph.GetExtension<POReceiptEntry_Extension>();
                            receiptExt.UsrFARelated = orderExt.UsrFARelated;
                            receiptExt.UsrDepartment = orderExt.UsrDepartment;
                            receiptExt.UsrEmployeeID = orderExt.UsrEmployeeID;
                            receipt = receiptGraph.Document.Insert(receipt);

                            if (orderExt.UsrFARelated != null && orderExt.UsrFARelated.Value)
                            {

                                foreach (AGFARelated item in FARelateds.Select())
                                {
                                    AGFARelated faRelated = new AGFARelated();
                                    faRelated.AssetCD = item.AssetCD;
                                    faRelated.RefNbr = receiptGraph.Document.Current.ReceiptNbr;
                                    faRelated.ExpenseAmount = item.ExpenseAmount;
                                    poreceiptExt.FARelateds.Cache.Insert(faRelated);
                                }
                            }

                            POReceipt copy = (POReceipt)receiptGraph.Document.Cache.CreateCopy(receipt);
                            copy.CuryID = order.CuryID;
                            copy = receiptGraph.Document.Update(copy);
                            receiptGraph.AddPurchaseOrder(order);



                            if (receiptGraph.transactions.Cache.IsDirty)
                            {
                                throw new PXRedirectRequiredException(receiptGraph, Messages.POReceiptRedirection);
                            }

                            throw new PXException(Messages.POReceiptFromOrderCreation_NoApplicableLinesFound);
                        });
                    }
                }
            }

            return adapter.Get();
        }


        public delegate IEnumerable CreateAPInvoiceDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable CreateAPInvoice(PXAdapter adapter, CreateAPInvoiceDelegate baseMethod)
        {
            if (Base.Document.Current != null &&
             (Base.Document.Current.OrderType == POOrderType.RegularOrder
                || Base.Document.Current.OrderType == POOrderType.DropShip))
            {
                POOrder order = (POOrder)Base.Document.Current;
                if (order.Status == POOrderStatus.Open)
                {
                    Base.ValidateLines();

                    bool needsAPInvoice = false;
                    foreach (POLine iLn in Base.Transactions.Select())
                    {
                        if (POOrderEntry.NeedsAPInvoice(iLn, true, Base.POSetup.Current))
                        {
                            needsAPInvoice = true;
                            break;
                        }
                    }

                    if (needsAPInvoice)
                    {
                        Base.Save.Press();
                        APInvoiceEntry receiptGraph = PXGraph.CreateInstance<APInvoiceEntry>();
                        receiptGraph.InvoicePOOrder(order, true);

                        var apInvoiceExt = receiptGraph.Caches[typeof(APInvoice)].GetExtension<APRegisterExt>(receiptGraph.Document.Current);
                        var orderExt = order.GetExtension<POOrderExt>();
                        apInvoiceExt.UsrContractID = orderExt.UsrContractID;
                        apInvoiceExt.UsrEmployeeID = orderExt.UsrEmployeeID;
                        apInvoiceExt.UsrDepartment = orderExt.UsrDepartment;
                        receiptGraph.AttachPrepayment();

                        if (orderExt.UsrFARelated != null && orderExt.UsrFARelated.Value)
                        {
                            var apExt = receiptGraph.GetExtension<APInvoiceEntry_Extension>();
                            var regExt = receiptGraph.Document.Current.GetExtension<APRegisterExt>();
                            regExt.UsrFARelated = orderExt.UsrFARelated;

                            foreach (AGFARelated item in FARelateds.Select())
                            {
                                AGFARelated faRelated = new AGFARelated();
                                faRelated.AssetCD = item.AssetCD;
                                faRelated.RefNbr = receiptGraph.Document.Current.RefNbr;
                                faRelated.ExpenseAmount = item.ExpenseAmount;
                                apExt.FARelateds.Cache.Insert(faRelated);
                            }
                        }

                        throw new PXRedirectRequiredException(receiptGraph, Messages.POReceiptRedirection);
                    }
                    else
                    {
                        throw new PXException(Messages.APInvoicePOOrderCreation_NoApplicableLinesFound);
                    }
                }
            }
            return adapter.Get();
        }

        public delegate void CreatePrepaymentDelegate();
        [PXOverride]
        public void CreatePrepayment(CreatePrepaymentDelegate baseMethod)
        {
            if (Base.Document.Current != null)
            {
                Base.Save.Press();

                POOrder order = (POOrder)Base.Document.Current;
                var orderExt = order.GetExtension<POOrderExt>();

                APInvoiceEntry target = PXGraph.CreateInstance<APInvoiceEntry>();
                if (Base.Document.Current.PrepaymentRefNbr == null)
                {
                    target.Document.Insert(new APInvoice { DocType = APDocType.Prepayment });
                    Base.CreatePrePaymentProc(target);

                    var apExt = target.GetExtension<APInvoiceEntry_Extension>();
                    var regExt = target.Document.Current.GetExtension<APRegisterExt>();

                    regExt.UsrPONumber = order.OrderNbr;
                    regExt.UsrPOPrepaymentAmt = orderExt.UsrPrepaymentAmt;
                    regExt.UsrEmployeeID = orderExt.UsrEmployeeID;
                    regExt.UsrDepartment = orderExt.UsrDepartment;
                    regExt.UsrContractID = orderExt.UsrContractID;

                    if (orderExt.UsrFARelated != null && orderExt.UsrFARelated.Value)
                    {
                        regExt.UsrFARelated = orderExt.UsrFARelated;

                        foreach (AGFARelated item in FARelateds.Select())
                        {
                            AGFARelated faRelated = new AGFARelated();
                            faRelated.AssetCD = item.AssetCD;
                            faRelated.RefNbr = target.Document.Current.RefNbr;
                            faRelated.ExpenseAmount = item.ExpenseAmount;
                            apExt.FARelateds.Cache.Insert(faRelated);
                        }
                    }

                    throw new PXPopupRedirectException(target, "New Prepayment", true);
                }
                else
                {
                    throw new InvalidOperationException(PXMessages.LocalizeNoPrefix(Messages.PrepaymentAlreadyExists));
                }
            }
        }

        public delegate IEnumerable InquiryDelegate(PXAdapter adapter, Nullable<Int32> inquiryID);
        [PXOverride]
        public IEnumerable Inquiry(PXAdapter adapter, Nullable<Int32> inquiryID, InquiryDelegate baseMethod)
        {
            switch (inquiryID)
            {
                case 1:
                    if (Base.vendor.Current != null)
                    {
                        APDocumentEnq graph = PXGraph.CreateInstance<APDocumentEnq>();
                        graph.Filter.Current.VendorID = Base.vendor.Current.BAccountID;
                        var row = Base.Document.Current;
                        if (row != null)
                        {
                            var rowExt = Base.Document.Cache.GetExtension<POOrderExt>(row);
                            graph.Filter.Current.GetExtension<APDocumentFilterExt>().UsrContractID = rowExt?.UsrContractID;
                        }
                        graph.Filter.Select();
                        throw new PXRedirectRequiredException(graph, "Vendor Details");
                    }
                    break;
                case 2:
                    if (Base.Document.Current != null)
                        Base.Activity.ButtonViewAllActivities.PressButton(adapter);
                    break;
            }
            return adapter.Get();
        }

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var current = Base.Document.Current;
            if (current != null)
            {
                var ext = current.GetExtension<POOrderExt>();

                if (!current.Hold.GetValueOrDefault() && ext.UsrDepartment == null)
                {
                    PXCache cache = Base.Caches[typeof(POOrderExt)];
                    cache.RaiseExceptionHandling<POOrderExt.usrDepartment>(current, ext.UsrDepartment,
                     new PXSetPropertyException(AG.Common.Messages.BugetOwnerDepartment, PXErrorLevel.Error));
                }

                if (ext.UsrFARelated != null)
                {
                    if (ext.UsrFARelated.Value && FARelateds.Select().Count == 0)
                        throw new PXException(AG.Common.Messages.FARelatedEmpty);
                    if (!ext.UsrFARelated.Value && FARelateds.Select().Count > 0)
                    {
                        foreach (AGFARelated item in FARelateds.Select())
                            FARelateds.Delete(item);
                    }
                }
                if (ext.UsrPrepaymentIsRequired.GetValueOrDefault())
                {
                    if (ext.UsrPrepaymentAmt.GetValueOrDefault() == 0)
                    {
                        PXCache cache = Base.Caches[typeof(POOrderExt)];
                        cache.RaiseExceptionHandling<POOrderExt.usrPrepaymentAmt>(current, ext.UsrPrepaymentAmt,
                         new PXSetPropertyException(AG.Common.Messages.POOrderPrepaymentAmountZero, PXErrorLevel.Error));
                    }
                    else if (ext.UsrPrepaymentAmt.GetValueOrDefault() > Base.CurrentDocument.Current.CuryOrderTotal)
                    {
                        PXCache cache = Base.Caches[typeof(POOrderExt)];
                        cache.RaiseExceptionHandling<POOrderExt.usrPrepaymentAmt>(current, ext.UsrPrepaymentAmt,
                         new PXSetPropertyException(AG.Common.Messages.POOrderPrepaymentAmountMore, PXErrorLevel.Error));
                    }
                }


                if (current.PrepaymentRefNbr != null)
                {
                    APRegister register = PXSelect<APRegister,
                                Where<APRegister.refNbr, Equal<Required<APRegister.refNbr>>,
                                And<APRegister.docType, Equal<APDocType.prepayment>,
                                And<Where<APRegister.status, Equal<APDocStatus.hold>, Or<APRegister.status, Equal<APDocStatus.balanced>>>>>>>.Select(Base, current.PrepaymentRefNbr);

                    if (register != null)
                    {
                        var extRegister = register.GetExtension<APRegisterExt>();
                        if (extRegister.UsrPOPrepaymentAmt != ext.UsrPrepaymentAmt)
                            PXDatabase.Update<APRegister>(new PXDataFieldAssign<APRegisterExt.usrPOPrepaymentAmt>(ext.UsrPrepaymentAmt));
                    }
                }
            }
            baseMethod();
        }


        public delegate IEnumerable NotificationDelegate(PXAdapter adapter, String notificationCD);
        [PXOverride]
        [PXUIField(DisplayName = "Notificationsss", Visible = false)]
        public IEnumerable Notification(PXAdapter adapter, String notificationCD, NotificationDelegate baseMethod)
        {
            return baseMethod(adapter, notificationCD);
        }

        private void EnableDisablePrepaymentAmt(PXCache cache, POOrder order)
        {
            var rowExt = PXCache<POOrder>.GetExtension<POOrderExt>(order);
            if (!rowExt.UsrPrepaymentIsRequired.GetValueOrDefault())
            {
                PXUIFieldAttribute.SetReadOnly<POOrderExt.usrPrepaymentAmt>(cache, order, true);
                Base.createPrepayment.SetEnabled(false);
            }
            else
                PXUIFieldAttribute.SetReadOnly<POOrderExt.usrPrepaymentAmt>(cache, order, false);
        }

        //
        [Serializable]
        public class INItemClassFilter : IBqlTable
        {
            #region ItemClassID

            public abstract class itemClassID : IBqlField { }
            [PXInt]
            [PXUIField(DisplayName = "Class ID", Visibility = PXUIVisibility.SelectorVisible)]
            [PXDimensionSelector(INItemClass.Dimension, typeof(Search<INItemClass.itemClassID, Where<INItemClass.stkItem, Equal<False>, Or<Where<INItemClass.stkItem, Equal<True>, And<FeatureInstalled<FeaturesSet.distributionModule>>>>>>), typeof(INItemClass.itemClassCD), DescriptionField = typeof(INItemClass.descr), ValidComboRequired = true)]
            public virtual int? ItemClassID { get; set; }

            #endregion
        }

        #region State

        private bool _allowToSyncTreeCurrentWithPrimaryViewCurrent;
        private bool _forbidToSyncTreeCurrentWithPrimaryViewCurrent;

        private readonly Lazy<bool> _timestampSelected = new Lazy<bool>(() => { PXDatabase.SelectTimeStamp(); return true; });

        #endregion

        #region Selects

        public PXSelectReadonly<INItemClass> ItemClassFilter;
        public PXSelect<INItemClass, Where<INItemClass.itemClassID, Equal<Current<INItemClass.itemClassID>>>> TreeViewAndPrimaryViewSynchronizationHelper;

        public PXSelectReadonly<ItemClassTree.INItemClass> ItemClasses;
        protected virtual IEnumerable itemClasses([PXInt] int? itemClassID) => ItemClassTree.EnrollNodes(itemClassID);

        #endregion

        #region Actions

        public PXAction<INItemClass> GoToNodeSelectedInTree;
        [PXButton, PXUIField(MapEnableRights = PXCacheRights.Select)]
        protected virtual IEnumerable goToNodeSelectedInTree(PXAdapter adapter)
        {
            _forbidToSyncTreeCurrentWithPrimaryViewCurrent = true;
            ItemClassFilter.Current = PXSelect<INItemClass>.Search<INItemClass.itemClassID>(Base, ItemClasses.Current?.ItemClassID);
            yield return ItemClassFilter.Current;
        }

        #endregion

        #region Event Handlers

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Vendor Class Description")]
        protected virtual void VendorClass_Descr_CacheAttached(PXCache sender) { }

        /// <summary><see cref="INItemClass"/> Selected</summary>
        protected virtual void INItemClass_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            _timestampSelected.Init();
            SyncTreeCurrentWithPrimaryViewCurrent((INItemClass)e.Row);
        }

        #endregion

        public IEnumerable ExecuteSelect(String viewName, Object[] parameters, Object[] searches, String[] sortcolumns, Boolean[] descendings, PXFilterRow[] filters, ref Int32 startRow, Int32 maximumRows, ref Int32 totalRows)
        {
            if (viewName == nameof(TreeViewAndPrimaryViewSynchronizationHelper))
                _allowToSyncTreeCurrentWithPrimaryViewCurrent = true;
            return Base.ExecuteSelect(viewName, parameters, searches, sortcolumns, descendings, filters, ref startRow, maximumRows, ref totalRows);
        }

        private void SyncTreeCurrentWithPrimaryViewCurrent(INItemClass primaryViewCurrent)
        {
            if (_allowToSyncTreeCurrentWithPrimaryViewCurrent && !_forbidToSyncTreeCurrentWithPrimaryViewCurrent
                && primaryViewCurrent != null && (ItemClasses.Current == null || ItemClasses.Current.ItemClassID != primaryViewCurrent.ItemClassID))
            {
                ItemClassTree.INItemClass current = ItemClassTree.Instance.GetNodeByID(primaryViewCurrent.ItemClassID ?? 0);
                ItemClasses.Current = current;
                ItemClasses.Cache.ActiveRow = current;
            }
        }


        public PXFilter<POSiteStatusFilter> sitestatusfilter1;

        [PXFilterable]
        [PXCopyPasteHiddenView]
        public POSiteStatusLookup<POSiteStatusSelected, INSiteStatusFilter> sitestatus1;

        public PXAction<POOrder> addInvBySite1;


        [PXUIField(DisplayName = "Add Item1", MapEnableRights = PXCacheRights.Select, MapViewRights = PXCacheRights.Select)]
        [PXLookupButton]
        public virtual IEnumerable AddInvBySite1(PXAdapter adapter)
        {
            sitestatusfilter1.Cache.Clear();
            if (Base.Document.Current.Hold == true)
            {
                if (sitestatus1.AskExt() == WebDialogResult.OK)
                {
                    
                }
                sitestatusfilter1.Cache.Clear();
                sitestatus1.Cache.Clear();
            }
            return adapter.Get();
        }

    }
}
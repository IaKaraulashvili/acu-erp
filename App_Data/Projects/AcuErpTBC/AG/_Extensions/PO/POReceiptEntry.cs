using System;
using System.Collections;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.AP;
using System.Linq;
using PX.Objects.FA;
using AG.DAC;
using AG.Shared;
using PX.Api;
using CustomSYProvider.Services;
using AG.RS.DAC;
using AG.RS;

namespace PX.Objects.PO
{
    public class POReceiptEntry_Extension : PXGraphExtension<POReceiptEntry>
    {
        public PXSelectJoin<POReceipt,
            LeftJoin<RSReceivedWaybill, On<RSReceivedWaybill.waybillNbr, Equal<POReceiptExt.usrReceivedWaybillNbr>>>,
            Where<POReceipt.receiptType, Equal<Current<POReceipt.receiptType>>,
              And<POReceipt.receiptNbr, Equal<Current<POReceipt.receiptNbr>>>>> CurrentDocument;

        public PXSelect<AGFARelated,
                Where<AGFARelated.refNbr, Equal<Current<POReceipt.receiptNbr>>, And<AGFARelated.type, Equal<AGFARelatedTypes.pOReceipt>>>> FARelateds;

        #region Event Handlers

        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "RefNbr")]
        [PXDBDefault(typeof(POReceipt.receiptNbr))]
        protected void AGFARelated_RefNbr_CacheAttached(PXCache cache)
        {

        }

        protected virtual void POReceipt_VendorID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            POReceipt row = (POReceipt)e.Row;
            if (row == null) return;

            var rowExt = PXCache<POReceipt>.GetExtension<POReceiptExt>(row);
            rowExt.UsrContractID = null;
        }

        protected virtual void AGFARelated_AssetCD_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (AGFARelated)e.Row;
            if (row == null) return;

            PXResult<PX.Objects.FA.FixedAsset> asset = PXSelectJoin<FixedAsset,
                        LeftJoin<FADetails, On<FixedAsset.assetID, Equal<FADetails.assetID>>,
                        LeftJoin<FALocationHistory, On<FixedAsset.assetID, Equal<FALocationHistory.assetID>>>>,
                            Where<FixedAsset.assetCD, Equal<Required<FixedAsset.assetCD>>>>.Select(Base, row.AssetCD).FirstOrDefault();


            if (asset != null)
            {
                row.Description = asset.GetItem<FixedAsset>().Description;
                row.AssetClass = asset.GetItem<FixedAsset>().ClassID;
                row.Status = asset.GetItem<FixedAsset>().Status;
                row.UsefulLifeYears = asset.GetItem<FixedAsset>().UsefulLife;
                row.ReceiptDate = asset.GetItem<FADetails>().ReceiptDate;
                row.PlacedInServiceDate = asset.GetItem<FADetails>().DepreciateFromDate;
                row.OrigAcquisitionCost = asset.GetItem<FADetails>().AcquisitionCost;
                row.Department = asset.GetItem<FALocationHistory>().Department;
            }
        }

        protected virtual void POReceipt_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var r = (POReceipt)e.Row;
            if (r.Status == POReceiptStatus.Released)
            {
                FARelateds.Cache.AllowDelete = false;
                FARelateds.Cache.AllowUpdate = false;
            }
        }

        protected virtual void POReceipt_UsrEmployeeID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (POReceipt)e.Row;
            sender.SetDefaultExt<POReceiptExt.usrDepartment>(row);
        }



        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {

            using (var scope = new PXTransactionScope())
            {
                var current = Base.Document.Current;
                if (current != null)
                {
                    var ext = current.GetExtension<POReceiptExt>();
                    if (ext.UsrFARelated != null)
                    {
                        if (ext.UsrFARelated.Value && FARelateds.Select().Count == 0)
                            throw new PXException(AG.Common.Messages.FARelatedEmpty);
                        if (!ext.UsrFARelated.Value && FARelateds.Select().Count > 0)
                        {
                            foreach (AGFARelated item in FARelateds.Select())
                                FARelateds.Delete(item);
                        }
                    }
                    if (!current.Hold.GetValueOrDefault() && ext.UsrDepartment == null)
                    {
                        PXCache cache = Base.Caches[typeof(POReceiptExt)];
                        cache.RaiseExceptionHandling<POReceiptExt.usrDepartment>(current, ext.UsrDepartment,
                         new PXSetPropertyException(AG.Common.Messages.BugetOwnerDepartment, PXErrorLevel.Error));
                    }
                }

                string oldWaybillNbr = null;
                string newWaybillNbr = null;

                POReceipt poReceipt = PXSelectReadonly<
                    POReceipt,
                    Where<POReceipt.receiptType, Equal<Current<POReceipt.receiptType>>,
                        And<POReceipt.receiptNbr, Equal<Current<POReceipt.receiptNbr>>>>>.Select(Base).FirstOrDefault();

                if (poReceipt != null)
                {
                    oldWaybillNbr = poReceipt.GetExtension<POReceiptExt>().UsrReceivedWaybillNbr;
                }

                baseMethod();


                poReceipt = PXSelectReadonly<
                    POReceipt,
                    Where<POReceipt.receiptType, Equal<Current<POReceipt.receiptType>>,
                        And<POReceipt.receiptNbr, Equal<Current<POReceipt.receiptNbr>>>>>.Select(Base);

                newWaybillNbr = poReceipt.GetExtension<POReceiptExt>().UsrReceivedWaybillNbr;

                var receivedWaybillGraph = PXGraph.CreateInstance<ReceivedWaybillEntry>();

                if (oldWaybillNbr != newWaybillNbr)
                {
                    if (!string.IsNullOrEmpty(newWaybillNbr))
                    {
                        receivedWaybillGraph.Waybills.Current = PXSelect<RSReceivedWaybill, Where<RSReceivedWaybill.waybillNbr, Equal<Required<RSReceivedWaybill.waybillNbr>>>>.Select(Base, newWaybillNbr);
                        receivedWaybillGraph.Waybills.Current.POReceiptNbr = current.ReceiptNbr;
                        receivedWaybillGraph.Waybills.Update(receivedWaybillGraph.Waybills.Current);
                        receivedWaybillGraph.Save.Press();
                    }
                    if (!string.IsNullOrEmpty(oldWaybillNbr))
                    {
                        receivedWaybillGraph.Waybills.Current = PXSelect<RSReceivedWaybill, Where<RSReceivedWaybill.waybillNbr, Equal<Required<RSReceivedWaybill.waybillNbr>>>>.Select(Base, oldWaybillNbr);
                        receivedWaybillGraph.Waybills.Current.POReceiptNbr = null;
                        receivedWaybillGraph.Waybills.Update(receivedWaybillGraph.Waybills.Current);
                        receivedWaybillGraph.Save.Press();
                    }
                }

                scope.Complete();
            }
        }


        public delegate IEnumerable CreateAPDocumentDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable CreateAPDocument(PXAdapter adapter, CreateAPDocumentDelegate baseMethod)
        {
            if (Base.Document.Current != null &&
                Base.Document.Current.Released == true)
            {
                POReceipt doc = Base.Document.Current;
                //
                var ext = doc.GetExtension<POReceiptExt>();
                //
                if (doc.UnbilledLineTotal != Decimal.Zero)
                {
                    APRegister apDoc = PXSelectJoin<APRegister, InnerJoin<APTran, On<APTran.tranType, Equal<APRegister.docType>,
                                                            And<APTran.refNbr, Equal<APRegister.refNbr>>>>,
                                        Where<APTran.receiptNbr, Equal<Required<APTran.receiptNbr>>>, OrderBy<Desc<APRegister.refNbr>>>.SelectWindowed(Base, 0, 1, doc.ReceiptNbr);
                    bool firstDoc = (apDoc == null);
                    bool hasUnreased = firstDoc ? false : (apDoc.Released == false);
                    if (!hasUnreased)
                    {
                        apDoc = PXSelectJoin<APRegister, InnerJoin<APTran, On<APTran.tranType, Equal<APRegister.docType>,
                                                            And<APTran.refNbr, Equal<APRegister.refNbr>>>>,
                                        Where<APTran.receiptNbr, Equal<Required<APTran.receiptNbr>>,
                                            And<APRegister.released, Equal<False>>>,
                                            OrderBy<Desc<APRegister.refNbr>>>.SelectWindowed(Base, 0, 1, doc.ReceiptNbr);
                        hasUnreased = (apDoc != null);
                    }
                    if (hasUnreased)
                    {
                        throw new PXException(Messages.UnreasedAPDocumentExistsForPOReceipt);
                    }
                    APInvoiceEntry invoiceGraph = PXGraph.CreateInstance<APInvoiceEntry>();
                    DocumentList<APInvoice> created = new DocumentList<APInvoice>(invoiceGraph);
                    CurrencyInfo info = PXSelect<CurrencyInfo, Where<CurrencyInfo.curyInfoID, Equal<Required<CurrencyInfo.curyInfoID>>>>.Select(Base, doc.CuryInfoID);
                    invoiceGraph.InvoicePOReceipt(doc, info, created, false, true);
                    invoiceGraph.AttachPrepayment();

                    //
                    var apExt = invoiceGraph.GetExtension<APInvoiceEntry_Extension>();
                    var invoice = invoiceGraph.Document.Current.GetExtension<APRegisterExt>();
                    invoice.UsrEmployeeID = ext.UsrEmployeeID;
                    invoice.UsrDepartment = ext.UsrDepartment;

                    if (ext.UsrFARelated != null && ext.UsrFARelated.Value)
                    {
                        invoice.UsrFARelated = ext.UsrFARelated;

                        foreach (AGFARelated item in FARelateds.Select())
                        {
                            AGFARelated faRelated = new AGFARelated();
                            faRelated.AssetCD = item.AssetCD;
                            faRelated.RefNbr = invoiceGraph.Document.Current.RefNbr;
                            faRelated.ExpenseAmount = item.ExpenseAmount;
                            apExt.FARelateds.Cache.Insert(faRelated);
                        }
                    }
                    //

                    throw new PXRedirectRequiredException(invoiceGraph, Messages.CreateAPInvoice);
                }
                else
                {
                    throw new PXException(Messages.AllTheLinesOfPOReceiptAreAlreadyBilled);
                }

            }
            return adapter.Get();
        }

        #endregion

        #region Actions

        public PXAction<POReceipt> Export;
        [PXUIField(DisplayName = "Export", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable export(PXAdapter adapter)
        {
            var current = Base.Document.Current;
            string exportScenarioName = "Purchase Receipt";

            SYMapping map = PXSelect<SYMapping, Where<SYMapping.name, Equal<Required<SYMapping.name>>>>.Select(Base, exportScenarioName);

            PXLongOperation.StartOperation(Base, delegate ()
            {
                WordPDFSYExportProcessHelper.RunScenarion(
                    Base,
                    map.Name,
                    current.NoteID.ToString(),
                    current.ReceiptNbr,
                    current.ReceiptType
                    );
            });

            return adapter.Get();
        }
        #endregion
    }
}
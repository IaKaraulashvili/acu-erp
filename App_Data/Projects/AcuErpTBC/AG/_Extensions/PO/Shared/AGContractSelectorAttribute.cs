﻿using System;
using PX.Data;
using AG.Extensions.PO.DAC;

namespace AG.Extensions.PO.Shared
{
    public class AGContractSelectorAttribute : PXSelectorAttribute
    {
        public AGContractSelectorAttribute(Type vendorID)
            : base(typeof(Search<AGContract.contractID>),
                    typeof(AGContract.contractNbr),
                    typeof(AGContract.description))
        {
            SubstituteKey = typeof(AGContract.contractNbr);

            _Select = _Select.WhereAnd(BqlCommand.Compose(typeof(Where<,>), typeof(AGContract.vendorID), typeof(Equal<>), typeof(Current<>), vendorID));
        }
    }
}

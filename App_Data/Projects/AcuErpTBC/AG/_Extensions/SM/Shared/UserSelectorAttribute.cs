﻿using PX.Data;
using PX.SM;

namespace AG.Extensions.SM.Shared
{
    public class UserSelectorAttribute : PXSelectorAttribute
    {
        public UserSelectorAttribute() : base(typeof(Search<Users.pKID>))
        {
            SubstituteKey = typeof(Users.username);
            DescriptionField = typeof(Users.displayName);
            SelectorMode = PXSelectorMode.DisplayModeText;
        }
    }
}

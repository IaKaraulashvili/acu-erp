using System;
using System.Collections;
using PX.Data;
using AG.RS.DAC;
using PX.Objects.IN;
using System.Linq;
using PX.Objects.CR;
using PX.Objects.AR;
using PX.Objects.CS;
using PX.Objects.TX;
using AG.RS;
using AG.RS.Shared;

namespace PX.Objects.SO
{
    public class SOShipmentEntryExt : PXGraphExtension<SOShipmentEntry>
    {
        private bool _Post { get; set; }


        public override void Initialize()
        {
            Base.action.AddMenuAction(postTransWaybill);
            Base.action.AddMenuAction(postWithoutTransWaybill);
            Base.action.AddMenuAction(postReturnWaybill);
            Base.action.AddMenuAction(postCorrectedWaybill);
        }

        protected virtual void SOShipment_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (SOShipment)e.Row;
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            if (row != null)
            {
                var isEnabled = row.Status == SOShipmentStatus.Confirmed || row.Status == SOShipmentStatus.Completed;
                var source = PXSelect<RSWaybillSource, Where<RSWaybillSource.sourceNbr, Equal<Required<SOShipment.shipmentNbr>>>>.Select(graph, row.ShipmentNbr).FirstTableItems;

                if (source.Count() > 0)
                {
                    postWithoutTransWaybill.SetEnabled(false);
                    postTransWaybill.SetEnabled(false);
                    postReturnWaybill.SetEnabled(false);

                    var wb = PXSelect<RSWaybill, Where<RSWaybill.waybillNbr, Equal<Required<RSWaybill.waybillNbr>>>>.Select(graph, source.First().WaybillNbr).FirstTableItems;
                    if (wb != null && wb.Count() > 0 && wb.First().Status == WaybillStatus.Posted && wb.First().Status == SOShipmentStatus.Open)
                    {
                        postCorrectedWaybill.SetEnabled(true);
                    }
                }
                else
                {
                    postWithoutTransWaybill.SetEnabled(isEnabled);
                    postTransWaybill.SetEnabled(isEnabled);
                    postReturnWaybill.SetEnabled(isEnabled);
                    postCorrectedWaybill.SetEnabled(false);
                }
            }
        }

        #region Actions

        public PXAction<SOShipment> postTransWaybill;
        [PXUIField(DisplayName = "Post Trans. Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        public virtual void PostTransWaybill()
        {
            PXLongOperation.StartOperation(Base, delegate()
            {
                try
                {
                    CreateWaybill(WaybillType.Transporation);
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });
        }

        public PXAction<SOShipment> postWithoutTransWaybill;
        [PXUIField(DisplayName = "Post Without Trans. Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        protected virtual IEnumerable PostWithoutTransWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(Base, delegate()
            {
                try
                {
                    CreateWaybill(WaybillType.WithoutTransporation);
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                    SOShipmentEntry shipmentgraph = PXGraph.CreateInstance<SOShipmentEntry>();
                    shipmentgraph.Document.Current = shipmentgraph.Document.Current;
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<SOShipment> postReturnWaybill;
        [PXUIField(DisplayName = "Post Return Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        public virtual IEnumerable PostReturnWaybill(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(Base, delegate()
            {
                try
                {
                    CreateWaybill(WaybillType.Rtrn);
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }

        public PXAction<SOShipment> postCorrectedWaybill;
        [PXUIField(DisplayName = "Correct Waybill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton(CommitChanges = true)]
        public virtual IEnumerable PostCorrectedWaybill(PXAdapter adapter)
        {
            Base.Actions.PressSave();
            PXLongOperation.StartOperation(Base, delegate()
            {
                try
                {
                    WaybillCorrection(true);
                    PXProcessing<RSWaybill>.SetInfo(ActionsMessages.RecordProcessed);
                }
                catch (Exception ex)
                {
                    throw new PXException(ex.Message, ex);
                }
            });

            return adapter.Get();
        }


        #endregion

        #region Methodes

        void CreateWaybill(int wbType)
        {
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            var shipment = Base.Document.Current;
            var document = Base.CurrentDocument.Current;
            PXResultset<SOOrderShipment> orders = Base.OrderList.Select();
            PXResultset<SOShipLine> documentDetails = Base.Transactions.Select();
          
            RSWaybill waybill = graph.Waybills.Insert();

            waybill.WaybillType = wbType;

            FillWaybill(graph, waybill, shipment, document);

            foreach (SOShipLine item in documentDetails)
            {
                RSWaybillItem waybillItem = graph.WaybillItems.Insert();
                FillWaybillItem(graph, waybillItem, item, orders);
            }
            SaveOrPost(graph);
            if (_Post)
            {
                using (var tran = new PXTransactionScope())
                {
                    waybill.Status = WaybillStatus.Open;
                    waybill.Hold = false;
                    graph.Actions.PressSave();
                    graph.PostWB();
                    FillSourceWaybill(graph, shipment, waybill);
                    graph.Actions.PressSave();
                    tran.Complete(graph);
                    if (waybill.WaybillType == WaybillType.WithoutTransporation)
                    {
                        graph.CloseWB();
                    }
                }
            }
            else
            {
                graph.Actions.PressSave();
                FillSourceWaybill(graph, shipment, waybill);
            }
        }
        void FillSourceWaybill(WaybillEntry graph, SOShipment shipment, RSWaybill waybill)
        {
            RSWaybillSource sourceWaybill = graph.WaybillSource.Insert();
            sourceWaybill.SourceNbr = shipment.ShipmentNbr;
            sourceWaybill.SourceType = AG.RS.DAC.RSWaybillSource.SourceTypes.Shipment;
        }

        void WaybillCorrection(bool postCorrectedWb)
        {
            WaybillEntry graph = PXGraph.CreateInstance<WaybillEntry>();
            var shipment = Base.Document.Current;
            var document = Base.CurrentDocument.Current;
            PXResultset<SOOrderShipment> orders = Base.OrderList.Select();
            PXResultset<SOShipLine> documentDetails = Base.Transactions.Select();


            var source = PXSelect<RSWaybillSource, Where<RSWaybillSource.sourceNbr, Equal<Required<RSWaybillSource.sourceNbr>>, And<RSWaybillSource.sourceType, Equal<Required<RSWaybillSource.sourceType>>>>>.Select(graph, shipment.ShipmentNbr, RSWaybillSource.SourceTypes.Shipment).FirstTableItems;
            if (source.Count() > 0)
            {
                graph.Waybills.Current = PXSelect<RSWaybill, Where<RSWaybill.waybillNbr, Equal<Required<RSWaybill.waybillNbr>>>>.Select(graph, source.First().WaybillNbr);

                var wb = graph.Waybills.Current;
                graph.CorrectWB();
                SaveOrPost(graph);

                FillWaybill(graph, wb, shipment, document);
                if (_Post)
                {
                    wb.Status = WaybillStatus.Posted;
                }
                else
                {
                    wb.Status = WaybillStatus.Open;
                }
                var waybillItems = graph.WaybillItems.Select().FirstTableItems;
                foreach (SOShipLine item in documentDetails)
                {
                    RSWaybillItem waybillItem = waybillItems.FirstOrDefault(x => x.InventoryID == item.InventoryID);
                    FillWaybillItem(graph, waybillItem, item, orders);
                }
                graph.Actions.PressSave();
                graph.PostCorrectedWB();
            }
        }

        void FillWaybill(WaybillEntry graph, RSWaybill waybill, SOShipment shipment, SOShipment document)
        {
            SetBranchInfo(waybill, shipment, graph);
            SetCustomerInfo(waybill, shipment, graph);
            waybill.ShippingCost = document.CuryFreightAmt;
            SetRecipientIsForeignCitizen(waybill, shipment, graph);
            if (waybill.WaybillType != WaybillType.WithoutTransporation)
            {
                graph.Waybills.Cache.SetValueExt<RSWaybill.transStartDate>(waybill, shipment.ShipDate);
                graph.Waybills.Cache.SetValueExt<RSWaybill.transStartTime>(waybill, 12 * 60);
            }
            else
            {
                waybill.DestinationAddress = waybill.SourceAddress;
            }

        }
        void FillWaybillItem(WaybillEntry graph, RSWaybillItem waybillItem, SOShipLine shipmentLine, PXResultset<SOOrderShipment> orders)
        {
            waybillItem.InventoryID = shipmentLine.InventoryID;
            waybillItem.ItemName = shipmentLine.TranDesc;
            SetUnitOfMeasure(waybillItem, shipmentLine, graph);
            waybillItem.ItemQty = shipmentLine.ShippedQty;
            SetBarCode(waybillItem, graph, shipmentLine);
            foreach (SOOrderShipment orderShipment in orders.FirstTableItems.Where(x => x.OrderNbr == shipmentLine.OrigOrderNbr))
            {
                SetUnitPrice(waybillItem, graph, orderShipment.OrderNbr, shipmentLine.InventoryID);
            }
        }


        void SetBranchInfo(RSWaybill waybill, SOShipment shipment, WaybillEntry graph)
        {
            var warehouse = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, shipment.SiteID).FirstTableItems;
            if (warehouse.Count() > 0)
            {
                graph.Waybills.Cache.SetValueExt<RSWaybill.branchID>(waybill, warehouse.First().BranchID);
            }
        }

        void SetCustomerInfo(RSWaybill waybill, SOShipment shipment, WaybillEntry graph)
        {
            var customer = PXSelect<Customer, Where<Customer.bAccountID, Equal<Required<Customer.bAccountID>>>>.Select(graph, shipment.CustomerID).FirstTableItems;
            if (customer != null && customer.Count() > 0)
            {
                graph.Waybills.Cache.SetValueExt<RSWaybill.customerID>(waybill, customer.First().AcctCD);
            }
        }

        void SetRecipientIsForeignCitizen(RSWaybill waybill, SOShipment shipment, WaybillEntry graph)
        {
            var warehouse = PXSelect<INSite, Where<INSite.siteID, Equal<Required<INSite.siteID>>>>.Select(graph, shipment.SiteID).FirstTableItems;
            if (warehouse.Count() > 0)
            {
                var branch = PXSelect<PX.Objects.GL.Branch, Where<PX.Objects.GL.Branch.branchID, Equal<Required<PX.Objects.GL.Branch.branchID>>>>.Select(graph, warehouse.First().BranchID).FirstTableItems;
                var customer = PXSelect<Customer, Where<Customer.bAccountID, Equal<Required<Customer.bAccountID>>>>.Select(graph, shipment.CustomerID).FirstTableItems;
                if (customer != null && customer.Count() > 0)
                {
                    var contact = PXSelect<Address, Where<Address.bAccountID, Equal<Required<Address.bAccountID>>>>.Select(graph, customer.First().BAccountID).FirstTableItems;

                    if (branch.Count() > 0 && contact.Count() > 0)
                    {
                        waybill.RecipientIsForeignCitizen = branch.First().CountryID == contact.First().CountryID;
                    }
                }
            }
        }

        void SetUnitOfMeasure(RSWaybillItem waybillItem, SOShipLine sOShipLine, WaybillEntry graph)
        {
            var uOM = PXSelect<RSUOMMapper, Where<RSUOMMapper.unit, Equal<Required<RSUOMMapper.unit>>>>.Select(graph, sOShipLine.UOM).FirstTableItems;
            if (uOM != null && uOM.Count() > 0)
            {
                if (!string.IsNullOrEmpty(uOM.First().Unit))
                {
                    waybillItem.ExtUOM = uOM.First().RSUOMID;
                    waybillItem.OtherUOMDescription = Convert.ToString(uOM.First().RSUOMID);
                }
            }
        }

        void SetUnitPrice(RSWaybillItem waybillItem, WaybillEntry graph, string orderNbr, int? inventoryID)
        {
            var order = PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Required<SOOrder.orderNbr>>>>.Select(graph, orderNbr).FirstTableItems;
            if (order.Count() > 0)
            {
                var orderLine = PXSelect<SOLine, Where<SOLine.orderNbr, Equal<Required<SOLine.orderNbr>>>>.Select(graph, order.First().OrderNbr).FirstTableItems;
                if (orderLine.Count() > 0 && orderLine != null)
                {
                    foreach (SOLine item in orderLine)
                    {
                        if (item.InventoryID != null && item.InventoryID == inventoryID)
                        {
                            if (item.UnitPrice != null)
                            {
                                waybillItem.UnitPrice = orderLine.First().UnitPrice;
                            }
                            if (item.ExtPrice != null)
                            {
                                waybillItem.ItemAmt = orderLine.First().ExtPrice;
                            }
                        }
                    }
                }
            }
        }

        void SetBarCode(RSWaybillItem waybillItem, WaybillEntry graph, SOShipLine line)
        {
            var feature = PXSelect<FeaturesSet>.Select(graph).FirstTableItems;
            if (feature.Count() > 0 && feature.First().SubItem == true)
            {
                var order = PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Required<SOOrder.orderNbr>>, And<SOOrder.orderType, Equal<Required<SOOrder.orderType>>>>>.Select(graph, line.OrigOrderNbr, line.OrigOrderType).FirstTableItems;
                if (order != null && order.Count() > 0)
                {
                    var orderline = PXSelect<SOLine, Where<SOLine.orderNbr, Equal<Required<SOLine.orderNbr>>, And<SOLine.orderType,
                        Equal<Required<SOLine.orderType>>>>>.Select(graph, order.First().OrderNbr, order.First().OrderType).FirstTableItems;
                    if (orderline != null && orderline.Count() > 0 && !string.IsNullOrEmpty(orderline.First().AlternateID))
                    {
                        waybillItem.ItemCode = orderline.First().AlternateID;
                    }
                }
            }
            else
            {
                var iNItemXRef = PXSelect<INItemXRef, Where<INItemXRef.inventoryID, Equal<Required<INItemXRef.inventoryID>>>>.Select(graph, line.InventoryID).FirstTableItems;
                if (iNItemXRef != null && iNItemXRef.Count() > 0)
                {
                    foreach (var item in iNItemXRef)
                    {
                        if (item.AlternateType == "BAR")
                        {
                            waybillItem.ItemCode = item.AlternateID;
                        }
                    }
                }
                else
                {
                    var inventoryItem = PXSelect<InventoryItem, Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>.Select(graph, line.InventoryID).FirstTableItems;
                    if (inventoryItem != null && inventoryItem.Count() > 0)
                    {
                        if (!string.IsNullOrEmpty(inventoryItem.First().InventoryCD))
                        {
                            waybillItem.ItemCode = inventoryItem.First().InventoryCD;
                        }
                    }
                }
            }
        }
        void SetTaxType(RSWaybillItem waybillItem, WaybillEntry graph, string orderNbr)
        {
            var order = PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Required<SOOrder.orderNbr>>>>.Select(graph, orderNbr).FirstTableItems;
            if (order.Count() > 0)
            {
                var soTax = PXSelect<SOTax, Where<SOTax.orderType, Equal<Required<SOOrder.orderType>>, And<SOTax.orderNbr, Equal<Required<SOOrder.orderNbr>>>>>.Select(graph, order.First().OrderNbr).FirstTableItems;
                if (soTax.Count() > 0)
                {
                    var tax = PXSelect<Tax, Where<Tax.taxID, Equal<Required<Tax.taxID>>>>.Select(graph, soTax.First().TaxID).FirstTableItems;
                    if (tax.Count() > 0)
                    {
                        //waybillItem.TaxType = tax.First().UsrGEVatType;
                    }
                }
            }
        }

        void SaveOrPost(WaybillEntry graph)
        {
            var setup = PXSelect<RSSetup>.Select(graph).FirstTableItems;
            if (setup.Count() > 0)
            {
                _Post = (bool)setup.First().WaybillPostOnShipment;
            }
        }
        #endregion

    }
}
﻿using System;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.AP;
using PX.Objects.BQLConstants;
using PX.Objects.CS;
using System.Collections.Generic;
using PX.Objects;
using PX.Objects.TX;
using PX.Objects.CR;


namespace PX.Objects.TX
{
    public class TaxExt : PXCacheExtension<PX.Objects.TX.Tax>
    {



        #region UsrRSVatType




        [PXDBInt(IsKey = false, BqlTable = typeof(PX.Objects.TX.Tax))]
        [PXUIField(DisplayName = "RS VAT Type")]
        [PXDefault(0)]
        [PXIntList(
            new int[]{
                0,
                1,
                2
            },
            new string[]{
                   "Normal",
                   "Nullable",
                   "TaxFree"
            }
                )]

        public virtual int? UsrRSVatType { get; set; }
        public abstract class usrRSVatType : IBqlField { }

        #endregion




    }
}

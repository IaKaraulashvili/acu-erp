﻿﻿namespace AcuErpTBC.TB
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class TBAccountType : PX.Data.IBqlTable
	{
		#region AccountTypeID
		public abstract class accountTypeID : PX.Data.IBqlField
		{
		}
		protected int? _AccountTypeID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? AccountTypeID
		{
			get
			{
				return this._AccountTypeID;
			}
			set
			{
				this._AccountTypeID = value;
			}
		}
		#endregion
		#region AccountType
		public abstract class accountType : PX.Data.IBqlField
		{
		}
		protected string _AccountType;
		[PXDBString(15, IsUnicode = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Account Type")]
		public virtual string AccountType
		{
			get
			{
				return this._AccountType;
			}
			set
			{
				this._AccountType = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(60, IsUnicode = true)]
		[PXUIField(DisplayName = "Description")]
        [PXDefault()]
        public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
		#endregion
	}
}

﻿﻿namespace AcuErpTBC.TB
{
    using System;
    using PX.Data;
    using PX.Objects.CM;
    using PX.Objects.AR;

    [System.SerializableAttribute()]
	public class TBCustomersAccount : PX.Data.IBqlTable
	{
		#region CustomerAccountID
		public abstract class customerAccountID : PX.Data.IBqlField
		{
		}
		protected int? _CustomerAccountID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? CustomerAccountID
		{
			get
			{
				return this._CustomerAccountID;
			}
			set
			{
				this._CustomerAccountID = value;
			}
		}
		#endregion
		#region CustomerID
		public abstract class customerID : PX.Data.IBqlField
		{
		}
		protected int? _CustomerID;
		[PXDBInt()]
		[PXDefault(typeof(Customer.bAccountID))]
		[PXUIField(DisplayName = "CustomerID")]
        [PXParent(typeof(Select<Customer,
                                 Where<Customer.bAccountID, Equal<Current<TBCustomersAccount.customerID>>>>))]
        public virtual int? CustomerID
		{
			get
			{
				return this._CustomerID;
			}
			set
			{
				this._CustomerID = value;
			}
		}
		#endregion
		#region IBANAccount
		public abstract class iBANAccount : PX.Data.IBqlField
		{
		}
		protected string _IBANAccount;
		[PXDBString(50, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "IBAN Account")]
		public virtual string IBANAccount
		{
			get
			{
				return this._IBANAccount;
			}
			set
			{
				this._IBANAccount = value;
			}
		}
		#endregion
		#region AccountTypeID
		public abstract class accountTypeID : PX.Data.IBqlField
		{
		}
		protected int? _AccountTypeID;
		[PXDBInt()]
		[PXDefault()]
		[PXUIField(DisplayName = "Account Type")]
        [PXSelector(typeof(Search<TBAccountType.accountTypeID>),
                    //typeof(TBAccountType.accountTypeID),
                    typeof(TBAccountType.accountType), SubstituteKey = typeof(TBAccountType.accountType))]
        public virtual int? AccountTypeID
		{
			get
			{
				return this._AccountTypeID;
			}
			set
			{
				this._AccountTypeID = value;
			}
		}
		#endregion
		#region Currency
		public abstract class currency : PX.Data.IBqlField
		{
		}
		protected string _Currency;
		[PXDBString(5, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Currency")]
        [PXSelector(typeof(Currency.curyID), CacheGlobal = true, DescriptionField = typeof(Currency.curyID))]
        public virtual string Currency
		{
			get
			{
				return this._Currency;
			}
			set
			{
				this._Currency = value;
			}
		}
		#endregion
	}
}

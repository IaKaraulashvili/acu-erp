﻿namespace AcuErpTBC.TB
{
    using System;
    using PX.Data;
    using PX.Objects.IN;
    using PX.Objects.AP;

    [System.SerializableAttribute()]
    public class TBVendorItemClass : PX.Data.IBqlTable
    {
        #region VendorID
        public abstract class vendorID : PX.Data.IBqlField
        {
        }
        protected int? _VendorID;
        [PXDBInt(IsKey = true)]
        [PXDefault(typeof(Vendor.bAccountID))]
        [PXParent(typeof(Select<Vendor,
                                 Where<Vendor.bAccountID, Equal<Current<TBVendorItemClass.vendorID>>>>))]
        public virtual int? VendorID
        {
            get
            {
                return this._VendorID;
            }
            set
            {
                this._VendorID = value;
            }
        }
        #endregion
        #region ItemClassID
        public abstract class itemClassID : PX.Data.IBqlField
        {
        }
        protected int? _ItemClassID;
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Item Class ID", Visibility = PXUIVisibility.SelectorVisible)]
        [PXDimensionSelector(INItemClass.Dimension, typeof(Search<INItemClass.itemClassID>), typeof(INItemClass.itemClassCD), DescriptionField = typeof(INItemClass.descr), ValidComboRequired = true)]
        public virtual int? ItemClassID
        {
            get
            {
                return this._ItemClassID;
            }
            set
            {
                this._ItemClassID = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
    }
}

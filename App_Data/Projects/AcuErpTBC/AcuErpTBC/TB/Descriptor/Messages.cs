﻿using PX.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcuErpTBC.TB.Descriptor
{
    [PXLocalizable(Messages.Prefix)]
    public static class Messages
    {
        public const string Prefix = "TBC general messages";

        public const string ItemClassIsAlreadyAdded = "Item class is already added!";
        public const string AssetClassRange = "Value don't correspond to the range";
        public const string FARelatedEmpty = "Add FixedAsset record!";
        public const string POOrderBoardAgreementNumber = "Board Agreement Number cannot be empty!";
        public const string POOrderBoardValidationType = "Board Validation Type cannot be empty!";
    }
}

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.Common;
using PX.Objects.Common.Extensions;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CT;
using PX.Objects.PM;
using PX.Objects.TX;
using PX.Objects.IN;
using PX.Objects.CA;
using PX.Objects.BQLConstants;
using PX.Objects.EP;
using PX.Objects.PO;
using PX.Objects.SO;
using PX.Objects.DR;
using PX.Objects.AR;
using AP1099Hist = PX.Objects.AP.Overrides.APDocumentRelease.AP1099Hist;
using AP1099Yr = PX.Objects.AP.Overrides.APDocumentRelease.AP1099Yr;
using PX.Objects.GL.Reclassification.UI;
using Branch = PX.Objects.GL.Branch;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AP.BQL;
using PX.Objects;
using PX.Objects.AP;

namespace PX.Objects.AP
{
  public class APInvoiceEntry_Extension : PXGraphExtension<APInvoiceEntry>
  {
        #region Event Handlers

        protected virtual void APInvoice_VendorID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            APRegister register = e.Row as APRegister;
            if (register == null) return;

            var rowExt = PXCache<APRegister>.GetExtension<APRegisterExt>(register);
            Vendor vendor = PXSelect<Vendor, Where<Vendor.bAccountID, Equal<Required<Vendor.bAccountID>>>>.Select(Base, register.VendorID).FirstOrDefault();
            if (vendor != null)
            {
                var vendorExt = vendor.GetExtension<VendorExt>();
                rowExt.UsrVEInsider = vendorExt.UsrInsider;
            }
        }

        #endregion
    }
}
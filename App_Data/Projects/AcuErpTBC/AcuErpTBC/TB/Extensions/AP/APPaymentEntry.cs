using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CA;
using PX.Objects.Common;
using PX.Objects.Common.Extensions;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.CR;
using PX.Objects.AP.MigrationMode;
using PX.Objects.EP;
using PX.Objects;
using PX.Objects.AP;
using PX.Data.Reports;
using PX.Reports;
using PX.Reports.Data;
using PX.SM;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.CR.Standalone;

namespace PX.Objects.AP
{
    public class APPaymentEntry_Extension : PXGraphExtension<APPaymentEntry>
    {
        #region Initialize

        public override void Initialize()
        {
            base.Initialize();
            Base.report.AddMenuAction(PaymentOrderReport);
        }

        #endregion

        #region Actions

        public PXAction<APPayment> PaymentOrderReport;
        [PXUIField(DisplayName = "Payment Order", MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        protected virtual IEnumerable paymentOrderReport(PXAdapter adapter)
        {
            foreach (var doc in adapter.Get<APPayment>())
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters["DocType"] = doc.DocType;
                parameters["RefNbr"] = doc.RefNbr;

                throw new PXReportRequiredException(parameters, "TB6001AP", "Report");
            }
            return adapter.Get();
        }

        #endregion

        #region Event Handlers

        protected virtual void APPayment_VendorID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            APRegister register = e.Row as APRegister;
            if (register == null) return;

            var rowExt = PXCache<APRegister>.GetExtension<APRegisterExt>(register);
            Vendor vendor = PXSelect<Vendor, Where<Vendor.bAccountID, Equal<Required<Vendor.bAccountID>>>>.Select(Base, register.VendorID).FirstOrDefault();
            if (vendor != null)
            {
                var vendorExt = vendor.GetExtension<VendorExt>();
                rowExt.UsrVEInsider = vendorExt.UsrInsider;
                CRLocation location = PXSelect<CRLocation, Where<CRLocation.bAccountID, Equal<Required<Vendor.bAccountID>>>>.Select(Base, vendor.BAccountID).FirstOrDefault();
                var locationExt = location.GetExtension<LocationExt>();
                rowExt.UsrOpCode = locationExt.UsrOpCode;
            }
        }


        protected virtual void APPayment_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (APRegister)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<APRegisterExt.usrVEInsider>(sender, row, false);
        }

        #endregion

        #region Send report PDF

        public void SendPaymentOrderReportPDF()
        {
            using (var tran = new PXTransactionScope())
            {
                var doc = Base.Document.Current;
                var setup = Base.APSetup.Current;
                var setupExt = setup.GetExtension<APSetupExt>();
                var defaultMailAccountID = PX.Data.EP.MailAccountManager.DefaultMailAccountID;

                if (!defaultMailAccountID.HasValue)
                {
                    throw new PXException("Default mail account is not configured!");
                }

                if (string.IsNullOrEmpty(setupExt.UsrPaymentOrderReceiver))
                {
                    throw new PXException("Receiver is not configured!");
                }

                //Report Paramenters
                Dictionary<String, String> parameters = new Dictionary<String, String>();

                parameters["DocType"] = doc.DocType;
                parameters["RefNbr"] = doc.RefNbr;

                // Report processing
                PX.Reports.Controls.Report _report = PXReportTools.LoadReport("TB6001AP", null);
                PXReportTools.InitReportParameters(_report, parameters,
                        SettingsProvider.Instance.Default);
                ReportNode reportNode = ReportProcessor.ProcessReport(_report);

                // PDF generation
                var user = new PXSelect<Users,
                    Where<Users.pKID, Equal<Required<Users.pKID>>>>(Base)
                    .Select(doc.CreatedByID).FirstTableItems.FirstOrDefault();

                var fileName = $"Payment_Order_{doc.RefNbr}_{doc.AdjDate?.ToString("yyyy_MM_dd")}_{user.Username}.pdf";
                byte[] data = PX.Reports.Mail.Message.GenerateReport(reportNode, ReportProcessor.FilterPdf).First();
                FileInfo file = new FileInfo(fileName, null, data);

                // Saving report
                UploadFileMaintenance graph = new UploadFileMaintenance();
                graph.SaveFile(file);
                PXNoteAttribute.AttachFile(Base.Document.Cache, Base.Document.Current, file);

                if (!file.UID.HasValue)
                {
                    throw new PXException("Can not create file!");
                }

                //// Downloading the report
                //throw new PXRedirectToFileException(file, true);

                // Sending report
                bool sent = false;
                string sError = "Failed to send E-mail.";
                try
                {
                    var sender = new NotificationGenerator(Base);

                    sender.Subject = $"Payment Order: {doc.RefNbr} / {doc.AdjDate?.ToString("d")} / {user.Username}";

                    sender.MailAccountId = defaultMailAccountID;
                    sender.RefNoteID = this.Base.Document.Current.NoteID;
                    sender.Owner = this.Base.Document.Current.OwnerID;
                    sender.To = setupExt.UsrPaymentOrderReceiver;

                    sender.AddAttachmentLink(file.UID.Value);

                    sent |= sender.Send().Any();
                }
                catch (Exception ex)
                {
                    sent = false;
                    sError = ex.Message;
                }

                if (!sent)
                {
                    throw new PXException(sError);
                }

                // Mark payment order as sent
                var docExt = doc.GetExtension<APRegisterExt>();
                docExt.UsrPaymentOrderSent = true;
                Base.Document.Update(doc);
                Base.Save.Press();

                tran.Complete();
            }
        }

        #endregion
    }
}
using APQuickCheck = PX.Objects.AP.Standalone.APQuickCheck;
using CRLocation = PX.Objects.CR.Standalone.Location;
using IRegister = PX.Objects.CM.IRegister;
using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.Common.Abstractions;
using PX.Objects.Common.MigrationMode;
using PX.Objects.Common;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects;
using PX.TM;
using System.Collections.Generic;
using System;

namespace PX.Objects.AP
{
    public class APRegisterExt : PXCacheExtension<PX.Objects.AP.APRegister>
    {
        #region UsrVEInsider
        [PXDBBool]
        [PXUIField(DisplayName = "Insider")]
        public virtual bool? UsrVEInsider { get; set; }
        public abstract class usrVEInsider : IBqlField { }
        #endregion
        #region UsrPaymentOrderSent
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Payment Order Sent")]

        public virtual bool? UsrPaymentOrderSent { get; set; }
        public abstract class usrPaymentOrderSent : IBqlField { }
        #endregion
        #region UsrOpCode
        [PXDBString(50)]
        [PXUIField(DisplayName = "Op Code")]

        public virtual string UsrOpCode { get; set; }
        public abstract class usrOpCode : IBqlField { }
        #endregion
    }
}
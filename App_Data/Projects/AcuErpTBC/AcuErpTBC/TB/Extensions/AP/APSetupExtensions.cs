﻿using PX.Data;

namespace PX.Objects.AP
{
    public class APSetupExt : PXCacheExtension<PX.Objects.AP.APSetup>
    {
        #region UsrPaymentOrderReceiver
        [PXDBEmail()]
        [PXUIField(DisplayName = "Payment Order Receiver")]

        public virtual string UsrPaymentOrderReceiver { get; set; }
        public abstract class usrPaymentOrderReceiver : IBqlField { }
        #endregion

        #region UsrHoldVendorOnEntry
        [PXDBBool]
        [PXUIField(DisplayName = "Hold Vendor On Entry")]

        public virtual bool? UsrHoldVendorOnEntry { get; set; }
        public abstract class usrHoldVendorOnEntry : IBqlField { }
        #endregion
    }
}
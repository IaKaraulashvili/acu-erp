﻿using PX.Data;
using PX.Objects.AP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcuErpTBC.TB.Extensions.AP
{
    public class TBSendPaymentReportProcess : AG.AGGraph<TBSendPaymentReportProcess>
    {
        public PXSetup<APSetup> Setup;

        public PXProcessing<APPayment,
            Where2<Where<APPayment.status, Equal<APDocStatus.closed>>,
                And<Where<APRegisterExt.usrPaymentOrderSent, Equal<False>, 
                    Or<APRegisterExt.usrPaymentOrderSent, IsNull>>>>> Payments;

        public TBSendPaymentReportProcess()
        {
            var setup = Setup.Current;
            Payments.SetProcessDelegate(delegate (APPaymentEntry graph, APPayment row)
            {
                graph.Clear();
                graph.Document.Current = row;
                var ext = graph.GetExtension<APPaymentEntry_Extension>();

                ext.SendPaymentOrderReportPDF();
            });
        }
    }
}

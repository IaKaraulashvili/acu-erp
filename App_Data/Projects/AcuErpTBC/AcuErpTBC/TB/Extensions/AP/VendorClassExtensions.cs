using PX.Data.EP;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP.Standalone;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using System.Collections.Generic;
using System;

namespace PX.Objects.AP
{
    public class VendorClassExt : PXCacheExtension<PX.Objects.AP.VendorClass>
    {
        #region UsrNonResident
        [PXDBBool]
        [PXUIField(DisplayName = "Non Resident")]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual bool? UsrNonResident { get; set; }
        public abstract class usrNonResident : IBqlField { }
        #endregion
        #region UsrInsider
        [PXDBBool]
        [PXUIField(DisplayName = "Insider")]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual bool? UsrInsider { get; set; }
        public abstract class usrInsider : IBqlField { }
        #endregion
    }
}
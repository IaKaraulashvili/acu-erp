using PX.Data;
using AcuErpTBC.TB;
using PX.Objects.IN;
using System.Collections;
using PX.Objects.CR;
using PX.Objects.AR;

namespace PX.Objects.AP
{
    public class VendorMaint_Extension : PXGraphExtension<VendorMaint>
    {
        #region Views

        public PXSelect<TBVendorItemClass,
            Where<TBVendorItemClass.vendorID, Equal<Current<Vendor.bAccountID>>>> Industries;

        /// <summary>
        /// Explicitly instantiate the item class cache to 
        /// rename the <see cref="INItemClass.Descr"/> column in the 
        /// <see cref="INItemClass_Descr_CacheAttached(PXCache)"/> handler.
        /// </summary>
        [PXHidden]
        public PXSelect<INItemClass> DummyItemClasses;

        #endregion

        #region Event Handlers

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Item Class Description")]
        protected virtual void INItemClass_Descr_CacheAttached(PXCache sender) { }

        protected void Vendor_RowUpdated(PXCache cache, PXRowUpdatedEventArgs e)
        {
            var row = (VendorR)e.Row;

            if (row == null) return;

            if (!cache.ObjectsEqual<Vendor.vendorClassID>(e.OldRow, e.Row))
            {
                var vendorClass = Base.VendorClass.Current;
                bool? insider = null;

                if (vendorClass != null)
                {
                    insider = Base.VendorClass.Cache.GetExtension<VendorClassExt>(vendorClass).UsrInsider;
                }

                cache.GetExtension<VendorExt>(row).UsrInsider = insider.GetValueOrDefault();
            }
        }

        protected void VendorR_Status_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e, PXFieldDefaulting InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null)
                InvokeBaseHandler(cache, e);

            var row = (VendorR)e.Row;
            var setup = Base.APSetup.Current.GetExtension<APSetupExt>();

            if (setup.UsrHoldVendorOnEntry.GetValueOrDefault())
            {
                e.NewValue = Vendor.status.Hold;
                e.Cancel = true;
            }
        }

        #endregion

        #region Actions
        public delegate IEnumerable ExtendToCustomerDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable ExtendToCustomer(PXAdapter adapter, ExtendToCustomerDelegate baseMethod)
        {
            BAccount bacct = Base.BAccount.Current;
            Vendor vendor = Base.CurrentVendor.SelectSingle();
            if (bacct != null && (bacct.Type == BAccountType.VendorType || bacct.Type == BAccountType.CombinedType))
            {
                Base.Save.Press();
                AR.CustomerMaint editingBO = PXGraph.CreateInstance<PX.Objects.AR.CustomerMaint>();
                AR.Customer customer = (AR.Customer)editingBO.BAccount.Cache.Extend<BAccount>(bacct);
                editingBO.BAccount.Current = customer;
                customer.Type = BAccountType.CombinedType;
                customer.LocaleName = vendor?.LocaleName;
                var customerExt = customer.GetExtension<CustomerExt>();
                var vendorExt = vendor.GetExtension<VendorExt>();
                customerExt.UsrVATTaxPayer = vendorExt.UsrVATTaxPayer;
                customerExt.UsrLegalFormID = vendorExt.UsrLegalFormID;
                customerExt.UsrNonResident = vendorExt.UsrNonResident;
                customerExt.UsrInsider = vendorExt.UsrInsider;
                LocationExtAddress defLocation = editingBO.DefLocation.Select();
                editingBO.DefLocation.Cache.RaiseRowSelected(defLocation);
                string locationType = LocTypeList.CombinedLoc;
                if (defLocation.CTaxZoneID == null)
                    editingBO.DefLocation.Cache.SetDefaultExt<Location.cTaxZoneID>(defLocation);
                editingBO.InitCustomerLocation(defLocation, locationType, false);
                defLocation = editingBO.DefLocation.Update(defLocation);
                foreach (Location iLoc in editingBO.IntLocations.Select())
                {
                    if (iLoc.LocationID != defLocation.LocationID)
                    {
                        editingBO.InitCustomerLocation(iLoc, locationType, false);
                        editingBO.IntLocations.Update(iLoc);
                    }
                }
                throw new PXRedirectRequiredException(editingBO, "Edit Customer");
            }
            return adapter.Get();
        }
        #endregion
    }
}
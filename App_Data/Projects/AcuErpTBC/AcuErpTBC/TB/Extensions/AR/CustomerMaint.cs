using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Data;
using PX.SM;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects.AR.Repositories;
using PX.Objects.Common;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using PX.Objects.AR.CCPaymentProcessing.Helpers;
using CashAccountAttribute = PX.Objects.GL.CashAccountAttribute;
using PX.Objects;
using PX.Objects.AR;
using AcuErpTBC.TB;
using PX.Api;
using CustomSYProvider.Services;
using PX.Objects.AP;

namespace PX.Objects.AR
{
  
  public class CustomerMaint_Extension:PXGraphExtension<CustomerMaint>
  {

        public PXSelect<TBCustomersAccount,
            Where<TBCustomersAccount.customerID, Equal<Current<Customer.bAccountID>>>> CustomerAccounts;


        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {
            var defLocation = (LocationExtAddress)Base.DefLocation.Select();
            if (defLocation != null)
            {
                defLocation.TaxRegistrationID = Base.BAccount.Current.AcctCD.Trim();
            }
            baseMethod();
        }

        #region Event Handlers

        protected void Customer_Status_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e, PXFieldDefaulting InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null)
                InvokeBaseHandler(cache, e);

            var row = (Customer)e.Row;
            var setup = Base.ARSetup.Current.GetExtension<ARSetupExt>();

            if (setup.UsrHoldCustomerOnEntry.GetValueOrDefault())
            {
                e.NewValue = Customer.status.Hold;
                e.Cancel = true;
            }
        }

        #endregion

        #region Actions
        public PXAction<Customer> Export;
        [PXUIField(DisplayName = "Export", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable export(PXAdapter adapter)
        {
            var current = Base.CurrentCustomer.Current;
            string exportScenarioName = "Customers Export";

            SYMapping map = PXSelect<SYMapping, Where<SYMapping.name, Equal<Required<SYMapping.name>>>>.Select(Base, exportScenarioName);

            PXLongOperation.StartOperation(Base, delegate ()
            {
                WordPDFSYExportProcessHelper.RunScenarion(
                    Base,
                    map.Name,
                    current.NoteID.ToString(),
                    current.AcctCD
                    );
            });

            return adapter.Get();
        }

        public delegate IEnumerable ExtendToVendorDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable ExtendToVendor(PXAdapter adapter, ExtendToVendorDelegate baseMethod)
        {
            BAccount bacct = Base.BAccount.Current;
            Customer customer = Base.CurrentCustomer.SelectSingle();

            if (bacct != null && (bacct.Type == BAccountType.CustomerType))
            {
                Base.Save.Press();
                AP.VendorMaint editingBO = PXGraph.CreateInstance<AP.VendorMaint>();
                AP.VendorR vendor = (AP.VendorR)editingBO.BAccount.Cache.Extend<BAccount>(bacct);
                editingBO.BAccount.Current = vendor;
                vendor.Type = BAccountType.CombinedType;
                vendor.LocaleName = customer?.LocaleName;
                var vendorExt = vendor.GetExtension<VendorExt>();
                var customerExt = customer.GetExtension<CustomerExt>();
                vendorExt.UsrVATTaxPayer = customerExt.UsrVATTaxPayer;
                vendorExt.UsrLegalFormID = customerExt.UsrLegalFormID;
                vendorExt.UsrNonResident = customerExt.UsrNonResident;
                vendorExt.UsrInsider = customerExt.UsrInsider;
                LocationExtAddress defLocation = editingBO.DefLocation.Select();
                editingBO.DefLocation.Cache.RaiseRowSelected(defLocation);
                string locationType = LocTypeList.CombinedLoc;

                var defaultLocation = (LocationExtAddress)Base.DefLocation.Select();
                if (defLocation != null && defaultLocation.CTaxZoneID != null)
                    editingBO.DefLocation.Cache.SetValueExt<Location.vTaxZoneID>(defLocation, defaultLocation.CTaxZoneID);

                editingBO.InitVendorLocation(defLocation, locationType, false);
                defLocation = editingBO.DefLocation.Update(defLocation);
                foreach (Location iLoc in editingBO.IntLocations.Select())
                {
                    if (iLoc.LocationID != defLocation.LocationID)
                    {
                        editingBO.InitVendorLocation(iLoc, locationType, false);
                        editingBO.IntLocations.Update(iLoc);
                    }
                }
                throw new PXRedirectRequiredException(editingBO, CR.Messages.EditVendor);
            }
            return adapter.Get();
            //return baseMethod(adapter);
        }
        #endregion
    }
}
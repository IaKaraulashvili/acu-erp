using PX.Data;

namespace PX.Objects.AR
{
    public class ARSetupExt : PXCacheExtension<PX.Objects.AR.ARSetup>
    {
        #region UsrHoldCustomerOnEntry
        [PXDBBool]
        [PXUIField(DisplayName = "Hold Customer On Entry")]

        public virtual bool? UsrHoldCustomerOnEntry { get; set; }
        public abstract class usrHoldCustomerOnEntry : IBqlField { }
        #endregion
    }
}
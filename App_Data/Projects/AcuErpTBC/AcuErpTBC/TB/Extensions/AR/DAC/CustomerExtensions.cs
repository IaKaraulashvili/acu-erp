﻿using PX.Data.EP;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP.Standalone;
using PX.Objects.GL;
using PX.Objects.PR;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using AG.Extensions.CS.DAC;

namespace PX.Objects.AR
{
    public class CustomerExt : PXCacheExtension<PX.Objects.AR.Customer>
    {
        #region UsrVATTaxPayer
        [PXDBBool]
        [PXUIField(DisplayName = "VAT TaxPayer")]

        public virtual bool? UsrVATTaxPayer { get; set; }
        public abstract class usrVATTaxPayer : IBqlField { }
        #endregion

        #region UsrLegalFormID
        [PXDBString(15, IsUnicode = true)]
        [PXUIField(DisplayName = "Legal Form")]
        [PXSelector(typeof(Search<AGLegalForm.legalFormID,
                          Where<AGLegalForm.countryID, Equal<Current<Address.countryID>>,
                              Or<AGLegalForm.countryID, IsNull>>>),
                    typeof(AGLegalForm.legalFormID), typeof(AGLegalForm.description),
                    typeof(AGLegalForm.countryID), SubstituteKey = typeof(AGLegalForm.legalFormID), CacheGlobal = true)]
        public virtual string UsrLegalFormID { get; set; }
        public abstract class usrLegalFormID : IBqlField { }
        #endregion

        #region UsrNonResident
        [PXDBBool]
        [PXUIField(DisplayName = "Non Resident")]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual bool? UsrNonResident { get; set; }
        public abstract class usrNonResident : IBqlField { }
        #endregion

        #region UsrInsider
        [PXDBBool]
        [PXUIField(DisplayName = "Insider")]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual bool? UsrInsider { get; set; }
        public abstract class usrInsider : IBqlField { }
        #endregion
    }
}

﻿using PX.Data;
using PX.Objects.CR.Standalone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PX.Objects.CR
{
    public class LocationExtAddressExt : PXCacheExtension<PX.Objects.CR.LocationExtAddress>
    {
        #region UsrOpCode
        [PXDBString(50, BqlField = typeof(LocationExt.usrOpCode))]
        [PXUIField(DisplayName = "Op Code")]

        public virtual string UsrOpCode { get; set; }
        public abstract class usrOpCode : IBqlField { }
        #endregion
    }
}

using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CA;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PO;
using PX.Objects.SO;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.CR.Standalone
{
  public class LocationExt : PXCacheExtension<PX.Objects.CR.Standalone.Location>
  {
    #region UsrOpCode
    [PXDBString(50)]
    [PXUIField(DisplayName="Op Code")]

    public virtual string UsrOpCode { get; set; }
    public abstract class usrOpCode : IBqlField { }
    #endregion
  }
}
using PX.Data;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System;
using PX.Objects.CR;
using AG.Extensions.EP.Shared;

namespace PX.Objects.EP
{

    public class PXCacheExtension1<Table> : PXCacheExtension<Table> where Table : IBqlTable
    {
    }

    public class EPDepartmentExt : PXCacheExtension1<PX.Objects.EP.EPDepartment>
    {
        #region UsrHeadOfDepartmentID
        [EmployeeSelector]
        [PXDBInt()]
        [PXUIField(DisplayName = "Head of Department", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual Int32? UsrHeadOfDepartmentID { get; set; }
        public abstract class usrHeadOfDepartmentID : IBqlField { }
        #endregion

        #region UsrDirectoryOfDepartmentID
        [EmployeeSelector]
        [PXDBInt()]
        [PXUIField(DisplayName = "Directory of Department", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual Int32? UsrDirectoryOfDepartmentID { get; set; }
        public abstract class usrDirectoryOfDepartmentID : IBqlField { }
        #endregion
    }
}
using System;
using System.Linq;
using PX.Data;
using System.Collections.Generic;
using PX.Common;
using PX.Objects.GL;
using PX.Objects;
using PX.Objects.FA;

namespace PX.Objects.FA
{
    public class AssetClassMaint_Extension : PXGraphExtension<AssetClassMaint>
    {
        #region Event Handlers


        protected virtual void FixedAsset_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (FixedAsset)e.Row;
            if (row != null)
            {
                var ext = row.GetExtension<FixedAssetExt>();
                if (Base.AssetClass.Current.Depreciable == false)
                {
                    PXUIFieldAttribute.SetVisible<FixedAssetExt.usrMaxUsefulLife>(sender, null, false);
                    PXUIFieldAttribute.SetVisible<FixedAssetExt.usrMinUsefulLife>(sender, null, false);
                }
                else
                {
                    PXUIFieldAttribute.SetVisible<FixedAssetExt.usrMaxUsefulLife>(sender, null, true);
                    PXUIFieldAttribute.SetVisible<FixedAssetExt.usrMinUsefulLife>(sender, null, true);
                }
            }
        }

        protected void FixedAsset_UsrMaxUsefulLife_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (FixedAsset)e.Row;
            if (row != null)
            {
                var ext = row.GetExtension<FixedAssetExt>();
                Base.AssetClass.Current.UsefulLife = ext.UsrMaxUsefulLife;
            }
        }

        #endregion
    }
}
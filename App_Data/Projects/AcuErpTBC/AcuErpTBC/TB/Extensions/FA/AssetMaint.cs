using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PX.Common;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CM;
using PX.Objects.GL;
using PX.Objects.AP;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.FA.Overrides.AssetProcess;
using PX.Objects;
using PX.Objects.FA;

namespace PX.Objects.FA
{
    public class AssetMaint_Extension : PXGraphExtension<AssetMaint>
    {
        #region Event Handlers
     
        protected virtual void FixedAsset_RowUpdated(PXCache cache, PXRowUpdatedEventArgs e)
        {
            var row = (FixedAsset)e.Row;

            if (row == null) return;

            if (!cache.ObjectsEqual<FixedAsset.usefulLife>(e.OldRow, e.Row))
            {
                var asset = Base.CurrentAsset.Current;

                if (asset != null)
                {
                    FixedAsset fAsset = PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.assetID>>>>.Select(Base, asset.ClassID);
                    var assetExt = fAsset.GetExtension<FixedAssetExt>();
                    if (assetExt.UsrMinUsefulLife!=null && assetExt.UsrMaxUsefulLife!=null && !(asset.UsefulLife >= assetExt.UsrMinUsefulLife && asset.UsefulLife <= assetExt.UsrMaxUsefulLife))
                    {
                        PXCache c = Base.Caches[typeof(FixedAsset)];
                        cache.RaiseExceptionHandling<FixedAsset.usefulLife>(row, asset.UsefulLife,
                         new PXSetPropertyException(AcuErpTBC.TB.Descriptor.Messages.AssetClassRange, PXErrorLevel.Warning));
                    }
                }
            }
        }

        #endregion
    }
}
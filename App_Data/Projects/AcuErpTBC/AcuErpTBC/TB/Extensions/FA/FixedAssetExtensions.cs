using PX.Data.EP;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.FA
{
  public class FixedAssetExt : PXCacheExtension<PX.Objects.FA.FixedAsset>
  {
    #region UsrMinUsefulLife
    [PXDBDecimal(4)]
    [PXUIField(DisplayName="Min Useful Life, Years")]

    public virtual Decimal? UsrMinUsefulLife { get; set; }
    public abstract class usrMinUsefulLife : IBqlField { }
    #endregion

    #region UsrMaxUsefulLife
    [PXDBDecimal(4)]
    [PXUIField(DisplayName="Max Useful Life, Years")]

    public virtual Decimal? UsrMaxUsefulLife { get; set; }
    public abstract class usrMaxUsefulLife : IBqlField { }
    #endregion
  }
}
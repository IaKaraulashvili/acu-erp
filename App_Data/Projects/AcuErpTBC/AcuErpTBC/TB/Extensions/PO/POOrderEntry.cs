using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PX.Common;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.TX;
using PX.Objects.IN;
using PX.Objects.EP;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.SO;
using PX.TM;
using SOOrder = PX.Objects.SO.SOOrder;
using SOLine = PX.Objects.SO.SOLine;
using PX.Objects.PM;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AP.MigrationMode;
using PX.Objects.Common;
using PX.Objects;
using PX.Objects.PO;

namespace PX.Objects.PO
{
  public class POOrderEntry_Extension : PXGraphExtension<POOrderEntry>
  {
        #region Event Handlers
        protected virtual void POOrder_VendorID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            POOrder order = e.Row as POOrder;
            if (order == null) return;

            var rowExt = PXCache<POOrder>.GetExtension<POOrderExt>(order);
            Vendor vendor = PXSelect<Vendor, Where<Vendor.bAccountID, Equal<Required<Vendor.bAccountID>>>>.Select(Base, order.VendorID).FirstOrDefault();
            if (vendor != null)
            {
                var vendorExt = vendor.GetExtension<VendorExt>();
                rowExt.UsrVEInsider = vendorExt.UsrInsider;
            }
        }


        protected virtual void POOrder_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            var row = (POOrder)e.Row;
            if (row == null) return;
            PXUIFieldAttribute.SetEnabled<POOrderExt.usrVEInsider>(sender, row, false);
            SetBoardAgrNbrVisible(sender, row);
        }

        protected virtual void POOrder_UsrBoardValidationType_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (POOrder)e.Row;
            if (row == null) return;
            SetBoardAgrNbrVisible(sender, row);
        }

        #endregion

        public delegate void PersistDelegate();
        [PXOverride]
        public void Persist(PersistDelegate baseMethod)
        {

            var current = Base.CurrentDocument.Current;
            if (current == null) return;
            var rowExt = PXCache<POOrder>.GetExtension<POOrderExt>(current);
            if (!current.Hold.GetValueOrDefault() && rowExt.UsrBoardValidationType == null)
            {
                PXCache cache = Base.Caches[typeof(POOrderExt)];
                cache.RaiseExceptionHandling<POOrderExt.usrBoardValidationType>(current, rowExt.UsrBoardAgrNbr,
                 new PXSetPropertyException(AcuErpTBC.TB.Descriptor.Messages.POOrderBoardValidationType, PXErrorLevel.Error));
            }

            switch (rowExt.UsrBoardValidationType)
            {
                case ValidationTypes.NeedAdvisoryBoardValidation:
                case ValidationTypes.NeedExecutiveBoardValidation:
                    if (rowExt.UsrBoardAgrNbr == null)
                    {
                        PXCache cache = Base.Caches[typeof(POOrderExt)];
                        cache.RaiseExceptionHandling<POOrderExt.usrBoardAgrNbr>(current, rowExt.UsrBoardAgrNbr,
                         new PXSetPropertyException(AcuErpTBC.TB.Descriptor.Messages.POOrderBoardAgreementNumber, PXErrorLevel.Error));
                    }
                    break;
                default:
                    break;
            }

            baseMethod();
        }

        private void SetBoardAgrNbrVisible(PXCache cache, POOrder order)
        {
            var rowExt = PXCache<POOrder>.GetExtension<POOrderExt>(order);
            if (rowExt.UsrBoardValidationType == ValidationTypes.DoNotNeedBoardValidation || rowExt.UsrBoardValidationType == null)
                PXUIFieldAttribute.SetVisible<POOrderExt.usrBoardAgrNbr>(cache, order, false);
            else
                PXUIFieldAttribute.SetVisible<POOrderExt.usrBoardAgrNbr>(cache, order, true);
        }
    }
}
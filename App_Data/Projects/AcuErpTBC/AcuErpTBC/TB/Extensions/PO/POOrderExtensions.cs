using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.PO;
using PX.Objects;
using PX.SM;
using PX.TM;
using System.Collections.Generic;
using System;

namespace PX.Objects.PO
{
    public class POOrderExt : PXCacheExtension<PX.Objects.PO.POOrder>
    {
        #region UsrBoardValidationType
        [PXDBString(2, IsFixed = true)]
        [PXUIField(DisplayName = "Board Validation Type", Enabled = false)]
        [PXStringList(
            new string[] 
            { ValidationTypes.DoNotNeedBoardValidation,
                ValidationTypes.NeedAdvisoryBoardValidation,
                ValidationTypes.NeedExecutiveBoardValidation },
            new string[]
            { ValidationTypes.UI.DoNotNeedBoardValidation,
                ValidationTypes.UI.NeedAdvisoryBoardValidation,
                ValidationTypes.UI.NeedExecutiveBoardValidation})]
        public virtual string UsrBoardValidationType { get; set; }
        public abstract class usrBoardValidationType : IBqlField { }
        #endregion

        #region UsrBoardAgrNbr
        [PXDBString(50)]
        [PXUIField(DisplayName = "Board Agreement Number")]

        public virtual string UsrBoardAgrNbr { get; set; }
        public abstract class usrBoardAgrNbr : IBqlField { }
        #endregion

        #region UsrVEInsider
        [PXDBBool]
        [PXUIField(DisplayName = "Insider", Enabled = false)]
        public virtual bool? UsrVEInsider { get; set; }
        public abstract class usrVEInsider : IBqlField { }
        #endregion
    }

    public static class ValidationTypes
    {
        public const string DoNotNeedBoardValidation = "DN";
        public const string NeedAdvisoryBoardValidation = "NA";
        public const string NeedExecutiveBoardValidation = "NE";

        public class UI
        {
            public const string DoNotNeedBoardValidation = "Do Not Need Board Validation";
            public const string NeedAdvisoryBoardValidation = "Need Supervisory Board Validation";
            public const string NeedExecutiveBoardValidation = "Need Directors Board Validation";
        }
    }
}
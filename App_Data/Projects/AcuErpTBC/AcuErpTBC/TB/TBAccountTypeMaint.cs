using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace AcuErpTBC.TB
{
    public class TBAccountTypeMaint : PXGraph<TBAccountTypeMaint>
    {
        public PXSave<TBAccountType> Save;
        public PXCancel<TBAccountType> Cancel;

        public PXSelect<TBAccountType> AccountTypes;
    }
}
using System;
using System.Collections;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.CS;
using PX.Web.UI;
using PX.Objects.IN;
using PX.Objects.AP;
using PX.Objects.CR;

namespace AcuErpTBC.TB
{
    public class TBVendorByItemClassInq : PXGraph<TBVendorByItemClassInq>
    {
        public static class ShowItemsMode
        {
            public class ListAttribute : PXStringListAttribute
            {
                public ListAttribute()
                    : base(
                    new[] { ChildrenOfCurrentClass, AllChildren },
                    new[] { "Related to Only the Current Item Class", "Related to the Current and Child Item Classes" })
                { }
            }
            public const string ChildrenOfCurrentClass = "C";
            public const string AllChildren = "A";
        }

        #region DAC

        [Serializable]
        public class VendorByClassFilter : IBqlTable
        {
            #region ShowItems
            [PXString(1, IsFixed = true)]
            [PXUIField(DisplayName = "Show Items")]
            [ShowItemsMode.List]
            [PXDefault(ShowItemsMode.ChildrenOfCurrentClass)]
            public virtual String ShowItems { get; set; }
            public abstract class showItems : IBqlField { }

            #endregion
        }

        [Serializable]
        public class INItemClassFilter : IBqlTable
        {
            #region ItemClassID

            public abstract class itemClassID : IBqlField { }
            [PXInt]
            [PXUIField(DisplayName = "Class ID", Visibility = PXUIVisibility.SelectorVisible)]
            [PXDimensionSelector(INItemClass.Dimension, typeof(Search<INItemClass.itemClassID, Where<INItemClass.stkItem, Equal<False>, Or<Where<INItemClass.stkItem, Equal<True>, And<FeatureInstalled<FeaturesSet.distributionModule>>>>>>), typeof(INItemClass.itemClassCD), DescriptionField = typeof(INItemClass.descr), ValidComboRequired = true)]
            public virtual int? ItemClassID { get; set; }

            #endregion
        }

        #endregion

        #region State

        private bool _allowToSyncTreeCurrentWithPrimaryViewCurrent;
        private bool _forbidToSyncTreeCurrentWithPrimaryViewCurrent;

        private readonly Lazy<bool> _timestampSelected = new Lazy<bool>(() => { PXDatabase.SelectTimeStamp(); return true; });

        #endregion

        #region Selects

        public PXSelectReadonly<INItemClass> ItemClassFilter;
        public PXSelect<INItemClass, Where<INItemClass.itemClassID, Equal<Current<INItemClass.itemClassID>>>> TreeViewAndPrimaryViewSynchronizationHelper;
        public PXFilter<VendorByClassFilter> VendorFilter;

        public PXSelectReadonly<ItemClassTree.INItemClass> ItemClasses;
        protected virtual IEnumerable itemClasses([PXInt] int? itemClassID) => ItemClassTree.EnrollNodes(itemClassID);

        public PXSelectJoin<Vendor,
            InnerJoin<TBVendorItemClass, On<Vendor.bAccountID, Equal<TBVendorItemClass.vendorID>>,
            InnerJoin<INItemClass, On<TBVendorItemClass.itemClassID, Equal<INItemClass.itemClassID>>,
            LeftJoin<Address, On<Address.bAccountID, Equal<Vendor.bAccountID>, And<Address.addressID, Equal<Vendor.defAddressID>>>,
            LeftJoin<Contact, On<Contact.bAccountID, Equal<Vendor.bAccountID>, And<Contact.contactID, Equal<Vendor.defContactID>>>>>>>> Vendors;
        protected virtual IEnumerable vendors() => GetRelevantVendors(VendorFilter.Current.ShowItems);

        /// <summary>
        /// Explicitly instantiate the vendor class cache to 
        /// rename the <see cref="VendorClass.Descr"/> column in the 
        /// <see cref="VendorClass_Descr_CacheAttached(PXCache)"/> handler.
        /// </summary>
        [PXHidden]
        public PXSelect<VendorClass> DummyVendorClasses;

        #endregion

        #region Actions

        public PXFirst<INItemClass> First;
        public PXPrevious<INItemClass> Previous;
        public PXNext<INItemClass> Next;
        public PXLast<INItemClass> Last;
        public PXSave<INItemClass> Save;
        public PXCancel<INItemClass> Cancel;

        public PXAction<INItemClass> GoToNodeSelectedInTree;
        [PXButton, PXUIField(MapEnableRights = PXCacheRights.Select)]
        protected virtual IEnumerable goToNodeSelectedInTree(PXAdapter adapter)
        {
            _forbidToSyncTreeCurrentWithPrimaryViewCurrent = true;
            ItemClassFilter.Current = PXSelect<INItemClass>.Search<INItemClass.itemClassID>(this, ItemClasses.Current?.ItemClassID);
            yield return ItemClassFilter.Current;
        }

        #endregion

        #region Event Handlers

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Vendor Class Description")]
        protected virtual void VendorClass_Descr_CacheAttached(PXCache sender) { }

        /// <summary><see cref="INItemClass"/> Selected</summary>
        protected virtual void INItemClass_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            _timestampSelected.Init();
            SyncTreeCurrentWithPrimaryViewCurrent((INItemClass)e.Row);
        }

        #endregion

        public override IEnumerable ExecuteSelect(String viewName, Object[] parameters, Object[] searches, String[] sortcolumns, Boolean[] descendings, PXFilterRow[] filters, ref Int32 startRow, Int32 maximumRows, ref Int32 totalRows)
        {
            if (viewName == nameof(TreeViewAndPrimaryViewSynchronizationHelper))
                _allowToSyncTreeCurrentWithPrimaryViewCurrent = true;
            return base.ExecuteSelect(viewName, parameters, searches, sortcolumns, descendings, filters, ref startRow, maximumRows, ref totalRows);
        }

        private void SyncTreeCurrentWithPrimaryViewCurrent(INItemClass primaryViewCurrent)
        {
            if (_allowToSyncTreeCurrentWithPrimaryViewCurrent && !_forbidToSyncTreeCurrentWithPrimaryViewCurrent
                && primaryViewCurrent != null && (ItemClasses.Current == null || ItemClasses.Current.ItemClassID != primaryViewCurrent.ItemClassID))
            {
                ItemClassTree.INItemClass current = ItemClassTree.Instance.GetNodeByID(primaryViewCurrent.ItemClassID ?? 0);
                ItemClasses.Current = current;
                ItemClasses.Cache.ActiveRow = current;
            }
        }

        private IEnumerable GetRelevantVendors(string showItemsMode)
        {
            if (ItemClasses.Current == null) yield break;

            var selectCommand = new PXSelectJoin<Vendor,
                InnerJoin<TBVendorItemClass, On<Vendor.bAccountID, Equal<TBVendorItemClass.vendorID>>,
                InnerJoin<INItemClass, On<TBVendorItemClass.itemClassID, Equal<INItemClass.itemClassID>>,
                LeftJoin<Address, On<Address.bAccountID, Equal<Vendor.bAccountID>, And<Address.addressID, Equal<Vendor.defAddressID>>>,
                LeftJoin<Contact, On<Contact.bAccountID, Equal<Vendor.bAccountID>, And<Contact.contactID, Equal<Vendor.defContactID>>>>>>>,
                Where<Match<Current<AccessInfo.userName>>>>(this);

            if (showItemsMode == ShowItemsMode.ChildrenOfCurrentClass)
            {
                selectCommand.WhereAnd<Where<INItemClass.itemClassID, Equal<Required<INItemClass.itemClassID>>>>();
                foreach (var res in selectCommand.Select(ItemClasses.Current.ItemClassID))
                {
                    yield return res;
                }
            }
            else if (showItemsMode == ShowItemsMode.AllChildren)
            {
                selectCommand.WhereAnd<Where<INItemClass.itemClassCD, Like<Required<INItemClass.itemClassCD>>>>();
                foreach (var res in selectCommand.Select(ItemClasses.Current.ItemClassCDWildcard))
                {
                    yield return res;
                }
            }
            else throw new PXInvalidOperationException();
        }
    }
}
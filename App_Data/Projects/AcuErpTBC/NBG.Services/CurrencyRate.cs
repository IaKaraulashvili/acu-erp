﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBG.Services
{
    public class CurrencyRate
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
    }
}

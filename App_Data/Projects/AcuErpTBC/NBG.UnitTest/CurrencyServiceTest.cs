﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NBG.Services;

namespace NBG.UnitTest
{
    [TestClass]
    public class CurrencyServiceTest
    {
        [TestMethod]
        public void get_currency()
        {
            using (var service = new CurrencyService("http://www.nbg.ge/rss.php"))
            {
                var rate = service.GetRates(DateTime.Now);

                Assert.IsNotNull(rate);
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;

namespace RA
{
    public class CapitalAmountMaint : RAGraph<CapitalAmountMaint>
    {
        public PXCancel<RACapitalAmount> Cancel;
        public PXSave<RACapitalAmount> Save;

        public PXSelect<RACapitalAmount> RACapitalAmounts;
    }
}
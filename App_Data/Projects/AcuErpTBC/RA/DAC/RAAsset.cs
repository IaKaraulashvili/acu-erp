﻿namespace RA
{
    using System;
    using PX.Data;
    using PX.Objects.CS;
    using _Shared;
    using PX.Objects.CR.Standalone;
    using PX.TM;
    using PX.Objects.GL;
    using PX.Objects.CM;
    using PX.Objects.CR;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(RAAssetEntry))]
    [PXEMailSource]
    public class RAAsset : PX.Data.IBqlTable
    {
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region AssetCD
        public abstract class assetCD : PX.Data.IBqlField
        {
        }
        protected string _AssetCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [RAAssetSelectorAttribute]
        [AutoNumber(typeof(RASetup.repossessedAssetsNumberingID), typeof(RAAsset.createdDateTime))]
        [PXUIField(DisplayName = "Repossessed Asset ID", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string AssetCD
        {
            get
            {
                return this._AssetCD;
            }
            set
            {
                this._AssetCD = value;
            }
        }
        #endregion
        #region ClassID
        public abstract class classID : PX.Data.IBqlField
        {
        }
        protected int? _ClassID;
        [PXDBInt()]
        [PXUIField(DisplayName = "RA Class")]
        [PXSelector(typeof(Search<RAClass.classID>),
            typeof(RAClass.classCD),
            typeof(RAClass.externalID),
            typeof(RAClass.assetType),
            typeof(RAClass.description),
            DescriptionField = typeof(RAClass.description), SubstituteKey = typeof(RAClass.classCD))]
        public virtual int? ClassID
        {
            get
            {
                return this._ClassID;
            }
            set
            {
                this._ClassID = value;
            }
        }
        #endregion
        #region AssetType
        public abstract class assetType : PX.Data.IBqlField
        {
        }
        protected string _AssetType;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Asset Type")]
        [AssetTypeListAttribute]
        public virtual string AssetType
        {
            get
            {
                return this._AssetType;
            }
            set
            {
                this._AssetType = value;
            }
        }
        #endregion
        #region RealEstate
        public abstract class realEstate : PX.Data.IBqlField
        {
        }
        protected bool? _RealEstate;
        [PXDBBool()]
        [PXUIField(DisplayName = "Real Estate", Enabled = false)]
        public virtual bool? RealEstate
        {
            get
            {
                return this._RealEstate;
            }
            set
            {
                this._RealEstate = value;
            }
        }
        #endregion
        #region Status
        public abstract class status : PX.Data.IBqlField
        {
        }
        protected string _Status;
        [PXDefault(AssetStatus.Draft)]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Status")]
        [AssetStatusListAttribute]
        public virtual string Status
        {
            get
            {
                return this._Status;
            }
            set
            {
                this._Status = value;
            }
        }
        #endregion
        #region Reason
        public abstract class reason : PX.Data.IBqlField
        {
        }
        protected string _Reason;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Reason")]
        [AssetReasonListAttribute]
        public virtual string Reason
        {
            get
            {
                return this._Reason;
            }
            set
            {
                this._Reason = value;
            }
        }
        #endregion
        #region RepossessionType
        public abstract class repossessionType : PX.Data.IBqlField
        {
        }
        protected string _RepossessionType;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Repossession Type")]
        [RepossessionTypeListSttribute]
        public virtual string RepossessionType
        {
            get
            {
                return this._RepossessionType;
            }
            set
            {
                this._RepossessionType = value;
            }
        }
        #endregion
        #region ExecutionType
        public abstract class executionType : PX.Data.IBqlField
        {
        }
        protected string _ExecutionType;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Execution Type")]
        [ExecutionTypeListAttribute]
        public virtual string ExecutionType
        {
            get
            {
                return this._ExecutionType;
            }
            set
            {
                this._ExecutionType = value;
            }
        }
        #endregion
        #region Description
        public abstract class description : PX.Data.IBqlField
        {
        }
        protected string _Description;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Description")]
        public virtual string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                this._Description = value;
            }
        }
        #endregion
        #region PropertyID
        public abstract class propertyID : PX.Data.IBqlField
        {
        }
        protected string _PropertyID;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Property ID(LMS ID)")]
        public virtual string PropertyID
        {
            get
            {
                return this._PropertyID;
            }
            set
            {
                this._PropertyID = value;
            }
        }
        #endregion
        #region PropertyRepossessionDate
        public abstract class propertyRepossessionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _PropertyRepossessionDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Property Repossession Date")]
        public virtual DateTime? PropertyRepossessionDate
        {
            get
            {
                return this._PropertyRepossessionDate;
            }
            set
            {
                this._PropertyRepossessionDate = value;
            }
        }
        #endregion
        #region InspectionForm
        public abstract class inspectionForm : PX.Data.IBqlField
        {
        }
        protected string _InspectionForm;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Inspection Form")]
        [InspectionFormListAttribute]
        public virtual string InspectionForm
        {
            get
            {
                return this._InspectionForm;
            }
            set
            {
                this._InspectionForm = value;
            }
        }
        #endregion
        #region AssetClassification
        public abstract class assetClassification : PX.Data.IBqlField
        {
        }
        protected string _AssetClassification;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Asset Classification")]
        [AssetClassificationListAttribute]
        public virtual string AssetClassification
        {
            get
            {
                return this._AssetClassification;
            }
            set
            {
                this._AssetClassification = value;
            }
        }
        #endregion
        #region PropertyRedeem
        public abstract class propertyRedeem : PX.Data.IBqlField
        {
        }
        protected bool? _PropertyRedeem;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Property Redeem")]
        public virtual bool? PropertyRedeem
        {
            get
            {
                return this._PropertyRedeem;
            }
            set
            {
                this._PropertyRedeem = value;
            }
        }
        #endregion
        #region InstalmentStartDate
        public abstract class instalmentStartDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _InstalmentStartDate;
        [PXDBDate()]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Instalment Start Date")]
        public virtual DateTime? InstalmentStartDate
        {
            get
            {
                return this._InstalmentStartDate;
            }
            set
            {
                this._InstalmentStartDate = value;
            }
        }
        #endregion
        #region InstalmentEndDate
        public abstract class instalmentEndDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _InstalmentEndDate;
        [PXDBDate()]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Instalment End Date")]
        public virtual DateTime? InstalmentEndDate
        {
            get
            {
                return this._InstalmentEndDate;
            }
            set
            {
                this._InstalmentEndDate = value;
            }
        }
        #endregion
        #region legalExpenses
        public abstract class LegalExpenses : PX.Data.IBqlField
        {
        }
        protected decimal? _legalExpenses;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Legal Expenses")]
        public virtual decimal? legalExpenses
        {
            get
            {
                return this._legalExpenses;
            }
            set
            {
                this._legalExpenses = value;
            }
        }
        #endregion
        #region UtilitiesExpenses
        public abstract class utilitiesExpenses : PX.Data.IBqlField
        {
        }
        protected decimal? _UtilitiesExpenses;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Utilities Expenses")]
        public virtual decimal? UtilitiesExpenses
        {
            get
            {
                return this._UtilitiesExpenses;
            }
            set
            {
                this._UtilitiesExpenses = value;
            }
        }
        #endregion
        #region LastEvaluationID
        public abstract class lastEvaluationID : PX.Data.IBqlField
        {
        }
        protected Int32? _LastEvaluationID;
        [PXDBInt()]
        [PXUIField(Enabled = false)]
        public virtual Int32? LastEvaluationID
        {
            get
            {
                return this._LastEvaluationID;
            }
            set
            {
                this._LastEvaluationID = value;
            }
        }
        #endregion
        #region LastEvaluationAmtCury
        public abstract class lastEvaluationAmtCury : PX.Data.IBqlField
        {
        }
        protected decimal? _LastEvaluationAmtCury;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Last Evaluation Amount In Currency")]
        public virtual decimal? LastEvaluationAmtCury
        {
            get
            {
                return this._LastEvaluationAmtCury;
            }
            set
            {
                this._LastEvaluationAmtCury = value;
            }
        }
        #endregion
        #region LastEvaluationAmt
        public abstract class lastEvaluationAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _LastEvaluationAmt;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Last Evaluation Amount")]
        public virtual decimal? LastEvaluationAmt
        {
            get
            {
                return this._LastEvaluationAmt;
            }
            set
            {
                this._LastEvaluationAmt = value;
            }
        }
        #endregion
        #region LastEvaluationSalvageAmtCury
        public abstract class lastEvaluationSalvageAmtCury : PX.Data.IBqlField
        {
        }
        protected decimal? _LastEvaluationSalvageAmtCury;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Last Evaluation Salvage Value In Currency")]
        public virtual decimal? LastEvaluationSalvageAmtCury
        {
            get
            {
                return this._LastEvaluationSalvageAmtCury;
            }
            set
            {
                this._LastEvaluationSalvageAmtCury = value;
            }
        }
        #endregion
        #region LastEvaluationSalvageAmt
        public abstract class lastEvaluationSalvageAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _LastEvaluationSalvageAmt;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Last Evaluation Salvage Value")]
        public virtual decimal? LastEvaluationSalvageAmt
        {
            get
            {
                return this._LastEvaluationSalvageAmt;
            }
            set
            {
                this._LastEvaluationSalvageAmt = value;
            }
        }
        #endregion
        #region RARecommendator
        public abstract class rARecommendator : PX.Data.IBqlField
        {
        }
        protected Guid? _RARecommendator;
        [PXDBField()]
        [PXUIField(DisplayName = "RA Recommendator")]
        [PXSelector(typeof(Search<PX.Objects.EP.EPEmployee.userID, Where<EPEmployeeExt.usrIsRaRecommendator, Equal<True>>>), DescriptionField = typeof(PX.Objects.EP.EPEmployee.acctName), SubstituteKey = typeof(PX.Objects.EP.EPEmployee.acctCD))]
        public virtual Guid? RARecommendator
        {
            get
            {
                return this._RARecommendator;
            }
            set
            {
                this._RARecommendator = value;
            }
        }
        #endregion
        #region LoanManager
        public abstract class loanManager : PX.Data.IBqlField
        {
        }
        protected Guid? _LoanManager;
        [PXDBField()]
        [PXUIField(DisplayName = "Problem Loan Manager")]
        [PXOwnerSelector()]
        public virtual Guid? LoanManager
        {
            get
            {
                return this._LoanManager;
            }
            set
            {
                this._LoanManager = value;
            }
        }
        #endregion
        #region RAAccountID
        public abstract class rAAccountID : PX.Data.IBqlField
        {
        }
        protected int? _RAAccountID;
        [PXDBInt()]
        [PXUIField(DisplayName = "RAAccountID")]
        public virtual int? RAAccountID
        {
            get
            {
                return this._RAAccountID;
            }
            set
            {
                this._RAAccountID = value;
            }
        }
        #endregion
        #region TransitAccountID
        public abstract class transitAccountID : PX.Data.IBqlField
        {
        }
        protected int? _TransitAccountID;
        [PXDBInt()]
        [PXUIField(DisplayName = "TransitAccountID")]
        public virtual int? TransitAccountID
        {
            get
            {
                return this._TransitAccountID;
            }
            set
            {
                this._TransitAccountID = value;
            }
        }
        #endregion

        #region EstateType
        public abstract class estateType : PX.Data.IBqlField
        {
        }
        protected string _EstateType;
        [PXString]
        [PXDefault("NotSpecified")]
        [PXUIField(Visible = false)]
        public string EstateType { get; set; }
        #endregion


        #region ExpectedRepossessionCuryID
        public abstract class expectedRepossessionCuryID : PX.Data.IBqlField
        {
        }
        protected string _ExpectedRepossessionCuryID;
        [PXDBString(5, IsUnicode = true)]
        [PXSelector(typeof(Currency.curyID))]
        [PXUIField(DisplayName = "Currency ID")]
        public virtual string ExpectedRepossessionCuryID
        {
            get
            {
                return this._ExpectedRepossessionCuryID;
            }
            set
            {
                this._ExpectedRepossessionCuryID = value;
            }
        }
        #endregion
        #region ExpectedRepossessionCuryRate
        public abstract class expectedRepossessionCuryRate : PX.Data.IBqlField
        {
        }
        protected decimal? _ExpectedRepossessionCuryRate;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Currency Rate")]
        public virtual decimal? ExpectedRepossessionCuryRate
        {
            get
            {
                return this._ExpectedRepossessionCuryRate;
            }
            set
            {
                this._ExpectedRepossessionCuryRate = value;
            }
        }
        #endregion
        #region FixedRate
        public abstract class fixedRate : PX.Data.IBqlField
        {
        }
        protected bool? _FixedRate;
        [PXDBBool()]
        [PXUIField(DisplayName = "Fixed Rate")]
        public virtual bool? FixedRate
        {
            get
            {
                return this._FixedRate;
            }
            set
            {
                this._FixedRate = value;

            }
        }
        #endregion
        #region ExpectedCuryDate
        public abstract class expectedCuryDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ExpectedCuryDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Currency Date")]
        public virtual DateTime? ExpectedCuryDate
        {
            get
            {
                return this._ExpectedCuryDate;
            }
            set
            {
                this._ExpectedCuryDate = value;
            }
        }
        #endregion

        #region ExpectedRepossessionAmt
        public abstract class expectedRepossessionAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _ExpectedRepossessionAmt;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Amount")]
        public virtual decimal? ExpectedRepossessionAmt
        {
            get
            {
                return this._ExpectedRepossessionAmt;
            }
            set
            {
                this._ExpectedRepossessionAmt = value;
            }
        }
        #endregion
        #region ExpectedRepossessionAmtCury
        public abstract class expectedRepossessionAmtCury : PX.Data.IBqlField
        {
        }
        protected decimal? _ExpectedRepossessionAmtCury;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in Currency")]
        public virtual decimal? ExpectedRepossessionAmtCury
        {
            get
            {
                return this._ExpectedRepossessionAmtCury;
            }
            set
            {
                this._ExpectedRepossessionAmtCury = value;
            }
        }
        #endregion
        #region ExpectedRepossessionAmtGel
        public abstract class expectedRepossessionAmtGel : PX.Data.IBqlField
        {
        }
        protected decimal? _ExpectedRepossessionAmtGel;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in GEL")]
        public virtual decimal? ExpectedRepossessionAmtGel
        {
            get
            {
                return this._ExpectedRepossessionAmtGel;
            }
            set
            {
                this._ExpectedRepossessionAmtGel = value;
            }
        }
        #endregion     

        #region RecommendedRepossessionCuryID
        public abstract class recommendedRepossessionCuryID : PX.Data.IBqlField
        {
        }
        protected string _RecommendedRepossessionCuryID;
        [PXDBString(5, IsUnicode = true)]
        [PXSelector(typeof(Currency.curyID))]
        [PXUIField(DisplayName = "Currency ID")]
        public virtual string RecommendedRepossessionCuryID
        {
            get
            {
                return this._RecommendedRepossessionCuryID;
            }
            set
            {
                this._RecommendedRepossessionCuryID = value;
            }
        }
        #endregion
        #region RecommendedRepossessionCuryRate
        public abstract class recommendedRepossessionCuryRate : PX.Data.IBqlField
        {
        }
        protected decimal? _RecommendedRepossessionCuryRate;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Currency Rate")]
        public virtual decimal? RecommendedRepossessionCuryRate
        {
            get
            {
                return this._RecommendedRepossessionCuryRate;
            }
            set
            {
                this._RecommendedRepossessionCuryRate = value;
            }
        }
        #endregion
        #region RecommendedCuryDate
        public abstract class recommendedCuryDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _RecommendedCuryDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Currency Date")]
        public virtual DateTime? RecommendedCuryDate
        {
            get
            {
                return this._RecommendedCuryDate;
            }
            set
            {
                this._RecommendedCuryDate = value;
            }
        }
        #endregion
        #region RecommendedRepossessionAmt
        public abstract class recommendedRepossessionAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _RecommendedRepossessionAmt;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Amount")]
        public virtual decimal? RecommendedRepossessionAmt
        {
            get
            {
                return this._RecommendedRepossessionAmt;
            }
            set
            {
                this._RecommendedRepossessionAmt = value;
            }
        }
        #endregion
        #region RecommendedRepossessionAmtCury
        public abstract class recommendedRepossessionAmtCury : PX.Data.IBqlField
        {
        }
        protected decimal? _RecommendedRepossessionAmtCury;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in Currency")]
        public virtual decimal? RecommendedRepossessionAmtCury
        {
            get
            {
                return this._RecommendedRepossessionAmtCury;
            }
            set
            {
                this._RecommendedRepossessionAmtCury = value;
            }
        }
        #endregion
        #region RecommendedRepossessionAmtGel
        public abstract class recommendedRepossessionAmtGel : PX.Data.IBqlField
        {
        }
        protected decimal? _RecommendedRepossessionAmtGel;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in GEL")]
        public virtual decimal? RecommendedRepossessionAmtGel
        {
            get
            {
                return this._RecommendedRepossessionAmtGel;
            }
            set
            {
                this._RecommendedRepossessionAmtGel = value;
            }
        }
        #endregion
        #region FinalRepossessionCuryID
        public abstract class finalRepossessionCuryID : PX.Data.IBqlField
        {
        }
        protected string _FinalRepossessionCuryID;
        [PXDBString(5, IsUnicode = true)]
        [PXSelector(typeof(Currency.curyID))]
        [PXUIField(DisplayName = "Currency ID")]
        public virtual string FinalRepossessionCuryID
        {
            get
            {
                return this._FinalRepossessionCuryID;
            }
            set
            {
                this._FinalRepossessionCuryID = value;
            }
        }
        #endregion
        #region FinalRepossessionCuryRate
        public abstract class finalRepossessionCuryRate : PX.Data.IBqlField
        {
        }
        protected decimal? _FinalRepossessionCuryRate;
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Currency Rate")]
        public virtual decimal? FinalRepossessionCuryRate
        {
            get
            {
                return this._FinalRepossessionCuryRate;
            }
            set
            {
                this._FinalRepossessionCuryRate = value;
            }
        }
        #endregion
        #region FinalCuryDate
        public abstract class finalCuryDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _FinalCuryDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Currency Date")]
        public virtual DateTime? FinalCuryDate
        {
            get
            {
                return this._FinalCuryDate;
            }
            set
            {
                this._FinalCuryDate = value;
            }
        }
        #endregion
        #region SoldOnAuctionDate
        public abstract class soldOnAuctionDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _SoldOnAuctionDate;
        [PXDBDate()]
        public virtual DateTime? SoldOnAuctionDate
        {
            get
            {
                return this._SoldOnAuctionDate;
            }
            set
            {
                this._SoldOnAuctionDate = value;
            }
        }
        #endregion
        #region ContractingStartDate
        public abstract class contractingStartDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _ContractingStartDate;
        [PXDBDate()]
        public virtual DateTime? ContractingStartDate
        {
            get
            {
                return this._ContractingStartDate;
            }
            set
            {
                this._ContractingStartDate = value;
            }
        }
        #endregion
        #region FinalRepossessionAmt
        public abstract class finalRepossessionAmt : PX.Data.IBqlField
        {
        }
        protected decimal? _FinalRepossessionAmt;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Amount")]
        public virtual decimal? FinalRepossessionAmt
        {
            get
            {
                return this._FinalRepossessionAmt;
            }
            set
            {
                this._FinalRepossessionAmt = value;
            }
        }
        #endregion
        #region FinalRepossessionAmtCury
        public abstract class finalRepossessionAmtCury : PX.Data.IBqlField
        {
        }
        protected decimal? _FinalRepossessionAmtCury;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in Currency")]
        public virtual decimal? FinalRepossessionAmtCury
        {
            get
            {
                return this._FinalRepossessionAmtCury;
            }
            set
            {
                this._FinalRepossessionAmtCury = value;
            }
        }
        #endregion
        #region FinalRepossessionAmtGel
        public abstract class finalRepossessionAmtGel : PX.Data.IBqlField
        {
        }
        protected decimal? _FinalRepossessionAmtGel;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Repossession Amount in GEL")]
        public virtual decimal? FinalRepossessionAmtGel
        {
            get
            {
                return this._FinalRepossessionAmtGel;
            }
            set
            {
                this._FinalRepossessionAmtGel = value;
            }
        }
        #endregion
        #region IFRSRAAccountID
        public abstract class iFRSRAAccountID : PX.Data.IBqlField
        {
        }
        protected Int32? _IFRSRAAccountID;
        [Account(DisplayName = "RA Account", DescriptionField = typeof(Account.description))]
        public virtual Int32? IFRSRAAccountID
        {
            get
            {
                return this._IFRSRAAccountID;
            }
            set
            {
                this._IFRSRAAccountID = value;
            }
        }
        #endregion
        #region IFRSTransitAccountID
        public abstract class iFRSTransitAccountID : PX.Data.IBqlField
        {
        }
        protected Int32? _IFRSTransitAccountID;
        [Account(DisplayName = "Transit Account", DescriptionField = typeof(Account.description))]
        public virtual Int32? IFRSTransitAccountID
        {
            get
            {
                return this._IFRSTransitAccountID;
            }
            set
            {
                this._IFRSTransitAccountID = value;
            }
        }
        #endregion
        #region NBGAccount
        public abstract class nBGAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Account")]
        public virtual string NBGAccount
        {
            get
            {
                return this._NBGAccount;
            }
            set
            {
                this._NBGAccount = value;
            }
        }
        #endregion
        #region NBGTransitAccount
        public abstract class nBGTransitAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGTransitAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Transit Account")]
        public virtual string NBGTransitAccount
        {
            get
            {
                return this._NBGTransitAccount;
            }
            set
            {
                this._NBGTransitAccount = value;
            }
        }
        #endregion
        #region NBGReserveAccount
        public abstract class nBGReserveAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGReserveAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Reserve Account")]
        public virtual string NBGReserveAccount
        {
            get
            {
                return this._NBGReserveAccount;
            }
            set
            {
                this._NBGReserveAccount = value;
            }
        }
        #endregion
        #region NBGReserveExpensesAccount
        public abstract class nNBGReserveExpensesAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGReserveExpensesAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Reserve Expenses Account")]
        public virtual string NBGReserveExpensesAccount
        {
            get
            {
                return this._NBGReserveExpensesAccount;
            }
            set
            {
                this._NBGReserveExpensesAccount = value;
            }
        }
        #endregion
        #region OtherImprovements
        public abstract class otherImprovements : PX.Data.IBqlField
        {
        }
        protected string _OtherImprovements;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Other Improvements")]
        public virtual string OtherImprovements
        {
            get
            {
                return this._OtherImprovements;
            }
            set
            {
                this._OtherImprovements = value;
            }
        }
        #endregion
        #region Comment
        public abstract class comment : PX.Data.IBqlField
        {
        }
        protected string _Comment;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Comment")]
        public virtual string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
            }
        }
        #endregion
        #region Image1
        public abstract class image1 : IBqlField { }
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Image 1")]
        public string Image1 { get; set; }
        #endregion
        #region Image2
        public abstract class image2 : IBqlField { }
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Image 2")]
        public string Image2 { get; set; }
        #endregion
        #region Image3
        public abstract class image3 : IBqlField { }
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "Image 3")]
        public string Image3 { get; set; }
        #endregion
        #region PropertyOwner
        public abstract class propertyOwners : PX.Data.IBqlField
        {
        }
        protected string _PropertyOwners;
        [PXDBString(1000, IsUnicode = true)]
        [PXUIField(DisplayName = "Property Owners")]
        public virtual string PropertyOwners
        {
            get
            {
                return this._PropertyOwners;
            }
            set
            {
                this._PropertyOwners = value;
            }
        }
        #endregion
        #region Borrowers
        public abstract class borrowers : PX.Data.IBqlField
        {
        }
        protected string _Borrowers;
        [PXDBString(1000, IsUnicode = true)]
        [PXUIField(DisplayName = "Borrowers")]
        public virtual string Borrowers
        {
            get
            {
                return this._Borrowers;
            }
            set
            {
                this._Borrowers = value;
            }
        }
        #endregion
        #region LineCntr
        public abstract class lineCntr : PX.Data.IBqlField
        {
        }
        protected int? _LineCntr;
        [PXDBInt()]
        [PXDefault(0)]
        public virtual int? LineCntr
        {
            get
            {
                return this._LineCntr;
            }
            set
            {
                this._LineCntr = value;
            }
        }
        #endregion
        #region RepossessionApplicationID
        public abstract class repossessionApplicationID : PX.Data.IBqlField
        {
        }
        protected int? _RepossessionApplicationID;
        [PXInt()]
        public virtual int? RepossessionApplicationID
        {
            get
            {
                return this._RepossessionApplicationID;
            }
            set
            {
                this._RepossessionApplicationID = value;
            }
        }
        #endregion

        #region ApplicationRefNbr
        public abstract class applicationRefNbr : PX.Data.IBqlField
        {
        }
        protected string _ApplicationRefNbr;
        [PXString]
        [PXUIField(Visible = false)]
        public string ApplicationRefNbr { get; set; }
        #endregion

        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
        
        #region Option30PercProjectCurrency
        public abstract class option30PercProjectCurrency : PX.Data.IBqlField
        {
        }
        protected string _Option30PercProjectCurrency;
        [PXDBString(5, IsUnicode = true)]
        [PXUIField(DisplayName = "Option/30% project currency", Visible = false)]
        [PXSelector(typeof(CurrencyList.curyID), typeof(CurrencyList.curyID))]


        public virtual string Option30PercProjectCurrency
        {
            get
            {
                return this._Option30PercProjectCurrency;
            }
            set
            {
                this._Option30PercProjectCurrency = value;
            }
        }
        #endregion
        #region Option30PercProjectAmountInCurrency
        public abstract class option30PercProjectAmountInCurrency : PX.Data.IBqlField
        {
        }
        protected decimal? _Option30PercProjectAmountInCurrency;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Option /30% project amount in currency", Visible = false)]
        public virtual decimal? Option30PercProjectAmountInCurrency
        {
            get
            {
                return this._Option30PercProjectAmountInCurrency;
            }
            set
            {
                this._Option30PercProjectAmountInCurrency = value;
            }
        }
        #endregion
        #region MortgageContractNumber
        public abstract class mortgageContractNumber : PX.Data.IBqlField
        {
        }
        protected string _MortgageContractNumber;
        [PXDBString(25, IsUnicode = true)]
        [PXUIField(DisplayName = "Mortgage Contract Number")]
        public virtual string MortgageContractNumber
        {
            get
            {
                return this._MortgageContractNumber;
            }
            set
            {
                this._MortgageContractNumber = value;
            }
        }
        #endregion


        #region Unbound Fields
        #region PrintDate
        public abstract class printDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _PrintDate;
        [PXDate]
        [PXUIField(Visible = false)]
        public DateTime? PrintDate { get; set; }
        #endregion

        #region RealNonRealEstate
        public abstract class realNonRealEstate : PX.Data.IBqlField
        {
        }
        protected string _RealNonRealEstate;
        [PXString]
        [PXUIField(Visible = false)]
        public string RealNonRealEstate { get; set; }
        #endregion

        #region CadastralCodeSerialNumber
        public abstract class cadastralCodeSerialNumber : PX.Data.IBqlField
        {
        }
        protected string _CadastralCodeSerialNumber;
        [PXString]
        [PXUIField(Visible = false)]
        public string CadastralCodeSerialNumber { get; set; }
        #endregion

        #region CreditDebtAmount
        public abstract class creditDebtAmount : PX.Data.IBqlField
        {
        }
        protected decimal? _CreditDebtAmount;
        [PXDecimal(2)]
        [PXUIField(Visible = false)]
        public decimal? CreditDebtAmount { get; set; }
        #endregion

        #region PaidAmount
        public abstract class paidAmount : PX.Data.IBqlField
        {
        }
        protected decimal? _PaidAmount;
        [PXDecimal(2)]
        [PXUIField(Visible = false)]
        public decimal? PaidAmount { get; set; }
        #endregion

        #region AllRealNonRealEstates
        //public abstract class allRealNonRealEstates : PX.Data.IBqlField
        //{
        //}
        //protected string _AllRealNonRealEstates;
        //[PXString]
        //[PXUIField(Visible = false)]
        //public string AllRealNonRealEstates { get; set; }
        #endregion

        #endregion
        #region Restrictions
        public abstract class restrictions : PX.Data.IBqlField
        {
        }
        protected string _Restrictions;
        [PXDBString(1000, IsUnicode = true)]
        [PXUIField(DisplayName = "Restrictions")]
        public virtual string Restrictions
        {
            get
            {
                return this._Restrictions;
            }
            set
            {
                this._Restrictions = value;
            }
        }
        #endregion
    }
}
﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using PX.Objects.CM;

    [System.SerializableAttribute()]
	public class RAAssetAllocation : PX.Data.IBqlTable
	{
		#region AssetAllocationID
		public abstract class assetAllocationID : PX.Data.IBqlField
		{
		}
		protected int? _AssetAllocationID;
		[PXDBIdentity()]
		[PXUIField(Enabled = false)]
		public virtual int? AssetAllocationID
		{
			get
			{
				return this._AssetAllocationID;
			}
			set
			{
				this._AssetAllocationID = value;
			}
		}
        #endregion
        #region RepossessionApplicationID
        public abstract class repossessionApplicationID : PX.Data.IBqlField
        {
        }
        protected int? _RepossessionApplicationID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RARepossessionApplication.repossessionApplicationID))]
        [PXParent(typeof(Select<
        	RARepossessionApplication,
        	Where<RARepossessionApplication.repossessionApplicationID, Equal<Current<RAAssetAllocation.repossessionApplicationID>>>>))]
        public virtual int? RepossessionApplicationID
        {
            get
            {
                return this._RepossessionApplicationID;
            }
            set
            {
                this._RepossessionApplicationID = value;
            }
        }
        #endregion
        #region LoanID
        public abstract class loanID : PX.Data.IBqlField
		{
		}
		protected int? _LoanID;
		[PXDBInt()]
		[PXDefault(0)]
		[PXUIField(DisplayName = "Loan ID")]
		public virtual int? LoanID
		{
			get
			{
				return this._LoanID;
			}
			set
			{
				this._LoanID = value;
			}
		}
		#endregion
		#region LineNbr
		public abstract class lineNbr : PX.Data.IBqlField
		{
		}
		protected int? _LineNbr;
        [PXDBInt(IsKey = true)]
        [PXLineNbr(typeof(RARepossessionApplication.lineCntr))]
        [PXUIField(DisplayName = "Line Nbr.")]
		public virtual int? LineNbr
		{
			get
			{
				return this._LineNbr;
			}
			set
			{
				this._LineNbr = value;
			}
		}
		#endregion
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
		[PXDBInt()]
		[PXUIField(DisplayName = "Asset ID")]
		public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
        #endregion
        #region PaidPrincipleCury
        public abstract class paidPrincipleCury : PX.Data.IBqlField
        {
        }
        protected decimal? _PaidPrincipleCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Paid Principle")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RAAssetAllocation.paidPrinciple))]
        public virtual decimal? PaidPrincipleCury
        {
            get
            {
                return this._PaidPrincipleCury;
            }
            set
            {
                this._PaidPrincipleCury = value;
            }
        }
        #endregion
        #region PaidInterestCury
        public abstract class paidInterestCury : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidInterestCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Paid Interest")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RAAssetAllocation.paidInterest))]
        public virtual decimal? PaidInterestCury
		{
			get
			{
				return this._PaidInterestCury;
			}
			set
			{
				this._PaidInterestCury = value;
			}
		}
		#endregion
		#region PaidPenaltyCury
		public abstract class paidPenaltyCury : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidPenaltyCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Paid Penalty")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RAAssetAllocation.paidPenalty))]
        public virtual decimal? PaidPenaltyCury
		{
			get
			{
				return this._PaidPenaltyCury;
			}
			set
			{
				this._PaidPenaltyCury = value;
			}
		}
		#endregion
		#region PaidInsuranceCury
		public abstract class paidInsuranceCury : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidInsuranceCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Paid Insurance")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RAAssetAllocation.paidInsurance))]
        public virtual decimal? PaidInsuranceCury
		{
			get
			{
				return this._PaidInsuranceCury;
			}
			set
			{
				this._PaidInsuranceCury = value;
			}
		}
		#endregion
		#region PaidCourtFeeCury
		public abstract class paidCourtFeeCury : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidCourtFeeCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Paid CourtFee")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RAAssetAllocation.paidCourtFee))]
        public virtual decimal? PaidCourtFeeCury
		{
			get
			{
				return this._PaidCourtFeeCury;
			}
			set
			{
				this._PaidCourtFeeCury = value;
			}
		}
        #endregion
        #region PaidPrinciple
        public abstract class paidPrinciple : PX.Data.IBqlField
        {
        }
        protected decimal? _PaidPrinciple;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Paid Principle (GEL)", Enabled = false)]
        public virtual decimal? PaidPrinciple
        {
            get
            {
                return this._PaidPrinciple;
            }
            set
            {
                this._PaidPrinciple = value;
            }
        }
        #endregion
        #region PaidInterest
        public abstract class paidInterest : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidInterest;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Paid Interest (GEL)", Enabled = false)]
		public virtual decimal? PaidInterest
		{
			get
			{
				return this._PaidInterest;
			}
			set
			{
				this._PaidInterest = value;
			}
		}
		#endregion
		#region PaidPenalty
		public abstract class paidPenalty : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidPenalty;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Paid Penalty (GEL)", Enabled = false)]
		public virtual decimal? PaidPenalty
		{
			get
			{
				return this._PaidPenalty;
			}
			set
			{
				this._PaidPenalty = value;
			}
		}
		#endregion
		#region PaidInsurance
		public abstract class paidInsurance : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidInsurance;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Paid Insurance (GEL)", Enabled = false)]
		public virtual decimal? PaidInsurance
        {
			get
			{
				return this._PaidInsurance;
			}
			set
			{
				this._PaidInsurance = value;
			}
		}
		#endregion
		#region PaidCourtFee
		public abstract class paidCourtFee : PX.Data.IBqlField
		{
		}
		protected decimal? _PaidCourtFee;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Paid CourtFee (GEL)", Enabled = false)]
		public virtual decimal? PaidCourtFee
		{
			get
			{
				return this._PaidCourtFee;
			}
			set
			{
				this._PaidCourtFee = value;
			}
		}
        #endregion
        #region RemainingPrinciple
        public abstract class remainingPrinciple : PX.Data.IBqlField
        {
        }
        protected decimal? _RemainingPrinciple;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Remaining Principle", Enabled = false)]
        public virtual decimal? RemainingPrinciple
        {
            get
            {
                return this._RemainingPrinciple;
            }
            set
            {
                this._RemainingPrinciple = value;
            }
        }
        #endregion
        #region RemainingInterest
        public abstract class remainingInterest : PX.Data.IBqlField
        {
        }
        protected decimal? _RemainingInterest;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Remaining Interest", Enabled = false)]
        public virtual decimal? RemainingInterest
        {
            get
            {
                return this._RemainingInterest;
            }
            set
            {
                this._RemainingInterest = value;
            }
        }
        #endregion
        #region RemainingPenalty
        public abstract class remainingPenalty : PX.Data.IBqlField
        {
        }
        protected decimal? _RemainingPenalty;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Remaining Penalty", Enabled = false)]
        public virtual decimal? RemainingPenalty
        {
            get
            {
                return this._RemainingPenalty;
            }
            set
            {
                this._RemainingPenalty = value;
            }
        }
        #endregion
        #region RemainingInsurance
        public abstract class remainingInsurance : PX.Data.IBqlField
        {
        }
        protected decimal? _RemainingInsurance;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Remaining Insurance", Enabled = false)]
        public virtual decimal? RemainingInsurance
        {
            get
            {
                return this._RemainingInsurance;
            }
            set
            {
                this._RemainingInsurance = value;
            }
        }
        #endregion
    }
}

﻿namespace RA
{
    using System;
    using PX.Data;

    [System.SerializableAttribute()]
    public class RAAssetLoan : PX.Data.IBqlTable
    {
        #region AssetLoanID
        public abstract class assetLoanID : PX.Data.IBqlField
        {
        }
        protected int? _AssetLoanID;
        [PXDBIdentity(IsKey =true)]
        [PXUIField(Enabled = false, Visible = false)]
        public virtual int? AssetLoanID
        {
            get
            {
                return this._AssetLoanID;
            }
            set
            {
                this._AssetLoanID = value;
            }
        }
        #endregion
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RAAsset.assetID))]
        [PXParent(typeof(Select<RAAsset,
        Where<RAAsset.assetID, Equal<Current<RAAssetLoan.assetID>>>>))]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region LoanID
        public abstract class loanID : PX.Data.IBqlField
        {
        }
        protected int? _LoanID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Loan ID")]
        [PXSelector(typeof(Search<RALoan.loanID>),
            typeof(RALoan.loanCD),
            typeof(RALoan.lMSID),
            typeof(RALoan.curyID),
            typeof(RALoan.borrowerName),
            SubstituteKey = typeof(RALoan.loanCD),DescriptionField = typeof(RALoan.loanCD))]
        public virtual int? LoanID
        {
            get
            {
                return this._LoanID;
            }
            set
            {
                this._LoanID = value;
            }
        }
        #endregion
        #region LineNbr
        public abstract class lineNbr : PX.Data.IBqlField
        {
        }
        protected int? _LineNbr;
        [PXDBInt()]
        [PXLineNbr(typeof(RAAsset.lineCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? LineNbr
        {
            get
            {
                return this._LineNbr;
            }
            set
            {
                this._LineNbr = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

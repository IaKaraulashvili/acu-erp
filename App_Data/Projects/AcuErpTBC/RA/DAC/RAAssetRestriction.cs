﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using _Shared.RestrictionType;

    [System.SerializableAttribute()]
	public class RAAssetRestriction : PX.Data.IBqlTable
	{
		#region RestrictionID
		public abstract class restrictionID : PX.Data.IBqlField
		{
		}
		protected int? _RestrictionID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false, Visible = false)]
        public virtual int? RestrictionID
		{
			get
			{
				return this._RestrictionID;
			}
			set
			{
				this._RestrictionID = value;
			}
		}
		#endregion
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RAAsset.assetID))]
        [PXParent(typeof(Select<RAAsset,
        Where<RAAsset.assetID, Equal<Current<RAPropertyOwner.assetID>>>>))]
        public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region LineNbr
		public abstract class lineNbr : PX.Data.IBqlField
		{
		}
		protected int? _LineNbr;
        [PXDBInt(IsKey = true)]
        [PXLineNbr(typeof(RAAsset.lineCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? LineNbr
		{
			get
			{
				return this._LineNbr;
			}
			set
			{
				this._LineNbr = value;
			}
		}
		#endregion
		#region RestrictionStartDate
		public abstract class restrictionStartDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _RestrictionStartDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Restriction Start Date")]
		public virtual DateTime? RestrictionStartDate
		{
			get
			{
				return this._RestrictionStartDate;
			}
			set
			{
				this._RestrictionStartDate = value;
			}
		}
		#endregion
		#region RestrictionType
		public abstract class restrictionType : PX.Data.IBqlField
		{
		}
		protected string _RestrictionType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Restriction Type")]
        [RestrictionTypeListAttribute]
        public virtual string RestrictionType
		{
			get
			{
				return this._RestrictionType;
			}
			set
			{
				this._RestrictionType = value;
			}
		}
		#endregion
		#region ParticipantPersons
		public abstract class participantPersons : PX.Data.IBqlField
		{
		}
		protected string _ParticipantPersons;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Participant Persons")]
		public virtual string ParticipantPersons
		{
			get
			{
				return this._ParticipantPersons;
			}
			set
			{
				this._ParticipantPersons = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

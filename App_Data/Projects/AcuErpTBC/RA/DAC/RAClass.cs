﻿namespace RA
{
    using System;
    using PX.Data;
    using PX.Objects.GL;
    using _Shared;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(RAClassMaint))]
    public class RAClass : PX.Data.IBqlTable
    {
        #region ClassID
        public abstract class classID : PX.Data.IBqlField
        {
        }
        protected int? _ClassID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual int? ClassID
        {
            get
            {
                return this._ClassID;
            }
            set
            {
                this._ClassID = value;
            }
        }
        #endregion
        #region ClassCD
        public abstract class classCD : PX.Data.IBqlField
        {
        }
        protected string _ClassCD;
        [PXDBString(15, IsKey = true, IsUnicode = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Class ID")]
        [RAClassSelectorAttribute]
        public virtual string ClassCD
        {
            get
            {
                return this._ClassCD;
            }
            set
            {
                this._ClassCD = value;
            }
        }
        #endregion
        #region ExternalID
        public abstract class externalID : PX.Data.IBqlField
        {
        }
        protected string _ExternalID;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "External ID")]
        public virtual string ExternalID
        {
            get
            {
                return this._ExternalID;
            }
            set
            {
                this._ExternalID = value;
            }
        }
        #endregion
        #region Description
        public abstract class description : PX.Data.IBqlField
        {
        }
        protected string _Description;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Description")]
        public virtual string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                this._Description = value;
            }
        }
        #endregion
        #region RealEstate
        public abstract class realEstate : PX.Data.IBqlField
        {
        }
        protected bool? _RealEstate;
        [PXDBBool()]
        [PXDefault(false)]
        [PXUIField(DisplayName = "Real Estate")]
        public virtual bool? RealEstate
        {
            get
            {
                return this._RealEstate;
            }
            set
            {
                this._RealEstate = value;
            }
        }
        #endregion
        #region AssetType
        public abstract class assetType : PX.Data.IBqlField
        {
        }
        protected string _AssetType;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Asset Type")]
        [AssetTypeListAttribute]
        public virtual string AssetType
        {
            get
            {
                return this._AssetType;
            }
            set
            {
                this._AssetType = value;
            }
        }
        #endregion
        #region IFRSRAAccountID
        public abstract class iFRSRAAccountID : PX.Data.IBqlField
        {
        }
        protected Int32? _IFRSRAAccountID;
        [Account(DisplayName = "RA Account", DescriptionField = typeof(Account.description))]
        public virtual Int32? IFRSRAAccountID
        {
            get
            {
                return this._IFRSRAAccountID;
            }
            set
            {
                this._IFRSRAAccountID = value;
            }
        }
        #endregion
        #region IFRSTransitAccountID
        public abstract class iFRSTransitAccountID : PX.Data.IBqlField
        {
        }
        protected Int32? _IFRSTransitAccountID;
        [Account(DisplayName = "Transit Account", DescriptionField = typeof(Account.description))]
        public virtual Int32? IFRSTransitAccountID
        {
            get
            {
                return this._IFRSTransitAccountID;
            }
            set
            {
                this._IFRSTransitAccountID = value;
            }
        }
        #endregion
        #region NBGAccount
        public abstract class nBGAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Account")]
        public virtual string NBGAccount
        {
            get
            {
                return this._NBGAccount;
            }
            set
            {
                this._NBGAccount = value;
            }
        }
        #endregion
        #region NBGTransitAccount
        public abstract class nBGTransitAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGTransitAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Transit Account")]
        public virtual string NBGTransitAccount
        {
            get
            {
                return this._NBGTransitAccount;
            }
            set
            {
                this._NBGTransitAccount = value;
            }
        }
        #endregion
        #region NBGReserveAccount
        public abstract class nBGReserveAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGReserveAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Reserve Account")]
        public virtual string NBGReserveAccount
        {
            get
            {
                return this._NBGReserveAccount;
            }
            set
            {
                this._NBGReserveAccount = value;
            }
        }
        #endregion
        #region NBGReserveExpensesAccount
        public abstract class nBGReserveExpensesAccount : PX.Data.IBqlField
        {
        }
        protected string _NBGReserveExpensesAccount;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Reserve Expenses Account")]
        public virtual string NBGReserveExpensesAccount
        {
            get
            {
                return this._NBGReserveExpensesAccount;
            }
            set
            {
                this._NBGReserveExpensesAccount = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

    }
}

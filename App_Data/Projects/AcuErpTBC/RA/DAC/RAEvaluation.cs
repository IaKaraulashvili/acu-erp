﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using PX.Objects.CS;
    using _Shared;
    using EstimationType._Shared;
    using _Shared.EvaluationStatus;
    using _Shared.EvaluationType;
    using _Shared.EvaluationApproach;
    using PX.Objects.CM;
    using PX.Objects.GL;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(RAEvaluationMaint))]
	public class RAEvaluation : PX.Data.IBqlTable
	{
		#region EvaluationID
		public abstract class evaluationID : PX.Data.IBqlField
		{
		}
		protected Int32? _EvaluationID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false)]
        public virtual Int32? EvaluationID
		{
			get
			{
				return this._EvaluationID;
			}
			set
			{
				this._EvaluationID = value;
			}
		}
		#endregion
		#region EvaluationCD
		public abstract class evaluationCD : PX.Data.IBqlField
		{
		}
		protected string _EvaluationCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [RAEvaluationSelectorAttribute]
        [AutoNumber(typeof(RASetup.evaluationNumberingID), typeof(RAEvaluation.createdDateTime))]
        [PXUIField(DisplayName = "Evaluation ID", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string EvaluationCD
		{
			get
			{
				return this._EvaluationCD;
			}
			set
			{
				this._EvaluationCD = value;
			}
		}
		#endregion
		#region ExternalID
		public abstract class externalID : PX.Data.IBqlField
		{
		}
		protected string _ExternalID;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "External ID")]
		public virtual string ExternalID
		{
			get
			{
				return this._ExternalID;
			}
			set
			{
				this._ExternalID = value;
			}
		}
		#endregion
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected Int32? _AssetID;
		[PXDBInt()]
		[PXUIField(DisplayName = "Asset ID")]
        [PXDefault()]
        [PXSelector(typeof(Search<RAAsset.assetID>),
            typeof(RAAsset.assetCD),
            typeof(RAAsset.assetType),
            typeof(RAAsset.description),
            DescriptionField = typeof(RAAsset.description), SubstituteKey = typeof(RAAsset.assetCD))]
        public virtual Int32? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected string _Status;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Status")]
        [EvaluationStatusListAttribute]
        public virtual string Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region EstimationType
		public abstract class estimationType : PX.Data.IBqlField
		{
		}
		protected string _EstimationType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Estimation Type")]
        [EstimationTypeListAttribute]
        public virtual string EstimationType
		{
			get
			{
				return this._EstimationType;
			}
			set
			{
				this._EstimationType = value;
			}
		}
		#endregion
		#region EvaluationType
		public abstract class evaluationType : PX.Data.IBqlField
		{
		}
		protected string _EvaluationType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Evaluation Type")]
        [EvaluationTypeListAttribute]
		public virtual string EvaluationType
		{
			get
			{
				return this._EvaluationType;
			}
			set
			{
				this._EvaluationType = value;
			}
		}
        #endregion
        #region CuryInfoID
        public abstract class curyInfoID : PX.Data.IBqlField
        {
        }
        protected Int64? _CuryInfoID;

        /// <summary>
        /// The identifier of the <see cref="CurrencyInfo">CurrencyInfo</see> object associated with the document.
        /// </summary>
        /// <value>
        /// Corresponds to the <see cref="CurrencyInfoID"/> field.
        /// </value>
        [PXDBLong()]
        [CurrencyInfo(ModuleCode = "RA")]
        public virtual Int64? CuryInfoID
        {
            get
            {
                return this._CuryInfoID;
            }
            set
            {
                this._CuryInfoID = value;
            }
        }
        #endregion
        #region CuryID
        public abstract class curyID : PX.Data.IBqlField
		{
		}
		protected string _CuryID;
        [PXDBString(5, IsUnicode = true, InputMask = ">LLLLL")]
        [PXUIField(DisplayName = "Evaluation Currency", Visibility = PXUIVisibility.SelectorVisible)]
        [PXDefault(typeof(Search<Company.baseCuryID>))]
        [PXSelector(typeof(Currency.curyID))]
        public virtual string CuryID
		{
			get
			{
				return this._CuryID;
			}
			set
			{
				this._CuryID = value;
			}
		}
        #endregion
        #region FairMarketValueCury
        public abstract class fairMarketValueCury : PX.Data.IBqlField
        {
        }
        protected Decimal? _FairMarketValueCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RAEvaluation.curyInfoID), typeof(RAEvaluation.fairMarketValue))]
        [PXUIField(DisplayName = "Fair/Market Value", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual Decimal? FairMarketValueCury
        {
            get
            {
                return this._FairMarketValueCury;
            }
            set
            {
                this._FairMarketValueCury = value;
            }
        }
        #endregion
        #region SalvageValueCury
        public abstract class salvageValueCury : PX.Data.IBqlField
        {
        }
        protected Decimal? _SalvageValueCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RAEvaluation.curyInfoID), typeof(RAEvaluation.salvageValue))]
        [PXUIField(DisplayName = "Salvage Value", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual Decimal? SalvageValueCury
        {
            get
            {
                return this._SalvageValueCury;
            }
            set
            {
                this._SalvageValueCury = value;
            }
        }
        #endregion
        #region FairMarketValue
        public abstract class fairMarketValue : PX.Data.IBqlField
        {
        }
        protected Decimal? _FairMarketValue;
        [PXDBBaseCury()]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual Decimal? FairMarketValue
        {
            get
            {
                return this._FairMarketValue;
            }
            set
            {
                this._FairMarketValue = value;
            }
        }
        #endregion
        #region SalvageValue
        public abstract class salvageValue : PX.Data.IBqlField
        {
        }
        protected Decimal? _SalvageValue;
        [PXDBBaseCury()]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual Decimal? SalvageValue
        {
            get
            {
                return this._SalvageValue;
            }
            set
            {
                this._SalvageValue = value;
            }
        }
        #endregion
        #region Appraiser
        public abstract class appraiser : PX.Data.IBqlField
		{
		}
		protected string _Appraiser;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Appraiser")]
		public virtual string Appraiser
		{
			get
			{
				return this._Appraiser;
			}
			set
			{
				this._Appraiser = value;
			}
		}
		#endregion
		#region EvaluationDate
		public abstract class evaluationDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _EvaluationDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Evaluation Date")]
		public virtual DateTime? EvaluationDate
		{
			get
			{
				return this._EvaluationDate;
			}
			set
			{
				this._EvaluationDate = value;
			}
		}
		#endregion
		#region EvaluationApproach
		public abstract class evaluationApproach : PX.Data.IBqlField
		{
		}
		protected string _EvaluationApproach;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Evaluation Approach")]
        [EvaluationApproachListAttribute]
		public virtual string EvaluationApproach
		{
			get
			{
				return this._EvaluationApproach;
			}
			set
			{
				this._EvaluationApproach = value;
			}
		}
        #endregion
        #region InspectDate
        public abstract class inspectDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _InspectDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Inspect Date")]
        public virtual DateTime? InspectDate
        {
            get
            {
                return this._InspectDate;
            }
            set
            {
                this._InspectDate = value;
            }
        }
        #endregion
        #region CurrencyDate
        public abstract class currencyDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CurrencyDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Currency Date")]
        public virtual DateTime? CurrencyDate
        {
            get
            {
                return this._CurrencyDate;
            }
            set
            {
                this._CurrencyDate = value;
            }
        }
        #endregion
        #region EvaluationExpenses
        public abstract class evaluationExpenses : PX.Data.IBqlField
		{
		}
		protected decimal? _EvaluationExpenses;
		[PXDBDecimal(2)]
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXUIField(DisplayName = "Evaluation Expenses")]
		public virtual decimal? EvaluationExpenses
		{
			get
			{
				return this._EvaluationExpenses;
			}
			set
			{
				this._EvaluationExpenses = value;
			}
		}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion

        #region EvaluationRequestDate
        public abstract class evaluationRequestDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _EvaluationRequestDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Evaluation Request Date")]
        public virtual DateTime? EvaluationRequestDate
        {
            get
            {
                return this._EvaluationRequestDate;
            }
            set
            {
                this._EvaluationRequestDate = value;
            }
        }
        #endregion

        #region Comment
        public abstract class comment : PX.Data.IBqlField
        {
        }
        protected string _Comment;
        [PXDBString(250, IsUnicode = true)]
        [PXUIField(DisplayName = "Comment")]
        public virtual string Comment
        {
            get
            {
                return this._Comment;
            }
            set
            {
                this._Comment = value;
            }
        }
        #endregion

    }
}

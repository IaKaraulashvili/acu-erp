﻿﻿namespace RA
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class RAFinancialDetail : PX.Data.IBqlTable
	{
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RAAsset.assetID))]
        [PXParent(typeof(Select<RAAsset,
        Where<RAAsset.assetID, Equal<Current<RAFinancialDetail.assetID>>>>))]
        public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region NBGBalanceValueGross
		public abstract class nBGBalanceValueGross : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGBalanceValueGross;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Balance Value NBG Gross")]
		public virtual decimal? NBGBalanceValueGross
		{
			get
			{
				return this._NBGBalanceValueGross;
			}
			set
			{
				this._NBGBalanceValueGross = value;
			}
		}
		#endregion
		#region NBGBalanceValueNet
		public abstract class nBGBalanceValueNet : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGBalanceValueNet;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Balance Value NBG Net")]
		public virtual decimal? NBGBalanceValueNet
		{
			get
			{
				return this._NBGBalanceValueNet;
			}
			set
			{
				this._NBGBalanceValueNet = value;
			}
		}
		#endregion
		#region VATNBG
		public abstract class vATNBG : PX.Data.IBqlField
		{
		}
		protected decimal? _VATNBG;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "VAT  (Repossession)")]
		public virtual decimal? VATNBG
		{
			get
			{
				return this._VATNBG;
			}
			set
			{
				this._VATNBG = value;
			}
		}
		#endregion
		#region ProvisionPct
		public abstract class provisionPct : PX.Data.IBqlField
		{
		}
		protected decimal? _ProvisionPct;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Provision %")]
		public virtual decimal? ProvisionPct
		{
			get
			{
				return this._ProvisionPct;
			}
			set
			{
				this._ProvisionPct = value;
			}
		}
		#endregion
		#region ProvisionInGel
		public abstract class provisionInGel : PX.Data.IBqlField
		{
		}
		protected decimal? _ProvisionInGel;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Provision in Gel")]
		public virtual decimal? ProvisionInGel
		{
			get
			{
				return this._ProvisionInGel;
			}
			set
			{
				this._ProvisionInGel = value;
			}
		}
		#endregion
		#region NBGWrittenOffProperty
		public abstract class nBGWrittenOffProperty : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGWrittenOffProperty;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Witten off Property NBG")]
		public virtual decimal? NBGWrittenOffProperty
		{
			get
			{
				return this._NBGWrittenOffProperty;
			}
			set
			{
				this._NBGWrittenOffProperty = value;
			}
		}
		#endregion
		#region NBGRepossessedAsset
		public abstract class nBGRepossessedAsset : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGRepossessedAsset;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Repossessed asset NBG")]
		public virtual decimal? NBGRepossessedAsset
		{
			get
			{
				return this._NBGRepossessedAsset;
			}
			set
			{
				this._NBGRepossessedAsset = value;
			}
		}
		#endregion
		#region NBGInvestmentProperty
		public abstract class nBGInvestmentProperty : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGInvestmentProperty;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Investment property NBG")]
		public virtual decimal? NBGInvestmentProperty
		{
			get
			{
				return this._NBGInvestmentProperty;
			}
			set
			{
				this._NBGInvestmentProperty = value;
			}
		}
		#endregion
		#region NBGInvestmentPropertyGrossLand
		public abstract class nBGInvestmentPropertyGrossLand : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGInvestmentPropertyGrossLand;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "NBG Investment property gross (land)")]
		public virtual decimal? NBGInvestmentPropertyGrossLand
		{
			get
			{
				return this._NBGInvestmentPropertyGrossLand;
			}
			set
			{
				this._NBGInvestmentPropertyGrossLand = value;
			}
		}
		#endregion
		#region NBGInvestmentPropertyGrossBuilding
		public abstract class nBGInvestmentPropertyGrossBuilding : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGInvestmentPropertyGrossBuilding;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "NBG Investment property gross (Building)")]
		public virtual decimal? NBGInvestmentPropertyGrossBuilding
		{
			get
			{
				return this._NBGInvestmentPropertyGrossBuilding;
			}
			set
			{
				this._NBGInvestmentPropertyGrossBuilding = value;
			}
		}
		#endregion
		#region IFRSBalanceValueGross
		public abstract class iFRSBalanceValueGross : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSBalanceValueGross;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Balance Value IFRS Gross ")]
		public virtual decimal? IFRSBalanceValueGross
		{
			get
			{
				return this._IFRSBalanceValueGross;
			}
			set
			{
				this._IFRSBalanceValueGross = value;
			}
		}
		#endregion
		#region IFRSBalanceValueNet
		public abstract class iFRSBalanceValueNet : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSBalanceValueNet;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Balance Value IFRS Net")]
		public virtual decimal? IFRSBalanceValueNet
		{
			get
			{
				return this._IFRSBalanceValueNet;
			}
			set
			{
				this._IFRSBalanceValueNet = value;
			}
		}
		#endregion
		#region VATIFRS
		public abstract class vATIFRS : PX.Data.IBqlField
		{
		}
		protected decimal? _VATIFRS;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "VAT  (Repossession)")]
		public virtual decimal? VATIFRS
		{
			get
			{
				return this._VATIFRS;
			}
			set
			{
				this._VATIFRS = value;
			}
		}
		#endregion
		#region IFRSInvestmentPropertyGrossLand
		public abstract class iFRSInvestmentPropertyGrossLand : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSInvestmentPropertyGrossLand;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "IFRS Investment property gross (land)")]
		public virtual decimal? IFRSInvestmentPropertyGrossLand
		{
			get
			{
				return this._IFRSInvestmentPropertyGrossLand;
			}
			set
			{
				this._IFRSInvestmentPropertyGrossLand = value;
			}
		}
		#endregion
		#region IFRSInvestmentPropertyGrossBuilding
		public abstract class iFRSInvestmentPropertyGrossBuilding : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSInvestmentPropertyGrossBuilding;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "IFRS Investment property gross (Building)")]
		public virtual decimal? IFRSInvestmentPropertyGrossBuilding
		{
			get
			{
				return this._IFRSInvestmentPropertyGrossBuilding;
			}
			set
			{
				this._IFRSInvestmentPropertyGrossBuilding = value;
			}
		}
		#endregion
		#region DepreciationCumulative
		public abstract class depreciationCumulative : PX.Data.IBqlField
		{
		}
		protected decimal? _DepreciationCumulative;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Depriciation Cumuative")]
		public virtual decimal? DepreciationCumulative
		{
			get
			{
				return this._DepreciationCumulative;
			}
			set
			{
				this._DepreciationCumulative = value;
			}
		}
		#endregion
		#region InvestmentPropertyGross
		public abstract class investmentPropertyGross : PX.Data.IBqlField
		{
		}
		protected decimal? _InvestmentPropertyGross;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Investment property gross")]
		public virtual decimal? InvestmentPropertyGross
		{
			get
			{
				return this._InvestmentPropertyGross;
			}
			set
			{
				this._InvestmentPropertyGross = value;
			}
		}
		#endregion
		#region IFRSWriteDown
		public abstract class iFRSWriteDown : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSWriteDown;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "IFRS write down")]
		public virtual decimal? IFRSWriteDown
		{
			get
			{
				return this._IFRSWriteDown;
			}
			set
			{
				this._IFRSWriteDown = value;
			}
		}
		#endregion
		#region IFRSAppreciation
		public abstract class iFRSAppreciation : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSAppreciation;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "IFRS appreciation")]
		public virtual decimal? IFRSAppreciation
		{
			get
			{
				return this._IFRSAppreciation;
			}
			set
			{
				this._IFRSAppreciation = value;
			}
		}
		#endregion
	}
}

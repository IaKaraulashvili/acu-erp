﻿﻿namespace RA
{
    using System;

    using PX.Data;
    using PX.Objects.CS;
    using _Shared;
    using PX.Objects.CM;
    using PX.Objects.GL;
    using _Shared.LoanStatus;

    [PXPrimaryGraph(typeof(RALoanEntry))]
    [System.SerializableAttribute()]
	public class RALoan : PX.Data.IBqlTable
	{
		#region LoanID
		public abstract class loanID : PX.Data.IBqlField
		{
		}
		protected int? _LoanID;
		[PXDBIdentity]
		[PXUIField(Enabled = false)]
		public virtual int? LoanID
		{
			get
			{
				return this._LoanID;
			}
			set
			{
				this._LoanID = value;
			}
		}
		#endregion
		#region LoanCD
		public abstract class loanCD : PX.Data.IBqlField
		{
		}
		protected string _LoanCD;
		[PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
		[PXUIField(DisplayName = "Loan ID")]
        [RALoanSelector]
        [AutoNumber(typeof(RASetup.loanNumberingID), typeof(RALoan.createdDateTime))]
        public virtual string LoanCD
		{
			get
			{
				return this._LoanCD;
			}
			set
			{
				this._LoanCD = value;
			}
		}
		#endregion
		#region LMSID
		public abstract class lMSID : PX.Data.IBqlField
		{
		}
		protected string _LMSID;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "LMS ID")]
		public virtual string LMSID
		{
			get
			{
				return this._LMSID;
			}
			set
			{
				this._LMSID = value;
			}
		}
		#endregion
		#region LoanProduct
		public abstract class loanProduct : PX.Data.IBqlField
		{
		}
		protected string _LoanProduct;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Loan Product")]
		public virtual string LoanProduct
		{
			get
			{
				return this._LoanProduct;
			}
			set
			{
				this._LoanProduct = value;
			}
		}
		#endregion
		#region LoanSegment
		public abstract class loanSegment : PX.Data.IBqlField
		{
		}
		protected string _LoanSegment;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Loan Segment")]
		public virtual string LoanSegment
		{
			get
			{
				return this._LoanSegment;
			}
			set
			{
				this._LoanSegment = value;
			}
		}
		#endregion
		#region LoanEndDate
		public abstract class loanEndDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _LoanEndDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Loan End Date")]
		public virtual DateTime? LoanEndDate
		{
			get
			{
				return this._LoanEndDate;
			}
			set
			{
				this._LoanEndDate = value;
			}
		}
		#endregion
		#region LoanDisbursementDate
		public abstract class loanDisbursementDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _LoanDisbursementDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Loan Disbursement Date")]
		public virtual DateTime? LoanDisbursementDate
		{
			get
			{
				return this._LoanDisbursementDate;
			}
			set
			{
				this._LoanDisbursementDate = value;
			}
		}
		#endregion
		#region LoanTransferDateToPLO
		public abstract class loanTransferDateToPLO : PX.Data.IBqlField
		{
		}
		protected DateTime? _LoanTransferDateToPLO;
		[PXDBDate()]
		[PXUIField(DisplayName = "Loan Transfer Date to PLO")]
		public virtual DateTime? LoanTransferDateToPLO
		{
			get
			{
				return this._LoanTransferDateToPLO;
			}
			set
			{
				this._LoanTransferDateToPLO = value;
			}
		}
        #endregion
        #region LoanStatus
        public abstract class loanStatus : PX.Data.IBqlField
        {
        }
        protected string _LoanStatus;
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Loan Status")]
        [PXDefault(_Shared.LoanStatus.LoanStatus.Active)]
        [LoanStatusListAttribute]
        public virtual string LoanStatus
        {
            get
            {
                return this._LoanStatus;
            }
            set
            {
                this._LoanStatus = value;
            }
        }
        #endregion
        #region ClientID
        public abstract class clientID : PX.Data.IBqlField
		{
		}
		protected int? _ClientID;
		[PXDBInt()]
		[PXUIField(DisplayName = "ClientID")]
		public virtual int? ClientID
		{
			get
			{
				return this._ClientID;
			}
			set
			{
				this._ClientID = value;
			}
		}
		#endregion
		#region BorrowerName
		public abstract class borrowerName : PX.Data.IBqlField
		{
		}
		protected string _BorrowerName;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Borrower Name")]
		public virtual string BorrowerName
		{
			get
			{
				return this._BorrowerName;
			}
			set
			{
				this._BorrowerName = value;
			}
		}
		#endregion
		#region BorrowerID
		public abstract class borrowerID : PX.Data.IBqlField
		{
		}
		protected string _BorrowerID;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Borrower ID")]
		public virtual string BorrowerID
		{
			get
			{
				return this._BorrowerID;
			}
			set
			{
				this._BorrowerID = value;
			}
		}
		#endregion
		#region BorrowerContactInfo
		public abstract class borrowerContactInfo : PX.Data.IBqlField
		{
		}
		protected string _BorrowerContactInfo;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Borrower Contact Info")]
		public virtual string BorrowerContactInfo
		{
			get
			{
				return this._BorrowerContactInfo;
			}
			set
			{
				this._BorrowerContactInfo = value;
			}
		}
		#endregion
		#region BorrowerInsider
		public abstract class borrowerInsider : PX.Data.IBqlField
		{
		}
		protected bool? _BorrowerInsider;
		[PXDBBool()]
		[PXUIField(DisplayName = "Borrower Insider")]
		public virtual bool? BorrowerInsider
		{
			get
			{
				return this._BorrowerInsider;
			}
			set
			{
				this._BorrowerInsider = value;
			}
		}
		#endregion
		#region CoBorrowerName
		public abstract class coBorrowerName : PX.Data.IBqlField
		{
		}
		protected string _CoBorrowerName;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Co-Borrower Name")]
		public virtual string CoBorrowerName
		{
			get
			{
				return this._CoBorrowerName;
			}
			set
			{
				this._CoBorrowerName = value;
			}
		}
		#endregion
		#region CoBorrowerID
		public abstract class coBorrowerID : PX.Data.IBqlField
		{
		}
		protected string _CoBorrowerID;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Co-Borrower ID")]
		public virtual string CoBorrowerID
		{
			get
			{
				return this._CoBorrowerID;
			}
			set
			{
				this._CoBorrowerID = value;
			}
		}
		#endregion
		#region CoBorrowerInsider
		public abstract class coBorrowerInsider : PX.Data.IBqlField
		{
		}
		protected bool? _CoBorrowerInsider;
		[PXDBBool()]
		[PXUIField(DisplayName = "Co-Borrower Insider")]
		public virtual bool? CoBorrowerInsider
		{
			get
			{
				return this._CoBorrowerInsider;
			}
			set
			{
				this._CoBorrowerInsider = value;
			}
		}
		#endregion
		#region GuarantorName1
		public abstract class guarantorName1 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorName1;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor Name 1")]
		public virtual string GuarantorName1
		{
			get
			{
				return this._GuarantorName1;
			}
			set
			{
				this._GuarantorName1 = value;
			}
		}
		#endregion
		#region GuarantorID1
		public abstract class guarantorID1 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorID1;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor ID 1")]
		public virtual string GuarantorID1
		{
			get
			{
				return this._GuarantorID1;
			}
			set
			{
				this._GuarantorID1 = value;
			}
		}
		#endregion
		#region GuarantorInsider1
		public abstract class guarantorInsider1 : PX.Data.IBqlField
		{
		}
		protected bool? _GuarantorInsider1;
		[PXDBBool()]
		[PXUIField(DisplayName = "Guarantor 1 Insider")]
		public virtual bool? GuarantorInsider1
		{
			get
			{
				return this._GuarantorInsider1;
			}
			set
			{
				this._GuarantorInsider1 = value;
			}
		}
		#endregion
		#region GuarantorName2
		public abstract class guarantorName2 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorName2;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor Name 2")]
		public virtual string GuarantorName2
		{
			get
			{
				return this._GuarantorName2;
			}
			set
			{
				this._GuarantorName2 = value;
			}
		}
		#endregion
		#region GuarantorID2
		public abstract class guarantorID2 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorID2;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor ID 2")]
		public virtual string GuarantorID2
		{
			get
			{
				return this._GuarantorID2;
			}
			set
			{
				this._GuarantorID2 = value;
			}
		}
		#endregion
		#region GuarantorInsider2
		public abstract class guarantorInsider2 : PX.Data.IBqlField
		{
		}
		protected bool? _GuarantorInsider2;
		[PXDBBool()]
		[PXUIField(DisplayName = "Guarantor 2 Insider")]
		public virtual bool? GuarantorInsider2
		{
			get
			{
				return this._GuarantorInsider2;
			}
			set
			{
				this._GuarantorInsider2 = value;
			}
		}
		#endregion
		#region GuarantorName3
		public abstract class guarantorName3 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorName3;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor Name 3")]
		public virtual string GuarantorName3
		{
			get
			{
				return this._GuarantorName3;
			}
			set
			{
				this._GuarantorName3 = value;
			}
		}
		#endregion
		#region GuarantorID3
		public abstract class guarantorID3 : PX.Data.IBqlField
		{
		}
		protected string _GuarantorID3;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Guarantor ID 3")]
		public virtual string GuarantorID3
		{
			get
			{
				return this._GuarantorID3;
			}
			set
			{
				this._GuarantorID3 = value;
			}
		}
		#endregion
		#region GuarantorInsider3
		public abstract class guarantorInsider3 : PX.Data.IBqlField
		{
		}
		protected bool? _GuarantorInsider3;
		[PXDBBool()]
		[PXUIField(DisplayName = "Guarantor 3 Insider")]
		public virtual bool? GuarantorInsider3
		{
			get
			{
				return this._GuarantorInsider3;
			}
			set
			{
				this._GuarantorInsider3 = value;
			}
		}
		#endregion
		#region OverdueDays
		public abstract class overdueDays : PX.Data.IBqlField
		{
		}
		protected int? _OverdueDays;
		[PXDBInt()]
		[PXUIField(DisplayName = "Overdue Days")]
		public virtual int? OverdueDays
		{
			get
			{
				return this._OverdueDays;
			}
			set
			{
				this._OverdueDays = value;
			}
		}
		#endregion
		#region OverduePrincipleCury
		public abstract class overduePrincipleCury : PX.Data.IBqlField
		{
		}
		protected decimal? _OverduePrincipleCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RALoan.overduePrinciple))]
        [PXUIField(DisplayName = "Overdue Principle", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual decimal? OverduePrincipleCury
		{
			get
			{
				return this._OverduePrincipleCury;
			}
			set
			{
				this._OverduePrincipleCury = value;
			}
		}
		#endregion
		#region OverduePenaltyCury
		public abstract class overduePenaltyCury : PX.Data.IBqlField
		{
		}
		protected decimal? _OverduePenaltyCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RALoan.overduePenalty))]
		[PXUIField(DisplayName = "Overdue Penalty", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual decimal? OverduePenaltyCury
		{
			get
			{
				return this._OverduePenaltyCury;
			}
			set
			{
				this._OverduePenaltyCury = value;
			}
		}
		#endregion
		#region OverdueInsuranceCury
		public abstract class overdueInsuranceCury : PX.Data.IBqlField
		{
		}
		protected decimal? _OverdueInsuranceCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RALoan.overduePenalty))]
        [PXUIField(DisplayName = "Overdue Insurance", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual decimal? OverdueInsuranceCury
		{
			get
			{
				return this._OverdueInsuranceCury;
			}
			set
			{
				this._OverdueInsuranceCury = value;
			}
		}
		#endregion
		#region OverdueInterestCury
		public abstract class overdueInterestCury : PX.Data.IBqlField
		{
		}
		protected decimal? _OverdueInterestCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RALoan.overdueInterest))]
        [PXUIField(DisplayName = "Overdue Interest", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual decimal? OverdueInterestCury
		{
			get
			{
				return this._OverdueInterestCury;
			}
			set
			{
				this._OverdueInterestCury = value;
			}
		}
		#endregion
		#region OverduePrinciple
		public abstract class overduePrinciple : PX.Data.IBqlField
		{
		}
		protected decimal? _OverduePrinciple;
        [PXDBBaseCury()]
        [PXDefault(TypeCode.Decimal, "0.0")]
        public virtual decimal? OverduePrinciple
		{
			get
			{
				return this._OverduePrinciple;
			}
			set
			{
				this._OverduePrinciple = value;
			}
		}
		#endregion
		#region OverduePenalty
		public abstract class overduePenalty : PX.Data.IBqlField
		{
		}
		protected decimal? _OverduePenalty;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Overdue Penalty")]
		public virtual decimal? OverduePenalty
		{
			get
			{
				return this._OverduePenalty;
			}
			set
			{
				this._OverduePenalty = value;
			}
        }
		#endregion
        #region CurrencyDate
        public abstract class currencyDate : PX.Data.IBqlField
        {
        }
        protected DateTime? _CurrencyDate;
        [PXDBDate()]
        [PXUIField(DisplayName = "Currency Date")]
        public virtual DateTime? CurrencyDate
        {
            get
            {
                return this._CurrencyDate;
            }
            set
            {
                this._CurrencyDate = value;
            }
        }
        #endregion
        #region CuryInfoID
        public abstract class curyInfoID : PX.Data.IBqlField
        {
        }
        protected Int64? _CuryInfoID;

        /// <summary>
        /// The identifier of the <see cref="CurrencyInfo">CurrencyInfo</see> object associated with the document.
        /// </summary>
        /// <value>
        /// Corresponds to the <see cref="CurrencyInfoID"/> field.
        /// </value>
        [PXDBLong()]
        [CurrencyInfo(ModuleCode = "RA")]
        public virtual Int64? CuryInfoID
        {
            get
            {
                return this._CuryInfoID;
            }
            set
            {
                this._CuryInfoID = value;
            }

        }
        #endregion
		#region OverdueInsurance
		public abstract class overdueInsurance : PX.Data.IBqlField
		{
		}
		protected decimal? _OverdueInsurance;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Overdue Insurance")]
		public virtual decimal? OverdueInsurance
		{
			get
			{
				return this._OverdueInsurance;
			}
			set
			{
				this._OverdueInsurance = value;
			}
		}
		#endregion
		#region OverdueInterest
		public abstract class overdueInterest : PX.Data.IBqlField
		{
		}
		protected decimal? _OverdueInterest;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Overdue Interest")]
		public virtual decimal? OverdueInterest
		{
			get
			{
				return this._OverdueInterest;
			}
			set
			{
				this._OverdueInterest = value;
			}
		}
        #endregion
        #region CuryID
        public abstract class curyID : PX.Data.IBqlField
		{
		}
		protected string _CuryID;
        [PXDBString(5, IsUnicode = true, InputMask = ">LLLLL")]
        [PXUIField(DisplayName = "Loan Currency", Visibility = PXUIVisibility.SelectorVisible)]
        [PXDefault(typeof(Search<Company.baseCuryID>))]
        [PXSelector(typeof(Currency.curyID))]
        public virtual string CuryID
		{
			get
			{
				return this._CuryID;
			}
			set
			{
				this._CuryID = value;
			}
		}
		#endregion
		#region LoanAmtCury
		public abstract class loanAmtCury : PX.Data.IBqlField
		{
		}
		protected decimal? _LoanAmtCury;
        [PXDefault(TypeCode.Decimal, "0.0")]
        [PXDBCurrency(typeof(RALoan.curyInfoID), typeof(RALoan.loanAmtGel))]
        [PXUIField(DisplayName = "Loan Amount", Visibility = PXUIVisibility.SelectorVisible)]
		public virtual decimal? LoanAmtCury
		{
			get
			{
				return this._LoanAmtCury;
			}
			set
			{
				this._LoanAmtCury = value;
			}
		}
		#endregion
		#region LoanAmtGel
		public abstract class loanAmtGel : PX.Data.IBqlField
		{
		}
		protected decimal? _LoanAmtGel;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Loan Amount in GEL")]
		public virtual decimal? LoanAmtGel
		{
			get
			{
				return this._LoanAmtGel;
			}
			set
			{
				this._LoanAmtGel = value;
			}
		}
		#endregion
		#region NBGLoanProvisionRate
		public abstract class nBGLoanProvisionRate : PX.Data.IBqlField
		{
		}
		protected decimal? _NBGLoanProvisionRate;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Loan Provision Rate (NBG)")]
		public virtual decimal? NBGLoanProvisionRate
		{
			get
			{
				return this._NBGLoanProvisionRate;
			}
			set
			{
				this._NBGLoanProvisionRate = value;
			}
		}
		#endregion
		#region IFRSLoanProvisionRate
		public abstract class iFRSLoanProvisionRate : PX.Data.IBqlField
		{
		}
		protected decimal? _IFRSLoanProvisionRate;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Loan Provision Rate (IFRS)")]
		public virtual decimal? IFRSLoanProvisionRate
		{
			get
			{
				return this._IFRSLoanProvisionRate;
			}
			set
			{
				this._IFRSLoanProvisionRate = value;
			}
		}
		#endregion
		#region LineCntr
		public abstract class lineCntr : PX.Data.IBqlField
		{
		}
		protected int? _LineCntr;
		[PXDBInt()]
		[PXDefault(0)]
		[PXUIField(DisplayName = "LineCntr")]
		public virtual int? LineCntr
		{
			get
			{
				return this._LineCntr;
			}
			set
			{
				this._LineCntr = value;
			}
		}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote()]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion


        #region BorrowerLMSID
        public abstract class borrowerLMSID : PX.Data.IBqlField
        {
        }
        protected string _BorrowerLMSID;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "Borrower LMS ID")]
        public virtual string BorrowerLMSID
        {
            get
            {
                return this._BorrowerLMSID;
            }
            set
            {
                this._BorrowerLMSID = value;
            }
        }
        #endregion
        #region CoBorrowerLMSID
        public abstract class coBorrowerLMSID : PX.Data.IBqlField
        {
        }
        protected string _CoBorrowerLMSID;
        [PXDBString(50, IsUnicode = true)]
        [PXUIField(DisplayName = "CoBorrower LMS ID")]
        public virtual string CoBorrowerLMSID
        {
            get
            {
                return this._CoBorrowerLMSID;
            }
            set
            {
                this._CoBorrowerLMSID = value;
            }
        }
        #endregion
        #region Interest
        public abstract class interest : PX.Data.IBqlField
        {
        }
        protected decimal? _Interest;
        [PXDBDecimal(2)]
        //[PXDefault(TypeCode.Decimal,"0.0")]
        [PXUIField(DisplayName = "Interest")]
        public virtual decimal? Interest
        {
            get
            {
                return this._Interest;
            }
            set
            {
                this._Interest = value;
            }
        }
        #endregion
        #region ContractNumber
        public abstract class contractNumber : PX.Data.IBqlField
        {
        }
        protected string _ContractNumber;
        [PXDBString(25, IsUnicode = true)]
        [PXUIField(DisplayName = "Contract Number")]
        public virtual string ContractNumber
        {
            get
            {
                return this._ContractNumber;
            }
            set
            {
                this._ContractNumber = value;
            }
        }
        #endregion
    }
}

﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using RA._Shared.VehicleCondition;
    using RA._Shared.Transmission;
    using RA._Shared.WheelLocation;
    using _Shared;

    [System.SerializableAttribute()]
	public class RANonRealEstate : PX.Data.IBqlTable
	{
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
		[PXDBInt(IsKey = true)]
		[PXDBDefault(typeof(RAAsset.assetID))]
		[PXParent(typeof(Select<RAAsset, Where<RAAsset.assetID, Equal<Current<RANonRealEstate.assetID>>>>))]
		public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region VINCode
		public abstract class vINCode : PX.Data.IBqlField
		{
		}
		protected string _VINCode;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "VIN Code")]
		public virtual string VINCode
		{
			get
			{
				return this._VINCode;
			}
			set
			{
				this._VINCode = value;
			}
		}
		#endregion
		#region ChassisID
		public abstract class chassisID : PX.Data.IBqlField
		{
		}
		protected string _ChassisID;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Chassis ID")]
		public virtual string ChassisID
		{
			get
			{
				return this._ChassisID;
			}
			set
			{
				this._ChassisID = value;
			}
		}
		#endregion
		#region TechPassportNumber
		public abstract class techPassportNumber : PX.Data.IBqlField
		{
		}
		protected string _TechPassportNumber;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Tech-passport number")]
		public virtual string TechPassportNumber
		{
			get
			{
				return this._TechPassportNumber;
			}
			set
			{
				this._TechPassportNumber = value;
			}
		}
		#endregion
		#region PlateNumber
		public abstract class plateNumber : PX.Data.IBqlField
		{
		}
		protected string _PlateNumber;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Plate Number")]
		public virtual string PlateNumber
		{
			get
			{
				return this._PlateNumber;
			}
			set
			{
				this._PlateNumber = value;
			}
		}
		#endregion
		#region Mark
		public abstract class mark : PX.Data.IBqlField
		{
		}
		protected string _Mark;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Mark")]
		public virtual string Mark
		{
			get
			{
				return this._Mark;
			}
			set
			{
				this._Mark = value;
			}
		}
		#endregion
		#region AutomotiveBodyType
		public abstract class automotiveBodyType : PX.Data.IBqlField
		{
		}
		protected string _AutomotiveBodyType;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Automotive Body Type")]
		public virtual string AutomotiveBodyType
		{
			get
			{
				return this._AutomotiveBodyType;
			}
			set
			{
				this._AutomotiveBodyType = value;
			}
		}
		#endregion
		#region Color
		public abstract class color : PX.Data.IBqlField
		{
		}
		protected string _Color;
		[PXDBString(20, IsUnicode = true)]
		[PXUIField(DisplayName = "Color")]
		public virtual string Color
		{
			get
			{
				return this._Color;
			}
			set
			{
				this._Color = value;
			}
		}
		#endregion
		#region RegistrationDate
		public abstract class registrationDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _RegistrationDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Registration Date")]
		public virtual DateTime? RegistrationDate
		{
			get
			{
				return this._RegistrationDate;
			}
			set
			{
				this._RegistrationDate = value;
			}
		}
		#endregion
		#region EngineVolume
		public abstract class engineVolume : PX.Data.IBqlField
		{
		}
		protected decimal? _EngineVolume;
		[PXDBDecimal(1)]
		[PXUIField(DisplayName = "Engine Volume")]
		public virtual decimal? EngineVolume
		{
			get
			{
				return this._EngineVolume;
			}
			set
			{
				this._EngineVolume = value;
			}
		}
		#endregion
		#region Model
		public abstract class model : PX.Data.IBqlField
		{
		}
		protected string _Model;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Model")]
		public virtual string Model
		{
			get
			{
				return this._Model;
			}
			set
			{
				this._Model = value;
			}
		}
		#endregion
		#region VehicleCondition
		public abstract class vehicleCondition : PX.Data.IBqlField
		{
		}
		protected string _VehicleCondition;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Vehicle Condition")]
        [VehicleConditionList]
		public virtual string VehicleCondition
		{
			get
			{
				return this._VehicleCondition;
			}
			set
			{
				this._VehicleCondition = value;
			}
		}
		#endregion
		#region CustomsDuty
		public abstract class customsDuty : PX.Data.IBqlField
		{
		}
		protected String _CustomsDuty;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Customs Duty")]
		public virtual String CustomsDuty
		{
			get
			{
				return this._CustomsDuty;
			}
			set
			{
				this._CustomsDuty = value;
			}
		}
		#endregion
		#region Transmission
		public abstract class transmission : PX.Data.IBqlField
		{
		}
		protected string _Transmission;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Transmission")]
        [TransmissionList]
		public virtual string Transmission
		{
			get
			{
				return this._Transmission;
			}
			set
			{
				this._Transmission = value;
			}
		}
		#endregion
		#region Mileage
		public abstract class mileage : PX.Data.IBqlField
		{
		}
		protected int? _Mileage;
		[PXDBInt]
		[PXUIField(DisplayName = "Mileage")]
		public virtual int? Mileage
		{
			get
			{
				return this._Mileage;
			}
			set
			{
				this._Mileage = value;
			}
		}
		#endregion
		#region WheelLocation
		public abstract class wheelLocation : PX.Data.IBqlField
		{
		}
		protected string _WheelLocation;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Wheel Location")]
        [WheelLocationList]
		public virtual string WheelLocation
		{
			get
			{
				return this._WheelLocation;
			}
			set
			{
				this._WheelLocation = value;
			}
		}
		#endregion
		#region Purpose
		public abstract class purpose : PX.Data.IBqlField
		{
		}
		protected string _Purpose;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Purpose")]
		public virtual string Purpose
		{
			get
			{
				return this._Purpose;
			}
			set
			{
				this._Purpose = value;
			}
		}
		#endregion
		#region IssueDate
		public abstract class issueDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _IssueDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Issue Date")]
		public virtual DateTime? IssueDate
		{
			get
			{
				return this._IssueDate;
			}
			set
			{
				this._IssueDate = value;
			}
		}
		#endregion
		#region Parameters
		public abstract class parameters : PX.Data.IBqlField
		{
		}
		protected string _Parameters;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Parameters")]
		public virtual string Parameters
		{
			get
			{
				return this._Parameters;
			}
			set
			{
				this._Parameters = value;
			}
		}
		#endregion
		#region PropertyDescription
		public abstract class propertyDescription : PX.Data.IBqlField
		{
		}
		protected string _PropertyDescription;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Property Description")]
		public virtual string PropertyDescription
		{
			get
			{
				return this._PropertyDescription;
			}
			set
			{
				this._PropertyDescription = value;
			}
		}
		#endregion
		#region SerialNumber
		public abstract class serialNumber : PX.Data.IBqlField
		{
		}
		protected string _SerialNumber;
		[PXDBString(25, IsUnicode = true)]
		[PXUIField(DisplayName = "Serial Number")]
		public virtual string SerialNumber
		{
			get
			{
				return this._SerialNumber;
			}
			set
			{
				this._SerialNumber = value;
			}
		}
		#endregion
		#region PropertyType
		public abstract class propertyType : PX.Data.IBqlField
		{
		}
		protected string _PropertyType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "PropertyType")]
		public virtual string PropertyType
		{
			get
			{
				return this._PropertyType;
			}
			set
			{
				this._PropertyType = value;
			}
		}
		#endregion
	}
}

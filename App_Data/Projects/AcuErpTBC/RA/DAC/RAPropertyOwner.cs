﻿﻿namespace RA
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class RAPropertyOwner : PX.Data.IBqlTable
	{
		#region PropertyOwnID
		public abstract class propertyOwnID : PX.Data.IBqlField
		{
		}
		protected int? _PropertyOwnID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false,Visible =false)]
        public virtual int? PropertyOwnID
		{
			get
			{
				return this._PropertyOwnID;
			}
			set
			{
				this._PropertyOwnID = value;
			}
		}
		#endregion
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RAAsset.assetID))]
        [PXParent(typeof(Select<RAAsset,
        Where<RAAsset.assetID, Equal<Current<RAPropertyOwner.assetID>>>>))]
        public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region LineNbr
		public abstract class lineNbr : PX.Data.IBqlField
		{
		}
		protected int? _LineNbr;
        [PXDBInt(IsKey =true)]
        [PXLineNbr(typeof(RAAsset.lineCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? LineNbr
		{
			get
			{
				return this._LineNbr;
			}
			set
			{
				this._LineNbr = value;
			}
		}
		#endregion
		#region PropertyOwner
		public abstract class propertyOwner : PX.Data.IBqlField
		{
		}
		protected string _PropertyOwner;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Property Owner")]
		public virtual string PropertyOwner
		{
			get
			{
				return this._PropertyOwner;
			}
			set
			{
				this._PropertyOwner = value;
			}
		}
		#endregion
		#region PropertyOwnerID
		public abstract class propertyOwnerID : PX.Data.IBqlField
		{
		}
		protected string _PropertyOwnerID;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Property Owner ID")]
		public virtual string PropertyOwnerID
		{
			get
			{
				return this._PropertyOwnerID;
			}
			set
			{
				this._PropertyOwnerID = value;
			}
		}
		#endregion
		#region Insider
		public abstract class insider : PX.Data.IBqlField
		{
		}
		protected bool? _Insider;
		[PXDBBool()]
		[PXUIField(DisplayName = "Insider")]
		public virtual bool? Insider
		{
			get
			{
				return this._Insider;
			}
			set
			{
				this._Insider = value;
			}
		}
		#endregion
		#region PORelationshipToClient
		public abstract class pORelationshipToClient : PX.Data.IBqlField
		{
		}
		protected string _PORelationshipToClient;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "PO Relationship To Client")]
		public virtual string PORelationshipToClient
		{
			get
			{
				return this._PORelationshipToClient;
			}
			set
			{
				this._PORelationshipToClient = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using _Shared.RegionType;
    using _Shared.TbilisiDistrict;
    using _Shared.InformationSource;
    using _Shared.RoadType;
    using _Shared.Coastline;
    using _Shared.AreaCondition;
    using _Shared.StatusOfConstruction;
    using _Shared.ConstructionDate;
    using PX.Objects.CM;
    using _Shared.ApartmentType;
    using _Shared.GeometryOfLandArea;
    using _Shared.Relief;
    using _Shared.FunctionalArea;
    using _Shared.ViticultureMicrozone;
    using _Shared;

    [System.SerializableAttribute()]
	public class RARealEstate : PX.Data.IBqlTable
	{
		#region AssetID
		public abstract class assetID : PX.Data.IBqlField
		{
		}
		protected int? _AssetID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RAAsset.assetID))]
        [PXParent(typeof(Select<RAAsset,
        Where<RAAsset.assetID, Equal<Current<RARealEstate.assetID>>>>))]
        public virtual int? AssetID
		{
			get
			{
				return this._AssetID;
			}
			set
			{
				this._AssetID = value;
			}
		}
		#endregion
		#region CadastralCode
		public abstract class cadastralCode : PX.Data.IBqlField
		{
		}
		protected string _CadastralCode;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Cadastral Code")]
		public virtual string CadastralCode
		{
			get
			{
				return this._CadastralCode;
			}
			set
			{
				this._CadastralCode = value;
			}
		}
		#endregion
		#region LandAreaDefined
		public abstract class landAreaDefined : PX.Data.IBqlField
		{
		}
		protected decimal? _LandAreaDefined;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Land Area Defined")]
		public virtual decimal? LandAreaDefined
		{
			get
			{
				return this._LandAreaDefined;
			}
			set
			{
				this._LandAreaDefined = value;
			}
		}
		#endregion
		#region LandAreaUndefined
		public abstract class landAreaUndefined : PX.Data.IBqlField
		{
		}
		protected decimal? _LandAreaUndefined;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Land Area Undefined")]
		public virtual decimal? LandAreaUndefined
		{
			get
			{
				return this._LandAreaUndefined;
			}
			set
			{
				this._LandAreaUndefined = value;
			}
		}
		#endregion
		#region UnitOfMeasure
		public abstract class unitOfMeasure : PX.Data.IBqlField
		{
		}
		protected string _UnitOfMeasure;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Unit of Measure")]
		public virtual string UnitOfMeasure
		{
			get
			{
				return this._UnitOfMeasure;
			}
			set
			{
				this._UnitOfMeasure = value;
			}
		}
		#endregion
		#region LandType
		public abstract class landType : PX.Data.IBqlField
		{
		}
		protected string _LandType;
		[PXDBString(20, IsUnicode = true)]
		[PXUIField(DisplayName = "Land Type")]
		public virtual string LandType
		{
			get
			{
				return this._LandType;
			}
			set
			{
				this._LandType = value;
			}
		}
		#endregion
		#region LandByPurpose
		public abstract class landByPurpose : PX.Data.IBqlField
		{
		}
		protected string _LandByPurpose;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Land By Purpose")]
		public virtual string LandByPurpose
		{
			get
			{
				return this._LandByPurpose;
			}
			set
			{
				this._LandByPurpose = value;
			}
		}
		#endregion
		#region MainEntrance
		public abstract class mainEntrance : PX.Data.IBqlField
		{
		}
		protected int? _MainEntrance;
		[PXDBInt()]
		[PXUIField(DisplayName = "Main Entrance")]
		public virtual int? MainEntrance
		{
			get
			{
				return this._MainEntrance;
			}
			set
			{
				this._MainEntrance = value;
			}
		}
		#endregion
		#region FlatNumber
		public abstract class flatNumber : PX.Data.IBqlField
		{
		}
		protected int? _FlatNumber;
		[PXDBInt()]
		[PXUIField(DisplayName = "Flat Nbr")]
		public virtual int? FlatNumber
		{
			get
			{
				return this._FlatNumber;
			}
			set
			{
				this._FlatNumber = value;
			}
		}
		#endregion
		#region Area
		public abstract class area : PX.Data.IBqlField
		{
		}
		protected decimal? _Area;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Area (sq.m)")]
		public virtual decimal? Area
		{
			get
			{
				return this._Area;
			}
			set
			{
				this._Area = value;
			}
		}
		#endregion
		#region Condition
		public abstract class condition : PX.Data.IBqlField
		{
		}
		protected string _Condition;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Condition")]
		public virtual string Condition
		{
			get
			{
				return this._Condition;
			}
			set
			{
				this._Condition = value;
			}
		}
		#endregion
		#region Region
		public abstract class region : PX.Data.IBqlField
		{
		}
		protected string _Region;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Region")]
        [RegionTypeListAttribute]
        public virtual string Region
		{
			get
			{
				return this._Region;
			}
			set
			{
				this._Region = value;
			}
		}
		#endregion
		#region District
		public abstract class tbilisiDistrict : PX.Data.IBqlField
		{
		}
		protected string _TbilisiDistrict;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Tbilisi District")]
        [TbilisiDistrictListAttribute]
		public virtual string TbilisiDistrict
        {
			get
			{
				return this._TbilisiDistrict;
			}
			set
			{
				this._TbilisiDistrict = value;
			}
		}
		#endregion
		#region Address
		public abstract class address : PX.Data.IBqlField
		{
		}
		protected string _Address;
		[PXDBString(250, IsUnicode = true)]
		[PXUIField(DisplayName = "Address")]
		public virtual string Address
		{
			get
			{
				return this._Address;
			}
			set
			{
				this._Address = value;
			}
		}
		#endregion
		#region InformationSource
		public abstract class informationSource : PX.Data.IBqlField
		{
		}
		protected string _InformationSource;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Information Source")]
        [InformationSourceListAttribute]
		public virtual string InformationSource
		{
			get
			{
				return this._InformationSource;
			}
			set
			{
				this._InformationSource = value;
			}
		}
		#endregion
		#region Water
		public abstract class water : PX.Data.IBqlField
		{
		}
		protected String _Water;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Water")]
		public virtual String Water
		{
			get
			{
				return this._Water;
			}
			set
			{
				this._Water = value;
			}
		}
		#endregion
		#region IrrigationSystem
		public abstract class irrigationSystem : PX.Data.IBqlField
		{
		}
		protected String _IrrigationSystem;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Irrigation System")]
		public virtual String IrrigationSystem
		{
			get
			{
				return this._IrrigationSystem;
			}
			set
			{
				this._IrrigationSystem = value;
			}
		}
		#endregion
		#region NaturalGas
		public abstract class naturalGas : PX.Data.IBqlField
		{
		}
		protected String _NaturalGas;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Natural Gas")]
		public virtual String NaturalGas
		{
			get
			{
				return this._NaturalGas;
			}
			set
			{
				this._NaturalGas = value;
			}
		}
		#endregion
		#region Fencing
		public abstract class fencing : PX.Data.IBqlField
		{
		}
		protected String _Fencing;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Fencing")]
		public virtual String Fencing
		{
			get
			{
				return this._Fencing;
			}
			set
			{
				this._Fencing = value;
			}
		}
		#endregion
		#region Electricity
		public abstract class electricity : PX.Data.IBqlField
		{
		}
		protected String _Electricity;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Electricity")]
		public virtual String Electricity
		{
			get
			{
				return this._Electricity;
			}
			set
			{
				this._Electricity = value;
			}
		}
		#endregion
		#region SewerageSystem
		public abstract class sewerageSystem : PX.Data.IBqlField
		{
		}
		protected String _SewerageSystem;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Sewerage System")]
		public virtual String SewerageSystem
		{
			get
			{
				return this._SewerageSystem;
			}
			set
			{
				this._SewerageSystem = value;
			}
		}
		#endregion
		#region RoadType
		public abstract class roadType : PX.Data.IBqlField
		{
		}
		protected string _RoadType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Road Type")]
        [RoadTypeListAttribute]
		public virtual string RoadType
		{
			get
			{
				return this._RoadType;
			}
			set
			{
				this._RoadType = value;
			}
		}
		#endregion
		#region Coastline
		public abstract class coastline : PX.Data.IBqlField
		{
		}
		protected int? _Coastline;
		[PXDBInt()]
		[PXUIField(DisplayName = "Coastline")]
        [CoastlineListAttribute]
		public virtual int? Coastline
		{
			get
			{
				return this._Coastline;
			}
			set
			{
				this._Coastline = value;
			}
		}
		#endregion
		#region AreaCondition
		public abstract class areaCondition : PX.Data.IBqlField
		{
		}
		protected string _AreaCondition;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Area Condition")]
        [AreaConditionListAttribute]
		public virtual string AreaCondition
		{
			get
			{
				return this._AreaCondition;
			}
			set
			{
				this._AreaCondition = value;
			}
		}
		#endregion
		#region RoomQuantity
		public abstract class roomQuantity : PX.Data.IBqlField
		{
		}
		protected int? _RoomQuantity;
		[PXDBInt()]
		[PXUIField(DisplayName = "Room Quantity")]
		public virtual int? RoomQuantity
		{
			get
			{
				return this._RoomQuantity;
			}
			set
			{
				this._RoomQuantity = value;
			}
		}
		#endregion
		#region BuildingQuantityNumbering
		public abstract class buildingQuantityNumbering : PX.Data.IBqlField
		{
		}
		protected int? _BuildingQuantityNumbering;
		[PXDBInt()]
		[PXUIField(DisplayName = "Building Quantity Numbering")]
		public virtual int? BuildingQuantityNumbering
		{
			get
			{
				return this._BuildingQuantityNumbering;
			}
			set
			{
				this._BuildingQuantityNumbering = value;
			}
		}
		#endregion
		#region FloorQuantity
		public abstract class floorQuantity : PX.Data.IBqlField
		{
		}
		protected int? _FloorQuantity;
		[PXDBInt()]
		[PXUIField(DisplayName = "Floor Quantity")]
		public virtual int? FloorQuantity
		{
			get
			{
				return this._FloorQuantity;
			}
			set
			{
				this._FloorQuantity = value;
			}
		}
		#endregion
		#region Floor
		public abstract class floor : PX.Data.IBqlField
		{
		}
		protected int? _Floor;
		[PXDBInt()]
		[PXUIField(DisplayName = "Floor")]
		public virtual int? Floor
		{
			get
			{
				return this._Floor;
			}
			set
			{
				this._Floor = value;
			}
		}
		#endregion
		#region StatusOfConstruction
		public abstract class statusOfConstruction : PX.Data.IBqlField
		{
		}
		protected string _StatusOfConstruction;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Status of Construction")]
        [StatusOfConstructionListAttribute]
		public virtual string StatusOfConstruction
		{
			get
			{
				return this._StatusOfConstruction;
			}
			set
			{
				this._StatusOfConstruction = value;
			}
		}
		#endregion
		#region ConstructionDate
		public abstract class constructionDate : PX.Data.IBqlField
		{
		}
		protected string _ConstructionDate;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Construction Date")]
        [ConstructionDateListAttribute]
		public virtual string ConstructionDate
		{
			get
			{
				return this._ConstructionDate;
			}
			set
			{
				this._ConstructionDate = value;
			}
		}
		#endregion
		#region Number
		public abstract class number : PX.Data.IBqlField
		{
		}
		protected string _Number;
		[PXDBString(15, IsUnicode = true)]
		[PXUIField(DisplayName = "Number")]
		public virtual string Number
		{
			get
			{
				return this._Number;
			}
			set
			{
				this._Number = value;
			}
		}
		#endregion
		#region OwnerUseBuildingAsHabitat
		public abstract class ownerUseBuildingAsHabitat : PX.Data.IBqlField
		{
		}
		protected String _OwnerUseBuildingAsHabitat;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Owner Use Building As Habitat")]
		public virtual String OwnerUseBuildingAsHabitat
		{
			get
			{
				return this._OwnerUseBuildingAsHabitat;
			}
			set
			{
				this._OwnerUseBuildingAsHabitat = value;
			}
		}
		#endregion
		#region Rented
		public abstract class rented : PX.Data.IBqlField
		{
		}
		protected String _Rented;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Rented")]
		public virtual String Rented
		{
			get
			{
				return this._Rented;
			}
			set
			{
				this._Rented = value;
			}
		}
		#endregion
		#region MonthlyRentFeeCury
		public abstract class monthlyRentFeeCury : PX.Data.IBqlField
		{
		}
		protected decimal? _MonthlyRentFeeCury;
		[PXDBDecimal(2)]  
        [PXDefault]
        [PXUIField(DisplayName = "Monthly Rent Fee", Enabled = false)]
		public virtual decimal? MonthlyRentFeeCury
		{
			get
			{
				return this._MonthlyRentFeeCury;
			}
			set
			{
				this._MonthlyRentFeeCury = value;
			}
		}
		#endregion
		#region CuryID
		public abstract class curyID : PX.Data.IBqlField
		{
		}
		protected string _CuryID;
        [PXDBString(5, IsUnicode = true)]
        [PXDefault("GEL")]
        [PXSelector(typeof(Currency.curyID))]
        [PXUIField(DisplayName = "Currency ID", Enabled = false)]
		public virtual string CuryID
		{
			get
			{
				return this._CuryID;
			}
			set
			{
				this._CuryID = value;
			}
		}
		#endregion
		#region ApartmentType
		public abstract class apartmentType : PX.Data.IBqlField
		{
		}
		protected string _ApartmentType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Apartment Type")]
        [ApartmentTypeListAttribute]
		public virtual string ApartmentType
		{
			get
			{
				return this._ApartmentType;
			}
			set
			{
				this._ApartmentType = value;
			}
		}
		#endregion
		#region Balcony
		public abstract class balcony : PX.Data.IBqlField
		{
		}
		protected String _Balcony;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Balcony")]
		public virtual String Balcony
		{
			get
			{
				return this._Balcony;
			}
			set
			{
				this._Balcony = value;
			}
		}
		#endregion
		#region Elevator
		public abstract class elevator : PX.Data.IBqlField
		{
		}
		protected String _Elevator;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Elevator")]
		public virtual String Elevator
		{
			get
			{
				return this._Elevator;
			}
			set
			{
				this._Elevator = value;
			}
		}
		#endregion
		#region ApartmentInItalianYard
		public abstract class apartmentInItalianYard : PX.Data.IBqlField
		{
		}
		protected String _ApartmentInItalianYard;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Apartment in Italian Yard")]
		public virtual String ApartmentInItalianYard
		{
			get
			{
				return this._ApartmentInItalianYard;
			}
			set
			{
				this._ApartmentInItalianYard = value;
			}
		}
		#endregion
		#region Bathroom
		public abstract class bathroom : PX.Data.IBqlField
		{
		}
		protected String _Bathroom;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Bathroom")]
		public virtual String Bathroom
		{
			get
			{
				return this._Bathroom;
			}
			set
			{
				this._Bathroom = value;
			}
		}
		#endregion
		#region Toilet
		public abstract class toilet : PX.Data.IBqlField
		{
		}
		protected String _Toilet;
        [YesNoListAttribute]
        [PXDBString(2, IsUnicode = true)]
        [PXUIField(DisplayName = "Toilet")]
		public virtual String Toilet
		{
			get
			{
				return this._Toilet;
			}
			set
			{
				this._Toilet = value;
			}
		}
		#endregion
		#region FunctionalArea
		public abstract class functionalArea : PX.Data.IBqlField
		{
		}
		protected string _FunctionalArea;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Functional Area")]
        [FunctionalAreaListAttribute]
		public virtual string FunctionalArea
		{
			get
			{
				return this._FunctionalArea;
			}
			set
			{
				this._FunctionalArea = value;
			}
		}
		#endregion
		#region ViticultureMicrozone
		public abstract class viticultureMicrozone : PX.Data.IBqlField
		{
		}
		protected string _ViticultureMicrozone;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Viticulture Microzone")]
        [ViticultureMicrozoneListAttribute]
		public virtual string ViticultureMicrozone
		{
			get
			{
				return this._ViticultureMicrozone;
			}
			set
			{
				this._ViticultureMicrozone = value;
			}
		}
		#endregion
		#region GeometryOfLandArea
		public abstract class geometryOfLandArea : PX.Data.IBqlField
		{
		}
		protected string _GeometryOfLandArea;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Geometry of Land Area")]
        [GeometryOfLandAreaListAttribute]
		public virtual string GeometryOfLandArea
		{
			get
			{
				return this._GeometryOfLandArea;
			}
			set
			{
				this._GeometryOfLandArea = value;
			}
		}
		#endregion
		#region Relief
		public abstract class relief : PX.Data.IBqlField
		{
		}
		protected string _Relief;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Relief")]
        [ReliefListAttribute]
		public virtual string Relief
		{
			get
			{
				return this._Relief;
			}
			set
			{
				this._Relief = value;
			}
		}
		#endregion



	}
}

﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using _Shared;
    using PX.Objects.CS;
    using PX.TM;
    using PX.Objects.EP;
    using Shared;
    using _Shared.RepossessionApplicationStatus;

    [System.SerializableAttribute()]
    [PXPrimaryGraph(typeof(RARepossessionApplicationEntry))]
    [PXEMailSource]
    public class RARepossessionApplication : PX.Data.IBqlTable
	{
		#region RepossessionApplicationID
		public abstract class repossessionApplicationID : PX.Data.IBqlField
		{
		}
		protected int? _RepossessionApplicationID;
        [PXDBIdentity()]
        [PXUIField(Enabled = false,Visible =false)]
        public virtual int? RepossessionApplicationID
		{
			get
			{
				return this._RepossessionApplicationID;
			}
			set
			{
				this._RepossessionApplicationID = value;
			}
		}
		#endregion
		#region RepossessionApplicationCD
		public abstract class repossessionApplicationCD : PX.Data.IBqlField
		{
		}
		protected string _RepossessionApplicationCD;
        [PXDBString(15, IsUnicode = true, IsKey = true, InputMask = ">CCCCCCCCCCCCCCC")]
        [RAApplicationSelectorAttribute]
        [AutoNumber(typeof(RASetup.repossessionApplicationNumberingID), typeof(RARepossessionApplication.createdDateTime))]
        [PXUIField(DisplayName = "Document Number", Visibility = PXUIVisibility.SelectorVisible)]
        public virtual string RepossessionApplicationCD
		{
			get
			{
				return this._RepossessionApplicationCD;
			}
			set
			{
				this._RepossessionApplicationCD = value;
			}
		}
		#endregion
		#region RepossessionType
		public abstract class repossessionType : PX.Data.IBqlField
		{
		}
		protected string _RepossessionType;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Repossession Type")]
        [RepossessionTypeListSttribute]
		public virtual string RepossessionType
		{
			get
			{
				return this._RepossessionType;
			}
			set
			{
				this._RepossessionType = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected string _Status;
		[PXDBString(2, IsUnicode = true)]
		[PXUIField(DisplayName = "Status")]
        [RepossessionApplicationStatusListAttribute]
		public virtual string Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region Hold
		public abstract class hold : PX.Data.IBqlField
		{
		}
		protected bool? _Hold;
		[PXDBBool()]
		[PXUIField(DisplayName = "Hold")]
		public virtual bool? Hold
		{
			get
			{
				return this._Hold;
			}
			set
			{
				this._Hold = value;
			}
		}
		#endregion
		#region LoanManager
		public abstract class loanManager : PX.Data.IBqlField
		{
		}
		protected Guid? _LoanManager;
        [PXDBField()]
        [PXUIField(DisplayName = "Loan Manager")]
        [PXOwnerSelector()]
        [PXDefault(typeof(Search<EPEmployee.userID, Where<EPEmployee.userID, Equal<Current<AccessInfo.userID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
        public virtual Guid? LoanManager
		{
			get
			{
				return this._LoanManager;
			}
			set
			{
				this._LoanManager = value;
			}
		}
		#endregion
		#region ReportsTo
		public abstract class reportsTo : PX.Data.IBqlField
		{
		}
		protected Guid? _ReportsTo;
        [PXDBField()]
        [PXUIField(DisplayName = "Reports To")]
        [PXOwnerSelector()]
        public virtual Guid? ReportsTo
		{
			get
			{
				return this._ReportsTo;
			}
			set
			{
				this._ReportsTo = value;
			}
		}
		#endregion
		#region ReleaseDate
		public abstract class releaseDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _ReleaseDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Release Date")]
		public virtual DateTime? ReleaseDate
		{
			get
			{
				return this._ReleaseDate;
			}
			set
			{
				this._ReleaseDate = value;
			}
		}
		#endregion
		#region ApprovalDate
		public abstract class approvalDate : PX.Data.IBqlField
		{
		}
		protected DateTime? _ApprovalDate;
		[PXDBDate()]
		[PXUIField(DisplayName = "Approval Date")]
		public virtual DateTime? ApprovalDate
		{
			get
			{
				return this._ApprovalDate;
			}
			set
			{
				this._ApprovalDate = value;
			}
		}
		#endregion
		#region Description
		public abstract class description : PX.Data.IBqlField
		{
		}
		protected string _Description;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Description")]
		public virtual string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this._Description = value;
			}
		}
		#endregion
		#region LMRemopossessionAmt
		public abstract class lMRemopossessionAmt : PX.Data.IBqlField
		{
		}
		protected decimal? _LMRemopossessionAmt;
        [LMRemopossessionAmtCalcAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "LM Repossession Amount",Enabled =false)]
		public virtual decimal? LMRemopossessionAmt
		{
			get
			{
				return this._LMRemopossessionAmt;
			}
			set
			{
				this._LMRemopossessionAmt = value;
			}
		}
		#endregion
		#region TotalFairValue
		public abstract class totalFairValue : PX.Data.IBqlField
		{
		}
		protected decimal? _TotalFairValue;
        [TotalFairValueCalcAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "ToTal Fair Value", Enabled = false)]
		public virtual decimal? TotalFairValue
		{
			get
			{
				return this._TotalFairValue;
			}
			set
			{
				this._TotalFairValue = value;
			}
		}
		#endregion
		#region TotalSalvageValue
		public abstract class totalSalvageValue : PX.Data.IBqlField
		{
		}
		protected decimal? _TotalSalvageValue;
        [TotalSalvageValueCalcAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Total Salvage Value", Enabled = false)]
		public virtual decimal? TotalSalvageValue
		{
			get
			{
				return this._TotalSalvageValue;
			}
			set
			{
				this._TotalSalvageValue = value;
			}
		}
		#endregion
		#region Difference
		public abstract class difference : PX.Data.IBqlField
		{
		}
		protected decimal? _Difference;
        [DifferenceValueCalcAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Difference", Enabled = false)]
		public virtual decimal? Difference
		{
			get
			{
				return this._Difference;
			}
			set
			{
				this._Difference = value;
			}
		}
		#endregion
		#region TotalDeffectiveAmt
		public abstract class totalDeffectiveAmt : PX.Data.IBqlField
		{
		}
		protected decimal? _TotalDeffectiveAmt;
        [TotalDeffectiveAmtCalcAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Total Deffective Amount", Enabled = false)]
		public virtual decimal? TotalDeffectiveAmt
		{
			get
			{
				return this._TotalDeffectiveAmt;
			}
			set
			{
				this._TotalDeffectiveAmt = value;
			}
		}
		#endregion
		#region RAManagerRemopossessionAmt
		public abstract class rAManagerRemopossessionAmt : PX.Data.IBqlField
		{
		}
		protected decimal? _RAManagerRemopossessionAmt;
        [RAManagerRemopossessionAmtCalcAttribute]
        [PXDecimal(2)]
		[PXUIField(DisplayName = "RA Manager Repossession Amount ", Enabled = false)]
		public virtual decimal? RAManagerRemopossessionAmt
		{
			get
			{
				return this._RAManagerRemopossessionAmt;
			}
			set
			{
				this._RAManagerRemopossessionAmt = value;
			}
		}
		#endregion
		#region TotalLoanAmt
		public abstract class totalLoanAmt : PX.Data.IBqlField
		{
		}
		protected decimal? _TotalLoanAmt;
        [TotalLoanAmtAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Total Loan Amount ", Enabled = false)]
		public virtual decimal? TotalLoanAmt
		{
			get
			{
				return this._TotalLoanAmt;
			}
			set
			{
				this._TotalLoanAmt = value;
			}
		}
		#endregion
		#region TotalPayedPrincipalAmt
		public abstract class totalPayedPrincipalAmt : PX.Data.IBqlField
		{
		}
		protected decimal? _TotalPayedPrincipalAmt;
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Total Payed Principal Amount", Enabled = false)]
		public virtual decimal? TotalPayedPrincipalAmt
		{
			get
			{
				return this._TotalPayedPrincipalAmt;
			}
			set
			{
				this._TotalPayedPrincipalAmt = value;
			}
		}
        #endregion
        #region LTV
        public abstract class lTV : PX.Data.IBqlField
        {
        }
        protected decimal? _LTV;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "LTV")]
        public virtual decimal? LTV
        {
            get
            {
                return this._LTV;
            }
            set
            {
                this._LTV = value;
            }
        }
        #endregion
        #region LTVWithoutPenalty
        public abstract class lTVWithoutPenalty : PX.Data.IBqlField
        {
        }
        protected decimal? _LTVWithoutPenalty;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "LTV Without Penalty")]
        public virtual decimal? LTVWithoutPenalty
        {
            get
            {
                return this._LTVWithoutPenalty;
            }
            set
            {
                this._LTVWithoutPenalty = value;
            }
        }
        #endregion
        #region Insider
        public abstract class insider : PX.Data.IBqlField
        {
        }
        protected bool? _Insider;
        [InsiderAttribute]
        [PXBool()]
        [PXUIField(DisplayName = "Insider", Enabled = false)]
        public virtual bool? Insider
        {
            get
            {
                return this._Insider;
            }
            set
            {
                this._Insider = value;
            }
        }
        #endregion
        #region CapitalSharePct
        public abstract class capitalSharePct : PX.Data.IBqlField
		{
		}
		protected decimal? _CapitalSharePct;
        [CapitalSharePctAttribute]
		[PXDecimal(2)]
		[PXUIField(DisplayName = "Capital Share %", Enabled = false)]
		public virtual decimal? CapitalSharePct
		{
			get
			{
				return this._CapitalSharePct;
			}
			set
			{
				this._CapitalSharePct = value;
			}
		}
		#endregion
		#region Comment
		public abstract class comment : PX.Data.IBqlField
		{
		}
		protected string _Comment;
        [PXDBLocalizableString(IsUnicode = true)]
        [PXUIField(DisplayName = "Comment")]
        public virtual string Comment
		{
			get
			{
				return this._Comment;
			}
			set
			{
				this._Comment = value;
			}
		}
		#endregion
		#region LineCntr
		public abstract class lineCntr : PX.Data.IBqlField
		{
		}
		protected int? _LineCntr;
		[PXDBInt()]
		[PXDefault(0)]
		[PXUIField(DisplayName = "LineCntr")]
		public virtual int? LineCntr
		{
			get
			{
				return this._LineCntr;
			}
			set
			{
				this._LineCntr = value;
			}
		}
		#endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        public abstract class noteID : PX.Data.IBqlField
        {
        }
        protected Guid? _NoteID;
        [PXNote]
        public virtual Guid? NoteID
        {
            get
            {
                return this._NoteID;
            }
            set
            {
                this._NoteID = value;
            }
        }
        #endregion
    }
}

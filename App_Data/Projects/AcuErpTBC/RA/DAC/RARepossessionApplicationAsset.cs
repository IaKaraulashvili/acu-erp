﻿namespace RA
{
    using System;
    using PX.Data;

    [System.SerializableAttribute()]
    public class RARepossessionApplicationAsset : PX.Data.IBqlTable
    {
        #region RepossessionApplicationAssetID
        public abstract class repossessionApplicationAssetID : PX.Data.IBqlField
        {
        }
        protected int? _RepossessionApplicationAssetID;
        [PXDBIdentity(IsKey = true)]
        [PXUIField(Enabled = false)]
        public virtual int? RepossessionApplicationAssetID
        {
            get
            {
                return this._RepossessionApplicationAssetID;
            }
            set
            {
                this._RepossessionApplicationAssetID = value;
            }
        }
        #endregion
        #region RepossessionApplicationID
        public abstract class repossessionApplicationID : PX.Data.IBqlField
        {
        }
        protected int? _RepossessionApplicationID;
        [PXDBInt(IsKey = true)]
        [PXDBDefault(typeof(RARepossessionApplication.repossessionApplicationID))]
        [PXParent(typeof(Select<RARepossessionApplication,
    Where<RARepossessionApplication.repossessionApplicationID, Equal<Current<RARepossessionApplicationAsset.repossessionApplicationID>>>>))]
        public virtual int? RepossessionApplicationID
        {
            get
            {
                return this._RepossessionApplicationID;
            }
            set
            {
                this._RepossessionApplicationID = value;
            }
        }
        #endregion
        #region AssetID
        public abstract class assetID : PX.Data.IBqlField
        {
        }
        protected int? _AssetID;
        [PXDBInt()]
        [PXUIField(DisplayName = "Asset ID")]
        [PXSelector(typeof(Search<RAAsset.assetID>),
            typeof(RAAsset.assetCD),
            typeof(RAAsset.assetType),
            typeof(RAAsset.status),
            typeof(RAAsset.reason),
            SubstituteKey = typeof(RAAsset.assetCD), DescriptionField = typeof(RAAsset.assetCD))]
        public virtual int? AssetID
        {
            get
            {
                return this._AssetID;
            }
            set
            {
                this._AssetID = value;
            }
        }
        #endregion
        #region LineNbr
        public abstract class lineNbr : PX.Data.IBqlField
        {
        }
        protected int? _LineNbr;
        [PXDBInt()]
        [PXLineNbr(typeof(RARepossessionApplication.lineCntr))]
        [PXUIField(DisplayName = "Item Nbr.", Visible = false)]
        public virtual int? LineNbr
        {
            get
            {
                return this._LineNbr;
            }
            set
            {
                this._LineNbr = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

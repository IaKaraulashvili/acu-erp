﻿﻿namespace RA
{
    using System;
    using PX.Data;
    using PX.Objects.CS;
    using PX.Objects.CM;

    [System.SerializableAttribute()]
	public class RASetup : PX.Data.IBqlTable
	{
		#region RepossessedAssetsNumberingID
		public abstract class repossessedAssetsNumberingID : PX.Data.IBqlField
		{
		}
		protected string _RepossessedAssetsNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault("")]
        [PXUIField(DisplayName = "Repossessed Assets Numbering Sequence")]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        public virtual string RepossessedAssetsNumberingID
		{
			get
			{
				return this._RepossessedAssetsNumberingID;
			}
			set
			{
				this._RepossessedAssetsNumberingID = value;
			}
		}
		#endregion
		#region RepossessionApplicationNumberingID
		public abstract class repossessionApplicationNumberingID : PX.Data.IBqlField
		{
		}
		protected string _RepossessionApplicationNumberingID;
        [PXDBString(10, IsUnicode = true)]
        [PXDefault("")]
        [PXUIField(DisplayName = "Repossession Application Numbering Sequence")]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        public virtual string RepossessionApplicationNumberingID
		{
			get
			{
				return this._RepossessionApplicationNumberingID;
			}
			set
			{
				this._RepossessionApplicationNumberingID = value;
			}
		}
		#endregion
		#region LoanNumberingID
		public abstract class loanNumberingID : PX.Data.IBqlField
		{
		}
		protected string _LoanNumberingID;
		[PXDBString(10, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Loan Numbering Sequence")]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        public virtual string LoanNumberingID
		{
			get
			{
				return this._LoanNumberingID;
			}
			set
			{
				this._LoanNumberingID = value;
			}
		}
		#endregion
		#region EvaluationNumberingID
		public abstract class evaluationNumberingID : PX.Data.IBqlField
		{
		}
		protected string _EvaluationNumberingID;
		[PXDBString(10, IsUnicode = true)]
		[PXDefault("")]
		[PXUIField(DisplayName = "Evaluation Numbering Sequence")]
        [PXSelector(typeof(Numbering.numberingID), DescriptionField = typeof(Numbering.descr))]
        public virtual string EvaluationNumberingID
		{
			get
			{
				return this._EvaluationNumberingID;
			}
			set
			{
				this._EvaluationNumberingID = value;
			}
		}
		#endregion
		#region URL
		public abstract class uRL : PX.Data.IBqlField
		{
		}
		protected string _URL;
		[PXDBString(150, IsUnicode = true)]
		[PXUIField(DisplayName = "URL")]
		public virtual string URL
		{
			get
			{
				return this._URL;
			}
			set
			{
				this._URL = value;
			}
		}
		#endregion
		#region UserName
		public abstract class userName : PX.Data.IBqlField
		{
		}
		protected string _UserName;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "User Name")]
		public virtual string UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				this._UserName = value;
			}
		}
		#endregion
		#region Password
		public abstract class password : PX.Data.IBqlField
		{
		}
		protected string _Password;
		[PXDBString(50, IsUnicode = true)]
		[PXUIField(DisplayName = "Password")]
		public virtual string Password
		{
			get
			{
				return this._Password;
			}
			set
			{
				this._Password = value;
			}
		}
		#endregion
		#region HoldRepossession
		public abstract class holdRepossession : PX.Data.IBqlField
		{
		}
		protected bool? _HoldRepossession;
		[PXDBBool()]
		[PXUIField(DisplayName = "Repossession Application On Hold")]
		public virtual bool? HoldRepossession
		{
			get
			{
				return this._HoldRepossession;
			}
			set
			{
				this._HoldRepossession = value;
			}
        }
        #endregion  
        #region CuryRateTypeID
        public abstract class curyRateTypeID : PX.Data.IBqlField
        {
        }
        protected String _CuryRateTypeID;
        [PXDBString(6, IsUnicode = true)]
        [PXSelector(typeof(CurrencyRateType.curyRateTypeID))]
        [PXUIField(DisplayName = "Curr. Rate Type ")]
        public virtual String CuryRateTypeID
        {
            get
            {
                return this._CuryRateTypeID;
            }
            set
            {
                this._CuryRateTypeID = value;
            }
        }
        #endregion
        #region AssetAllocationAcceptableDeviationPct      
        public abstract class assetAllocationAcceptableDeviationPct : PX.Data.IBqlField
        {
        }
        protected decimal? _AssetAllocationAcceptableDeviationPct;
        [PXDBDecimal(2)]
        [PXUIField(DisplayName = "Asset Allocation Acceptable Deviation %")]
        public virtual decimal? AssetAllocationAcceptableDeviationPct
        {
            get
            {
                return this._AssetAllocationAcceptableDeviationPct;
            }
            set
            {
                this._AssetAllocationAcceptableDeviationPct = value;
            }
        }
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected Byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual Byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected String _CreatedByScreenID;
        [PXDBCreatedByScreenID()]
        public virtual String CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected String _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID()]
        public virtual String LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
    }
}

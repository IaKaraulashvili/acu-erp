﻿using System;
using PX.Data;
using PX.Common;
using System.Collections.Generic;

namespace RA.Descriptor
{
    [PXLocalizable(Messages.Prefix)]
    public static class Messages
    {
        public const string Prefix = "RA";

        public const string LoanIDCanNotBeDuplicate = "Loan Can Not Be Duplicate";
        public const string AssetCanNotBeDuplicate = "Asset Can Not Be Duplicate";
        public const string Delete = "Delete";
        public const string Add = "Add";
        public const string Next = "Next";
        public const string GetDataFromApi = "Get Data From Api";
        public const string RepossessionAsset = "Repossession Asset";
        public const string PaidInsuranceValidation = "Invalid value: Paid insurance can not be greater than overdue insurance";
        public const string PaidInterestValidation = "Invalid value: Paid interest can not be greater than overdue interest";
        public const string PaidPenaltyValidation = "Invalid value: Paid penalty can not be greater than overdue penalty";
        public const string PaidPrincipleValidation = "Invalid value: Paid principle can not be greater than overdue principle";
        public const string PaidAmountTooLow = "Invalid value: Paid amount is too low";
        public const string PaidAmountTooHigh = "Invalid value: paid amount is too high";
        public const string Recalculation = "Recalculation";
        public const string SendToRAManager = "Send to RA Manager";
        public const string ResponseFromRAManager = "Response from RA manager";
        public const string RecommendedAmountCannotBeEmpty = "Recommended Amount cannot be empty";
        public const string AssetAllocationAcceptableDeviationMissing = "Asset allocation acceptable deviation % is not specified";
        public const string StartAuction = "Start Auction";
        public const string SoldOnAuction = "Sold on Auction";
        public const string StartContracting = "Start Contracting";
    }
}

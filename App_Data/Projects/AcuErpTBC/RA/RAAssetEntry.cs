﻿using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;

using RA._Shared.EvaluationStatus;
using RA.EstimationType._Shared;
using RA._Shared.EvaluationType;
using RA._Shared.EvaluationApproach;

using PX.Objects.CM;
using System.Collections.Concurrent;
using PX.Objects.GL;
using PX.Objects.CS;
using RA._Shared;
using System.Text;
using CustomSYProvider.Services;
using System.Linq;
using RA._Shared.RestrictionType;

namespace RA
{
    public class RAAssetEntry : RAGraph<RAAssetEntry, RAAsset>
    {
        #region Data Views

        public PXSelect<RAAsset> Assets;

        public PXSelect<RAAsset, Where<RAAsset.assetID, Equal<Current<RAAsset.assetID>>>> CurrentAsset;

        public PXSetup<RASetup> Setup;

        public PXSelect<RAFinancialDetail, Where<RAFinancialDetail.assetID, Equal<Current<RAAsset.assetID>>>> FinancialDetail;

        public PXSelect<RAPropertyOwner, Where<RAPropertyOwner.assetID, Equal<Current<RAAsset.assetID>>>, OrderBy<Asc<RAPropertyOwner.propertyOwnerID>>> PropertyOwner;


        public PXSelect<RAEvaluation, Where<RAEvaluation.assetID, Equal<Current<RAAsset.assetID>>>, OrderBy<Asc<RAEvaluation.evaluationID>>> Evaluation;

        public PXSetup<Company> Company;

        public PXSelectJoin<RAAssetLoan, InnerJoin<RALoan, On<RAAssetLoan.loanID, Equal<RALoan.loanID>>>, Where<RAAssetLoan.assetID, Equal<Current<RAAsset.assetID>>>, OrderBy<Asc<RAAssetLoan.assetLoanID>>> AssetLoanDetails;

        public PXSelectJoin<RALinkedAsset, InnerJoin<RAAsset, On<RALinkedAsset.linkedID, Equal<RAAsset.assetID>>>, Where<RALinkedAsset.assetID, Equal<Current<RAAsset.assetID>>>, OrderBy<Asc<RAAsset.assetID>>> LinkedAsset;

        [PXVirtualDAC]
        public PXSelect<RALoan> Loan;

        public PXSelect<RARealEstate, Where<RARealEstate.assetID, Equal<Current<RAAsset.assetID>>>> RealEstate;

        public PXSelect<RANonRealEstate, Where<RANonRealEstate.assetID, Equal<Current<RAAsset.assetID>>>> NonRealEstate;

        public PXSelect<RAAssetRestriction, Where<RAAssetRestriction.assetID, Equal<Current<RAAsset.assetID>>>, OrderBy<Asc<RAAssetRestriction.restrictionID>>> Restrictions;

        #endregion

        #region Event Handlers


        protected virtual void RAAsset_AssetClassification_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            if (row.AssetClassification != AssetClassification.ThirtyPercentProject)
            {
                row.Option30PercProjectCurrency = null;
                row.Option30PercProjectAmountInCurrency = null;
            }
        }

        protected virtual void RAAsset_RowSelecting(PXCache sender, PXRowSelectingEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            using (new PXConnectionScope())
            {
                decimal? creditDebtAmount = 0;
                decimal? paidAmount = 0;

                RARepossessionApplicationAsset applicationAsset = PXSelectReadonly<
                	RARepossessionApplicationAsset, 
                	Where<RARepossessionApplicationAsset.assetID, Equal<Required<RARepossessionApplicationAsset.assetID>>>>
                	.Select(this, row.AssetID)
                	.FirstOrDefault();
                RARepossessionApplication applicaton = null;

                if (applicationAsset != null)
                {
                    applicaton = PXSelectReadonly<
                    	RARepossessionApplication, 
                    	Where<RARepossessionApplication.repossessionApplicationID, Equal<Required<RARepossessionApplication.repossessionApplicationID>>>>
                    	.Select(this, applicationAsset.RepossessionApplicationID)
                    	.FirstOrDefault();
                    row.ApplicationRefNbr = applicaton?.RepossessionApplicationCD;

                    var assetAllocations = PXSelectReadonly<
                    	RAAssetAllocation,
                    	Where<RAAssetAllocation.assetID, Equal<Required<RAAssetAllocation.assetID>>,
                    		And<RAAssetAllocation.repossessionApplicationID, Equal<Required<RAAssetAllocation.repossessionApplicationID>>>>>
                    	.Select(this, row.AssetID, applicaton?.RepossessionApplicationID);

                    foreach (RAAssetAllocation itemAssetAllocation in assetAllocations)
                    {
                        paidAmount += itemAssetAllocation.PaidPrincipleCury + itemAssetAllocation.PaidInterestCury + itemAssetAllocation.PaidPenaltyCury + itemAssetAllocation.PaidInsuranceCury;
                    }
                }

                row.PrintDate = DateTime.Now;
                row.RealNonRealEstate = row.RealEstate == true ? "უძრავი ქონება" : "მოძრავი ქონება";

                RARealEstate realEstate = PXSelect<RARealEstate, Where<RARealEstate.assetID, Equal<Required<RARealEstate.assetID>>>>.Select(this, row.AssetID).FirstOrDefault();
                RANonRealEstate nonRealEstate = PXSelect<RANonRealEstate, Where<RANonRealEstate.assetID, Equal<Required<RANonRealEstate.assetID>>>>.Select(this, row.AssetID).FirstOrDefault();

                row.CadastralCodeSerialNumber = row.RealEstate == true ? realEstate?.CadastralCode : nonRealEstate?.SerialNumber;
                var assetLoans = PXSelectJoin<
                	RAAssetLoan, 
                	InnerJoin<RALoan, 
                		On<RAAssetLoan.loanID, Equal<RALoan.loanID>>>,
                	Where<RAAssetLoan.assetID, Equal<Required<RAAssetLoan.assetID>>>,
                	OrderBy<
                		Asc<RAAssetLoan.assetLoanID>>>
                	.Select(this, row.AssetID);

                foreach (PXResult<RAAssetLoan, RALoan> res in assetLoans)
                {
                    RALoan itemLoan = res;
                    creditDebtAmount += itemLoan.LoanAmtCury + itemLoan.OverduePrincipleCury + itemLoan.OverduePenaltyCury + itemLoan.OverdueInsuranceCury;
                }

                row.CreditDebtAmount = creditDebtAmount;
                row.PaidAmount = paidAmount;
            }
        }

        #endregion

        protected virtual void RAAsset_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            if (row.AssetClassification == AssetClassification.ThirtyPercentProject)  // "30% project" "TP"
            {
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectCurrency>(Assets.Cache, row, true);
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectAmountInCurrency>(Assets.Cache, row, true);
            }
            else
            {
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectCurrency>(Assets.Cache, row, false);
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectAmountInCurrency>(Assets.Cache, row, false);
            }


            PXUIFieldAttribute.SetEnabled<RAAsset.instalmentStartDate>(sender, null, row.PropertyRedeem == true);
            PXUIFieldAttribute.SetEnabled<RAAsset.instalmentEndDate>(sender, null, row.PropertyRedeem == true);
            if (row.PropertyRedeem == true)
            {
                PXDefaultAttribute.SetPersistingCheck<RAAsset.instalmentStartDate>(Assets.Cache, row, PXPersistingCheck.NullOrBlank);
                PXDefaultAttribute.SetPersistingCheck<RAAsset.instalmentEndDate>(Assets.Cache, row, PXPersistingCheck.NullOrBlank);
            }
            else
            {
                PXDefaultAttribute.SetPersistingCheck<RAAsset.instalmentStartDate>(Assets.Cache, row, PXPersistingCheck.Nothing);
                PXDefaultAttribute.SetPersistingCheck<RAAsset.instalmentEndDate>(Assets.Cache, row, PXPersistingCheck.Nothing);
            }
            if(row.Status == AssetStatus.SoldOnAuction)
            {
                PXUIFieldAttribute.SetEnabled(sender, row, false);
                PXUIFieldAttribute.SetEnabled(FinancialDetail.Cache, (RAFinancialDetail)FinancialDetail.Select(), false);
                PXUIFieldAttribute.SetEnabled(AssetLoanDetails.Cache, (RAAssetLoan)AssetLoanDetails.Select(), false);
                PXUIFieldAttribute.SetEnabled(PropertyOwner.Cache, (RAPropertyOwner)PropertyOwner.Select(), false);
                PXUIFieldAttribute.SetEnabled(Evaluation.Cache, (RAEvaluation)Evaluation.Select(), false);
                PXUIFieldAttribute.SetEnabled(LinkedAsset.Cache, (RALinkedAsset)LinkedAsset.Select(), false);
                PXUIFieldAttribute.SetEnabled(Restrictions.Cache, (RAAssetRestriction)Restrictions.Select(), false);
            }
            SetVisibleRealEstateTab(row, sender);
            SetVisibleNonRealEstateTab(row, sender);

            #region Actions
            bool reaclculationEnabled = row.Status == AssetStatus.Draft || row.Status == AssetStatus.ReadyForEvaluation || row.Status == AssetStatus.WaitingForEvaluation
                                                   || row.Status == AssetStatus.Evaluated || row.Status == AssetStatus.WaitingRAManager || row.Status == AssetStatus.RespondedByRAManager;

            currencyRecalculation.SetEnabled(reaclculationEnabled);

            bool startAuctionEnable = row.Status == AssetStatus.Approved && row.RepossessionType == RepossessionType.Auction && row.Reason != AssetReason.Replace;
            bool SoldOnAuctionEnable = row.Status == AssetStatus.WaitingAuctionResult;
            bool startContractingEnable = row.RepossessionType == RepossessionType.Agreement && row.Reason != AssetReason.Replace;
            startAuction.SetEnabled(startAuctionEnable);
            soldOnAuction.SetEnabled(SoldOnAuctionEnable);
            startContracting.SetEnabled(startContractingEnable);
            #endregion
        }

        protected virtual void RAAsset_RowPersisting(PXCache sender, PXRowPersistingEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row != null)
            {
                /* მფლობელების კონკატენაცია */
                StringBuilder cOwners = new StringBuilder();
                using (var conn = new PXConnectionScope())
                {
                    foreach (RAPropertyOwner item in PropertyOwner.Select())
                    {
                        if (!string.IsNullOrEmpty(item.PropertyOwner))
                            if (((item.PropertyOwner.Trim()).Length > 0) && (cOwners.ToString().Length > 0))
                            {
                                cOwners.Append(",");
                            };
                        cOwners.Append(item.PropertyOwner);
                    }
                }
                row.PropertyOwners = cOwners.ToString();

                /* მსესხებლების კონკატენაცია */
                StringBuilder cBorrowers = new StringBuilder();
                foreach (RALoan itemL in Loan.Select())
                {
                    if (!string.IsNullOrEmpty(itemL.BorrowerName))
                        if (((itemL.BorrowerName.Trim()).Length > 0) && (cBorrowers.ToString().Length > 0))
                        {
                            cBorrowers.Append(",");
                        };
                    cBorrowers.Append(itemL.BorrowerName);
                }
                row.Borrowers = cBorrowers.ToString();

                /* შეზღუდვების კონკატენაცია */
                StringBuilder cRestrictions = new StringBuilder();
                List<RAAssetRestriction> MyList = new List<RAAssetRestriction>();
                foreach (RAAssetRestriction itemR in Restrictions.Select())
                {
                    bool ItemExists = false;
                    foreach (RAAssetRestriction itemRR in MyList)
                    {
                        if (itemR.RestrictionType == itemRR.RestrictionType)
                        {
                            ItemExists = true;
                        }

                    }
                    MyList.Add(itemR);

                    string type = "";
                    switch (itemR.RestrictionType)
                    {
                        case RestrictionType.Sequestration: type = RestrictionType.UI.Sequestration; break;
                        case RestrictionType.LowMortgage: type = RestrictionType.UI.LowMortgage; break;
                        case RestrictionType.Obliged: type = RestrictionType.UI.Obliged; break;
                        case RestrictionType.Forbid: type = RestrictionType.UI.Forbid; break;
                        case RestrictionType.PartnersShareEquitableLien: type = RestrictionType.UI.PartnersShareEquitableLien; break;
                        case RestrictionType.MortgageLeasing: type = RestrictionType.UI.MortgageLeasing; break;
                        default:
                            break;
                    }

                    if (!ItemExists)
                        {
                        if (type != string.Empty)
                            if (((type.Trim()).Length > 0) && (cRestrictions.ToString().Length > 0))
                            {
                                cRestrictions.Append(",");
                            };
                        cRestrictions.Append(type);
                        }

                    ItemExists = false;
                }
                row.Restrictions = cRestrictions.ToString();

            }
        }

        public override void Persist()
        {
            using (var tran = new PXTransactionScope())
            {
                base.Persist();
                if (Assets.Current != null && Assets.Current.RepossessionApplicationID != null)
                {
                    RARepossessionApplicationEntry applicationEntry = CreateInstance<RARepossessionApplicationEntry>();
                    var assetApplication = new RARepossessionApplicationAsset();
                    assetApplication.AssetID = Assets.Current.AssetID;
                    assetApplication.RepossessionApplicationID = Assets.Current.RepossessionApplicationID;
                    applicationEntry.RepossessionApplicationAsset.Insert(assetApplication);
                    applicationEntry.Save.Press();
                }
                tran.Complete();
            }
        }

        #region GL Accounts Tab

        protected virtual void RAAsset_ClassID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RAClass raClass = PXSelectReadonly<RAClass, Where<RAClass.classID, Equal<Required<RAClass.classID>>>>.Select(this, row.ClassID);
            if (raClass != null)
            {
                row.RealEstate = raClass.RealEstate;
                row.AssetType = raClass.AssetType;
                row.IFRSRAAccountID = raClass.IFRSRAAccountID;
                row.IFRSTransitAccountID = raClass.IFRSTransitAccountID;
                row.NBGAccount = raClass.NBGAccount;
                row.NBGTransitAccount = raClass.NBGTransitAccount;
                row.NBGReserveAccount = raClass.NBGReserveAccount;
                row.NBGReserveExpensesAccount = raClass.NBGReserveExpensesAccount;

            }
        }
        #endregion

        #region Repossession Info Tab

        protected virtual void RAAsset_ExpectedCuryDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "ExpectedCuryDate");
        }

        protected virtual void RAAsset_ExpectedRepossessionCuryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionCuryID");
        }

        protected virtual void RAAsset_ExpectedRepossessionCuryRate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;
            row.FixedRate = true;
            RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionCuryRate");
        }

        protected virtual void RAAsset_FixedRate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;
            if (row.FixedRate == false)
            {
                RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionCuryID");
            }
        }

        protected virtual void RAAsset_ExpectedRepossessionAmt_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionAmt");
        }

        protected virtual void RAAsset_RecommendedCuryDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "RecommendedCuryDate");
        }

        protected virtual void RAAsset_RecommendedRepossessionCuryID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionCuryID");
        }

        protected virtual void RAAsset_RecommendedRepossessionCuryRate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionCuryRate");
        }

        protected virtual void RAAsset_RecommendedRepossessionAmt_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionAmt");
        }

        protected virtual void RAAsset_FinalRepossessionAmt_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            RepossessionInfoTabFieldsUpdated(row, "FinalRepossessionAmt");
        }

        protected virtual void RepossessionInfoTabFieldsUpdated(RAAsset row, string field)
        {
            if (field == "ExpectedCuryDate")
            {
                RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionCuryID");
            }
            else if (field == "ExpectedRepossessionCuryID")
            {
                string toCury = Company.Current.BaseCuryID;
                string fromCury = row.ExpectedRepossessionCuryID;
                string rateType = Setup.Current.CuryRateTypeID;

                if (string.IsNullOrEmpty(rateType))
                    return;

                if (row.ExpectedCuryDate != null && !string.IsNullOrEmpty(fromCury) && !string.IsNullOrEmpty(toCury))
                {
                    DateTime effectiveDate = Convert.ToDateTime(row.ExpectedCuryDate);

                    if (!fromCury.Equals(toCury))
                    {
                        CurrencyRate currencyRate = PXSelectReadonly<CurrencyRate, Where<CurrencyRate.fromCuryID, Equal<Required<CurrencyRate.fromCuryID>>,
                        And<CurrencyRate.toCuryID, Equal<Required<CurrencyRate.toCuryID>>,
                        And<CurrencyRate.curyRateType, Equal<Required<CurrencyRate.curyRateType>>,
                        And<CurrencyRate.curyEffDate, LessEqual<Required<CurrencyRate.curyEffDate>>>>>>,
                        OrderBy<Desc<PX.Objects.CM.CurrencyRate.curyEffDate>>>.Select(this, fromCury, toCury, rateType, effectiveDate);

                        if (currencyRate == null)
                            row.ExpectedRepossessionCuryRate = 0;
                        else
                            row.ExpectedRepossessionCuryRate = currencyRate.CuryMultDiv == "D" ? currencyRate.RateReciprocal : currencyRate.CuryRate;
                    }
                    else
                        row.ExpectedRepossessionCuryRate = 1;
                }
                else if (row.ExpectedCuryDate == null || string.IsNullOrEmpty(fromCury))
                    row.ExpectedRepossessionCuryRate = 0;

                row.FinalRepossessionCuryID = row.ExpectedRepossessionCuryID;

                RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionCuryRate");
            }
            else if (field == "ExpectedRepossessionCuryRate")
            {
                RepossessionInfoTabFieldsUpdated(row, "ExpectedRepossessionAmt");
                RepossessionInfoTabFieldsUpdated(row, "FinalRepossessionAmt");
            }
            else if (field == "ExpectedRepossessionAmt")
            {
                row.ExpectedRepossessionAmtCury = row.ExpectedRepossessionAmt;
                row.ExpectedRepossessionAmtGel = row.ExpectedRepossessionAmt * row.ExpectedRepossessionCuryRate;
            }
            else if (field == "RecommendedRepossessionAmt")
            {
                row.RecommendedRepossessionAmtCury = row.RecommendedRepossessionAmt;
                row.RecommendedRepossessionAmtGel = row.RecommendedRepossessionAmt * row.RecommendedRepossessionCuryRate;
            }
            else if (field == "FinalRepossessionAmt")
            {
                row.FinalRepossessionAmtCury = row.FinalRepossessionAmt;
                row.FinalRepossessionAmtGel = row.FinalRepossessionAmt * row.FinalRepossessionCuryRate;
            }
            else if (field == "RecommendedCuryDate")
            {
                RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionCuryID");
            }
            else if (field == "RecommendedRepossessionCuryID")
            {
                string toCury = Company.Current.BaseCuryID;
                string fromCury = row.RecommendedRepossessionCuryID;
                string rateType = Setup.Current.CuryRateTypeID;

                if (string.IsNullOrEmpty(rateType))
                    return;

                if (row.RecommendedCuryDate != null && !string.IsNullOrEmpty(fromCury) && !string.IsNullOrEmpty(toCury))
                {
                    DateTime effectiveDate = Convert.ToDateTime(row.RecommendedCuryDate);

                    if (!fromCury.Equals(toCury))
                    {
                        CurrencyRate currencyRate = PXSelectReadonly<CurrencyRate, Where<CurrencyRate.fromCuryID, Equal<Required<CurrencyRate.fromCuryID>>,
                        And<CurrencyRate.toCuryID, Equal<Required<CurrencyRate.toCuryID>>,
                        And<CurrencyRate.curyRateType, Equal<Required<CurrencyRate.curyRateType>>,
                        And<CurrencyRate.curyEffDate, LessEqual<Required<CurrencyRate.curyEffDate>>>>>>,
                        OrderBy<Desc<PX.Objects.CM.CurrencyRate.curyEffDate>>>.Select(this, fromCury, toCury, rateType, effectiveDate);

                        if (currencyRate == null)
                            row.RecommendedRepossessionCuryRate = 0;
                        else
                            row.RecommendedRepossessionCuryRate = currencyRate.CuryMultDiv == "D" ? currencyRate.RateReciprocal : currencyRate.CuryRate;
                    }
                    else
                        row.RecommendedRepossessionCuryRate = 1;
                }
                else if (row.RecommendedCuryDate == null || string.IsNullOrEmpty(fromCury))
                    row.RecommendedRepossessionCuryRate = 0;

                RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionCuryRate");
            }
            else if (field == "RecommendedRepossessionCuryRate")
            {
                RepossessionInfoTabFieldsUpdated(row, "RecommendedRepossessionAmt");
            }
            else return;
        }
        #endregion

        #region Details Real Estate Tab

        protected virtual void RARealEstate_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RARealEstate row = (RARealEstate)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<RARealEstate.monthlyRentFeeCury>(RealEstate.Cache, null, row.Rented == YesNoList.Yes);
            PXUIFieldAttribute.SetEnabled<RARealEstate.curyID>(RealEstate.Cache, null, row.Rented == YesNoList.Yes);
            PXDefaultAttribute.SetPersistingCheck<RARealEstate.monthlyRentFeeCury>(RealEstate.Cache, row, row.Rented == YesNoList.Yes ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
            PXDefaultAttribute.SetPersistingCheck<RARealEstate.curyID>(RealEstate.Cache, row, row.Rented == YesNoList.Yes ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
        }


        protected virtual void RARealEstate_Rented_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RARealEstate row = (RARealEstate)e.Row;
            if (row == null) return;

            if (row.Rented == YesNoList.No)
            {
                row.MonthlyRentFeeCury = 0;
                row.CuryID = null;
            }
            PXUIFieldAttribute.SetEnabled<RARealEstate.monthlyRentFeeCury>(RealEstate.Cache, null, row.Rented == YesNoList.Yes);
            PXUIFieldAttribute.SetEnabled<RARealEstate.curyID>(RealEstate.Cache, null, row.Rented == YesNoList.Yes);
        }

        protected virtual void SetVisibleRealEstateTab(RAAsset row, PXCache sender)
        {
            if (row.AssetType == AssetType.Land || row.AssetType == AssetType.Inhabitance || row.AssetType == AssetType.Garage ||
                row.AssetType == AssetType.IndustrialArea || row.AssetType == AssetType.CommercialOfficeArea || row.AssetType == AssetType.Hotel || row.AssetType == AssetType.Farm)
            {
                sender.SetValue<RAAsset.estateType>(row, "RealEstate");

                RealEstate.Cache.AllowSelect = true;
                PXUIFieldAttribute.SetVisible<RAAsset.otherImprovements>(Assets.Cache, row, true);
                PXUIFieldAttribute.SetVisible<RAAsset.comment>(Assets.Cache, row, true);

                PXUIFieldAttribute.SetVisible<RARealEstate.mainEntrance>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.flatNumber>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.area>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.condition>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.areaCondition>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.roomQuantity>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.buildingQuantityNumbering>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.floorQuantity>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.floor>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.statusOfConstruction>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.constructionDate>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.number>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.ownerUseBuildingAsHabitat>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.rented>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.monthlyRentFeeCury>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.curyID>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.apartmentType>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.balcony>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.apartmentInItalianYard>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.elevator>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.bathroom>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
                PXUIFieldAttribute.SetVisible<RARealEstate.toilet>(RealEstate.Cache, null, row.AssetType != AssetType.Land);
            }
            else
            {
                RealEstate.Cache.AllowSelect = false;
                row.EstateType = row.EstateType == EstateType.NonRealEstate ? EstateType.NonRealEstate : EstateType.NotSpecified;
                PXUIFieldAttribute.SetVisible<RAAsset.otherImprovements>(Assets.Cache, row, false);
                PXUIFieldAttribute.SetVisible<RAAsset.comment>(Assets.Cache, row, false);
            }
        }

        protected virtual void SetVisibleNonRealEstateTab(RAAsset row, PXCache sender)
        {
            if (row.AssetType == AssetType.Equipment || row.AssetType == AssetType.TransportVehicles || row.AssetType == AssetType.OtherNonRealEstate)
            {
                sender.SetValue<RAAsset.estateType>(row, EstateType.NonRealEstate);

                PXUIFieldAttribute.SetVisible<RAAsset.otherImprovements>(Assets.Cache, row, true);
                PXUIFieldAttribute.SetVisible<RAAsset.comment>(Assets.Cache, row, true);
                row.RealEstate = false;
            }
            else
            {
                row.EstateType = row.EstateType == EstateType.RealEstate ? EstateType.RealEstate : EstateType.NotSpecified;
            }
        }

        #endregion

        #region Loan Info

        protected virtual void RAAssetLoan_RowPersisting(PXCache sender, PXRowPersistingEventArgs e)
        {
            RAAssetLoan row = (RAAssetLoan)e.Row;
            if (row == null) return;
            foreach (RAAssetLoan item in AssetLoanDetails.Select())
            {
                if (item.LoanID == row.LoanID && item.AssetLoanID != row.AssetLoanID)
                {
                    sender.RaiseExceptionHandling<RAAssetLoan.loanID>(row, row.LoanID, new PXSetPropertyException(RA.Descriptor.Messages.LoanIDCanNotBeDuplicate, PXErrorLevel.Error));

                }
            }
        }


        #endregion

        #region ctor

        public RAAssetEntry()
        {
            actionsMenu.AddMenuAction(currencyRecalculation);
            actionsMenu.AddMenuAction(startAuction);
            actionsMenu.AddMenuAction(soldOnAuction);
            actionsMenu.AddMenuAction(startContracting);
        }

        #endregion

        #region Actions

        public PXAction<RAAsset> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

        public PXAction<RAAsset> startContracting;
        [PXUIField(DisplayName = Descriptor.Messages.StartContracting, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable StartContracting(PXAdapter adapter)
        {
            try
            {
                RAAsset currentAsset = Assets.Current;
                if (currentAsset == null) return adapter.Get();
                
                PXLongOperation.StartOperation(this, delegate ()
                {
                    currentAsset.Status = AssetStatus.Contracting;
                    currentAsset.ContractingStartDate = Accessinfo.BusinessDate;
                    this.Save.Press();
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RAAsset> startAuction;
        [PXUIField(DisplayName = Descriptor.Messages.StartAuction, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable StartAuction(PXAdapter adapter)
        {
            try
            {
                RAAsset doc = this.Assets.Current;
                if (doc == null) return adapter.Get();

                PXLongOperation.StartOperation(this, delegate ()
                {
                    doc.Status = AssetStatus.WaitingAuctionResult;
                    Assets.Update(doc);
                    this.Save.Press();
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RAAsset> soldOnAuction;
        [PXUIField(DisplayName = Descriptor.Messages.SoldOnAuction, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable SoldOnAuction(PXAdapter adapter)
        {
            try
            {
                RAAsset currentAsset = Assets.Current;
                if (currentAsset == null) return adapter.Get();

                PXLongOperation.StartOperation(this, delegate () 
                {
                    currentAsset.Status = AssetStatus.SoldOnAuction;
                    currentAsset.SoldOnAuctionDate = Accessinfo.BusinessDate;
                    Assets.Update(currentAsset);
                    this.Save.Press();
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RAAsset> currencyRecalculation;
        [PXUIField(DisplayName = Descriptor.Messages.Recalculation, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable CurrencyRecalculation(PXAdapter adapter)
        {
            try
            {
                RAAsset doc = this.Assets.Current;
                if (doc == null) return adapter.Get();

                PXLongOperation.StartOperation(this, delegate ()
                {
                    Recalculation(doc);
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public void Recalculation(RAAsset doc)
        {
            RAEvaluationMaint evaluationMaint = CreateInstance<RAEvaluationMaint>();
            RALoanEntry loanEnatry = CreateInstance<RALoanEntry>();

            using (var tran = new PXTransactionScope())
            {
                if (doc.FixedRate != true)
                {
                    doc.ExpectedCuryDate = DateTime.Now;
                }
                doc.RecommendedCuryDate = DateTime.Now;
                this.Assets.Update(doc);
                foreach (RALoan item in Loan.Select())
                {
                    item.CurrencyDate = DateTime.Now;
                    loanEnatry.Loans.Update(item);
                }
                foreach (RAEvaluation item in Evaluation.Select())
                {
                    item.CurrencyDate = DateTime.Now;
                    evaluationMaint.Evaluation.Update(item);
                    if (item.EvaluationID == doc.LastEvaluationID)
                    {
                        doc.LastEvaluationAmt = item.FairMarketValue;
                        doc.LastEvaluationAmtCury = item.FairMarketValueCury;
                        doc.LastEvaluationSalvageAmt = item.SalvageValue;
                        doc.LastEvaluationSalvageAmtCury = item.SalvageValueCury;
                    }
                }
                this.Assets.Update(doc);
                loanEnatry.Save.Press();
                evaluationMaint.Save.Press();
                this.Save.Press();
                tran.Complete();
            }
        }


        public PXAction<RAAsset> Export;
        [PXUIField(DisplayName = "Export", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable export(PXAdapter adapter)
        {
            var current = Assets.Current;
            string exportScenarioName = "Repossessed Assets Export";

            PXLongOperation.StartOperation(this, delegate ()
            {
                WordPDFSYExportProcessHelper.RunScenarion(
                    this,
                    exportScenarioName,
                    current.NoteID.ToString(),
                    current.AssetCD
                    );
            });

            return adapter.Get();
        }

        #endregion

        #region CacheAttached

        #region Evaluation 
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Asset ID", Visible = false)]
        protected virtual void RAEvaluation_AssetID_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Status", Visible = false)]
        protected virtual void RAEvaluation_Status_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Estimation Type", Visible = false)]
        protected virtual void RAEvaluation_EstimationType_CacheAttached(PXCache cache)
        {

        }

        [PXUIField(DisplayName = "Evaluation Type")]
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        protected virtual void RAEvaluation_EvaluationType_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Expenses", Visible = false)]
        protected virtual void RAEvaluation_EvaluationExpenses_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Approach", Visible = false)]
        protected virtual void RAEvaluation_EvaluationApproach_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Inspect Date", Visible = false)]
        protected virtual void RAEvaluation_InspectDate_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Currency Date", Visible = false)]
        protected virtual void RAEvaluation_CurrencyDate_CacheAttached(PXCache cache)
        {

        }
        #endregion

        #region Loan

        #region LoanProduct
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Product", Visible = false)]
        protected virtual void RALoan_LoanProduct_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanSegment
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Segment", Visible = false)]
        protected virtual void RALoan_LoanSegment_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanEndDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan End Date", Visible = false)]
        protected virtual void RALoan_LoanEndDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanDisbursementDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Disbursement Date", Visible = false)]
        protected virtual void RALoan_LoanDisbursementDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanTransferDateToPLO
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Transfer Date to PLO", Visible = false)]
        protected virtual void RALoan_LoanTransferDateToPLO_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region ClientID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "ClientID", Visible = false)]
        protected virtual void RALoan_ClientID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower ID", Visible = false)]
        protected virtual void RALoan_BorrowerID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerContactInfo
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower Contact Info", Visible = false)]
        protected virtual void RALoan_BorrowerContactInfo_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerInsider
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower Insider", Visible = false)]
        protected virtual void RALoan_BorrowerInsider_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerName
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower Name", Visible = false)]
        protected virtual void RALoan_CoBorrowerName_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower ID", Visible = false)]
        protected virtual void RALoan_CoBorrowerID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerInsider
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower Insider", Visible = false)]
        protected virtual void RALoan_CoBorrowerInsider_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 1", Visible = false)]
        protected virtual void RALoan_GuarantorName1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 1", Visible = false)]
        protected virtual void RALoan_GuarantorID1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 1 Insider", Visible = false)]
        protected virtual void RALoan_GuarantorInsider1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 2", Visible = false)]
        protected virtual void RALoan_GuarantorName2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 2", Visible = false)]
        protected virtual void RALoan_GuarantorID2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 2 Insider", Visible = false)]
        protected virtual void RALoan_GuarantorInsider2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 3", Visible = false)]
        protected virtual void RALoan_GuarantorName3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 3", Visible = false)]
        protected virtual void RALoan_GuarantorID3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 3 Insider", Visible = false)]
        protected virtual void RALoan_GuarantorInsider3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverduePrincipleCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Principle", Visibility = PXUIVisibility.SelectorVisible, Visible = false)]
        protected virtual void RALoan_OverduePrincipleCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverduePenaltyCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Penalty", Visibility = PXUIVisibility.SelectorVisible, Visible = false)]
        protected virtual void RALoan_OverduePenaltyCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInsuranceCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Insurance", Visibility = PXUIVisibility.SelectorVisible, Visible = false)]
        protected virtual void RALoan_OverdueInsuranceCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInterestCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Interest", Visibility = PXUIVisibility.SelectorVisible, Visible = false)]
        protected virtual void RALoan_OverdueInterestCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverduePenalty
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Penalty", Visible = false)]
        protected virtual void RALoan_OverduePenalty_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CurrencyDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Currency Date", Visible = false)]
        protected virtual void RALoan_CurrencyDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInsurance
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Insurance", Visible = false)]
        protected virtual void RALoan_OverdueInsurance_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInterest
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Interest", Visible = false)]
        protected virtual void RALoan_OverdueInterest_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanAmtGel
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Amount in GEL", Visible = false)]
        protected virtual void RALoan_LoanAmtGel_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region IFRSLoanProvisionRate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "IFRSLoanProvisionRate", Visible = false)]
        protected virtual void RALoan_IFRSLoanProvisionRate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #endregion

        #endregion
    }
}
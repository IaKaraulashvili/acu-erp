using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.GL;

namespace RA
{
    public class RAEvaluationMaint : RAGraph<RAEvaluationMaint, RAEvaluation>
    {

        #region Data Views

        public PXSelect<RAEvaluation> Evaluation;

        public PXSetup<RASetup> Setup;

        public PXSelect<CurrencyInfo> currencyinfo;

        public PXSelect<CurrencyInfo, Where<CurrencyInfo.curyInfoID, Equal<Current<RAEvaluation.curyInfoID>>>> CurrencyInfo_CuryInfoID;

        public ToggleCurrency<RAEvaluation> CurrencyView;

        public PXSetup<Company> Company;
        #endregion

        #region Events
        protected virtual void RAEvaluation_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            RAEvaluation row = e.Row as RAEvaluation;

            if (row == null)
            {
                return;
            }

            PXDBCurrencyAttribute.SetBaseCalc<RAEvaluation.fairMarketValueCury>(cache, row.FairMarketValueCury, true);
            PXDBCurrencyAttribute.SetBaseCalc<RAEvaluation.salvageValueCury>(cache, row.SalvageValueCury, true);

        }
        
        protected virtual void RAEvaluation_CurrencyDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            CurrencyInfoAttribute.SetEffectiveDate<RAEvaluation.currencyDate>(sender, e);

        }

        protected virtual void RAEvaluation_CurrencyDate_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.NewValue = DateTime.Now;
        }

        protected virtual void CurrencyInfo_CuryID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (PXAccess.FeatureInstalled<FeaturesSet.multicurrency>())
            {
                if (Evaluation.Current != null && !string.IsNullOrEmpty(Company.Current.BaseCuryID))
                {
                    e.NewValue = Company.Current.BaseCuryID;
                    e.Cancel = true;
                }
            }
        }

        protected virtual void CurrencyInfo_CuryRateTypeID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (PXAccess.FeatureInstalled<FeaturesSet.multicurrency>() )
            {
                e.NewValue = Setup.Current.CuryRateTypeID;
                e.Cancel = true;
            }
        }

        protected virtual void CurrencyInfo_CuryEffDate_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (Evaluation.Cache.Current != null)
            {
                e.NewValue = ((RAEvaluation)Evaluation.Cache.Current).CurrencyDate;
                e.Cancel = true;
            }
        }

        private void UpdateLastEvaluationAmtOnAsset(RAEvaluation row)
        {
            if (row != null)
            {
                RAAssetEntry assetEntry = PXGraph.CreateInstance<RAAssetEntry>();
                RAAsset asset = (RAAsset)PXSelect<RAAsset, Where<RAAsset.assetID, Equal<Required<RAAsset.assetID>>>>.Select(assetEntry, row.AssetID);
                asset.LastEvaluationID=row.EvaluationID;
                asset.LastEvaluationAmt = row.FairMarketValue;
                asset.LastEvaluationAmtCury = row.FairMarketValueCury;
                asset.LastEvaluationSalvageAmt = row.SalvageValue;
                asset.LastEvaluationSalvageAmtCury = row.SalvageValueCury;
                assetEntry.Assets.Update(asset);
                assetEntry.Save.Press();
            }
        }

        public override void Persist()
        {
            using (var tran = new PXTransactionScope())
            {
                base.Persist();
                UpdateLastEvaluationAmtOnAsset(Evaluation.Current);
                tran.Complete();
            }
        }
        #endregion

        #region Reports

        public RAEvaluationMaint()
        {
            reportsMenu.AddMenuAction(printRA);
        }

        public PXAction<RAEvaluation> reportsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Reports")]
        protected virtual void ReportsMenu()
        {

        }

        public PXAction<RAEvaluation> printRA;
        [PXUIField(DisplayName = "Print RA", MapViewRights = PXCacheRights.Select, MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        public virtual IEnumerable PrintRA(PXAdapter adapter)
        {
            if (Evaluation.Current != null)
            {
                Dictionary<string, string> info = new Dictionary<string, string>();
                info["AssetID"] = Evaluation.Current.AssetID.ToString();
                info["EvaluationID"] = Evaluation.Current.EvaluationID.ToString();
                RAAsset type = PXSelectReadonly<RAAsset, Where<RAAsset.assetID, Equal<Required<RAAsset.assetID>>>>.Select(this, Evaluation.Current.AssetID);

                switch (type.AssetType)
                {
                    case "EQ":
                    case "TV":
                    case "OE":
                        throw new PXReportRequiredException(info, "RA601002", "Print RA");
                        break;
                    case "LN":
                        throw new PXReportRequiredException(info, "RA601001", "Print RA");
                        break;
                    case "IH":
                    case "GR":
                    case "IA":
                    case "CO":
                    case "HT":
                    case "FM":
                        throw new PXReportRequiredException(info, "RA601000", "Print RA");
                        break;
                    default:
                        break;
                }
            }
            return adapter.Get();
        }

        #endregion
    }
}
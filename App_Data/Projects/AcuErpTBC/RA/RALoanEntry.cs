using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.GL;

namespace RA
{
    [PXPrimaryGraph(typeof(RALoanEntry))]
    [System.SerializableAttribute()]
    public class RALoanEntry : RAGraph<RALoanEntry, RALoan>
    {
        #region Data View
        public PXSelect<RALoan> Loans;

        public PXSetup<RASetup> Setup;

        public PXSelect<CurrencyInfo> currencyinfo;
        public PXSelect<CurrencyInfo, Where<CurrencyInfo.curyInfoID, Equal<Current<RALoan.curyInfoID>>>> CurrencyInfo_CuryInfoID;
        public ToggleCurrency<RALoan> CurrencyView;

        public PXSetup<Company> Company;
        #endregion

        #region Events
        protected virtual void RALoan_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            RALoan row = e.Row as RALoan;
            if (row == null)
            {
                return;
            }


            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overdueInsuranceCury>(cache, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overdueInterestCury>(cache, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overduePenaltyCury>(cache, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overduePrincipleCury>(cache, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.loanAmtCury>(cache, null, true);
        }

        protected virtual void RALoan_CurrencyDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            CurrencyInfoAttribute.SetEffectiveDate<RALoan.currencyDate>(sender, e);
        }

        protected virtual void RALoan_CurrencyDate_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            e.NewValue = DateTime.Now;
        }

        protected virtual void CurrencyInfo_CuryID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (PXAccess.FeatureInstalled<FeaturesSet.multicurrency>())
            {
                if (Loans.Current != null && !string.IsNullOrEmpty(Company.Current.BaseCuryID))
                {
                    e.NewValue = Company.Current.BaseCuryID;
                    e.Cancel = true;
                }
            }
        }

        protected virtual void CurrencyInfo_CuryRateTypeID_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (PXAccess.FeatureInstalled<FeaturesSet.multicurrency>())
            {
                e.NewValue = Setup.Current.CuryRateTypeID;
                e.Cancel = true;
            }
        }

        protected virtual void CurrencyInfo_CuryEffDate_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            if (Loans.Cache.Current != null)
            {
                e.NewValue = ((RALoan)Loans.Cache.Current).CurrencyDate;
                e.Cancel = true;
            }
        }
        #endregion
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CM;
using RA._Shared;
using PX.Objects.EP;
using PX.Objects.CR;
using RA._Shared.LoanStatus;
using PX.Common;
using System.Linq;


namespace RA
{
    public partial class RARepossessionApplicationEntry
    {
        #region  Data Views
        public PXAction<RARepossessionApplication> AddAsset;
        public PXAction<RARepossessionApplication> GetDataFromApi;
        public PXAction<RARepossessionApplication> OpenWizard;

        public PXFilter<RAAsset> AssetWizard;
        public PXFilter<RARealEstate> RealEstateWizard;
        public PXFilter<RANonRealEstate> NonRealEstateWizard;
        public PXFilter<LoanItem> LoanWizard;
        public PXFilter<AssetAllocationWzrd> AssetAllocationsWizard;

        #endregion

        #region Events
        protected virtual void RAAsset_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RAAssetEntry assetEntry = CreateInstance<RAAssetEntry>();
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;

            row.ClassID = NewAsset.Current.ClassID;
            row.AssetType = NewAsset.Current.AssetType;
            row.Reason = NewAsset.Current.Reason;

            if (row.AssetClassification == AssetClassification.ThirtyPercentProject)  // "30% project" "TP"
            {
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectCurrency>(AssetWizard.Cache, row, true);
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectAmountInCurrency>(AssetWizard.Cache, row, true);
            }
            else
            {
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectCurrency>(AssetWizard.Cache, row, false);
                PXUIFieldAttribute.SetVisible<RAAsset.option30PercProjectAmountInCurrency>(AssetWizard.Cache, row, false);
            }
            PXDefaultAttribute.SetPersistingCheck<RAAsset.description>(AssetWizard.Cache, row, PXPersistingCheck.NullOrBlank);

            PXUIFieldAttribute.SetEnabled<RAAsset.instalmentStartDate>(sender, null, row.PropertyRedeem == true);
            PXUIFieldAttribute.SetEnabled<RAAsset.instalmentEndDate>(sender, null, row.PropertyRedeem == true);

            PXCache realEstateCache = RealEstateWizard.Cache;
            PXCache nonRealEstateCache = NonRealEstateWizard.Cache;
            var realEstate = row.AssetType == AssetType.Land || row.AssetType == AssetType.Inhabitance || row.AssetType == AssetType.Garage ||
                row.AssetType == AssetType.IndustrialArea || row.AssetType == AssetType.CommercialOfficeArea || row.AssetType == AssetType.Hotel || row.AssetType == AssetType.Farm;

            var NonRealEstate = row.AssetType == AssetType.Equipment || row.AssetType == AssetType.OtherNonRealEstate || row.AssetType == AssetType.TransportVehicles;


            PXUIFieldAttribute.SetVisible<RARealEstate.mainEntrance>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.flatNumber>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.area>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.condition>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.areaCondition>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.roomQuantity>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.buildingQuantityNumbering>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.floorQuantity>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.floor>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.statusOfConstruction>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.constructionDate>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.number>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.ownerUseBuildingAsHabitat>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.rented>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.monthlyRentFeeCury>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.curyID>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.apartmentType>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.balcony>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.apartmentInItalianYard>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.elevator>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.bathroom>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.toilet>(realEstateCache, null, row.AssetType != AssetType.Land && realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.informationSource>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.water>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.fencing>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.irrigationSystem>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.electricity>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.naturalGas>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.sewerageSystem>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.roadType>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.coastline>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.areaCondition>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.roomQuantity>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.buildingQuantityNumbering>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.functionalArea>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.viticultureMicrozone>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.geometryOfLandArea>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.relief>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.region>(realEstateCache, null, realEstate);
            PXUIFieldAttribute.SetVisible<RARealEstate.tbilisiDistrict>(realEstateCache, null, realEstate);

            PXUIFieldAttribute.SetVisible<RANonRealEstate.vINCode>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.chassisID>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.techPassportNumber>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.plateNumber>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.mark>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.automotiveBodyType>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.color>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.engineVolume>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.model>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.vehicleCondition>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.customsDuty>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.transmission>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.mileage>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.wheelLocation>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.purpose>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.issueDate>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.registrationDate>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.parameters>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.propertyDescription>(nonRealEstateCache, null, NonRealEstate);
            PXUIFieldAttribute.SetVisible<RANonRealEstate.serialNumber>(nonRealEstateCache, null, NonRealEstate);
        }

        protected virtual void RARealEstate_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RARealEstate row = (RARealEstate)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<RARealEstate.monthlyRentFeeCury>(RealEstateWizard.Cache, null, row.Rented == YesNoList.Yes);
            PXUIFieldAttribute.SetEnabled<RARealEstate.curyID>(RealEstateWizard.Cache, null, row.Rented == YesNoList.Yes);
            PXDefaultAttribute.SetPersistingCheck<RARealEstate.monthlyRentFeeCury>(RealEstateWizard.Cache, row, row.Rented == YesNoList.Yes ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
            PXDefaultAttribute.SetPersistingCheck<RARealEstate.curyID>(RealEstateWizard.Cache, row, row.Rented == YesNoList.Yes ? PXPersistingCheck.NullOrBlank : PXPersistingCheck.Nothing);
        }

        protected virtual void NAsset_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            NAsset row = (NAsset)e.Row;
            if (row == null) return;

            if (row.ClassID == null || row.Reason == null)
            {
                GetDataFromApi.SetEnabled(false);
                OpenWizard.SetEnabled(false);
                PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, false);
                PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
            }
            else {
                GetDataFromApi.SetEnabled(true);
                OpenWizard.SetEnabled(true);
            }
            if (row.ClassID != null)
            {
                if ((row.AssetType == AssetType.Land || row.AssetType == AssetType.Inhabitance || row.AssetType == AssetType.Garage ||
                row.AssetType == AssetType.IndustrialArea || row.AssetType == AssetType.CommercialOfficeArea || row.AssetType == AssetType.Hotel || row.AssetType == AssetType.Farm) && (row.Reason == AssetReason.Replace || row.Reason == AssetReason.Repossession))
                {
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                }
                else if ((row.AssetType == AssetType.Land || row.AssetType == AssetType.Inhabitance || row.AssetType == AssetType.Garage ||
                    row.AssetType == AssetType.IndustrialArea || row.AssetType == AssetType.CommercialOfficeArea || row.AssetType == AssetType.Hotel || row.AssetType == AssetType.Farm) && row.Reason == AssetReason.RepossessNewAsset)
                {
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, true);

                }
                else if (row.AssetType == AssetType.TransportVehicles && (row.Reason == AssetReason.Replace || row.Reason == AssetReason.Repossession))
                {
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                }
                else if (row.AssetType == AssetType.TransportVehicles && row.Reason == AssetReason.RepossessNewAsset)
                {
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                }
                else if ((row.AssetType == AssetType.Equipment || row.AssetType == AssetType.OtherNonRealEstate) && row.Reason == AssetReason.RepossessNewAsset)
                {
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                }
                else if ((row.AssetType == AssetType.Equipment || row.AssetType == AssetType.OtherNonRealEstate) && (row.Reason == AssetReason.Repossession || row.Reason == AssetReason.Replace))
                {
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, true);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                }
                else
                {
                    PXUIFieldAttribute.SetVisible<NAsset.plateNumber>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.propertyID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.ownerID>(sender, row, false);
                    PXUIFieldAttribute.SetVisible<NAsset.cadastralCode>(sender, row, false);
                }
            }
        }

        protected virtual void RAAsset_ExpectedRepossessionCuryRate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RAAsset row = (RAAsset)e.Row;
            if (row == null) return;
            row.FixedRate = true;
        }
        #endregion

        #region Actions
        [PXButton()]
        [PXUIField(DisplayName = Descriptor.Messages.Add, MapEnableRights = PXCacheRights.Insert)]
        public virtual IEnumerable addAsset(PXAdapter adapter)
        {
            NewAsset.AskExt();
            return adapter.Get();
        }


        [PXButton(OnClosingPopup = PXSpecialButtonType.Cancel, CommitChanges = true)]
        [PXUIField(DisplayName = Descriptor.Messages.GetDataFromApi, MapEnableRights = PXCacheRights.Insert)]
        public virtual void getDataFromApi()
        {
            OpenCreatedAsset();
        }


        [PXButton(OnClosingPopup = PXSpecialButtonType.Cancel, CommitChanges = true)]
        [PXUIField(DisplayName = Descriptor.Messages.Next, MapEnableRights = PXCacheRights.Insert)]
        public virtual IEnumerable openWizard(PXAdapter adapter)
        {
            AssetWizard.AskExt();
            return adapter.Get();
        }

        public PXAction<RARepossessionApplication> Wnext;
        [PXButton(OnClosingPopup = PXSpecialButtonType.Cancel, CommitChanges = true)]
        [PXUIField(MapEnableRights = PXCacheRights.Update, Visible = false)]
        public virtual IEnumerable wnext(PXAdapter adapter)
        {
            var row = AssetWizard.Current;
            var realEstateRow = RealEstateWizard.Current;
            var nonRealEstateRow = NonRealEstateWizard.Current;

            bool errorHappened = false;

            PXCache realEstateCache = RealEstateWizard.Cache;
            PXCache nonRealEstateCache = NonRealEstateWizard.Cache;

            var realEstate = row.AssetType == AssetType.Land || row.AssetType == AssetType.Inhabitance || row.AssetType == AssetType.Garage ||
                row.AssetType == AssetType.IndustrialArea || row.AssetType == AssetType.CommercialOfficeArea || row.AssetType == AssetType.Hotel || row.AssetType == AssetType.Farm;

            var NonRealEstate = row.AssetType == AssetType.Equipment || row.AssetType == AssetType.OtherNonRealEstate || row.AssetType == AssetType.TransportVehicles;
            errorHappened = CheckAssetFields(row, realEstateRow, nonRealEstateRow, errorHappened, realEstate, NonRealEstate);

            #region Loan Check
            var loans = LoanWizard.Select();
            if (loans.Count > 0)
            {
                foreach (LoanItem item in loans)
                {
                    if (item.Validated != true && !string.IsNullOrEmpty(item.LMSID))
                    {
                        LoanWizard.Cache.RaiseExceptionHandling<LoanItem.lMSID>(item, item.LMSID,
                    new PXSetPropertyException("Check LMS ID and try again", PXErrorLevel.RowError));
                        errorHappened = true;
                    }
                }
            }
            #endregion

            #region Allocation Ckeck
            var allocations = Allocation.Cache.Cached;
            AssetAllocationsWizard.Current.Validated = null;
            bool error = false;
            if (Allocation.Select().Count > 0)
            {
                foreach (RAAssetAllocation item in allocations)
                {
                    var currentRowLoan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Required<RAAssetAllocation.loanID>>>>.Select(this, item.LoanID);
                    if (item.PaidPrincipleCury > currentRowLoan.LoanAmtCury)
                    {
                        Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPrincipleCury>(item, item.PaidPrincipleCury,
                  new PXSetPropertyException(Descriptor.Messages.PaidPrincipleValidation, PXErrorLevel.RowError));
                        AssetAllocationsWizard.Current.Validated = false;

                    }
                    else if (item.PaidInterestCury > currentRowLoan.OverdueInterestCury)
                    {
                        Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidInterestCury>(item, item.PaidInsuranceCury,
                                new PXSetPropertyException(Descriptor.Messages.PaidInterestValidation, PXErrorLevel.RowError));
                        AssetAllocationsWizard.Current.Validated = false;

                    }
                    else if (item.PaidPenaltyCury > currentRowLoan.OverduePenaltyCury)
                    {
                        Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPenaltyCury>(item, item.PaidPenaltyCury,
                                              new PXSetPropertyException(Descriptor.Messages.PaidPenaltyValidation, PXErrorLevel.RowError));
                        AssetAllocationsWizard.Current.Validated = false;

                    }
                    else if (item.PaidInsuranceCury > currentRowLoan.OverdueInsuranceCury)
                    {
                        Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidInsuranceCury>(item, item.PaidInsuranceCury,
                                        new PXSetPropertyException(Descriptor.Messages.PaidInsuranceValidation, PXErrorLevel.RowError));
                        AssetAllocationsWizard.Current.Validated = false;

                    }
                    validateExpectedAmount(item, out error);
                    if (error)
                    {
                        AssetAllocationsWizard.Current.Validated = false;
                    }
                    else
                    {
                        AssetAllocationsWizard.Current.Validated = AssetAllocationsWizard.Current.Validated != false ? AssetAllocationsWizard.Current.Validated = true : AssetAllocationsWizard.Current.Validated;
                    }
                    if (AssetAllocationsWizard.Current.Validated == false) break;
                }
            }

            if (AssetAllocationsWizard.Current != null && AssetAllocationsWizard.Current.Validated == false)
            {
                AssetAllocationsWizard.Cache.RaiseExceptionHandling<AssetAllocationWzrd.validated>(AssetAllocationsWizard.Current, AssetAllocationsWizard.Current.Validated,
                 new PXSetPropertyException(Descriptor.Messages.PaidPrincipleValidation, PXErrorLevel.Error));
                errorHappened = true;
            }

            #endregion

            if (errorHappened)
            {
                return adapter.Get();
            }
            else
            {
                this.Save.Press();
                RAAssetEntry assetEntry = CreateInstance<RAAssetEntry>();
                var asset = AssetWizard.Current;

                assetEntry.Assets.Cache.SetValueExt<RAAsset.classID>(asset, NewAsset.Current.ClassID);
                asset.Reason = NewAsset.Current.Reason;
                asset.PropertyID = NewAsset.Current.PropertyID;

                assetEntry.Assets.Update(asset);

                RealEstateWizard.Current.CadastralCode = NewAsset.Current.CadastralCode;
                NonRealEstateWizard.Current.PlateNumber = NewAsset.Current.PlateNumber;
                if (AssetWizard.Current.AssetID == null || AssetWizard.Current.AssetID < 0)
                {

                    assetEntry.Assets.Insert(asset);
                    assetEntry.RealEstate.Insert(RealEstateWizard.Current);
                    assetEntry.NonRealEstate.Insert(NonRealEstateWizard.Current);
                    assetEntry.Save.Press();
                    AssetWizard.Current.AssetID = assetEntry.Assets.Current.AssetID;
                }
                else
                {
                    RealEstateWizard.Current.AssetID = asset.AssetID;
                    NonRealEstateWizard.Current.AssetID = asset.AssetID;
                    assetEntry.Assets.Update(asset);
                    assetEntry.RealEstate.Update(RealEstateWizard.Current);
                    assetEntry.NonRealEstate.Update(NonRealEstateWizard.Current);
                    assetEntry.Save.Press();
                }

            }
            return adapter.Get();
        }

        public int GetNextPage(int page)
        {
            if (NewAsset.Current != null && NewAsset.Current.Reason == AssetReason.Repossession && page == 0)
            {
                return 2;
            }
            else if (NewAsset.Current != null && NewAsset.Current.Reason == AssetReason.Repossession && page == 2)
            {
                return 3;
            }
            else if (NewAsset.Current != null && NewAsset.Current.Reason == AssetReason.RepossessNewAsset && page == 0)
            {
                return 1;
            }
            else if (NewAsset.Current != null && NewAsset.Current.Reason == AssetReason.RepossessNewAsset && page == 1)
            {
                return 2;
            }
            return 3;
        }

        private bool CheckAssetFields(RAAsset row, RARealEstate realEstateRow, RANonRealEstate nonRealEstateRow, bool errorHappened, bool realEstate, bool NonRealEstate)
        {
            if (row.RepossessionType == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.repossessionType>(row, row.RepossessionType,
                        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.Description == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.description>(row, row.Description,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.ExecutionType == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.executionType>(row, row.ExecutionType,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (row.InspectionForm == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.inspectionForm>(row, row.InspectionForm,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.AssetClassification == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.assetClassification>(row, row.AssetClassification,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.InstalmentStartDate == null && row.PropertyRedeem == true)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.instalmentStartDate>(row, row.InstalmentStartDate,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.InstalmentEndDate == null && row.PropertyRedeem == true)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.instalmentEndDate>(row, row.InstalmentEndDate,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.Option30PercProjectCurrency == null && row.AssetClassification == AssetClassification.ThirtyPercentProject)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.option30PercProjectAmountInCurrency>(row, row.Option30PercProjectCurrency,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.Option30PercProjectAmountInCurrency == null && row.AssetClassification == AssetClassification.ThirtyPercentProject)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.option30PercProjectAmountInCurrency>(row, row.Option30PercProjectAmountInCurrency,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.legalExpenses == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.LegalExpenses>(row, row.legalExpenses,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.RARecommendator == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.rARecommendator>(row, row.RARecommendator,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.ExpectedRepossessionCuryID == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.expectedRepossessionCuryID>(row, row.ExpectedRepossessionCuryID,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            if (row.ExpectedRepossessionCuryRate == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.expectedRepossessionCuryRate>(row, row.ExpectedRepossessionCuryRate,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (row.ExpectedRepossessionAmt == null)
            {
                AssetWizard.Cache.RaiseExceptionHandling<RAAsset.expectedRepossessionAmt>(row, row.ExpectedRepossessionAmt,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.Region == null && realEstate)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.region>(realEstateRow, realEstateRow.Region,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.TbilisiDistrict == null && realEstate)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.tbilisiDistrict>(realEstateRow, realEstateRow.TbilisiDistrict,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.InformationSource == null && realEstate)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.informationSource>(realEstateRow, realEstateRow.InformationSource,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            else if (realEstateRow.Water == null && realEstate)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.water>(realEstateRow, realEstateRow.Water,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.RoadType == null && realEstate)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.roadType>(realEstateRow, realEstateRow.RoadType,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.AreaCondition == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.areaCondition>(realEstateRow, realEstateRow.AreaCondition,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.RoomQuantity == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.roomQuantity>(realEstateRow, realEstateRow.RoomQuantity,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.FloorQuantity == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.floorQuantity>(realEstateRow, realEstateRow.FloorQuantity,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.Floor == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.floor>(realEstateRow, realEstateRow.Floor,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.StatusOfConstruction == null && realEstate&& row.AssetType!= AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.statusOfConstruction>(realEstateRow, realEstateRow.StatusOfConstruction,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.OwnerUseBuildingAsHabitat == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.ownerUseBuildingAsHabitat>(realEstateRow, realEstateRow.OwnerUseBuildingAsHabitat,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.Rented == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.rented>(realEstateRow, realEstateRow.Rented,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.MonthlyRentFeeCury == null && realEstate && realEstateRow.Rented == YesNoList.Yes && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.monthlyRentFeeCury>(realEstateRow, realEstateRow.MonthlyRentFeeCury,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.CuryID == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.curyID>(realEstateRow, realEstateRow.CuryID,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.ApartmentInItalianYard == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.apartmentInItalianYard>(realEstateRow, realEstateRow.ApartmentInItalianYard,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.Bathroom == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.bathroom>(realEstateRow, realEstateRow.Bathroom,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
             if (realEstateRow.Toilet == null && realEstate && row.AssetType != AssetType.Land)
            {
                RealEstateWizard.Cache.RaiseExceptionHandling<RARealEstate.toilet>(realEstateRow, realEstateRow.Toilet,
                    new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
                errorHappened = true;
            }
            // if (nonRealEstateRow.VehicleCondition == null && NonRealEstate)
            //{
            //    NonRealEstateWizard.Cache.RaiseExceptionHandling<RANonRealEstate.vehicleCondition>(nonRealEstateRow, nonRealEstateRow.VehicleCondition,
            //        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
            //    errorHappened = true;
            //}
            // if (nonRealEstateRow.CustomsDuty == null && NonRealEstate)
            //{
            //    NonRealEstateWizard.Cache.RaiseExceptionHandling<RANonRealEstate.customsDuty>(nonRealEstateRow, nonRealEstateRow.CustomsDuty,
            //        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
            //    errorHappened = true;
            //}
            // if (nonRealEstateRow.Transmission == null && NonRealEstate)
            //{
            //    NonRealEstateWizard.Cache.RaiseExceptionHandling<RANonRealEstate.transmission>(nonRealEstateRow, nonRealEstateRow.Transmission,
            //        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
            //    errorHappened = true;
            //}
            // if (nonRealEstateRow.Mileage == null && NonRealEstate)
            //{
            //    NonRealEstateWizard.Cache.RaiseExceptionHandling<RANonRealEstate.mileage>(nonRealEstateRow, nonRealEstateRow.Mileage,
            //        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
            //    errorHappened = true;
            //}
            // if (nonRealEstateRow.WheelLocation == null && NonRealEstate)
            //{
            //    NonRealEstateWizard.Cache.RaiseExceptionHandling<RANonRealEstate.wheelLocation>(nonRealEstateRow, nonRealEstateRow.WheelLocation,
            //        new PXSetPropertyException(ErrorMessages.FieldIsEmpty, PXErrorLevel.RowError));
            //    errorHappened = true;
            //}

            return errorHappened;
        }

        public PXAction<RARepossessionApplication> OpenAsset;
        [PXButton(OnClosingPopup = PXSpecialButtonType.Cancel, CommitChanges = true)]
        [PXUIField(DisplayName = Descriptor.Messages.Next, MapEnableRights = PXCacheRights.Insert)]
        public virtual IEnumerable openAsset(PXAdapter adapter)
        {
            OpenCreatedAsset();
            return adapter.Get();
        }

        private void OpenCreatedAsset()
        {
            RAAssetEntry assetEntry = CreateInstance<RAAssetEntry>();
            assetEntry.Assets.Current = assetEntry.Assets.Search<RAAsset.assetID>(AssetWizard.Current.AssetID);
            if (assetEntry.Assets.Current != null)
            {
                assetEntry.Assets.Current.RepossessionApplicationID = RepossessionApplication.Current.RepossessionApplicationID;
            }
            assetEntry.Assets.Update(assetEntry.Assets.Current);
            assetEntry.Save.Press();
            //AssetAllocationsWizard.Current = new AssetAllocationWzrd();
            //NewAsset.Current.ClassID = null;
            //NewAsset.Current.Reason = null;
            Clear();
            PXRedirectHelper.TryRedirect(assetEntry, PXRedirectHelper.WindowMode.NewWindow);
        }

        #endregion
        
        public class LoanItem : PX.Data.IBqlTable
        {
            #region LMSID
            public abstract class lMSID : PX.Data.IBqlField
            {
            }
            protected string _LMSID;
            [PXString(50, IsUnicode = true)]
            [PXUIField(DisplayName = "LMS ID")]
            public virtual string LMSID
            {
                get
                {
                    return this._LMSID;
                }
                set
                {
                    this._LMSID = value;
                }
            }
            #endregion
            #region Validated
            public abstract class validated : PX.Data.IBqlField
            {
            }
            protected bool? _Validated;
            [PXBool()]
            [PXUIField(DisplayName = "Validated")]
            public virtual bool? Validated
            {
                get
                {
                    return this._Validated;
                }
                set
                {
                    this._Validated = value;
                }
            }
            #endregion
        }

        public class AssetAllocationWzrd : PX.Data.IBqlTable
        {
            #region Validated
            public abstract class validated : PX.Data.IBqlField
            {
            }
            protected bool? _Validated;
            [PXBool()]
            [PXUIField(DisplayName = "Validated", Visible = false)]
            public virtual bool? Validated
            {
                get
                {
                    return this._Validated;
                }
                set
                {
                    this._Validated = value;
                }
            }
            #endregion
        }

    }
}
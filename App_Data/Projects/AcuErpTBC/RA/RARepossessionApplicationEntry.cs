using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CM;
using RA._Shared;
using PX.Objects.EP;
using PX.Objects.CR;
using RA._Shared.LoanStatus;
using PX.Common;
using System.Linq;


namespace RA
{
    public partial class RARepossessionApplicationEntry : RAGraph<RARepossessionApplicationEntry, RARepossessionApplication>
    {
        #region Data Views
        public PXSelect<RARepossessionApplication> RepossessionApplication;

        public PXSelect<
            RARepossessionApplication,
            Where<RARepossessionApplication.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>>
            RepossessionApplicationCurrent;

        public PXSetup<RASetup> Setup;

        public PXSelectJoin<
            RARepossessionApplicationAsset,
            InnerJoin<RAAsset,
                On<RARepossessionApplicationAsset.assetID, Equal<RAAsset.assetID>>>,
            Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>,
            OrderBy<
                Desc<RARepossessionApplicationAsset.repossessionApplicationAssetID>>>
            RepossessionApplicationAsset;

        public PXSelectJoin<
            RAEvaluation,
            InnerJoin<RARepossessionApplicationAsset,
                On<RAEvaluation.assetID, Equal<RARepossessionApplicationAsset.assetID>>>,
            Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>,
            OrderBy<
                Asc<RAEvaluation.evaluationID>>>
            Evaluation;

        public PXSelectJoin<
            RALoan,
            InnerJoin<RAAssetLoan,
                On<RALoan.loanID, Equal<RAAssetLoan.loanID>>,
            InnerJoin<RARepossessionApplicationAsset,
                On<RAAssetLoan.assetID, Equal<RARepossessionApplicationAsset.assetID>>>>,
            Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>,
            OrderBy<
                Asc<RALoan.loanID>>>
            Loan;

        public PXSelectJoin<
            RAAssetAllocation,
            InnerJoin<RALoan,
                On<RAAssetAllocation.loanID, Equal<RALoan.loanID>>,
            InnerJoin<RAAsset,
                On<RAAssetAllocation.assetID, Equal<RAAsset.assetID>>,
            InnerJoin<RAAssetLoan,
                On<RAAssetAllocation.loanID, Equal<RAAssetLoan.loanID>,
                And<RAAssetAllocation.assetID, Equal<RAAssetLoan.assetID>>>>>>,
            Where<RAAssetAllocation.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>,
            OrderBy<
                Asc<RAAssetAllocation.assetID>>>
            Allocation;

        [PXVirtualDAC]
        public PXSelect<RAAsset> Asset;

        [PXCopyPasteHiddenView]
        public PXFilter<NAsset> NewAsset;

        public PXFilter<MessageDialog> Dialog;

      
        #endregion

        #region Actions
        public PXAction<RARepossessionApplication> actionsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Actions")]
        protected virtual void ActionsMenu()
        {
        }

      

        public PXDBAction<RARepossessionApplication> deleteAsset;
        [PXUIField(DisplayName = Descriptor.Messages.Delete)]
        [PXButton(ImageKey = PX.Web.UI.Sprite.Main.DataEntry)]
        public virtual void DeleteAsset()
        {
            var row = RepossessionApplicationAsset.Current;
            if (row == null) return;
            if (Dialog.AskExt() == WebDialogResult.OK)
            {
                RAAssetEntry assetEntry = PXGraph.CreateInstance<RAAssetEntry>();
                RAAsset asset = (RAAsset)PXSelect<RAAsset, Where<RAAsset.assetID, Equal<Required<RAAsset.assetID>>>>.Select(this, row.AssetID);

                assetEntry.Assets.Delete(asset);
                assetEntry.Actions.PressSave();
                RepossessionApplicationAsset.Delete(row);
                this.Save.Press();
                RepossessionApplicationAsset.View.RequestRefresh();
            }
        }

        #endregion

        #region Events
        protected virtual void RARepossessionApplication_RowSelected(PXCache sender, PXRowSelectedEventArgs e)
        {
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            if (row == null) return;

            responseFromRAManager.SetEnabled(row.Status == AssetStatus.WaitingRAManager);

            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overdueInsuranceCury>(sender, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overdueInterestCury>(sender, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overduePenaltyCury>(sender, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.overduePrincipleCury>(sender, null, true);
            PXDBCurrencyAttribute.SetBaseCalc<RALoan.loanAmtCury>(sender, null, true);
        }

        protected virtual void RAAssetAllocation_PaidInsuranceCury_FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            RAAssetAllocation row = (RAAssetAllocation)e.Row;
            if (row == null) return;

            RALoan loan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);

            var allocations = PXSelect<RAAssetAllocation, Where<RAAssetAllocation.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var tempOverdueInsuranceCury = loan.OverdueInsuranceCury;
            foreach (var item in allocations)
            {
                tempOverdueInsuranceCury -= ((RAAssetAllocation)item).PaidInsuranceCury;
                row.RemainingInsurance = tempOverdueInsuranceCury;
            }
        }

        protected virtual void RAAssetAllocation_PaidInterestCury_FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            RAAssetAllocation row = (RAAssetAllocation)e.Row;
            if (row == null) return;

            RALoan loan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var allocations = PXSelect<RAAssetAllocation, Where<RAAssetAllocation.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var tempOverdueInterestCury = loan.OverdueInterestCury;
            foreach (var item in allocations)
            {
                tempOverdueInterestCury -= ((RAAssetAllocation)item).PaidInterestCury;
                row.RemainingInterest = tempOverdueInterestCury;
            }
        }

        protected virtual void RAAssetAllocation_PaidPenaltyCury_FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            RAAssetAllocation row = (RAAssetAllocation)e.Row;
            if (row == null) return;

            RALoan loan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var allocations = PXSelect<RAAssetAllocation, Where<RAAssetAllocation.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var tempOverduePenaltyCury = loan.OverduePenaltyCury;
            foreach (var item in allocations)
            {
                tempOverduePenaltyCury -= ((RAAssetAllocation)item).PaidPenaltyCury;
                row.RemainingPenalty = tempOverduePenaltyCury;
            }
        }

        protected virtual void RAAssetAllocation_PaidPrincipleCury_FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            RAAssetAllocation row = (RAAssetAllocation)e.Row;
            if (row == null) return;

            RALoan loan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var allocations = PXSelect<RAAssetAllocation, Where<RAAssetAllocation.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            var tempOverduePrincipleCury = loan.OverduePrincipleCury;
            foreach (var item in allocations)
            {
                tempOverduePrincipleCury -= ((RAAssetAllocation)item).PaidPrincipleCury;
                row.RemainingPrinciple = tempOverduePrincipleCury;
            }
        }

        protected virtual void RAAssetAllocation_RowUpdating(PXCache sender, PXRowUpdatingEventArgs e)
        {
            RAAssetAllocation newRow = (RAAssetAllocation)e.NewRow;
            if (newRow == null) return;
            checkRowValidation(newRow);
            bool errorHappened = false;
            validateExpectedAmount(newRow,out errorHappened);
        }

        protected virtual void RAAssetAllocation_RowPersisting(PXCache sender, PXRowPersistingEventArgs e)
        {
            RAAssetAllocation row = (RAAssetAllocation)e.Row;
            if (row == null) return;
            checkRowValidation(row);
            bool errorHappened = false;
            validateExpectedAmount(row, out errorHappened);
        }

        private void checkRowValidation(RAAssetAllocation row)
        {
            var currentRowLoan = (RALoan)PXSelectReadonly<RALoan, Where<RALoan.loanID, Equal<Current<RAAssetAllocation.loanID>>>>.Select(this);
            if (currentRowLoan == null) return;
            if (row.PaidPrincipleCury > currentRowLoan.LoanAmtCury)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPrincipleCury>(row, row.PaidPrincipleCury,
                new PXSetPropertyException(Descriptor.Messages.PaidPrincipleValidation, PXErrorLevel.RowError));
            }
            if (row.PaidInterestCury > currentRowLoan.OverdueInterestCury)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidInterestCury>(row, row.PaidInterestCury,
                new PXSetPropertyException(Descriptor.Messages.PaidInterestValidation, PXErrorLevel.RowError));
            }
            if (row.PaidPenaltyCury > currentRowLoan.OverduePenaltyCury)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPenaltyCury>(row, row.PaidPenaltyCury,
                new PXSetPropertyException(Descriptor.Messages.PaidPenaltyValidation, PXErrorLevel.RowError));
            }
            if (row.PaidInsuranceCury > currentRowLoan.OverdueInsuranceCury)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidInsuranceCury>(row, row.PaidInsuranceCury,
                new PXSetPropertyException(Descriptor.Messages.PaidInsuranceValidation, PXErrorLevel.RowError));
            }
        }

        private void validateExpectedAmount(RAAssetAllocation allocation,out bool errorHappened)
        {
            errorHappened = false;
            var assetAllocations = Allocation.Select();
            var expectedRepossessionAmt = ((RAAsset)PXSelectReadonly<RAAsset, Where<RAAsset.assetID, Equal<Required<RAAssetAllocation.assetID>>>>.Select(this, allocation.AssetID)[0])?.ExpectedRepossessionAmtGel;
            decimal? assetAllocationAcceptableDeviationPct = Setup.Current.AssetAllocationAcceptableDeviationPct;
            if (assetAllocationAcceptableDeviationPct == null)
            {
                throw new PXException(Descriptor.Messages.AssetAllocationAcceptableDeviationMissing);
            }
            decimal? assetAllocationAcceptableDeviation = assetAllocationAcceptableDeviationPct / 100;
            var minimalAcceptablePaidAmount = expectedRepossessionAmt - expectedRepossessionAmt * assetAllocationAcceptableDeviation;
            if (minimalAcceptablePaidAmount != null)
            {
                minimalAcceptablePaidAmount = Decimal.Round((decimal)minimalAcceptablePaidAmount, 2);
            }
            decimal? totalPaidAmount = 0;

            foreach (RAAssetAllocation item in assetAllocations)
            {
                totalPaidAmount += item.PaidPrinciple;
                totalPaidAmount += item.PaidInterest;
                totalPaidAmount += item.PaidPenalty;
                totalPaidAmount += item.PaidInsurance;
                totalPaidAmount += item.PaidCourtFee;
            }

            if (totalPaidAmount < minimalAcceptablePaidAmount)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPrincipleCury>(allocation, allocation.PaidPrincipleCury,
      new PXSetPropertyException(Descriptor.Messages.PaidAmountTooLow, PXErrorLevel.RowError));
                errorHappened = true;
            }
            else if (totalPaidAmount > expectedRepossessionAmt)
            {
                Allocation.Cache.RaiseExceptionHandling<RAAssetAllocation.paidPrincipleCury>(allocation, allocation.PaidPrincipleCury,
        new PXSetPropertyException(Descriptor.Messages.PaidAmountTooHigh, PXErrorLevel.RowError));
                errorHappened = true;
            }
        }

        public PXAction<RARepossessionApplication> sendToRAManager;
        [PXUIField(DisplayName = Descriptor.Messages.SendToRAManager, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable SendToRAManager(PXAdapter adapter)
        {
            try
            {
                RARepossessionApplication doc = this.RepossessionApplication.Current;
                if (doc == null) return adapter.Get();

                PXLongOperation.StartOperation(this, delegate ()
                {
                    RAAssetEntry raAssetEntry = CreateInstance<RAAssetEntry>();

                    using (var tran = new PXTransactionScope())
                    {
                        doc.Status = AssetStatus.WaitingRAManager;
                        this.RepossessionApplication.Update(doc);

                        foreach (PXResult<RARepossessionApplicationAsset, RAAsset> res in RepossessionApplicationAsset.Select())
                        {
                            RAAsset item = res;
                            item.Status = AssetStatus.WaitingRAManager;
                            raAssetEntry.Assets.Update(item);
                        }
                        raAssetEntry.Save.Press();
                        this.Save.Press();
                        tran.Complete();
                    }
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RARepossessionApplication> responseFromRAManager;
        [PXUIField(DisplayName = Descriptor.Messages.ResponseFromRAManager, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable ResponseFromRAManager(PXAdapter adapter)
        {
            using (var tran = new PXTransactionScope())
            {
                RARepossessionApplication doc = this.RepossessionApplication.Current;
                RAAssetEntry assetEntry = CreateInstance<RAAssetEntry>();

                foreach (PXResult<RARepossessionApplicationAsset, RAAsset> item in this.RepossessionApplicationAsset.Select())
                {
                    var repossessionApplicationAsset = ((RARepossessionApplicationAsset)item);
                    var asset = ((RAAsset)item);
                    if (asset.RecommendedRepossessionAmt == null || asset.RecommendedRepossessionAmt <= 0)
                    {
                        RepossessionApplicationAsset.Cache.RaiseExceptionHandling<RARepossessionApplicationAsset.assetID>(repossessionApplicationAsset, repossessionApplicationAsset.AssetID,
                                      new PXSetPropertyException(Descriptor.Messages.RecommendedAmountCannotBeEmpty, PXErrorLevel.RowError));
                        return adapter.Get();

                    }
                    asset.Status = AssetStatus.RespondedByRAManager;
                    assetEntry.Assets.Update(asset);
                }
                doc.Status = AssetStatus.RespondedByRAManager;
                this.RepossessionApplication.Update(doc);
                this.Save.Press();
                assetEntry.Save.Press();
                tran.Complete();
            }
            return adapter.Get();
        }


        public PXAction<RARepossessionApplication> fillAllocation;
        [PXUIField(DisplayName = "Fill", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable FillAllocation(PXAdapter adapter)
        {
            try
            {
                foreach (PXResult<RALoan, RAAssetLoan, RARepossessionApplicationAsset> res in Loan.Select())
                {
                    RAAssetLoan assetLoan = res;

                    PXResultset<RAAssetAllocation> row = this.Allocation.Search<RAAssetAllocation.assetID, RAAssetAllocation.loanID>(assetLoan.AssetID, assetLoan.LoanID);
                    if (row.Count == 0)
                    {
                        RAAsset raAsset = this.Asset.Search<RAAsset.assetID>(assetLoan.AssetID);

                        if (raAsset != null && raAsset.Reason != AssetReason.Replace)
                        {
                            RAAssetAllocation assetAllocationObj = new RAAssetAllocation();
                            assetAllocationObj.RepossessionApplicationID = this.RepossessionApplication.Current.RepossessionApplicationID;
                            assetAllocationObj.AssetID = assetLoan.AssetID;
                            assetAllocationObj.LoanID = assetLoan.LoanID;

                            this.Allocation.Insert(assetAllocationObj);
                        }
                    }
                }
                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RARepossessionApplication> release;
        [PXUIField(DisplayName = "Release", MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable Release(PXAdapter adapter)
        {
            try
            {
                RARepossessionApplication doc = this.RepossessionApplication.Current;
                if (doc == null) return adapter.Get();

                PXLongOperation.StartOperation(this, delegate ()
                {
                    RAAssetEntry raAssetEntry = CreateInstance<RAAssetEntry>();

                    using (var tran = new PXTransactionScope())
                    {
                        doc.Status = AssetStatus.PendingForApproval;
                        this.RepossessionApplication.Update(doc);

                        foreach (PXResult<RARepossessionApplicationAsset, RAAsset> res in RepossessionApplicationAsset.Select())
                        {
                            RAAsset item = res;
                            raAssetEntry.Recalculation(item);
                            item.Status = AssetStatus.PendingForApproval;
                            raAssetEntry.Assets.Update(item);
                        }
                        raAssetEntry.Save.Press();
                        this.Save.Press();
                        tran.Complete();
                    }
                });

                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }

        public PXAction<RARepossessionApplication> currencyRecalculation;
        [PXUIField(DisplayName = Descriptor.Messages.Recalculation, MapEnableRights = PXCacheRights.Update, MapViewRights = PXCacheRights.Update)]
        [PXProcessButton]
        public virtual IEnumerable CurrencyRecalculation(PXAdapter adapter)
        {
            try
            {
                PXLongOperation.StartOperation(this, delegate ()
                {
                    RAAssetEntry assetEntry = PXGraph.CreateInstance<RAAssetEntry>();
                    foreach (PXResult<RARepossessionApplicationAsset, RAAsset> item in this.RepossessionApplicationAsset.Select())
                    {
                        var asset = (RAAsset)item;
                        assetEntry.Recalculation(asset);
                    }
                });
                return adapter.Get();
            }
            catch (Exception ex)
            {
                throw new PXException(ex.Message, ex);
            }
        }


        #endregion

        #region ctor

        public RARepossessionApplicationEntry()
        {
            actionsMenu.AddMenuAction(sendToRAManager);
            actionsMenu.AddMenuAction(responseFromRAManager);
            actionsMenu.AddMenuAction(currencyRecalculation);
            actionsMenu.AddMenuAction(release);
            reportsMenu.AddMenuAction(printRA);
        }

        #endregion

        #region Reports
        public PXAction<RARepossessionApplication> reportsMenu;
        [PXButton(CommitChanges = true, MenuAutoOpen = true)]
        [PXUIField(DisplayName = "Reports")]
        protected virtual void ReportsMenu()
        {

        }

        public PXAction<RARepossessionApplication> printRA;
        [PXUIField(DisplayName = "Print RA Application", MapViewRights = PXCacheRights.Select, MapEnableRights = PXCacheRights.Select)]
        [PXButton()]
        public virtual IEnumerable PrintRA(PXAdapter adapter)
        {
            if (RepossessionApplicationCurrent.Current != null)
            {
                Dictionary<string, string> info = new Dictionary<string, string>();
                info["RepossessionApplicationID"] = RepossessionApplicationCurrent.Current.RepossessionApplicationID.ToString();
                throw new PXReportRequiredException(info, "RA602000", "Print RA");
            }
            return adapter.Get();
        }


        #endregion


        #region Events
        protected virtual void RARepossessionApplication_Hold_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            if (row == null) return;
            sender.SetValueExt<RARepossessionApplication.hold>(row, Setup.Current.HoldRepossession == true);
            row.Status = row.Hold == true ? AssetStatus.Draft : AssetStatus.ReadyForEvaluation;

        }

        protected virtual void RARepossessionApplication_Hold_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RARepossessionApplication row = RepossessionApplication.Current;
            if (row == null) return;
            row.Status = row.Hold == true ? AssetStatus.Draft : AssetStatus.ReadyForEvaluation;
        }

        protected virtual void RARepossessionApplication_ReportsTo_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            RARepossessionApplication row = e.Row as RARepossessionApplication;
            if (row == null) return;
            SetReportsTo(row);
        }

        protected virtual void RARepossessionApplication_LoanManager_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RARepossessionApplication row = e.Row as RARepossessionApplication;
            if (row == null) return;
            SetReportsTo(row);
        }

        private void SetReportsTo(RARepossessionApplication row)
        {
            var employee = (EPEmployee)PXSelectReadonly<EPEmployee, Where<EPEmployee.userID, Equal<Required<RARepossessionApplication.loanManager>>>>.Select(this, row.LoanManager);
            if (employee != null)
            {

                var reportsTo = (EPEmployee)PXSelectReadonly<EPEmployee, Where<EPEmployee.bAccountID, Equal<Required<EPEmployee.bAccountID>>>>.Select(this, employee.SupervisorID);
                if (reportsTo != null)
                {
                    row.ReportsTo = reportsTo.UserID;
                }
            }
        }

        protected virtual void RARepossessionApplicationAsset_RowPersisting(PXCache sender, PXRowPersistingEventArgs e)
        {
            RARepossessionApplicationAsset row = (RARepossessionApplicationAsset)e.Row;
            if (row == null) return;
            foreach (RARepossessionApplicationAsset item in RepossessionApplicationAsset.Select())
            {
                if (item.AssetID == row.AssetID && item.RepossessionApplicationAssetID != row.RepossessionApplicationAssetID)
                {
                    sender.RaiseExceptionHandling<RARepossessionApplicationAsset.assetID>(row, row.AssetID, new PXSetPropertyException(RA.Descriptor.Messages.AssetCanNotBeDuplicate, PXErrorLevel.Error));

                }
            }
            this.RepossessionApplication.Cache.SetDefaultExt<RARepossessionApplication.lMRemopossessionAmt>(RepossessionApplication.Current);
        }

        protected virtual void NAsset_AssetType_FieldDefaulting(PXCache sender, PXFieldDefaultingEventArgs e)
        {
            NAsset row = e.Row as NAsset;
            if (row == null) return;
            SetAssetType(row);
        }

        protected virtual void NAsset_ClassID_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            NAsset row = e.Row as NAsset;
            if (row == null) return;
            SetAssetType(row);
        }

        private void SetAssetType(NAsset row)
        {
            var raClass = (RAClass)PXSelectReadonly<RAClass, Where<RAClass.classID, Equal<Required<NAsset.classID>>>>.Select(this, row.ClassID);
            if (raClass != null)
            {
                row.AssetType = raClass.AssetType;
            }
        }

        protected virtual void RARepossessionApplication_Status_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            RARepossessionApplication row = e.Row as RARepossessionApplication;
            UpdateLoanStatus(row);
        }

        private void UpdateLoanStatus(RARepossessionApplication row)
        {
            string loanStatus = "";
            if (row == null) return;
            else
            {
                RARepossessionApplicationEntry repossessionApplicationEntry = CreateInstance<RARepossessionApplicationEntry>();
                switch (row.Status)
                {
                    case AssetStatus.Draft:
                    case AssetStatus.ReadyForEvaluation:
                    case AssetStatus.WaitingForEvaluation:
                    case AssetStatus.Evaluated:
                    case AssetStatus.WaitingRAManager:
                    case AssetStatus.RespondedByRAManager:
                        loanStatus = LoanStatus.Active;
                        break;
                    default:
                        loanStatus = LoanStatus.Inactive;
                        break;
                }

                foreach (RALoan item in Loan.Select())
                {
                    item.LoanStatus = loanStatus;
                    Loan.Update(item);
                }

                repossessionApplicationEntry.Save.Press();
            }
        }

        #endregion

        #region CacheAttached

        #region Assets
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Asset Name")]
        protected virtual void RAAsset_Description_CacheAttached(PXCache cache)
        {

        }

        #region RecommendedRepossessionAmt
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Recommended Repossession Amount")]
        protected virtual void RAAsset_RecommendedRepossessionAmt_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region RecommendedRepossessionAmtCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Recommended Repossession Amount In Currency")]
        protected virtual void RAAsset_RecommendedRepossessionAmtCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #endregion

        #region Evaluation 
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Asset ID", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_AssetID_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Status", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_Status_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Estimation Type", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_EstimationType_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Type", Enabled = false)]
        protected virtual void RAEvaluation_EvaluationType_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Expenses", Visible = false)]
        protected virtual void RAEvaluation_EvaluationExpenses_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Approach", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_EvaluationApproach_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Inspect Date", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_InspectDate_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Currency Date", Visible = false, Enabled = false)]
        protected virtual void RAEvaluation_CurrencyDate_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Date", Enabled = false)]
        protected virtual void RAEvaluation_EvaluationDate_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Appraiser", Enabled = false)]
        protected virtual void RAEvaluation_Appraiser_CacheAttached(PXCache cache)
        {

        }


        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Fair/Market Value", Visibility = PXUIVisibility.SelectorVisible, Enabled = false)]
        protected virtual void RAEvaluation_FairMarketValueCury_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Salvage Value", Visibility = PXUIVisibility.SelectorVisible, Enabled = false)]
        protected virtual void RAEvaluation_SalvageValueCury_CacheAttached(PXCache cache)
        {

        }

        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Evaluation Currency", Enabled = false, Visibility = PXUIVisibility.SelectorVisible)]
        protected virtual void RAEvaluation_CuryID_CacheAttached(PXCache cache)
        {

        }
        #endregion

        #region Loan
        #region LoanProduct
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Product", Enabled = false)]
        protected virtual void RALoan_LoanProduct_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanSegment
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Segment", Visible = false, Enabled = false)]
        protected virtual void RALoan_LoanSegment_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanEndDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan End Date", Visible = false, Enabled = false)]
        protected virtual void RALoan_LoanEndDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanDisbursementDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Disbursement Date", Visible = false, Enabled = false)]
        protected virtual void RALoan_LoanDisbursementDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanTransferDateToPLO
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Transfer Date to PLO", Visible = false, Enabled = false)]
        protected virtual void RALoan_LoanTransferDateToPLO_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region ClientID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "ClientID", Visible = false, Enabled = false)]
        protected virtual void RALoan_ClientID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower ID", Visible = false, Enabled = false)]
        protected virtual void RALoan_BorrowerID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerContactInfo
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower Contact Info", Visible = false, Enabled = false)]
        protected virtual void RALoan_BorrowerContactInfo_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region BorrowerInsider
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Borrower Insider", Visible = false, Enabled = false)]
        protected virtual void RALoan_BorrowerInsider_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerName
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower Name", Visible = false, Enabled = false)]
        protected virtual void RALoan_CoBorrowerName_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerID
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower ID", Visible = false)]
        protected virtual void RALoan_CoBorrowerID_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CoBorrowerInsider
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Co-Borrower Insider", Visible = false, Enabled = false)]
        protected virtual void RALoan_CoBorrowerInsider_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 1", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorName1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 1", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorID1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider1
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 1 Insider", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorInsider1_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 2", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorName2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 2", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorID2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider2
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 2 Insider", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorInsider2_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorName3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor Name 3", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorName3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorID3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor ID 3", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorID3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region GuarantorInsider3
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Guarantor 3 Insider", Visible = false, Enabled = false)]
        protected virtual void RALoan_GuarantorInsider3_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverduePrincipleCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Principle", Visibility = PXUIVisibility.SelectorVisible, Visible = false, Enabled = false)]
        protected virtual void RALoan_OverduePrincipleCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverduePenaltyCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Penalty", Visibility = PXUIVisibility.SelectorVisible, Visible = false, Enabled = false)]
        protected virtual void RALoan_OverduePenaltyCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInsuranceCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Insurance", Visibility = PXUIVisibility.SelectorVisible, Visible = false, Enabled = false)]
        protected virtual void RALoan_OverdueInsuranceCury_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInterestCury
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Interest", Visibility = PXUIVisibility.SelectorVisible, Visible = false, Enabled = false)]
        protected virtual void RALoan_OverdueInterestCury_CacheAttached(PXCache cache)
        {
        }
        #region OverduePenalty
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Penalty", Visible = false, Enabled = false)]
        protected virtual void RALoan_OverduePenalty_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region CurrencyDate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXDefault(typeof(AccessInfo.businessDate))]
        protected virtual void RALoan_CurrencyDate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInsurance
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Insurance", Visible = false, Enabled = false)]
        protected virtual void RALoan_OverdueInsurance_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region OverdueInterest
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Overdue Interest", Visible = false, Enabled = false)]
        protected virtual void RALoan_OverdueInterest_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region LoanAmtGel
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "Loan Amount in GEL", Visible = false, Enabled = false)]
        protected virtual void RALoan_LoanAmtGel_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #region IFRSLoanProvisionRate
        [PXMergeAttributes(Method = MergeMethod.Merge)]
        [PXUIField(DisplayName = "IFRSLoanProvisionRate", Visible = false, Enabled = false)]
        protected virtual void RALoan_IFRSLoanProvisionRate_CacheAttached(PXCache cache)
        {

        }
        #endregion
        #endregion

        #endregion

        #region NAsset

        public class NAsset : PX.Data.IBqlTable
        {
            #region Reason
            public abstract class reason : PX.Data.IBqlField
            {
            }
            protected string _Reason;
            [PXString(2, IsUnicode = true)]
            [PXUIField(DisplayName = "Reason")]
            [AssetReasonListAttribute]
            [PXDefault()]
            public virtual string Reason
            {
                get
                {
                    return this._Reason;
                }
                set
                {
                    this._Reason = value;
                }
            }
            #endregion
            #region ClassID
            public abstract class classID : PX.Data.IBqlField
            {
            }
            protected int? _ClassID;
            [PXInt()]
            [PXUIField(DisplayName = "RA Class")]
            [PXSelector(typeof(Search<RAClass.classID>),
                    typeof(RAClass.classCD),
                typeof(RAClass.externalID),
                typeof(RAClass.assetType),
                typeof(RAClass.description),
                DescriptionField = typeof(RAClass.description), SubstituteKey = typeof(RAClass.classCD))]
            public virtual int? ClassID
            {
                get
                {
                    return this._ClassID;
                }
                set
                {
                    this._ClassID = value;
                }
            }
            #endregion
            #region AssetType
            public abstract class assetType : PX.Data.IBqlField
            {
            }
            protected string _AssetType;
            [PXString(2, IsUnicode = true)]
            [PXUIField(DisplayName = "Asset Type")]
            [AssetTypeListAttribute]
            [PXDefault(typeof(Search<RAClass.assetType, Where<RAClass.classID, Equal<Current<NAsset.classID>>>>), PersistingCheck = PXPersistingCheck.Nothing)]
            public virtual string AssetType
            {
                get
                {
                    return this._AssetType;
                }
                set
                {
                    this._AssetType = value;
                }
            }
            #endregion
            #region PropertyID
            public abstract class propertyID : PX.Data.IBqlField
            {
            }
            protected string _PropertyID;
            [PXString(50, IsUnicode = true)]
            [PXUIField(DisplayName = "Property ID(LMS ID)")]
            public virtual string PropertyID
            {
                get
                {
                    return this._PropertyID;
                }
                set
                {
                    this._PropertyID = value;
                }
            }
            #endregion
            #region CadastralCode
            public abstract class cadastralCode : PX.Data.IBqlField
            {
            }
            protected string _CadastralCode;
            [PXString(50, IsUnicode = true)]
            [PXUIField(DisplayName = "Cadastral Code")]
            public virtual string CadastralCode
            {
                get
                {
                    return this._CadastralCode;
                }
                set
                {
                    this._CadastralCode = value;
                }
            }
            #endregion
            #region PlateNumber 
            public abstract class plateNumber : PX.Data.IBqlField { }
            protected string _PlateNumber;
            [PXString(25, IsUnicode = true)]
            [PXUIField(DisplayName = "Plate Number")]
            public virtual string PlateNumber
            {
                get { return this._PlateNumber; }
                set { this._PlateNumber = value; }
            }
            #endregion
            #region OwnerID 
            public abstract class ownerID : PX.Data.IBqlField { }
            protected string _OwnerID;
            [PXString(25, IsUnicode = true)]
            [PXUIField(DisplayName = "Owner ID")]
            public virtual string OwnerID
            {
                get { return this._OwnerID; }
                set { this._OwnerID = value; }
            }
            #endregion
        }

        #endregion
        
        public class MessageDialog : PX.Data.IBqlTable
        {

        }

  
        #endregion
    }
}
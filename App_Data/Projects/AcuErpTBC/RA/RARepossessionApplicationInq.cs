
using PX.Data;
using PX.Objects.EP;
using RA._Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA
{
    public class RARepossessionApplicationInq : RAGraph<RARepossessionApplicationInq>
    {
        [PXFilterable]
        public PXSelectReadonly<RARepossessionApplication> RepossessionApplications;

        public IEnumerable repossessionApplications()
        {
            PXSelectBase<RARepossessionApplication> query = new PXSelect<RARepossessionApplication>(this);
            IEnumerable<string> fiels = new List<string>
            {
                 "RepossessionApplicationID",
                 "RepossessionApplicationCD",
                 "RepossessionType",
                 "Status",
                 "LoanManager",
                 "ReportsTo",
                 "ReleaseDate",
                 "ApprovalDate",
                 "Description",
                 "LMRemopossessionAmt",
                 "TotalFairValue",
                 "TotalSalvageValue",
                 "Difference",
                 "TotalDeffectiveAmt",
                 "RAManagerRemopossessionAmt",
                 "TotalLoanAmt",
                 "TotalPayedPrincipalAmt",
                 "LTV",
                 "LTVWithoutPenalty",
                 "Insider",
                 "CapitalSharePct"
            };
            using (new PXFieldScope(RepossessionApplications.View, fiels))
            {
                PXView select = new PXView(this, true, RepossessionApplications.View.BqlSelect);
                Int32 totalrow = 0;
                Int32 startrow = PXView.StartRow;

                List<object> result = select.Select(
                            PXView.Currents, PXView.Parameters, PXView.Searches,
                            PXView.SortColumns, PXView.Descendings, PXView.Filters, ref startrow, PXView.MaximumRows, ref totalrow);

                PXView.StartRow = 0;
                return result;
            }
        }

        #region Action

        public PXInsert<RARepossessionApplication> Insert;
        [PXInsertButton]
        public virtual void insert()
        {
            RARepossessionApplicationEntry graph = PXGraph.CreateInstance<RARepossessionApplicationEntry>();
            throw new PXRedirectRequiredException(graph, true, "Repossession Application");
        }

        public PXCancel<RARepossessionApplication> Cancel;

        public PXAction<RARepossessionApplication> Edit;
        [PXEditDetailButton]
        [PXUIField(DisplayName = "")]
        public virtual void edit()
        {
            RARepossessionApplication row = RepossessionApplications.Current;
            RARepossessionApplicationEntry graph = PXGraph.CreateInstance<RARepossessionApplicationEntry>();
            graph.RepossessionApplication.Current = graph.RepossessionApplication.Search<RARepossessionApplication.repossessionApplicationID>(row.RepossessionApplicationID);
            if (graph.RepossessionApplication.Current != null)
            {
                throw new PXRedirectRequiredException(graph, true, "Repossession Application");
            }
        }

        #endregion
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA
{
    public class RASetupMaint : RAGraph<RASetupMaint>
    {
        public PXSelect<RASetup> Setup;
        public PXSave<RASetup> Save;
        public PXCancel<RASetup> Cancel;
    }
}
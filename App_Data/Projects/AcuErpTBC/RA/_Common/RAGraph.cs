using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA
{
    public class RAGraph<TGraph, TPrimary> : PXGraph<TGraph, TPrimary>
        where TGraph : PXGraph
        where TPrimary : class, IBqlTable, new()
    {
    }
    public class RAGraph<TGraph> : PXGraph<TGraph> where TGraph : PXGraph
    {

    }
}
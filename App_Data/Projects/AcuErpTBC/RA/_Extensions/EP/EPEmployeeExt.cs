﻿using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using PX.TM;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace PX.Objects.CR
{
    public class EPEmployeeExt : PXCacheExtension<PX.Objects.EP.EPEmployee>
    {
        #region UsrIsRaRecommendator
        [PXDBBool]
        [PXUIField(DisplayName = "Is Ra Recommendator")]

        public virtual bool? UsrIsRaRecommendator { get; set; }
        public abstract class usrIsRaRecommendator : IBqlField { }
        #endregion
    }
}
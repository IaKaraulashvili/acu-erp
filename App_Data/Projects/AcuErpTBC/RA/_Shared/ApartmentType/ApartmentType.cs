﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ApartmentType
{
    public static class ApartmentType
    {
        public const string ApartmentInNewBuilding = "AN";
        public class apartmentInNewBuilding : Constant<String>
        {
            public apartmentInNewBuilding()
                : base(ApartmentInNewBuilding)
            {
            }
        }

        public const string ApartmentInOldBuilding = "AO";
        public class apartmentInOldBuilding : Constant<String>
        {
            public apartmentInOldBuilding()
                : base(ApartmentInOldBuilding)
            {
            }
        }

        public const string ApartmentInOldTbilisi = "AT";
        public class apartmentInOldTbilisi : Constant<String>
        {
            public apartmentInOldTbilisi()
                : base(ApartmentInOldTbilisi)
            {
            }
        }

        public class UI
        {
            public const string ApartmentInNewBuilding = "Apartment in New Building";
            public const string ApartmentInOldBuilding = "Apartment in Old Building";
            public const string ApartmentInOldTbilisi = "Apartment in Old Tbilisi";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ApartmentType
{
    public class ApartmentTypeListAttribute : PXStringListAttribute
    {
        public ApartmentTypeListAttribute() : base(
        new string[]{
                ApartmentType.ApartmentInNewBuilding,
                ApartmentType.ApartmentInOldBuilding,
                ApartmentType.ApartmentInOldTbilisi
        },
        new string[]{
                ApartmentType.UI.ApartmentInNewBuilding,
                ApartmentType.UI.ApartmentInOldBuilding,
                ApartmentType.UI.ApartmentInOldTbilisi
        })
        { }
    }
}

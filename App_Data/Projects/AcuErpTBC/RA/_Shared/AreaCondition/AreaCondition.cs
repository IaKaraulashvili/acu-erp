﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.AreaCondition
{
    public static class AreaCondition
    {
        public const string NotRepaired = "NR";
        public class notRepaired : Constant<String>
        {
            public notRepaired()
                : base(NotRepaired)
            {
            }
        }

        public const string AverageRepairs = "AR";
        public class averageRepairs : Constant<String>
        {
            public averageRepairs()
                : base(AverageRepairs)
            {
            }
        }

        public const string GoodRepairs = "GR";
        public class goodRepairs : Constant<String>
        {
            public goodRepairs()
                : base(GoodRepairs)
            {
            }
        }

        public const string EuroRepairs = "ER";
        public class euroRepairs : Constant<String>
        {
            public euroRepairs()
                : base(EuroRepairs)
            {
            }
        }

        public class UI
        {
            public const string NotRepaired = "Not Repaired";
            public const string AverageRepairs = "Average Repairs";
            public const string GoodRepairs = "Good Repairs";
            public const string EuroRepairs = "Euro Repairs";
        }
    }
}

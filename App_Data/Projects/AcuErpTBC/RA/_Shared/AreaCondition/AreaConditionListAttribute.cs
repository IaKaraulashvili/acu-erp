﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.AreaCondition
{
    public class AreaConditionListAttribute : PXStringListAttribute
    {
        public AreaConditionListAttribute() : base(
        new string[]{
                AreaCondition.NotRepaired,
                AreaCondition.AverageRepairs,
                AreaCondition.GoodRepairs,
                AreaCondition.EuroRepairs,
        },
        new string[]{
                AreaCondition.UI.NotRepaired,
                AreaCondition.UI.AverageRepairs,
                AreaCondition.UI.GoodRepairs,
                AreaCondition.UI.EuroRepairs
        })
        { }
    }
}

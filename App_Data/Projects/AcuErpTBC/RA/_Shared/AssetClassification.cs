﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public static class AssetClassification
    {
        public const string ForSale = "FS";
        public class forSale : Constant<String>
        {
            public forSale()
                : base(ForSale)
            {
            }
        }

        public const string Option = "OP";
        public class option : Constant<String>
        {
            public option()
                : base(Option)
            {
            }
        }

        public const string ThirtyPercentProject = "TP";
        public class thirtyPercentProject : Constant<String>
        {
            public thirtyPercentProject()
                : base(ThirtyPercentProject)
            {
            }
        }

        public const string Instalment = "IN";
        public class instalment : Constant<String>
        {
            public instalment()
                : base(Instalment)
            {
            }
        }

        public const string Paused = "PA";
        public class paused : Constant<String>
        {
            public paused()
                : base(Paused)
            {
            }
        }

        public const string Deffective = "DE";
        public class deffective : Constant<String>
        {
            public deffective()
                : base(Deffective)
            {
            }
        }

        public const string Settled = "SE";
        public class settled : Constant<String>
        {
            public settled()
                : base(Settled)
            {
            }
        }


        public class UI
        {
            public const string ForSale = "For sale";
            public const string Option = "Option";
            public const string ThirtyPercentProject = "30% project";
            public const string Instalment = "Instalment";
            public const string Paused = "Paused";
            public const string Deffective = "Deffective";
            public const string Settled = "Settled";
        }
    }
}

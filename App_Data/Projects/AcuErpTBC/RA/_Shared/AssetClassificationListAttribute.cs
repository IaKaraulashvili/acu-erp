﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class AssetClassificationListAttribute : PXStringListAttribute
    {
        public AssetClassificationListAttribute() : base(
        new string[]{
                AssetClassification.ForSale,
                AssetClassification.Option,
                AssetClassification.ThirtyPercentProject,
                AssetClassification.Instalment,
                AssetClassification.Paused,
                AssetClassification.Deffective,
                AssetClassification.Settled,
        },
        new string[]{
                AssetClassification.UI.ForSale,
                AssetClassification.UI.Option,
                AssetClassification.UI.ThirtyPercentProject,
                AssetClassification.UI.Instalment,
                AssetClassification.UI.Paused,
                AssetClassification.UI.Deffective,
                AssetClassification.UI.Settled,
        })
        { }
    }
}

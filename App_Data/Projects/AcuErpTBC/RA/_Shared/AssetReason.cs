﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public static class AssetReason
    {
        public const string Repossession = "RE";
        public class repossession : Constant<String>
        {
            public repossession()
                : base(Repossession)
            {
            }
        }

        public const string Replace = "RP";
        public class replace : Constant<String>
        {
            public replace()
                : base(Replace)
            {
            }
        }

        public const string RepossessNewAsset = "RA";
        public class repossessNewAsset : Constant<String>
        {
            public repossessNewAsset()
                : base(RepossessNewAsset)
            {
            }
        }

        public class UI
        {
            public const string Repossession = "Repossession";
            public const string Replace = "Replace";
            public const string RepossessNewAsset = "Repossess New Asset";
        }
    }
}


﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class AssetReasonListAttribute : PXStringListAttribute
    {
        public AssetReasonListAttribute() : base(
        new string[]{
                AssetReason.Repossession,
                AssetReason.Replace,
                AssetReason.RepossessNewAsset
        },
        new string[]{
                AssetReason.UI.Repossession,
                AssetReason.UI.Replace,
                AssetReason.UI.RepossessNewAsset
        })
    { }
    }
}


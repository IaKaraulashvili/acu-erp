﻿using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;

namespace RA._Shared
{
    public class AssetStatusListAttribute : PXStringListAttribute
    {
        public AssetStatusListAttribute() : base(

        new string[]{

        AssetStatus.Draft,
        AssetStatus.ReadyForEvaluation,
        AssetStatus.WaitingForEvaluation,
        AssetStatus.Evaluated,
        AssetStatus.WaitingRAManager,
        AssetStatus.RespondedByRAManager,
        AssetStatus.PendingForApproval,
        AssetStatus.ReturnedForCorrection,
        AssetStatus.Approved,
        AssetStatus.Rejected,

        AssetStatus.WaitingAuctionResult,
        AssetStatus.SoldOnAuction,
        AssetStatus.Contracting,
        AssetStatus.RepossesionStarted,
        AssetStatus.WaitingAccountantApprove,
        AssetStatus.Repossessed
        },
        new string[] {
                        "Draft",
                        "Ready For Evaluation",
                        "Waiting For Evaluation",
                        "Evaluated",
                        "Waiting RA Manager",
                        "Responded By RA Manager",
                        "Pending For Approval",
                        "Returned For Correction",
                        "Approved",
                        "Rejected",

                         "Waiting Auction Result",
                         "Sold On Auction",
                         "Contracting",
                         "Repossesion Started",
                         "Waiting Accountant Approve",
                         "Repossessed"
                    })
        { }
    }

}

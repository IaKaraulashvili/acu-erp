﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;

namespace RA._Shared
{
    public class AssetTypeListAttribute : PXStringListAttribute
    {
        public AssetTypeListAttribute() : base(
            new string[]{
                AssetType.Equipment,
                AssetType.TransportVehicles,
                AssetType.OtherNonRealEstate,
                AssetType.Land,
                AssetType.Inhabitance,
                AssetType.Garage,
                AssetType.IndustrialArea,
                AssetType.CommercialOfficeArea,
                AssetType.Hotel,
                AssetType.Farm
            },
            new string[]{
               "Equipment",
                "Transport Vehicles",
                "Other Non Real Estate",
                "Land",
                "Inhabitance",
                "Garage",
                "Industrial Area",
                "Commercial/Office Area",
                "Hotel",
                "Farm"
            })
        { }
    }

    public static class AssetType
    {
        public const string Equipment = "EQ";
        public const string TransportVehicles = "TV";
        public const string OtherNonRealEstate = "OE";
        public const string Land = "LN";
        public const string Inhabitance = "IH";
        public const string Garage = "GR";
        public const string IndustrialArea = "IA";
        public const string CommercialOfficeArea = "CO";
        public const string Hotel = "HT";
        public const string Farm = "FM";
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.Coastline
{
    public class CoastlineListAttribute : PXIntListAttribute
    {
        public CoastlineListAttribute() : base(

        new int[]{
                Coastline.One,
                Coastline.Two,
                Coastline.Three,
                Coastline.Four,
                Coastline.Five,
                Coastline.Six,
                Coastline.Seven,
                Coastline.Eight,
                Coastline.Nine
        },
        new string[]{
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"})
        { }
    }
}

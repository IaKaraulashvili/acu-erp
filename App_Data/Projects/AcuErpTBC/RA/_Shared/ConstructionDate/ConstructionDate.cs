﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ConstructionDate
{
    public static class ConstructionDate
    {
        public const string N1955 = "R1";
        public class n1955 : Constant<String>
        {
            public n1955()
                : base(N1955)
            {
            }
        }

        public const string N1955_2000 = "R2";
        public class n1955_2000 : Constant<String>
        {
            public n1955_2000()
                : base(N1955_2000)
            {
            }
        }

        public const string N2000 = "R3";
        public class n2000 : Constant<String>
        {
            public n2000()
                : base(N2000)
            {
            }
        }

        public class UI
        {
            public const string N1955 = "<1955";
            public const string N1955_2000 = "1955-2000";
            public const string N2000 = ">2000";
        }
    }
}

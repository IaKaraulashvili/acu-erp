﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ConstructionDate
{
    public class ConstructionDateListAttribute : PXStringListAttribute
    {
        public ConstructionDateListAttribute() : base(
        new string[]{
                ConstructionDate.N1955,
                ConstructionDate.N1955_2000,
                ConstructionDate.N2000,
        },
        new string[]{
                ConstructionDate.UI.N1955,
                ConstructionDate.UI.N1955_2000,
                ConstructionDate.UI.N2000,
        })
        { }
    }
}

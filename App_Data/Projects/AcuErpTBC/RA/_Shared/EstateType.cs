﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public static class EstateType
    {
        public const string RealEstate = "RealEstate";
        public const string NonRealEstate = "NonRealEstate";
        public const string NotSpecified = "NotSpecified";
    }
}

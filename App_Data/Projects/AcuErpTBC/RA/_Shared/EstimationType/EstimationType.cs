﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.EstimationType._Shared
{
    public class EstimationType
    {
        public const string External = "E";
        public class external : Constant<String>
        {
            public external()
                : base(External)
            {
            }
        }

        public const string Internal = "I";
        public class _internal : Constant<String>
        {
            public _internal()
                : base(Internal)
            {
            }
        }

        public class UI
        {
            public const string External = "External";
            public const string Internal = "Internal";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.EstimationType._Shared
{
    public class EstimationTypeListAttribute : PXStringListAttribute
    {
        public EstimationTypeListAttribute() : base(
        new string[]{
                EstimationType.External,
                EstimationType.Internal
        },
        new string[]{
                EstimationType.UI.External,
                EstimationType.UI.Internal
        })
        { }
    }
}

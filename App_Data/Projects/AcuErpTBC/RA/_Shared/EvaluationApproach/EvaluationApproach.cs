﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationApproach
{
    public class EvaluationApproach
    {
        public const string Market = "M";
        public class market : Constant<String>
        {
            public market()
                : base(Market)
            {
            }
        }

        public const string Income = "I";
        public class income : Constant<String>
        {
            public income()
                : base(Income)
            {
            }
        }

        public const string Expenses = "E";
        public class expenses : Constant<String>
        {
            public expenses()
                : base(Expenses)
            {
            }
        }

        public class UI
        {
            public const string Market = "Market";
            public const string Income = "Income";
            public const string Expenses = "Expenses";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationApproach
{
    public class EvaluationApproachListAttribute : PXStringListAttribute
    {
        public EvaluationApproachListAttribute() : base(
        new string[]{
                EvaluationApproach.Market,
                EvaluationApproach.Income,
                EvaluationApproach.Expenses
        },
        new string[]{
                EvaluationApproach.UI.Market,
                EvaluationApproach.UI.Income,
                EvaluationApproach.UI.Expenses,
        })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationStatus
{
   public class EvaluationStatus
    {
        public const string WaitingForEvaluation = "WE";
        public class waitingForEvaluation : Constant<String>
        {
            public waitingForEvaluation()
                : base(WaitingForEvaluation)
            {
            }
        }

        public const string Evaluated = "ED";
        public class evaluated : Constant<String>
        {
            public evaluated()
                : base(Evaluated)
            {
            }
        }

        public class UI
        {
            public const string WaitingForEvaluation = "Waiting for evaluation";
            public const string Evaluated = "Evaluated";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationStatus
{
    public class EvaluationStatusListAttribute : PXStringListAttribute
    {
        public EvaluationStatusListAttribute() : base(
   new string[]{
                EvaluationStatus.WaitingForEvaluation,
                EvaluationStatus.Evaluated
   },
   new string[]{
                EvaluationStatus.UI.WaitingForEvaluation,
                EvaluationStatus.UI.Evaluated
   })
        { }
    }
}

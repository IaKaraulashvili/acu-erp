﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationType
{
    public class EvaluationType
    {
        public const string Primary = "P";
        public class primary : Constant<String>
        {
            public primary()
                : base(Primary)
            {
            }
        }

        public const string Secondary = "S";
        public class secondary : Constant<String>
        {
            public secondary()
                : base(Secondary)
            {
            }
        }

        public class UI
        {
            public const string Primary = "Primary";
            public const string Secondary = "Secondary";
        }
    }
}

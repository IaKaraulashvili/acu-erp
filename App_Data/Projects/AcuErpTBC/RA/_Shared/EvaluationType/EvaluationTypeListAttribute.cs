﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.EvaluationType
{
   public class EvaluationTypeListAttribute :PXStringListAttribute
    {
        public EvaluationTypeListAttribute() : base(
        new string[]{
                EvaluationType.Primary,
                EvaluationType.Secondary
        },
        new string[]{
                EvaluationType.UI.Primary,
                EvaluationType.UI.Secondary
        })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public static class ExecutionType
    {
        public const string Personal = "PR";
        public class personal : Constant<String>
        {
            public personal()
                : base(Personal)
            {
            }
        }

        public const string State = "ST";
        public class state : Constant<String>
        {
            public state()
                : base(State)
            {
            }
        }

        public const string Simplified = "SI";
        public class simplified : Constant<String>
        {
            public simplified()
                : base(Simplified)
            {
            }
        }

        public class UI
        {
            public const string Personal = "Personal";
            public const string State = "State";
            public const string Simplified = "Simplified";
        }
    }
}


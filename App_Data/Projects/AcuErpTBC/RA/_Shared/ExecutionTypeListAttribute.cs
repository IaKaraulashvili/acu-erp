﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
   public class ExecutionTypeListAttribute : PXStringListAttribute
    {
        public ExecutionTypeListAttribute() : base(
        new string[]{
                ExecutionType.Personal,
                ExecutionType.State,
                ExecutionType.Simplified
        },
        new string[]{
                ExecutionType.UI.Personal,
                ExecutionType.UI.State,
                ExecutionType.UI.Simplified
        })
    { }
    }
}

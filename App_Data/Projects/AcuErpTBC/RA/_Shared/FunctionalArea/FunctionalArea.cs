﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.FunctionalArea
{
    public static class FunctionalArea
    {
        public const string LandscapeRecreationalArea = "LR";
        public class landscapeRecreationalArea : Constant<String>
        {
            public landscapeRecreationalArea()
                : base(LandscapeRecreationalArea)
            {
            }
        }

        public const string PublicWorkingArea1 = "P1";
        public class publicWorkingArea1 : Constant<String>
        {
            public publicWorkingArea1()
                : base(PublicWorkingArea1)
            {
            }
        }

        public const string PublicWorkingArea2 = "P2";
        public class publicWorkingArea2 : Constant<String>
        {
            public publicWorkingArea2()
                : base(PublicWorkingArea2)
            {
            }
        }

        public const string PublicWorkingArea3 = "P3";
        public class publicWorkingArea3 : Constant<String>
        {
            public publicWorkingArea3()
                : base(PublicWorkingArea3)
            {
            }
        }

        public const string IndustrialArea1 = "I1";
        public class industrialArea1 : Constant<String>
        {
            public industrialArea1()
                : base(IndustrialArea1)
            {
            }
        }

        public const string IndustrialArea2 = "I2";
        public class industrialArea2 : Constant<String>
        {
            public industrialArea2()
                : base(IndustrialArea2)
            {
            }
        }

        public const string MilitaryDestinationArea1 = "M1";
        public class militaryDestinationArea1 : Constant<String>
        {
            public militaryDestinationArea1()
                : base(MilitaryDestinationArea1)
            {
            }
        }

        public const string MilitaryDestinationArea2 = "M2";
        public class militaryDestinationArea2 : Constant<String>
        {
            public militaryDestinationArea2()
                : base(MilitaryDestinationArea2)
            {
            }
        }

        public const string SanitaryArea = "SA";
        public class sanitaryArea : Constant<String>
        {
            public sanitaryArea()
                : base(SanitaryArea)
            {
            }
        }

        public const string RecreationalArea1 = "R1";
        public class recreationalArea1 : Constant<String>
        {
            public recreationalArea1()
                : base(RecreationalArea1)
            {
            }
        }

        public const string RecreationalArea2 = "R2";
        public class recreationalArea2 : Constant<String>
        {
            public recreationalArea2()
                : base(RecreationalArea2)
            {
            }
        }

        public const string RecreationalArea3 = "R3";
        public class recreationalArea3 : Constant<String>
        {
            public recreationalArea3()
                : base(RecreationalArea3)
            {
            }
        }

        public const string AgriculturalArea = "AA";
        public class agriculturalArea : Constant<String>
        {
            public agriculturalArea()
                : base(AgriculturalArea)
            {
            }
        }

        public const string TransportArea1 = "T1";
        public class transportArea1 : Constant<String>
        {
            public transportArea1()
                : base(TransportArea1)
            {
            }
        }

        public const string TransportArea2 = "T2";
        public class transportArea2 : Constant<String>
        {
            public transportArea2()
                : base(TransportArea1)
            {
            }
        }

        public const string TransportArea3 = "T3";
        public class transportArea3 : Constant<String>
        {
            public transportArea3()
                : base(TransportArea3)
            {
            }
        }

        public const string ForestArea = "FA";
        public class forestArea : Constant<String>
        {
            public forestArea()
                : base(ForestArea)
            {
            }
        }

        public const string Inhabitance1 = "H1";
        public class inhabitance1 : Constant<String>
        {
            public inhabitance1()
                : base(Inhabitance1)
            {
            }
        }

        public const string Inhabitance2 = "H2";
        public class inhabitance2 : Constant<String>
        {
            public inhabitance2()
                : base(Inhabitance2)
            {
            }
        }

        public const string Inhabitance3 = "H3";
        public class inhabitance3 : Constant<String>
        {
            public inhabitance3()
                : base(Inhabitance3)
            {
            }
        }

        public const string Inhabitance4 = "H4";
        public class inhabitance4 : Constant<String>
        {
            public inhabitance4()
                : base(Inhabitance4)
            {
            }
        }

        public const string Inhabitance5 = "H5";
        public class inhabitance5 : Constant<String>
        {
            public inhabitance5()
                : base(Inhabitance5)
            {
            }
        }

        public const string Inhabitance6 = "H6";
        public class inhabitance6 : Constant<String>
        {
            public inhabitance6()
                : base(Inhabitance6)
            {
            }
        }

        public const string EspecialArea1 = "E1";
        public class especialArea1 : Constant<String>
        {
            public especialArea1()
                : base(EspecialArea1)
            {
            }
        }

        public const string EspecialArea2 = "E2";
        public class especialArea2 : Constant<String>
        {
            public especialArea2()
                : base(EspecialArea1)
            {
            }
        }

        public class UI
        {
            public const string LandscapeRecreationalArea = "Landscape/Recreational Area";
            public const string PublicWorkingArea1 = "Public-Working Area 1";
            public const string PublicWorkingArea2 = "Public-Working Area 2";
            public const string PublicWorkingArea3 = "Public-Working Area 3";
            public const string IndustrialArea1 = "Industrial Area 1";
            public const string IndustrialArea2 = "Industrial Area 2";
            public const string MilitaryDestinationArea1 = "Military Destination Area 1";
            public const string MilitaryDestinationArea2 = "Military Destination Area 2";
            public const string SanitaryArea = "Sanitary Area";
            public const string RecreationalArea1 = "Recreational Area 1";
            public const string RecreationalArea2 = "Recreational Area 2";
            public const string RecreationalArea3 = "Recreational Area 3";
            public const string AgriculturalArea = "Agricultural Area";
            public const string TransportArea1 = "Transport Area 1";
            public const string TransportArea2 = "Transport Area 2";
            public const string TransportArea3 = "Transport Area 3";
            public const string ForestArea = "Forest Area";
            public const string Inhabitance1 = "Inhabitance 1";
            public const string Inhabitance2 = "Inhabitance 2";
            public const string Inhabitance3 = "Inhabitance 3";
            public const string Inhabitance4 = "Inhabitance 4";
            public const string Inhabitance5 = "Inhabitance 5";
            public const string Inhabitance6 = "Inhabitance 6";
            public const string EspecialArea1 = "Especial Area 1";
            public const string EspecialArea2 = "Especial Area 2";
        }
    }
}

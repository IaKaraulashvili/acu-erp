﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.FunctionalArea
{
    public class FunctionalAreaListAttribute : PXStringListAttribute
    {
        public FunctionalAreaListAttribute() : base(
        new string[]{
                FunctionalArea.LandscapeRecreationalArea,
                FunctionalArea.PublicWorkingArea1,
                FunctionalArea.PublicWorkingArea2,
                FunctionalArea.PublicWorkingArea3,
                FunctionalArea.IndustrialArea1,
                FunctionalArea.IndustrialArea2,
                FunctionalArea.MilitaryDestinationArea1,
                FunctionalArea.MilitaryDestinationArea2,
                FunctionalArea.SanitaryArea,
                FunctionalArea.RecreationalArea1,
                FunctionalArea.RecreationalArea2,
                FunctionalArea.RecreationalArea3,
                FunctionalArea.AgriculturalArea,
                FunctionalArea.TransportArea1,
                FunctionalArea.TransportArea2,
                FunctionalArea.TransportArea3,
                FunctionalArea.ForestArea,
                FunctionalArea.Inhabitance1,
                FunctionalArea.Inhabitance2,
                FunctionalArea.Inhabitance3,
                FunctionalArea.Inhabitance4,
                FunctionalArea.Inhabitance5,
                FunctionalArea.Inhabitance6,
                FunctionalArea.EspecialArea1,
                FunctionalArea.EspecialArea2
        },
        new string[]{
                FunctionalArea.UI.LandscapeRecreationalArea,
                FunctionalArea.UI.PublicWorkingArea1,
                FunctionalArea.UI.PublicWorkingArea2,
                FunctionalArea.UI.PublicWorkingArea3,
                FunctionalArea.UI.IndustrialArea1,
                FunctionalArea.UI.IndustrialArea2,
                FunctionalArea.UI.MilitaryDestinationArea1,
                FunctionalArea.UI.MilitaryDestinationArea2,
                FunctionalArea.UI.SanitaryArea,
                FunctionalArea.UI.RecreationalArea1,
                FunctionalArea.UI.RecreationalArea2,
                FunctionalArea.UI.RecreationalArea3,
                FunctionalArea.UI.AgriculturalArea,
                FunctionalArea.UI.TransportArea1,
                FunctionalArea.UI.TransportArea2,
                FunctionalArea.UI.TransportArea3,
                FunctionalArea.UI.ForestArea,
                FunctionalArea.UI.Inhabitance1,
                FunctionalArea.UI.Inhabitance2,
                FunctionalArea.UI.Inhabitance3,
                FunctionalArea.UI.Inhabitance4,
                FunctionalArea.UI.Inhabitance5,
                FunctionalArea.UI.Inhabitance6,
                FunctionalArea.UI.EspecialArea1,
                FunctionalArea.UI.EspecialArea2
        })
        { }
    }
}

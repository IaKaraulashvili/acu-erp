﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.GeometryOfLandArea
{
    public static class GeometryOfLandArea
    {
        public const string Traingle = "TR";
        public class traingle : Constant<String>
        {
            public traingle()
                : base(Traingle)
            {
            }
        }

        public const string Square = "SQ";
        public class square : Constant<String>
        {
            public square()
                : base(Square)
            {
            }
        }

        public const string Polygon = "PL";
        public class polygon : Constant<String>
        {
            public polygon()
                : base(Polygon)
            {
            }
        }

        public const string Other = "OT";
        public class other : Constant<String>
        {
            public other()
                : base(Other)
            {
            }
        }

        public class UI
        {
            public const string Traingle = "Traingle";
            public const string Square = "Square";
            public const string Polygon = "Polygon";
            public const string Other = "Other";
        }
    }
}

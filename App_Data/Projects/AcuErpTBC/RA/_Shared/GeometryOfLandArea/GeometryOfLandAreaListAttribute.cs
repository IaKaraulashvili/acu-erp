﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.GeometryOfLandArea
{
    public class GeometryOfLandAreaListAttribute : PXStringListAttribute
    {
        public GeometryOfLandAreaListAttribute() : base(
        new string[]{
                GeometryOfLandArea.Traingle,
                GeometryOfLandArea.Square,
                GeometryOfLandArea.Polygon,
                GeometryOfLandArea.Other
        },
        new string[]{
                GeometryOfLandArea.UI.Traingle,
                GeometryOfLandArea.UI.Square,
                GeometryOfLandArea.UI.Polygon,
                GeometryOfLandArea.UI.Other
        })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.InformationSource
{
    public static class InformationSource
    {
        public const string InternalMeasuredDrawn = "IM";
        public class internalMeasuredDrawn : Constant<String>
        {
            public internalMeasuredDrawn()
                : base(InternalMeasuredDrawn)
            {
            }
        }

        public const string Extraction = "EX";
        public class extraction : Constant<String>
        {
            public extraction()
                : base(Extraction)
            {
            }
        }

        public const string CadastralMeasuredDrawn = "CM";
        public class cadastralMeasuredDrawn : Constant<String>
        {
            public cadastralMeasuredDrawn()
                : base(CadastralMeasuredDrawn)
            {
            }
        }

        public const string Unreported = "UR";
        public class unreported : Constant<String>
        {
            public unreported()
                : base(Unreported)
            {
            }
        }

        public class UI
        {
            public const string InternalMeasuredDrawn = "Internal Measured Drawn";
            public const string Extraction = "Extraction";
            public const string CadastralMeasuredDrawn = "Cadastral Measured Drawn";
            public const string Unreported = "Unreported";
        }
    }
}

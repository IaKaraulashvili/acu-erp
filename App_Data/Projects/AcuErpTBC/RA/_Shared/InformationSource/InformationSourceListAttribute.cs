﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.InformationSource
{
    public class InformationSourceListAttribute : PXStringListAttribute
    {
        public InformationSourceListAttribute() : base(
        new string[]{
                InformationSource.InternalMeasuredDrawn,
                InformationSource.Extraction,
                InformationSource.CadastralMeasuredDrawn,
                InformationSource.Unreported
        },
        new string[]{
                InformationSource.UI.InternalMeasuredDrawn,
                InformationSource.UI.Extraction,
                InformationSource.UI.CadastralMeasuredDrawn,
                InformationSource.UI.Unreported
        })
        { }
    }
}

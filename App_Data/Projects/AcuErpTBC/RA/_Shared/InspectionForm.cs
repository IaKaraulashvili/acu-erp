﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
   public static class InspectionForm
    {
        public const string Visit = "VI";
        public class visit : Constant<String>
        {
            public visit()
                : base(Visit)
            {
            }
        }

        public const string VerbalInformation = "VE";
        public class verbalInformation : Constant<String>
        {
            public verbalInformation()
                : base(VerbalInformation)
            {
            }
        }

     

        public class UI
        {
            public const string Visit = "Visit";
            public const string VerbalInformation = "Verbal Information";
        }
    }
}

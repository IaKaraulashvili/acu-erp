﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
  public  class InspectionFormListAttribute : PXStringListAttribute
    {
        public InspectionFormListAttribute() : base(
        new string[]{
                InspectionForm.Visit,
                InspectionForm.VerbalInformation,
        },
        new string[]{
                InspectionForm.UI.Visit,
                InspectionForm.UI.VerbalInformation,
        })
    { }
    }
}

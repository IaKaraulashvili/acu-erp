﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    class LinkedAssetAttribute : PXSelectorAttribute
    {
        public LinkedAssetAttribute()
            :base(typeof(Search5<RAAsset.assetID, 
                InnerJoin<RARepossessionApplicationAsset, 
                    On<RAAsset.assetID, Equal<RARepossessionApplicationAsset.assetID>>>,
                Where2<Where2<Where<Current<RAAsset.reason>,
                    Equal<AssetReason.replace>, And<RAAsset.reason,
                        Equal<AssetReason.repossessNewAsset>>>,
                    Or<Where<Current<RAAsset.reason>,
                        Equal<AssetReason.repossessNewAsset>,
                        And<RAAsset.reason, Equal<AssetReason.replace>>>>>,
                    And<Where<RAAsset.assetID, NotEqual<Current<RAAsset.assetID>>, 
                        And<Current<RAAsset.reason>, 
                            NotEqual<AssetReason.repossession>>>>>, 
                Aggregate<GroupBy<RARepossessionApplicationAsset.assetID>>, OrderBy<Asc<RAAsset.assetID>>>),
            typeof(RAAsset.assetCD),
            typeof(RAAsset.reason),
            typeof(RAAsset.description))
        {
            SubstituteKey = typeof(RAAsset.assetCD);
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.LoanStatus
{
    public static class LoanStatus
    {
        public const string Active = "AC";
        public class active : Constant<string>
        {
            public active()
                :base(Active)
            {
            }
        }

        public const string Inactive = "IN";
        public class inactive : Constant<string>
        {
            public inactive()
                :base(Inactive)
            {
            }
        }

        public class UI
        {
            public const string Active = "Active";
            public const string Inactive = "Inactive";
        }
    }
}

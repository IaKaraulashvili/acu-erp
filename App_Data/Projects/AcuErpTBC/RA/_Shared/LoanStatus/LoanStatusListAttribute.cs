﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.LoanStatus
{
    class LoanStatusListAttribute : PXStringListAttribute
    {
        public LoanStatusListAttribute() : base(
            new string[]
            {
                LoanStatus.Active,
                LoanStatus.Inactive
            },
            new string[]
            {
                LoanStatus.UI.Active,
                LoanStatus.UI.Inactive
            })
        { }
    }
}

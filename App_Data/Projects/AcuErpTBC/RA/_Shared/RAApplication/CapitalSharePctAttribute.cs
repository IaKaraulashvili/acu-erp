﻿using PX.Data;
using RA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.Shared
{
    public class CapitalSharePctAttribute : PXEventSubscriberAttribute, IPXFieldSelectingSubscriber
    {
        public virtual void FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            if (e.Row == null) return;
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            
            var capitalAmount = ((RACapitalAmount)PXSelectGroupBy<RACapitalAmount, Where<RACapitalAmount.effectiveDate, LessEqual<Current<AccessInfo.businessDate>>>, Aggregate<Max<RACapitalAmount.effectiveDate>>>.Select(sender.Graph))?.Capital;
            e.ReturnValue = (row.LMRemopossessionAmt / capitalAmount ?? 0) * 100;
        }
    }
}

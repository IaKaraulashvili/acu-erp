﻿using PX.Data;
using RA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.Shared
{
    public class InsiderAttribute : PXEventSubscriberAttribute, IPXFieldSelectingSubscriber
    {
        public virtual void FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            if (e.Row == null) return;
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            bool insider = false;
            var repossessionApplicationAsset = PXSelectReadonly2<RARepossessionApplicationAsset, InnerJoin<RAAsset, On<RARepossessionApplicationAsset.assetID, Equal<RAAsset.assetID>>,
                                                       LeftJoin<RAPropertyOwner, On<RAAsset.assetID, Equal<RAPropertyOwner.assetID>>>>, Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Required<RARepossessionApplication.repossessionApplicationID>>>,
                                                       OrderBy<Desc<RARepossessionApplicationAsset.repossessionApplicationAssetID>>>.Select(sender.Graph, row.RepossessionApplicationID);

            var loan = PXSelectJoin<RALoan, InnerJoin<RAAssetLoan, On<RALoan.loanID, Equal<RAAssetLoan.loanID>>, InnerJoin<RARepossessionApplicationAsset, On<RAAssetLoan.assetID, Equal<RARepossessionApplicationAsset.assetID>>>>, Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>, OrderBy<Asc<RALoan.loanID>>>.Select(sender.Graph, row.RepossessionApplicationID);

            foreach (PXResult<RARepossessionApplicationAsset, RAAsset> item in repossessionApplicationAsset)
            {
                var asset = ((RAAsset)item[1]);
                var _PropertyOwnerInsider = PXSelectReadonly<RAPropertyOwner, Where<RAPropertyOwner.insider, Equal<True>, And<RAPropertyOwner.assetID, Equal<Required<RAAsset.assetID>>>>>.Select(sender.Graph, asset?.AssetID).Count > 0;
                insider = _PropertyOwnerInsider;
            }
            foreach (RALoan item in loan)
            {
                if (insider == false)
                {
                    insider = item.BorrowerInsider == true || item.CoBorrowerInsider == true || item.GuarantorInsider1 == true || item.GuarantorInsider2 == true || item.GuarantorInsider3 == true;
                }
            }

            e.ReturnValue = insider;
        }
    }
}

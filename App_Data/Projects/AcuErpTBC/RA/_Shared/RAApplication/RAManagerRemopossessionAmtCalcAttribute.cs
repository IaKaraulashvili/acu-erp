﻿using PX.Data;
using RA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.Shared
{
    public class RAManagerRemopossessionAmtCalcAttribute : PXEventSubscriberAttribute, IPXFieldSelectingSubscriber
    {
        public virtual void FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            if (e.Row == null) return;
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            decimal _RAManagerRepossessionAmount = Decimal.Zero;
            var repossessionApplicationAsset = PXSelectReadonly2<RARepossessionApplicationAsset, InnerJoin<RAAsset, On<RARepossessionApplicationAsset.assetID, Equal<RAAsset.assetID>>,
                                                       LeftJoin<RAPropertyOwner, On<RAAsset.assetID, Equal<RAPropertyOwner.assetID>>>>, Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Required<RARepossessionApplication.repossessionApplicationID>>>,
                                                       OrderBy<Desc<RARepossessionApplicationAsset.repossessionApplicationAssetID>>>.Select(sender.Graph, row.RepossessionApplicationID);

            foreach (PXResult<RARepossessionApplicationAsset, RAAsset> item in repossessionApplicationAsset)
            {
                var asset = ((RAAsset)item[1]);
                _RAManagerRepossessionAmount += asset.RecommendedRepossessionAmtGel != null ? asset.RecommendedRepossessionAmtGel.Value : decimal.Zero;
            }
            e.ReturnValue = _RAManagerRepossessionAmount;
        }
    }
}

﻿using PX.Data;
using RA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA.Shared
{
    public class TotalLoanAmtAttribute : PXEventSubscriberAttribute, IPXFieldSelectingSubscriber
    {
        public virtual void FieldSelecting(PXCache sender, PXFieldSelectingEventArgs e)
        {
            if (e.Row == null) return;
            RARepossessionApplication row = (RARepossessionApplication)e.Row;
            decimal _TotalLoanAmount = Decimal.Zero;
            var repossessionApplicationAsset = PXSelectReadonly2<RARepossessionApplicationAsset, InnerJoin<RAAsset, On<RARepossessionApplicationAsset.assetID, Equal<RAAsset.assetID>>,
                                                                LeftJoin<RAPropertyOwner, On<RAAsset.assetID, Equal<RAPropertyOwner.assetID>>>>, Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Required<RARepossessionApplication.repossessionApplicationID>>>,
                                                                OrderBy<Desc<RARepossessionApplicationAsset.repossessionApplicationAssetID>>>.Select(sender.Graph, row.RepossessionApplicationID);

            var loan = PXSelectJoin<RALoan, InnerJoin<RAAssetLoan, On<RALoan.loanID, Equal<RAAssetLoan.loanID>>, InnerJoin<RARepossessionApplicationAsset, On<RAAssetLoan.assetID, Equal<RARepossessionApplicationAsset.assetID>>>>, Where<RARepossessionApplicationAsset.repossessionApplicationID, Equal<Current<RARepossessionApplication.repossessionApplicationID>>>, OrderBy<Asc<RALoan.loanID>>>.Select(sender.Graph, row.RepossessionApplicationID);

            foreach (RALoan item in loan)
            {
                _TotalLoanAmount += item.LoanAmtGel != null ? item.LoanAmtGel.Value : Decimal.Zero;
            }
            e.ReturnValue = _TotalLoanAmount;
        }
    }
}

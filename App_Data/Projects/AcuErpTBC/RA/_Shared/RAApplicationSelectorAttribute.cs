using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA._Shared
{
    public class RAApplicationSelectorAttribute : PXSelectorAttribute
    {
        public RAApplicationSelectorAttribute()
            : base(typeof(Search<RARepossessionApplication.repossessionApplicationCD>),
              typeof(RARepossessionApplication.repossessionApplicationCD),
              typeof(RARepossessionApplication.repossessionType),
              typeof(RARepossessionApplication.status)
              )
        { }
    }
}
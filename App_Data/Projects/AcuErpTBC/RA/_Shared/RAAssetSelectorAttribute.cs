using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA._Shared
{
    public class RAAssetSelectorAttribute : PXSelectorAttribute
    {
        public RAAssetSelectorAttribute()
            : base(typeof(Search<RAAsset.assetCD>),
              typeof(RAAsset.assetCD),
              typeof(RAAsset.assetType),
              typeof(RAAsset.description)
              )
        { }
    }
}
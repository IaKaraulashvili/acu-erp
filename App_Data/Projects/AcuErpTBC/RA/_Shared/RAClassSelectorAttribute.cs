﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class RAClassSelectorAttribute : PXSelectorAttribute
    {
        public RAClassSelectorAttribute()
            : base(typeof(Search<RAClass.classCD>),
            typeof(RAClass.classCD),
            typeof(RAClass.assetType))
        {}
    }
}

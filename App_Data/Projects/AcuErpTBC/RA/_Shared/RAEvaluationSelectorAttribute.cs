using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA._Shared
{
    public class RAEvaluationSelectorAttribute : PXSelectorAttribute
    {
        public RAEvaluationSelectorAttribute()
            : base(typeof(Search<RAEvaluation.evaluationCD>),
              typeof(RAEvaluation.evaluationCD),
              typeof(RAEvaluation.externalID),
              typeof(RAEvaluation.assetID)
              )
        { }
    }
}
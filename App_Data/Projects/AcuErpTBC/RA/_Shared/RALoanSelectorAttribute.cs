﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class RALoanSelectorAttribute : PXSelectorAttribute
    {
        public RALoanSelectorAttribute()
            : base(typeof(Search<RALoan.loanCD>),
            typeof(RALoan.loanCD),
            typeof(RALoan.lMSID))
        {}
    }
}

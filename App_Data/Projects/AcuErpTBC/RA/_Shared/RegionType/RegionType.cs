﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RegionType
{
    public class RegionType
    {
        public const string Tbilisi = "TB";
        public const string Adjara = "AD";
        public const string Guria = "GR";
        public const string Imereti = "IM";
        public const string SamegreloZemoSvaneti = "SS";
        public const string Kakheti = "KH";
        public const string SamtskheJavakheti = "SJ";
        public const string ShidaKartli = "SK";
        public const string MtskhetaMtianeti = "MM";
        public const string KvemoKartli = "KK";
        public const string RachaLechkhumi = "RL";
    }
}

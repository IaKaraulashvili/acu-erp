﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RegionType
{
    public class RegionTypeListAttribute : PXStringListAttribute
    {
        public RegionTypeListAttribute() : base(

        new string[]{
                RegionType.Tbilisi,
                RegionType.Adjara,
                RegionType.Guria,
                RegionType.Imereti,
                RegionType.SamegreloZemoSvaneti,
                RegionType.Kakheti,
                RegionType.SamtskheJavakheti,
                RegionType.ShidaKartli,
                RegionType.MtskhetaMtianeti,
                RegionType.KvemoKartli,
                RegionType.RachaLechkhumi
            },
            new string[]{
                "Tbilisi",
                "Adjara",
                "Guria",
                "Imereti",
                "Samegrelo-Zemo Svaneti",
                "Kakheti",
                "Samtskhe-Javakheti",
                "Shida Kartli",
                "Mtskheta-Mtianeti",
                "Kvemo Kartli",
                "Racha-Lechkhumi"
            })
        { }
    }
}

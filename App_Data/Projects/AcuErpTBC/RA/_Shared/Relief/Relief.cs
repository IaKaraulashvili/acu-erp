﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.Relief
{
    public static class Relief
    {
        public const string Plane = "PL";
        public class plane : Constant<String>
        {
            public plane()
                : base(Plane)
            {
            }
        }

        public const string Wavy = "WV";
        public class wavy : Constant<String>
        {
            public wavy()
                : base(Wavy)
            {
            }
        }

        public const string Hilly = "HL";
        public class hilly : Constant<String>
        {
            public hilly()
                : base(Hilly)
            {
            }
        }

        public const string Crooked = "CR";
        public class crooked : Constant<String>
        {
            public crooked()
                : base(Crooked)
            {
            }
        }

        public class UI
        {
            public const string Plane = "Plane";
            public const string Wavy = "Wavy";
            public const string Hilly = "Hilly";
            public const string Crooked = "Crooked";
        }
    }
}

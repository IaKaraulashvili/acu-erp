﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.Relief
{
    public class ReliefListAttribute : PXStringListAttribute
    {
        public ReliefListAttribute() : base(
        new string[]{
                Relief.Plane,
                Relief.Wavy,
                Relief.Hilly,
                Relief.Crooked
        },
        new string[]{
                Relief.UI.Plane,
                Relief.UI.Wavy,
                Relief.UI.Hilly,
                Relief.UI.Crooked
        })
        { }
    }
}

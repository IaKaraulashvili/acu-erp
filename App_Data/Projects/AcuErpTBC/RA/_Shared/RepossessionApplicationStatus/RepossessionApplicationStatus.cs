﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RepossessionApplicationStatus
{
    public static class RepossessionApplicationStatus
    {
        public const string Draft = "DR";
        public class draft : Constant<String>
        {
            public draft()
                : base(Draft)
            {
            }
        }

        public const string ReadyForEvaluation = "RE";
        public class readyForEvaluation : Constant<String>
        {
            public readyForEvaluation()
                : base(ReadyForEvaluation)
            {
            }
        }

        public const string WaitingForEvaluation = "WE";
        public class waitingForEvaluation : Constant<String>
        {
            public waitingForEvaluation()
                : base(WaitingForEvaluation)
            {
            }
        }

        public const string Evaluated = "EV";
        public class evaluated : Constant<String>
        {
            public evaluated()
                : base(Evaluated)
            {
            }
        }

        public const string WaitingRAManager = "WM";
        public class waitingRAManager : Constant<String>
        {
            public waitingRAManager()
                : base(WaitingRAManager)
            {
            }
        }

        public const string RespondedByRAManager = "RM";
        public class respondedByRAManager : Constant<String>
        {
            public respondedByRAManager()
                : base(RespondedByRAManager)
            {
            }
        }

        public const string PendingForApproval = "PA";
        public class pendingForApproval : Constant<String>
        {
            public pendingForApproval()
                : base(PendingForApproval)
            {
            }
        }

        public const string ReturnedForCorrection = "RC";
        public class returnedForCorrection : Constant<String>
        {
            public returnedForCorrection()
                : base(ReturnedForCorrection)
            {
            }
        }

        public const string Approved = "AP";
        public class approved : Constant<String>
        {
            public approved()
                : base(Approved)
            {
            }
        }


        public const string Rejected = "RJ";
        public class rejected : Constant<String>
        {
            public rejected()
                : base(Rejected)
            {
            }
        }

        public class UI
        {
            public const string Draft = "Draft";
            public const string ReadyForEvaluation = "Ready For Evaluation";
            public const string WaitingForEvaluation = "Waiting For Evaluation";
            public const string Evaluated = "Evaluated";
            public const string WaitingRAManager = "Waiting RA Manager";
            public const string RespondedByRAManager = "Responded By RA Manager";
            public const string PendingForApproval = "Pending For Approval";
            public const string ReturnedForCorrection = "Returned For Correction";
            public const string Approved = "Approved";
            public const string Rejected = "Rejected";
        }
    }
}

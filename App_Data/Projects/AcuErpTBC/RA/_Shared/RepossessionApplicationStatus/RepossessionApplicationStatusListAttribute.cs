﻿
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RepossessionApplicationStatus
{
    public class RepossessionApplicationStatusListAttribute : PXStringListAttribute
    {
        public RepossessionApplicationStatusListAttribute() : base(
            new string[]{
                RepossessionApplicationStatus.Draft,
                RepossessionApplicationStatus.ReadyForEvaluation,
                RepossessionApplicationStatus.WaitingForEvaluation,
                RepossessionApplicationStatus.Evaluated,
                RepossessionApplicationStatus.WaitingRAManager,
                RepossessionApplicationStatus.RespondedByRAManager,
                RepossessionApplicationStatus.PendingForApproval,
                RepossessionApplicationStatus.ReturnedForCorrection,
                RepossessionApplicationStatus.Approved,
                RepossessionApplicationStatus.Rejected,
            },
            new string[]{
                RepossessionApplicationStatus.UI.Draft,
                RepossessionApplicationStatus.UI.ReadyForEvaluation,
                RepossessionApplicationStatus.UI.WaitingForEvaluation,
                RepossessionApplicationStatus.UI.Evaluated,
                RepossessionApplicationStatus.UI.WaitingRAManager,
                RepossessionApplicationStatus.UI.RespondedByRAManager,
                RepossessionApplicationStatus.UI.PendingForApproval,
                RepossessionApplicationStatus.UI.ReturnedForCorrection,
                RepossessionApplicationStatus.UI.Approved,
                RepossessionApplicationStatus.UI.Rejected
            })
        { }
    }
}

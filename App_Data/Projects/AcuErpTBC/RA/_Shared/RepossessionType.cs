﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public static class RepossessionType
    {
        public const string Auction = "AU";
        public class auction : Constant<String>
        {
            public auction()
                : base(Auction)
            {
            }
        }
        public const string Agreement = "AG";
        public class agreement : Constant<String>
        {
            public agreement()
                : base(Agreement)
            {
            }
        }

        public class UI
        {
            public const string Auction = "Auction";
            public const string Agreement = "Agreement";
        }
    }
}

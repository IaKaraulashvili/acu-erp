﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class RepossessionTypeListSttribute : PXStringListAttribute
    {
        public RepossessionTypeListSttribute() : base(
        new string[]{
                RepossessionType.Auction,
                RepossessionType.Agreement,
        },
        new string[]{
                RepossessionType.UI.Auction,
                RepossessionType.UI.Agreement,
        })
        { }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA._Shared.RestrictionType
{
    public static class RestrictionType
    {

        public const string Sequestration = "SE";
        public class sequestration : Constant<String>
        {
            public sequestration()
                : base(Sequestration)
            {
            }
        }

        public const string LowMortgage = "LM";
        public class lowMortgage : Constant<String>
        {
            public lowMortgage()
                : base(LowMortgage)
            {
            }
        }

        public const string Obliged = "OB";
        public class obliged : Constant<String>
        {
            public obliged()
                : base(Obliged)
            {
            }
        }

        public const string Forbid = "FO";
        public class forbid : Constant<String>
        {
            public forbid()
                : base(Forbid)
            {
            }
        }

        public const string PartnersShareEquitableLien = "PS";
        public class partnersShareEquitableLien : Constant<String>
        {
            public partnersShareEquitableLien()
                : base(PartnersShareEquitableLien)
            {
            }
        }

        public const string MortgageLeasing = "ML";
        public class mortgageLeasing : Constant<String>
        {
            public mortgageLeasing()
                : base(MortgageLeasing)
            {
            }
        }

        public class UI
        {
            public const string Sequestration = "Sequestration";
            public const string LowMortgage = "Low Mortgage";
            public const string Obliged = "Obliged";
            public const string Forbid = "Forbid";
            public const string PartnersShareEquitableLien = "Partners Share Equitable Lien";
            public const string MortgageLeasing = "Mortgage Leasing";
        }
        }
}
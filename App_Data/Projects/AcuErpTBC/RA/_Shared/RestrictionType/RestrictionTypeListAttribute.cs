using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace RA._Shared.RestrictionType
{
    public class RestrictionTypeListAttribute : PXStringListAttribute
    {
        public RestrictionTypeListAttribute() : base(

        new string[]{
                RestrictionType.Sequestration,
                RestrictionType.LowMortgage,
                RestrictionType.Obliged,
                RestrictionType.Forbid,
                RestrictionType.PartnersShareEquitableLien,
                RestrictionType.MortgageLeasing
            },
            new string[]{
                RestrictionType.UI.Sequestration,
                RestrictionType.UI.LowMortgage,
                RestrictionType.UI.Obliged,
                RestrictionType.UI.Forbid,
                RestrictionType.UI.PartnersShareEquitableLien,
                RestrictionType.UI.MortgageLeasing
            })
        { }
    }
}
﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RoadType
{
    public static class RoadType
    {
        public const string Asphalt = "AS";
        public class asphalt : Constant<String>
        {
            public asphalt()
                : base(Asphalt)
            {
            }
        }

        public const string Ground = "GR";
        public class ground : Constant<String>
        {
            public ground()
                : base(Ground)
            {
            }
        }

        public const string Concrete = "CN";
        public class concrete : Constant<String>
        {
            public concrete()
                : base(Concrete)
            {
            }
        }

        public const string CrushedStoneGravel = "CR";
        public class crushedStoneGravel : Constant<String>
        {
            public crushedStoneGravel()
                : base(CrushedStoneGravel)
            {
            }
        }

        public const string Paved = "PV";
        public class paved : Constant<String>
        {
            public paved()
                : base(Paved)
            {
            }
        }

        public class UI
        {
            public const string Asphalt = "Asphalt";
            public const string Ground = "Ground";
            public const string Concrete = "Concrete";
            public const string CrushedStoneGravel = "Crushed Stone Gravel";
            public const string Paved = "Paved";
        }
    }
}

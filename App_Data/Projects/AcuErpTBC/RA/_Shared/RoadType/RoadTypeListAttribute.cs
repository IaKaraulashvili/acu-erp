﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.RoadType
{
    public class RoadTypeListAttribute : PXStringListAttribute
    {
        public RoadTypeListAttribute() : base(
        new string[]{
                RoadType.Asphalt,
                RoadType.Ground,
                RoadType.Concrete,
                RoadType.CrushedStoneGravel,
                RoadType.Paved
        },
        new string[]{
                RoadType.UI.Asphalt,
                RoadType.UI.Ground,
                RoadType.UI.Concrete,
                RoadType.UI.CrushedStoneGravel,
                RoadType.UI.Paved
        })
        { }
    }
}

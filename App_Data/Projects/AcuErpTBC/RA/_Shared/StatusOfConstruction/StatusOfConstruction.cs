﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.StatusOfConstruction
{
    public static class StatusOfConstruction
    {
        public const string BlackFrame = "BF";
        public class blackFrame : Constant<String>
        {
            public blackFrame()
                : base(BlackFrame)
            {
            }
        }

        public const string WhiteFrame = "WF";
        public class whiteFrame : Constant<String>
        {
            public whiteFrame()
                : base(WhiteFrame)
            {
            }
        }

        public const string GreenFrame = "GF";
        public class greenFrame : Constant<String>
        {
            public greenFrame()
                : base(GreenFrame)
            {
            }
        }

        public const string Finished = "FN";
        public class finished : Constant<String>
        {
            public finished()
                : base(Finished)
            {
            }
        }

        public const string PlaceIntoOperation = "PO";
        public class placeIntoOperation : Constant<String>
        {
            public placeIntoOperation()
                : base(PlaceIntoOperation)
            {
            }
        }

        public class UI
        {
            public const string BlackFrame = "Black Frame";
            public const string WhiteFrame = "White Frame";
            public const string GreenFrame = "Green Frame";
            public const string Finished = "Finished";
            public const string PlaceIntoOperation = "Placed into Operation";
        }
    }
}

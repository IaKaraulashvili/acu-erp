﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.StatusOfConstruction
{
    public class StatusOfConstructionListAttribute : PXStringListAttribute
    {
        public StatusOfConstructionListAttribute() : base(
        new string[]{
                StatusOfConstruction.BlackFrame,
                StatusOfConstruction.WhiteFrame,
                StatusOfConstruction.GreenFrame,
                StatusOfConstruction.Finished,
                StatusOfConstruction.PlaceIntoOperation
        },
        new string[]{
                StatusOfConstruction.UI.BlackFrame,
                StatusOfConstruction.UI.WhiteFrame,
                StatusOfConstruction.UI.GreenFrame,
                StatusOfConstruction.UI.Finished,
                StatusOfConstruction.UI.PlaceIntoOperation
        })
        { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.TbilisiDistrict
{
    public class TbilisiDistrict
    {
        public const string GldaniDistrict = "GL";
        public const string DidubeDistrict = "DB";
        public const string VakeDistrict = "VK";
        public const string IsaniDistrict = "IS";
        public const string KrtsanisiDistrict = "KR";
        public const string MtatsmindaDistrict = "MD";
        public const string NadzaladeviDistrict = "ND";
        public const string SaburtaloDistrict = "SB";
        public const string SamgoriDistrict = "SM";
        public const string ChuguretiDistrict = "CH";
    }
}

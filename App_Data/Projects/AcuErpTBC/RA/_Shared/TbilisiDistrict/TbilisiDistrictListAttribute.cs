﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.TbilisiDistrict
{
    public class TbilisiDistrictListAttribute : PXStringListAttribute
    {
        public TbilisiDistrictListAttribute() : base(

        new string[]{
                TbilisiDistrict.GldaniDistrict,
                TbilisiDistrict.DidubeDistrict,
                TbilisiDistrict.VakeDistrict,
                TbilisiDistrict.IsaniDistrict,
                TbilisiDistrict.KrtsanisiDistrict,
                TbilisiDistrict.MtatsmindaDistrict,
                TbilisiDistrict.NadzaladeviDistrict,
                TbilisiDistrict.SaburtaloDistrict,
                TbilisiDistrict.SamgoriDistrict,
                TbilisiDistrict.ChuguretiDistrict
            },
            new string[]{
                "Gldani District",
                "Didube District",
                "Vake District",
                "Isani District",
                "Krtsanisi District",
                "Mtatsminda District",
                "Nadzaladevi District",
                "Saburtalo District",
                "Samgori District",
                "Chugureti District"
            })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.Transmission
{
    public static class Transmission
    {
        public const string Manual = "MA";
        public class manual : Constant<string>
        {
            public manual()
                : base(Manual)
            {
            }
        }

        public const string Automatic = "AU";
        public class automatic : Constant<string>
        {
            public automatic()
                : base(Automatic)
            {
            }
        }

        public const string SemiAutomatic = "SA";
        public class semiAutomatic : Constant<string>
        {
            public semiAutomatic()
                : base(SemiAutomatic)
            {
            }
        }

        public const string Tiptronic = "TI";
        public class tiptronic : Constant<string>
        {
            public tiptronic()
                : base(Tiptronic)
            {
            }
        }

        public class UI
        {
            public const string Manual = "Manual";
            public const string Automatic = "Automatic";
            public const string SemiAutomatic = "Semi Automatic";
            public const string Tiptronic = "Tiptronic";
        }


    }
}

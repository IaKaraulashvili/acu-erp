﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.Transmission
{
    class TransmissionListAttribute : PXStringListAttribute
    {
        public TransmissionListAttribute() : base(
            new string[]
            {
                Transmission.Manual,
                Transmission.Automatic,
                Transmission.SemiAutomatic,
                Transmission.Tiptronic
            },
            new string[]
            {
                Transmission.UI.Manual,
                Transmission.UI.Automatic,
                Transmission.UI.SemiAutomatic,
                Transmission.UI.Tiptronic
            })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.VehicleCondition
{
    public static class VehicleCondition
    {
        public const string Good = "GD";
        public class good : Constant<string>
        {
            public good()
                : base(Good)
            {
            }
        }

        public const string Bad = "BD";
        public class bad : Constant<string>
        {
            public bad()
                : base(Bad)
            {
            }
        }

        public const string Medium = "MD";
        public class medium : Constant<string>
        {
            public medium()
                : base(Medium)
            {
            }
        }

        public class UI
        {
            public const string Good = "Good";
            public const string Bad = "Bad";
            public const string Medium = "Medium";
        }
    }
}

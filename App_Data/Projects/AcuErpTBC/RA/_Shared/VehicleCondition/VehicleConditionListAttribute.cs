﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.VehicleCondition
{
    public class VehicleConditionListAttribute : PXStringListAttribute
    {
        public VehicleConditionListAttribute() : base(
            new string[]
            {
                VehicleCondition.Good,
                VehicleCondition.Bad,
                VehicleCondition.Medium
            },
            new string[]
            {
                VehicleCondition.UI.Good,
                VehicleCondition.UI.Bad,
                VehicleCondition.UI.Medium
            })
        { }
    }
}

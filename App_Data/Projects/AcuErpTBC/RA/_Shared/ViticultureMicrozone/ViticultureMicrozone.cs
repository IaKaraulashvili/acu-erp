﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ViticultureMicrozone
{
    public static class ViticultureMicrozone
    {
        public const string Khvanchkara = "KV";
        public class khvanchkara : Constant<String>
        {
            public khvanchkara()
                : base(Khvanchkara)
            {
            }
        }

        public const string Ateni = "AT";
        public class ateni : Constant<String>
        {
            public ateni()
                : base(Ateni)
            {
            }
        }

        public const string Gurjaani = "GJ";
        public class gurjaani : Constant<String>
        {
            public gurjaani()
                : base(Gurjaani)
            {
            }
        }

        public const string Vazisubani = "VZ";
        public class vazisubani : Constant<String>
        {
            public vazisubani()
                : base(Vazisubani)
            {
            }
        }

        public const string Kardenakhi = "KR";
        public class kardenakhi : Constant<String>
        {
            public kardenakhi()
                : base(Kardenakhi)
            {
            }
        }

        public const string Kotekhi = "KT";
        public class kotekhi : Constant<String>
        {
            public kotekhi()
                : base(Kotekhi)
            {
            }
        }

        public const string Akhasheni = "AK";
        public class akhasheni : Constant<String>
        {
            public akhasheni()
                : base(Akhasheni)
            {
            }
        }

        public const string Mukuzani = "MK";
        public class mukuzani : Constant<String>
        {
            public mukuzani()
                : base(Mukuzani)
            {
            }
        }

        public const string Sviri = "SV";
        public class sviri : Constant<String>
        {
            public sviri()
                : base(Sviri)
            {
            }
        }

        public const string Tsinandali = "TS";
        public class tsinandali : Constant<String>
        {
            public tsinandali()
                : base(Tsinandali)
            {
            }
        }

        public const string Nafareuli = "NF";
        public class nafareuli : Constant<String>
        {
            public nafareuli()
                : base(Nafareuli)
            {
            }
        }

        public const string Teliani = "TL";
        public class teliani : Constant<String>
        {
            public teliani()
                : base(Teliani)
            {
            }
        }

        public const string Kakheti = "KH";
        public class kakheti : Constant<String>
        {
            public kakheti()
                : base(Kakheti)
            {
            }
        }

        public const string Manavi = "MN";
        public class manavi : Constant<String>
        {
            public manavi()
                : base(Manavi)
            {
            }
        }

        public const string Tibaani = "TB";
        public class tibaani : Constant<String>
        {
            public tibaani()
                : base(Tibaani)
            {
            }
        }

        public const string Qindzmarauli = "QN";
        public class qindzmarauli : Constant<String>
        {
            public qindzmarauli()
                : base(Qindzmarauli)
            {
            }
        }

        public const string Kvareli = "KL";
        public class kvareli : Constant<String>
        {
            public kvareli()
                : base(Kvareli)
            {
            }
        }

        public const string Tvishi = "TV";
        public class tvishi : Constant<String>
        {
            public tvishi()
                : base(Tvishi)
            {
            }
        }

        public class UI
        {
            public const string Khvanchkara = "Khvanchkara";
            public const string Ateni = "Ateni";
            public const string Gurjaani = "Gurjaani";
            public const string Vazisubani = "Vazisubani";
            public const string Kardenakhi = "Kardenakhi";
            public const string Kotekhi = "Kotekhi";
            public const string Akhasheni = "Akhasheni";
            public const string Mukuzani = "Mukuzani";
            public const string Sviri = "Sviri";
            public const string Tsinandali = "Tsinandali";
            public const string Nafareuli = "Nafareuli";
            public const string Teliani = "Teliani";
            public const string Kakheti = "Kakheti";
            public const string Manavi = "Manavi";
            public const string Tibaani = "Tibaani";
            public const string Qindzmarauli = "Qindzmarauli";
            public const string Kvareli = "Kvareli";
            public const string Tvishi = "Tvishi";
        }
    }
}

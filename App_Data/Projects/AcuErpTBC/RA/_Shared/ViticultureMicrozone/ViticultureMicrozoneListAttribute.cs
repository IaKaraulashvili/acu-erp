﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.ViticultureMicrozone
{
    public class ViticultureMicrozoneListAttribute : PXStringListAttribute
    {
        public ViticultureMicrozoneListAttribute() : base(
        new string[]{
                ViticultureMicrozone.Khvanchkara,
                ViticultureMicrozone.Ateni,
                ViticultureMicrozone.Gurjaani,
                ViticultureMicrozone.Vazisubani,
                ViticultureMicrozone.Kardenakhi,
                ViticultureMicrozone.Kotekhi,
                ViticultureMicrozone.Akhasheni,
                ViticultureMicrozone.Mukuzani,
                ViticultureMicrozone.Sviri,
                ViticultureMicrozone.Tsinandali,
                ViticultureMicrozone.Nafareuli,
                ViticultureMicrozone.Teliani,
                ViticultureMicrozone.Kakheti,
                ViticultureMicrozone.Manavi,
                ViticultureMicrozone.Tibaani,
                ViticultureMicrozone.Qindzmarauli,
                ViticultureMicrozone.Kvareli,
                ViticultureMicrozone.Tvishi
        },
        new string[]{
                ViticultureMicrozone.UI.Khvanchkara,
                ViticultureMicrozone.UI.Ateni,
                ViticultureMicrozone.UI.Gurjaani,
                ViticultureMicrozone.UI.Vazisubani,
                ViticultureMicrozone.UI.Kardenakhi,
                ViticultureMicrozone.UI.Kotekhi,
                ViticultureMicrozone.UI.Akhasheni,
                ViticultureMicrozone.UI.Mukuzani,
                ViticultureMicrozone.UI.Sviri,
                ViticultureMicrozone.UI.Tsinandali,
                ViticultureMicrozone.UI.Nafareuli,
                ViticultureMicrozone.UI.Teliani,
                ViticultureMicrozone.UI.Kakheti,
                ViticultureMicrozone.UI.Manavi,
                ViticultureMicrozone.UI.Tibaani,
                ViticultureMicrozone.UI.Qindzmarauli,
                ViticultureMicrozone.UI.Kvareli,
                ViticultureMicrozone.UI.Tvishi
        })
        { }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.WheelLocation
{
    public static class WheelLocation
    {
        public const string Left = "LE";
        public class left : Constant<string>
        {
            public left()
                : base(Left)
            {
            }
        }
        public const string Right = "RI";
        public class right : Constant<string>
        {
            public right()
                : base(Right)
            {
            }
        }

        public class UI
        {
            public const string Left = "Left";
            public const string Right = "Right";
        }
    }
}

﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared.WheelLocation
{
    class WheelLocationListAttribute : PXStringListAttribute
    {
        public WheelLocationListAttribute() : base(
            new string[]
            {
                WheelLocation.Left,
                WheelLocation.Right
            },
            new string[]
            {
                WheelLocation.UI.Left,
                WheelLocation.UI.Right
            })
        { }
    }
}

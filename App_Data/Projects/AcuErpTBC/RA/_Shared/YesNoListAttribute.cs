﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA._Shared
{
    public class YesNoListAttribute : PXStringListAttribute
    {
        public YesNoListAttribute() : base(

        new string[]{
                YesNoList.Yes,
                YesNoList.No
            },
            new string[]{
                "Yes",
                "No"
            })
        { }
    }
}

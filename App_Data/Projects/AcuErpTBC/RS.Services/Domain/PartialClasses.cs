﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS.Services.Domain
{
    public partial class WAYBILLGOODS_LISTGOODS
    {
        private int? _WaybillItemNbr = 0;

        public int? WaybillItemNbr
        {
            get
            {
                return _WaybillItemNbr;
            }
            set
            {
                _WaybillItemNbr = value;
            }
        }
    }

    public partial class WAYBILL
    {
        private string _WaybillNbr;

        public string WaybillNbr
        {
            get
            {
                return _WaybillNbr;
            }
            set
            {
                _WaybillNbr = value;
            }
        }

        public string Trailer
        {
            get
            {
                if (TRANS_ID != "4")
                {
                    return TRANS_TXT;
                }
                return null;
            }
            set
            {
                if (TRANS_ID != "4")
                {
                    TRANS_TXT = value;
                }
            }
        }

        public string OtherTransText
        {
            get
            {
                if (TRANS_ID == "4")
                {
                    return TRANS_TXT;
                }
                return null;
            }
            set
            {
                if (TRANS_ID == "4")
                {
                    TRANS_TXT = value;
                }
            }
        }
    }
}

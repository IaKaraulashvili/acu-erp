﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS.Services.Domain
{
    /// <summary>
    /// ანგარიშ/ფაქტურის სერვისის მომხმარებელი
    /// </summary>
    public class TaxInvoiceUser
    {
        /// <summary>
        /// სერვისის მომხმარებლის უნიკალური ნომერი.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// სერვისის მომხმარებლის სახელი.
        /// </summary>
        public string USER_NAME { get; set; }

        /// <summary>
        /// ელ დეკლარირების მომხმარებლის უნიკალური ნომერი.
        /// </summary>
        public int? USER_ID { get; set; }

        /// <summary>
        /// სერვისის მომხმარებლის IP.
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// მაღაზიის/ობიექტის სახელი.
        /// </summary>
        public string NOTES { get; set; }
    }

    /// <summary>
    /// ანგარიშ/ფაქტურის სერვისის მომხმარებელის მაღაზია/ობიექტი
    /// </summary>
    public class TaxInvoiceUserCompany
    {
        /// <summary>
        /// სერვისი მომხმარებლის უნიკალური ნომერი
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// მაღაზიის/ობიექტის სახელი.
        /// </summary>
        public string NOTES { get; set; }
    }


    public class TaxInvoiceWaybill
    {

        public int? ID { get; set; }
        public int? INV_ID { get; set; }

        public string OVERHEAD_NO { get; set; }

        public DateTime? OVERHEAD_DT { get; set; }
    }

    /// <summary>
    /// ანგარიშ/ფაქტურის საქონლი
    /// </summary>
    public class TaxInvoiceItem
    {
        /// <summary>
        /// ანგარიშ ფაქტურის საქონლის მონაცემის უნიკალური ნომერი.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// ანგარიშ ფაქტურის უნიკალური ნომერი.
        /// </summary>
        public int? INV_ID { get; set; }

        /// <summary>
        /// საქონლის დასახელება.
        /// </summary>
        public string GOODS { get; set; }

        /// <summary>
        /// საქონლის ერთეული.
        /// </summary>
        public string G_UNIT { get; set; }

        /// <summary>
        /// რაოდენობა.
        /// </summary>
        public decimal? G_NUMBER { get; set; }

        /// <summary>
        /// თანხა დღგ-ს და აქციზის ჩათვლლით.
        /// </summary>
        public decimal? FULL_AMOUNT { get; set; }

        /// <summary>
        /// დღგ-ს თანხა.
        /// </summary>
        public decimal? DRG_AMOUNT { get; set; }

        /// <summary>
        /// აქციზი.
        /// </summary>
        public decimal? AQCIZI_AMOUNT { get; set; }

        /// <summary>
        /// აქციზური საქონლის კოდი.
        /// </summary>
        public int? AKCIS_ID { get; set; }

        /// <summary>
        /// სასაქონლო ზედნადების უნიკალური ნომერი.
        /// </summary>
        public int? WAYBILL_ID { get; set; }

        /// <summary>
        /// დღგ სტრიქონული ტიპის.
        /// </summary>
        public string SDRG_AMOUNT { get; set; }
    }

    /// <summary>
    /// ანგარიშ ფაქტურა
    /// </summary>
    public class TaxInvoice
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// ანგარიშფაქტურის სერია
        /// </summary>
        public string F_SERIES { get; set; }

        /// <summary>
        /// ანგარიშფაქტურის ნომერი
        /// </summary>
        public string F_NUMBER { get; set; }

        /// <summary>
        /// ოპერაციის განხორციელების თარიღი
        /// </summary>
        public DateTime? OPERATION_DT { get; set; }

        /// <summary>
        /// რეგისტრაციის თარიღი
        /// </summary>
        public DateTime? REG_DT { get; set; }

        /// <summary>
        /// გამყიდველის გადამხდელის უნიკალური ნომერი
        /// </summary>
        public int? SELLER_UN_ID { get; set; }

        /// <summary>
        /// მყიდველის გადამხდელის უნიკალური ნომერი
        /// </summary>
        public int? BUYER_UN_ID { get; set; }

        /// <summary>
        /// ზედნადებს ნომერი
        /// </summary>
        public string OVERHEAD_NO { get; set; }

        /// <summary>
        /// ანგარიშფაქტურის სტატუსი
        /// </summary>
        public int? STATUS { get; set; }

        /// <summary>
        /// გამყიდველის დეკლარაციის ნომერი
        /// </summary>
        public int? SEQ_NUM_S { get; set; }

        /// <summary>
        /// ელ დეკლარირების მომხმარებლის უნიკალური ნომერი
        /// </summary>
        public int? USER_ID { get; set; }

        /// <summary>
        /// სერვისის მომხმარებლის უნიკალური ნომერი
        /// </summary>
        public int? S_USER_ID { get; set; }

        /// <summary>
        /// კორექტირების ანგარიშ ფაქტურის ნომერი
        /// </summary>
        public int? K_ID { get; set; }

        /// <summary>
        /// კორექტირების ტიპი
        /// </summary>
        public string K_TYPE { get; set; }

        /// <summary>
        /// ანგარიშ/ფაქტურის გამაუქმებელის გადამხდელის უნიკალური ნომერი
        /// </summary>
        public int? R_UN_ID { get; set; }

        /// <summary>
        /// ამოღებულია
        /// </summary>
        public int? NO_STATUS { get; set; }

        /// <summary>
        /// ამოღებულია
        /// </summary>
        public string NO_TEXT { get; set; }

        /// <summary>
        /// უარყოფილი მეორე მხარის მიერ 0- არა 1-კი
        /// </summary>
        public bool? WAS_REF { get; set; }

        /// <summary>
        /// მყიდველის დეკლარაციის ნომერი
        /// </summary>
        public int? SEQ_NUM_B { get; set; }

        /// <summary>
        /// ინვოისის უნიკალური ნომერი ?
        /// </summary>
        public int? INVOICE_ID { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        public string DOC_MOS_NOM_S { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        public string DOC_MOS_NOM_B { get; set; }

        /// <summary>
        /// ზედნადების თარიღი
        /// </summary>
        public DateTime? OVERHEAD_DT { get; set; }

        /// <summary>
        /// მყიდველის სერვისის მომხმარებლის უნიკალური ნომერი
        /// </summary>
        public int? B_S_USER_ID { get; set; }

        /// <summary>
        /// მყიდველის საიდენტიფიკაციო ნომერი
        /// </summary>
        public string SA_IDENT_NO { get; set; }

        /// <summary>
        /// მყიდველის/გადამხდელის დასახელება
        /// </summary>
        public string ORG_NAME { get; set; }

        /// <summary>
        /// მყიდველის მაღაზიის ნომერი
        /// </summary>
        public string NOTES { get; set; }

        /// <summary>
        /// თანხა ?
        /// </summary>
        public decimal? TANXA { get; set; }

        /// <summary>
        /// შეთანხმების თარიღი ?
        /// </summary>
        public DateTime? AGREE_DATE { get; set; }

        /// <summary>
        /// ოპერაციის განხორციელების თარიღი ?
        /// </summary>
        public DateTime? OP_DT { get; set; }

        /// <summary>
        /// თარიღი ?
        /// </summary>
        public DateTime? DT { get; set; }

        /// <summary>
        /// მყიდველის/გადამხდელის საიდენტიფიკაციო ნომერი
        /// </summary>
        public string BUYER_TIN { get; set; }

        /// <summary>
        /// გამყიდველის საიდენტიფიკაციო ნომერი
        /// </summary>
        public string SELLER_TIN { get; set; }

        /// <summary>
        /// მყიდველის/გადამხდელის დასახელება ?
        /// </summary>
        public string BUYER_DESC { get; set; }

        /// <summary>
        /// გამყიდველის დასახელება ?
        /// </summary>
        public string SELLER_DESC { get; set; }

        /// <summary>
        /// კორექტირებული ა/ფ სერია ?
        /// </summary>
        public string F_SERIES_K { get; set; }

        /// <summary>
        /// კორექტირებული ა/ფ ნომერი ?
        /// </summary>
        public string F_NUMBER_K { get; set; }

        /// <summary>
        /// კორექტირების თარიღი?
        /// </summary>
        public DateTime? OP_DT_K { get; set; }


        public TaxInvoiceItem[] TaxInvoiceItems { get; set; }


    }

    /// <summary>
    /// აქციზური საქონლის კოდები და უნიკალური ნომრები
    /// </summary>
    public class TaxInvoiceExcise
    {
        /// <summary>
        /// აქციზური საქონლის უნიკალური კოდი.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// აქციზური დაქონლის დასახელება.
        /// </summary>
        public string TITLE { get; set; }

        /// <summary>
        /// ზომის ერთეული.
        /// </summary>
        public string MEASUREMENT { get; set; }

        /// <summary>
        /// აქციზური საქონლის კოდი.
        /// </summary>
        public string SAKON_KODI { get; set; }

        /// <summary>
        /// განაკვეთი.
        /// </summary>
        public string AKCIS_GANAKV { get; set; }
    }

    public class TaxInvoiceRequest
    {
        /// <summary>
        /// უნიკალური ნომერი
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// მყიდველის/გადამხდელის უნიკალური ნომერი
        /// </summary>
        public int? BAYER_UN_ID { get; set; }

        /// <summary>
        /// გამყიდველის უნიკალური ნომერი
        /// </summary>
        public int? SELLER_UN_ID { get; set; }

        /// <summary>
        /// ზედნადების ნომერი
        /// </summary>
        public string OVERHEAD_NO { get; set; }

        /// <summary>
        /// თარიღი
        /// </summary>
        public DateTime? DT { get; set; }

        /// <summary>
        /// კომენტარი
        /// </summary>
        public string NOTES { get; set; }

        /// <summary>
        /// სტატუსი
        /// </summary>
        public int? STATUS { get; set; }

        /// <summary>
        /// რეგისტრაციის თარიღი
        /// </summary>
        public DateTime? REG_DT { get; set; }

        /// <summary>
        /// დადასტურების თარიღი
        /// </summary>
        public DateTime? ACSEPT_DT { get; set; }

        /// <summary>
        /// გამყიდველის ორგანიზაციის დასახელება
        /// </summary>
        public string SELLER_ORG_NAME { get; set; }

        /// <summary>
        /// მყიდველის ორგანიზაციის დასახელება
        /// </summary>
        public string BUYER_ORG_NAME { get; set; }

        /// <summary>
        /// გამყიდველის საიდენტიფიკაციო ნომერი 
        /// </summary>
        public string SELLER_TIN { get; set; }

        /// <summary>
        /// მყიდველის საიდენტიფიკაციო ნომერი
        /// </summary>
        public string BUYER_TIN { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        public DateTime? READ_DATE { get; set; }

        /// <summary>
        /// შეტყობინების ტექსტი
        /// </summary>
        public string NOTIFICATION_TEXT { get; set; }

        /// <summary>
        /// ნანახია თუ არა ?
        /// </summary>
        public bool? VIEWED { get; set; }

        /// <summary>
        /// მყიდველის საიდენტიფიკაციო ნომერი
        /// </summary>
        public string SA_IDENT_NO { get; set; }

        /// <summary>
        /// ორგანიზაციის დასახელება
        /// </summary>
        public string ORG_NAME { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        public string SIDE { get; set; }

    }
}

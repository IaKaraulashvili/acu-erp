﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS.Services.Domain.Waybill.Dto
{
    public class WaybillFilterDto
    {
        public string WaybillNumber { get; set; }
        public string Itpes { get; set; }
        public string BuyerTin { get; set; }
        public string Statuses { get; set; }
        public string CarNumber { get; set; }
        public string DriverTin { get; set; }
        public decimal? FullAmount { get; set; }
        public DateTime? CreateDateStart { get; set; }
        public DateTime? CreateDateEnd { get; set; }
        public DateTime? BeginDateStart { get; set; }
        public DateTime? BeginDateEnd { get; set; }
        public DateTime? DeliveryDateStart { get; set; }
        public DateTime? DeliveryDateEnd { get; set; }
        public DateTime? CloseDateStart { get; set; }
        public DateTime? CloseDateEnd { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
    }
}

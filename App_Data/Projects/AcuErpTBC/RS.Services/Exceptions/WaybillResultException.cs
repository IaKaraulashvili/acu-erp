﻿using System;
using System.Runtime.Serialization;

namespace RS.Services
{
    [Serializable]
    public class WaybillResultException : Exception
    {
        public string ErrorCode { get; private set; }

        public WaybillResultException(string errorCode)
        {
            ErrorCode = errorCode;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RS.Services.Helpers
{
    public class ExcelHelper
    {
        public static void ExportToExcel(GridView gvList)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Waybills.xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";

            // New Logic
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.BinaryWrite(Encoding.UTF8.GetPreamble());

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();

                    //  include the gridline settings
                    table.GridLines = gvList.GridLines;

                    //  add the header row to the table
                    if (gvList.HeaderRow != null)
                    {
                        PrepareControlForExport(gvList.HeaderRow);
                        table.Rows.Add(gvList.HeaderRow);
                    }

                    //  add each of the data rows to the table
                    foreach (GridViewRow row in gvList.Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);
                    }

                    //  add the footer row to the table
                    if (gvList.FooterRow != null)
                    {
                        PrepareControlForExport(gvList.FooterRow);
                        table.Rows.Add(gvList.FooterRow);
                    }

                    //  render the table into the htmlwriter


                    table.RenderControl(htw);

                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];

                if (current is System.Web.UI.WebControls.LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as System.Web.UI.WebControls.LinkButton).Text));
                }
                else if (current is System.Web.UI.WebControls.ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as System.Web.UI.WebControls.ImageButton).AlternateText));
                }
                else if (current is System.Web.UI.WebControls.HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as System.Web.UI.WebControls.HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "Yes" : "No"));
                }

                // Fields Which Must Be Removed
                if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }

                // Recurse
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }
    }
}

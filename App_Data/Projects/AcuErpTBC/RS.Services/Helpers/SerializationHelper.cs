﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;

namespace RS.Services.Helpers
{
    public class SerializationHelper
    {
        public static T Deserialize<T>(XElement objectXml)
        {
            using (MemoryStream resultStream = new MemoryStream())
            {
                objectXml.Save(resultStream);

                using (StreamReader resultStreamReader = new StreamReader(resultStream))
                {
                    resultStream.Position = 0;

                    XmlSerializer xSerializer = new XmlSerializer(typeof(T));

                    return (T)xSerializer.Deserialize(resultStreamReader);
                }
            }
        }

        public static XElement Serialize<T>(T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XDocument documentXml = new XDocument();
            using (XmlWriter xw = documentXml.CreateWriter())
            {
                serializer.Serialize(xw, obj);
                xw.Close();
            }
            XElement objectXml = documentXml.Root;
            return objectXml;
        }
    }

    public static class DocumentExtensions
    {
        public static XmlDocument ToXmlDocument(this XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        public static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }
    }
}

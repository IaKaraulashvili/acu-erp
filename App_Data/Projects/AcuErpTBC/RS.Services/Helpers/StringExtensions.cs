﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AG.Utils.Extensions;

namespace RS.Services.Helpers
{
    public static class StringExtensions
    {
        public static DateTime? ToDateTimeRS(this string val)
        {
            return val.ToDateTime(format: CultureInfo.InvariantCulture);
        }

        public static decimal? ToDecimalRS(this string val)
        {
            return val.ToDecimal(defaultValue: 0, format: CultureInfo.InvariantCulture);
        }
    }
}

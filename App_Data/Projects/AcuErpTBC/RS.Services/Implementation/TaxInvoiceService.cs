﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RS.Services.Helpers;
using System.Configuration;
using System.IO;
using System.Xml;
//using System.Data.Linq.SqlClient;
using System.Data;
using System.Data.Common;
using System.Transactions;
using RS.Services.Domain;

namespace RS.Services.Implementation
{
    public class TaxInvoiceService : IDisposable
    {
        #region Private Fields

        private string su = string.Empty;
        private string sp = string.Empty;
        private int sua = 0;
        private int user_id = 0;
        private string url = @"https://www.revenue.mof.ge/ntosservice/ntosservice.asmx";
        private int timeout = 0;
        private Services.TaxInvoiceService.NtosServiceSoapClient _Service;

        #endregion

        #region ctor
        public TaxInvoiceService(string serviceUser, string servicePassword, string url, int? timeout)
        {
            this.su = serviceUser;
            this.sp = servicePassword;
            if (!string.IsNullOrEmpty(url))
                this.url = url;
            this.timeout = timeout.HasValue ? timeout.Value : 0;

            var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.Transport);
            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;

            var address = new System.ServiceModel.EndpointAddress(url);

            _Service = new RS.Services.TaxInvoiceService.NtosServiceSoapClient(binding, address);

            if (timeout > 0)
                _Service.InnerChannel.OperationTimeout = new TimeSpan(this.timeout / 3600, this.timeout / 60, this.timeout % 60);

            this.check();
        }

        public void Dispose()
        {
            (_Service as IDisposable).Dispose();
        }
        #endregion
        #region Service Methods
        ///// <summary>
        ///// აბრუნებს ანგარიშ/ფაქტურის სერვისს.
        ///// </summary>
        ///// <returns>აბრუნებს სერვისს</returns>
        //private RS.Services.TaxInvoiceService.NtosServiceSoapClient GetTaxInvoiceService()
        //{
        //    var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.Transport);
        //    var address = new System.ServiceModel.EndpointAddress(url);
        //    var service = new RS.Services.TaxInvoiceService.NtosServiceSoapClient(binding, address);
        //    if (timeout > 0)
        //        service.InnerChannel.OperationTimeout = new TimeSpan(timeout / 3600, timeout / 60, timeout % 60);
        //    return service;
        //}

        /// <summary>
        /// სერვისის მომხმარებლის ცვლილების მეთოდი.
        /// იმ პარამეტრში რომლის ცვლილებაც არ ხდება გადაეცით სტრიქონული ტიპის ცარიელი ცვლადი
        /// </summary>
        /// <param name="user_name">ელექტრონული დეკლარირების მომხმარებელი.</param>
        /// <param name="user_password">ელექტრონული დეკლარირების მომხმარებელიs პაროლი.</param>
        /// <param name="ip">იმ კომპიუტერის IP რომლიდანაც მოახდენს ამ სერვისის მომხმარებელი ვებ სერვისის მეთოდების გამოძახებას.</param>
        /// <param name="notes">მაღაზიის/ობიექტის სახელი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს სერვისის მომხმარებელის შექმნის შესახებ.
        /// </returns>
        public bool update_ser_user(string user_name, string user_password, string notes)
        {
            return _Service.update_ser_user(user_name, user_password, _Service.what_is_my_ip(), notes, su, sp);
        }

        /// <summary>
        /// IP მისამართის დადგენის მეთოდი.
        /// </summary>
        /// /// <returns>
        /// სერვისის მეთოდი აბრუნებს IP მისამართის.
        /// </returns>
        public string what_is_my_ip()
        {

            return _Service.what_is_my_ip();
        }

        /// <summary>
        /// სერვისის მომხმარებლების სრული სიის გამოტანის მეთოდი.
        /// </summary>
        /// <param name="user_name">ელექტრონული დეკლარირების მომხმარებელი.</param>
        /// <param name="user_password">ელექტრონული დეკლარირების მომხმარებელიs პაროლი.</param>
        /// <param name="user_id">გამომავალი პარამეტრი რომელიც არის ელექტრონული დეკლარირების მომხმარებლის უნიკალური ნომერი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ლისტს, შემდეგი მონაცემებით:
        /// Id - სერვისის მომხმარებლის უნიკალური ნომერი
        /// user_name - სერვისის მომხმარებლის სახელი
        /// user__id - ელ დეკლარირების მომხმარებლის უნიკალური ნომერი 
        /// ip - სერვისის მომხმარებლის IP
        /// notes - მაღაზიის/ობიექტის სახელი
        /// </returns>
        public List<TaxInvoiceUser> get_ser_users(string user_name, string user_password, out int user__id)
        {
            var taxInvoiceUsers = DataTableMapper.ToList<TaxInvoiceUser>(_Service.get_ser_users(out user_id, user_name, user_password));
            user__id = user_id;
            return taxInvoiceUsers;
        }



        /// <summary>
        /// სერვისის მომხმარებლების მაღაზიების სიის გამოტანა.
        /// </summary>
        /// <param name="tin">გადამხდელის საიდენტიფიკაციო ნომერი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ლისტს რომელიც შედგება შემდეგი სვეტებით:
        /// id -სერვისი მომხმარებლის უნიკალური ნომერი
        /// notes - მაღაზიის/ობიექტის სახელი
        /// </returns>
        public List<TaxInvoiceUserCompany> get_ser_users_notes(string tin)
        {
            return DataTableMapper.ToList<TaxInvoiceUserCompany>(_Service.get_ser_users_notes(tin));
        }

        /// <summary>
        /// სერვისის მომხმარებლების პაროლის შემოწმება.
        /// </summary>
        /// <returns> 
        /// სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რომელიც ნიშნავს რომ პაროლი სწორია.
        /// მეთოდი აკეთებს განახლებას შემდეგი ველების:
        /// su - სერვისის მომხმარბლის სახელი
        /// sp - სერვისი მომხმარებლის პაროლი
        /// user_id - ორმხრივი პარამეტრი დეკლარირების მომხმარებილის უნიკალური ნომერი
        /// sua - გამომავალი პარამეტრი სერვისის მომხმარებლის უნიკალური ნომერი
        /// </returns>
        public bool check()
        {
            int user__id = 0;
            var isChecked = _Service.chek(su, sp, ref user__id, out sua);
            if (user__id != 0)
                user_id = user__id;
            return isChecked;
        }

        /// <summary>
        /// ანგარიშ/ფაქტურის შენახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ორმხრივი პარამეტრი ანგარიშ/ფაქტურის უნიკალური ნომერი, ახალი ანგარიშ/ფაქტურის შექმნის შემთხვევაში გადაეცით „0“.</param>
        /// <param name="operation_date">ოპერაციის განხორციელების თარიღი.</param>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <param name="buyer_un_id">მყიდველის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ხმარებიდან ამოღებულია (სტრიქონული ტიპის ცარიელი მნიშვნელობა).</param>
        /// <param name="overhead_dt">ხმარებიდან ამოღებულია (ნებისმიერი თარიღი).</param>
        /// <param name="b_s_user_id">მყიდველის/გამყიდველის სერვისის მომხმარებლის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ ანგარიშ/ფაქტურა შეიქმნა.</returns>
        public bool save_invoice(ref int invois_id, DateTime operation_date, int seller_un_id, int buyer_un_id, string overhead_no, DateTime overhead_dt, int b_s_user_id)
        {
            return _Service.save_invoice(user_id, ref invois_id, operation_date, seller_un_id, buyer_un_id, overhead_no, overhead_dt, b_s_user_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ/ფაქტურის შენახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ორმხრივი პარამეტრი ანგარიშ/ფაქტურის უნიკალური ნომერი, ახალი ანგარიშ/ფაქტურის შექმნის შემთხვევაში გადაეცით „0“.</param>
        /// <param name="operation_date">ოპერაციის განხორციელების თარიღი.</param>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <param name="buyer_un_id">მყიდველის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ხმარებიდან ამოღებულია (სტრიქონული ტიპის ცარიელი მნიშვნელობა).</param>
        /// <param name="overhead_dt">ხმარებიდან ამოღებულია (ნებისმიერი თარიღი).</param>
        /// <param name="b_s_user_id">მყიდველის/გამყიდველის სერვისის მომხმარებლის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ ანგარიშ/ფაქტურა შეიქმნა.</returns>
        public bool save_buyer_invoice(ref int invois_id, DateTime operation_date, string seller_tin)
        {
            string name = string.Empty;
            int buyer_un_id = _Service.get_un_id_from_user_id(user_id, su, sp);
            int seller_un_id = _Service.get_un_id_from_tin(out name, user_id, seller_tin, su, sp);

            return _Service.save_invoice(user_id, ref invois_id, operation_date, seller_un_id, buyer_un_id, "", operation_date, 0, su, sp);
        }

        /// <summary>
        /// ანგარიშ/ფაქტურის შენახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ორმხრივი პარამეტრი ანგარიშ/ფაქტურის უნიკალური ნომერი, ახალი ანგარიშ/ფაქტურის შექმნის შემთხვევაში გადაეცით „0“.</param>
        /// <param name="operation_date">ოპერაციის განხორციელების თარიღი.</param>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <param name="buyer_un_id">მყიდველის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ხმარებიდან ამოღებულია (სტრიქონული ტიპის ცარიელი მნიშვნელობა).</param>
        /// <param name="overhead_dt">ხმარებიდან ამოღებულია (ნებისმიერი თარიღი).</param>
        /// <param name="b_s_user_id">მყიდველის/გამყიდველის სერვისის მომხმარებლის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ ანგარიშ/ფაქტურა შეიქმნა.</returns>
        public bool save_seller_invoice(ref int invois_id, DateTime operation_date, string buyer_tin)
        {
            string name = string.Empty;
            int seller_un_id = _Service.get_un_id_from_user_id(user_id, su, sp);
            int buyer_un_id = _Service.get_un_id_from_tin(out name, user_id, buyer_tin, su, sp);
            return _Service.save_invoice(user_id, ref invois_id, operation_date, seller_un_id, buyer_un_id, "", operation_date, 0, su, sp);
        }


        /// <summary>
        /// საკომპენსაციო თანხის (ავანსის) ანგარიშ-ფაქტურების შენახვის მეთოდი
        /// </summary>
        /// <param name="invois_id">ორმხრივი პარამეტრი ანგარიშ/ფაქტურის უნიკალური ნომერი, ახალი ანგარიშ/ფაქტურის შექმნის შემთხვევაში გადაეცით „0“.</param>
        /// <param name="operation_date">ოპერაციის განხორციელების თარიღი.</param>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <param name="buyer_un_id">მყიდველის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ხმარებიდან ამოღებულია (სტრიქონული ტიპის ცარიელი მნიშვნელობა).</param>
        /// <param name="overhead_dt">ხმარებიდან ამოღებულია (ნებისმიერი თარიღი).</param>
        /// <param name="b_s_user_id">მყიდველის/გამყიდველის სერვისის მომხმარებლის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ ანგარიშ/ფაქტურა შეიქმნა.</returns>
        public bool save_seller_invoice_a(ref int invois_id, DateTime operation_date, string buyer_tin)
        {
            string name = string.Empty;
            int seller_un_id = _Service.get_un_id_from_user_id(user_id, su, sp);
            int buyer_un_id = _Service.get_un_id_from_tin(out name, user_id, buyer_tin, su, sp);
            return _Service.save_invoice_a(user_id, ref invois_id, operation_date, seller_un_id, buyer_un_id, "", operation_date, 0, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის ნახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="f_series">ანგარიშ ფაქტურის სერია.</param>
        /// <param name="f_number">ანგარიშფაქტურის ნომერი.</param>
        /// <param name="operation_dt">ოპერაციის განხორციელების თარიღი.</param>
        /// <param name="reg_dt">რაგისტრაციის თარიღი.</param>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <param name="buyer_un_id">მყიდველის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">გამოვალი პარამეტრი ზედნადების ნომერი.</param>
        /// <param name="overhead_dt">ზედნადების თარიღი.</param>
        /// <param name="status">ანგარიშ/ფაქტურის სტატუსი.</param>
        /// <param name="seq_num_s">გამყიდველის დეკლარაციის ნომერი.</param>
        /// <param name="seq_num_b">მყიდველის დეკლარაციის ნომერი.</param>
        /// <param name="k_id">ანგარიშ/ფაქტურის ნომერი რომლის მაკორექტირებელიც არის მოცემული ანგარიშ/ფაქტურა. 
        /// „-1“ - ანგ/ფაქტურა კორექტირებული არ არის.</param>
        /// <param name="r_un_id">ფაქტურის გამუქმებელის გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="k_type">კორექტირების ტიპი.</param>
        /// <param name="b_s_user_id">მყიდველის სერვისის მომხმარებლის უნიკალური ნომერი.</param>
        /// <param name="dec_status">დეკლარაციის სტატუსი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს, რომ მონაცემები წარმატებით გაიგზავნა.</returns>
        public bool get_invoice(int invois_id, out string f_series, out int f_number, out DateTime operation_dt, out DateTime reg_dt, out int seller_un_id, out int buyer_un_id, out string overhead_no, out DateTime overhead_dt, out int status, out string seq_num_s, out string seq_num_b, out int k_id, out int r_un_id, out int k_type, out int b_s_user_id, out int dec_status)
        {
            return _Service.get_invoice(out f_series, out f_number, out operation_dt, out reg_dt, out seller_un_id, out buyer_un_id, out overhead_no, out overhead_dt, out status, out seq_num_s, out seq_num_b, out k_id, out r_un_id, out k_type, out b_s_user_id, out dec_status, user_id, invois_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ/ფაქტურის საქონლის მონაცემების შენახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="id">ორმხრივი პარამეტრი ანგარიშ/ფაქტურის საქონლის მონაცემის უნიკალური ნომერი . ახლის შესაქმნელად გადაეცემა 0.</param>
        /// <param name="invois_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="goods">საქონლის/სერისის დასახელება.</param>
        /// <param name="g_unit">ერთეულის ზომა.</param>
        /// <param name="g_number">რაოდენომა.</param>
        /// <param name="full_amount">თანხა დღგ- და აქციზის ჩათვლით.</param>
        /// <param name="drg_amount">დღგ-ს თანხა.</param>
        /// <param name="aqcizi_amount">აქციზის თანხა.</param>
        /// <param name="akciz_id">აქციზური საქონლის კოდი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ მონაცემები წარმატებით შეინახა</returns>
        public bool save_invoice_desc(ref int id, int invois_id, string goods, string g_unit, decimal g_number, decimal full_amount, decimal drg_amount, decimal aqcizi_amount, int akciz_id)
        {

            return _Service.save_invoice_desc(user_id, ref id, su, sp, invois_id, goods, g_unit, g_number, full_amount, drg_amount, aqcizi_amount, akciz_id);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის საქონლის მონაცემების ნახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს რომელიც შეიცავს შემდეგ მონაცემებს:
        /// id -ანგარიშ ფაქტურის საქონლის მონაცემის უნიკალური ნომერი
        /// inv_id - ანგარიშ ფაქტურის უნიკალური ნომერი
        /// goods - საქონლის დასახელება
        /// g_unit - საქონლის ერთეული
        /// g_number - რაოდენობა
        /// full_amount - თანხა დღგ-ს და აქციზის ჩათვლლით
        /// drg_amount - დღგ
        /// aqcizi_amount - აქციზი
        /// akcis_id - აქციზური საქონლის კოდი
        /// waybill_id - სასაქონლო ზედნადების უნიკალური ნომერი
        /// sdrg_amount - დღგ სტრიქონული ტიპის 
        /// </returns>
        public List<TaxInvoiceItem> get_invoice_desc(int invois_id)
        {
            var datatable = _Service.get_invoice_desc(user_id, invois_id, su, sp);
            var data = DataTableMapper.ToList<TaxInvoiceItem>(datatable);
            return data;
        }

        /// <summary>
        /// ანგარიშ ფაქტურის ზედნადებების წამოღების მეთოდი
        /// </summary>
        /// <param name="invois_id">ანგარიშ ფაქტურის უნიკალური ნომერი</param>
        /// <returns></returns>
        public List<TaxInvoiceWaybill> get_ntos_invoices_inv_nos(int invois_id)
        {
            var datatable = _Service.get_ntos_invoices_inv_nos(user_id, invois_id, su, sp);
           
            var data = DataTableMapper.ToList<TaxInvoiceWaybill>(datatable);
            return data;
        }


        /// <summary>
        /// ანგარიშ ფაქტურის საქონლის მონაცემის წაშლის სერვისის მეთოდი.
        /// </summary>
        /// <param name="id">ანგარიშ ფაქტურის საქონლის მონაცემის უნიკალური ნომერი.</param>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს, რომ მონაცემები წარმატებით წაიშლა</returns>
        public bool delete_invoice_desc(int id, int inv_id)
        {
            return _Service.delete_invoice_desc(user_id, id, inv_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის ზედნადების შენახვის სერვისის მეთოდი.
        /// </summary>
        /// <param name="invois_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ზედნადების ნომერი.</param>
        /// <param name="overhead_dt">ზედნადების თარიღი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს რომ მონაცემები წარმატებით შეინახა.</returns>
        public bool save_ntos_invoices_inv_nos(int invois_id, string overhead_no, DateTime overhead_dt)
        {
            return _Service.save_ntos_invoices_inv_nos(invois_id, user_id, overhead_no, overhead_dt, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის ზედნადების წაშლის სერვისის მეთოდი.
        /// </summary>
        /// <param name="id">ანგარიშ ფაქტურის საქონლის მონაცემის უნიკალური ნომერი.</param>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს, რაც ნიშნავს, რომ მონაცემები წარმატებით წაიშლა.</returns>
        public bool delete_ntos_invoices_inv_nos(int id, int inv_id)
        {
            return _Service.delete_ntos_invoices_inv_nos(user_id, id, inv_id, su, sp);
        }

        /// <summary>
        /// საიდენტიფიკაციო ნომრიდან გადამხდელის უნიკალური ნომრის გაგება.
        /// </summary>
        /// <param name="tin">გადამხდელის საიდენტიფიკაციო ნომერი.</param>
        /// <param name="name">გამოვალი პარამეტრი გადამხდელის დასახელება.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს მთელი რიცხვის ტიპის მონაცემს რომელიც არის გადამხდელის უნიკალური ნომერი</returns>
        public int get_un_id_from_tin(string tin, out string name)
        {
            return _Service.get_un_id_from_tin(out name, user_id, tin, su, sp);
        }



        /// <summary>
        /// საიდენტიფიკაციო ნომრიდან გადამხდელის უნიკალური ნომრის გაგება.
        /// </summary>
        /// <param name="tin">გადამხდელის საიდენტიფიკაციო ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს მთელი რიცხვის ტიპის მონაცემს რომელიც არის გადამხდელის უნიკალური ნომერი</returns>
        public int get_un_id_from_tin(string tin)
        {
            string name;
            return _Service.get_un_id_from_tin(out name, user_id, tin, su, sp);
        }

        /// <summary>
        /// გადამხდელის უნიკალური ნომრით საიდენტიფიკაციო ნომრის გაგება.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="name">გამოვალი პარამეტრი გადამხდელის დასა.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს მთელი რიცხვის ტიპის მონაცემს რომელიც არის გადამხდელის საიდენტიფიკაციო კოდი</returns>
        public string get_tin_from_un_id(int un_id, out string name)
        {
            return _Service.get_tin_from_un_id(out name, user_id, un_id, su, sp);

        }

        /// <summary>
        /// გადამხდელის უნიკალური ნომრით გადამხდელის სახელის გაგება გაგება.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს სტრიქონის ტიპის მონაცემს – გადამხდელის დასახელება.</returns>
        public string get_org_name_from_un_id(int un_id)
        {
            return _Service.get_org_name_from_un_id(user_id, un_id, su, sp);
        }

        /// <summary>
        /// ელექტრონული დეკლარირების მომხმარებლის ნომრიდან გადამხდელის უნიკალური ნომრის გაგება.
        /// </summary>
        /// <returns>სერვისის მეთოდი აბრუნებს მთელი რიცხვის ტიპის მონაცემს – გადამხდელის უნიკალური ნომერი</returns>
        public int get_un_id_from_user_id()
        {
            return _Service.get_un_id_from_user_id(user_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის სტატუსის ცვლილება.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="status">სტატუსი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, ანგარიშ ფაქტურას სტატუსი შეეცვალა.</returns>
        public bool change_invoice_status(int inv_id, int status)
        {
            return _Service.change_invoice_status(user_id, inv_id, status, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის სტატუსის ცვლილება (დასტური).
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="status">სტატუსი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, ანგარიშ ფაქტურას სტატუსი შეეცვალა.</returns>
        public bool accept_invoice_status(int inv_id, int status)
        {
            return _Service.acsept_invoice_status(user_id, inv_id, status, su, sp);

        }

        /// <summary>
        /// ანგარიშ ფაქტურის სტატუსის ცვლილება (უარყოფა).
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="ref_text">უარყოფის მიზეზი.</param>
        /// <returns></returns>
        public bool ref_invoice_status(int inv_id, string ref_text)
        {
            return _Service.ref_invoice_status(user_id, inv_id, ref_text, su, sp);

        }

        /// <summary>
        /// ანგარიშ ფაქტურის სტატუსის კორექტირება.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="k_type">კორექტირების ტიპი.</param>
        /// <param name="k_id">გამოვავალი პარამეტრი მაკორექტირებელი ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რომელიც ნიშნავს რომ კორექტირება წარმატებით დასრულდა</returns>
        public bool k_invoice(int inv_id, int k_type, out int k_id)
        {
            return _Service.k_invoice(out k_id, user_id, inv_id, k_type, su, sp);

        }

        /// <summary>
        /// კორექტირებული ანგარიშ ფაქტურის მაკორექტირებელის უნიკალური ნომრის გაგება.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="k_id">გამომავალი პარამეტრი . მაკორექტირებელი ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, მონაცემი წარმატებით დაბრუნდა</returns>
        public bool get_makoreqtirebeli(int inv_id, out int k_id)
        {
            return _Service.get_makoreqtirebeli(out k_id, user_id, inv_id, su, sp);

        }

        /// <summary>
        /// გამყიდველის მხარეს გამოწერილი ანგარიშ ფაქტურების სიის გამოტანა.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="s_dt">გამოწერის თარიღის საწყისი თარიღი.</param>
        /// <param name="e_dt">გამოწერის თარიღის საბოლოო თარიღი.</param>
        /// <param name="op_s_dt">ოპერაციის განხორციელების საწყისი თარიღი.</param>
        /// <param name="op_e_dt">ოპერაციის განხორციელების საბოლოო თარიღი.</param>
        /// <param name="invoice_no">ანგარიშ ფაქტურის ნომერი.</param>
        /// <param name="sa_ident_no">მყიდველის საიდენტიფიკაციო ნომერი.</param>
        /// <param name="desc">მყიდველის დასახელება.</param>
        /// <param name="doc_mos_nom">დეკლარაციის ნომერი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს რომელიც შედგება შემდეგი სვეტებით:
        /// Id - ანგარიშფაქტურის უნიკალური ნომერი
        /// f_series - ანგარიშფაქტურის სერია
        /// f_number - ანგარიშფაქტურის ნომერი
        /// operation_dt - ოპერაციის განხორციელების თარიღი
        /// reg_dt -რეგისტრაციის თარიღი
        /// status - ანგარიშფაქტურის სტატუსი
        /// seq_num_s - გამყიდველის დეკლარაციის ნომერი
        /// s_user_id - სერვისის მომხმარებლის უნიკალური ნომერი
        /// k_id - კორექტირების ანგარიშ ფაქტურის ნომერი
        /// was_ref - უარყოფილი მეორე მხარის მიერ 0- არა 1-კი
        /// doc_mos_nom_s - ამოღებულია
        /// sa_ident_no- მყიდველის საიდენტიფიკაციო ნომერი
        /// org_name –მყიდველის დასახელება
        /// notes - მყიდველის მაღაზიის ნომერი
        /// tanxa - თანხა ?
        /// agree_date - შეთანხმების თარიღი ?
        /// </returns>
        public List<TaxInvoice> get_seller_invoices(DateTime s_dt, DateTime e_dt, DateTime op_s_dt, DateTime op_e_dt, string invoice_no, string sa_ident_no, string desc, string doc_mos_nom)
        {
            var datatable = _Service.get_seller_invoices(user_id, _Service.get_un_id_from_user_id(user_id, su, sp), s_dt, e_dt, op_s_dt, op_e_dt, invoice_no, sa_ident_no, desc, doc_mos_nom, su, sp);
            var data = DataTableMapper.ToList<TaxInvoice>(datatable);
            return data;
        }

        /// <summary>
        /// მყიდველის მხარეს გამოწერილი ანგარიშ ფაქტურების სიის გამოტანა.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="s_dt">გამოწერის თარიღის საწყისი თარიღი.</param>
        /// <param name="e_dt">გამოწერის თარიღის საბოლოო თარიღი.</param>
        /// <param name="op_s_dt">ოპერაციის განხორციელების საწყისი თარიღი.</param>
        /// <param name="op_e_dt">ოპერაციის განხორციელების საბოლოო თარიღი.</param>
        /// <param name="invoice_no">ანგარიშ ფაქტურის ნომერი.</param>
        /// <param name="sa_ident_no">მყიდველის საიდენტიფიკაციო ნომერი.</param>
        /// <param name="desc">მყიდველის დასახელება.</param>
        /// <param name="doc_mos_nom">დეკლარაციის ნომერი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს რომელიც შედგება შემდეგი სვეტებით:
        /// Id - ანგარიშფაქტურის უნიკალური ნომერი
        /// f_series - ანგარიშფაქტურის სერია
        /// f_number - ანგარიშფაქტურის ნომერი
        /// operation_dt - ოპერაციის განხორციელების თარიღი
        /// reg_dt -რეგისტრაციის თარიღი
        /// status - ანგარიშფაქტურის სტატუსი
        /// seq_num_s - გამყიდველის დეკლარაციის ნომერი
        /// s_user_id - სერვისის მომხმარებლის უნიკალური ნომერი
        /// k_id - კორექტირების ანგარიშ ფაქტურის ნომერი
        /// was_ref - უარყოფილი მეორე მხარის მიერ 0- არა 1-კი
        /// doc_mos_nom_s - ამოღებულია
        /// sa_ident_no- მყიდველის საიდენტიფიკაციო ნომერი
        /// org_name –მყიდველის დასახელება
        /// notes - მყიდველის მაღაზიის ნომერი
        /// tanxa - თანხა ?
        /// agree_date - შეთანხმების თარიღი ?
        /// b_s_user_id - მყიდველის სერვისის მომხმარებლის უნიკალური ნომერი 
        /// seller_un_id - გამყიდველის გადამხდელის უნიკალური ნომერი
        /// </returns>
        public List<TaxInvoice> get_buyer_invoices(int un_id, DateTime s_dt, DateTime e_dt, DateTime op_s_dt, DateTime op_e_dt, string invoice_no, string sa_ident_no, string desc, string doc_mos_nom)
        {
            var datatable = _Service.get_buyer_invoices(user_id, un_id, s_dt, e_dt, op_s_dt, op_e_dt, invoice_no, sa_ident_no, desc, doc_mos_nom, su, sp);
            var data = DataTableMapper.ToList<TaxInvoice>(datatable);
            return data;
        }

        /// <summary>
        /// დარეგისტრირებული ანგარიშ ფაქტურების ნომრების გამოტანა.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="v_invoice_n">ანგარიშ ფაქტურის ნომერი (არასრული ).</param>
        /// <param name="v_count">გამოსატანი ჩანაწერების რაოდენობა.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს შემდეგ მონაცემს
        /// f_number - ანგარიშ ფაქტურის ნომერი
        /// </returns>
        public List<string> get_invoice_numbers(int un_id, string v_invoice_n, int v_count)
        {
            var datatable = _Service.get_invoice_numbers(user_id, un_id, v_invoice_n, v_count, su, sp);
            var data = datatable.AsEnumerable().Where(row => row.Field<string>("F_NUMBER") != null && row.Field<string>("F_NUMBER") != "")
                           .Select(r => r.Field<string>("F_NUMBER"))
                           .ToList();
            return data;
        }

        /// <summary>
        /// მყიდველის (მეორე მხარის) საიდენტიფიკაციო ნომრები.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="v_invoice_t">გადამხდელის საიდენტიფიკაციო ნომერი (არასრული).</param>
        /// <param name="v_count">გამოსატანი ჩანაწერების რაოდენობა.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს შემდეგ მონაცემს:
        /// sa_ident_no- გადამხდელის საიდენტიფიკაციო ნომერი
        /// </returns>
        public List<string> get_invoice_tins(string v_invoice_t, int v_count)
        {
            var datatable = _Service.get_invoice_tins(user_id, _Service.get_un_id_from_user_id(user_id, su, sp), v_invoice_t, v_count, su, sp);
            var data = datatable.AsEnumerable().Where(row => row.Field<string>("SA_IDENT_NO") != null && row.Field<string>("SA_IDENT_NO") != "")
                           .Select(r => r.Field<string>("SA_IDENT_NO"))
                           .ToList();
            return data;
        }

        /// <summary>
        /// დეცლარაციების ნომრები რომლებზედაც ანგარიშ ფაქტურებია მიბმული.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="v_invoice_d">დეცლარაციის ნომერი (არასრული).</param>
        /// <param name="v_count">გამოსატანი ჩანაწერების რაოდენობა.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს შემდეგ მონაცემს:
        /// seq_num - გადამხდელის საიდენტიფიკაციო ნომერი
        /// </returns>
        public List<string> get_invoice_d(int un_id, string v_invoice_d, int v_count)
        {
            var datatable = _Service.get_invoice_d(user_id, un_id, v_invoice_d, v_count, su, sp);
            var data = datatable.AsEnumerable().Where(row => row.Field<string>("SEQ_NUM") != null && row.Field<string>("SEQ_NUM") != "")
                           .Select(r => r.Field<string>("SEQ_NUM"))
                           .ToList();
            return data;
        }

        /// <summary>
        /// ანგარიშ ფაქტურების სია რომელიც მოითხოვს რეაგირება გამყიდველის მხარე.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="status">ანგარიშ ფაქტურების სტატუსების ბიტური ნამრავლი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს რომელიც შედგება შემდეგი სვეტებით:
        /// Id - ანგარიშფაქტურის უნიკალური ნომერი
        /// status - ანგარიშფაქტურის სტატუსი
        /// seq_num_s - გამყიდველის დეკლარაციის ნომერი
        /// was_ref - უარყოფილი მეორე მხარის მიერ 0- არა 1-კი
        /// f_series - ანგარიშფაქტურის სერია
        /// f_number - ანგარიშფაქტურის ნომერი
        /// reg_dt -რეგისტრაციის თარიღი
        /// operation_dt - ოპერაციის განხორციელების თარიღი
        /// s_user_id - სერვისის მომხმარებლის უნიკალური ნომერი
        /// b_s_user_id –მყიდველის სერვისის მომხმარებლის უნიკალური ნომერი
        /// doc_mos_nom_s - ამოღებულია
        /// doc_mos_nom -დეცლარაციის ნომერი
        /// sa_ident_no -გადამხდელის საიდენტიფიკაციო ნომერი
        /// org_name -გადამხდელის დასახელება
        /// notes -სერვისი მომხმარებლის (მაღაზიის )სახელი
        /// tanxa - თანხა ?
        /// </returns>
        public List<TaxInvoice> get_seller_invoices_r(int un_id, int status)
        {
            var datatable = _Service.get_seller_invoices_r(user_id, un_id, status, su, sp);
            var data = DataTableMapper.ToList<TaxInvoice>(datatable);
            return data;
        }

        /// <summary>
        /// ანგარიშ ფაქტურების სია რომელიც მოითხოვს რეაგირება მყიდველის მხარე.
        /// </summary>
        /// <param name="un_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="status">ანგარიშ ფაქტურების სტატუსების ბიტური ნამრავლი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს რომელიც შედგება შემდეგი სვეტებით:
        /// Id - ანგარიშფაქტურის უნიკალური ნომერი
        /// seller_un_id -გამყიდველის გადამხდელის უნიკალური ნომერი
        /// seq_num_b -მყიდველის დეკლარაციის ნომერი
        /// status - ანგარიშფაქტურის სტატუსი
        /// was_ref - უარყოფილი მეორე მხარის მიერ 0- არა 1-კი
        /// f_series - ანგარიშფაქტურის სერია
        /// f_number - ანგარიშფაქტურის ნომერი
        /// reg_dt -რეგისტრაციის თარიღი
        /// operation_dt - ოპერაციის განხორციელების თარიღი
        /// s_user_id - სერვისის მომხმარებლის უნიკალური ნომერი
        /// b_s_user_id – მყიდველის სერვისის მომხმარებლის უნიკალური ნომერი
        /// doc_mos_nom_b – ამოღებულია
        /// sa_ident_no - გადამხდელის საიდენტიფიკაციო ნომერი
        /// org_name - გადამხდელის დასახელება
        /// notes - სერვისი მომხმარებლის (მაღაზიის)სახელი
        /// tanxa - თანხა ?
        /// </returns>
        public List<TaxInvoice> get_buyer_invoices_r(int un_id, int status)
        {
            var datatable = _Service.get_buyer_invoices_r(user_id, un_id, status, su, sp);
            var data = DataTableMapper.ToList<TaxInvoice>(datatable);
            return data;
        }

        /// <summary>
        /// მონაცემები საბეჭდი ფორმისთვის.
        /// </summary>
        /// <param name="inv_id">გადამხდელის უნიკალური ნომერი.</param>
        /// <returns>მეთოდი აბრუნებს TaxInvoice ობიექტს</returns>
        public TaxInvoice print_invoices(int inv_id)
        {
            var datatable = _Service.print_invoices(user_id, inv_id, su, sp);
            var data = DataTableMapper.ToList<TaxInvoice>(datatable);
            return data.Count > 0 ? data[0] : null;
        }

        /// <summary>
        /// აქციზური საქონლის კოდების და უნიკალური ნომრების გამოტანა.
        /// </summary>
        /// <param name="s_text">აქციზური საქონლის კოდი.</param>
        /// <returns>
        /// სერვისის მეთოდი აბრუნებს ცხრილს შემდეგი მონაცემებით:
        /// Id- აქციზური საქონლის უნიკალური კოდი
        /// Title- აქციზური დაქონლის დასახელება
        /// Measurement- ზომის ერთეული
        /// sakon_kodi- აქციზური საქონლის კოდი
        /// akcis_ganakv- განაკვეთი
        /// </returns>
        public List<TaxInvoiceExcise> get_akciz(string s_text)
        {
            var datatable = _Service.get_akciz(s_text, user_id, su, sp);
            var data = DataTableMapper.ToList<TaxInvoiceExcise>(datatable);
            return data;
        }

        /// <summary>
        /// დეცლარაციის ნომრების გამოტანა პერიოდის მიხედვით.
        /// </summary>
        /// <param name="sag_periodi">საგადასახადო პერიოდი.</param>
        /// <returns>
        /// სერვისის მეთოდი შემდეგ მონაცემებს:
        /// seq_num - დეცლარაციის ნომერი
        /// </returns>
        public List<string> get_seq_nums(string sag_periodi)
        {
            var datatable = _Service.get_seq_nums(sag_periodi, user_id, su, sp);
            var data = datatable.AsEnumerable().Where(row => row.Field<string>("SEQ_NUM") != null && row.Field<string>("SEQ_NUM") != "")
                           .Select(r => r.Field<string>("SEQ_NUM"))
                           .ToList();
            return data;
        }

        /// <summary>
        /// ანგარიშფაქტურის დეკლარაციაზე მიბმა.
        /// </summary>
        /// <param name="seq_num">დეცლარაციი ნომერი.</param>
        /// <param name="inv_id">ანგარიშფაქტურის ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, ანგარიშ ფაქტურა დეკლარაციას მიება</returns>
        public bool add_inv_to_decl(int seq_num, int inv_id)
        {
            return _Service.add_inv_to_decl(user_id, seq_num, inv_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენება (მოთხოვნა) გამყიდველისთვის.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="bayer_un_id">მყიდველის გადამხდელიუნიკალური ნომერი.</param>
        /// <param name="seller_un_id">გამყიდველის გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">ზედნადების ნომერი.</param>
        /// <param name="dt">თარიღი.</param>
        /// <param name="notes">კომენტარი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, მონაცემი შეინახა</returns>
        public bool save_invoice_request(int inv_id, int bayer_un_id, int seller_un_id, string overhead_no, DateTime dt, string notes)
        {
            return _Service.save_invoice_request(inv_id, user_id, bayer_un_id, seller_un_id, overhead_no, dt, notes, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენების (მოთხოვნა) წაშლა გამყიდველისთვის.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="bayer_un_id">მყიდველის გადამხდელიუნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, მონაცემი წაიშალა</returns>
        public bool del_invoice_request(int inv_id, int bayer_un_id)
        {
            return _Service.del_invoice_request(inv_id, user_id, bayer_un_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენების (მოთხოვნა) გამოტანა.
        /// </summary>
        /// <param name="inv_id">ანგარიშ ფაქტურის უნიკალური ნომერი.</param>
        /// <param name="bayer_un_id">გამოვალი პარამეტრი მყიდველის გადამხდელიუნიკალური ნომერი.</param>
        /// <param name="seller_un_id">გამოვალი პარამეტრი გამყიდველის გადამხდელის უნიკალური ნომერი.</param>
        /// <param name="overhead_no">გამოვალი პარამეტრი ზედნადების ნომერი.</param>
        /// <param name="dt">გამოვალი პარამეტრი თარიღი.</param>
        /// <param name="notes">გამოვალი პარამეტრი კომენტარი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, მონაცემი გამოიტანა.</returns>
        public bool get_invoice_request(int inv_id, out int bayer_un_id, out int seller_un_id, out string overhead_no, out DateTime dt, out string notes)
        {
            return _Service.get_invoice_request(out bayer_un_id, out seller_un_id, out overhead_no, out dt, out notes, inv_id, user_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენების (მოთხოვნა) გადაგზავნა.
        /// </summary>
        /// <param name="id">ანგარიშ მოთხოვნის უნიკალური ნომერი.</param>
        /// <param name="seller_un_id">გამყიდველის გადამხდელის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ლოგიკურ ცვლადს რაც ნიშნავს რომ, მონაცემი გადაიგზავნა.</returns>
        public bool acsept_invoice_request_status(int id, int seller_un_id)
        {
            return _Service.acsept_invoice_request_status(id, user_id, seller_un_id, su, sp);
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენებების (მოთხოვნა) სია გამყიდველის მხარეს.
        /// </summary>
        /// <param name="seller_un_id">გამყიდველის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ცხრილს</returns>
        public List<TaxInvoiceRequest> get_requested_invoices(int seller_un_id)
        {
            var datatable = _Service.get_requested_invoices(user_id, seller_un_id, su, sp);
            var data = DataTableMapper.ToList<TaxInvoiceRequest>(datatable);
            return data;
        }

        /// <summary>
        /// ანგარიშ ფაქტურის გამოწერის შეხსენებების (მოთხოვნა) სია მყიდველის მხარეს.
        /// </summary>
        /// <param name="bayer_un_id">ანგარიშ მოთხოვნის უნიკალური ნომერი.</param>
        /// <returns>სერვისის მეთოდი აბრუნებს ცხრილს</returns>
        public List<TaxInvoiceRequest> get_invoice_requests(int bayer_un_id)
        {
            var datatable = _Service.get_invoice_requests(bayer_un_id, user_id, su, sp);
            var data = DataTableMapper.ToList<TaxInvoiceRequest>(datatable);
            return data;
        }

        #endregion

    }
}

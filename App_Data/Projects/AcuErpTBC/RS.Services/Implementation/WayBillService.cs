﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RS.Services.Helpers;
using System.Configuration;
using System.IO;
using System.Xml;
//using System.Data.Linq.SqlClient;
using System.Data;
using System.Data.Common;
using System.Transactions;
using RS.Services.Domain;
using AG.Utils.Extensions;
namespace RS.Services.Implementation
{
    public class WaybillService : IDisposable
    {
        #region Private Fields

        private RS.Services.WaybillService.WayBillsSoapClient _Service;

        private string su = string.Empty;
        private string sp = string.Empty;
        private int un_id = 0;
        private int s_user_id = 0;
        private string url = @"https://services.rs.ge/WaybillService/WaybillService.asmx";
        private int timeout = 0;

        #endregion

        #region Public Methods

        public WaybillService(string serviceUser, string servicePassword, string url, int? timeout)
        {
            this.su = serviceUser;
            this.sp = servicePassword;
            if (!string.IsNullOrEmpty(url))
                this.url = url;
            this.timeout = timeout.HasValue ? timeout.Value : 0;

            var binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.Transport);
            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;

            var address = new System.ServiceModel.EndpointAddress(url);

            _Service = new RS.Services.WaybillService.WayBillsSoapClient(binding, address);

            if (timeout > 0)
                _Service.InnerChannel.OperationTimeout = new TimeSpan(this.timeout / 3600, this.timeout / 60, this.timeout % 60);
        }

        public bool CheckServiceUser()
        {
            return _Service.chek_service_user(su, sp, out un_id, out s_user_id);
        }

        public WAYBILL_UNITSWAYBILL_UNIT[] GetWaybillUnitTypes()
        {
            XElement waybill_units = null;
            waybill_units = _Service.get_waybill_units(su, sp);
            WAYBILL_UNITS data = SerializationHelper.Deserialize<WAYBILL_UNITS>(waybill_units);
            if (data != null)
                return data.Items;
            return null;
        }

        public TRANSPORT_TYPESTRANSPORT_TYPE[] GetTransportationTypes()
        {
            XElement trans_types = null;
            trans_types = _Service.get_trans_types(su, sp);
            var data = SerializationHelper.Deserialize<TRANSPORT_TYPES>(trans_types);
            if (data != null)
                return data.Items;
            else return null;
        }

        public BAR_CODES GetBarCodes(string barCode)
        {
            XElement barCodes = null;
            _Service.get_bar_codes(su, sp, barCode, out barCodes);
            return SerializationHelper.Deserialize<BAR_CODES>(barCodes);
        }

        public ERROR_CODES GetErrorCodes()
        {
            XElement errorCodes = null;
            errorCodes = _Service.get_error_codes(su, sp);
            return SerializationHelper.Deserialize<ERROR_CODES>(errorCodes);
        }

        public WAYBILL GetWaybill(int waybillID)
        {
            XElement waybillXml = null;
            waybillXml = _Service.get_waybill(su, sp, waybillID);

            CheckResult(waybillXml);

            return SerializationHelper.Deserialize<WAYBILL>(waybillXml);
        }

        public WAYBILL[] GetWaybills(
            string waybillNumber = null,
            DateTime? createDateStart = null,
            DateTime? createDateEnd = null,
            DateTime? beginDateStart = null,
            DateTime? beginDateEnd = null,
            DateTime? deliveryDateStart = null,
            DateTime? deliveryDateEnd = null,
            DateTime? closeDateStart = null,
            DateTime? closeDateEnd = null)
        {
            XElement waybillsXml = null;
            waybillsXml = _Service.get_waybills_ex(
                su: su,
                sp: sp,
                itypes: null,
                statuses: null,
                buyer_tin: null,
                car_number: null,
                begin_date_s: beginDateStart,
                begin_date_e: beginDateEnd,
                create_date_s: createDateStart,
                create_date_e: createDateEnd,
                driver_tin: null,
                delivery_date_s: deliveryDateStart,
                delivery_date_e: deliveryDateEnd,
                full_amount: null,
                waybill_number: waybillNumber,
                close_date_s: closeDateStart,
                close_date_e: closeDateEnd,
                s_user_ids: null,
                comment: null,
                is_confirmed: null);

            CheckResult(waybillsXml);

            WAYBILL_LIST sourceList = SerializationHelper.Deserialize<WAYBILL_LIST>(waybillsXml);

            if (sourceList.Items == null)
            {
                return null;
            }

            var waybillList = new List<WAYBILL>();

            foreach (var item in sourceList.Items)
            {
                if (item.ID.IsNullOrEmpty())
                {
                    continue;
                }

                var waybill = GetWaybill(int.Parse(item.ID));

                if (waybill != null)
                    waybillList.Add(waybill);
            }

            return waybillList.ToArray();
        }

        public WAYBILL[] GetReceivedWaybills(
            string waybillNumber = null,
            DateTime? createDateStart = null,
            DateTime? createDateEnd = null,
            DateTime? beginDateStart = null,
            DateTime? beginDateEnd = null,
            DateTime? deliveryDateStart = null,
            DateTime? deliveryDateEnd = null,
            DateTime? closeDateStart = null,
            DateTime? closeDateEnd = null)
        {
            XElement waybillsXml = null;
            waybillsXml = _Service.get_buyer_waybills_ex(
                su: su,
                sp: sp,
                itypes: null,
                seller_tin: null,
                statuses: null,
                car_number: null,
                begin_date_s: beginDateStart,
                begin_date_e: beginDateEnd,
                create_date_s: createDateStart,
                create_date_e: createDateEnd,
                driver_tin: null,
                delivery_date_s: deliveryDateStart,
                delivery_date_e: deliveryDateEnd,
                full_amount: null,
                waybill_number: waybillNumber,
                close_date_s: closeDateStart,
                close_date_e: closeDateEnd,
                s_user_ids: null,
                comment: null,
                is_confirmed: null);


            CheckResult(waybillsXml);

            WAYBILL_LIST wb = SerializationHelper.Deserialize<WAYBILL_LIST>(waybillsXml);

            if (wb.Items == null)
            {
                return null;
            }

            var waybillList = new List<WAYBILL>();

            foreach (var item in wb.Items)
            {
                if (item.WAYBILL_NUMBER == null)
                {
                    continue;
                }

                var waybill = GetWaybill(int.Parse(item.ID));

                if (waybill != null)
                {
                    waybillList.Add(waybill);
                }
            }

            return waybillList.ToArray();
        }

        public WAYBILL[] GetReceivedWaybillsFull(
            DateTime startDate,
            DateTime endDate,
            DateTime canceledStartDate)
        {
            var idList = new List<int>();

            Action<List<int>> addToList = (List<int> list) =>
            {
                if (list != null)
                {
                    idList.AddRange(list);
                }
            };

            //Get new waybills by transportation start  date  (split by 71 interval hours)  (sent to carrier case)
            var beginDateStart = startDate;
            while (beginDateStart < endDate)
            {
                var temp = beginDateStart.AddHours(71);

                if (temp <= endDate)
                {
                    addToList(GetReceivedWaybillIds(statuses: ",1,", beginDateStart: beginDateStart, beginDateEnd: temp));
                }
                else
                {
                    addToList(GetReceivedWaybillIds(statuses: ",1,", beginDateStart: beginDateStart, beginDateEnd: endDate));
                }

                beginDateStart = temp;
            }


            // Get new waybills
            addToList(GetReceivedWaybillIds(statuses: ",1,", createDateStart: startDate, createDateEnd: endDate));

            // Get closed waybills
            addToList(GetReceivedWaybillIds(statuses: ",2,", closeDateStart: startDate, closeDateEnd: endDate));

            // Get canceled waybills
            addToList(GetReceivedWaybillIds(statuses: ",-2,", createDateStart: canceledStartDate, createDateEnd: endDate));

            // Get corrected waybills
            addToList(GetCorrectedWaybillIds(startDate, endDate));

            // Remove duplicate ids
            idList = idList.Distinct().ToList();

            var waybillList = new List<WAYBILL>();

            foreach (var id in idList)
            {
                var waybill = GetWaybill(id);

                if (waybill != null)
                {
                    waybillList.Add(waybill);
                }
            }

            return waybillList.ToArray();
        }



        public RESULT SaveWaybill(WAYBILL waybill)
        {
            XElement waybillXml = null;
            XElement waybillSaveResult = null;
            waybill.SELER_UN_ID = un_id.ToString();
            waybill.S_USER_ID = s_user_id.ToString();

            waybillXml = SerializationHelper.Serialize(waybill);

            waybillXml = RemoveAllNamespaces(waybillXml);

            waybillSaveResult = _Service.save_waybill(su, sp, waybillXml);
            RESULT data = SerializationHelper.Deserialize<RESULT>(waybillSaveResult);
            return data;
        }

        public RESULT DeleteWaybillItem(WAYBILL waybill)
        {
            XElement waybillXml = null;
            XElement waybillSaveResult = null;

            waybillXml = SerializationHelper.Serialize(waybill);
            waybillXml = RemoveAllNamespaces(waybillXml);

            waybillSaveResult = _Service.save_waybill(su, sp, waybillXml);
            RESULT data = SerializationHelper.Deserialize<RESULT>(waybillSaveResult);
            return data;
        }

        public string ActivateWaybill(WAYBILL waybill)
        {
            return _Service.send_waybill(su, sp, int.Parse(waybill.ID));
        }

        public string ActivateWaybill(int waybillID)
        {
            return _Service.send_waybill(su, sp, waybillID);
        }

        public string ActivateWaybill(int waybillID, DateTime transStartDate)
        {

            return _Service.send_waybil_vd(su, sp, transStartDate, waybillID);
        }

        public int CloseWaybills(int waybillID, DateTime? deliveryDate = null)
        {
            int status;

            if (!deliveryDate.HasValue)
            {
                status = _Service.close_waybill(su, sp, waybillID);
            }
            else
            {
                status = _Service.close_waybill_vd(su, sp, deliveryDate.Value, waybillID);
            }

            return status;
        }

        public int CancelWaybills(int waybillID)
        {
            int status;

            status = _Service.ref_waybill(su, sp, waybillID);

            return status;
        }

        public int DeleteWaybills(int waybill_id)
        {
            int status;
            status = _Service.del_waybill(su, sp, waybill_id);

            return status;
        }

        public bool ConfirmWaybill(int waybillID)
        {
            return _Service.confirm_waybill(su, sp, waybillID);
        }

        public bool RejectWaybill(int waybillID)
        {
            return _Service.reject_waybill(su, sp, waybillID);
        }

        public WAYBILL[] GetCorrectedWaybills(DateTime startDate, DateTime endDate)
        {
            var corWaybillIds = GetCorrectedWaybillIds(startDate, endDate);

            if (corWaybillIds.IsNullOrEmpty())
            {
                return null;
            }

            var waybillList = new List<WAYBILL>();

            foreach (var corId in corWaybillIds)
            {
                var adjWaybillsXml = _Service.get_adjusted_waybills(su, sp, corId);
                var adjWaybills = SerializationHelper.Deserialize<ADJUSTED_WAYBILLS>(adjWaybillsXml);

                foreach (var adjWaybill in adjWaybills.Items.OrderBy(m => DateTime.Parse(m.DT)))
                {
                    var waybillXml = _Service.get_adjusted_waybill(su, sp, int.Parse(adjWaybill.ID));

                    WAYBILL waybill = SerializationHelper.Deserialize<WAYBILL>(waybillXml);

                    waybill.ID = adjWaybill.ID;
                    waybill.CORRECTION_DATE = adjWaybill.DT;

                    waybillList.Add(waybill);
                }
            }

            return waybillList.OrderBy(x => x.CORRECTION_DATE).ToArray();
        }

        public ADJUSTED_WAYBILL GetLastCorrectedWaybill(int waybillID)
        {
            var adjWaybillsXml = _Service.get_adjusted_waybills(su, sp, waybillID);
            var adjWaybills = SerializationHelper.Deserialize<ADJUSTED_WAYBILLS>(adjWaybillsXml);

            return adjWaybills.Items.OrderByDescending(m => DateTime.Parse(m.DT)).FirstOrDefault();
        }

        public string GetNameByUID(string uID)
        {
            return _Service.get_name_from_tin(su, sp, uID);
        }

        public CAR_NUMBERSCAR_NUMBER[] GetCarNumbers()
        {
            CAR_NUMBERS car_number = new CAR_NUMBERS();
            XElement carNumberXml = SerializationHelper.Serialize(car_number);
            _Service.get_car_numbers(su, sp, out carNumberXml);
            car_number = SerializationHelper.Deserialize<CAR_NUMBERS>(carNumberXml);
            if (car_number != null)
                return car_number.Items;
            return null;
        }

        public int SaveCarNumber(string carNumber)
        {
            return _Service.save_car_numbers(su, sp, carNumber);
        }

        public int DeleteCarNumber(string carNumberID)
        {
            return _Service.delete_car_numbers(su, sp, carNumberID);
        }

        #endregion

        #region Private Methods

        private XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;
                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));

        }

        private XElement GetFromStaticXml(string sellerUnID)
        {
            #region string s

            string s = @"<WAYBILL>
                      <SUB_WAYBILLS></SUB_WAYBILLS>
                      <GOODS_LIST>
                        <GOODS>
                          <ID>0</ID>
                          <W_NAME>წამალი #001</W_NAME>
                          <UNIT_ID>1</UNIT_ID>
                          <UNIT_TXT>ცალი</UNIT_TXT>
                          <QUANTITY>5</QUANTITY>
                          <PRICE>4</PRICE>
                          <STATUS>1</STATUS>
                          <AMOUNT>20</AMOUNT>
                          <BAR_CODE>psp101</BAR_CODE>
                          <A_ID>0</A_ID>
                          <VAT_TYPE></VAT_TYPE>
                        </GOODS>
                        <GOODS>
                          <ID>0</ID>
                          <W_NAME>წამალი #002</W_NAME>
                          <UNIT_ID>1</UNIT_ID>
                          <UNIT_TXT>ცალი</UNIT_TXT>
                          <QUANTITY>5</QUANTITY>
                          <PRICE>3</PRICE>
                          <STATUS>1</STATUS>
                          <AMOUNT>15</AMOUNT>
                          <BAR_CODE>psp102</BAR_CODE>
                          <A_ID>0</A_ID>
                          <VAT_TYPE></VAT_TYPE>
                        </GOODS>
                      </GOODS_LIST>
                      <ID>0</ID>
                      <TYPE>2</TYPE>
                      <CREATE_DATE>2012-03-07T10:11:47</CREATE_DATE>
                      <BUYER_TIN>12345678910</BUYER_TIN>
                      <CHEK_BUYER_TIN>0</CHEK_BUYER_TIN>
                      <BUYER_NAME>სატესტო სატესტო</BUYER_NAME>
                      <START_ADDRESS>სატესტო მისამართი</START_ADDRESS>
                      <END_ADDRESS>სატესტო დასრულების მისამართი</END_ADDRESS>
                      <DRIVER_TIN>1</DRIVER_TIN>
                      <CHEK_DRIVER_TIN></CHEK_DRIVER_TIN>
                      <DRIVER_NAME>მძღოლის სახელია</DRIVER_NAME>
                      <TRANSPORT_COAST>0</TRANSPORT_COAST>
                      <RECEPTION_INFO></RECEPTION_INFO>
                      <RECEIVER_INFO></RECEIVER_INFO>
                      <DELIVERY_DATE></DELIVERY_DATE>
                      <STATUS>0</STATUS>
                      <SELER_UN_ID>" + sellerUnID + @"</SELER_UN_ID>
                      <ACTIVATE_DATE></ACTIVATE_DATE>
                      <PAR_ID></PAR_ID>
                      <FULL_AMOUNT>35</FULL_AMOUNT>
                      <CAR_NUMBER></CAR_NUMBER>
                      <WAYBILL_NUMBER></WAYBILL_NUMBER>
                      <CLOSE_DATE></CLOSE_DATE>
                      <S_USER_ID>20350</S_USER_ID>
                      <BEGIN_DATE>2012-03-15T12:15:21</BEGIN_DATE>
                      <TRAN_COST_PAYER>1</TRAN_COST_PAYER>
                      <TRANS_ID>1</TRANS_ID>
                      <TRANS_TXT>საცალფეხო ბილიკით :)</TRANS_TXT>
                      <COMMENT></COMMENT>
                    </WAYBILL>";

            #endregion

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(s);
            return xDoc.ToXDocument().Root;
        }

        private void GenerateUploadError(List<object> errorList, string waybillID, string itemName, string errorStatusID, int type)
        {

            string status = "";
            if (errorStatusID == "0")
            {
                status = "ზედნადები ნომრით " + waybillID + " აიტვირთა.";
            }
            else
            {
                XElement errorXml = _Service.get_error_codes(su, sp);
                ERROR_CODES data = SerializationHelper.Deserialize<ERROR_CODES>(errorXml);

                string error = (from er in data.Items
                                where er.ID == errorStatusID
                                select er.TEXT).FirstOrDefault();
                if (type == 1 || type == 3)
                {
                    status = "ზედნადები ნომრით " + waybillID + " ვერ აიტვირთა. ზედნადებზე " + error;
                }
                else
                {
                    status = "ზედნადები ნომრით " + waybillID + " ვერ აიტვირთა. ზედნადების ნივთზე '" + itemName + "' " + error;
                }
            }
            errorList.Add(new { Status = status });
        }

        private List<int> GetCorrectedWaybillIds(DateTime startDate, DateTime endDate)
        {
            XElement waybillsXml;

            var any = _Service.get_c_waybill(su, sp, startDate, endDate, out waybillsXml);

            CheckResult(waybillsXml);

            if (waybillsXml == null)
            {
                return null;
            }

            var correctedWaybills = SerializationHelper.Deserialize<CORRECTED_WAYBILLS>(waybillsXml);

            if (correctedWaybills.Items == null || correctedWaybills.Items.Length == 0)
            {
                return null;
            }

            return correctedWaybills.Items.Select(x => int.Parse(x.WAYBILL_ID)).Distinct().ToList();
        }

        private List<int> GetReceivedWaybillIds(
            string waybillNumber = null,
            string statuses = null,
            string itypes = null,
            DateTime? createDateStart = null,
            DateTime? createDateEnd = null,
            DateTime? beginDateStart = null,
            DateTime? beginDateEnd = null,
            DateTime? deliveryDateStart = null,
            DateTime? deliveryDateEnd = null,
            DateTime? closeDateStart = null,
            DateTime? closeDateEnd = null)
        {
            XElement waybillsXml = _Service.get_buyer_waybills_ex(
                su: su,
                sp: sp,
                itypes: itypes,
                seller_tin: null,
                statuses: statuses,
                car_number: null,
                begin_date_s: beginDateStart,
                begin_date_e: beginDateEnd,
                create_date_s: createDateStart,
                create_date_e: createDateEnd,
                driver_tin: null,
                delivery_date_s: deliveryDateStart,
                delivery_date_e: deliveryDateEnd,
                full_amount: null,
                waybill_number: waybillNumber,
                close_date_s: closeDateStart,
                close_date_e: closeDateEnd,
                s_user_ids: null,
                comment: null,
                is_confirmed: null);

            CheckResult(waybillsXml);

            var waybills = SerializationHelper.Deserialize<WAYBILL_LIST>(waybillsXml);

            if (waybills.Items.IsNullOrEmpty())
            {
                return null;
            }

            return waybills.Items?.Select(x => int.Parse(x.ID)).Distinct().ToList();
        }


        private void CheckResult(XElement xml)
        {
            if (xml == null)
            {
                throw new ArgumentNullException(nameof(xml));
            }

            if (xml.Name == nameof(RESULT))
            {
                var result = SerializationHelper.Deserialize<RESULT>(xml);

                throw new WaybillResultException(result.STATUS);
            }
        }

        #endregion

        #region User

        public ServiceUsersServiceUser[] GetUsersFromRS(string userName, string userPassrowrd)
        {
            XElement service_user = null;

            service_user = _Service.get_service_users(userName, userPassrowrd);

            ServiceUsers serviceUsers = SerializationHelper.Deserialize<ServiceUsers>(service_user);

            return serviceUsers.Items;
        }


        public bool CreateServiceUser(string user_name, string user_password, string ip, string name, string su, string sp)
        {
            return _Service.create_service_user(user_name, user_password, ip, name, su, sp);
        }


        public string GetMyIP()
        {
            return _Service.what_is_my_ip();
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            (_Service as IDisposable).Dispose();
        }

        #endregion
    }
}

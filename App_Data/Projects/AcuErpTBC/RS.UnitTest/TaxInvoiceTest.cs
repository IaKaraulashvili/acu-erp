﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PX.Data;
using PX.Objects.AP;
using PX.Objects;
using RS.Services.Domain;

namespace RS.UnitTest
{
    [TestClass]
    public class TaxInvoiceTest
    {
        //private string account = "tbilisi";
        private string licence = "123456";
        private string userName = "sopo3:206322102";
        //private string userName = "tbilisi";
        private string password = "123456";
        private string url = @"https://www.revenue.mof.ge/ntosservice/ntosservice.asmx";
        private int? timeout = 30;

        [TestMethod]
        public void check()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                var isChecked = context.check();
                Assert.IsTrue(isChecked, "Check Failed");
            }
        }

        [TestMethod]
        public void acsept_invoice_status()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var un_id = context.get_un_id_from_user_id();
                bool b = context.accept_invoice_status(117196698, 2);
            }
        }


        [TestMethod]
        public void get_un_id_from_user_id()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var un_id = context.get_un_id_from_user_id();
                Assert.IsNotNull(un_id, "Does not return result");
                Assert.AreNotEqual(0, un_id, "Does not return result");
            }
        }

        [TestMethod]
        public void get_ser_users_notes()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                string note = string.Empty;
                var notes = context.get_ser_users_notes(context.get_tin_from_un_id(context.get_un_id_from_user_id(), out note));
                Assert.AreNotEqual(0, notes.Count, "Does not return result");
            }
        }

        [TestMethod]
        public void get_invoice_requests()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var invoice_requests = context.get_invoice_requests(context.get_un_id_from_tin("206322102"));
                foreach (var ir in invoice_requests)
                {
                    if (ir.REG_DT >= new DateTime(2015, 3, 25, 16, 12, 0)
                        && ir.REG_DT < new DateTime(2015, 3, 25, 16, 13, 0))
                        timeout = 30;
                }
                Assert.AreNotEqual(0, invoice_requests.Count, "Does not return result");
            }
        }



        [TestMethod]
        public void get_requested_invoices()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var invoice_requests = context.get_requested_invoices(context.get_un_id_from_tin("206322102"));
                foreach (var ir in invoice_requests)
                {
                    if (ir.REG_DT >= new DateTime(2014, 3, 16, 19, 0, 0)
                        && ir.REG_DT < new DateTime(2014, 3, 16, 19, 1, 0))
                        timeout = 30;
                }
                Assert.AreNotEqual(0, invoice_requests.Count, "Does not return result");
            }
        }



        [TestMethod]
        public void get_invoice_request()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                int bayer_un_id;
                int seller_un_id;
                string overhead_no;
                DateTime dt;
                string notes;
                var complete_successfully = context.get_invoice_request(1699567, out bayer_un_id,
                        out seller_un_id, out overhead_no, out dt, out notes);
                Assert.IsTrue(complete_successfully, "Does not exists");
            }
        }

        [TestMethod]
        public void save_invoice_request()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var complete_successfully = context.save_invoice_request(1699567, context.get_un_id_from_tin("404923749"), context.get_un_id_from_user_id(), "0165001253;0163134721",
                    new DateTime(2015, 3, 1), "");
                Assert.IsTrue(complete_successfully, "Does not exists");
            }
        }

        [TestMethod]
        public void save_seller_invoice()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                string name = string.Empty;
                int invois_id = 0;
                int seller_un_id = context.get_un_id_from_user_id();
                int buyer_un_id = context.get_un_id_from_tin("12345678910");

                bool b= context.save_invoice(ref invois_id, DateTime.Now, seller_un_id, buyer_un_id, string.Empty, DateTime.Now, 0);
            }
        }



        [TestMethod]
        public void get_buyer_invoices_r()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                //
                //var result = context.get_buyer_invoices_r(context.get_un_id_from_user_id(), -3);
                var result = context.get_buyer_invoices_r(context.get_un_id_from_user_id(), -2);



                var current = result.Find(x => x.ID.Value == 117666516);

                var complete_successfully = result;
                Assert.AreEqual(complete_successfully.Count, 0, "Does not exists");
            }
        }


        [TestMethod]
        public void get_buyer_invoices()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var complete_successfully = context.get_buyer_invoices(context.get_un_id_from_user_id(),
                    DateTime.Now.AddHours(-2), DateTime.Now, DateTime.Now.AddHours(-2), DateTime.Now, string.Empty, string.Empty, null, null);
                Assert.AreEqual(complete_successfully.Count, 0, "Does not exists");
            }
        }

        [TestMethod]
        public void get_seller_invoices_r()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                var complete_successfully = context.get_seller_invoices_r(context.get_un_id_from_user_id(), 1);
                Assert.AreEqual(complete_successfully.Count, 0, "Does not exists");
            }
        }


        [TestMethod]
        public void get_invoice()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                int invois_id = 110335174;
                string f_series;
                int f_number;
                DateTime operation_dt;
                DateTime reg_dt;
                int seller_un_id;
                int buyer_un_id;
                string overhead_no;
                DateTime overhead_dt;
                int status;
                string seq_num_s;
                string seq_num_b;
                int k_id;
                int r_un_id;
                int k_type;
                int b_s_user_id;
                int dec_status;

                var complete_successfully = context.get_invoice(invois_id, out f_series, out f_number, out operation_dt, out reg_dt, out seller_un_id, out buyer_un_id, out overhead_no, out overhead_dt, out status, out seq_num_s, out seq_num_b, out k_id, out r_un_id, out k_type, out b_s_user_id, out dec_status);
                Assert.IsTrue(complete_successfully, "Does not exists");
            }
        }



        [TestMethod]
        public void get_seller_invoices()
        {
            using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            {
                context.check();
                DateTime startDate = DateTime.Now.AddDays(-30);
                DateTime endDate = DateTime.Now.AddDays(-30);
                //DateTime opStartDate = DateTime.Now.AddDays(-30);
                //DateTime opEndDate = DateTime.Now.AddDays(-30);
                var complete_successfully = context.get_seller_invoices(startDate, endDate, startDate, endDate, "", "12345678910", "", "");
                Assert.AreEqual(complete_successfully.Count, 0, "Does not exists");
            }
        }


        [TestMethod]
        public void get_vendor_tins()
        {
            //var graph = PXGraph.CreateInstance<VendorMaint>();
            //var vendors = PXSelect<Vendor, Where<Vendor.taxRegistrationID, IsNotNull>>.Select(graph).FirstTableItems;
            //foreach (var vendor in vendors)
            //{

            //    using (var context = new RS.Services.Implementation.TaxInvoiceService(userName, password, url, timeout))
            //    {
            //        context.check();
            //        var invoice_requests = context.get_invoice_requests(context.get_un_id_from_tin(vendor.TaxRegistrationID));
            //        foreach (var ir in invoice_requests)
            //        {
            //            if (ir.REG_DT >= new DateTime(2015, 3, 25, 16, 12, 0)
            //                && ir.REG_DT < new DateTime(2015, 3, 25, 16, 13, 0))
            //                timeout = 30;
            //        }
            //        Assert.AreNotEqual(0, invoice_requests.Count, "Does not return result");
            //    }

            //}
        }

    }
}

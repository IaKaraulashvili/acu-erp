﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS.UnitTest
{
    [TestClass]
    public class WaybillTest
    {
        private string userName = "sopo3:206322102";
        private string password = "123456";
        private string url = @"https://services.rs.ge/WayBillService/WayBillService.asmx";
        private int? timeout = 30;

        [TestMethod]
        public void get_corrected_waybills()
        {
            using (var service = new RS.Services.Implementation.WaybillService(userName, password, url, timeout))
            {
                var a = service.GetCorrectedWaybills(DateTime.Today, new DateTime(2017, 10, 11, 10, 55, 30));

                Assert.AreEqual(a.Length, 2);
            }
        }
    }
}

﻿using PX.Api;
using PX.Data;
using PX.DataSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomSYProvider.Services
{
    public static class WordPDFSYExportProcessHelper
    {
        public static void RunScenarion(PXGraph graph, string scenario, string noteId, string refNbr, string docType = null)
        {
            var parameters = new List<PX.Api.PXSYParameter>();

            //attach file to concrete document
            parameters.Add(new PXSYParameter(WordPDFSYProvider.CONCRETE_DOCUMENT_NOTEID_PARAM, noteId));

            //prepare only one document 
            parameters.Add(new PXSYParameter(WordPDFSYProvider.INCOMING_DOC_REF_NBR, refNbr));
            parameters.Add(new PXSYParameter(WordPDFSYProvider.INCOMING_DOC_TYPE, docType));


            //generate unique file name for concrete document (graph name + keys)
            var fileName = "WORD_" + graph.GetType().Name + "_" + refNbr;
            if (docType != null)
                fileName += "_" + docType;

            parameters.Add(new PXSYParameter(WordPDFSYProvider.CONCRETE_DOCUMENT_FILE_PARAM, fileName));

            PX.Api.SYExportProcess.RunScenario(scenario,
              SYMapping.RepeatingOption.Primary,
              true,
              true,
              parameters.ToArray());
        }
    }

}

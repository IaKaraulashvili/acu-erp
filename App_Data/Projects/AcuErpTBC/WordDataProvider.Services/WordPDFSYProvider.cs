﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PX.Api;
using PX.Data;
using PX.DataSync.Filter;
using Aspose.Words;
using Aspose.Words.MailMerging;
using System.Collections;
using CustomSYProvider.Services;
using PX.SM;

namespace PX.DataSync
{
    public class WordPDFSYProvider : IPXSYProvider
    {
        #region Consts
        //Name of provider specific parameters
        public const String FILE_PARAM = "File Name";
        public const String PROVIDER_NOTEID_PARAM = "ProviderNoteID";
        public const string TABLE_NAME_PARAM = "Table Name";
        public const string SAVE_FORMAT_PARAM = "Format";
        public const string DIRECT_DOWNLOAD_PARAM = "Direct Download";
        public const string INCOMING_DOC_TYPE = "DocType";
        public const string INCOMING_DOC_REF_NBR = "RefNbr";

        public const string CONCRETE_DOCUMENT_NOTEID_PARAM = "ConcreteDocumentNoteID";
        public const string CONCRETE_DOCUMENT_FILE_PARAM = "ConcreteDocumentFileName";

        //Save format settings
        protected static readonly string[] _saveFormats;
        protected static readonly string DEFAULT_SAVE_FORMAT = SaveFormat.Docx.ToString();

        protected static readonly string[] _booleanStrings;
        #endregion

        //Default extension for files created from provider
        public string DefaultFileExtension
        {
            get
            {
                return ".docx";
            }
        }
        //Provider description name
        public string ProviderName
        {
            get
            {
                return PX.Data.PXMessages.Localize("Word PDF Provider");
            }
        }



        static WordPDFSYProvider()
        {
            //Initializing aspose license inside static constructor
            AsposeLicense.Set();

            //Initializing save formats 
            _saveFormats = new string[] { SaveFormat.Docx.ToString(), SaveFormat.Pdf.ToString() };
            _booleanStrings = new string[] { "True", "False" };
        }

        #region Parameters
        //variable for storing parameters
        protected IEnumerable<PXSYParameter> _Parameters;

        //returning description for parameters that is used by provider
        public PXStringState[] GetParametersDefenition()
        {
            return FillParameters().ToArray();
        }
        //returning parameters values
        public virtual PXSYParameter[] GetParameters()
        {
            return _Parameters.ToArray();
        }
        //setting parameter values from outside of provider
        public virtual void SetParameters(PXSYParameter[] parameters)
        {
            _Parameters = parameters;
        }
        public List<PXStringState> FillParameters()
        {
            List<PXStringState> ret = new List<PXStringState>();

            //Parameter for file name
            PXStringState fileParam = CreateParameter(FILE_PARAM, Titles.FileName, InfoMessages.FileNameDefaultValue);
            ret.Add(fileParam);

            //Parameter for table name
            PXStringState tableNameParam = CreateParameter(TABLE_NAME_PARAM, "Table Name", null);
            ret.Add(tableNameParam);

            //Parameter for saveformats 
            PXStringState saveFormatParam = CreateParameter(SAVE_FORMAT_PARAM, "Format", DEFAULT_SAVE_FORMAT, null, _saveFormats, _saveFormats);
            ret.Add(saveFormatParam);

            //Parameter for directDownload 
            PXStringState directDownloadParam = CreateParameter(DIRECT_DOWNLOAD_PARAM, "Direct Download", "True", null, _booleanStrings, _booleanStrings);
            ret.Add(directDownloadParam);

            return ret;
        }

        public string GetDocType()
        {
            return  GetParameter(INCOMING_DOC_TYPE, checkEmpty : false, checkExistanse : false);
        }

        public string GetRefNbr()
        {
            return  GetParameter(INCOMING_DOC_REF_NBR, checkEmpty: false, checkExistanse: false);
        }

        #endregion

        #region Schema
        //Returning Provider Objects
        public virtual String[] GetSchemaObjects()
        {
            //Reading file from database before processing 
            InitialiseFile(GetParameter(FILE_PARAM), false);

            List<String> ret = new List<String>();
            //For doc file we don't have any objects, so we will use file name
            ret.Add(_file.Name);
            return ret.ToArray();
        }
        //Returning Provider Fields for specific Provider Objectord
        public virtual PXFieldState[] GetSchemaFields(String objectName)
        {
            //Reading file from database before processing 
            InitialiseFile(GetParameter(FILE_PARAM), false);

            List<PXFieldState> ret = new List<PXFieldState>();

            //Opening DOC Reader with all parameters
            Document doc = GetDocument();
            var fieldNames = doc.MailMerge.GetFieldNames();

            int index = 0;
            foreach (var field in fieldNames)
            {
                if (!string.IsNullOrEmpty(field))
                {
                    PXFieldState fs = CreateFieldState(new SchemaFieldInfo(index, field));
                    ret.Add(fs);
                    index++;
                }
            }

            return ret.ToArray();
        }
        #endregion

        #region Import
        public virtual PXSYTable Import(String objectName, String[] fieldNames, PXSYFilterRow[] filters, String lastTimeStamp, PXSYSyncTypes syncType)
        {
            throw new NotImplementedException("Import");
        }
        #endregion

        #region Export
        public virtual void Export(String objectName, PXSYTable table, Boolean breakOnError, Action<SyProviderRowResult> callback)
        {

          
            if (table.Count == 0)
            {
                InvokeCallback(table, callback);
                return;
            }

            

            //Reading file from database before processing, if file does not exists, we have to crete new one.
            InitialiseFile(GetParameter(FILE_PARAM), true);

            string tableName = GetParameter(TABLE_NAME_PARAM);

            Document doc = GetDocument();
            DocumentBuilder builder = new DocumentBuilder(doc);

            // key columns
            //int rowIndex = 0;
            //List<string> keys = table.Rows[0].Keys.Select(m => m.Key).ToList();

            //Func<PXSYRow, bool> currentRowEqualToPrev = delegate (PXSYRow row)
            //{               
            //    foreach (var key in keys)
            //    {
            //        if (row[key] != table.GetValue(table.Columns.IndexOf(key), rowIndex))
            //            return false;
            //    }
            //    return true;
            //};

            //Func<string, bool> isParentColumn = delegate (string column)
            //{
            //    return table.Columns.ToList().TakeWhile(m => m == m.StartsWith("TableStart")).Last();
            //    foreach (var col in table.Columns)
            //    {
            //        if (col.StartsWith("TableStart:" + tableName, StringComparison.InvariantCultureIgnoreCase))
            //        {
            //            write = true;
            //            isParentColumn = true;
            //        }
            //        if (col.StartsWith("TableEnd:" + tableName, StringComparison.InvariantCultureIgnoreCase))
            //        {
            //            break;
            //        }

            //        if (write && !isParentColumn)
            //        {
            //            columns.Add(col);
            //        }
            //    }
            //    return true;
            //};

            //for (rowIndex = 0; rowIndex < table.Count; rowIndex++)
            //{
            //    var row = table[rowIndex];
            //    while (currentRowEqualToPrev(row))
            //    {                    
            //        rowIndex++;
            //    }
            //}

            var dataSource = new MailMergeDataSource(tableName, table);
               
            doc.MailMerge.CleanupOptions = MailMergeCleanupOptions.RemoveUnusedRegions;
           // doc.MailMerge.CleanupOptions |= MailMergeCleanupOptions.RemoveEmptyParagraphs;
            doc.MailMerge.CleanupOptions |= MailMergeCleanupOptions.RemoveUnusedFields;

            doc.MailMerge.FieldMergingCallback = new HandleMergeField();

            doc.MailMerge.ExecuteWithRegions(dataSource);

            // Create a new memory stream.
            byte[] docBytes;
            using (MemoryStream outStream = new MemoryStream())
            {
                //Get format(word or pdf) 
                SaveFormat format;
                Enum.TryParse(GetParameter(SAVE_FORMAT_PARAM), out format);
                // Save the document to stream
                doc.Save(outStream, format);
                // Convert the document to byte form.
                docBytes = outStream.ToArray();
            }

            PX.SM.FileInfo fileToDownload;
            if (GetParameter(CONCRETE_DOCUMENT_NOTEID_PARAM, checkEmpty: false, checkExistanse: false) != null)
            {
                //attach file to document      
               fileToDownload = AttachFileToConcreteDocument(docBytes);
            }
            else
            {
                //attach file to provider
                fileToDownload =  SetFile(docBytes);
            }

            InvokeCallback(table, callback);

            //direct download
            if (Convert.ToBoolean(GetParameter(DIRECT_DOWNLOAD_PARAM)))
                throw new PXRedirectToFileException(fileToDownload, true);
            
        }
        #endregion

        private PXSYTable GetPXSYTableBySpecificDocument(PXSYTable table)
        {
            var documentTable = new PXSYTable(table.Columns);
            var keys = table.Rows[0].Keys.Select(m => m.Key).ToList();

            int rowIndex = 0;
            Func<bool> equalTo = delegate ()
            {
                foreach (var key in keys)
                {
                    if (table.GetValue(table.Columns.IndexOf(key), rowIndex) != GetParameter(key))
                        return false;
                }
                return true;
            };

            foreach (var row in table)
            {
                if (equalTo())
                {
                    PXSYRow rowToAdd = documentTable.CreateRow();
                    foreach (var col in table.Columns)
                    {
                        int columnIndex = table.Columns.IndexOf(col);
                        PXSYItem item = new PXSYItem(table.GetValue(columnIndex, rowIndex));
                        rowToAdd.SetItem(col, item);
                    }
                    rowToAdd.Keys = row.Keys;
                    documentTable.Add(rowToAdd);
                }
                rowIndex++;
            }

            return documentTable;
        }


        #region Auxiliary File
        protected PX.SM.FileInfo _file;

        protected virtual void InitialiseFile(String fileName, Boolean create)
        {
            if (_file == null)
            {
                //serching for file in the database.
                PX.SM.FileInfo file = GetFileTemplate();

                //If file does not exist than trow and error.
                if ((file == null || file.BinData == null) && !create) throw new PXException(Messages.FileNotFound, fileName);

                //creating new file if needed
                if (file == null)
                {
                    //Searching for specific provider in the database
                    SYProvider prov = GetProvider();
                    //Generation name
                    String filename = fileName ?? String.Concat(prov.Name, DefaultFileExtension);
                    //creating file
                    file = new PX.SM.FileInfo(filename, filename, null);
                }

                _file = file;
            }
        }

        private Document GetDocument()
        {
            using (Stream stream = new MemoryStream(_file.BinData))
            {
                return new Document(stream);
            }
        }

        protected virtual PX.SM.FileInfo GetFile(String fileName)
        {
            PX.SM.FileInfo file;

            //creating special graph for managing files
            PX.SM.UploadFileMaintenance upload = new PX.SM.UploadFileMaintenance();
            //searching for file
            file = upload.GetFile(fileName);

            return file;
        }

        protected virtual PX.SM.FileInfo GetFirstFile(String fileName)
        {
            PX.SM.FileInfo file;

            //creating special graph for managing files
            PX.SM.UploadFileMaintenance upload = new PX.SM.UploadFileMaintenance();
            //searching for file
            file = upload.GetFile(fileName, 1);

            return file;
        }

        protected virtual PX.SM.FileInfo GetFileTemplate()
        {
            PX.SM.FileInfo file;

            //creating special graph for managing files
            PX.SM.UploadFileMaintenance upload = new PX.SM.UploadFileMaintenance();

            //always get docx  or doc and version one of file

            var ext = Path.GetExtension(GetParameter(FILE_PARAM));
            var fileName = GetParameter(FILE_PARAM).Replace(ext, ".docx");
            file = upload.GetFile(fileName, 1);
            
            //if docx not found get doc 
            if(file == null)
            {
                fileName = fileName.Replace(".docx", ".doc");
                file = upload.GetFile(fileName, 1);
            }
                      
            return file;
        }

        protected virtual PX.SM.FileInfo SetFile(Byte[] bytes)
        {
            //creating special graph for managing files
            PX.SM.UploadFileMaintenance upload = new PX.SM.UploadFileMaintenance();

            var ext = Path.GetExtension(GetParameter(FILE_PARAM));
            var saveToExt = "." + GetParameter(SAVE_FORMAT_PARAM).ToLower();
            var fileName = GetParameter(FILE_PARAM).Replace(ext, saveToExt);

            var file = upload.GetFile(fileName);

            bool exist = file != null;
            if (!exist)
            {
                file = new PX.SM.FileInfo(fileName, fileName, null);
                file.UID = Guid.NewGuid();
            }

            //initialisubg files data.
            file.Comment = String.Format(Messages.ExportedComment, this.ProviderName);
            file.BinData = bytes;

            //saiving file to the database
            upload.SaveFile(file, PX.SM.FileExistsAction.CreateVersion);

            //attaching new file to current provider.
            if (!exist) AttachFile((Guid)file.UID);

            return file;
        }


        protected virtual PX.SM.FileInfo AttachFileToConcreteDocument(Byte[] bytes)
        {
            //creating special graph for managing files 
            PX.SM.UploadFileMaintenance upload = new PX.SM.UploadFileMaintenance();

            string fileName = GetParameter(CONCRETE_DOCUMENT_FILE_PARAM) + "."+ GetParameter(SAVE_FORMAT_PARAM).ToLower();
            var file = upload.GetFile(fileName);

            bool exist = file != null;
            if (!exist)
            {
                file = new PX.SM.FileInfo(fileName, fileName, null);
                file.UID = Guid.NewGuid();
            }
            file.BinData = bytes;

        
            //saiving file to the database
            upload.SaveFile(file, PX.SM.FileExistsAction.CreateVersion);

            //inserting reference between file and document to the database
            if (!exist)
            {
                Guid noteID = new Guid(GetParameter(CONCRETE_DOCUMENT_NOTEID_PARAM));
                PXDatabase.Insert(typeof(NoteDoc),
                new PXDataFieldAssign(typeof(NoteDoc.noteID).Name, PXDbType.UniqueIdentifier, noteID),
                new PXDataFieldAssign(typeof(NoteDoc.fileID).Name, PXDbType.UniqueIdentifier, file.UID),
                PXDataFieldAssign.OperationSwitchAllowed);
            }

            return file;
        }

        protected virtual void AttachFile(Guid file)
        {
            //searching for current provider.
            SYProvider prov = GetProvider();
            try
            {
                //inserting reference between file and provider to the database
                PXDatabase.Insert(typeof(NoteDoc),
                    new PXDataFieldAssign(typeof(NoteDoc.noteID).Name, PXDbType.UniqueIdentifier, prov.NoteID),
                    new PXDataFieldAssign(typeof(NoteDoc.fileID).Name, PXDbType.UniqueIdentifier, file),
                    PXDataFieldAssign.OperationSwitchAllowed
                    );
            }
            catch (PXDatabaseException ex)
            {
                if (ex.ErrorCode == PXDbExceptions.OperationSwitchRequired)
                {
                    //if table is shared we need to check different way
                    PXDatabase.Update<NoteDoc>(
                        new PXDataFieldAssign(typeof(NoteDoc.noteID).Name, PXDbType.BigInt, 8, prov.NoteID),
                        new PXDataFieldAssign(typeof(NoteDoc.fileID).Name, PXDbType.UniqueIdentifier, file),
                        new PXDataFieldRestrict(typeof(NoteDoc.noteID).Name, PXDbType.BigInt, 8, prov.NoteID),
                        new PXDataFieldRestrict(typeof(NoteDoc.fileID).Name, PXDbType.UniqueIdentifier, file)
                    );
                }
                else
                {
                    throw;
                }
            }
        }


        protected virtual SYProvider GetProvider()
        {
            //getting provider note id from parameters
            var noteId = Guid.Parse(GetParameter(PROVIDER_NOTEID_PARAM, false));
            //Int32 noteid;
            //if (!Int32.TryParse(notestring, out noteid))
            //    throw new PXException(Messages.ProviderNoteIDNotFound);

            //searching fo providers by note id
            SYProviderMaint graph = new SYProviderMaint();
            SYProvider provider = PXSelect<SYProvider, Where<SYProvider.noteID, Equal<Required<SYMapping.noteID>>>>.Select(graph, noteId);
            if (provider == null) throw new PXException(Messages.ProviderNotFound, noteId);

            return provider;
        }
        #endregion

        #region Auxiliary Methods
        //Searching for specific parameter in parameters collection
        protected String GetParameter(String paremeter, Boolean checkEmpty = true, Boolean checkExistanse = true)
        {
            foreach (PXSYParameter pr in _Parameters)
            {
                if (String.Compare(pr.Name, paremeter, true) == 0)
                {
                    if (checkEmpty && String.IsNullOrEmpty(pr.Value)) throw new PXException(Messages.ParameterIsEmpty, paremeter);
                    return pr.Value;
                }
            }
            if (checkExistanse) throw new PXException(Messages.ParameterNotFound, paremeter);
            return null;
        }
        //Creating definition class for the known parameter
        protected PXStringState CreateParameter(String name, String displayName, String value = null, String mask = null, String[] alowedLabels = null, String[] alowedValues = null)
        {
            PXStringState param = (PXStringState)PXStringState.CreateInstance(
                value,
                null,
                false,
                name,
                null,
                1,
                String.IsNullOrEmpty(mask) ? null : mask,
                alowedValues == null ? null : alowedValues,
                alowedLabels == null ? null : alowedLabels,
                null,
                String.IsNullOrEmpty(value) ? null : value);
            if (!String.IsNullOrEmpty(displayName)) param.DisplayName = displayName;

            return param;
        }

        //create difinition class for provider field.
        protected PXFieldState CreateFieldState(SchemaFieldInfo fieled)
        {
            PXFieldState fieldState = PXStringState.CreateInstance(
            null,                                               //value
            fieled.DataType,                                    //dataType			
            false,                                              //isKey				
            true,                                               //nullable			
            null,                                               //required		
            null,                                               //precision
            fieled.Length,                                      //length
            null,                                               //defaultValue
            fieled.Name,                                        //fieldName
            fieled.Name,                                        //descriptionName
            fieled.Name,                                        //displayName
            null,                                               //error
            PXErrorLevel.Undefined,                             //errorLevel
            null,                                               //enabled
            null,                                               //visible
            null,                                               //readOnly	
            PXUIVisibility.Undefined,                           //visibility
            null,                                               //viewName
            null,                                               //fieldList
            null                                                //headerList
            );

            return fieldState;
        }

        //invoking callback for all rows in table.
        protected virtual void InvokeCallback(PXSYTable table, Action<SyProviderRowResult> callback)
        {
            for (int i = 0; i < table.Count; i++)
            {
                callback.Invoke(new SyProviderRowResult(i));
            }
        }
        #endregion
    }

    #region Writer
    //Special class that can write data in to DOC format.
    public class MailMergeDataSource : IMailMergeDataSource
    {
        private readonly string _tableName;
        private int _rowIndex;
        private readonly PXSYTable _table;
        private List<string> _keys;
        private RowKeyValues _prevRow;
        private bool _isEof
        {
            get { return (_rowIndex >= _table.Count); }
        }

        public MailMergeDataSource(string tableName, PXSYTable table)
        {
            _tableName = tableName;
            _table = table;
            _keys = _table.Rows.Count > 0 ? _table.Rows[0].Keys.Select(m => m.Key).ToList() : new List<string>();
            _rowIndex = -1;
        }

        public string TableName { get { return _tableName; } }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            var columns = new List<string>();
            bool write = false;
            bool isParentColumn = false;

            foreach (var col in _table.Columns)
            {
                isParentColumn = false;
                if (col.StartsWith("TableStart:" + tableName, StringComparison.InvariantCultureIgnoreCase))
                {
                    write = true;
                    isParentColumn = true;
                }
                if (col.StartsWith("TableEnd:" + tableName, StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                if (write && !isParentColumn)
                {
                    columns.Add(col);
                }
            }

            PXSYTable table = new PXSYTable(columns);
            int rowIndex = _rowIndex;

            while (CurrentRowIsSelfOrEqualToPrevRow(rowIndex))
            {
                bool rowCreated = false;
                PXSYRow row = null;
                foreach (var col in columns)
                {
                    int columnIndex = _table.Columns.IndexOf(col);
                    var value = _table.GetValue(columnIndex, rowIndex);
                    if (value == null) continue;

                    if (!rowCreated)
                    {
                        row = table.CreateRow();
                        rowCreated = true;
                    }

                    PXSYItem item = new PXSYItem(value);
                    row.SetItem(col, item);
                }

                if (row != null)
                    table.Add(row);

                rowIndex++;
            }

            if (table.Count == 0)
                return null;

            return new ChildDMailMergeDataSource(tableName, table);
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            //fieldValue = null;
            //if (fieldName == "LoanAmount") return false; //დროებით

            int columnIndex = _table.Columns.IndexOf(fieldName);
            fieldValue = _table.GetValue(columnIndex, _rowIndex);
            SetRow();

            return fieldValue != null;
        }

        public bool MoveNext()
        {
            if (!_isEof)
                _rowIndex++;

            while (CurrentRowEqualToPrevRow())
            {
                SetRow();
                _rowIndex++;

                if (_isEof)
                    return (!_isEof);
            }     
            return (!_isEof);
        }

        private bool CurrentRowEqualToPrevRow()
        {
            if (_prevRow == null || _prevRow.RowIndex == _rowIndex || _isEof)
                return false;

            foreach (var key in _keys)
            {
                if (_prevRow.dict[key] != _table.GetValue(_table.Columns.IndexOf(key), _rowIndex))
                    return false;
            }

            return true;
        }

        private bool CurrentRowIsSelfOrEqualToPrevRow(int rowIndex)
        {
            if (rowIndex >= _table.Count)
                return false;

            if (_prevRow == null || _prevRow.RowIndex == rowIndex)
                return true;

            foreach (var key in _keys)
            {
                if (_prevRow.dict[key] != _table.GetValue(_table.Columns.IndexOf(key), rowIndex))
                    return false;
            }

            return true;
        }
        private void SetRow()
        {
            if (_prevRow != null && _prevRow.RowIndex == _rowIndex)
                return;

            _prevRow = new RowKeyValues
            {
                RowIndex = _rowIndex
            };

            foreach (var key in _keys)
            {
                var value = _table.GetValue(_table.Columns.IndexOf(key), _rowIndex);
                if (_prevRow.dict.ContainsKey(key))
                {
                    _prevRow.dict[key] = value;
                }
                else
                {
                    _prevRow.dict.Add(key, value);
                }
            }
        }

  
    }

    public class HandleMergeField : IFieldMergingCallback
    {
        public void FieldMerging(FieldMergingArgs args)
        {
            
        }

        public void ImageFieldMerging(ImageFieldMergingArgs args)
        {
            // Do nothing
        }
    }

    public class ChildDMailMergeDataSource : IMailMergeDataSource
    {
        private readonly string _tableName;
        private readonly PXSYTable _table;
        private int _rowIndex = -1;
        private bool _isEof
        {
            get { return (_rowIndex >= _table.Count); }
        }

        public ChildDMailMergeDataSource(string tableName, PXSYTable table)
        {
            _tableName = tableName;
            _table = table;
        }

        public string TableName { get { return _tableName; } }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            int columnIndex = _table.Columns.IndexOf(fieldName);
            fieldValue = _table.GetValue(columnIndex, _rowIndex);

            return fieldValue != null;
        }

        public bool MoveNext()
        {
            if (!_isEof)
                _rowIndex++;

            return (!_isEof);
        }
    }
    public class RowKeyValues
    {
        public RowKeyValues()
        {
            dict = new Dictionary<string, string>();
        }
        public Dictionary<string, string> dict { get; set; }
        public int RowIndex { get; set; }
    }
    #endregion
}
<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AG2010AP.aspx.cs" Inherits="Page_AG2010AP" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="Banks" SuspendUnloading="False" TypeName="AG.Extensions.AP.AGBankMaint" >
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="ViewCountry" Visible="false" DependOnGrid="grid"></px:PXDSCallbackCommand>
        </CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
    <px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100"
		AllowPaging="True" AllowSearch="True" AdjustPageSize="Auto" DataSourceID="ds" SkinID="Primary" TabIndex="1500" TemporaryFilterCaption="Filter Applied">
<EmptyMsg ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here." NamedComboMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." NamedComboAddMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." FilteredMessage="No records found.
Try to change filter to see records here." FilteredAddMessage="No records found.
Try to change filter to see records here." NamedFilteredMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." NamedFilteredAddMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." AnonFilteredMessage="No records found.
Try to change filter to see records here." AnonFilteredAddMessage="No records found.
Try to change filter to see records here."></EmptyMsg>
		<Levels>
			<px:PXGridLevel DataKeyNames="BankID,SwiftCode" DataMember="Banks">
			    <RowTemplate>
                    <px:PXMaskEdit ID="edSwiftCode" runat="server" AlreadyLocalized="False" DataField="SwiftCode" DefaultLocale="">
                    </px:PXMaskEdit>
                    <px:PXTextEdit ID="edBankName" runat="server" AlreadyLocalized="False" DataField="BankName" DefaultLocale="">
                    </px:PXTextEdit>
                    <px:PXSelector ID="edCountryID" runat="server" DataField="CountryID">
                    </px:PXSelector>
                    <px:PXTextEdit ID="edAddress" runat="server" AlreadyLocalized="False" DataField="Address" DefaultLocale="">
                    </px:PXTextEdit>
                </RowTemplate>
			    <Columns>
                    <px:PXGridColumn DataField="SwiftCode">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="BankName" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="CountryID" LinkCommand="ViewCountry">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="Address" Width="200px">
                    </px:PXGridColumn>
                </Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXGrid>
</asp:Content>

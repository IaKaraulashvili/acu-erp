<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="EA302000.aspx.cs" Inherits="Page_EA302000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="LCARegistration" SuspendUnloading="False" TypeName="AG.EA.LCARegistrationEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="viewAsset" Visible="False"  DependOnGrid="PXGrid1"/>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>

<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="LCARegistration" TabIndex="1400">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" StartColumn="True" />
            <px:PXSelector ID="edLCARegistrationCD" runat="server" DataField="LCARegistrationCD">
            </px:PXSelector>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status">
            </px:PXDropDown>
            <px:PXCheckBox ID="edHold" runat="server" AlreadyLocalized="False" CommitChanges="True" DataField="Hold" Text="Hold">
            </px:PXCheckBox>
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM">
            </px:PXLayoutRule>
            <px:PXSelector ID="edINRegisterRefNbr" runat="server" DataField="INRegisterRefNbr" AllowEdit="True" Enabled="False">
            </px:PXSelector>
            <px:PXSelector ID="edWaybillRefNbr" runat="server" AllowEdit="True" Enabled="False" DataField="WaybillRefNbr" edit="1">
            </px:PXSelector>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Details">
                <Template>
                    <px:PXGrid ID="PXGrid1" runat="server" DataSourceID="ds" TabIndex="1200" Width="100%" SkinID="Details" SyncPosition="true" KeepPosition="true">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="LCARegistrationDetailID" DataMember="LCARegistrationDetails">
                                <RowTemplate>
                                    <px:PXSegmentMask ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="true">
                                    </px:PXSegmentMask>
                                    <px:PXTextEdit ID="edLotSerialNbr" runat="server" AlreadyLocalized="False" DataField="LotSerialNbr" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="Description" DefaultLocale="" AlreadyLocalized="False" ID="edDescription"></px:PXTextEdit>
                                    <px:PXSelector ID="edClassID" runat="server" DataField="ClassID">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edUOM" runat="server" DataField="UOM">
                                    </px:PXSelector>
                                    <px:PXNumberEdit ID="edQty" runat="server" AlreadyLocalized="False" DataField="Qty" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXTextEdit ID="edAssetCD" runat="server" AlreadyLocalized="False" DataField="AssetCD" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXSegmentMask runat="server" DataField="BranchID" ID="edBranchID" CommitChanges="true"></px:PXSegmentMask>
                                    <px:PXSelector runat="server" DataField="BuildingID" ID="edBuildingID" CommitChanges="true"></px:PXSelector>
                                    <px:PXTextEdit ID="edFloor" runat="server" AlreadyLocalized="False" DataField="Floor" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edRoom" runat="server" AlreadyLocalized="False" DataField="Room" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXSelector runat="server" DataField="EmployeeID" ID="edEmployeeID" CommitChanges="true"></px:PXSelector>
                                    <px:PXSelector runat="server" DataField="Department" ID="edDepartment"></px:PXSelector>
                                    <px:PXSegmentMask ID="edSiteID" runat="server" DataField="SiteID">
                                    </px:PXSegmentMask>
                                    <px:PXTextEdit ID="edReason" runat="server" AlreadyLocalized="False" DataField="Reason" DefaultLocale="">
                                    </px:PXTextEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="true">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="LotSerialNbr" Width="200px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Description" Width="200px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Cost" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ClassID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="UOM">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Qty" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="AssetCD" LinkCommand="viewAsset">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="BranchID" Width="120px" CommitChanges="true">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="BuildingID" CommitChanges="true">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Floor">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Room">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EmployeeID" Width="120px" CommitChanges="true">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Department">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="SiteID" Width="120px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Reason" Width="200px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

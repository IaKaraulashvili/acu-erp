<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EA501000.aspx.cs" Inherits="Page_EA501000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="AG.EA.AssetProcess" PrimaryView="Assets">
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
    <px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100"
        AllowPaging="True" AllowSearch="True" AdjustPageSize="Auto" DataSourceID="ds" SkinID="PrimaryInquire" TabIndex="2500"
        TemporaryFilterCaption="Filter Applied" FastFilterFields="AssetCD,Description,EAAssetLocationHistory__BuildingID,EAAssetLocationHistory__Room,EAAssetLocationHistory__BranchID,UOM,EAAssetLocationHistory__EmployeeID_EPEmployee_acctName,EAAssetLocationHistory__SiteID,EAAssetLocationHistory__Department">
<EmptyMsg ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here." NamedComboMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." NamedComboAddMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." FilteredMessage="No records found.
Try to change filter to see records here." FilteredAddMessage="No records found.
Try to change filter to see records here." NamedFilteredMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." NamedFilteredAddMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." AnonFilteredMessage="No records found.
Try to change filter to see records here." AnonFilteredAddMessage="No records found.
Try to change filter to see records here."></EmptyMsg>
        <Levels>
            <px:PXGridLevel DataKeyNames="AssetCD" DataMember="Assets">
                <RowTemplate>
                    <px:PXCheckBox ID="edSelected" runat="server" AlreadyLocalized="False" DataField="Selected" Text="Selected" >
                    </px:PXCheckBox>
                    <px:PXSelector ID="edAssetCD" runat="server" DataField="AssetCD" AllowEdit="True" edit="1">
                    </px:PXSelector>
                    <px:PXDropDown ID="edStatus" runat="server" DataField="Status">
                    </px:PXDropDown>
                    <px:PXTextEdit ID="edDescription" runat="server" AlreadyLocalized="False" DataField="Description" DefaultLocale="">
                    </px:PXTextEdit>
                    <px:PXSegmentMask ID="edInventoryID" runat="server" DataField="InventoryID">
                    </px:PXSegmentMask>
                    <px:PXTextEdit ID="edLotSerialNbr" runat="server" AlreadyLocalized="False" DataField="LotSerialNbr">
                    </px:PXTextEdit>
                    <px:PXNumberEdit ID="edCost" runat="server" AlreadyLocalized="False" DataField="Cost" DefaultLocale="">
                    </px:PXNumberEdit>
                    <px:PXSelector ID="edUOM" runat="server" DataField="UOM">
                    </px:PXSelector>
                    <px:PXSegmentMask ID="edEAAssetLocationHistory__BranchID" runat="server" DataField="EAAssetLocationHistory__BranchID">
                    </px:PXSegmentMask>
                    <px:PXSelector ID="edEAAssetLocationHistory__BuildingID" runat="server" DataField="EAAssetLocationHistory__BuildingID">
                    </px:PXSelector>
                    <px:PXTextEdit ID="edEAAssetLocationHistory__Floor" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__Floor">
                    </px:PXTextEdit>
                    <px:PXTextEdit ID="edEAAssetLocationHistory__Room" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__Room">
                    </px:PXTextEdit>
                    <px:PXTextEdit ID="edEAAssetLocationHistory__EmployeeID_EPEmployee_acctName" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__EmployeeID_EPEmployee_acctName">
                    </px:PXTextEdit>
                    <px:PXTextEdit ID="edEAAssetLocationHistory__Department_description" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__Department_description">
                    </px:PXTextEdit>
                    <px:PXSegmentMask ID="edEAAssetLocationHistory__SiteID" runat="server" DataField="EAAssetLocationHistory__SiteID">
                    </px:PXSegmentMask>
                    <px:PXDropDown ID="edCondition" runat="server" DataField="Condition">
                    </px:PXDropDown>
                    <px:PXDateTimeEdit ID="edIssueDate" runat="server" AlreadyLocalized="False" DataField="IssueDate">
                    </px:PXDateTimeEdit>
                    <px:PXDateTimeEdit ID="edLastTransportationDate" runat="server" AlreadyLocalized="False" DataField="LastTransportationDate" DefaultLocale="">
                    </px:PXDateTimeEdit>
                    <px:PXTextEdit ID="edEAAssetLocationHistory__LastModifiedByID_Modifier_Username" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__LastModifiedByID_Modifier_Username">
                    </px:PXTextEdit>
                </RowTemplate>
                <Columns>
                    <px:PXGridColumn DataField="Selected" TextAlign="Center" Type="CheckBox" Width="60px" AllowCheckAll="True">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="AssetCD" Width="100px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="Status">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="Description" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="InventoryID">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="LotSerialNbr" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="Cost" TextAlign="Right" Width="100px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="UOM">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__BranchID">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__BuildingID" Width="120px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__Floor">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__Room">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__EmployeeID_EPEmployee_acctName" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__Department_description" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__SiteID" Width="120px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="Condition" Width="100px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="IssueDate" Width="90px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="LastTransportationDate" Width="90px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="EAAssetLocationHistory__LastModifiedByID_Modifier_Username">
                    </px:PXGridColumn>
                </Columns>
            </px:PXGridLevel>
        </Levels>
        <AutoSize Container="Window" Enabled="True" MinHeight="200" />
    </px:PXGrid>
</asp:Content>

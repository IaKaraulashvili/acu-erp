﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="TB401000.aspx.cs" Inherits="Page_TB401000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" Width="100%" runat="server" Visible="True" TypeName="AcuErpTBC.TB.TBVendorByItemClassInq" PrimaryView="ItemClassFilter" PageLoadBehavior="GoFirstRecord">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="GoToNodeSelectedInTree" Visible="false" CommitChanges="True" />
            <px:PXDSCallbackCommand Visible="false" Name="ViewItem" DependOnGrid="gridVendors" />
            <px:PXDSCallbackCommand Visible="false" Name="ViewClass" DependOnGrid="gridVendors" />
        </CallbackCommands>
        <DataTrees>
            <px:PXTreeDataMember TreeView="ItemClasses" TreeKeys="ItemClassID" />
        </DataTrees>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="syncForm" runat="server" DataSourceID="ds" DataMember="TreeViewAndPrimaryViewSynchronizationHelper" Height="0" Width="100%" RenderStyle="Simple">
        <Template>
            <px:PXSegmentMask ID="edItemClassID" runat="server" DataField="ItemClassCD" Visible="False" />
        </Template>
    </px:PXFormView>
    <px:PXSplitContainer runat="server" ID="sp1" SplitterPosition="300">
        <AutoSize Enabled="true" Container="Window" />
        <Template1>
            <px:PXFormView ID="treeFilter" runat="server" DataSourceID="ds" DataMember="ItemClassFilter" Caption="Category Info" Width="100%">
                <Template>
                    <px:PXLayoutRule runat="server" ControlSize="M" LabelsWidth="XS" StartColumn="True" />
                    <px:PXSegmentMask ID="edItemClassID" runat="server" DataField="ItemClassCD" CommitChanges="True" />
                </Template>
            </px:PXFormView>
            <px:PXTreeView ID="tree" runat="server" DataSourceID="ds" DataMember="ItemClasses" Height="180px" Caption="Item Class Tree"
                ShowRootNode="False" AllowCollapse="False" AutoRepaint="True"
                SyncPosition="True" SyncPositionWithGraph="True" PreserveExpanded="True" ExpandDepth="0" PopulateOnDemand="True" SelectFirstNode="True">
                <AutoCallBack Target="ds" Command="GoToNodeSelectedInTree" />
                <DataBindings>
                    <px:PXTreeItemBinding DataMember="ItemClasses" TextField="SegmentedClassCD" ValueField="ItemClassID" DescriptionField="Descr" />
                </DataBindings>
                <AutoSize Enabled="True" />
            </px:PXTreeView>
        </Template1>

        <Template2>
            <px:PXFormView ID="gridFilter" runat="server" DataSourceID="ds" DataMember="VendorFilter" Width="100%">
                <Template>
                    <px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="S" StartColumn="True" />
                    <px:PXGroupBox runat="server" DataField="ShowItems" RenderStyle="RoundBorder" ID="gbShowItemsMode" CommitChanges="True" Caption="Show Vendors">
                        <ContentLayout Layout="Stack" Orientation="Vertical" />
                        <Template>
                            <px:PXRadioButton runat="server" Value="C" ID="gbShowItemsMode_opA" GroupName="gbShowItemsMode" />
                            <px:PXRadioButton runat="server" Value="A" ID="gbShowItemsMode_opC" GroupName="gbShowItemsMode" />
                        </Template>
                    </px:PXGroupBox>
                </Template>
            </px:PXFormView>
            <px:PXGrid ID="gridVendors" runat="server" DataSourceID="ds" Caption="Vendors" Width="100%"
                SkinID="Inquire" ActionsPosition="Top" SyncPosition="True" AdjustPageSize="Auto" NoteIndicator="False" FilesIndicator="False" TabIndex="9500" TemporaryFilterCaption="Filter Applied">
                <Levels>
                    <px:PXGridLevel DataMember="Vendors">
                        <RowTemplate>
                            <px:PXSegmentMask runat="server" DataField="AcctCD" ID="edAcctCD" AllowEdit="true"></px:PXSegmentMask>
                            <px:PXTextEdit runat="server" DataField="AcctName" DefaultLocale="" AlreadyLocalized="False" ID="edAcctName"></px:PXTextEdit>
                            <px:PXSelector runat="server" DataField="VendorClassID" ID="edVendorClassID" AllowEdit="true"></px:PXSelector>
                            <px:PXTextEdit runat="server" DataField="VendorClassID_description" DefaultLocale="" AlreadyLocalized="False" ID="edVendorClassID_description"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="Contact__Phone1" DefaultLocale="" AlreadyLocalized="False" ID="edContact__Phone1"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="Contact__Phone2" DefaultLocale="" AlreadyLocalized="False" ID="edContact__Phone2"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="Contact__EMail" DefaultLocale="" AlreadyLocalized="False" ID="edContact__EMail"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="Address__AddressLine1" DefaultLocale="" AlreadyLocalized="False" ID="edAddress__AddressLine1"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="Address__AddressLine2" DefaultLocale="" AlreadyLocalized="False" ID="edAddress__AddressLine2"></px:PXTextEdit>
                            <px:PXSelector runat="server" DataField="Address__CountryID_description" ID="edAddress__CountryID_description"></px:PXSelector>
                        </RowTemplate>
                        <Columns>
                            <px:PXGridColumn DataField="AcctCD" Width="120px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="AcctName" Width="200px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="VendorClassID"></px:PXGridColumn>
                            <px:PXGridColumn DataField="VendorClassID_description" Width="200px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Contact__Phone1" Width="200px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Contact__Phone2" Width="200px" SyncVisible="False" Visible="False"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Contact__EMail" Width="200px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Address__AddressLine1" Width="200px"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Address__AddressLine2" Width="200px" SyncVisible="False" Visible="False"></px:PXGridColumn>
                            <px:PXGridColumn DataField="Address__CountryID_description" Width="200px"></px:PXGridColumn>
                        </Columns>
                    </px:PXGridLevel>
                </Levels>
                <ActionBar>
                    <Actions>
                        <Search Enabled="False" />
                        <EditRecord Enabled="False" />
                        <NoteShow Enabled="False" />
                        <FilterShow Enabled="False" />
                        <FilterSet Enabled="False" />
                        <ExportExcel Enabled="False" />
                    </Actions>
                </ActionBar>
                <AutoSize Enabled="True" Container="Parent" />
                <Mode InitNewRow="False" AllowAddNew="False" AllowDelete="False" AllowUpdate="False"></Mode>
            </px:PXGrid>
        </Template2>
    </px:PXSplitContainer>
</asp:Content>

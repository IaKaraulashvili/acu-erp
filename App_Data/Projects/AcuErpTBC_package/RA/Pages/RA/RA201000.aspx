<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RA201000.aspx.cs" Inherits="Page_RA201000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="RA.RAClassMaint" PrimaryView="RAClasses" SuspendUnloading="False">
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="RAClasses" TabIndex="1100">
		<Template>
			<px:PXLayoutRule runat="server" StartRow="True" ControlSize="L" LabelsWidth="XM" StartColumn="True"/>
		    <px:PXSelector ID="edClassCD" runat="server" DataField="ClassCD">
            </px:PXSelector>
            <px:PXTextEdit ID="edExternalID" runat="server" AlreadyLocalized="False" DataField="ExternalID">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edDescription" runat="server" AlreadyLocalized="False" DataField="Description">
            </px:PXTextEdit>
            <px:PXDropDown ID="edAssetType" runat="server" DataField="AssetType">
            </px:PXDropDown>
            <px:PXCheckBox ID="edRealEstate" runat="server" AlreadyLocalized="False" DataField="RealEstate" Text="Real Estate">
            </px:PXCheckBox>
            <px:PXLayoutRule runat="server" ControlSize="L" GroupCaption="NBG" LabelsWidth="XM" StartRow="True">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edNBGAccount" runat="server" AlreadyLocalized="False" DataField="NBGAccount">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edNBGTransitAccount" runat="server" AlreadyLocalized="False" DataField="NBGTransitAccount">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edNBGReserveAccount" runat="server" AlreadyLocalized="False" DataField="NBGReserveAccount">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edNBGReserveExpensesAccount" runat="server" AlreadyLocalized="False" DataField="NBGReserveExpensesAccount">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" ControlSize="L" GroupCaption="IFRS" LabelsWidth="XM" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXSelector ID="edIFRSRAAccountID" runat="server" DataField="IFRSRAAccountID">
            </px:PXSelector>
            <px:PXSegmentMask ID="edIFRSTransitAccountID" runat="server" DataField="IFRSTransitAccountID">
            </px:PXSegmentMask>
		</Template>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXFormView>
</asp:Content>

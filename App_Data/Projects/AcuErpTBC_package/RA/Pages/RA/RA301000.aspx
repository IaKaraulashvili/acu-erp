<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RA301000.aspx.cs" Inherits="Page_RA301000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="RA.RAAssetEntry" PrimaryView="Assets" SuspendUnloading="False">
        <CallbackCommands>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" NotifyIndicator="True" ActivityIndicator="True" DataMember="Assets" TabIndex="1600">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="SM" StartColumn="True" />
            <px:PXSelector runat="server" DataField="AssetCD" ID="edAssetCD" AllowEdit="True" edit="1"></px:PXSelector>
            <px:PXSelector runat="server" DataField="ClassID" ID="edClassID" AllowEdit="True" edit="1" CommitChanges="True"></px:PXSelector>
            <px:PXDropDown runat="server" DataField="AssetType" ID="edAssetType" CommitChanges="True"></px:PXDropDown>
            <px:PXCheckBox runat="server" Text="Real Estate" DataField="RealEstate" AlreadyLocalized="False" ID="edRealEstate" CommitChanges="True"></px:PXCheckBox>
            <px:PXLayoutRule runat="server" ColumnSpan="2"></px:PXLayoutRule>
            <px:PXTextEdit runat="server" DataField="Description" AlreadyLocalized="False" ID="edDescription"></px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="Reason" ID="edReason" CommitChanges="true"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="RepossessionType" ID="edRepossessionType"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="ExecutionType" ID="edExecutionType"></px:PXDropDown>
            <px:PXTextEdit runat="server" DataField="EstateType" ID="edEstateType" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" Style="margin-top: 0px" DataMember="AssetLoanDetails">
        <Items>
            <px:PXTabItem Text="General Info">
                <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" TabIndex="7800" DataMember="CurrentAsset" DataSourceID="ds">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" GroupCaption="Asset General Info" LabelsWidth="M" ControlSize="M" StartColumn="True"></px:PXLayoutRule>
                            <px:PXTextEdit runat="server" DataField="PropertyID" AlreadyLocalized="False" ID="edPropertyID"></px:PXTextEdit>
                            <px:PXDateTimeEdit runat="server" DataField="PropertyRepossessionDate" AlreadyLocalized="False" ID="edPropertyRepossessionDate"></px:PXDateTimeEdit>
                            <px:PXDropDown runat="server" DataField="InspectionForm" ID="edInspectionForm"></px:PXDropDown>
                            <px:PXDropDown runat="server" DataField="AssetClassification" ID="edAssetClassification" CommitChanges="True"></px:PXDropDown>                           
                            <px:PXSelector ID="edOption30PercProjectCurrency" runat="server" DataField="Option30PercProjectCurrency">
                            </px:PXSelector>
                            <px:PXNumberEdit ID="edOption30PercProjectAmountInCurrency" runat="server" AlreadyLocalized="False" DataField="Option30PercProjectAmountInCurrency">
                            </px:PXNumberEdit>
                            <px:PXCheckBox runat="server" Text="Property Redeem" CommitChanges="True" DataField="PropertyRedeem" AlreadyLocalized="False" ID="edPropertyRedeem"></px:PXCheckBox>
                            <px:PXDateTimeEdit runat="server" DataField="InstalmentStartDate" AlreadyLocalized="False" ID="edInstalmentStartDate" DefaultLocale=""></px:PXDateTimeEdit>
                            <px:PXDateTimeEdit runat="server" DataField="InstalmentEndDate" AlreadyLocalized="False" ID="edInstalmentEndDate" DefaultLocale=""></px:PXDateTimeEdit>
                            <px:PXNumberEdit runat="server" DataField="legalExpenses" AlreadyLocalized="False" ID="edlegalExpenses" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="UtilitiesExpenses" AlreadyLocalized="False" ID="edUtilitiesExpenses" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="Last Evaluation" LabelsWidth="XM" ControlSize="XL"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="LastEvaluationAmtCury" AlreadyLocalized="False" ID="edLastEvaluationAmtCury" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="LastEvaluationAmt" AlreadyLocalized="False" ID="edLastEvaluationAmt" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="LastEvaluationSalvageAmtCury" AlreadyLocalized="False" ID="edLastEvaluationSalvageAmtCury" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="LastEvaluationSalvageAmt" AlreadyLocalized="False" ID="edLastEvaluationSalvageAmt" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Assignment" LabelsWidth="SM" ControlSize="M"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="RARecommendator" ID="edRARecommendator" AllowEdit="True" edit="1"></px:PXSelector>
                            <px:PXSelector runat="server" DataField="LoanManager" ID="edLoanManager" AllowEdit="True" edit="1"></px:PXSelector>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Loan Information" LoadOnDemand="true">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Details" Width="100%" ID="PXGrid3" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds" TabIndex="-12708">
                        <Levels>
                            <px:PXGridLevel DataMember="AssetLoanDetails">
                                <RowTemplate>
                                    <px:PXSelector runat="server" DataField="LoanID" ID="edLoanID"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LMSID" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LMSID"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanProduct" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanProductID"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanSegment" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanSegment"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanEndDate" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanEndDate"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanDisbursementDate" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanDisbursementDate"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanTransferDateToPLO" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanTransferDateToPLO"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__ClientID" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__ClientID"></px:PXTextEdit>
                                    <%--<px:PXTextEdit runat="server" DataField="RALoan__BorrowerName" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__BorrowerName"></px:PXTextEdit>--%>
                                    <px:PXTextEdit runat="server" DataField="RALoan__BorrowerID" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__BorrowerID"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__BorrowerContactInfo" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__BorrowerContactInfo"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__BorrowerInsider" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__BorrowerInsider"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__CoBorrowerName" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__CoBorrowerName"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__CoBorrowerID" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__CoBorrowerID"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__CoBorrowerInsider" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__CoBorrowerInsider"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorName1" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorName1"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorID1" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorID1"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorInsider1" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorInsider1"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorName2" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorName2"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorID2" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorID2"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorInsider2" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorInsider2"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorName3" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorName3"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorID3" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorID3"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__GuarantorInsider3" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__GuarantorInsider3"></px:PXTextEdit>
                                    <%--<px:PXTextEdit runat="server" DataField="RALoan__OverdueDays" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueDays"></px:PXTextEdit>--%>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverduePrincipleCury" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverduePrincipleCury"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverduePenaltyCury" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverduePenaltyCury"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverdueInsuranceCury" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueInsuranceCury"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverdueInterestCury" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueInterestCury"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverduePenalty" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverduePenalty"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__CurrencyDate" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__CurrencyDate"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverdueInsurance" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueInsurance"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverdueInterest" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueInterest"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__CuryID" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__CuryID"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanAmtCury" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanAmtCury"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__LoanAmtGel" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__LoanAmtGel"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__BorrowerName" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__BorrowerName"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__NBGLoanProvisionRate" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__NBGLoanProvisionRate"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__IFRSLoanProvisionRate" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__IFRSLoanProvisionRate"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="RALoan__OverdueDays" DefaultLocale="" AlreadyLocalized="False" ID="edRALoan__OverdueDays"></px:PXTextEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn LinkCommand="ViewLoan" DataField="LoanID" CommitChanges="true"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LMSID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanProduct" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanSegment" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanEndDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanDisbursementDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanTransferDateToPLO" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__ClientID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerContactInfo" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterestCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenalty" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CurrencyDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsurance" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterest" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CuryID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtGel" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__NBGLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__IFRSLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueDays" Width="200px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Repossession Info">
                <Template>
                    <px:PXFormView runat="server" ID="PXFormView4" DataSourceID="ds" DataMember="Assets" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" GroupCaption="Expected Repossession Amount" LabelsWidth="XM" ControlSize="L" StartColumn="True"></px:PXLayoutRule>
                            <px:PXDateTimeEdit runat="server" DataField="ExpectedCuryDate" AlreadyLocalized="False" ID="edExpectedCuryDate" Size="SM" CommitChanges="True"></px:PXDateTimeEdit>
                            <px:PXLayoutRule runat="server" Merge="True"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="ExpectedRepossessionCuryID" ID="edExpectedRepossessionCuryID" CommitChanges="True" Width="60px"></px:PXSelector>
                            <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionCuryRate" AlreadyLocalized="False" ID="edExpectedRepossessionCuryRate" SuppressLabel="True" Width="74px" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionAmt" AlreadyLocalized="False" ID="edExpectedRepossessionAmt" Size="SM" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXCheckBox runat="server" Text="FixedRate" CommitChanges="true" DataField="FixedRate" AlreadyLocalized="False" ID="edFixedRate"></px:PXCheckBox>
                            <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionAmtCury" AlreadyLocalized="False" ID="edExpectedRepossessionAmtCury" Size="SM" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionAmtGel" AlreadyLocalized="False" ID="edExpectedRepossessionAmtGel" Size="SM" DefaultLocale=""></px:PXNumberEdit>

                            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="Recommended Repossession Amount" LabelsWidth="XM" ControlSize="L"></px:PXLayoutRule>
                            <px:PXDateTimeEdit runat="server" DataField="RecommendedCuryDate" AlreadyLocalized="False" ID="PXDateTimeEdit1" Size="SM" CommitChanges="True"></px:PXDateTimeEdit>
                            <px:PXLayoutRule runat="server" Merge="True"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="RecommendedRepossessionCuryID" CommitChanges="True" ID="edRecommendedRepossessionCuryID" Width="60px"></px:PXSelector>
                            <px:PXNumberEdit runat="server" DataField="RecommendedRepossessionCuryRate" CommitChanges="True" AlreadyLocalized="False" ID="edRecommendedRepossessionCuryRate" SuppressLabel="True" Width="74px" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="RecommendedRepossessionAmt" AlreadyLocalized="False" ID="edRecommendedRepossessionAmt" Size="SM" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="RecommendedRepossessionAmtCury" AlreadyLocalized="False" ID="edRecommendedRepossessionAmtCury" Size="SM" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="RecommendedRepossessionAmtGel" AlreadyLocalized="False" ID="edRecommendedRepossessionAmtGel" Size="SM" DefaultLocale=""></px:PXNumberEdit>

                            <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Final Repossession Amount" LabelsWidth="XM" ControlSize="L"></px:PXLayoutRule>
                            <px:PXDateTimeEdit runat="server" DataField="FinalCuryDate" AlreadyLocalized="False" ID="edFinalCuryDate" Size="SM"></px:PXDateTimeEdit>
                            <px:PXLayoutRule runat="server" Merge="True"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="FinalRepossessionCuryID" ID="edFinalRepossessionCuryID" Width="60px"></px:PXSelector>
                            <px:PXNumberEdit runat="server" DataField="FinalRepossessionCuryRate" AlreadyLocalized="False" ID="edFinalRepossessionCuryRate" SuppressLabel="True" Width="74px" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="FinalRepossessionAmt" AlreadyLocalized="False" ID="edFinalRepossessionAmt" Size="SM" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="FinalRepossessionAmtCury" AlreadyLocalized="False" ID="edFinalRepossessionAmtCury" Size="SM" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="FinalRepossessionAmtGel" AlreadyLocalized="False" ID="edFinalRepossessionAmtGel" Size="SM" DefaultLocale=""></px:PXNumberEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Property Owners" LoadOnDemand="true">
                <Template>
                    <px:PXGrid runat="server" ID="PXGrid1" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" TabIndex="14700" AllowPaging="True" Height="500px" SkinID="Details" Width="100%">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="AssetID,LineNbr" DataMember="PropertyOwner">
                                <RowTemplate>
                                    <px:PXTextEdit runat="server" DataField="PropertyOwner" AlreadyLocalized="False" ID="edPropertyOwner"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="PropertyOwnerID" AlreadyLocalized="False" ID="edPropertyOwnerID"></px:PXTextEdit>
                                    <px:PXCheckBox runat="server" Text="Insider" DataField="Insider" AlreadyLocalized="False" ID="edInsider"></px:PXCheckBox>
                                    <px:PXTextEdit runat="server" DataField="PORelationshipToClient" AlreadyLocalized="False" ID="edPORelationshipToClient"></px:PXTextEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="PropertyOwner" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="PropertyOwnerID"></px:PXGridColumn>
                                    <px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="Insider" Width="60px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="PORelationshipToClient" Width="200px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <ContentLayout LabelsWidth="SM" ControlSize="M" AutoSizeControls="True"></ContentLayout>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Evaluation" LoadOnDemand="True">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Inquire" Width="100%" ID="PXGrid2" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds">
                        <Levels>
                            <px:PXGridLevel DataMember="Evaluation" DataKeyNames="EvaluationCD">
                                <RowTemplate>
                                    <px:PXSelector runat="server" DataField="EvaluationCD" ID="edEvaluationCD"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="Appraiser" AlreadyLocalized="False" ID="edAppraiser"></px:PXTextEdit>
                                    <px:PXDropDown runat="server" DataField="EvaluationType" ID="edEvaluationType"></px:PXDropDown>
                                    <px:PXNumberEdit runat="server" DataField="FairMarketValueCury" AlreadyLocalized="False" ID="edFairMarketValueCury"></px:PXNumberEdit>
                                    <px:PXSelector runat="server" DataField="CuryID" ID="edCuryID"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="ExternalID" AlreadyLocalized="False" ID="edExternalID"></px:PXTextEdit>
                                    <px:PXSelector runat="server" DataField="AssetID" ID="edAssetID"></px:PXSelector>
                                    <px:PXDropDown runat="server" DataField="Status" ID="edStatus" CommitChanges="true"></px:PXDropDown>
                                    <px:PXDropDown runat="server" DataField="EstimationType" ID="edEstimationType"></px:PXDropDown>
                                    <px:PXNumberEdit runat="server" DataField="SalvageValueCury" AlreadyLocalized="False" ID="edSalvageValueCury"></px:PXNumberEdit>
                                    <px:PXDateTimeEdit runat="server" DataField="EvaluationDate" AlreadyLocalized="False" ID="edEvaluationDate"></px:PXDateTimeEdit>
                                    <px:PXDropDown runat="server" DataField="EvaluationApproach" ID="edEvaluationApproach"></px:PXDropDown>
                                    <px:PXDateTimeEdit runat="server" DataField="InspectDate" AlreadyLocalized="False" ID="edInspectDate"></px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit runat="server" DataField="CurrencyDate" AlreadyLocalized="False" ID="edCurrencyDate"></px:PXDateTimeEdit>
                                    <px:PXNumberEdit runat="server" DataField="EvaluationExpenses" AlreadyLocalized="False" ID="edEvaluationExpenses"></px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="EvaluationCD" LinkCommand="ViewEvaluation"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationDate" Width="90px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Appraiser" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="SalvageValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="FairMarketValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="AssetID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EstimationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationApproach"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="InspectDate"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CurrencyDate"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="EvaluationExpenses"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowAddNew="False" AllowDelete="False" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Financial Details" LoadOnDemand="true">
                <Template>
                    <px:PXFormView runat="server" ID="PXFormView2" DataSourceID="ds" DataMember="FinancialDetail" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" GroupCaption="NBG" LabelsWidth="XM" ControlSize="L" StartColumn="True"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="NBGBalanceValueGross" AlreadyLocalized="False" ID="edNBGBalanceValueGross" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGBalanceValueNet" AlreadyLocalized="False" ID="edNBGBalanceValueNet" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="VATNBG" AlreadyLocalized="False" ID="edVATNBG" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGInvestmentPropertyGrossLand" AlreadyLocalized="False" ID="edNBGInvestmentPropertyGrossLand" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGInvestmentPropertyGrossBuilding" AlreadyLocalized="False" ID="edNBGInvestmentPropertyGrossBuilding" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGInvestmentProperty" AlreadyLocalized="False" ID="edNBGInvestmentProperty" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGRepossessedAsset" AlreadyLocalized="False" ID="edNBGRepossessedAsset" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="NBGWrittenOffProperty" AlreadyLocalized="False" ID="edNBGWrittenOffProperty" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="ProvisionPct" AlreadyLocalized="False" ID="edProvisionPct" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="ProvisionInGel" AlreadyLocalized="False" ID="edProvisionInGel" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="IFRS" LabelsWidth="XM" ControlSize="L"></px:PXLayoutRule>
                            <px:PXNumberEdit runat="server" DataField="IFRSBalanceValueGross" AlreadyLocalized="False" ID="edIFRSBalanceValueGross" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="IFRSBalanceValueNet" AlreadyLocalized="False" ID="edIFRSBalanceValueNet" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="VATIFRS" AlreadyLocalized="False" ID="edVATIFRS" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="IFRSInvestmentPropertyGrossLand" AlreadyLocalized="False" ID="edIFRSInvestmentPropertyGrossLand" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="IFRSInvestmentPropertyGrossBuilding" AlreadyLocalized="False" ID="edIFRSInvestmentPropertyGrossBuilding" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="InvestmentPropertyGross" AlreadyLocalized="False" ID="edInvestmentPropertyGross" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="IFRSAppreciation" AlreadyLocalized="False" ID="edIFRSAppreciation" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="IFRSWriteDown" AlreadyLocalized="False" ID="edIFRSWriteDown" DefaultLocale=""></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="DepreciationCumulative" AlreadyLocalized="False" ID="edDepreciationCumulative" DefaultLocale=""></px:PXNumberEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="GL Accounts" LoadOnDemand="true">
                <Template>
                    <px:PXFormView runat="server" ID="PXFormView3" DataSourceID="ds" DataMember="Assets" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" GroupCaption="NBG" LabelsWidth="XM" ControlSize="L" StartColumn="True"></px:PXLayoutRule>
                            <px:PXTextEdit runat="server" DataField="NBGAccount" AlreadyLocalized="False" ID="edNBGAccount"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="NBGTransitAccount" AlreadyLocalized="False" ID="edNBGTransitAccount"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="NBGReserveAccount" AlreadyLocalized="False" ID="edNBGReserveAccount"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="NBGReserveExpensesAccount" AlreadyLocalized="False" ID="edNBGReserveExpensesAccount"></px:PXTextEdit>
                            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="IFRS" LabelsWidth="XM" ControlSize="L"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="IFRSRAAccountID" DefaultLocale="" AlreadyLocalized="False" ID="edIFRSRAAccountID"></px:PXSelector>
                            <px:PXSelector runat="server" DataField="IFRSTransitAccountID" DefaultLocale="" AlreadyLocalized="False" ID="edIFRSTransitAccountID"></px:PXSelector>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Attachments">
                <Template>
                    <px:PXFormView runat="server" ID="PXFormView5" DataMember="Assets" Width="100%" SkinID="Transparent">
                        <Template>
                            <px:PXLayoutRule ID="PXLayoutRule5" runat="server" StartRow="True" LabelsWidth="S" ControlSize="XM" />
                            <px:PXImageUploader ID="PXImageUploader1" runat="server" DataField="Image1" Height="120px" Width="420px" AllowUpload="True" AllowNoImage="true"  />
                            <px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" />
                            <px:PXImageUploader ID="PXImageUploader2" runat="server" DataField="Image2" Height="120px" Width="420px" AllowUpload="True" AllowNoImage="true" />
                            <px:PXLayoutRule ID="PXLayoutRule2" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" />
                            <px:PXImageUploader ID="PXImageUploader3" runat="server" DataField="Image3" Height="120px" Width="420px" AllowUpload="True" AllowNoImage="true" />
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem LoadOnDemand="True" RepaintOnDemand="false" Text="Linked Asset" VisibleExp="DataControls[&quot;edReason&quot;].Value != RE" BindingContext="form">
                <Template>
                    <px:PXGrid ID="PXGrid4" runat="server" AllowPaging="True" Height="500px" SkinID="Details" TemporaryFilterCaption="Filter Applied" Width="100%" DataSourceID="ds" TabIndex="19200">
                        <Levels>
                            <px:PXGridLevel DataMember="LinkedAsset" DataKeyNames="AssetID,LineNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edLinkedID" runat="server" DataField="LinkedID" CommitChanges="True" Size="Empty" Width="200px" AutoRefresh="True">
                                    </px:PXSelector>
                                    <px:PXDropDown ID="edRAAsset__Reason" runat="server" DataField="RAAsset__Reason" Size="Empty">
                                    </px:PXDropDown>
                                    <px:PXTextEdit ID="edRAAsset__Description" runat="server" AlreadyLocalized="False" DataField="RAAsset__Description" Size="Empty" Width="20%" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXNumberEdit ID="edRAAsset__LastEvaluationAmt" runat="server" AlreadyLocalized="False" DataField="RAAsset__LastEvaluationAmt" Size="Empty">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRAAsset__LastEvaluationAmtCury" runat="server" AlreadyLocalized="False" DataField="RAAsset__LastEvaluationAmtCury" Size="Empty">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="LinkedID" Width="150px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Description" Width="150px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Reason" Width="200px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmt" TextAlign="Left" Width="250px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmtCury" TextAlign="Left" Width="250px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Details" LoadOnDemand="true" RepaintOnDemand="false" VisibleExp="DataControls[&quot;edEstateType&quot;].Value == RealEstate" BindingContext="form">
                <Template>
                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True"></px:PXLayoutRule>
                    <px:PXFormView runat="server" ID="PXFormViewMainInfo" DataSourceID="ds" DataMember="RealEstate" Caption="Main Info" RenderStyle="Fieldset" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edCadastralCode" DataField="CadastralCode" LabelWidth="150px" Width="150px" DefaultLocale=""></px:PXTextEdit>
                            <px:PXNumberEdit ID="edLandAreaDefined" runat="server" AlreadyLocalized="False" DataField="LandAreaDefined" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edLandAreaUndefined" runat="server" AlreadyLocalized="False" DataField="LandAreaUndefined" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXTextEdit ID="edUnitOfMeasure" runat="server" AlreadyLocalized="False" DataField="UnitOfMeasure" LabelWidth="150px" Width="100px" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edLandType" runat="server" AlreadyLocalized="False" DataField="LandType" LabelWidth="150px" Width="150px" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edLandByPurpose" runat="server" AlreadyLocalized="False" DataField="LandByPurpose" LabelWidth="150px" Width="150px" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXNumberEdit ID="edMainEntrance" runat="server" AlreadyLocalized="False" DataField="MainEntrance" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edFlatNumber" runat="server" AlreadyLocalized="False" DataField="FlatNumber" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edArea" runat="server" AlreadyLocalized="False" DataField="Area" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXTextEdit ID="edCondition" runat="server" AlreadyLocalized="False" DataField="Condition" LabelWidth="150px" Width="150px" DefaultLocale="">
                            </px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                    <px:PXFormView runat="server" ID="PXFormViewLocationInfo" DataSourceID="ds" DataMember="RealEstate" Caption="Location Info" RenderStyle="Fieldset" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXDropDown ID="edRegion" runat="server" DataField="Region" LabelWidth="150px" Width="150px">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edTbilisiDistrict" runat="server" DataField="TbilisiDistrict" LabelWidth="150px" Width="150px">
                            </px:PXDropDown>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edAddress" DataField="Address" LabelWidth="150px" LocalizationInfo="" Width="150px" DefaultLocale=""></px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
                    <px:PXFormView runat="server" ID="PXFormViewInspectionFields" DataSourceID="ds" DataMember="RealEstate" Caption="Inspection Fields" RenderStyle="Fieldset" TabIndex="1900">
                        <Template>
                             <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
                            <px:PXDropDown ID="edInformationSource" runat="server" DataField="InformationSource" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edWater" runat="server" AlreadyLocalized="False" DataField="Water" Text="Water" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edFencing" runat="server" AlreadyLocalized="False" DataField="Fencing" Text="Fencing" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edIrrigationSystem" runat="server" AlreadyLocalized="False" DataField="IrrigationSystem" Text="Irrigation System" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edElectricity" runat="server" AlreadyLocalized="False" DataField="Electricity" Text="Electricity" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edNaturalGas" runat="server" AlreadyLocalized="False" DataField="NaturalGas" Text="Natural Gas" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edSewerageSystem" runat="server" AlreadyLocalized="False" DataField="SewerageSystem" Text="Sewerage System" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edRoadType" runat="server" DataField="RoadType">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edCoastline" runat="server" DataField="Coastline" >
                            </px:PXDropDown>
                            <px:PXDropDown ID="edAreaCondition" runat="server" DataField="AreaCondition" >
                            </px:PXDropDown>
                            <px:PXNumberEdit ID="edRoomQuantity" runat="server" AlreadyLocalized="False" DataField="RoomQuantity" >
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edBuildingQuantityNumbering" runat="server" AlreadyLocalized="False" DataField="BuildingQuantityNumbering" >
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edFloorQuantity" runat="server" AlreadyLocalized="False" DataField="FloorQuantity">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edFloor" runat="server" AlreadyLocalized="False" DataField="Floor">
                            </px:PXNumberEdit>
                            <px:PXDropDown ID="edStatusOfConstruction" runat="server" DataField="StatusOfConstruction">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edConstructionDate" runat="server" DataField="ConstructionDate" >
                            </px:PXDropDown>
                           <%-- <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edNumber" DataField="Number" LabelWidth="150px" Width="150px" DefaultLocale=""></px:PXTextEdit>--%>
                            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
                            <px:PXDropDown ID="edOwnerUseBuildingAsHabitat" runat="server" AlreadyLocalized="False" DataField="OwnerUseBuildingAsHabitat" Text="Owner Use Building As Habitat">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edRented" runat="server" AlreadyLocalized="False" DataField="Rented" Text="Rented" CommitChanges="True">
                            </px:PXDropDown>
                             <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXLayoutRule runat="server" Merge="True" ControlSize="Empty" LabelsWidth="0"></px:PXLayoutRule>
                            <px:PXNumberEdit ID="edMonthlyRentFeeCury" runat="server" AlreadyLocalized="False" DataField="MonthlyRentFeeCury" Width="74px" LabelWidth="150px" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXSelector ID="edCuryID" runat="server" DataField="CuryID" SuppressLabel="True" Width="60px">
                            </px:PXSelector>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXDropDown ID="edApartmentType" runat="server" DataField="ApartmentType" LabelWidth="150px" Width="200px">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edApartmentInItalianYard" runat="server" AlreadyLocalized="False" DataField="ApartmentInItalianYard" Text="Apartment in Italian Yard" Size="M" AlignLeft="True">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edBalcony" runat="server" AlreadyLocalized="False" DataField="Balcony" Text="Balcony" Size="S">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edElevator" runat="server" AlreadyLocalized="False" DataField="Elevator" Text="Elevator" Size="S">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edBathroom" runat="server" AlreadyLocalized="False" DataField="Bathroom" Text="Bathroom" Size="S">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edToilet" runat="server" AlreadyLocalized="False" DataField="Toilet" Text="Toilet" Size="S">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edFunctionalArea" runat="server" DataField="FunctionalArea" LabelWidth="150px" Width="200px">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edViticultureMicrozone" runat="server" DataField="ViticultureMicrozone" LabelWidth="150px" Width="200px">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edGeometryOfLandArea" runat="server" DataField="GeometryOfLandArea" LabelWidth="150px" Width="200px">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edRelief" runat="server" DataField="Relief" LabelWidth="150px" Width="200px">
                            </px:PXDropDown>
                        </Template>
                    </px:PXFormView>
                    <px:PXLayoutRule runat="server" StartRow="True"></px:PXLayoutRule>
                    <px:PXFormView runat="server" ID="PXFormViewCommonFieldsDetailsTab" DataSourceID="ds" DataMember="CurrentAsset" RenderStyle="Fieldset" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edOtherImprovements" DataField="OtherImprovements" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edComment" DataField="Comment" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem LoadOnDemand="True" RepaintOnDemand="False" Text="Details" VisibleExp="DataControls[&quot;edEstateType&quot;].Value == NonRealEstate" BindingContext="form">
                <Template>
                    <px:PXLayoutRule runat="server" GroupCaption="Non-real estate info" StartRow="True">
                    </px:PXLayoutRule>
                    <px:PXFormView DataMember="NonRealEstate" ID="PXFormView6" runat="server" DataSourceID="ds">
                        <Template>
                            <px:PXLayoutRule runat="server" ColumnWidth="L" StartColumn="True" StartRow="True" LabelsWidth="M">
                            </px:PXLayoutRule>
                            <px:PXTextEdit ID="edVINCode" runat="server" AlreadyLocalized="False" DataField="VINCode" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edChassisID" runat="server" AlreadyLocalized="False" DataField="ChassisID" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edTechPassportNumber" runat="server" AlreadyLocalized="False" DataField="TechPassportNumber" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edPlateNumber" runat="server" AlreadyLocalized="False" DataField="PlateNumber" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edMark" runat="server" AlreadyLocalized="False" DataField="Mark" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edAutomotiveBodyType" runat="server" AlreadyLocalized="False" DataField="AutomotiveBodyType" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edColor" runat="server" AlreadyLocalized="False" DataField="Color" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXNumberEdit ID="edEngineVolume" runat="server" AlreadyLocalized="False" DataField="EngineVolume" Width="100%" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXTextEdit ID="edModel" runat="server" AlreadyLocalized="False" DataField="Model" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXDropDown ID="edVehicleCondition" runat="server" DataField="VehicleCondition">
                            </px:PXDropDown>
                            <px:PXLayoutRule LabelsWidth="M" runat="server" ColumnWidth="L" StartColumn="True">
                            </px:PXLayoutRule>
                            <px:PXDropDown AlignLeft="True" ID="edCustomsDuty" runat="server" AlreadyLocalized="False" DataField="CustomsDuty" Text="CustomsDuty">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edTransmission" runat="server" DataField="Transmission">
                            </px:PXDropDown>
                            <px:PXNumberEdit ID="edMileage" runat="server" AlreadyLocalized="False" DataField="Mileage" Width="100%" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXDropDown ID="edWheelLocation" runat="server" DataField="WheelLocation">
                            </px:PXDropDown>
                            <px:PXTextEdit ID="edPurpose" runat="server" AlreadyLocalized="False" DataField="Purpose" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXDateTimeEdit ID="edIssueDate" runat="server" AlreadyLocalized="False" DataField="IssueDate" Width="100%" DefaultLocale="">
                            </px:PXDateTimeEdit>
                            <px:PXDateTimeEdit ID="edRegistrationDate" runat="server" AlreadyLocalized="False" DataField="RegistrationDate" Width="100%" DefaultLocale="">
                            </px:PXDateTimeEdit>
                            <px:PXTextEdit ID="edParameters" runat="server" AlreadyLocalized="False" DataField="Parameters" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edPropertyDescription" runat="server" AlreadyLocalized="False" DataField="PropertyDescription" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edSerialNumber" runat="server" AlreadyLocalized="False" DataField="SerialNumber" DefaultLocale="">
                            </px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                    <px:PXFormView ID="PXFormView7" runat="server" DataMember="CurrentAsset" DataSourceID="ds" RenderStyle="Fieldset" TabIndex="1900">
                        <Template>
                            <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edOtherImprovements" DataField="OtherImprovements" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                            <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edComment" DataField="Comment" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Restrictions On Property" LoadOnDemand="true">
                <Template>
                    <px:PXGrid runat="server" ID="PXGrid1" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" TabIndex="14700" AllowPaging="True" Height="500px" SkinID="Details" Width="100%">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="AssetID,LineNbr" DataMember="Restrictions">
                                <Columns>
                                    <px:PXGridColumn DataField="RestrictionStartDate" Width="100px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RestrictionType" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ParticipantPersons" Width="300px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <ContentLayout LabelsWidth="SM" ControlSize="M" AutoSizeControls="True"></ContentLayout>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RA302000.aspx.cs" Inherits="Page_RA302000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="RA.RALoanEntry" PrimaryView="Loans">
        <CallbackCommands>
            <px:PXDSCallbackCommand Visible="False" Name="CurrencyView" />
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataMember="Loans" DataSourceID="ds" Style="z-index: 100" Width="100%">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" />
            <px:PXSelector runat="server" DataField="LoanCD" ID="edLoanCD"></px:PXSelector>
            <px:PXTextEdit ID="edLMSID" runat="server" AlreadyLocalized="False" DataField="LMSID" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXDropDown CommitChanges="true" DataField="LoanStatus" ID="edLoanStatus" runat="server"></px:PXDropDown>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Loan details">
                <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" TabIndex="7800" DataMember="Loans" DataSourceID="ds">
                        <Template>
                            <px:PXLayoutRule runat="server" GroupCaption="People related to loan" LabelsWidth="M" StartColumn="True" StartRow="True">
                            </px:PXLayoutRule>
                            <px:PXTextEdit ID="edBorrowerName" runat="server" AlreadyLocalized="False" DataField="BorrowerName" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edBorrowerID" runat="server" AlreadyLocalized="False" DataField="BorrowerID" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edBorrowerLMSID" runat="server" AlreadyLocalized="False" DataField="BorrowerLMSID" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edBorrowerContactInfo" runat="server" AlreadyLocalized="False" DataField="BorrowerContactInfo" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXCheckBox ID="edBorrowerInsider" runat="server" AlreadyLocalized="False" DataField="BorrowerInsider" Text="BorrowerInsider">
                            </px:PXCheckBox>
                            <px:PXTextEdit ID="edCoBorrowerName" runat="server" AlreadyLocalized="False" DataField="CoBorrowerName" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edCoBorrowerID" runat="server" AlreadyLocalized="False" DataField="CoBorrowerID" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edCoBorrowerLMSID" runat="server" AlreadyLocalized="False" DataField="CoBorrowerLMSID" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXCheckBox ID="edCoBorrowerInsider" runat="server" AlreadyLocalized="False" DataField="CoBorrowerInsider" Text="CoBorrowerInsider">
                            </px:PXCheckBox>
                            <px:PXTextEdit ID="edGuarantorName1" runat="server" AlreadyLocalized="False" DataField="GuarantorName1" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edGuarantorID1" runat="server" AlreadyLocalized="False" DataField="GuarantorID1" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXCheckBox ID="edGuarantorInsider1" runat="server" AlreadyLocalized="False" DataField="GuarantorInsider1" Text="GuarantorInsider1">
                            </px:PXCheckBox>
                            <px:PXTextEdit ID="edGuarantorName2" runat="server" AlreadyLocalized="False" DataField="GuarantorName2" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edGuarantorID2" runat="server" AlreadyLocalized="False" DataField="GuarantorID2" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXCheckBox ID="edGuarantorInsider2" runat="server" AlreadyLocalized="False" DataField="GuarantorInsider2" Text="GuarantorInsider2">
                            </px:PXCheckBox>
                            <px:PXTextEdit ID="edGuarantorName3" runat="server" AlreadyLocalized="False" DataField="GuarantorName3" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edGuarantorID3" runat="server" AlreadyLocalized="False" DataField="GuarantorID3" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXCheckBox ID="edGuarantorInsider3" runat="server" AlreadyLocalized="False" DataField="GuarantorInsider3" Text="GuarantorInsider3">
                            </px:PXCheckBox>
                            <px:PXLayoutRule runat="server" GroupCaption="Loan info" LabelsWidth="M" StartColumn="True">
                            </px:PXLayoutRule>
                            <px:PXTextEdit ID="edLoanProduct" runat="server" AlreadyLocalized="False" DataField="LoanProduct" DefaultLocale="">
                            </px:PXTextEdit>
                            <px:PXTextEdit ID="edLoanSegment" runat="server" AlreadyLocalized="False" DataField="LoanSegment" DefaultLocale="" Size="S">
                            </px:PXTextEdit>
                            <px:PXDateTimeEdit ID="edLoanDisbursementDate" runat="server" AlreadyLocalized="False" DataField="LoanDisbursementDate" DefaultLocale="">
                            </px:PXDateTimeEdit>
                            <px:PXDateTimeEdit ID="edLoanEndDate" runat="server" AlreadyLocalized="False" DataField="LoanEndDate" DefaultLocale="">
                            </px:PXDateTimeEdit>
                            <px:PXDateTimeEdit ID="edLoanTransferDateToPLO" runat="server" AlreadyLocalized="False" DataField="LoanTransferDateToPLO" DefaultLocale="">
                            </px:PXDateTimeEdit>
                            <px:PXLayoutRule runat="server" GroupCaption="Loan amount" StartGroup="True">
                            </px:PXLayoutRule>
                            <px:PXDateTimeEdit ID="edCurrencyDate" runat="server" AlreadyLocalized="False" CommitChanges="True" DataField="CurrencyDate">
                            </px:PXDateTimeEdit>
                            <pxa:PXCurrencyRate DataField="CuryID" ID="edCury" runat="server" DataMember="_RALoan_PX.Objects.CM.Currency+curyID_" RateTypeView="_RALoan_CurrencyInfo_" DataSourceID="ds" Width="285px"></pxa:PXCurrencyRate>
                            <px:PXNumberEdit ID="edLoanAmtCury" runat="server" AlreadyLocalized="False" CommitChanges="true" DataField="LoanAmtCury" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edInterest" runat="server" AlreadyLocalized="False" DataField="Interest" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edNBGLoanProvisionRate" runat="server" AlreadyLocalized="False" DataField="NBGLoanProvisionRate" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edIFRSLoanProvisionRate" runat="server" AlreadyLocalized="False" DataField="IFRSLoanProvisionRate" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXLayoutRule runat="server" GroupCaption="Overdue" StartGroup="True">
                            </px:PXLayoutRule>
                            <px:PXNumberEdit ID="edOverdueDays" runat="server" AlreadyLocalized="False" DataField="OverdueDays" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edOverduePrincipleCury" runat="server" CommitChanges="true" AlreadyLocalized="False" DataField="OverduePrincipleCury" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edOverduePenaltyCury" runat="server" CommitChanges="true" AlreadyLocalized="False" DataField="OverduePenaltyCury" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edOverdueInsuranceCury" runat="server" CommitChanges="true" AlreadyLocalized="False" DataField="OverdueInsuranceCury" DefaultLocale="">
                            </px:PXNumberEdit>
                            <px:PXNumberEdit ID="edOverdueInterestCury" runat="server" CommitChanges="true" AlreadyLocalized="False" DataField="OverdueInterestCury" DefaultLocale="">
                            </px:PXNumberEdit>

                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Assets">
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

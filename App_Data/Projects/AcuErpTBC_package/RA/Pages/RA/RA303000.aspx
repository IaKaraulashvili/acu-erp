<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RA303000.aspx.cs" Inherits="Page_RA303000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="RA.RARepossessionApplicationEntry" PrimaryView="RepossessionApplication" SuspendUnloading="False">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="DeleteAsset" Visible="False" CommitChanges="True" />
            <px:PXDSCallbackCommand Name="AddAsset" Visible="False" CommitChanges="True" />
            <px:PXDSCallbackCommand CommitChanges="True" Name="showAddAsset" Visible="false" />
            <px:PXDSCallbackCommand Visible="false" Name="GetDataFromApi" ></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Visible="false" Name="OpenAsset"></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="fillAllocation" Visible="false"></px:PXDSCallbackCommand>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXSmartPanel ID="PXSmartPanel1" runat="server" AcceptButtonID="PXButtonOK" AutoReload="true" CancelButtonID="PXButtonCancel"
        Caption="Add Asset" CaptionVisible="True" DesignView="Content" HideAfterAction="false" Width="500px" Key="NewAsset" AutoRepaint="true"
        Overflow="Hidden">
        <px:PXFormView ID="PXFormView2" runat="server" SkinID="Transparent" DataMember="NewAsset"
            DefaultControlID="edOwner">
            <Template>
                <px:PXLayoutRule ID="PXLayoutRule33" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" />
                <px:PXSelector runat="server" DataField="ClassID" CommitChanges="true" ID="edClassID" AllowEdit="True" edit="1"></px:PXSelector>
                <px:PXDropDown runat="server" DataField="Reason" CommitChanges="true" ID="edReason"></px:PXDropDown>
                <px:PXTextEdit runat="server" DataField="PropertyID" AlreadyLocalized="False" ID="edPropertyID" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="PlateNumber" AlreadyLocalized="False" ID="edPlateNumber" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="OwnerID" AlreadyLocalized="False" ID="edOwnerID" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="CadastralCode" AlreadyLocalized="False" ID="PXTextEdit1" DefaultLocale=""></px:PXTextEdit>
            </Template>
        </px:PXFormView>
        <px:PXPanel ID="PXPanel2" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton2" runat="server" DialogResult="Cancel" Text="Back" />
            <px:PXButton ID="PXButton6" runat="server" DialogResult="OK" Text="Get Data From API" SyncVisible="false">
                <AutoCallBack Command="GetDataFromApi" Target="ds"></AutoCallBack>
            </px:PXButton>
            <px:PXButton ID="PXButton1" runat="server" DialogResult="OK" Text="Open Asset" SyncVisible="false">
                <AutoCallBack Command="OpenAsset" Target="ds"></AutoCallBack>
            </px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
        <px:PXSmartPanel ID="PXSmartPanel2" runat="server" AcceptButtonID="PXButtonOK" AutoReload="true" CancelButtonID="PXButtonCancel"
        Caption="Delete Asset" CaptionVisible="True" DesignView="Content" HideAfterAction="false" Width="300px" Key="Dialog" AutoRepaint="true"
        Overflow="Hidden">
        <px:PXFormView ID="PXFormView1" runat="server" SkinID="Transparent" DataMember="Dialog">
            <Template>
                <px:PXLayoutRule ID="PXLayoutRule33" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" />
                <px:PXLabel runat="server"  CommitChanges="true" ID="edlbl1" Text="Are you sure to delete this record?" AllowEdit="True" edit="1"></px:PXLabel>
            </Template>
        </px:PXFormView>
        <px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButtonCancel" runat="server" DialogResult="Cancel" Text="Cancel" />
            <px:PXButton ID="PXButtonOK" runat="server" DialogResult="OK" Text="Yes" >
            </px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="RepossessionApplication" TabIndex="4200" ActivityIndicator="True" NotifyIndicator="True">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="SM" />
            <px:PXSelector runat="server" DataField="RepossessionApplicationCD" ID="edRepossessionApplicationCD"></px:PXSelector>
            <px:PXDropDown runat="server" DataField="RepossessionType" ID="edRepossessionType"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
            <px:PXCheckBox runat="server" Text="Hold" CommitChanges="true" DataField="Hold" AlreadyLocalized="False" ID="edHold"></px:PXCheckBox>
            <px:PXSelector runat="server" DataField="LoanManager" ID="edLoanManager" CommitChanges="true"></px:PXSelector>
            <px:PXSelector runat="server" DataField="ReportsTo" ID="edReportsTo"></px:PXSelector>
            <px:PXDateTimeEdit runat="server" DataField="ReleaseDate" DefaultLocale="" AlreadyLocalized="False" ID="edReleaseDate"></px:PXDateTimeEdit>
            <px:PXDateTimeEdit runat="server" DataField="ApprovalDate" DefaultLocale="" AlreadyLocalized="False" ID="edApprovalDate"></px:PXDateTimeEdit>
            <px:PXLayoutRule runat="server" ColumnSpan="2"></px:PXLayoutRule>
            <px:PXTextEdit runat="server" DataField="Description" AlreadyLocalized="False" ID="edDescription" DefaultLocale=""></px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
            <px:PXNumberEdit runat="server" DataField="LMRemopossessionAmt" DefaultLocale="" AlreadyLocalized="False" ID="edLMRemopossessionAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalFairValue" DefaultLocale="" AlreadyLocalized="False" ID="edTotalFairValue"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalSalvageValue" DefaultLocale="" AlreadyLocalized="False" ID="edTotalSalvageValue"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="Difference" DefaultLocale="" AlreadyLocalized="False" ID="edDifference"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalDeffectiveAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalDeffectiveAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="RAManagerRemopossessionAmt" DefaultLocale="" AlreadyLocalized="False" ID="edRAManagerRemopossessionAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalLoanAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalLoanAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalPayedPrincipalAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalPayedPrincipalAmt"></px:PXNumberEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
            <px:PXNumberEdit runat="server" DataField="LTV" DefaultLocale="" AlreadyLocalized="False" ID="edLTV"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="LTVWithoutPenalty" DefaultLocale="" AlreadyLocalized="False" ID="edLTVWithoutPenalty"></px:PXNumberEdit>
            <px:PXCheckBox runat="server" Text="Insider" DataField="Insider" AlreadyLocalized="False" ID="edInsider"></px:PXCheckBox>
            <px:PXNumberEdit runat="server" DataField="CapitalSharePct" DefaultLocale="" AlreadyLocalized="False" ID="edCapitalSharePct"></px:PXNumberEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Assets">
                <Template>
                    <px:PXGrid runat="server" TabIndex="3000" Height="500px" SyncPosition="true" ID="grdAssets" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds" SkinID="Details" Width="100%">
                        <Levels>
                            <px:PXGridLevel DataMember="RepossessionApplicationAsset" DataKeyNames="RepossessionApplicationAssetID,RepossessionApplicationID">
                                <Columns>
                                    <px:PXGridColumn DataField="AssetID" LinkCommand="ViewAsset"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__AssetType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Reason"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__PropertyID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Description" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__AssetClassification"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionCuryID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionAmtGel"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__RecommendedRepossessionAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__RecommendedRepossessionAmtGel"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmt"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationSalvageAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationSalvageAmt"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <ActionBar DefaultAction="AddAsset" PagerVisible="False">
                            <CustomItems>
                                <px:PXToolBarButton Text="Add" Key="AddAsset" AlreadyLocalized="False" SuppressHtmlEncoding="False">
                                    <AutoCallBack Command="AddAsset" Target="ds">
                                    </AutoCallBack>
                                    <PopupCommand Command="Refresh" Target="grdAssets">
                                    </PopupCommand>
                                </px:PXToolBarButton>
                                <px:PXToolBarButton Text="Delete" Key="deleteAsset" Visible="False" AlreadyLocalized="False" SuppressHtmlEncoding="False">
                                    <AutoCallBack Command="DeleteAsset" Target="ds">
                                    </AutoCallBack>
                                    <PopupCommand Command="Refresh" Target="grdAssets">
                                    </PopupCommand>
                                </px:PXToolBarButton>
                            </CustomItems>
                        </ActionBar>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Evaluation">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Inquire" Width="100%" ID="PXGrid2" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds">
                        <Levels>
                            <px:PXGridLevel DataMember="Evaluation" DataKeyNames="EvaluationCD">
                                <Columns>
                                    <px:PXGridColumn DataField="EvaluationCD" LinkCommand="ViewEvaluation"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationDate" Width="90px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Appraiser" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="SalvageValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="FairMarketValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="AssetID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EstimationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationApproach"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="InspectDate"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CurrencyDate"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="EvaluationExpenses"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowAddNew="False" AllowDelete="False" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Loan Information">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Inquire" Width="100%" ID="PXGrid2" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds">
                        <Levels>
                            <px:PXGridLevel DataMember="Loan" DataKeyNames="LoanCD">
                                <Columns>
                                    <px:PXGridColumn LinkCommand="ViewLoan" DataField="LoanCD" CommitChanges="True"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LMSID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanProduct" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanSegment" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanEndDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanDisbursementDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanTransferDateToPLO" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__ClientID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerContactInfo" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterestCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenalty" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CurrencyDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsurance" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterest" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CuryID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtGel" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__NBGLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__IFRSLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueDays" Width="200px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowAddNew="False" AllowDelete="False" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            

            <px:PXTabItem Text="Allocation">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" TabIndex="3400" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" SkinID="DetailsInTab" Width="100%" SyncPosition="True" KeepPosition="True">
                        <Levels>
                            <px:PXGridLevel DataMember="Allocation" DataKeyNames="AssetAllocationID">
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" StartRow="True">
                                    </px:PXLayoutRule>
                                    <px:PXSelector ID="edRAAsset__AssetCD" runat="server" DataField="RAAsset__AssetCD">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edRALoan__LoanCD" runat="server" DataField="RALoan__LoanCD">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edRALoan__LMSID" runat="server" AlreadyLocalized="False" DataField="RALoan__LMSID" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXSelector ID="edRALoan__CuryID" runat="server" DataField="RALoan__CuryID">
                                    </px:PXSelector>
                                    <px:PXNumberEdit ID="edRALoan__LoanAmtCury" runat="server" AlreadyLocalized="False" DataField="RALoan__LoanAmtCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverduePrincipleCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverduePrincipleCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverdueInterestCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverdueInterestCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverduePenaltyCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverduePenaltyCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverdueInsuranceCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverdueInsuranceCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPrincipleCury" runat="server" AlreadyLocalized="False" DataField="PaidPrincipleCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInterestCury" runat="server" AlreadyLocalized="False" DataField="PaidInterestCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPenaltyCury" runat="server" AlreadyLocalized="False" DataField="PaidPenaltyCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXLayoutRule runat="server" StartColumn="True">
                                    </px:PXLayoutRule>
                                    <px:PXNumberEdit ID="edPaidInsuranceCury" runat="server" AlreadyLocalized="False" DataField="PaidInsuranceCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidCourtFeeCury" runat="server" AlreadyLocalized="False" DataField="PaidCourtFeeCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPrinciple" runat="server" AlreadyLocalized="False" DataField="PaidPrinciple" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInterest" runat="server" AlreadyLocalized="False" DataField="PaidInterest" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPenalty" runat="server" AlreadyLocalized="False" DataField="PaidPenalty" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInsurance" runat="server" AlreadyLocalized="False" DataField="PaidInsurance" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidCourtFee" runat="server" AlreadyLocalized="False" DataField="PaidCourtFee" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingPrinciple" runat="server" AlreadyLocalized="False" DataField="RemainingPrinciple" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingInterest" runat="server" AlreadyLocalized="False" DataField="RemainingInterest" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingPenalty" runat="server" AlreadyLocalized="False" DataField="RemainingPenalty" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingInsurance" runat="server" AlreadyLocalized="False" DataField="RemainingInsurance" DefaultLocale="">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="RAAsset__AssetCD">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanCD">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LMSID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CuryID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterestCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPrincipleCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInterestCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPenaltyCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInsuranceCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidCourtFeeCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPrinciple" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInterest" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPenalty" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInsurance" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidCourtFee" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingPrinciple" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingInterest" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingPenalty" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingInsurance" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowFormEdit="True" />
                        <ActionBar>
                            <Actions>
                                <AddNew Enabled="False" />
                                <Delete Enabled="False" />
                            </Actions>
                            <CustomItems>
                                <px:PXToolBarButton Text="Fill" Tooltip="Fill">
                                    <AutoCallBack Command="FillAllocation" Target="ds">
                                        <Behavior CommitChanges="True"></Behavior>
                                    </AutoCallBack>
                                </px:PXToolBarButton>
                            </CustomItems>
                        </ActionBar>                        
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Comment" LoadOnDemand="true">
                <Template>
                    <px:PXFormView runat="server" DataMember="RepossessionApplicationCurrent" RenderStyle="Simple" DataSourceID="ds"
                        ID="RepossessionApplicatioID" TabIndex="30700">
                        <Template>
                            <px:PXRichTextEdit ID="edComment" runat="server" DataField="Comment" Style="border-width: 0px; border-top-width: 1px; width: 100%;"
                                AllowAttached="true" AllowSearch="true" AllowLoadTemplate="false" AllowSourceMode="true">
                                <AutoSize Enabled="True" MinHeight="216" />
                                <LoadTemplate TypeName="PX.SM.SMNotificationMaint" DataMember="Notifications" ViewName="NotificationTemplate" ValueField="notificationID" TextField="Name" DataSourceID="ds" Size="M" />
                            </px:PXRichTextEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

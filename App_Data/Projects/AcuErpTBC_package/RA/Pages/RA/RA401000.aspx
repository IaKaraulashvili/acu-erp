<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RA401000.aspx.cs" Inherits="Page_RA401000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="RepossessionApplications" TypeName="RA.RARepossessionApplicationInq">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="Edit"
                DependOnGrid="grid">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="Add" Visible="false">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="ViewDetails" Visible="false" CommitChanges="true">
            </px:PXDSCallbackCommand>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
    <px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100"
        AllowPaging="True" AllowSearch="true" AdjustPageSize="Auto" DataSourceID="ds" SkinID="PrimaryInquire">
        <Levels>
            <px:PXGridLevel DataKeyNames="RepossessionApplicationID" DataMember="RepossessionApplications">
                <RowTemplate>
                </RowTemplate>
                <Columns>
                    <px:PXGridColumn DataField="RepossessionApplicationID" TextAlign="Right"></px:PXGridColumn>
                    <px:PXGridColumn DataField="RepossessionApplicationCD" LinkCommand="RepossessionApplicationCD_ViewDetails"></px:PXGridColumn>
                    <px:PXGridColumn DataField="Description" Width="250px" TextAlign="Center"></px:PXGridColumn>
                    <px:PXGridColumn DataField="RepossessionType" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="Status" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="LoanManager" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="LoanManager_Description" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="ReportsTo" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="ReportsTo_Description" Width="200px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="ReleaseDate" Width="200px" TextAlign="Center"></px:PXGridColumn>
                    <px:PXGridColumn DataField="ApprovalDate" Width="250px" TextAlign="Center"></px:PXGridColumn>
                    <px:PXGridColumn DataField="LMRemopossessionAmt" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="TotalFairValue" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="TotalSalvageValue" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="Difference" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="TotalDeffectiveAmt" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="RAManagerRemopossessionAmt" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="TotalLoanAmt" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="TotalPayedPrincipalAmt" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="LTV" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="LTVWithoutPenalty" TextAlign="Right" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="Insider" Type="CheckBox" Width="100px"></px:PXGridColumn>
                    <px:PXGridColumn DataField="CapitalSharePct" TextAlign="Right" Width="100px"></px:PXGridColumn>
                </Columns>
            </px:PXGridLevel>
        </Levels>
        <AutoSize Container="Window" Enabled="True" MinHeight="200" />
        <ActionBar DefaultAction="Edit" />
    </px:PXGrid>
</asp:Content>

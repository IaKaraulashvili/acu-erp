<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS101000.aspx.cs" Inherits="Page_RS101000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="Setup"
        TypeName="AG.RS.RSSetupMaint" SuspendUnloading="False">
        <CallbackCommands>
            <px:PXDSCallbackCommand CommitChanges="True" Name="Save" />
            <px:PXDSCallbackCommand CommitChanges="True" Name="WaybillTest" Visible="false" />
            <px:PXDSCallbackCommand CommitChanges="True" Name="TaxInvoiceTest" Visible="false" />
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" Width="100%" DataMember="Setup" DataSourceID="ds" TabIndex="2500">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M"
                GroupCaption="Waybill Configuration" />
            <px:PXCheckBox ID="edWaybillIsActive" runat="server" DataField="WaybillIsActive" Text="Active" AlreadyLocalized="False" />
            <px:PXTextEdit ID="edWaybillAccount" runat="server" DataField="WaybillAccount" AlreadyLocalized="False" DefaultLocale="" />
            <px:PXTextEdit ID="edWaybillLicence" runat="server" DataField="WaybillLicence" TextMode="Password" AlreadyLocalized="False" DefaultLocale="" />
            <px:PXTextEdit ID="edWaybillUrl" runat="server" DataField="WaybillUrl" TextMode="MultiLine" Height="60px" Size="M" AlreadyLocalized="False" DefaultLocale="" />
            <px:PXNumberEdit ID="edWaybillTimeout" runat="server" DataField="WaybillTimeout" Size="XXS" AlreadyLocalized="False" DefaultLocale="">
            </px:PXNumberEdit>
            <px:PXCheckBox  ID="edWaybillHoldOnEntry" runat="server" DataField="WaybillHoldOnEntry" Text="Hold Waybill On Entry" AlreadyLocalized="False" />
            <px:PXCheckBox  ID="edWaybillPostOnShipment" runat="server" DataField="WaybillPostOnShipment" Text="Post Automatically Waybill On Shipment" AlreadyLocalized="False" />
             <px:PXCheckBox  ID="edWaybillPostOnTransfer" runat="server" DataField="WaybillPostOnTransfer" Text="Post Automatically Waybill On Transfer" AlreadyLocalized="False" />
            <px:PXSelector ID="edWaybillNumberingID" runat="server" DataField="WaybillNumberingID" AllowEdit="True" edit="1" />
            <px:PXSelector ID="edReceivedWaybillNumberingID" runat="server" DataField="ReceivedWaybillNumberingID" AllowEdit="True" edit="1" />
             <px:PXSelector ID="edWaybillHistoryNumberingID" runat="server" DataField="WaybillHistoryNumberingID" AllowEdit="True" edit="1" />
            <px:PXButton ID="edWaybillTestConnection" runat="server" Text="Test Connection" CommandName="wayBillTest"
                 CommandSourceID="ds" Width="150px" AlreadyLocalized="False" />
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M"
                GroupCaption="Tax Invoice Configuration"/>
            <px:PXCheckBox ID="edTaxInvoiceIsActive" runat="server" DataField="TaxInvoiceIsActive" Text="Active" AlreadyLocalized="False">
            </px:PXCheckBox>
            <px:PXTextEdit ID="edTaxInvoiceAccount" runat="server" DataField="TaxInvoiceAccount" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceLicence" runat="server" DataField="TaxInvoiceLicence" TextMode="Password" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceUri" runat="server" DataField="TaxInvoiceUri" Height="60px" Size="M" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edTaxInvoiceTimeout" runat="server" DataField="TaxInvoiceTimeout" Size="XXS" AlreadyLocalized="False" DefaultLocale="">
            </px:PXNumberEdit>
            <px:PXCheckBox ID="edTaxInvoiceHoldOnEntry" runat="server" DataField="TaxInvoiceHoldOnEntry" Text="Hold Tax Invoice On Entry" AlreadyLocalized="False">
            </px:PXCheckBox>
            <px:PXCheckBox ID="edTaxInvoicePostOnBillAdjustment" runat="server" DataField="TaxInvoicePostOnBillAdjustment" Text="Post Automatically Tax Invoice On Bill&amp;Adjustment" AlreadyLocalized="False">
            </px:PXCheckBox>
            <px:PXSelector ID="edTaxInvoiceRequestNumberingID" runat="server" DataField="TaxInvoiceRequestNumberingID" AllowEdit="True" edit="1">
            </px:PXSelector>
            <px:PXSelector ID="edReceivedTaxInvoiceRequestNumberingID" runat="server" DataField="ReceivedTaxInvoiceRequestNumberingID" AllowEdit="True" edit="1">
            </px:PXSelector>
            <px:PXSelector ID="edTaxInvoiceNumberingID" runat="server" DataField="TaxInvoiceNumberingID" AllowEdit="True" edit="1">
            </px:PXSelector>
            <px:PXSelector ID="edReceivedTaxInvoiceNumberingID" runat="server" DataField="ReceivedTaxInvoiceNumberingID" AllowEdit="True" edit="1">
            </px:PXSelector>

            <px:PXSelector ID="edTaxInvoiceVendorID" runat="server" DataField="TaxInvoiceVendorID">
            </px:PXSelector>

            <px:PXButton ID="edTextInvoiceTestConnection" runat="server" Text="Test Connection" CommandName="taxInvoiceTest"
                CommandSourceID="ds" Width="150px" AlreadyLocalized="False">
            </px:PXButton>
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="210" />
    </px:PXFormView>
</asp:Content>

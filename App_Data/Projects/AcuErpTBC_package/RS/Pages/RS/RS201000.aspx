
<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS201000.aspx.cs" Inherits="Page_RS201000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="BarCodes"
       TypeName="AG.RS.BarCodeMaint" BorderStyle="NotSet" SuspendUnloading="False">
       <CallbackCommands>
		</CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" Width="100%" DataMember="BarCodes" DataSourceID="ds">
        <Template>
           	<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True" ControlSize="XM" LabelsWidth="SM"/>
            <px:PXSelector ID="edBarCode" runat="server" CommitChanges="True" 
					DataField="BarCode" DataSourceID="ds">
                </px:PXSelector>
                <px:PXTextEdit ID="edGoodsName" runat="server" DataField="GoodsName">
            </px:PXTextEdit>
              <px:PXTextEdit ID="edUnitTxt" runat="server" DataField="UnitTxt">
            </px:PXTextEdit>
             <px:PXTextEdit ID="edUnitID" runat="server" DataField="UnitID">
            </px:PXTextEdit>
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="210" />
    </px:PXFormView>
</asp:Content>

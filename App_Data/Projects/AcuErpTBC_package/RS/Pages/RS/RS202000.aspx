<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS202000.aspx.cs" Inherits="Page_RS202000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="ErrorCodes"
       TypeName="AG.RS.RSErrorCodeMaint" BorderStyle="NotSet" SuspendUnloading="False">
       <CallbackCommands>
		</CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="ErrorCodes">
		<Template>
               	<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True" ControlSize="XM" LabelsWidth="S"/>
            <px:PXSelector ID="edID" runat="server" CommitChanges="True" 
					DataField="ID" DataSourceID="ds">
                </px:PXSelector>
                <px:PXTextEdit ID="edName" runat="server" DataField="Name">
            </px:PXTextEdit>
              <px:PXTextEdit ID="edType" runat="server" DataField="Type">
            </px:PXTextEdit>
		</Template>
		<AutoSize Container="Window" Enabled="True" MinHeight="210" />
	</px:PXFormView>
</asp:Content>

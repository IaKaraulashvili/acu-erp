<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS204000.aspx.cs" Inherits="Page_RS204000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" 
        PrimaryView="Mapper" TypeName="AG.RS.RSUnitMapperMaint" >
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100"
		AllowPaging="True" AllowSearch="True" AdjustPageSize="Auto" DataSourceID="ds" 
        SkinID="Primary" TabIndex="100">
		<Levels>
			<px:PXGridLevel DataKeyNames="Unit" DataMember="Mapper">
                 <RowTemplate>
                    <px:PXSelector ID="edUnit" runat="server" DataField="Unit">
                    </px:PXSelector>
                    <px:PXSelector ID="edRSUOMID" runat="server" DataField="RSUOMID">
                    </px:PXSelector>
                </RowTemplate>
                <Columns>
                    <px:PXGridColumn DataField="Unit" Width="200px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="RSUOMID" Width="200px">
                    </px:PXGridColumn>
                </Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXGrid>
</asp:Content>

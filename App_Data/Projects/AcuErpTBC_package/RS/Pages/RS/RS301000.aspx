<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS301000.aspx.cs" Inherits="Page_RS301000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Waybills" SuspendUnloading="False" TypeName="AG.RS.WaybillEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="getDriverName" PostData="Self" Visible="False" />
            <px:PXDSCallbackCommand Name="viewInventory" Visible="False" DependOnGrid="grid" />
            <px:PXDSCallbackCommand Name="viewHistory" Visible="False" DependOnGrid="grid2" />
            <px:PXDSCallbackCommand Name="viewSource" Visible="False" />
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" AllowCollapse="True" MinHeight="86" Width="100%" DataMember="Waybills" TabIndex="11700">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" />
            <px:PXFormView runat="server" ID="fvMain" DataMember="Waybills" Caption="Main Information" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="100">
                <Template>
                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXSelector ID="edWaybillNbr" runat="server" DataField="WaybillNbr" AutoRefresh="True" DataSourceID="ds">
                        <GridProperties FastFilterFields="WaybillType, TransportationType, Status, WaybillNumber, WaybillState, WaybillStatus, WaybillID" />
                    </px:PXSelector>
                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType" CommitChanges="True" />
                    <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False" />
                    <px:PXCheckBox CommitChanges="True" ID="chkHold" runat="server" DataField="Hold" AlreadyLocalized="False" />
                    <px:PXCheckBox ID="edIsCorrected" runat="server" DataField="IsCorrected" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edWaybillCatergory" runat="server" DataField="WaybillCategory" />
                    <px:PXNumberEdit ID="edTotalAmount" runat="server" AlreadyLocalized="False" DataField="TotalAmount" DisplayFormat="n2" Enabled="False" DefaultLocale="" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edWaybillNumber" runat="server" DataField="WaybillNumber" Enabled="False" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edWaybillID" runat="server" DataField="WaybillID" Enabled="False" AlreadyLocalized="False" />
                    <px:PXSelector ID="edLinkedWaybillNbr" runat="server" DataField="LinkedWaybillNbr" DataSourceID="ds" Enabled="false" AllowEdit="true"></px:PXSelector>
                    <px:PXDateTimeEdit ID="edWaybillCreateDate" runat="server" DataField="WaybillCreateDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" DataField="WaybillActivationDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCorrectionDate" runat="server" DataField="WaybillCorrectionDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCloseDate" runat="server" DataField="WaybillCloseDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCancelDate" runat="server" DataField="WaybillCancelDate" Enabled="False" Size="empty" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXDropDown ID="edWaybillStatus" runat="server" DataField="WaybillStatus" Enabled="False" />
                    <px:PXDropDown ID="edWaybillState" runat="server" DataField="WaybillState" Enabled="False" />
                    <px:PXSelector runat="server" DataField="WaybillCreatedByID" ID="edWaybillCreatedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="PostedByID" ID="edPostedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="CorrectedByID" ID="edCorrectedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="ClosedByID" ID="edClosedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="CanceledByID" ID="edCanceledByID"></px:PXSelector>
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvSupplier" DataMember="Waybills" Caption="Supplier (Sender)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="200">
                <Template>
                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXSelector ID="edBranchID" runat="server" DataField="BranchID" CommitChanges="True" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierTaxRegistrationID" runat="server" DataField="SupplierTaxRegistrationID" Enabled="False" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edTransportationType" runat="server" DataField="TransportationType" CommitChanges="True" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierName" runat="server" DataField="SupplierName" Enabled="False" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edOtherTransportationTypeDescription" runat="server" DataField="OtherTransportationTypeDescription" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSourceAddress" runat="server" DataField="SourceAddress" CommitChanges="True" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edSupplierDestinationAddress" runat="server" DataField="SupplierDestinationAddress" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartRow="True" Merge="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverUID" runat="server" DataField="DriverUID" CommitChanges="True" Width="100px" AlreadyLocalized="False" />
                    <px:PXButton ID="edGetDriverName" runat="server" CommandName="GetDriverName" CommandSourceID="ds" AlreadyLocalized="False" Text="edGetDriverName" />
                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" />
                    <px:PXCheckBox ID="edDriverIsForeignCitizen" runat="server" DataField="DriverIsForeignCitizen" CommitChanges="True" AlreadyLocalized="False" Size="M" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverName" runat="server" DataField="DriverName" AlreadyLocalized="False" />
                    <px:PXNumberEdit ID="edShippingCost" runat="server" DataField="ShippingCost" DisplayFormat="#,##0.00" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarRegistrationNumber" runat="server" DataField="CarRegistrationNumber" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edShippingCostPayer" runat="server" DataField="ShippingCostPayer" />
                    <px:PXTextEdit ID="edTrailer" runat="server" DataField="Trailer" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierTaxRegistrationID" runat="server" DataField="CarrierTaxRegistrationID" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierInfo" runat="server" DataField="CarrierInfo" AlreadyLocalized="False" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvBuyer" DataMember="Waybills" Caption="Buyer (Receiver)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="300">
                <Template>
                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXSegmentMask CommitChanges="True" ID="edCustomerID" runat="server" DataField="CustomerID" />
                    <px:PXLayoutRule runat="server" ControlSize="M" StartRow="True" />
                    <px:PXTextEdit ID="edRecipientTaxRegistrationID" runat="server" DataField="RecipientTaxRegistrationID" Enabled="False" CommitChanges="True" AlreadyLocalized="False" />
                    <px:PXCheckBox ID="edRecipientIsForeignCitizen" runat="server" DataField="RecipientIsForeignCitizen" AlreadyLocalized="False" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientName" runat="server" DataField="RecipientName" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientDestinationAddress" runat="server" DataField="RecipientDestinationAddress" CommitChanges="True" AlreadyLocalized="False" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvOther" DataMember="Waybills" Caption="Other" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="400">
                <Template>
                    <px:PXLayoutRule runat="server" Merge="True" StartColumn="True" />
                    <px:PXDateTimeEdit ID="edTransStartDate" runat="server" DataField="TransStartDate" CommitChanges="True" AlreadyLocalized="False" Size="Empty" Width="98px" />
                    <px:PXDateTimeEdit ID="edTransStartTime" runat="server" DataField="TransStartTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierInfo" runat="server" DataField="SupplierInfo" AlreadyLocalized="False" />
                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXTextEdit ID="edRecipientInfo" runat="server" DataField="RecipientInfo" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" Merge="True" />
                    <px:PXDateTimeEdit ID="edDeliveryDate" runat="server" DataField="DeliveryDate" CommitChanges="True" AlreadyLocalized="False" Size="Empty" Width="98px" />
                    <px:PXDateTimeEdit ID="edDeliveryTime" runat="server" DataField="DeliveryTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ColumnSpan="3" />
                    <px:PXTextEdit ID="edComment" runat="server" DataField="Comment" TextMode="MultiLine" AlreadyLocalized="False" />
                </Template>
            </px:PXFormView>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="grid" runat="server" DataSourceID="ds" SkinID="DetailsInTab" Width="100%" SyncPosition="True" KeepPosition="True" TabIndex="2700" TemporaryFilterCaption="Filter Applied">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataKeyNames="WaybillNbr,WaybillItemNbr" DataMember="WaybillItems">
                                <RowTemplate>
                                    <px:PXDropDown ID="edWaybillItemType" runat="server" DataField="WaybillItemType" CommitChanges="True" />
                                    <px:PXSegmentMask ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="True">
                                    </px:PXSegmentMask>
                                    <px:PXSelector ID="edFixedAssetID" runat="server" DataField="FixedAssetID" CommitChanges="True" AutoRefresh="True" />
                                    <px:PXSelector ID="edEAAssetID" runat="server" DataField="EAAssetID" CommitChanges="True" AutoRefresh="True" />
                                    <px:PXTextEdit ID="edItemName" runat="server" DataField="ItemName" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXTextEdit ID="edItemCode" runat="server" DataField="ItemCode" AlreadyLocalized="False" CommitChanges="True" DefaultLocale="" />
                                    <px:PXDropDown ID="edExtUOM" runat="server" DataField="ExtUOM" CommitChanges="True" />
                                    <px:PXTextEdit ID="edOtherUOMDescription" runat="server" DataField="OtherUOMDescription" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXNumberEdit ID="edItemQty" runat="server" DataField="ItemQty" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXNumberEdit ID="edItemExtraQty" runat="server" DataField="ItemExtraQty" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXNumberEdit ID="edUnitPrice" runat="server" DataField="UnitPrice" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXNumberEdit ID="edItemAmt" runat="server" DataField="ItemAmt" AlreadyLocalized="False" DefaultLocale="" />
                                    <px:PXDropDown ID="edTaxType" runat="server" DataField="TaxType" />
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillItemType" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="true" Width="100px" />
                                    <px:PXGridColumn DataField="FixedAssetID" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="EAAssetID" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemCode" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemName" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ExtUOM" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="OtherUOMDescription" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemQty" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="ItemExtraQty" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="UnitPrice" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemAmt" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="TaxType" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybill Source">
                <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" DataSourceID="ds" DataMember="Waybills" SkinID="Transparent">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                            <px:PXTextEdit ID="edSourceNbr" runat="server" DataField="SourceNbr" Enabled="False" AlreadyLocalized="False">
                                <LinkCommand Target="ds" Command="ViewSource"></LinkCommand>
                            </px:PXTextEdit>
                            <px:PXDropDown ID="edSourceType" runat="server" DataField="SourceType" Enabled="False" />
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Correction History">
                <Template>
                    <px:PXGrid ID="grid2" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%" SyncPosition="True">
                        <Levels>
                            <px:PXGridLevel DataMember="HistoryItems" DataKeyNames="ClassID,WaybillNbr,WaybillHistoryNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edWaybillHistoryNbr" runat="server" DataField="WaybillHistoryNbr">
                                    </px:PXSelector>
                                    <px:PXDropDown ID="edStatus" runat="server" DataField="Status" CommitChanges="True" Enabled="False" />
                                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType">
                                    </px:PXDropDown>
                                    <px:PXTextEdit ID="edWaybillID" runat="server" AlreadyLocalized="False" DataField="WaybillID">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edWaybillNumber" runat="server" AlreadyLocalized="False" DataField="WaybillNumber" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edWaybillState" runat="server" DataField="WaybillState">
                                    </px:PXDropDown>
                                    <px:PXDropDown ID="edWaybillStatus" runat="server" DataField="WaybillStatus">
                                    </px:PXDropDown>
                                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" AlreadyLocalized="False" DataField="WaybillActivationDate" DefaultLocale="">
                                    </px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit runat="server" DataField="WaybillCorrectionDate" AlreadyLocalized="False" ID="edWaybillCorrectionDate" Enabled="False"></px:PXDateTimeEdit>
                                    <px:PXSelector ID="edCorrectedByID" runat="server" DataField="CorrectedByID">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edSourceAddress" runat="server" AlreadyLocalized="False" DataField="SourceAddress">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDestinationAddress" runat="server" AlreadyLocalized="False" DataField="DestinationAddress">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDriverName" runat="server" AlreadyLocalized="False" DataField="DriverName">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDriverUID" runat="server" AlreadyLocalized="False" DataField="DriverUID">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edCarRegistrationNumber" runat="server" AlreadyLocalized="False" DataField="CarRegistrationNumber">
                                    </px:PXTextEdit>
                                    <px:PXNumberEdit ID="edTotalAmount" runat="server" AlreadyLocalized="False" DataField="TotalAmount" DefaultLocale="">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillHistoryNbr" LinkCommand="viewHistory" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillType" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillID" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillNumber" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillState" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillStatus" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillActivationDate" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillCorrectionDate" Width="140px" DisplayFormat="g">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="CorrectedByID" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="SourceAddress" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DestinationAddress" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DriverUID" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DriverName" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="CarRegistrationNumber" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TotalAmount" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
    </px:PXTab>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RS302000.aspx.cs" Inherits="Page_RS302000"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Waybills" SuspendUnloading="False" TypeName="AG.RS.ReceivedWaybillEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="viewHistory" Visible="False" DependOnGrid="grid2" />
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Waybills" TabIndex="11700"  >
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" />
            <px:PXFormView runat="server" ID="fvMain" DataMember="Waybills" Caption="Main Information" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="100">
                <Template>
                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXSelector ID="edWaybillNbr" runat="server" DataField="WaybillNbr">
                        <GridProperties FastFilterFields="WaybillType, TransportationType, Status, WaybillNumber, WaybillState, WaybillStatus, WaybillID" />
                    </px:PXSelector>
                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType" CommitChanges="True" />
                    <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False" />
                    <px:PXCheckBox CommitChanges="True" ID="chkHold" runat="server" DataField="Hold" Enabled="False" AlreadyLocalized="False" />
                    <px:PXCheckBox ID="edIsCorrected" runat="server" DataField="IsCorrected" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edWaybillCatergory" runat="server" DataField="WaybillCategory" Enabled="False" />
                    <px:PXNumberEdit ID="edTotalAmount" runat="server" AlreadyLocalized="False" DataField="TotalAmount" DisplayFormat="n2" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edWaybillNumber" runat="server" DataField="WaybillNumber" Enabled="False" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edWaybillID" runat="server" DataField="WaybillID" Enabled="False" AlreadyLocalized="False" />
                    <px:PXSelector ID="edLinkedWaybillNbr" runat="server" DataField="LinkedWaybillNbr" DataSourceID="ds" Enabled="false" AllowEdit="true"></px:PXSelector>
                    <px:PXDateTimeEdit ID="edWaybillCreateDate" runat="server" DataField="WaybillCreateDate" Enabled="False" Size="Empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" DataField="WaybillActivationDate" Enabled="False" Size="Empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCorrectionDate" runat="server" DataField="WaybillCorrectionDate" Enabled="False" Size="Empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCloseDate" runat="server" DataField="WaybillCloseDate" Enabled="False" Size="Empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCancelDate" runat="server" DataField="WaybillCancelDate" Enabled="False" Size="Empty" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXDropDown ID="edWaybillStatus" runat="server" DataField="WaybillStatus" Enabled="False" />
                    <px:PXDropDown ID="edWaybillState" runat="server" DataField="WaybillState" Enabled="False" />
                    <px:PXDateTimeEdit ID="edAcceptedDate" runat="server" DataField="AcceptedDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXSelector runat="server" DataField="AcceptedByID" ID="edAcceptedByID"></px:PXSelector>
                    <px:PXDateTimeEdit ID="edRejetedDate" runat="server" DataField="RejectedDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXSelector runat="server" DataField="RejectedByID" ID="edRejectedByID"></px:PXSelector>
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvSupplier" DataMember="Waybills" Caption="Supplier (Sender)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="200">
                <Template>
                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXSelector ID="edVendorID" runat="server" DataField="VendorID" AllowEdit="True" AllowAddNew="True" CommitChanges="True" AutoRefresh="True" edit="1">
                    </px:PXSelector>

                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" StartRow="True" />
                    <px:PXTextEdit ID="edSupplierTaxRegistrationID" runat="server" DataField="SupplierTaxRegistrationID" Enabled="False" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edTransportationType" runat="server" DataField="TransportationType" CommitChanges="True" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierName" runat="server" DataField="SupplierName" Enabled="False" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edOtherTransportationTypeDescription" runat="server" DataField="OtherTransportationTypeDescription" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSourceAddress" runat="server" DataField="SourceAddress" Enabled="False" AlreadyLocalized="False" />
                    <px:PXTextEdit ID="edSupplierDestinationAddress" runat="server" DataField="SupplierDestinationAddress" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverUID" runat="server" DataField="DriverUID" CommitChanges="True" Enabled="False" AlreadyLocalized="False" />
                    <px:PXCheckBox ID="edDriverIsForeignCitizen" runat="server" DataField="DriverIsForeignCitizen" CommitChanges="True" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverName" runat="server" DataField="DriverName" Enabled="False" AlreadyLocalized="False" />
                    <px:PXNumberEdit ID="edShippingCost" runat="server" DataField="ShippingCost" DisplayFormat="#,##0.00" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarRegistrationNumber" runat="server" DataField="CarRegistrationNumber" Enabled="False" AlreadyLocalized="False" />
                    <px:PXDropDown ID="edShippingCostPayer" runat="server" DataField="ShippingCostPayer" Enabled="False" />
                    <px:PXTextEdit ID="edTrailer" runat="server" DataField="Trailer" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierTaxRegistrationID" runat="server" DataField="CarrierTaxRegistrationID" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierInfo" runat="server" DataField="CarrierInfo" Enabled="False" AlreadyLocalized="False" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvBuyer" DataMember="Waybills" Caption="Buyer (Receiver)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="300">
                <Template>
                    <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" StartColumn="True" />
                    <px:PXTextEdit ID="edRecipientTaxRegistrationID" runat="server" DataField="RecipientTaxRegistrationID" Enabled="False" CommitChanges="True" AlreadyLocalized="False" />
                    <px:PXCheckBox ID="edRecipientIsForeignCitizen" runat="server" DataField="RecipientIsForeignCitizen" AlreadyLocalized="False" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientName" runat="server" DataField="RecipientName" Enabled="False" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientDestinationAddress" runat="server" DataField="RecipientDestinationAddress" CommitChanges="True" AlreadyLocalized="False" Enabled="False" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvOther" DataMember="Waybills" Caption="Other" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="400">
                <Template>
                    <px:PXLayoutRule runat="server" Merge="True" StartColumn="True" />
                    <px:PXDateTimeEdit ID="edTransStartDate" runat="server" DataField="TransStartDate" CommitChanges="True" AlreadyLocalized="False" Size="Empty" Width="98px" Enabled="False" DefaultLocale="" />
                    <px:PXDateTimeEdit ID="edTransStartTime" runat="server" DataField="TransStartTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" Enabled="False" DefaultLocale="" />

                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" StartRow="True" />
                    <px:PXTextEdit ID="edSupplierInfo" runat="server" AlreadyLocalized="False" DataField="SupplierInfo" />

                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXTextEdit ID="edRecipientInfo" runat="server" AlreadyLocalized="False" DataField="RecipientInfo" />

                    <px:PXLayoutRule runat="server" StartColumn="True" Merge="True" />
                    <px:PXDateTimeEdit ID="edDeliveryDate" runat="server" DataField="DeliveryDate" CommitChanges="True" AlreadyLocalized="False" Size="Empty" Width="98px" DefaultLocale="" />
                    <px:PXDateTimeEdit ID="edDeliveryTime" runat="server" DataField="DeliveryTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" DefaultLocale="" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ColumnSpan="3" />
                    <px:PXTextEdit ID="edComment" runat="server" DataField="Comment" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="" />
                </Template>
            </px:PXFormView>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" DataSourceID="ds" Width="100%">
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="grid" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%" SyncPosition="True">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="WaybillNbr,WaybillItemNbr" DataMember="WaybillItems">
                                <RowTemplate>
                                    <px:PXDropDown ID="edWaybillItemType" runat="server" DataField="WaybillItemType" CommitChanges="True" />
                                    <px:PXSegmentMask ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="True">
                                    </px:PXSegmentMask>
                                    <px:PXSelector ID="edFixedAssetID" runat="server" DataField="FixedAssetID" CommitChanges="True" AutoRefresh="True" />
                                    <px:PXSelector ID="edEAAssetID" runat="server" DataField="EAAssetID" CommitChanges="True" AutoRefresh="True" />
                                    <px:PXTextEdit ID="edItemCode" runat="server" DataField="ItemCode" />
                                    <px:PXTextEdit ID="edItemName" runat="server" DataField="ItemName" />
                                    <px:PXDropDown ID="edExtUOM" runat="server" DataField="ExtUOM" CommitChanges="true" />
                                    <px:PXTextEdit ID="edOtherUOMDescription" runat="server" DataField="OtherUOMDescription" TextMode="MultiLine" />
                                    <px:PXNumberEdit ID="edItemQty" runat="server" DataField="ItemQty" CommitChanges="true" />
                                    <px:PXNumberEdit ID="edItemExtraQty" runat="server" DataField="ItemExtraQty" CommitChanges="true" />
                                    <px:PXNumberEdit ID="edUnitPrice" runat="server" DataField="UnitPrice" CommitChanges="true" />
                                    <px:PXNumberEdit ID="edItemAmt" runat="server" DataField="ItemAmt" />
                                    <px:PXDropDown ID="edTaxType" runat="server" DataField="TaxType" />
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillItemType" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="FixedAssetID" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="EAAssetID" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemCode" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemName" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ExtUOM" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="OtherUOMDescription" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemQty" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="ItemExtraQty" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="UnitPrice" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemAmt" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="TaxType" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Purchase Receipt">
                <Template>
                    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100"
                        Width="100%" DataMember="WaybillDetails">
                        <Template>
                            <px:PXLayoutRule ID="PXLayoutRule19" runat="server" StartRow="true" />
                            <px:PXSelector ID="edPOReceiptNbr" runat="server" DataField="POReceiptNbr" CommitChanges="true" />
                        </Template>
                    </px:PXFormView>
                    <px:PXGrid ID="poreceipt" runat="server" DataSourceID="ds" SkinID="Attributes" Width="100%" Caption="Details">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="WaybillNbr" DataMember="POReceipt">
                                <RowTemplate>
                                    <px:PXSelector ID="edPOReceiptNbr" runat="server" DataField="POReceiptNbr" CommitChanges="true" />
                                    <px:PXTextEdit ID="edPOReceipt__ReceiptType" runat="server" DataField="POReceipt__ReceiptType" />
                                    <px:PXTextEdit ID="edPOReceipt__CuryOrderTotal" runat="server" DataField="POReceipt__CuryOrderTotal" />
                                    <px:PXDropDown ID="edPOReceipt__OrderQty" runat="server" DataField="POReceipt__OrderQty" />
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="POReceiptNbr" CommitChanges="true" LinkCommand="ViewPOReceipt" />
                                    <px:PXGridColumn DataField="POReceipt__ReceiptType" />
                                    <px:PXGridColumn DataField="POReceipt__CuryOrderTotal" />
                                    <px:PXGridColumn DataField="POReceipt__OrderQty" />
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Correction History">
                <Template>
                    <px:PXGrid ID="grid2" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%" SyncPosition="True">
                        <Levels>
                            <px:PXGridLevel DataMember="HistoryItems" DataKeyNames="ClassID,WaybillNbr,WaybillHistoryNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edWaybillHistoryNbr" runat="server" DataField="WaybillHistoryNbr">
                                    </px:PXSelector>
                                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType">
                                    </px:PXDropDown>
                                    <px:PXTextEdit ID="edWaybillID" runat="server" AlreadyLocalized="False" DataField="WaybillID">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edWaybillNumber" runat="server" AlreadyLocalized="False" DataField="WaybillNumber" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edWaybillState" runat="server" DataField="WaybillState">
                                    </px:PXDropDown>
                                    <px:PXDropDown ID="edWaybillStatus" runat="server" DataField="WaybillStatus">
                                    </px:PXDropDown>
                                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" AlreadyLocalized="False" DataField="WaybillActivationDate" DefaultLocale="">
                                    </px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit runat="server" DataField="WaybillCorrectionDate" AlreadyLocalized="False" ID="edWaybillCorrectionDate" Enabled="False"></px:PXDateTimeEdit>
                                    <px:PXTextEdit ID="edSourceAddress" runat="server" AlreadyLocalized="False" DataField="SourceAddress">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDestinationAddress" runat="server" AlreadyLocalized="False" DataField="DestinationAddress">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDriverName" runat="server" AlreadyLocalized="False" DataField="DriverName">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edDriverUID" runat="server" AlreadyLocalized="False" DataField="DriverUID">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edCarRegistrationNumber" runat="server" AlreadyLocalized="False" DataField="CarRegistrationNumber">
                                    </px:PXTextEdit>
                                    <px:PXNumberEdit ID="edTotalAmount" runat="server" AlreadyLocalized="False" DataField="TotalAmount" DefaultLocale="">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillHistoryNbr" LinkCommand="viewHistory" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillType" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillID" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillNumber" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillState" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillStatus" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillActivationDate" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillCorrectionDate" Width="90px" DisplayFormat="g">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="SourceAddress" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DestinationAddress" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DriverUID" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="DriverName" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="CarRegistrationNumber" TextAlign="Left" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TotalAmount" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
    </px:PXTab>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RS303000.aspx.cs" Inherits="Page_RS303000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="ReceivedTaxInvoiceRequest" TypeName="AG.RS.ReceivedTaxInvoiceRequestEntry">
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="ReceivedTaxInvoiceRequest" TabIndex="1400">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="M" StartColumn="True" />
            <px:PXSelector ID="edTaxInvoiceNbr" runat="server" DataField="TaxInvoiceNbr" CommitChanges="True">
            </px:PXSelector>
            <px:PXSelector ID="edPostPeriod" runat="server" DataField="PostPeriod" CommitChanges="True">
            </px:PXSelector>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False"></px:PXDropDown>
            <px:PXCheckBox ID="edHold" runat="server" DataField="Hold" Text="Hold" CommitChanges="True"></px:PXCheckBox>
            <px:PXLayoutRule runat="server" ControlSize="M" LabelsWidth="M" StartColumn="True" />
            <px:PXDateTimeEdit ID="edCreateDate" runat="server" DataField="CreateDate" Enabled="False">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edRequestDate" runat="server" DataField="RequestDate" Enabled="False">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edViewDate" runat="server" DataField="ViewDate" Enabled="False">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edConfirmDate" runat="server" DataField="ConfirmDate" Enabled="False">
            </px:PXDateTimeEdit>
            <px:PXTextEdit ID="edTaxInvoiceNumber" runat="server" DataField="TaxInvoiceNumber">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="M" />
            <px:PXSelector ID="edCustomerID" runat="server" DataField="CustomerID" CommitChanges="True">
            </px:PXSelector>
            <px:PXTextEdit ID="edCustomerTaxRegistrationID" runat="server" DataField="CustomerTaxRegistrationID" Enabled="False">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edCustomerName" runat="server" DataField="CustomerName" Enabled="False">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" ControlSize="M" LabelsWidth="M" StartColumn="True" />
            <px:PXSelector ID="edBranchID" runat="server" DataField="BranchID" CommitChanges="True">
            </px:PXSelector>
            <px:PXTextEdit ID="edBranchTaxRegistrationID" runat="server" DataField="BranchTaxRegistrationID" Enabled="False">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edBranchName" runat="server" DataField="BranchName" Enabled="False">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" LabelsWidth="M" ColumnSpan="2" Merge="True" StartRow="True" />
            <px:PXTextEdit ID="edSenderNote" runat="server" DataField="SenderNote" TextMode="MultiLine" Height="60px">
            </px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" DataMember="InvoiceAndMemos">
        <Items>
            <px:PXTabItem Text="Invoice and Memos">
			    <Template>
                    <px:PXGrid ID="PXGrid1" runat="server" TabIndex="13200" DataSourceID="ds" SkinID="Details" Width="100%" ActionPosition="Top" Height="300px">
                        <Levels>
                            <px:PXGridLevel DataMember="InvoiceAndMemos">
                                <RowTemplate>
                                    <px:PXSelector ID="edInvoiceNbr" runat="server" DataField="InvoiceNbr" CommitChanges="True" AutoRefresh="true">
                                    </px:PXSelector>                                    
                                    <px:PXSelector ID="edARInvoice__BatchNbr" runat="server" DataField="ARInvoice__BatchNbr">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edARInvoice__BranchID" runat="server" DataField="ARInvoice__BranchID">
                                    </px:PXTextEdit>
                                    <px:PXNumberEdit ID="edARInvoice__CuryTaxTotal" runat="server" DataField="ARInvoice__CuryTaxTotal">
                                    </px:PXNumberEdit>
                                    <px:PXTextEdit ID="edDueDate" runat="server" DataField="ARInvoice__DueDate" Enabled="False"/>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="InvoiceNbr" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ARInvoice__BatchNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__DueDate" />                                    
                                    <px:PXGridColumn DataField="ARInvoice__BranchID" Width="120px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__CuryTaxTotal" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
			</px:PXTabItem>            
             <px:PXTabItem Text="Waybill">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" DataSourceID="ds" TabIndex="10010" Width="100%"  Height="300px" SkinID="Inquire">
                        <Levels>
                            <px:PXGridLevel DataMember="Waybills">
                                <RowTemplate>
                                    <px:PXSelector ID="edWaybillNbr" runat="server" DataField="WaybillNbr">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edWaybillNumber" runat="server" DataField="WaybillNumber">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType">
                                    </px:PXDropDown>
                                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" DataField="WaybillActivationDate">
                                    </px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit ID="edTransportationStartDateTime" runat="server" DataField="TransportationStartDateTime">
                                    </px:PXDateTimeEdit>
                                    <px:PXNumberEdit ID="edTotalAmount" runat="server" DataField="TotalAmount">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillNumber" Width="80px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillType" TextAlign="Right">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="WaybillActivationDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TransportationStartDateTime" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TotalAmount" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

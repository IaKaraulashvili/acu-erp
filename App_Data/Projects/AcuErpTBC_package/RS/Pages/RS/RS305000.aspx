<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RS305000.aspx.cs" Inherits="Page_RS305000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="TaxInvoices" TypeName="AG.RS.TaxInvoiceEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="addARInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="addARInvoice2" Visible="False">
            </px:PXDSCallbackCommand>

            <px:PXDSCallbackCommand Name="ViewPrevTaxInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="ViewNextTaxInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="PrepaymentTaxInvoiceAction"
                Visible="false" CommitChanges="true" RepaintControlsIDs="PXGrid1" />

        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="TaxInvoices" TabIndex="2600">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ColumnWidth="XM" />
            <px:PXSelector ID="edTaxInvoiceNbr" runat="server" CommitChanges="True" DataField="TaxInvoiceNbr">
            </px:PXSelector>
            <px:PXSelector ID="edPostPeriod" runat="server" DataField="PostPeriod" CommitChanges="True">
            </px:PXSelector>
            <px:PXCheckBox ID="edPrepaymentTaxInvoice" runat="server" AlreadyLocalized="False" CommitChanges="True" DataField="PrepaymentTaxInvoice" Text="Prepayment Tax Invoice">
                <AutoCallBack Command="PrepaymentTaxInvoiceAction" Target="ds">
                    <Behavior CommitChanges="True" RepaintControlsIDs="PXGrid1" />
                </AutoCallBack>

            </px:PXCheckBox>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False">
            </px:PXDropDown>

            <px:PXTextEdit ID="edCorrectedRefNbr" runat="server" AlreadyLocalized="False" DataField="CorrectedRefNbr" Enabled="False" DefaultLocale="">
                <LinkCommand Command="ViewPrevTaxInvoice" Target="ds">
                </LinkCommand>
            </px:PXTextEdit>
            <px:PXTextEdit ID="edRSTaxInvoiceCorrected__TaxInvoiceNbr" runat="server" AlreadyLocalized="False" DataField="RSTaxInvoiceCorrected__TaxInvoiceNbr" Enabled="False" DefaultLocale="">
                <LinkCommand Command="ViewNextTaxInvoice" Target="ds">
                </LinkCommand>
            </px:PXTextEdit>

            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM" ControlSize="M">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edTaxInvoiceNumber" runat="server" AlreadyLocalized="False" DataField="TaxInvoiceNumber" Enabled="False">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceID" runat="server" DataField="TaxInvoiceID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edDeclarationNumber" runat="server" DataField="DeclarationNumber" AlreadyLocalized="False" Enabled="False">
            </px:PXTextEdit>
            <px:PXDateTimeEdit ID="edRegistrationDate" runat="server" DataField="RegistrationDate" Enabled="False" AlreadyLocalized="False" DefaultLocale="" Size="M">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edConfirmDate" runat="server" DataField="ConfirmDate" Enabled="False" AlreadyLocalized="False" DefaultLocale="" Size="M">
            </px:PXDateTimeEdit>

            <px:PXDropDown ID="edTaxInvoiceStatus" runat="server" DataField="TaxInvoiceStatus" Enabled="False">
            </px:PXDropDown>
            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM" ControlSize="M">
            </px:PXLayoutRule>
            <px:PXDateTimeEdit ID="edCreateDate" runat="server" DataField="CreateDate" Enabled="False" AlreadyLocalized="False" DefaultLocale="" Size="M">
            </px:PXDateTimeEdit>
            <px:PXTextEdit ID="edCreatedByID_Creator_Username" runat="server" AlreadyLocalized="False" DataField="CreatedByID_Creator_Username">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edFullAmt" runat="server" AlreadyLocalized="False" DataField="FullAmt">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edVATAmt" runat="server" AlreadyLocalized="False" DataField="VATAmt">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edTotalExciseAmt" runat="server" AlreadyLocalized="False" DataField="TotalExciseAmt">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edTotalAmt" runat="server" AlreadyLocalized="False" DataField="TotalAmt">
            </px:PXNumberEdit>

            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXDropDown ID="edCorrectionType" runat="server" DataField="CorrectionType">
            </px:PXDropDown>
            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXDateTimeEdit ID="edCorrectionDate" runat="server" DataField="CorrectionDate" Width="150px" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXDateTimeEdit>

            <px:PXLayoutRule runat="server" StartRow="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="Customer">
            </px:PXLayoutRule>
            <px:PXSelector ID="edCustomerID" runat="server" CommitChanges="True" DataField="CustomerID">
            </px:PXSelector>
            <px:PXTextEdit ID="edCustomerTaxRegID" runat="server" DataField="CustomerTaxRegistrationID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edCustomerName" runat="server" DataField="CustomerName" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="Branch" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXSelector ID="edBranchID" runat="server" CommitChanges="True" DataField="BranchID">
            </px:PXSelector>
            <px:PXTextEdit ID="edBranchTaxRegistrationID" runat="server" DataField="BranchTaxRegistrationID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edBranchName" runat="server" DataField="BranchName" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXLayoutRule runat="server" Merge="True" StartRow="True">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edNote" runat="server" DataField="Note" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
        </Template>
    </px:PXFormView>

    <px:PXSmartPanel ID="spCorrectionTypes" runat="server" Key="CorrectionTypes"
        Caption="Choose correction type" CaptionVisible="True" DesignView="Content" TabIndex="1500" AlreadyLocalized="False" CreateOnDemand="True">

        <px:PXFormView ID="frmCorrType" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="CorrectionTypes"
            CaptionVisible="False" TabIndex="13901">

            <Template>
                <px:PXLayoutRule runat="server" ControlSize="SM" LabelsWidth="S" StartRow="True" />

                <px:PXDropDown ID="edCorrectionType" runat="server" DataField="CorrectionType">
                </px:PXDropDown>

            </Template>




        </px:PXFormView>
        <px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
            <%--<px:PXButton ID="PXButton1" runat="server" CommandName="addARInvoice2" CommandSourceID="ds" Text="Add" SyncVisible="false"></px:PXButton>--%>
            <px:PXButton ID="PXButton2" runat="server" Text="Add & Close" DialogResult="OK"></px:PXButton>
            <px:PXButton ID="PXButton3" runat="server" DialogResult="Cancel" Text="Cancel"></px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>

</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="PXGrid1" runat="server" DataSourceID="ds" SkinID="Details" TabIndex="11900" Width="100%" Height="130px" TemporaryFilterCaption="Filter Applied" RepaintColumns="true">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataMember="TaxInvoiceItems" DataKeyNames="TaxInvoiceNbr,TaxInvoiceItemNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="True">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edItemName" runat="server" DataField="ItemName" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edItemRowStatus" runat="server" DataField="ItemRowStatus">
                                    </px:PXDropDown>
                                    <px:PXTextEdit ID="edItemBarCode" runat="server" DataField="ItemBarCode" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edExtUOM" runat="server" DataField="ExtUOM">
                                    </px:PXDropDown>
                                    <px:PXNumberEdit ID="edItemQty" runat="server" DataField="ItemQty" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edUnitPrice" runat="server" DataField="UnitPrice" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edItemAmt" runat="server" DataField="ItemAmt" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edExciseAmt" runat="server" DataField="ExciseAmt" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edVATAmt" runat="server" DataField="VATAmt" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edFullAmt" runat="server" DataField="FullAmt" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemName">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemRowStatus">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemBarCode">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ExtUOM">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemQty" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="UnitPrice" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemAmt">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ExciseAmt" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TaxType" TextAlign="Right">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="VATAmt" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="FullAmt">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>

                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybills">
                <Template>
                    <px:PXGrid runat="server" ID="PXGrid2" DataSourceID="ds" TabIndex="11200" SkinID="Details" Height="130px" Width="100%" TemporaryFilterCaption="Filter Applied">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataKeyNames="TaxInvoiceNbr,WaybillNbr" DataMember="TaxInvoiceWaybills">
                                <RowTemplate>
                                    <px:PXTextEdit ID="edRSWaybill__WaybillNumber" runat="server" DataField="RSWaybill__WaybillNumber" AlreadyLocalized="False" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDateTimeEdit ID="edRSWaybill__WaybillActivationDate" runat="server" AlreadyLocalized="False" DataField="RSWaybill__WaybillActivationDate" DefaultLocale="">
                                    </px:PXDateTimeEdit>
                                    <px:PXDropDown ID="edRSWaybill__TransportationType" runat="server" DataField="RSWaybill__TransportationType">
                                    </px:PXDropDown>

                                    <px:PXSelector ID="edWaybillNbr" runat="server" DataField="WaybillNbr">
                                    </px:PXSelector>
                                </RowTemplate>
                                <Columns>

                                    <px:PXGridColumn DataField="WaybillNbr" Width="140px" TextAlign="Left" CommitChanges="True">
                                    </px:PXGridColumn>

                                    <px:PXGridColumn DataField="RSWaybill__WaybillNumber" Width="140px" TextAlign="Left">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSWaybill__WaybillActivationDate" Width="140px" TextAlign="Left">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSWaybill__TransportationType" Width="140px" TextAlign="Left">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSWaybill__TransportationStartDateTime" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSWaybill__TotalAmount" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>

                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <%--<px:PXTabItem Text="Invoice and Memos">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" DataSourceID="ds" Height="130px" TabIndex="16500" Width="100%" SkinID="Details">
                        <Levels>
                            <px:PXGridLevel DataKeyNames="TaxInvoiceNbr,InvoiceNbr,taxInvoiceItemNbr" DataMember="InvoiceAndMemos">
                                <RowTemplate>
                                    <px:PXSelector ID="edInvoiceNbr" runat="server" CommitChanges="True" DataField="InvoiceNbr">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edARInvoice__BatchNbr" runat="server" DataField="ARInvoice__BatchNbr">
                                    </px:PXSelector>
                                    <px:PXSegmentMask ID="edARInvoice__BranchID" runat="server" DataField="ARInvoice__BranchID">
                                    </px:PXSegmentMask>
                                    <px:PXDateTimeEdit ID="edARInvoice__DueDate" runat="server" DataField="ARInvoice__DueDate">
                                    </px:PXDateTimeEdit>
                                    <px:PXNumberEdit ID="edARInvoice__CuryTaxTotal" runat="server" DataField="ARInvoice__CuryTaxTotal">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="InvoiceNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__BatchNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__BranchID" Width="120px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__DueDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ARInvoice__CuryTaxTotal" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>--%>
            <px:PXTabItem Text="Invoices and Memos">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" SkinID="Details" Height="130px" TabIndex="-16336" Width="100%">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataMember="InvoiceAndMemos" DataKeyNames="RSTaxInvoiceMemoID">
                                <RowTemplate>
                                    <px:PXSelector ID="edArInvoiceNbr" runat="server" DataField="ArInvoiceNbr">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edShipmentNbr" runat="server" DataField="ShipmentNbr">
                                    </px:PXSelector>
                                    <px:PXDateTimeEdit ID="edShipmentDate" runat="server" AlreadyLocalized="False" DataField="ShipmentDate">
                                    </px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit ID="edInvoiceAndMemoDate" runat="server" AlreadyLocalized="False" DataField="InvoiceAndMemoDate">
                                    </px:PXDateTimeEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="ArInvoiceNbr" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="InvoiceAndMemoDate">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ShipmentNbr" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ShipmentDate">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>

                        <AutoSize Enabled="True"></AutoSize>

                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>


</asp:Content>

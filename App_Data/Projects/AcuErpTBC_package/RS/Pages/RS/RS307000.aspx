<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS307000.aspx.cs" Inherits="Page_RS307000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Waybills" SuspendUnloading="False" TypeName="AG.RS.WaybillHistoryEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="getDriverName" PostData="Self" Visible="False" />
            <px:PXDSCallbackCommand Name="viewInventory" Visible="False" DependOnGrid="grid" />
            <px:PXDSCallbackCommand Name="viewSource" Visible="False" />
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="PXFormView1" runat="server" DataSourceID="ds" Style="z-index: 100" AllowCollapse="True" MinHeight="86" Width="100%" DataMember="Waybills" TabIndex="11700">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" />
            <px:PXFormView runat="server" ID="fvMain" DataMember="Waybills" Caption="Main Information" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="100">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXSelector ID="edWaybillHistoryNbr" runat="server" DataField="WaybillHistoryNbr">
                        <GridProperties FastFilterFields="WaybillType, TransportationType, Status, WaybillNumber, WaybillState, WaybillStatus, WaybillID" />
                    </px:PXSelector>
                    <px:PXDropDown ID="edClassID" runat="server" DataField="ClassID" Enabled="false" />
                    <px:PXMaskEdit ID="edWaybillNbr" runat="server" AlreadyLocalized="False" DataField="WaybillNbr" Enabled="false" />
                    <px:PXDropDown ID="edWaybillType" runat="server" DataField="WaybillType" CommitChanges="True" Enabled="false" />
                    <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False" />
                    <px:PXCheckBox CommitChanges="True" ID="chkHold" runat="server" DataField="Hold" AlreadyLocalized="False" Enabled="false" />
                    <px:PXDropDown ID="edWaybillCatergory" runat="server" DataField="WaybillCategory" Enabled="false" />
                    <px:PXNumberEdit ID="edTotalAmount" runat="server" AlreadyLocalized="False" DataField="TotalAmount" DisplayFormat="n2" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edWaybillNumber" runat="server" DataField="WaybillNumber" Enabled="False" AlreadyLocalized="False" DefaultLocale="" />
                    <px:PXTextEdit ID="edWaybillID" runat="server" DataField="WaybillID" Enabled="False" AlreadyLocalized="False" DefaultLocale="" />
                    <px:PXDateTimeEdit ID="edWaybillCreateDate" runat="server" DataField="WaybillCreateDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillActivationDate" runat="server" DataField="WaybillActivationDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCorrectionDate" runat="server" DataField="WaybillCorrectionDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCloseDate" runat="server" DataField="WaybillCloseDate" Enabled="False" Size="empty" AlreadyLocalized="False" />
                    <px:PXDateTimeEdit ID="edWaybillCancelDate" runat="server" DataField="WaybillCancelDate" Enabled="False" Size="empty" AlreadyLocalized="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXDropDown ID="edWaybillStatus" runat="server" DataField="WaybillStatus" Enabled="False" />
                    <px:PXDropDown ID="edWaybillState" runat="server" DataField="WaybillState" Enabled="False" />
                    <px:PXSelector runat="server" DataField="WaybillCreatedByID" ID="edWaybillCreatedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="PostedByID" ID="edPostedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="CorrectedByID" ID="edCorrectedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="ClosedByID" ID="edClosedByID"></px:PXSelector>
                    <px:PXSelector runat="server" DataField="CanceledByID" ID="edCanceledByID"></px:PXSelector>
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvSupplier" DataMember="Waybills" Caption="Supplier (Sender)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="200">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXSelector ID="edBranchID" runat="server" DataField="BranchID" CommitChanges="True" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierTaxRegistrationID" runat="server" DataField="SupplierTaxRegistrationID" Enabled="False" AlreadyLocalized="False" DefaultLocale="" />
                    <px:PXDropDown ID="edTransportationType" runat="server" DataField="TransportationType" CommitChanges="True" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierName" runat="server" DataField="SupplierName" Enabled="False" AlreadyLocalized="False" DefaultLocale="" />
                    <px:PXTextEdit ID="edOtherTransportationTypeDescription" runat="server" DataField="OtherTransportationTypeDescription" AlreadyLocalized="False" DefaultLocale="" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSourceAddress" runat="server" DataField="SourceAddress" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                    <px:PXTextEdit ID="edSupplierDestinationAddress" runat="server" DataField="SupplierDestinationAddress" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" Merge="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverUID" runat="server" DataField="DriverUID" CommitChanges="True" Width="100px" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="true" />
                    <px:PXCheckBox ID="edDriverIsForeignCitizen" runat="server" DataField="DriverIsForeignCitizen" CommitChanges="True" AlreadyLocalized="False" Size="M" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edDriverName" runat="server" DataField="DriverName" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                    <px:PXNumberEdit ID="edShippingCost" runat="server" DataField="ShippingCost" DisplayFormat="#,##0.00" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarRegistrationNumber" runat="server" DataField="CarRegistrationNumber" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                    <px:PXDropDown ID="edShippingCostPayer" runat="server" DataField="ShippingCostPayer" Enabled="false" />
                    <px:PXTextEdit ID="edTrailer" runat="server" DataField="Trailer" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierTaxRegistrationID" runat="server" DataField="CarrierTaxRegistrationID" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edCarrierInfo" runat="server" DataField="CarrierInfo" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvBuyer" DataMember="Waybills" Caption="Buyer (Receiver)" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="300">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="true" ControlSize="M" />
                    <px:PXSegmentMask CommitChanges="True" ID="edCustomerID" runat="server" DataField="CustomerID" Enabled="false" />

                    <px:PXLayoutRule runat="server" ControlSize="M" StartRow="True" />
                    <px:PXTextEdit ID="edRecipientTaxRegistrationID" runat="server" DataField="RecipientTaxRegistrationID" Enabled="False" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" />
                    <px:PXCheckBox ID="edRecipientIsForeignCitizen" runat="server" DataField="RecipientIsForeignCitizen" AlreadyLocalized="False" Enabled="False" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientName" runat="server" DataField="RecipientName" Enabled="False" AlreadyLocalized="False" DefaultLocale="" />

                    <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edRecipientDestinationAddress" runat="server" DataField="RecipientDestinationAddress" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                </Template>
            </px:PXFormView>

            <px:PXFormView runat="server" ID="fvOther" DataMember="Waybills" Caption="Other" RenderStyle="Fieldset" DataSourceID="ds" TabIndex="400">
                <Template>
                    <px:PXLayoutRule runat="server" Merge="True" />
                    <px:PXDateTimeEdit ID="edTransStartDate" runat="server" DataField="TransStartDate" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Size="Empty" Width="98px" Enabled="false" />
                    <px:PXDateTimeEdit ID="edTransStartTime" runat="server" DataField="TransStartTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                    <px:PXTextEdit ID="edSupplierInfo" runat="server" DataField="SupplierInfo" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                    <px:PXLayoutRule runat="server" ControlSize="M" StartColumn="True" />
                    <px:PXTextEdit ID="edRecipientInfo" runat="server" DataField="RecipientInfo" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartColumn="True" Merge="true" />
                    <px:PXDateTimeEdit ID="edDeliveryDate" runat="server" DataField="DeliveryDate" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Size="Empty" Width="98px" Enabled="false" />
                    <px:PXDateTimeEdit ID="edDeliveryTime" runat="server" DataField="DeliveryTime" CommitChanges="True" TimeMode="True" SuppressLabel="True" AlreadyLocalized="False" Size="Empty" Width="87px" DefaultLocale="" Enabled="false" />

                    <px:PXLayoutRule runat="server" StartRow="True" StartColumn="true" ColumnSpan="3" />
                    <px:PXTextEdit ID="edComment" runat="server" DataField="Comment" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                </Template>
            </px:PXFormView>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="grid" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%">

                        <Levels>
                            <px:PXGridLevel DataKeyNames="WaybillHistoryID,WaybillItemNbr" DataMember="WaybillItems">
                                <RowTemplate>
                                    <px:PXDropDown ID="edWaybillItemType" runat="server" DataField="WaybillItemType" CommitChanges="True" Enabled="false" />
                                    <px:PXSegmentMask ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="True" Enabled="false">
                                    </px:PXSegmentMask>
                                    <px:PXSelector ID="edAssetID" runat="server" DataField="AssetID" CommitChanges="True" AutoRefresh="True" Enabled="false" />
                                    <px:PXSelector ID="edEAAssetID" runat="server" DataField="EAAssetID" CommitChanges="True" AutoRefresh="True" Enabled="false" />
                                    <px:PXTextEdit ID="edItemName" runat="server" DataField="ItemName" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXTextEdit ID="edItemCode" runat="server" DataField="ItemCode" AlreadyLocalized="False" CommitChanges="True" DefaultLocale="" Enabled="false" />
                                    <px:PXDropDown ID="edExtUOM" runat="server" DataField="ExtUOM" CommitChanges="True" Enabled="false" />
                                    <px:PXTextEdit ID="edOtherUOMDescription" runat="server" DataField="OtherUOMDescription" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXNumberEdit ID="edItemQty" runat="server" DataField="ItemQty" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXNumberEdit ID="edItemExtraQty" runat="server" DataField="ItemExtraQty" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXNumberEdit ID="edUnitPrice" runat="server" DataField="UnitPrice" CommitChanges="True" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXNumberEdit ID="edItemAmt" runat="server" DataField="ItemAmt" AlreadyLocalized="False" DefaultLocale="" Enabled="false" />
                                    <px:PXDropDown ID="edTaxType" runat="server" DataField="TaxType" Enabled="false" />
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillItemType" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="FixedAssetID" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="EAAssetID" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemCode" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemName" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ExtUOM" TextAlign="Left" Width="100px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="OtherUOMDescription" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="ItemQty" CommitChanges="True" TextAlign="Left" Width="100px" />
                                    <px:PXGridColumn DataField="ItemExtraQty" CommitChanges="True" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="UnitPrice" TextAlign="Left" Width="140px" CommitChanges="True" />
                                    <px:PXGridColumn DataField="ItemAmt" TextAlign="Left" Width="140px" />
                                    <px:PXGridColumn DataField="TaxType" TextAlign="Left" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybill Source">
                <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" DataSourceID="ds" DataMember="Waybills" SkinID="Transparent">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                            <px:PXTextEdit ID="edSourceNbr" runat="server" DataField="SourceNbr" Enabled="False" AlreadyLocalized="False">
                                <LinkCommand Target="ds" Command="ViewSource"></LinkCommand>
                            </px:PXTextEdit>
                            <px:PXDropDown ID="edSourceType" runat="server" DataField="SourceType" Enabled="False" />
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
        </Items>
    </px:PXTab>
</asp:Content>

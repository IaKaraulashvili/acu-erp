<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS503000.aspx.cs" Inherits="Page_RS503000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" Width="100%" runat="server" Visible="True" PrimaryView="Waybills" TypeName="AG.RS.ExternalWaybillCancel">
		<CallbackCommands>
			<px:PXDSCallbackCommand CommitChanges="True" Name="Process" StartNewGroup="true" />
			<px:PXDSCallbackCommand CommitChanges="True" Name="ProcessAll" />
			<px:PXDSCallbackCommand DependOnGrid="grid" Name="ViewWaybill" Visible="false" />
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phL" runat="Server">
    <px:PXGrid ID="PXGrid1" runat="server" Height="400px" Width="100%" Style="z-index: 100" AllowPaging="true" AdjustPageSize="Auto" AllowSearch="true" DataSourceID="ds" BatchUpdate="True" SkinID="Inquire" Caption="Waybills">
		<Levels>
			<px:PXGridLevel DataMember="Waybills">
				<RowTemplate>
					<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" />
					<px:PXSelector SuppressLabel="True" ID="edRefNbr" runat="server" DataField="WaybillNbr" Enabled="False" AllowEdit="True" />
				</RowTemplate>
				<Columns>
					<px:PXGridColumn DataField="Selected" TextAlign="Center" Type="CheckBox" AllowCheckAll="True" AllowSort="False" AllowMove="False" Width="20px" />
                    <px:PXGridColumn DataField="WaybillNbr"  LinkCommand="ViewWaybill"  />
                    <px:PXGridColumn DataField="WaybillID" />
                    <px:PXGridColumn DataField="ParentWaybillID" />
                    <px:PXGridColumn DataField="WaybillNumber" />
                    <px:PXGridColumn DataField="WaybillType" />
                    <px:PXGridColumn DataField="WaybillStatus" />
                    <px:PXGridColumn DataField="WaybillState" />
                    <px:PXGridColumn DataField="WaybillCreateDate" />
                    <px:PXGridColumn DataField="WaybillActivationDate" />
                    <px:PXGridColumn DataField="WaybillCloseDate" />
                    <px:PXGridColumn DataField="RecipientName" />
                    <px:PXGridColumn DataField="RecipientTaxRegistrationID" />
				</Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
         <ActionBar>
			<CustomItems>
				<px:PXToolBarButton Text="View Waybill">
					<AutoCallBack Command="ViewWaybill" Target="ds">
						<Behavior CommitChanges="True" />
					</AutoCallBack>
				</px:PXToolBarButton>
			</CustomItems>
		</ActionBar>
		<Layout ShowRowStatus="False" />
	</px:PXGrid>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AP301000.aspx.cs" Inherits="Page_AP301000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="PX.Objects.AP.APInvoiceEntry" PrimaryView="Document">
		<CallbackCommands>
			<px:PXDSCallbackCommand Name="Insert" PostData="Self" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand CommitChanges="True" Name="Save" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="First" PostData="Self" StartNewGroup="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Last" PostData="Self" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand StartNewGroup="True" Name="Release" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Prebook" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="VoidInvoice" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand StartNewGroup="True" Name="Action" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Inquiry" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Report" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewTask" Visible="False" CommitChanges="True"></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewEvent" Visible="False" CommitChanges="True"></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewActivity" Visible="False" CommitChanges="True"></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewMailActivity" Visible="False" CommitChanges="True"></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ReverseInvoice" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ReclassifyBatch" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="VendorRefund" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="VoidDocument" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="PayInvoice" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="ViewSchedule" CommitChanges="true" DependOnGrid="grid" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="CreateSchedule" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewBatch" ></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Visible="False" Name="ViewOriginalDocument"></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewVoucherBatch" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewWorkBook" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="NewVendor" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="EditVendor" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="VendorDocuments" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOReceipt2" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddReceiptLine2" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOOrder2" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOReceipt" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddReceiptLine" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOOrder" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="LinkLine" CommitChanges="true" DependOnGrid="grid" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewPODocument" DependOnGrid="grid" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewLCPOReceipt" DependOnGrid="gridLCTran" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewLCINDocument" DependOnGrid="gridLCTran" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AutoApply" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="ViewItem" DependOnGrid="grid" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewPayment" DependOnGrid="detgrid" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="CurrencyView" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPostLandedCostTran" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand CommitChanges="True" Visible="False" Name="LsLCSplits" DependOnGrid="gridLCTran" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="RecalculateDiscountsAction" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="RecalcOk" PopupCommand="" PopupCommandTarget="" PopupPanel="" Text="" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand CommitChanges="True" Name="AddReceivedTaxInvoice" Visible="False" />
			<px:PXDSCallbackCommand CommitChanges="True" Name="RemoveTaxInvoice" Visible="False" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
	<px:PXFormView ID="form" runat="server" Style="z-index: 100" Width="100%"
		DataMember="Document" Caption="Document Summary" NoteIndicator="True"
		FilesIndicator="True" ActivityIndicator="True" ActivityField="NoteActivity"
		LinkIndicator="True" NotifyIndicator="True" DefaultControlID="edDocType"
		TabIndex="100" DataSourceID="ds" MarkRequired="Dynamic">
		<CallbackCommands>
			<Save PostData="Self" ></Save>
		</CallbackCommands>
		<Template>
			<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="S" ></px:PXLayoutRule>
			<px:PXDropDown CommitChanges="True" ID="edDocType" runat="server" DataField="DocType" ></px:PXDropDown>
			    <px:PXSelector ID="edRefNbr" runat="server" DataField="RefNbr" AutoRefresh="True" DataSourceID="ds">
				<GridProperties FastFilterFields="APInvoice__InvoiceNbr, VendorID, VendorID_Vendor_acctName" ></GridProperties>
			</px:PXSelector>
			<px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False" ></px:PXDropDown>
			<px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkHold" runat="server" DataField="Hold" ></px:PXCheckBox>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox7" DataField="UsrFARelated" CommitChanges="True" />
			<px:PXDateTimeEdit CommitChanges="True" ID="edDocDate" runat="server" DataField="DocDate" ></px:PXDateTimeEdit>
			    <px:PXSelector CommitChanges="True" ID="edFinPeriodID" runat="server" DataField="FinPeriodID" DataSourceID="ds" ></px:PXSelector>
			<px:PXTextEdit CommitChanges="True" ID="edInvoiceNbr" runat="server" DataField="InvoiceNbr" ></px:PXTextEdit>

			<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="S" StartColumn="True" ></px:PXLayoutRule>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox2" DataField="UsrVEInsider" />
			    <px:PXSegmentMask CommitChanges="True" ID="edVendorID1" runat="server" DataField="VendorID"
                    AllowAddNew="True" AllowEdit="True" DataSourceID="ds" AutoRefresh="True" ></px:PXSegmentMask>
			<px:PXSelector runat="server" ID="edUsrContractID" DataField="UsrContractID" CommitChanges="True" AutoRefresh="True" />
			<px:PXSegmentMask CommitChanges="True" ID="edVendorLocationID" runat="server"
				AutoRefresh="True" DataField="VendorLocationID" DataSourceID="ds" ></px:PXSegmentMask>
			    <pxa:PXCurrencyRate ID="edCury" DataField="CuryID" runat="server" RateTypeView="_APInvoice_CurrencyInfo_"
                    DataMember="_Currency_" DataSourceID="ds"></pxa:PXCurrencyRate>
			    <px:PXSelector CommitChanges="True" ID="edTermsID" runat="server" DataField="TermsID" DataSourceID="ds" ></px:PXSelector>
			<px:PXDateTimeEdit CommitChanges="True" ID="edDueDate" runat="server" DataField="DueDate" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit CommitChanges="True" ID="edDiscDate" runat="server" DataField="DiscDate" ></px:PXDateTimeEdit>

			<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="S" ></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule4" StartRow="True" LabelsWidth="S" />
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit2" DataField="UsrPrepaymentCloseDate" />
			<px:PXPanel ID="PXPanel1" runat="server" RenderSimple="True" RenderStyle="Simple">
				<px:PXLayoutRule runat="server" StartColumn="True" ></px:PXLayoutRule>
				<px:PXNumberEdit ID="edCuryLineTotal" runat="server" DataField="CuryLineTotal" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit CommitChanges="True" ID="edCuryDiscTot" runat="server" Enabled="False" DataField="CuryDiscTot" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="CuryVatTaxableTotal" runat="server" DataField="CuryVatTaxableTotal" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="CuryVatExemptTotal" runat="server" DataField="CuryVatExemptTotal" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryTaxTotal" runat="server" DataField="CuryTaxTotal" Enabled="False" Size="s" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryOrigWhTaxAmt" runat="server" DataField="CuryOrigWhTaxAmt" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryDocBal" runat="server" DataField="CuryDocBal" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryInitDocBal" runat="server" DataField="CuryInitDocBal" CommitChanges="True" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryRoundDiff" runat="server" CommitChanges="True" DataField="CuryRoundDiff" Enabled="False" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryOrigDocAmt" runat="server" CommitChanges="True" DataField="CuryOrigDocAmt" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryTaxAmt" runat="server" CommitChanges="True" DataField="CuryTaxAmt" ></px:PXNumberEdit>
				<px:PXNumberEdit ID="edCuryOrigDiscAmt" runat="server" CommitChanges="True" DataField="CuryOrigDiscAmt" ></px:PXNumberEdit>
				<px:PXCheckBox ID="chkLCEnabled" runat="server" DataField="LCEnabled" ></px:PXCheckBox>
			</px:PXPanel>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule8" StartRow="True" LabelsWidth="S" />
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule15" StartRow="True" ControlSize="" LabelsWidth="S" />
			<px:PXTextEdit ID="edDocDesc" runat="server" DataField="DocDesc" ></px:PXTextEdit></Template>
	</px:PXFormView>

	<style type="text/css">
		.leftDocTemplateCol {
			width: 50%;
			float: left;
			min-width: 90px;
		}

		.rightDocTemplateCol {
			margin-left: 51%;
			min-width: 90px;
		}
	</style>
	<px:PXGrid ID="docsTemplate" runat="server" Visible="false">
		<Levels>
			<px:PXGridLevel>
				<Columns>
					<px:PXGridColumn Key="Template">
						<CellTemplate>
							<div id="ParentDiv1" class="leftDocTemplateCol">
								<div id="div11" class="Field0"><%# ((PXGridCellContainer)Container).Text("refNbr") %></div>
								<div id="div12" class="Field1"><%# ((PXGridCellContainer)Container).Text("docDate") %></div>
							</div>
							<div id="ParentDiv2" class="rightDocTemplateCol">
								<span id="span21" class="Field1"><%# ((PXGridCellContainer)Container).Text("curyOrigDocAmt") %></span>
								<span id="span22" class="Field1"><%# ((PXGridCellContainer)Container).Text("curyID") %></span>
								<div id="div21" class="Field1"><%# ((PXGridCellContainer)Container).Text("status") %></div>
							</div>
							<div id="div3" class="Field1"><%# ((PXGridCellContainer)Container).Text("vendorID_Vendor_acctName") %></div>
						</CellTemplate>
					</px:PXGridColumn>
				</Columns>
			</px:PXGridLevel>
		</Levels>
	</px:PXGrid>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
	<px:PXTab ID="tab" runat="server" Height="300px" Style="z-index: 100;" Width="100%">
		<Items>
			<px:PXTabItem Text="Document Details">
				<Template>
					<px:PXGrid ID="grid" runat="server" Style="z-index: 100;" Width="100%" Height="300px" SyncPosition="True" SkinID="DetailsInTab" DataSourceID="ds" TabIndex="18300" KeepPosition="True">
						<Levels>
							<px:PXGridLevel DataMember="Transactions">
								<Columns>
									<px:PXGridColumn DataField="BranchID" Width="100px" AutoCallBack="True" AllowShowHide="Server" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LineNbr" TextAlign="Right" Width="50px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SortOrder" TextAlign="Right" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID" Width="100px" AutoCallBack="True" LinkCommand="ViewItem" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AppointmentDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CustomerLocationID" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AppointmentID" Width="80" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SOID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="POReceiptLine__SubItemID" Label="Subitem" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TranDesc" Width="200px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Qty" TextAlign="Right" Width="81px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="BaseQty" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UOM" Width="50px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryUnitCost" TextAlign="Right" Width="81px" AutoCallBack="True" CommitChanges="true" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ManualPrice" TextAlign="Center" AllowNull="False" Type="CheckBox" CommitChanges="True"></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryLineAmt" TextAlign="Right" Width="81px" AutoCallBack="True" CommitChanges="true" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscPct" TextAlign="Right" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscAmt" TextAlign="Right" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscCost" TextAlign="Right" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ManualDisc" TextAlign="Center" Type="CheckBox" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountID" RenderEditorText="True" TextAlign="Left" AllowShowHide="Server" Width="90px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountSequenceID" TextAlign="Left" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryTranAmt" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AccountID" Width="100px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AccountID_Account_description" Width="100px" SyncVisibility="false" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SubID" Width="200px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ProjectID" Label="Project" Width="100px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TaskID" Label="Task" Width="100px" AutoCallBack="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CostCodeID" Width="100px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="NonBillable" Label="Non Billable" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Box1099" Width="200px" AllowShowHide="Server" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DeferredCode" Width="50px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DefScheduleID" TextAlign="Right" Width="100px"></px:PXGridColumn>
									<px:PXGridColumn DataField="TaxCategoryID" Width="50px" AutoCallBack="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Date" ></px:PXGridColumn>
									<px:PXGridColumn DataField="POOrderType" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PONbr" ></px:PXGridColumn>
									<px:PXGridColumn DataField="POLineNbr" TextAlign="Right" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ReceiptNbr" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ReceiptLineNbr" TextAlign="Right" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PPVDocType" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PPVRefNbr" TextAlign="Right" ></px:PXGridColumn>
								</Columns>
								<RowTemplate>
									<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXSegmentMask CommitChanges="True" ID="edInventoryID" runat="server" DataField="InventoryID" AllowEdit="True" AllowAddNew="True" AutoRefresh="True" ></px:PXSegmentMask>
									<px:PXSelector runat="server" ID="edAppointmentID" DataField="AppointmentID" AllowEdit="True" ></px:PXSelector>
									<px:PXSelector runat="server" ID="edSOID" DataField="SOID" AllowEdit="True" ></px:PXSelector>
									<px:PXSegmentMask ID="edPOReceiptLine__SubItemID" runat="server" DataField="POReceiptLine__SubItemID" Enabled="False" ></px:PXSegmentMask>
									<px:PXSelector CommitChanges="True" ID="edUOM" runat="server" DataField="UOM" ></px:PXSelector>
									<px:PXNumberEdit ID="edQty" runat="server" DataField="Qty" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edCuryUnitCost" runat="server" DataField="CuryUnitCost" CommitChanges="true" ></px:PXNumberEdit>
                                    <px:PXCheckBox ID="chkManualPrice" runat="server" DataField="ManualPrice" CommitChanges="True" ></px:PXCheckBox>
									<px:PXNumberEdit ID="edCuryLineAmt" runat="server" DataField="CuryLineAmt" CommitChanges="true" ></px:PXNumberEdit>
									<px:PXSelector ID="edDiscountCode" runat="server" DataField="DiscountID" CommitChanges="True" AllowEdit="True" ></px:PXSelector>
									<px:PXNumberEdit ID="edDiscPct" runat="server" DataField="DiscPct" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edCuryDiscAmt" runat="server" DataField="CuryDiscAmt" ></px:PXNumberEdit>
									<px:PXCheckBox ID="chkManualDisc" runat="server" DataField="ManualDisc" CommitChanges="true" ></px:PXCheckBox>
									<px:PXNumberEdit ID="edCuryTranAmt" runat="server" DataField="CuryTranAmt" Enabled="False" ></px:PXNumberEdit>
									<px:PXSegmentMask CommitChanges="True" ID="edAccountID" runat="server" DataField="AccountID" AutoRefresh="True" ></px:PXSegmentMask>
									<px:PXSegmentMask ID="edSubID" runat="server" DataField="SubID" AutoRefresh="True" ></px:PXSegmentMask>
									<px:PXDropDown ID="edBox1099" runat="server" DataField="Box1099" ></px:PXDropDown>

									<px:PXLayoutRule runat="server" ColumnSpan="2" ></px:PXLayoutRule>
									<px:PXTextEdit ID="edTranDesc" runat="server" DataField="TranDesc" ></px:PXTextEdit>

									<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXSegmentMask CommitChanges="True" ID="edBranchID" runat="server" DataField="BranchID" ></px:PXSegmentMask>
									<px:PXSelector ID="edDeferredCode" runat="server" DataField="DeferredCode" ></px:PXSelector>
									<px:PXSelector ID="edDefScheduleID" runat="server" DataField="DefScheduleID" AutoRefresh="True" AllowEdit="True"></px:PXSelector>
									<px:PXSelector ID="edTaxCategoryID" runat="server" DataField="TaxCategoryID" CommitChanges="True" AutoRefresh="True" ></px:PXSelector>
									<px:PXSegmentMask CommitChanges="True" ID="edProjectID" runat="server" DataField="ProjectID" ></px:PXSegmentMask>
									<px:PXSegmentMask CommitChanges="True" ID="edTaskID" runat="server" DataField="TaskID" AutoRefresh="True" ></px:PXSegmentMask>
                                    <px:PXSegmentMask ID="edCostCode" runat="server" DataField="CostCodeID" AutoRefresh="True" AllowAddNew="true" ></px:PXSegmentMask>
									<px:PXDropDown ID="edPOOrderType" runat="server" DataField="POOrderType" Enabled="False" ></px:PXDropDown>
									<px:PXSelector ID="edPONbr" runat="server" DataField="PONbr" Enabled="False" AllowEdit="True" ></px:PXSelector>
									<px:PXSelector ID="edReceiptNbr" runat="server" DataField="ReceiptNbr" Enabled="False" AllowEdit="True" ></px:PXSelector>
									<px:PXDropDown ID="edPPVDocType" runat="server" DataField="PPVDocType" Enabled="False" AllowEdit="True" ></px:PXDropDown>
									<px:PXSelector ID="edPPVRefNbr" runat="server" DataField="PPVRefNbr" Enabled="False" AllowEdit="True" ></px:PXSelector>
								</RowTemplate>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
						<Mode InitNewRow="True" AllowFormEdit="True" AllowUpload="True" AutoInsertField="CuryLineAmt" ></Mode>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton Text="View Schedule" Key="cmdViewSchedule">
									<AutoCallBack Command="ViewSchedule" Target="ds" ></AutoCallBack>
									<PopupCommand Command="Cancel" Target="ds" ></PopupCommand>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Add PO Receipt" Key="cmdRT">
									<AutoCallBack Command="AddPOReceipt" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Add PO Receipt Line" Key="cmdRTL">
									<AutoCallBack Command="AddReceiptLine" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Add PO Order" Key="cmdPO">
									<AutoCallBack Command="AddPOOrder" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton StateColumn="InventoryID">
									<AutoCallBack Command="LinkLine" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="View Item" Key="ViewItem">
									<AutoCallBack Command="ViewItem" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Financial Details" LoadOnDemand="false">
				<Template>
					<px:PXFormView ID="form2" runat="server" Style="z-index: 100" Width="100%" DataMember="CurrentDocument" CaptionVisible="False" SkinID="Transparent" DataSourceID="ds" MarkRequired="Dynamic">
						<Template>
							<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" GroupCaption="Link to GL" StartGroup="True" ></px:PXLayoutRule>
							<px:PXSelector ID="edBatchNbr" runat="server" DataField="BatchNbr" Enabled="False" AllowEdit="True" DataSourceID="ds" ></px:PXSelector>
							<px:PXSelector ID="edPrebookBatchNbr" runat="server" DataField="PrebookBatchNbr" Enabled="False" AllowEdit="true" DataSourceID="ds" ></px:PXSelector>
							<px:PXSelector ID="edVoidBatchNbr" runat="server" DataField="VoidBatchNbr" Enabled="False" AllowEdit="true" DataSourceID="ds" ></px:PXSelector>
							<px:PXNumberEdit ID="edDisplayCuryInitDocBal" runat="server" DataField="DisplayCuryInitDocBal" Enabled="False" ></px:PXNumberEdit>
							<px:PXSegmentMask CommitChanges="True" ID="edBranchID" runat="server" DataField="BranchID" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edAPAccountID" runat="server" DataField="APAccountID" CommitChanges="True" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edAPSubID" runat="server" DataField="APSubID" CommitChanges="True" AutoRefresh="True" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edPrebookAcctID" runat="server" DataField="PrebookAcctID" CommitChanges="True" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edPrebookSubID" runat="server" DataField="PrebookSubID" CommitChanges="True" AutoRefresh="True" DataSourceID="ds" ></px:PXSegmentMask>
                                <px:PXTextEdit ID="edOrigRefNbr" runat="server" DataField="OrigRefNbr" Enabled="False" AllowEdit="True">
                                    <LinkCommand Target="ds" Command="ViewOriginalDocument"></LinkCommand>
                                </px:PXTextEdit>

							<px:PXLayoutRule runat="server" GroupCaption="Default Payment Info" StartGroup="True" ></px:PXLayoutRule>
							<px:PXCheckBox ID="chkSeparateCheck" runat="server" DataField="SeparateCheck" ></px:PXCheckBox>
							<px:PXCheckBox CommitChanges="True" ID="chkPaySel" runat="server" DataField="PaySel" ></px:PXCheckBox>
							<px:PXDateTimeEdit ID="edPayDate" runat="server" DataField="PayDate" ></px:PXDateTimeEdit>
							<px:PXSegmentMask CommitChanges="True" ID="edPayLocationID" runat="server" AutoRefresh="True" DataField="PayLocationID" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSelector CommitChanges="True" ID="edPayTypeID" runat="server" DataField="PayTypeID" DataSourceID="ds" ></px:PXSelector>
							<px:PXSegmentMask CommitChanges="True" ID="edPayAccountID" runat="server" DataField="PayAccountID" DataSourceID="ds" ></px:PXSegmentMask>
							<px:PXSelector runat="server" ID="CstUsrAGBankAccountID" DataField="UsrAGBankAccountID" AutoRefresh="True" CommitChanges="True" />

							<px:PXLayoutRule runat="server" ControlSize="XM" GroupCaption="Tax" LabelsWidth="SM" StartColumn="True" StartGroup="True" ></px:PXLayoutRule>
							<px:PXSelector CommitChanges="True" ID="edTaxZoneID" runat="server" DataField="TaxZoneID" DataSourceID="ds" ></px:PXSelector>
							<px:PXCheckBox ID="chkUsesManualVAT" runat="server" DataField="UsesManualVAT" Enabled="false" ></px:PXCheckBox>
                            <px:PXDropDown runat="server" ID="edTaxCalcMode" DataField="TaxCalcMode" CommitChanges="true" ></px:PXDropDown>

                            <px:PXLayoutRule runat="server" ControlSize="XM" GroupCaption="Assigned To" LabelsWidth="SM" StartGroup="True" ></px:PXLayoutRule>
                            <px:PXSelector ID="edEmployeeWorkgroupID" CommitChanges="true" runat="server" AutoRefresh="True" DataField="EmployeeWorkgroupID" DataSourceID="ds" ></px:PXSelector>
                            <px:PXSelector ID="edEmployeeID" CommitChanges="true" runat="server" AutoRefresh="True" DataField="EmployeeID" DataSourceID="ds" ></px:PXSelector>
							<px:PXLayoutRule runat="server" ID="CstPXLayoutRule11" StartGroup="True" GroupCaption="PO" />
							<px:PXTextEdit runat="server" ID="CstPXTextEdit12" DataField="UsrPONumber" Enabled="False" />
							<px:PXNumberEdit runat="server" ID="CstPXNumberEdit13" DataField="UsrPOPrepaymentAmt" Enabled="False" />

                            <px:PXLayoutRule runat="server" LabelsWidth="SM" ControlSize="S" GroupCaption="Voucher Details" ></px:PXLayoutRule>
                            <px:PXFormView ID="VoucherDetails" runat="server" RenderStyle="Simple"
                                DataMember="Voucher" DataSourceID="ds" TabIndex="1100">
                                <Template>
                                    <px:PXTextEdit ID="linkGLVoucherBatch" runat="server" DataField="VoucherBatchNbr" Enabled="false">
                                        <LinkCommand Target="ds" Command="ViewVoucherBatch"></LinkCommand>
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="linkGLWorkBook" runat="server" DataField="WorkBookID" Enabled="false">
                                        <LinkCommand Target="ds" Command="ViewWorkBook"></LinkCommand>
                                    </px:PXTextEdit>
                                </Template>
                            </px:PXFormView>
							<px:PXLayoutRule runat="server" ControlSize="XM" GroupCaption="Receipt Info" LabelsWidth="SM" StartGroup="True" ></px:PXLayoutRule>
							<px:PXSegmentMask CommitChanges="True" ID="edSuppliedByVendorID" runat="server" DataField="SuppliedByVendorID" AllowEdit="True" AutoRefresh="True"></px:PXSegmentMask>
							<px:PXSegmentMask CommitChanges="True" ID="edSuppliedByVendorLocationID" runat="server" DataField="SuppliedByVendorLocationID" AutoRefresh="True" ></px:PXSegmentMask>
							<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="M" GroupCaption="Cash Discount Info" StartGroup="True" ></px:PXLayoutRule>
							<px:PXNumberEdit runat="server" DataField="CuryDiscountedDocTotal" ID="edCuryDiscountedDocTotal" Enabled="false" ></px:PXNumberEdit>
							<px:PXNumberEdit runat="server" DataField="CuryDiscountedTaxableTotal" ID="edCuryDiscountedTaxableTotal" Enabled="false" ></px:PXNumberEdit>
							<px:PXNumberEdit runat="server" DataField="CuryDiscountedPrice" ID="edCuryDiscountedPrice" Enabled="false" ></px:PXNumberEdit>
							<px:PXLayoutRule runat="server" ID="CstPXLayoutRule18" StartGroup="True" GroupCaption="Budget owner" />
							<px:PXSelector runat="server" ID="CstPXSelector20" DataField="UsrEmployeeID" CommitChanges="True" />
							<px:PXSelector runat="server" ID="CstPXSelector19" DataField="UsrDepartment" CommitChanges="True" /></Template>
					</px:PXFormView>
					<px:PXLayoutRule runat="server" ID="CstPXLayoutRule10" StartGroup="True" /></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Tax Details">
				<Template>
					<px:PXGrid ID="grid1" runat="server" Style="z-index: 100;" Height="300px"
						Width="100%" ActionsPosition="Top" SkinID="DetailsInTab" DataSourceID="ds"
						TabIndex="3900">
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
						<LevelStyles>
							<RowForm Width="300px">
							</RowForm>
						</LevelStyles>
						<ActionBar>
							<Actions>
								<Search Enabled="False" ></Search>
								<Save Enabled="False" ></Save>
							</Actions>
						</ActionBar>
						<Levels>
							<px:PXGridLevel DataMember="Taxes">
								<Columns>
									<px:PXGridColumn DataField="TaxID" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TaxRate" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryTaxableAmt" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryTaxAmt" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="NonDeductibleTaxRate" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryExpenseAmt" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Tax__TaxType"></px:PXGridColumn>
									<px:PXGridColumn DataField="Tax__PendingTax" TextAlign="Center" Type="CheckBox" Width="60px"></px:PXGridColumn>
									<px:PXGridColumn DataField="Tax__ReverseTax" TextAlign="Center" Type="CheckBox" Width="60px"></px:PXGridColumn>
									<px:PXGridColumn DataField="Tax__ExemptTax" TextAlign="Center" Type="CheckBox" Width="60px"></px:PXGridColumn>
									<px:PXGridColumn DataField="Tax__StatisticalTax" TextAlign="Center" Type="CheckBox" Width="60px"></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscountedTaxableAmt" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscountedPrice" TextAlign="Right" Width="100px" ></px:PXGridColumn>
								</Columns>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Landed Costs" BindingContext="form"  LoadOnDemand="True" VisibleExp="DataControls[&quot;chkLCEnabled&quot;].Value = 1"
				RepaintOnDemand="false" >
				<Template>
					<px:PXGrid ID="gridLCTran" runat="server" Style="z-index: 100; left: 0px; top: 0px; height: 269px;" Width="100%" ActionsPosition="Top" BorderWidth="0px" SkinID="Details" DataSourceID="ds" Height="269px"
						TabIndex="18100" SyncPosition="true">
						<Levels>
							<px:PXGridLevel DataMember="landedCostTrans" SortOrder="GroupTranID">
								<RowTemplate>
									<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXSelector CommitChanges="True" ID="edLandedCostCodeID" runat="server" AutoRefresh="True" DataField="LandedCostCodeID" ></px:PXSelector>
									<px:PXNumberEdit ID="edCuryLCAmount" runat="server" DataField="CuryLCAmount" ></px:PXNumberEdit>
									<px:PXDropDown CommitChanges="True" ID="edPOReceiptType" runat="server" DataField="POReceiptType" ></px:PXDropDown>
									<px:PXSelector CommitChanges="True" ID="edPOReceiptNbr" runat="server" DataField="POReceiptNbr" AllowEdit="true" AutoRefresh="True" ></px:PXSelector>
									<px:PXSegmentMask ID="edInventoryID1" runat="server" DataField="InventoryID" AutoRefresh="True" ></px:PXSegmentMask>
									<px:PXTextEdit ID="edDescr" runat="server" DataField="Descr" ></px:PXTextEdit>
									<px:PXSelector ID="edTaxCategoryID1" runat="server" DataField="TaxCategoryID" AutoRefresh="True" ></px:PXSelector>
									<px:PXSegmentMask CommitChanges="True" ID="edSiteID" runat="server" DataField="SiteID" ></px:PXSegmentMask>
									<px:PXSegmentMask Size="xs" ID="edVendorID" runat="server" DataField="VendorID" ></px:PXSegmentMask>
									<px:PXSegmentMask ID="edLocationID" runat="server" DataField="LocationID" AutoRefresh="True" ></px:PXSegmentMask>
									<px:PXSegmentMask ID="edVendorLocationID" runat="server" DataField="VendorLocationID" ></px:PXSegmentMask>
									<px:PXNumberEdit CommitChanges="True" ID="edLCTranID" runat="server" DataField="LCTranID" Enabled="False" ></px:PXNumberEdit>
								</RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="LCTranID" TextAlign="Right" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LandedCostCodeID" Width="117px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Descr" Width="351px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorID" Width="81px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorLocationID" Width="54px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryLCAPEffAmount" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TaxCategoryID" Label="Tax Category" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="POReceiptType" Width="110px" AutoCallBack="true" CommitChanges="true" ></px:PXGridColumn>
									<px:PXGridColumn DataField="POReceiptNbr" AutoCallBack="true" Width="115px" NullText="&lt;SPLIT&gt;" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID" Width="115px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SiteID" Width="81px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LocationID" Width="81px" ></px:PXGridColumn>
								</Columns>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
						<Mode InitNewRow="True" AllowFormEdit="True" ></Mode>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton Text="View PO Receipt">
									<AutoCallBack Command="ViewLCPOReceipt" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="View LC IN Adjustment" Key="cmdViewLCTran">
									<AutoCallBack Command="ViewLCINDocument" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Add Posted Landed Cost" Key="cmdAddPLCT">
									<AutoCallBack Command="AddPostLandedCostTran" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="LC Splits" Key="cmdLS" CommandName="LsLCSplits" CommandSourceID="ds" DependOnGrid="gridLCTran" ></px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
						<Mode InitNewRow="True" ></Mode>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Approval Details" BindingContext="form" RepaintOnDemand="false">
				<Template>
					<px:PXGrid ID="gridApproval" runat="server" DataSourceID="ds" Width="100%" SkinID="DetailsInTab" NoteIndicator="True" Style="left: 0; top: 0;">
						<AutoSize Enabled="True" ></AutoSize>
						<Mode AllowAddNew="False" AllowDelete="False" AllowUpdate="False" ></Mode>
						<Levels>
							<px:PXGridLevel DataMember="Approval">
								<Columns>
									<px:PXGridColumn DataField="ApproverEmployee__AcctCD" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproverEmployee__AcctName" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctCD" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctName" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproveDate" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" AllowNull="False" AllowUpdate="False" RenderEditorText="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="WorkgroupID" Width="150px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AssignmentMapID"  Visible="false" SyncVisible="false" Width="160px"></px:PXGridColumn>
									<px:PXGridColumn DataField="RuleID" Visible="false" SyncVisible="false" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CreatedDateTime" Visible="false" SyncVisible="false" Width="100px" ></px:PXGridColumn>
								</Columns>
							</px:PXGridLevel>
						</Levels>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Discount Details">
				<Template>
					<px:PXGrid ID="formDiscountDetail" runat="server" DataSourceID="ds" Width="100%" SkinID="Details" BorderStyle="None">
						<Levels>
							<px:PXGridLevel DataMember="DiscountDetails" DataKeyNames="DocType,RefNbr,DiscountID,DiscountSequenceID,Type">
								<RowTemplate>
									<px:PXLayoutRule ID="PXLayoutRule3" runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXCheckBox ID="chkSkipDiscount" runat="server" DataField="SkipDiscount" ></px:PXCheckBox>
									<px:PXSelector ID="edDiscountID" runat="server" DataField="DiscountID"
										AllowEdit="True" edit="1" ></px:PXSelector>
									<px:PXDropDown ID="edType" runat="server" DataField="Type" Enabled="False" ></px:PXDropDown>
									<px:PXCheckBox ID="chkIsManual" runat="server" DataField="IsManual" ></px:PXCheckBox>
									<px:PXSelector ID="edDiscountSequenceID" runat="server" DataField="DiscountSequenceID" AutoRefresh="true" AllowEdit="true" ></px:PXSelector>
									<px:PXNumberEdit ID="edCuryDiscountableAmt" runat="server" DataField="CuryDiscountableAmt" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edDiscountableQty" runat="server" DataField="DiscountableQty" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edCuryDiscountAmt" runat="server" DataField="CuryDiscountAmt" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edDiscountPct" runat="server" DataField="DiscountPct" ></px:PXNumberEdit>
									<px:PXSelector ID="edPOReceiptRefNbr" runat="server" DataField="ReceiptNbr" Enabled="false" ></px:PXSelector>
								</RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="SkipDiscount" Width="75px" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountID" Width="90px" CommitChanges="true" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountSequenceID" Width="90px" CommitChanges="true" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Type" RenderEditorText="True" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="IsManual" Width="75px" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscountableAmt" TextAlign="Right" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountableQty" TextAlign="Right" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscountAmt" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DiscountPct" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderNbr" TextAlign="Right" Width="81px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ReceiptNbr" TextAlign="Right" Width="81px" ></px:PXGridColumn>
								</Columns>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Applications" RepaintOnDemand="false" >
				<Template>
					<px:PXGrid ID="detgrid" runat="server" Style="z-index: 100;" Width="100%" Height="300px" SkinID="DetailsInTab">
						<ActionBar DefaultAction="ViewPayment">
							<CustomItems>
								<px:PXToolBarButton Text="Auto Apply" Tooltip="Auto Apply">
									<AutoCallBack Command="AutoApply" Target="ds">
										<Behavior CommitChanges="True" ></Behavior>
									</AutoCallBack>
								</px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
						<Levels>
							<px:PXGridLevel DataMember="Adjustments">
								<RowTemplate>
									<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXNumberEdit CommitChanges="True" ID="edCuryAdjdAmt" runat="server" DataField="CuryAdjdAmt" ></px:PXNumberEdit>
								</RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="DisplayDocType" Width="100px" Type="DropDownList" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayRefNbr" Width="100px" AutoCallBack="True" LinkCommand="ViewPayment" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryAdjdAmt" AutoCallBack="True" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryAdjdPPDAmt" AutoCallBack="True" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayDocDate" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDocBal" TextAlign="Right" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayDocDesc" Width="250px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayCuryID" Width="50px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayFinPeriodID" ></px:PXGridColumn>
									<px:PXGridColumn DataField="APPayment__ExtRefNbr" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AdjdDocType" Width="18px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AdjdRefNbr" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DisplayStatus" Label="Status" ></px:PXGridColumn>
								</Columns>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="ADD TAX INVOICE">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid8" SkinID="DetailsInTab" Width="100%" DataSourceID="ds">
						<AutoSize Enabled="True" MinHeight="200" />
						<Levels>
							<px:PXGridLevel DataMember="ReceivedTaxInvoices">
								<RowTemplate>
									<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit9" DataField="RSReceivedTaxInvoice__CreateDate" />
									<px:PXSelector runat="server" ID="CstPXSelector10" DataField="RSReceivedTaxInvoice__TaxInvoiceNbr" AllowEdit="True" />
									<px:PXNumberEdit runat="server" DataField="RSReceivedTaxInvoiceItem__FullAmt" ID="CstPXNumberEdit11" />
									<px:PXNumberEdit runat="server" DataField="RSReceivedTaxInvoiceItem__VATAmt" ID="CstPXNumberEdit12" />
									<px:PXNumberEdit runat="server" DataField="TaxInvoiceID" ID="CstPXNumberEdit13" />
									<px:PXNumberEdit runat="server" ID="CstPXNumberEdit1" DataField="RSReceivedTaxInvoice__FullAmt" />
									<px:PXNumberEdit runat="server" ID="CstPXNumberEdit2" DataField="RSReceivedTaxInvoice__VATAmt" /></RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="TaxInvoiceID" Width="120" />
									<px:PXGridColumn DataField="RSReceivedTaxInvoice__TaxInvoiceNbr" Width="120" />
									<px:PXGridColumn DataField="RSReceivedTaxInvoice__CreateDate" Width="150" />
									<px:PXGridColumn DataField="RSReceivedTaxInvoice__VATAmt" Width="100" />
									<px:PXGridColumn DataField="RSReceivedTaxInvoice__FullAmt" Width="100" /></Columns></px:PXGridLevel></Levels>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton Text="Add" Tooltip="Add" DisplayStyle="Image" ImageKey="AddNew" Visible="">
									<AutoCallBack Command="AddReceivedTaxInvoice" Enabled="True" Target="ds" /></px:PXToolBarButton>
								<px:PXToolBarButton Text="Delete" Tooltip="Delete" DisplayStyle="Image" ImageKey="Remove">
									<AutoCallBack Enabled="True" Target="ds" Command="RemoveTaxInvoice" /></px:PXToolBarButton></CustomItems>
							<Actions>
								<AddNew ToolBarVisible="False" />
								<Delete MenuVisible="False" ToolBarVisible="False" />
								<FilesMenu ToolBarVisible="False" /></Actions></ActionBar></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Related FA" BindingContext="form" VisibleExp="DataControls[&quot;CstPXCheckBox7&quot;].Value==1" RepaintOnDemand="False">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid9" SkinID="DetailsInTab" Width="100%" DataSourceID="ds">
						<Levels>
							<px:PXGridLevel DataMember="FARelateds">
								<Columns>
									<px:PXGridColumn DataField="AssetCD" Width="150" TextAlign="Center" CommitChanges="True" />
									<px:PXGridColumn DataField="Description" Width="180" TextAlign="Center" />
									<px:PXGridColumn DataField="AssetClass" Width="130" TextAlign="Center" />
									<px:PXGridColumn DataField="Status" Width="80" TextAlign="Center" />
									<px:PXGridColumn DataField="ReceiptDate" Width="150" TextAlign="Center" />
									<px:PXGridColumn DataField="PlacedInServiceDate" Width="150" TextAlign="Center" />
									<px:PXGridColumn DataField="OrigAcquisitionCost" Width="150" TextAlign="Center" />
									<px:PXGridColumn DataField="UsefulLifeYears" Width="140" TextAlign="Center" />
									<px:PXGridColumn DataField="Department" Width="120" TextAlign="Center" />
									<px:PXGridColumn DataField="ExpenseAmount" Width="150" TextAlign="Center" /></Columns>
								<RowTemplate>
									<px:PXSelector runat="server" ID="CstPXSelector8" DataField="AssetCD" AllowEdit="True" /></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="200" /></px:PXGrid></Template></px:PXTabItem></Items>
		<CallbackCommands>
			<Search CommitChanges="True" PostData="Page" ></Search>
			<Refresh CommitChanges="True" PostData="Page" ></Refresh>
		</CallbackCommands>
		<AutoSize Container="Window" Enabled="True" MinHeight="180" ></AutoSize>
	</px:PXTab>
	<px:PXSmartPanel ID="PanelAddPOReceipt" runat="server" Style="z-index: 108;" Width="900px" Height="400px"
		Key="poreceiptslist" AutoCallBack-Command="Refresh" AutoCallBack-Target="grid4" CommandSourceID="ds"
		Caption="Add PO Receipt" CaptionVisible="True" LoadOnDemand="True" ContentLayout-OuterSpacing="None" AutoReload="True">
		<px:PXFormView ID="frmPOrderFilter" runat="server" DataMember="filter" Style="z-index: 100;" Width="100%" CaptionVisible="false" SkinID="Transparent">
			<Template>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="MS" ControlSize="M"></px:PXLayoutRule>
				<px:PXSelector CommitChanges="True" ID="edOrderNbr" runat="server" DataField="OrderNbr" AutoRefresh="True" ></px:PXSelector>
			</Template>
		</px:PXFormView>
		<px:PXGrid ID="grid4" runat="server" Width="100%" Height="200px" BatchUpdate="True" SkinID="Inquire"
			DataSourceID="ds" FastFilterFields="ReceiptNbr" TabIndex="16900">
			<Levels>
				<px:PXGridLevel DataMember="poreceiptslist" DataKeyNames="ReceiptType,ReceiptNbr">
					<Columns>
                        <px:PXGridColumn AllowCheckAll="True" AllowMove="False" AllowSort="False" DataField="Selected" TextAlign="Center" Type="CheckBox" Width="26px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptNbr" Width="108px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptType" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorID" Width="81px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorLocationID" Width="63px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryID" Width="60px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptDate" Width="90px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderQty" TextAlign="Right" Width="81px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryOrderTotal" TextAlign="Right" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UnbilledQty" TextAlign="Right" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryUnbilledTotal" TextAlign="Right" Width="100px" ></px:PXGridColumn>
					</Columns>
					<Layout FormViewHeight="" ></Layout>
				</px:PXGridLevel>
			</Levels>
			<AutoSize Enabled="True" ></AutoSize>
		</px:PXGrid>
		<px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton1" runat="server" DialogResult="OK" Text="Add & Close" ></px:PXButton>
			<px:PXButton ID="PXButton2" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelAddPOReceiptLine" runat="server" Caption="Add Receipt Line" CaptionVisible="True"
		Height="400px" Width="900px" Key="poReceiptLinesSelection" LoadOnDemand="True" AutoCallBack-Command="Refresh" AutoCallBack-Target="gridOL" AutoReload="True">
		<px:PXFormView ID="frmPOFilter" runat="server" DataMember="filter" Style="z-index: 100;" Width="100%" CaptionVisible="false" SkinID="Transparent" >
			<Template>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="MS" ControlSize="M" ></px:PXLayoutRule>
				<px:PXSelector CommitChanges="True" ID="edOrderNbr" runat="server" DataField="OrderNbr" AutoRefresh="True" ></px:PXSelector>
			</Template>
		</px:PXFormView>
		<px:PXGrid ID="gridOL" runat="server" Height="200px" Width="100%" SkinID="Inquire" DataSourceID="ds"
			FastFilterFields="InvenotryID,TranDesc" TabIndex="17300">
			<Levels>
				<px:PXGridLevel DataMember="poReceiptLinesSelection">
					<Columns>
						<px:PXGridColumn DataField="Selected" Type="CheckBox" AllowCheckAll="True" AllowSort="False" AllowMove="False" TextAlign="Center" Width="26px" AutoCallBack="True" CommitChanges="True" AllowResize="False" AllowShowHide="False" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PONbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POType" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POReceipt__InvoiceNbr" Width="80px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SubItemID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SiteID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UOM" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LineNbr" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POReceipt__CuryID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryExtCost" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UnbilledQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TotalCuryUnbilledAmount" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TranDesc" Width="200px" ></px:PXGridColumn>
					    <px:PXGridColumn DataField="VendorID" Width="100px"></px:PXGridColumn>
					    <px:PXGridColumn DataField="POReceipt__VendorLocationID" Width="100px"></px:PXGridColumn>
					</Columns>
				</px:PXGridLevel>
			</Levels>
			<AutoSize Enabled="true" ></AutoSize>
		</px:PXGrid>
		<px:PXPanel ID="PXPanel2" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton3" runat="server" Text="Add" SyncVisible="false">
				<AutoCallBack Command="AddReceiptLine2" Target="ds" ></AutoCallBack>
			</px:PXButton>
			<px:PXButton ID="PXButton4" runat="server" DialogResult="OK" Text="Add & Close" ></px:PXButton>
			<px:PXButton ID="PXButton5" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelAddPOOrder" runat="server" Width="800px" Height="400px" Key="poorderslist" CommandSourceID="ds"
		Caption="Add PO Order" CaptionVisible="True" LoadOnDemand="True" AutoCallBack-Command="Refresh" AutoCallBack-Target="PXGrid1">
		<px:PXGrid ID="PXGrid1" runat="server" Height="200px" Width="100%" BatchUpdate="True" SkinID="Inquire"
			DataSourceID="ds" FastFilterFields="OrderNbr" TabIndex="17500">
			<AutoSize Enabled="true" ></AutoSize>
			<Levels>
				<px:PXGridLevel DataMember="poorderslist">
					<Columns>
						<px:PXGridColumn DataField="Selected" Type="CheckBox" AllowCheckAll="True" AllowSort="False" AllowMove="False" TextAlign="Center" AllowResize="False" Width="26px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderType" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorLocationID" Width="80px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderDate" Width="90px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryID" Width="60px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryOrderTotal" TextAlign="Right" Width="81px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LeftToBillQty" TextAlign="Right" Width="81px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryLeftToBillCost" TextAlign="Right" Width="81px" ></px:PXGridColumn>
					</Columns>
					<Layout FormViewHeight="" ></Layout>
				</px:PXGridLevel>
			</Levels>
		</px:PXGrid>
		<px:PXPanel ID="PXPanel3" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton6" runat="server" Text="Add" SyncVisible="false">
				<AutoCallBack Command="AddPOOrder2" Target="ds" ></AutoCallBack>
			</px:PXButton>
			<px:PXButton ID="PXButton7" runat="server" DialogResult="OK" Text="Add & Close" ></px:PXButton>
			<px:PXButton ID="PXButton8" runat="server" DialogResult="No" Text="Cancel" SyncVisible="false" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelAddPostLandedCostTran" runat="server" Width="800px" Height="400px" Style="z-index: 108;" Key="landedCostTranSelection" AutoCallBack-Command="Refresh" AutoCallBack-Target="gridLCSelection"
		CommandSourceID="ds" Caption="Add Postponed Landed Cost" CaptionVisible="True" LoadOnDemand="True">
		<px:PXGrid ID="gridLCSelection" runat="server" Height="200px" Width="100%" BatchUpdate="True" SkinID="Inquire"
			DataSourceID="ds" FastFilterFields="POReceiptNbr,VendorID" TabIndex="17700">
			<AutoSize Enabled="true" ></AutoSize>
			<Levels>
				<px:PXGridLevel DataMember="landedCostTranSelection">
					<Columns>
                        <px:PXGridColumn DataField="Selected" TextAlign="Center" Type="CheckBox" AllowCheckAll="True" AllowSort="False" AllowMove="False" Width="26px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LCTranID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LandedCostCodeID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="VendorLocationID" Width="80px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POReceiptNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryLCAmount" TextAlign="Right" Width="100px" ></px:PXGridColumn>
					</Columns>
					<Layout FormViewHeight="" ></Layout>
				</px:PXGridLevel>
			</Levels>
		</px:PXGrid>
		<px:PXPanel ID="PXPanel4" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton9" runat="server" DialogResult="OK" Text="Add & Close" ></px:PXButton>
			<px:PXButton ID="PXButton10" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelLCS" runat="server" Style="z-index: 108;" Width="800px" Height="400px" Caption="Landed Cost Splits"
		CaptionVisible="True" Key="landedCostTrans" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True"
		AutoCallBack-Target="grdLCSplit">
		<px:PXGrid ID="grdLCSplit" runat="server" Width="100%" Height="240px" SkinID="Details" DataSourceID="ds" TabIndex="17900">
			<Mode InitNewRow="True" ></Mode>
			<Parameters>
				<px:PXSyncGridParam ControlID="gridLCTran" ></px:PXSyncGridParam>
			</Parameters>
			<Levels>
				<px:PXGridLevel DataMember="LCTranSplit">
					<RowTemplate>
						<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="MS" ControlSize="M" ></px:PXLayoutRule>
						<px:PXSelector CommitChanges="True" ID="edPOReceiptNbr2" runat="server" DataField="POReceiptNbr" AutoRefresh="True" ></px:PXSelector>
						<px:PXSegmentMask ID="edInventoryID2" runat="server" DataField="InventoryID" AutoRefresh="True" ></px:PXSegmentMask>
					</RowTemplate>
					<Columns>
						<px:PXGridColumn DataField="POReceiptNbr" Label="PO Receipt Nbr." AutoCallBack="True" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID" Label="Inventory ID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Descr" Width="250px">
						</px:PXGridColumn>
					</Columns>
					<Layout FormViewHeight="" ></Layout>
				</px:PXGridLevel>
			</Levels>
			<AutoSize Enabled="true" ></AutoSize>
		</px:PXGrid>
		<px:PXPanel ID="PXPanel5" SkinID="Buttons" runat="server">
			<px:PXButton ID="PXButton11" runat="server" DialogResult="OK" Text="Ok" ></px:PXButton>
			<px:PXButton ID="PXButton12" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="panelDuplicate" runat="server" Caption="Duplicate Reference Nbr." CaptionVisible="true" LoadOnDemand="true" Key="duplicatefilter"
		AutoCallBack-Enabled="true" AutoCallBack-Command="Refresh" CallBackMode-CommitChanges="True"
		CallBackMode-PostData="Page">
		<div style="padding: 5px">
			<px:PXFormView ID="formCopyTo" runat="server" DataSourceID="ds" CaptionVisible="False" DataMember="duplicatefilter">
				<ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
				<Template>
					<px:PXLayoutRule ID="PXLayoutRule2" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="SM" ></px:PXLayoutRule>
					<px:PXLabel Size="xl" ID="lblMessage" runat="server">Record already exists. Please enter new Reference Nbr.</px:PXLabel>
					<px:PXMaskEdit CommitChanges="True" ID="edRefNbr" runat="server" DataField="RefNbr" InputMask="&gt;CCCCCCCCCCCCCCC" ></px:PXMaskEdit>
				</Template>
			</px:PXFormView>
		</div>
		<px:PXPanel ID="PXPanel7" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton14" runat="server" DialogResult="OK" Text="OK" CommandSourceID="ds" ></px:PXButton>
			<px:PXButton ID="PXButton15" runat="server" DialogResult="Cancel" Text="Cancel" CommandSourceID="ds" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelRecalcDiscounts" runat="server" Caption="Recalculate Prices" CaptionVisible="true" LoadOnDemand="true" Key="recalcdiscountsfilter"
		AutoCallBack-Enabled="true" AutoCallBack-Target="formRecalcDiscounts" AutoCallBack-Command="Refresh" CallBackMode-CommitChanges="True"
		CallBackMode-PostData="Page">
		<div style="padding: 5px">
			<px:PXFormView ID="formRecalcDiscounts" runat="server" DataSourceID="ds" CaptionVisible="False" DataMember="recalcdiscountsfilter">
				<Activity Height="" HighlightColor="" SelectedColor="" Width="" ></Activity>
				<ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
				<Template>
					<px:PXLayoutRule ID="PXLayoutRule3" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="SM" ></px:PXLayoutRule>
					<px:PXDropDown ID="edRecalcTerget" runat="server" DataField="RecalcTarget" CommitChanges="true" ></px:PXDropDown>
					<px:PXCheckBox CommitChanges="True" ID="chkRecalcUnitPrices" runat="server" DataField="RecalcUnitPrices" ></px:PXCheckBox>
					<px:PXCheckBox CommitChanges="True" ID="chkOverrideManualPrices" runat="server" DataField="OverrideManualPrices" ></px:PXCheckBox>
					<px:PXCheckBox CommitChanges="True" ID="chkRecalcDiscounts" runat="server" DataField="RecalcDiscounts" ></px:PXCheckBox>
					<px:PXCheckBox CommitChanges="True" ID="chkOverrideManualDiscounts" runat="server" DataField="OverrideManualDiscounts" ></px:PXCheckBox>
				</Template>
			</px:PXFormView>
		</div>
		<px:PXPanel ID="PXPanel6" runat="server" SkinID="Buttons">
			<px:PXButton ID="PXButton13" runat="server" DialogResult="OK" Text="OK" CommandName="RecalcOk" CommandSourceID="ds" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel ID="PanelLinkLine" runat="server" Width="1100px" Height="500px" Key="linkLineFilter" CommandSourceID="ds" Caption="Link Line" CaptionVisible="True" LoadOnDemand="False" AutoCallBack-Command="Refresh" AutoCallBack-Target="LinkLineFilterForm">
		<px:PXFormView runat="server" ID="LinkLineFilterForm" DataMember="linkLineFilter" Style="z-index: 100;" Width="100%" CaptionVisible="false" SkinID="Transparent">
			<Template>
				<px:PXLayoutRule ID="PXLayoutRule02" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" ></px:PXLayoutRule>
				<px:PXSelector runat="server" ID="edPOOrderNbr" DataField="POOrderNbr" CommitChanges="True" AutoRefresh="True" ></px:PXSelector>
				<px:PXSelector runat="server" ID="edSiteID" DataField="SiteID" CommitChanges="True" AutoRefresh="True" ></px:PXSelector>
				<px:PXSelector runat="server" ID="edInventoryID" DataField="InventoryID" ></px:PXSelector>
				<px:PXSelector runat="server" ID="edUOM" DataField="UOM" ></px:PXSelector>
				<px:PXLayoutRule ID="PXLayoutRule01" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="SM" ></px:PXLayoutRule>
				<px:PXGroupBox CommitChanges="True" RenderStyle="RoundBorder" ID="gpMode" runat="server" Caption="Selected Mode" DataField="SelectedMode">
					<Template>
						<px:PXRadioButton runat="server" ID="rOrder" Value="O" ></px:PXRadioButton>
						<px:PXRadioButton runat="server" ID="rReceipt" Value="R" ></px:PXRadioButton>
					</Template>
				</px:PXGroupBox>
			</Template>
		</px:PXFormView>
		<px:PXGrid ID="LinkLineGrid" runat="server" Height="200px" Width="100%" BatchUpdate="False" SkinID="Inquire" DataSourceID="ds" FastFilterFields="PONbr,ReceiptNbr,POReceipt__InvoiceNbr,TranDesc,SiteID" TabIndex="17500" FilesIndicator="False" NoteIndicator="False">
			<AutoSize Enabled="true" ></AutoSize>
			<Levels>
				<px:PXGridLevel DataMember="linkLineReceiptTran">
					<Columns>
						<px:PXGridColumn DataField="Selected" Type="CheckBox" AllowCheckAll="False" AllowSort="False" AllowMove="False" TextAlign="Center" Width="20px" AutoCallBack="True" CommitChanges="True" AllowResize="False" AllowShowHide="False" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PONbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POType" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POReceipt__InvoiceNbr" Width="80px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SubItemID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SiteID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LineNbr" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POReceipt__CuryID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ReceiptQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryExtCost" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UnbilledQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TotalCuryUnbilledAmount" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TranDesc" Width="200px" ></px:PXGridColumn>
					</Columns>
				</px:PXGridLevel>
			</Levels>
			<Mode AllowAddNew="False" AllowDelete="False" ></Mode>
		</px:PXGrid>
		<px:PXGrid ID="LinkLineOrderGrid" runat="server" Height="200px" Width="100%" BatchUpdate="False" SkinID="Inquire" DataSourceID="ds" FastFilterFields="POOrder__OrderNbr,POOrder__VendorRefNbr,TranDesc,SiteID" TabIndex="17500" FilesIndicator="False" NoteIndicator="False">
			<AutoSize Enabled="true" ></AutoSize>
			<Levels>
				<px:PXGridLevel DataMember="linkLineOrderTran">
					<Columns>
						<px:PXGridColumn DataField="Selected" Type="CheckBox" AllowCheckAll="False" AllowSort="False" AllowMove="False" TextAlign="Center" Width="20px" AutoCallBack="True" CommitChanges="True" AllowResize="False" AllowShowHide="False" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POOrder__OrderNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POOrder__OrderType" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LineNbr" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POOrder__VendorRefNbr" Width="80px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SubItemID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SiteID" Width="100px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="POOrder__CuryID" Width="70px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="curyLineAmt" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderUnbilledQty" Width="50px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderCuryUnbilledAmt" Width="95px" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TranDesc" Width="200px" ></px:PXGridColumn>
					</Columns>
				</px:PXGridLevel>
			</Levels>
			<Mode AllowAddNew="False" AllowDelete="False" ></Mode>
		</px:PXGrid>
		<px:PXPanel ID="LinkLineButtons" runat="server" SkinID="Buttons">
			<px:PXButton ID="LinkLineSave" runat="server" DialogResult="Yes" Text="Save" ></px:PXButton>
			<px:PXButton ID="LinkLineCancel" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
		</px:PXPanel>
	</px:PXSmartPanel>
	<px:PXSmartPanel runat="server" Height="650px" Width="1200px" ID="pnlTaxInvoice" LoadOnDemand="False" CaptionVisible="True" Caption="Received Tax Invoices" Key="Filter" CommandSourceID="ds" AutoCallBack-Command="Refresh" AutoCallBack-Target="form1">
		<px:PXFormView runat="server" ID="form1" SkinID="Transparent" CaptionVisible="false" Width="100%" DataMember="Filter">
			<Template>
				<px:PXLayoutRule runat="server" ID="CstPXLayoutRule6" StartColumn="True" />
				<px:PXDateTimeEdit runat="server" ID="edStartDate" CommitChanges="True" DataField="StartDate" AlreadyLocalized="False" />
				<px:PXNumberEdit runat="server" CommitChanges="True" DataField="AmountFrom" AlreadyLocalized="False" ID="edAmountFrom" />
				<px:PXSelector runat="server" ID="edFinancePeriod" DataField="FinancePeriod" CommitChanges="True" />
				<px:PXDropDown runat="server" ID="edStatus" CommitChanges="True" DataField="Status" />
				<px:PXLayoutRule runat="server" ID="CstPXLayoutRule7" StartColumn="True" />
				<px:PXDateTimeEdit runat="server" ID="edEndDate" CommitChanges="True" DataField="EndDate" AlreadyLocalized="False" />
				<px:PXNumberEdit runat="server" CommitChanges="True" DataField="AmountTo" AlreadyLocalized="False" ID="edAmountTo" />
				<px:PXCheckBox runat="server" Text="Hide linked with AP Bill" CommitChanges="True" DataField="CheckAPBill" AlreadyLocalized="False" ID="edCheckAPBill" />
				<px:PXCheckBox runat="server" Text="Prepayment Tax Invoice" CommitChanges="True" DataField="PrepaymentTaxInvoice" AlreadyLocalized="False" ID="edPrepaymentTaxInvoice" /></Template></px:PXFormView>
		<px:PXGrid runat="server" ID="grid" Height="200px" SkinID="Inquire" TabIndex="1100" Width="100%" DataSourceID="ds" NoteIndicator="False" FilesIndicator="False" TemporaryFilterCaption="Filter Applied">
			<AutoSize Enabled="true" />
			<Mode AllowAddNew="False" AllowDelete="False" />
			<Levels>
				<px:PXGridLevel DataMember="TaxInvoices" DataKeyNames="TaxInvoiceNbr">
					<RowTemplate>
						<px:PXCheckBox runat="server" Text="Selected" DataField="Selected" AlreadyLocalized="False" ID="edSelected" />
						<px:PXNumberEdit runat="server" DataField="TaxInvoiceID" AlreadyLocalized="False" ID="edTaxInvoiceID" />
						<px:PXSelector runat="server" ID="edTaxInvoiceNbr" DataField="TaxInvoiceNbr" />
						<px:PXTextEdit runat="server" ID="edTaxInvoiceNumber" DataField="TaxInvoiceNumber" AlreadyLocalized="False" />
						<px:PXTextEdit runat="server" ID="edTaxInvoiceSeries" DataField="TaxInvoiceSeries" AlreadyLocalized="False" />
						<px:PXNumberEdit runat="server" ID="edFullAmt" DataField="FullAmt" AlreadyLocalized="False" />
						<px:PXNumberEdit runat="server" ID="edVATAmt" DataField="VATAmt" AlreadyLocalized="False" />
						<px:PXNumberEdit runat="server" ID="edTotalExciseAmt" DataField="TotalExciseAmt" AlreadyLocalized="False" />
						<px:PXNumberEdit runat="server" ID="edTotalAmt" DataField="TotalAmt" AlreadyLocalized="False" /></RowTemplate>
					<Columns>
						<px:PXGridColumn DataField="Selected" Width="60px" AllowCheckAll="True" Type="CheckBox" TextAlign="Center" />
						<px:PXGridColumn DataField="TaxInvoiceNbr" Width="120px" TextAlign="Center" />
						<px:PXGridColumn DataField="TaxInvoiceID" TextAlign="Center" Width="120px" />
						<px:PXGridColumn DataField="TaxInvoiceNumber" Width="150px" TextAlign="Center" />
						<px:PXGridColumn DataField="TaxInvoiceSeries" Width="200px" TextAlign="Center" />
						<px:PXGridColumn DataField="FullAmt" TextAlign="Center" Width="100px" />
						<px:PXGridColumn DataField="VATAmt" TextAlign="Center" Width="100px" />
						<px:PXGridColumn DataField="TotalExciseAmt" TextAlign="Center" Width="100px" />
						<px:PXGridColumn DataField="TotalAmt" TextAlign="Center" Width="120px" /></Columns></px:PXGridLevel></Levels></px:PXGrid>
		<px:PXPanel runat="server" ID="pnlTaxInvoiceButtons" SkinID="Buttons">
			<px:PXButton runat="server" ID="TaxInvoiceAdd" Text="Add" DialogResult="OK" CommandSourceID="" CommandName="" />
			<px:PXButton runat="server" ID="TaxInvoiceCancel" Text="Cancel" DialogResult="Cancel" CommandSourceID="" CommandName="" /></px:PXPanel></px:PXSmartPanel></asp:Content>

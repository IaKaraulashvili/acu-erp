<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AP303000.aspx.cs" Inherits="Page_TabView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="PX.Objects.AP.VendorMaint" PrimaryView="BAccount">
		<CallbackCommands>
			<px:PXDSCallbackCommand CommitChanges="True" Name="Save" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand StartNewGroup="True" Name="First" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand DependOnGrid="grdContacts" Name="ViewContact" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewContact" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand DependOnGrid="grdLocations" Name="ViewLocation" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewLocation" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand DependOnGrid="grdLocations" Name="SetDefault" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewCustomer" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewBusnessAccount" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewMainOnMap" Visible="false" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewDefLocationOnMap" Visible="false" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewRestrictionGroups" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ExtendToCustomer" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewBalanceDetails" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewRemitOnMap" Visible="false" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewBillAdjustment" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewManualCheck" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="VendorDetails" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ApproveBillsForPayments" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="PayBills" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="BalanceByVendor" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="VendorHistory" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="APAgedPastDue" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="APAgedOutstanding" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="APDocumentRegister" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="RepVendorDetails" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Action" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Inquiry" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Report" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ValidateAddresses" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="VendorPrice" Visible="False" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewTask" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewEvent" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewActivity" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewMailActivity" DependOnGrid="grdContacts" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewActivity" DependOnGrid="gridActivities" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="OpenActivityOwner" Visible="False" CommitChanges="True" DependOnGrid="gridActivities" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewCurrency" Visible="False" DependOnGrid="CstPXGrid2" />
			<px:PXDSCallbackCommand Name="ViewCountry" Visible="False" DependOnGrid="CstPXGrid2" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXSmartPanel ID="pnlChangeID" runat="server"  Caption="Specify New ID"
        CaptionVisible="true" DesignView="Hidden" LoadOnDemand="true" Key="ChangeIDDialog" CreateOnDemand="false" AutoCallBack-Enabled="true"
        AutoCallBack-Target="formChangeID" AutoCallBack-Command="Refresh" CallBackMode-CommitChanges="True" CallBackMode-PostData="Page"
        AcceptButtonID="btnOK">
            <px:PXFormView ID="formChangeID" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" CaptionVisible="False"
                DataMember="ChangeIDDialog">
                <ContentStyle BackColor="Transparent" BorderStyle="None" />
                <Template>
                    <px:PXLayoutRule ID="rlAcctCD" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" />
                    <px:PXSegmentMask ID="edAcctCD" runat="server" DataField="CD" />
                </Template>
            </px:PXFormView>
            <px:PXPanel ID="pnlChangeIDButton" runat="server" SkinID="Buttons">
			<px:PXButton ID="btnOK" runat="server" DialogResult="OK" Text="OK">
                    <AutoCallBack Target="formChangeID" Command="Save" />
                </px:PXButton>
				<px:PXButton ID="btnCancel" runat="server" DialogResult="Cancel" Text="Cancel" />						
            </px:PXPanel>
    </px:PXSmartPanel>
	<px:PXFormView ID="BAccount" runat="server" Width="100%" Caption="Vendor Summary" DataMember="BAccount" NoteIndicator="True" FilesIndicator="True" NotifyIndicator="True" ActivityIndicator="False" LinkIndicator="True"
		DefaultControlID="edAcctCD" DataSourceID="ds">
		<Template>
			<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" />
			<px:PXSegmentMask ID="edAcctCD" runat="server" DataField="AcctCD" DisplayMode="Value" DataSourceID="ds" FilterByAllFields="True" />
			<px:PXLayoutRule runat="server" ColumnSpan="2" />
			<px:PXTextEdit CommitChanges="True" ID="edAcctName" runat="server" DataField="AcctName" />
			<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="XS" ControlSize="S" />
			<px:PXDropDown ID="edStatus" runat="server" DataField="Status" CommitChanges="True"/>
			<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" />
			<px:PXFormView ID="VendorBalance" runat="server" DataMember="VendorBalance" DataSourceID="ds" RenderStyle="Simple">
				<Template>
					<px:PXLayoutRule runat="server" ControlSize="M" LabelsWidth="SM" StartColumn="True" />
					<px:PXNumberEdit ID="edBalance" runat="server" DataField="Balance" Enabled="False">
					</px:PXNumberEdit>
					<px:PXNumberEdit ID="edDepositsBalance" runat="server" DataField="DepositsBalance" Enabled="False">
					</px:PXNumberEdit>
				</Template>
			</px:PXFormView>
		</Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
	<px:PXTab ID="tab" runat="server" Height="494px" Style="z-index: 100" Width="100%" DataMember="CurrentVendor" MarkRequired="Dynamic" >
		<Activity HighlightColor="" SelectedColor="" Width="" Height=""></Activity>
		<Items>
			<px:PXTabItem Text="General Info" RepaintOnDemand="false">
				<Template>
					<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
					<px:PXFormView ID="DefContact" runat="server" Caption="Main Contact" DataMember="DefContact" RenderStyle="Fieldset" TabIndex="1100">
						<Template>
							<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
							<px:PXTextEdit ID="edFullName" runat="server" DataField="FullName" ></px:PXTextEdit>
							<px:PXTextEdit ID="edSalutation" runat="server" DataField="Salutation" ></px:PXTextEdit>
							<px:PXMailEdit ID="edEMail" runat="server" DataField="EMail" CommitChanges="True" ></px:PXMailEdit>
							<px:PXLinkEdit ID="edWebSite" runat="server" DataField="WebSite" CommitChanges="True" ></px:PXLinkEdit>
							<px:PXMaskEdit ID="edPhone1" runat="server" DataField="Phone1" ></px:PXMaskEdit>
							<px:PXMaskEdit ID="edPhone2" runat="server" DataField="Phone2" ></px:PXMaskEdit>
							<px:PXMaskEdit ID="edFax" runat="server" DataField="Fax" ></px:PXMaskEdit></Template>
					</px:PXFormView>
					<px:PXTextEdit ID="edAcctReferenceNbr" runat="server" DataField="AcctReferenceNbr" ></px:PXTextEdit>
					<px:PXSegmentMask ID="edParentBAccountID" runat="server" DataField="ParentBAccountID" AllowEdit="True" ></px:PXSegmentMask>
					<px:PXSelector runat="server" ID="edLocale" DataField="BAccount.LocaleName" DisplayMode="Text" ></px:PXSelector>
					<px:PXFormView ID="DefAddress" runat="server" Caption="Main Address" DataMember="DefAddress" SyncPosition="true" RenderStyle="Fieldset" TabIndex="1160">
						<Template>
							<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
							<px:PXCheckBox ID="edIsValidated" runat="server" DataField="IsValidated" Enabled="False" ></px:PXCheckBox>
							<px:PXTextEdit ID="edAddressLine1" runat="server" DataField="AddressLine1" ></px:PXTextEdit>
							<px:PXTextEdit ID="edAddressLine2" runat="server" DataField="AddressLine2" ></px:PXTextEdit>
							<px:PXTextEdit ID="edCity" runat="server" DataField="City" ></px:PXTextEdit>
							<px:PXSelector ID="edCountryID" runat="server" CommitChanges="True" AllowAddNew="True" DataField="CountryID" ></px:PXSelector>
							<px:PXSelector ID="edState" runat="server" AutoRefresh="True" DataField="State" AllowAddNew="True" CommitChanges="True" ></px:PXSelector>
							<px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
							<px:PXMaskEdit ID="edPostalCode" runat="server" CommitChanges="True" DataField="PostalCode" Size="s" ></px:PXMaskEdit>
							<px:PXButton ID="btnViewMainOnMap" runat="server" CommandName="ViewMainOnMap" CommandSourceID="ds" Text="View on Map" ></px:PXButton>
							<px:PXLayoutRule runat="server" ></px:PXLayoutRule>
						</Template>
					</px:PXFormView>
					<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" StartGroup="True" GroupCaption="Financial Settings" ></px:PXLayoutRule>
					<px:PXSelector CommitChanges="True" ID="edVendorClassID" runat="server" DataField="VendorClassID" AllowEdit="True" ></px:PXSelector>
					<px:PXSelector ID="edTermsID" runat="server" DataField="TermsID" AllowEdit="True" ></px:PXSelector>
					<px:PXLayoutRule ID="PXLayoutRule3" runat="server" Merge="True" ></px:PXLayoutRule>
					<px:PXSelector ID="edCuryID" runat="server" DataField="CuryID" Size="S" ></px:PXSelector>
					<px:PXCheckBox ID="chkAllowOverrideCury" runat="server" DataField="AllowOverrideCury" ></px:PXCheckBox>
					<px:PXLayoutRule ID="PXLayoutRule4" runat="server" ></px:PXLayoutRule>
					<px:PXLayoutRule ID="PXLayoutRule5" runat="server" Merge="True" ></px:PXLayoutRule>
					<px:PXSelector ID="edCuryRateTypeID" runat="server" DataField="CuryRateTypeID" Size="S" ></px:PXSelector>
					<px:PXCheckBox ID="chkAllowOverrideRate" runat="server" DataField="AllowOverrideRate" ></px:PXCheckBox>
					<px:PXLayoutRule ID="PXLayoutRule6" runat="server" ></px:PXLayoutRule>
					<px:PXLayoutRule runat="server" ID="CstPXLayoutRule24" StartGroup="True" />
					<px:PXCheckBox runat="server" ID="CstPXCheckBox14" DataField="UsrVATTaxPayer" />
					<px:PXLayoutRule ID="PXLayoutRule7" runat="server" GroupCaption="Vendor Properties" ></px:PXLayoutRule>
					<px:PXCheckBox ID="chkLandedCostVendor" runat="server"  AllowEdit="True" DataField="LandedCostVendor" CommitChanges="True"></px:PXCheckBox>
					<px:PXCheckBox CommitChanges="True" ID="chkTaxAgency" runat="server" DataField="TaxAgency" ></px:PXCheckBox>
					<px:PXSelector runat="server" ID="CstPXSelector14" DataField="UsrLegalFormID" AutoRefresh="True" />
					<px:PXCheckBox runat="server" ID="CstPXCheckBox9" DataField="UsrNonResident" />
                    <px:PXCheckBox ID="chkLaborUnion" runat="server"  AllowEdit="True" DataField="IsLaborUnion" CommitChanges="True"></px:PXCheckBox>
					<px:PXCheckBox ID="chkVendor1099" runat="server" DataField="Vendor1099" CommitChanges="True" ></px:PXCheckBox>
					<px:PXDropDown ID="edBox1099" runat="server" DataField="Box1099" ></px:PXDropDown>
                    <px:PXCheckBox runat="server" ID="edForeignEntity" DataField="ForeignEntity" Text="ForeignEntity" ></px:PXCheckBox>
					<px:PXCheckBox ID="chkFATCA" runat="server" DataField="FATCA" ></px:PXCheckBox>
					<px:PXCheckBox runat="server" ID="CstPXCheckBox11" DataField="UsrInsider" />
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Service Management" ></px:PXLayoutRule>
					<px:PXCheckBox runat="server" ID="edSDEnabled" DataField="SDEnabled" CommitChanges="True" AlignLeft="True" ></px:PXCheckBox>
					<px:PXCheckBox runat="server" ID="edSendAppNotification" DataField="SendAppNotification" CommitChanges="True" AlignLeft="True" ></px:PXCheckBox></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Bank Accounts">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid2" SkinID="DetailsInTab" Width="100%" DataSourceID="ds" AllowPaging="True">
						<Levels>
							<px:PXGridLevel DataMember="BankAccounts">
								<RowTemplate>
									<px:PXTextEdit runat="server" ID="CstPXTextEdit3" DataField="AccountName" />
									<px:PXTextEdit runat="server" ID="CstPXTextEdit5" DataField="BankRegistrationID" />
									<px:PXSelector runat="server" ID="CstPXSelector7" DataField="CurrencyID" />
									<px:PXTextEdit runat="server" ID="CstPXTextEdit8" DataField="IBANAccount" />
									<px:PXCheckBox runat="server" ID="CstPXCheckBox9" DataField="IsDefault" />
									<px:PXSelector runat="server" ID="CstPXSelector10" DataField="BankID" />
									<px:PXTextEdit runat="server" ID="CstPXTextEdit11" DataField="AGBank__BankName" />
									<px:PXSelector runat="server" ID="CstPXSelector12" DataField="AGBank__CountryID" />
									<px:PXTextEdit runat="server" ID="CstPXTextEdit17" DataField="IntermediaryBankName" />
									<px:PXSelector runat="server" ID="CstPXSelector19" DataField="IntermediaryBankID" CommitChanges="True" AllowEdit="" /></RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="IBANAccount" Width="84" CommitChanges="True" />
									<px:PXGridColumn DataField="AccountName" Width="140" />
									<px:PXGridColumn DataField="CurrencyID" Width="100" LinkCommand="ViewCurrency" />
									<px:PXGridColumn DataField="BankID" Width="100" LinkCommand="ViewBank" CommitChanges="True" />
									<px:PXGridColumn DataField="AGBank__BankName" Width="200" />
									<px:PXGridColumn DataField="IntermediaryBankID" Width="200" CommitChanges="True" LinkCommand="ViewBank" />
									<px:PXGridColumn DataField="IntermediaryBankName" Width="200" />
									<px:PXGridColumn DataField="BankRegistrationID" Width="200" CommitChanges="True" LinkCommand="" />
									<px:PXGridColumn DataField="AGBank__CountryID" Width="70" LinkCommand="ViewCountry" />
									<px:PXGridColumn DataField="IsDefault" Width="100" Type="CheckBox" /></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="200" /></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Payment Settings">
				<Template>
					<px:PXFormView ID="DefLocationPayment" runat="server" DataMember="DefLocation" SkinID="Transparent">
						<Template>
							<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" StartGroup="True" GroupCaption="Remittance Contact" ></px:PXLayoutRule>
							<px:PXCheckBox CommitChanges="True" ID="chkIsRemitContactSameAsMain" runat="server" DataField="IsRemitContactSameAsMain" ></px:PXCheckBox>
							<px:PXFormView ID="RemitContact" runat="server" DataMember="RemitContact" RenderStyle="Simple">
								<Template>
									<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
									<px:PXLinkEdit ID="edWebSite" runat="server" DataField="WebSite" CommitChanges="True" ></px:PXLinkEdit>
									<px:PXMailEdit ID="edEMail" runat="server" DataField="EMail" CommitChanges="True" ></px:PXMailEdit>
									<px:PXMaskEdit ID="edFax" runat="server" DataField="Fax" ></px:PXMaskEdit>
									<px:PXMaskEdit ID="edPhone1" runat="server" DataField="Phone1" ></px:PXMaskEdit>
									<px:PXMaskEdit ID="edPhone2" runat="server" DataField="Phone2" ></px:PXMaskEdit>
									<px:PXTextEdit ID="edSalutation" runat="server" DataField="Salutation" ></px:PXTextEdit>
									<px:PXTextEdit ID="edFullName" runat="server" DataField="FullName" ></px:PXTextEdit>
								</Template>
							</px:PXFormView>
							<px:PXLayoutRule runat="server" GroupCaption="Remittance Address" StartGroup="True" ></px:PXLayoutRule>
							<px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkIsRemitAddressSameAsMain" runat="server" DataField="IsRemitAddressSameAsMain" ></px:PXCheckBox>
							<px:PXFormView ID="RemitAddress" runat="server" DataMember="RemitAddress" SyncPosition="true" RenderStyle="Simple">
								<Template>
									<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
									<px:PXCheckBox ID="edIsValidated" runat="server" DataField="IsValidated" Enabled="False" ></px:PXCheckBox>
									<px:PXTextEdit ID="edAddressLine1" runat="server" DataField="AddressLine1" ></px:PXTextEdit>
									<px:PXTextEdit ID="edAddressLine2" runat="server" DataField="AddressLine2" ></px:PXTextEdit>
									<px:PXTextEdit ID="edCity" runat="server" DataField="City" ></px:PXTextEdit>
									<px:PXSelector ID="edCountryID" runat="server" AllowAddNew="True" AutoRefresh="True" CommitChanges="True" DataField="CountryID" ></px:PXSelector>
									<px:PXSelector ID="edState" runat="server" AllowAddNew="True" AutoRefresh="True" DataField="State" ></px:PXSelector>
									<px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
									<px:PXMaskEdit ID="edPostalCode" runat="server" CommitChanges="True" DataField="PostalCode" Size="s" ></px:PXMaskEdit>
									<px:PXButton ID="btnViewRemitOnMap" runat="server" CommandName="ViewRemitOnMap" CommandSourceID="ds" Size="xs" Text="View on Map" ></px:PXButton>
									<px:PXLayoutRule runat="server" ></px:PXLayoutRule>
								</Template>
							</px:PXFormView>
							<px:PXLayoutRule runat="server" ControlSize="XM" GroupCaption="Default Payment Settings" LabelsWidth="SM" StartColumn="True" StartGroup="True" ></px:PXLayoutRule>
							<px:PXSelector CommitChanges="True" ID="edVPaymentMethodID" runat="server" DataField="VPaymentMethodID" AutoRefresh="True"   AllowAddNew="True" ></px:PXSelector>
							<px:PXSegmentMask CommitChanges="True" ID="edVCashAccountID" runat="server" DataField="VCashAccountID" AllowAddNew="True" AutoRefresh="True" ></px:PXSegmentMask>
							<px:PXDropDown ID="edVPaymentByType" runat="server" DataField="VPaymentByType" ></px:PXDropDown>
							<px:PXNumberEdit ID="edVPaymentLeadTime" runat="server" DataField="VPaymentLeadTime" ></px:PXNumberEdit>
							<px:PXSegmentMask CommitChanges="True" ID="edPayToVendorID" runat="server" DataField="CurrentVendor.PayToVendorID" AllowEdit="True" AutoRefresh="True" ></px:PXSegmentMask>
							<px:PXCheckBox ID="chkVSeparateCheck" runat="server" DataField="VSeparateCheck" ></px:PXCheckBox>
							<px:PXTextEdit runat="server" ID="CstPXTextEdit13" DataField="UsrOpCode" />
							<px:PXGrid ID="grdPaymentDetails" runat="server" Caption="Payment Instructions" SkinID="Attributes" MatrixMode="True" Height="160px" Width="400px">
								<Levels>
									<px:PXGridLevel DataMember="PaymentDetails" DataKeyNames="BAccountID,LocationID,PaymentMethodID,DetailID">
										<Columns>
											<px:PXGridColumn DataField="PaymentMethodDetail__descr" Width="150px" ></px:PXGridColumn>
											<px:PXGridColumn DataField="DetailValue" Width="200px" ></px:PXGridColumn>
										</Columns>
										<Layout FormViewHeight="" ></Layout>
									</px:PXGridLevel>
								</Levels>
							</px:PXGrid></Template>
					</px:PXFormView></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Purchase Settings" LoadOnDemand="True">
				<Template>
					<px:PXFormView ID="DefLocation" runat="server" CaptionVisible="False" DataMember="DefLocation" Width="100%" SkinID="Transparent">
						<Template>
							<px:PXLayoutRule ID="PXLayoutRule8" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
							<px:PXLayoutRule ID="PXLayoutRule9" runat="server" StartGroup="True" GroupCaption="Shipper's Contact" ControlSize="XM" LabelsWidth="SM" ></px:PXLayoutRule>
							<px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkIsContactSameAsMain" runat="server" DataField="IsContactSameAsMain" ></px:PXCheckBox>
							<px:PXFormView ID="DefLocationContact" runat="server" DataMember="DefLocationContact" RenderStyle="Simple">
								<Template>
									<px:PXLayoutRule ID="PXLayoutRule10" runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
									<px:PXTextEdit ID="edFullName" runat="server" DataField="FullName" ></px:PXTextEdit>
									<px:PXTextEdit ID="edSalutation" runat="server" DataField="Salutation" ></px:PXTextEdit>
									<px:PXMaskEdit ID="edPhone1" runat="server" DataField="Phone1" ></px:PXMaskEdit>
									<px:PXMaskEdit ID="edPhone2" runat="server" DataField="Phone2" ></px:PXMaskEdit>
									<px:PXMaskEdit ID="edFax" runat="server" DataField="Fax" ></px:PXMaskEdit>
									<px:PXMailEdit ID="edEMail" runat="server" DataField="EMail" CommitChanges="True" ></px:PXMailEdit>
									<px:PXLinkEdit ID="edWebSite" runat="server" DataField="WebSite" CommitChanges="True" ></px:PXLinkEdit>
								</Template>
							</px:PXFormView>
							<px:PXLayoutRule ID="PXLayoutRule11" runat="server" GroupCaption="Shipper's Address" StartGroup="True" ControlSize="XM" LabelsWidth="SM" ></px:PXLayoutRule>
							<px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkIsMain" runat="server" DataField="IsAddressSameAsMain" ></px:PXCheckBox>
							<px:PXCheckBox ID="edIsValidated" runat="server" DataField="IsValidated" Enabled="False" ></px:PXCheckBox>
							<px:PXTextEdit ID="edAddressLine1" runat="server" DataField="AddressLine1" ></px:PXTextEdit>
							<px:PXTextEdit ID="edAddressLine2" runat="server" DataField="AddressLine2" ></px:PXTextEdit>
							<px:PXTextEdit ID="edCity" runat="server" DataField="City" ></px:PXTextEdit>
							<px:PXSelector ID="edCountryID" runat="server" CommitChanges="True" DataField="CountryID" AllowAddNew="True" ></px:PXSelector>
							<px:PXSelector ID="edState" runat="server" DataField="State" AutoRefresh="True" AllowAddNew="True" ></px:PXSelector>
							<px:PXLayoutRule ID="PXLayoutRule12" runat="server" Merge="True" ></px:PXLayoutRule>
							<px:PXMaskEdit Size="s" ID="edPostalCode" runat="server" CommitChanges="True" DataField="PostalCode" ></px:PXMaskEdit>
							<px:PXButton Size="xs" ID="btnViewDefLoactionOnMap" runat="server" CommandName="ViewDefLocationOnMap" CommandSourceID="ds" Text="View on Map" ></px:PXButton>
							<px:PXLayoutRule ID="PXLayoutRule13" runat="server" ></px:PXLayoutRule>
							<px:PXLayoutRule ID="PXLayoutRule14" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" StartGroup="True" GroupCaption="Default Location Settings" ></px:PXLayoutRule>
							<px:PXTextEdit ID="edDescr" runat="server" DataField="Descr" ></px:PXTextEdit>
							<px:PXSelector ID="edVBranchID" runat="server" DataField="VBranchID" AllowEdit="True" ></px:PXSelector>
							<px:PXSelector ID="edTaxZoneID" runat="server" DataField="VTaxZoneID" AllowEdit="True" ></px:PXSelector>
							<px:PXDropDown ID="edTaxCalcMode" runat="server" DataField="VTaxCalcMode" ></px:PXDropDown>
							<px:PXTextEdit ID="edTaxRegistrationID" runat="server" DataField="TaxRegistrationID" ></px:PXTextEdit>
							<px:PXLayoutRule ID="PXLayoutRule5" runat="server" Merge="True" ></px:PXLayoutRule>
							<px:PXCheckBox ID="chkVPrintOrder" runat="server" DataField="VPrintOrder" ></px:PXCheckBox>
							<px:PXCheckBox ID="chkVEmailOrder" runat="server" DataField="VEmailOrder" ></px:PXCheckBox>
							<px:PXLayoutRule ID="PXLayoutRule15" runat="server" ></px:PXLayoutRule>
							<px:PXLayoutRule ID="PXLayoutRule7" runat="server" StartGroup="True" GroupCaption="Shipping Instructions" ></px:PXLayoutRule>
							<px:PXSegmentMask ID="edVSiteID" runat="server" DataField="VSiteID" AllowEdit="True" CommitChanges="True" AutoRefresh="True" ></px:PXSegmentMask>
							<px:PXSelector ID="edShipTermsID" runat="server" DataField="VShipTermsID" AllowEdit="True" ></px:PXSelector>
							<px:PXSelector ID="edVCarrierID" runat="server" DataField="VCarrierID" AllowEdit="True" ></px:PXSelector>
							<px:PXSelector ID="edFOBPointID" runat="server" DataField="VFOBPointID" AllowEdit="True" ></px:PXSelector>
							<px:PXNumberEdit ID="edLeadTime" runat="server" DataField="VLeadTime" ></px:PXNumberEdit>
							<px:PXLayoutRule ID="PXLayoutRule16" runat="server" StartGroup="True" GroupCaption="Receipt Actions" ></px:PXLayoutRule>
							<px:PXNumberEdit ID="edVRcptQtyMin" runat="server" DataField="VRcptQtyMin" ></px:PXNumberEdit>
							<px:PXNumberEdit ID="edVRcptQtyMax" runat="server" DataField="VRcptQtyMax" ></px:PXNumberEdit>
							<px:PXNumberEdit ID="edVRcptQtyThreshold" runat="server" DataField="VRcptQtyThreshold" ></px:PXNumberEdit>
							<px:PXDropDown ID="edVRcptQtyAction" runat="server" DataField="VRcptQtyAction" ></px:PXDropDown>
						</Template>
					</px:PXFormView>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Locations" LoadOnDemand="False">
				<Template>
					<px:PXGrid ID="grdLocations" runat="server" AllowSearch="True" Height="99%" SkinID="DetailsInTab" Style="z-index: 100;" Width="100%">
						<LevelStyles>
							<RowForm Height="400px" Width="800px">
							</RowForm>
						</LevelStyles>
						<Mode AllowAddNew="False" AllowColMoving="False" AllowDelete="False" ></Mode>
						<ActionBar>
							<Actions>
								<Save Enabled="False" ></Save>
								<AddNew Enabled="False" ></AddNew>
								<Delete Enabled="False" ></Delete>
								<EditRecord Enabled="False" ></EditRecord>
							</Actions>
							<CustomItems>
								<px:PXToolBarButton Text="New Location">
									<AutoCallBack Command="NewLocation" Target="ds" ></AutoCallBack>
								    <PopupCommand Command="Refresh" Target="grdLocations" ></PopupCommand>
								</px:PXToolBarButton>
								<px:PXToolBarButton Key="cmdViewLocation" Text="Location Details">
									<AutoCallBack Command="ViewLocation" Target="ds" ></AutoCallBack>
								    <PopupCommand Command="Refresh" Target="grdLocations" ></PopupCommand>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Set as Default">
									<AutoCallBack Command="SetDefault" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
						<Levels>
							<px:PXGridLevel DataMember="Locations">
								<RowTemplate>
									<px:PXSelector ID="edLocationCD" runat="server" DataField="LocationCD" AllowEdit="True" ></px:PXSelector>
								</RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="IsActive" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="IsDefault" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
									<px:PXGridColumn AllowShowHide="False" DataField="LocationBAccountID" TextAlign="Right" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LocationID" TextAlign="Right" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LocationCD" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Descr" TextCase="Upper" Width="150px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="City" Width="130px" ></px:PXGridColumn>
									<px:PXGridColumn AutoCallBack="True" DataField="CountryID" RenderEditorText="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="State" RenderEditorText="True" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VTaxZoneID" Width="80px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VExpenseAcctID" Width="108px" AutoCallBack="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VExpenseSubID" Width="180px" ></px:PXGridColumn>
								</Columns>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize>
						<Mode AllowUpdate="False" AllowAddNew="False" AllowDelete="False"></Mode>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Contacts" LoadOnDemand="True">
				<Template>
					<px:PXGrid ID="grdContacts" runat="server" Height="100%" Width="100%" AllowSearch="True" SkinID="DetailsInTab">
						<Levels>
							<px:PXGridLevel DataMember="ExtContacts">
								<RowTemplate>
								</RowTemplate>
								<Columns>
									<px:PXGridColumn DataField="ContactBAccountID" TextAlign="Right" Visible="False" AllowShowHide="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ContactID" TextAlign="Right" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="IsActive" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Salutation" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ContactDisplayName" Width="280px" LinkCommand="ViewContact" ></px:PXGridColumn>
									<px:PXGridColumn DataField="City" TextCase="Upper" Width="180px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="EMail" Width="200px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Phone1" Width="140px" ></px:PXGridColumn>
								</Columns>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<Mode AllowAddNew="False" AllowDelete="False" AllowUpdate="false" ></Mode>
						<ActionBar DefaultAction="grdContacts">
							<Actions>
								<Save Enabled="False" ></Save>
								<AddNew Enabled="False" ></AddNew>
								<Delete Enabled="False" ></Delete>
							</Actions>
							<CustomItems>
								<px:PXToolBarButton Text="New Contact">
								    <AutoCallBack Command="NewContact" Target="ds" ></AutoCallBack>
								    <PopupCommand Command="Refresh" Target="grdContacts" ></PopupCommand>
								</px:PXToolBarButton>
								<px:PXToolBarButton Text="Contact Details">
								    <AutoCallBack Command="ViewContact" Target="ds" ></AutoCallBack>
								    <PopupCommand Command="Refresh" Target="grdContacts" ></PopupCommand>
								</px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Industries">
				<Template>
					<px:PXGrid runat="server" ID="grdIndustries" SkinID="DetailsInTab" Width="100%" DataSourceID="ds" AllowPaging="True">
						<Levels>
							<px:PXGridLevel DataMember="Industries">
								<Columns>
									<px:PXGridColumn DataField="ItemClassID" Width="120" CommitChanges="True" />
									<px:PXGridColumn DataField="ItemClassID_description" Width="200">
										<Header Text="" /></px:PXGridColumn></Columns>
								<RowTemplate>
									<px:PXSegmentMask runat="server" ID="CstPXSegmentMask10" DataField="ItemClassID" SelectMode="Segment" /></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="200" /></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Attributes">
				<Template>
					<px:PXGrid ID="PXGridAnswers" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%"
						Height="200px" MatrixMode="True">
						<Levels>
							<px:PXGridLevel DataMember="Answers">
								<Columns>
									<px:PXGridColumn DataField="AttributeID" TextAlign="Left" Width="250px" AllowShowHide="False"
										TextField="AttributeID_description" ></px:PXGridColumn>
    								<px:PXGridColumn DataField="isRequired" TextAlign="Center" Type="CheckBox" Width="75px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Value" Width="300px" AllowShowHide="False" AllowSort="False" ></px:PXGridColumn>
								</Columns>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="200" ></AutoSize>
						<ActionBar>
							<Actions>
								<Search Enabled="False" ></Search>
							</Actions>
						</ActionBar>
                        <Mode AllowAddNew="False" AllowColMoving="False" AllowDelete="False" ></Mode>
					</px:PXGrid>
				</Template>
			</px:PXTabItem>
             <px:PXTabItem Text="Activities" LoadOnDemand="True">
				<Template>
					<pxa:PXGridWithPreview ID="gridActivities" runat="server" DataSourceID="ds" Width="100%"
						AllowSearch="True" DataMember="Activities" AllowPaging="true" NoteField="NoteText"
						FilesField="NoteFiles" BorderWidth="0px" GridSkinID="Inquire" SplitterStyle="z-index: 100; border-top: solid 1px Gray;  border-bottom: solid 1px Gray"
						PreviewPanelStyle="z-index: 100; background-color: Window" PreviewPanelSkinID="Preview"
						BlankFilterHeader="All Activities" MatrixMode="true" PrimaryViewControlID="form">
						<ActionBar DefaultAction="cmdViewActivity" CustomItemsGroup="0" PagerVisible="False">
							<CustomItems>
								<px:PXToolBarButton Key="cmdAddTask">
									<AutoCallBack Command="NewTask" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Key="cmdAddEvent">
									<AutoCallBack Command="NewEvent" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Key="cmdAddEmail">
									<AutoCallBack Command="NewMailActivity" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
								<px:PXToolBarButton Key="cmdAddActivity">
									<AutoCallBack Command="NewActivity" Target="ds" ></AutoCallBack>
								</px:PXToolBarButton>
							</CustomItems>
						</ActionBar>
						<Levels>
							<px:PXGridLevel DataMember="Activities">
								<Columns>
									<px:PXGridColumn DataField="IsCompleteIcon" Width="21px" AllowShowHide="False" AllowResize="False"
										ForceExport="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PriorityIcon" Width="21px" AllowShowHide="False" AllowResize="False"
										ForceExport="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CRReminder__ReminderIcon" Width="21px" AllowShowHide="False" AllowResize="False"
										ForceExport="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ClassIcon" Width="31px" AllowShowHide="False" AllowResize="False"
										ForceExport="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ClassInfo" Width="60px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="RefNoteID" Visible="false" AllowShowHide="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Subject" LinkCommand="ViewActivity" Width="297px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UIStatus" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Released" TextAlign="Center" Type="CheckBox" Width="80px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="StartDate" DisplayFormat="g" Width="120px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CreatedDateTime" DisplayFormat="g" Width="120px" Visible="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TimeSpent" Width="80px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CreatedByID" Visible="false" AllowShowHide="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CreatedByID_Creator_Username" Visible="false"
										SyncVisible="False" SyncVisibility="False" Width="108px">
										<NavigateParams>
											<px:PXControlParam Name="PKID" ControlID="gridActivities" PropertyName="DataValues[&quot;CreatedByID&quot;]" ></px:PXControlParam>
										</NavigateParams>
									</px:PXGridColumn>
									<px:PXGridColumn DataField="WorkgroupID" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OwnerID" LinkCommand="OpenActivityOwner" Width="150px" DisplayMode="Text"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ProjectID" Width="80px" AllowShowHide="true" Visible="false" SyncVisible="false" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ProjectTaskID" Width="80px" AllowShowHide="true" Visible="false" SyncVisible="false"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Source" Width="120" AllowResize="True" ></px:PXGridColumn>
								</Columns>
							</px:PXGridLevel>
						</Levels>
						<PreviewPanelTemplate>
							<px:PXHtmlView ID="edBody" runat="server" DataField="body" TextMode="MultiLine"
								MaxLength="50" Width="100%" Height="100%" SkinID="Label" >
                                      <AutoSize Container="Parent" Enabled="true" ></AutoSize>
                                </px:PXHtmlView>
						</PreviewPanelTemplate>
						<AutoSize Enabled="true" ></AutoSize>
						<GridMode AllowAddNew="False" AllowDelete="False" AllowFormEdit="False" AllowUpdate="False" AllowUpload="False" ></GridMode>
					</pxa:PXGridWithPreview>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="GL Accounts" LoadOnDemand="False">
				<Template>
					<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
					<px:PXFormView ID="DefLocationGLAccounts" runat="server" DataMember="DefLocation" RenderStyle="Simple" TabIndex="2100" MarkRequired="Dynamic">
						<Template>
							<px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
							<px:PXSegmentMask ID="edVAPAccountID" runat="server" CommitChanges="True" DataField="VAPAccountID" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edVAPSubID" runat="server" DataField="VAPSubID" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edVExpenseAcctID" runat="server" CommitChanges="True" DataField="VExpenseAcctID" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edVExpenseSubID" runat="server" DataField="VExpenseSubID" ></px:PXSegmentMask>
						    <px:PXSegmentMask ID="edVDiscountAcctID" runat="server" CommitChanges="True" DataField="VDiscountAcctID" ></px:PXSegmentMask>
							<px:PXSegmentMask ID="edVDiscountSubID" runat="server" DataField="VDiscountSubID" ></px:PXSegmentMask>
						</Template>
					</px:PXFormView>
					<px:PXSegmentMask CommitChanges="True" ID="edDiscTakenAcctID" runat="server" DataField="DiscTakenAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask ID="edDiscTakenSubID" runat="server" DataField="DiscTakenSubID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edPrepaymentAcctID" runat="server" DataField="PrepaymentAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask ID="edPrepaymentSubID" runat="server" DataField="PrepaymentSubID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edPOAccrualAcctID" runat="server" DataField="POAccrualAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask ID="edPOAccrualSubID" runat="server" DataField="POAccrualSubID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edPrebookAcctID" runat="server" DataField="PrebookAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask ID="edPrebookSubID" runat="server" DataField="PrebookSubID" ></px:PXSegmentMask>
				</Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Tax Agency Settings" BindingContext="tab" VisibleExp="DataControls[&quot;chkTaxAgency&quot;].Value = 1">
				<Template>
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Default Tax Accounts" 
						StartColumn="True" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
                    <px:PXSegmentMask CommitChanges="True" ID="edSalesTaxAcctID" runat="server" DataField="SalesTaxAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edSalesTaxSubID" runat="server" DataField="SalesTaxSubID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edPurchTaxAcctID" runat="server" DataField="PurchTaxAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edPurchTaxSubID" runat="server" DataField="PurchTaxSubID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edTaxExpenseAcctID" runat="server" DataField="TaxExpenseAcctID" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edTaxExpenseSubID" runat="server" DataField="TaxExpenseSubID" ></px:PXSegmentMask>
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Pending VAT Settings" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
					<px:PXDropDown ID="edSVATReversalMethod" runat="server" DataField="SVATReversalMethod" SelectedIndex="-1" CommitChanges="True" ></px:PXDropDown>
					<px:PXDropDown ID="edSVATInputTaxEntryRefNbr" runat="server" DataField="SVATInputTaxEntryRefNbr" SelectedIndex="-1" CommitChanges="True" ></px:PXDropDown>
					<px:PXDropDown ID="edSVATOutputTaxEntryRefNbr" runat="server" DataField="SVATOutputTaxEntryRefNbr" SelectedIndex="-1" CommitChanges="True" ></px:PXDropDown>
					<px:PXSelector ID="edSVATTaxInvoiceNumberingID" runat="server" DataField="SVATTaxInvoiceNumberingID" CommitChanges="True" ></px:PXSelector>
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Tax Report Settings" 
						StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
					<px:PXDropDown ID="edTaxPeriodType" runat="server" DataField="TaxPeriodType" CommitChanges="true" ></px:PXDropDown>
					<px:PXCheckBox ID="chktaxReportFinPeriod" runat="server" DataField="TaxReportFinPeriod" ></px:PXCheckBox>
					<px:PXCheckBox ID="chkUpdClosedTaxPeriods" runat="server" DataField="UpdClosedTaxPeriods" ></px:PXCheckBox>
                    <px:PXCheckBox ID="chkAutoGenerateTaxBill" runat="server" DataField="AutoGenerateTaxBill" ></px:PXCheckBox>
					<px:PXDropDown ID="edTaxReportRounding" runat="server" DataField="TaxReportRounding" ></px:PXDropDown>					
					<px:PXLayoutRule runat="server" Merge="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
					<px:PXNumberEdit ID="edTaxReportPrecision" runat="server" DataField="TaxReportPrecision" Size="xxs" ></px:PXNumberEdit>
                    <px:PXCheckBox ID="chkTaxUseVendorCurPrecision" runat="server" DataField="TaxUseVendorCurPrecision" CommitChanges="true" ></px:PXCheckBox>
					<px:PXLayoutRule runat="server" Merge="False" ></px:PXLayoutRule>
				</Template>
			</px:PXTabItem>
            <px:PXTabItem Text="Mailing Settings">
				<Template>
					<px:PXSplitContainer runat="server" ID="sp1" SplitterPosition="350" SkinID="Horizontal" Height="494px">
						<AutoSize Enabled="true" ></AutoSize>
						<Template1>
							<px:PXGrid ID="gridNS" runat="server" SkinID="DetailsInTab" Width="100%" Height="150px" Caption="Mailings" AdjustPageSize="Auto" AllowPaging="True" DataSourceID="ds">
								<AutoSize Enabled="True" ></AutoSize>
								<AutoCallBack Target="gridNR" Command="Refresh" ></AutoCallBack>
								<Levels>
									<px:PXGridLevel DataMember="NotificationSources" DataKeyNames="SourceID,SetupID">
										<RowTemplate>
											<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" ></px:PXLayoutRule>
											<px:PXDropDown ID="edFormat" runat="server" DataField="Format" ></px:PXDropDown>
											<px:PXSegmentMask ID="edNBranchID" runat="server" DataField="NBranchID" ></px:PXSegmentMask>
											<px:PXCheckBox ID="chkActive" runat="server" Checked="True" DataField="Active" ></px:PXCheckBox>
											<px:PXSelector ID="edSetupID" runat="server" DataField="SetupID" ></px:PXSelector>
											<px:PXSelector ID="edReportID" runat="server" DataField="ReportID" ValueField="ScreenID" ></px:PXSelector>
											<px:PXSelector ID="edNotificationID" runat="server" DataField="NotificationID" ValueField="Name" ></px:PXSelector>
											<px:PXSelector ID="edEMailAccountID" runat="server" DataField="EMailAccountID" DisplayMode="Text" ></px:PXSelector>
										</RowTemplate>
										<Columns>
											<px:PXGridColumn DataField="SetupID" Width="108px" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="NBranchID" AutoCallBack="True" Label="Branch" ></px:PXGridColumn>
											<px:PXGridColumn DataField="EMailAccountID" Width="200px" DisplayMode="Text" ></px:PXGridColumn>
											<px:PXGridColumn DataField="ReportID" Width="150px" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="NotificationID" Width="150px" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="Format" Width="54px" RenderEditorText="True" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="Active" TextAlign="Center" Type="CheckBox" ></px:PXGridColumn>
											<px:PXGridColumn DataField="OverrideSource" TextAlign="Center" Type="CheckBox" AutoCallBack="True" ></px:PXGridColumn>
										</Columns>
										<Layout FormViewHeight="" ></Layout>
									</px:PXGridLevel>
								</Levels>
							</px:PXGrid>
						</Template1>
						<Template2>
							<px:PXGrid ID="gridNR" runat="server" SkinID="DetailsInTab" Width="100%" Caption="Recipients" AdjustPageSize="Auto" AllowPaging="True" DataSourceID="ds">
								<AutoSize Enabled="True" ></AutoSize>
								<Mode InitNewRow="True"></Mode>
								<Parameters>
									<px:PXSyncGridParam ControlID="gridNS" ></px:PXSyncGridParam>
								</Parameters>
								<CallbackCommands>
									<Save RepaintControls="None" RepaintControlsIDs="ds" ></Save>
									<FetchRow RepaintControls="None" ></FetchRow>
								</CallbackCommands>
								<Levels>
									<px:PXGridLevel DataMember="NotificationRecipients" DataKeyNames="NotificationID">
										<Mode InitNewRow="True"></Mode>
										<Columns>
											<px:PXGridColumn DataField="ContactType" RenderEditorText="True" Width="100px" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="OriginalContactID" Visible="False" AllowShowHide="False" ></px:PXGridColumn>
											<px:PXGridColumn DataField="ContactID" Width="200px">
												<NavigateParams>
													<px:PXControlParam Name="ContactID" ControlID="gridNR" PropertyName="DataValues[&quot;OriginalContactID&quot;]" ></px:PXControlParam>
												</NavigateParams>
											</px:PXGridColumn>
											<px:PXGridColumn DataField="Email" Width="200px" ></px:PXGridColumn>
											<px:PXGridColumn DataField="Format" RenderEditorText="True" Width="60px" AutoCallBack="True" ></px:PXGridColumn>
											<px:PXGridColumn DataField="Active" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
											<px:PXGridColumn DataField="Hidden" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
										</Columns>
										<RowTemplate>
											<px:PXLayoutRule ID="PXLayoutRule2" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" ></px:PXLayoutRule>
											<px:PXDropDown ID="edContactType" runat="server" DataField="ContactType" ></px:PXDropDown>
											<px:PXSelector ID="edContactID" runat="server" DataField="ContactID" AutoRefresh="True" ValueField="DisplayName" AllowEdit="True" ></px:PXSelector>
										</RowTemplate>
										<Layout FormViewHeight="" ></Layout>
									</px:PXGridLevel>
								</Levels>
							</px:PXGrid>
						</Template2>
					</px:PXSplitContainer>
				</Template>
			</px:PXTabItem>
            <px:PXTabItem Text="Supplied-by Vendors" RepaintOnDemand="False">
				<Template>
					<px:PXGrid ID="PXGridSuppliedByVendors" runat="server" DataSourceID="ds" SkinID="Inquire" Width="100%" Height="200px">
						<Levels>
							<px:PXGridLevel DataMember="SuppliedByVendors">
								<Columns>
									<px:PXGridColumn DataField="AcctCD" Width="100px" LinkCommand="viewDetails" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AcctName" Width="250px" ></px:PXGridColumn>
								</Columns>
								<Layout FormViewHeight="" ></Layout>
							</px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" MinHeight="200" ></AutoSize>
					</px:PXGrid>
				</Template>
			</px:PXTabItem></Items>
		<AutoSize Container="Window" Enabled="True" MinWidth="300" ></AutoSize>
	</px:PXTab></asp:Content>

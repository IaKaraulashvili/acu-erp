<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="CM101000.aspx.cs" Inherits="Page_CM101000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" AutoCallBack="True" Visible="True" Width="100%" PrimaryView="cmsetup" TypeName="PX.Objects.CM.CMSetupMaint">
		<CallbackCommands>
			<px:PXDSCallbackCommand CommitChanges="True" Name="Save" />
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
	<px:PXFormView ID="formSettings" runat="server" DataSourceID="ds" Width="100%" DataMember="cmsetup" Caption="CM Settings" TemplateContainer="" TabIndex="900">
		<AutoSize Container="Window" Enabled="True" ></AutoSize>
		<Template>
			<px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="M" ></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" GroupCaption="Setup and Numbering Settings" StartGroup="True" ></px:PXLayoutRule>
			<px:PXSelector ID="edBatchNumberingID" runat="server" DataField="BatchNumberingID" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edTranslNumberingID" runat="server" DataField="TranslNumberingID" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edExtRefNbrNumberingID" runat="server" AllowEdit="True" DataField="ExtRefNbrNumberingID" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSegmentMask ID="edGainLossSubMask" runat="server" DataField="GainLossSubMask" AllowEdit="True" DataSourceID="ds" ></px:PXSegmentMask>
			<px:PXLayoutRule runat="server" GroupCaption="Posting Settings" StartGroup="True" ></px:PXLayoutRule>
			<px:PXCheckBox ID="chkAutoPostOption" runat="server" DataField="AutoPostOption" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Data Entry Settings" ></px:PXLayoutRule>
			<px:PXCheckBox ID="chkRateVarianceWarn" runat="server" DataField="RateVarianceWarn" ></px:PXCheckBox>
			<px:PXNumberEdit ID="edRateVariance" runat="server" DataField="RateVariance" Size="XXS" ></px:PXNumberEdit>
			<px:PXSelector ID="edTranslDefId" runat="server" DataField="TranslDefId" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Default Rate Types" ></px:PXLayoutRule>
			<px:PXSelector ID="edGLRateTypeDflt" runat="server" DataField="GLRateTypeDflt" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edGLRateTypeReval" runat="server" DataField="GLRateTypeReval" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edCARateTypeDflt" runat="server" DataField="CARateTypeDflt" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edARRateTypeDflt" runat="server" DataField="ARRateTypeDflt" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edARRateTypeReval" runat="server" DataField="ARRateTypeReval" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edAPRateTypeDflt" runat="server" DataField="APRateTypeDflt" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXSelector ID="edAPRateTypeReval" runat="server" DataField="APRateTypeReval" AllowEdit="True" DataSourceID="ds" edit="1" ></px:PXSelector>
			<px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="M" ></px:PXLayoutRule>
			<px:PXFormView ID="basecurrency" runat="server" RenderStyle="Simple" DataMember="basecurrency" DataSourceID="ds" TabIndex="1100">
				<Template>
					<px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="M" ></px:PXLayoutRule>
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Realized Gain and Loss Settings" ></px:PXLayoutRule>
					<px:PXSegmentMask CommitChanges="True" ID="edRealGainAcctID" runat="server" DataField="RealGainAcctID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edRealGainSubID" runat="server" DataField="RealGainSubID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edRealLossAcctID" runat="server" DataField="RealLossAcctID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edRealLossSubID" runat="server" DataField="RealLossSubID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Translation Gain and Loss Settings" ></px:PXLayoutRule>
					<px:PXSegmentMask CommitChanges="True" ID="edTranslationGainAcctID" runat="server" DataField="TranslationGainAcctID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edTranslationGainSubID" runat="server" DataField="TranslationGainSubID" AutoRefresh="True" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edTranslationLossAcctID" runat="server" DataField="TranslationLossAcctID" DataSourceID="ds" ></px:PXSegmentMask>
					<px:PXSegmentMask CommitChanges="True" ID="edTranslationLossSubID" runat="server" DataField="TranslationLossSubID" AutoRefresh="True" DataSourceID="ds" ></px:PXSegmentMask>
				</Template>
			</px:PXFormView>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule3" StartGroup="True" GroupCaption="NBG" />
			<px:PXCheckBox runat="server" ID="CstUsrUseNBGService" DataField="UsrUseNBGService" CommitChanges="True" />
			<px:PXTextEdit runat="server" ID="CstUsrNBGServiceUrl" DataField="UsrNBGServiceUrl" /></Template>
	</px:PXFormView>
</asp:Content>


<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="PO301000.aspx.cs"
    Inherits="Page_PO301000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>
<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="PX.Objects.PO.POOrderEntry" PrimaryView="Document" Width="100%">
		<CallbackCommands>
			<px:PXDSCallbackCommand CommitChanges="True" Name="GoToNodeSelectedInTree" Visible="false" PostData="Self" />
			<px:PXDSCallbackCommand Name="Insert" PostData="Self" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand CommitChanges="True" Name="Save" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="First" PostData="Self" StartNewGroup="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="Last" PostData="Self" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand StartNewGroup="True" Name="Action" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="Hold" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="NewVendor" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="EditVendor" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOOrder" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="AddPOOrderLine" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="false" Name="CreatePOReceipt" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="CurrencyView" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="CurrencyView" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="AddInvBySite" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="AddInvBySite1" Visible="False" />
			<px:PXDSCallbackCommand Name="AddInvSelBySite" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewDemand" Visible="false" DependOnGrid="grid" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewTask" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewEvent" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewActivity" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="NewMailActivity" Visible="False" CommitChanges="True" PopupCommand="Cancel" PopupCommandTarget="ds" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand StartNewGroup="True" Name="ValidateAddresses" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="RecalculateDiscountsAction" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="RecalcOk" PopupCommand="" PopupCommandTarget="" PopupPanel="" Text="" Visible="False" ></px:PXDSCallbackCommand>       
            <px:PXDSCallbackCommand Name="CreatePrepayment" Visible="False" CommitChanges="True" PopupCommand="Cancel" PopupCommandTarget="ds" ></px:PXDSCallbackCommand>   
            <px:PXDSCallbackCommand Name="PasteLine" Visible="False" CommitChanges="true" DependOnGrid="grid" ></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="ResetOrder" Visible="False" CommitChanges="true" DependOnGrid="grid" ></px:PXDSCallbackCommand></CallbackCommands>
		<DataTrees>
			<px:PXTreeDataMember TreeView="_EPCompanyTree_Tree_" TreeKeys="WorkgroupID" ></px:PXTreeDataMember>
		</DataTrees>
	</px:PXDataSource>
    <px:PXSmartPanel ID="PanelAddPOLine" runat="server" Height="370px" Style="z-index: 108; position: absolute; left: 660px;
        top: 99px" Width="960px" Key="poLinesSelection" Caption="Add Purchase Order Line" CaptionVisible="True" LoadOnDemand="true" AutoRepaint="true" 
        ShowAfterLoad="true">
        <px:PXFormView ID="frmPOFilter" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="filter" Caption="PO Selection"
            CaptionVisible="false" SkinID="Transparent">
            <Template>
                <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" />
                <px:PXDropDown CommitChanges="True" ID="edOrderType" runat="server" AllowNull="False" DataField="OrderType" SelectedIndex="2" />
                <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" />
                <px:PXSelector CommitChanges="True" ID="edOrderNbr" runat="server" DataField="OrderNbr" AutoRefresh="True" />
            </Template>
        </px:PXFormView>
        <px:PXGrid ID="gridOL" runat="server" Height="240px" Width="100%" DataSourceID="ds" Style="border-width: 1px 0px" AutoAdjustColumns="true">
            <Levels>
                <px:PXGridLevel DataMember="poLinesSelection">
                    <Columns>
                        <px:PXGridColumn DataField="Selected" Width="60px" Type="CheckBox" AllowCheckAll="True" />
                        <px:PXGridColumn AllowNull="False" DataField="LineType" Width="90px" />
                        <px:PXGridColumn DataField="InventoryID" DisplayFormat="&gt;AAAAAAAAAA" />
                        <px:PXGridColumn DataField="SubItemID" DisplayFormat="&gt;AA-A-A" Width="60px" />
                        <px:PXGridColumn DataField="UOM" Width="63px" DisplayFormat="&gt;aaaaaa" />
                        <px:PXGridColumn DataField="OrderQty" Width="100px" TextAlign="Right" />
                        <px:PXGridColumn DataField="LeftToReceiveQty" Width="108px" TextAlign="Right" />
                        <px:PXGridColumn DataField="TranDesc" Width="200px" />
                        <px:PXGridColumn AllowNull="False" DataField="RcptQtyMin" TextAlign="Right" Width="100px" />
                        <px:PXGridColumn AllowNull="False" DataField="RcptQtyMax" TextAlign="Right" Width="100px" />
                        <px:PXGridColumn AllowNull="False" DataField="RcptQtyAction" Type="DropDownList" Width="100px" />
                    </Columns>
                    <Mode AllowAddNew="false" AllowUpdate="false" AllowDelete="false" />
                </px:PXGridLevel>
            </Levels>
            <AutoSize Enabled="true" />
        </px:PXGrid>
        <px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton1" runat="server" DialogResult="OK" Text="Save" />
            <px:PXButton ID="PXButton2" runat="server" DialogResult="No" Text="Cancel" />
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXSmartPanel ID="PanelAddPO" runat="server" Height="415px" Style="z-index: 108; left: 486px; position: absolute; top: 99px"
        Width="960px" Caption="Add Purchase Order" CaptionVisible="True" LoadOnDemand="true" Key="openOrders" ShowAfterLoad="true"
        AutoCallBack-Enabled="True" AutoCallBack-Target="grdOpenOrders" AutoCallBack-Command="Refresh">
        <px:PXGrid ID="grdOpenOrders" runat="server" Height="340px" Width="100%" DataSourceID="ds" Style="border-width: 1px 0px;
            left: 0px; top: 2px;" AutoAdjustColumns="true">
            <AutoSize Enabled="true" />
            <Levels>
                <px:PXGridLevel DataMember="openOrders">
                    <Columns>
                        <px:PXGridColumn AllowCheckAll="True" AllowNull="False" AutoCallBack="True" DataField="Selected" TextAlign="Center" Type="CheckBox"
                            Width="60px" />
                        <px:PXGridColumn DataField="OrderType" />
                        <px:PXGridColumn DataField="OrderNbr" />
                        <px:PXGridColumn DataField="OrderDate" Width="90px" />
                        <px:PXGridColumn DataField="ExpirationDate" Width="90px" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="Status" />
                        <px:PXGridColumn DataField="CuryID" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="CuryOrderTotal" Width="100px" TextAlign="Right" />
                        <px:PXGridColumn DataField="VendorRefNbr" />
                        <px:PXGridColumn DataField="TermsID" />
                        <px:PXGridColumn DataField="OrderDesc" Width="200px" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="LeftToReceiveQty" TextAlign="Right" Width="100px" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="CuryLeftToReceiveCost" TextAlign="Right" Width="100px" />
                    </Columns>
                </px:PXGridLevel>
            </Levels>
        </px:PXGrid>
        <px:PXPanel ID="PXPanel2" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton3" runat="server" DialogResult="OK" Text="Save" />
            <px:PXButton ID="PXButton4" runat="server" DialogResult="No" Text="Cancel" />
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXSmartPanel ID="PanelFixedDemand" runat="server" Height="415px" Style="z-index: 108; left: 486px; position: absolute;
        top: 99px" Width="960px" Caption="Demand" CaptionVisible="True" LoadOnDemand="true" Key="FixedDemand" ShowAfterLoad="true"
        AutoCallBack-Enabled="True" AutoCallBack-Target="gridFixedDemand" AutoCallBack-Command="Refresh">
        <px:PXGrid ID="gridFixedDemand" runat="server" Height="340px" Width="100%" DataSourceID="ds" Style="border-width: 1px 0px;
            left: 0px; top: 2px;" AutoAdjustColumns="true">
            <AutoSize Enabled="true" />
            <Levels>
                <px:PXGridLevel DataMember="FixedDemand">
                    <Columns>
                        <px:PXGridColumn DataField="OrderType" />
                        <px:PXGridColumn DataField="OrderNbr" />
                        <px:PXGridColumn DataField="RequestDate" Width="90px" />
                        <px:PXGridColumn DataField="CustomerID" Width="90px" />
                        <px:PXGridColumn DataField="SiteID" Width="90px" />
                        <px:PXGridColumn AllowUpdate="False" DataField="UOM" DisplayFormat="&gt;aaaaaa" Label="Orig. UOM" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="OrderQty" Label="Orig. Quantity" TextAlign="Right" Width="100px" />
                        <px:PXGridColumn AllowUpdate="False" DataField="POUOM" DisplayFormat="&gt;aaaaaa" Label="UOM" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="POUOMOrderQty" Label="Quantity" TextAlign="Right" Width="100px" />
                        <px:PXGridColumn DataField="INItemPlan__Active" Width="60px" AllowNull="False" TextAlign="Center" Type="CheckBox" />
                    </Columns>
					<RowTemplate>
						<px:PXSelector ID="edOrderNbr" DataField="OrderNbr" runat="server" AllowEdit="True" />
					</RowTemplate>
                </px:PXGridLevel>
            </Levels>
        </px:PXGrid>
        <px:PXPanel ID="PXPanel3" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton5" runat="server" DialogResult="Cancel" Text="Close" />
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXSmartPanel ID="PanelReplenishment" runat="server" Height="415px" Style="z-index: 108; left: 486px; position: absolute;
        top: 99px" Width="960px" Caption="Demand" CaptionVisible="True" LoadOnDemand="True" Key="ReplenishmentLines" ShowAfterLoad="True"
        AutoCallBack-Target="gridReplenishmentLines" AutoCallBack-Command="Refresh">
        <px:PXGrid ID="gridReplenishmentLines" runat="server" Height="340px" Width="100%" DataSourceID="ds" Style="border-width: 1px 0px;
            left: 0px; top: 2px;" AutoAdjustColumns="true">
            <AutoSize Enabled="true" />
            <Levels>
                <px:PXGridLevel DataMember="ReplenishmentLines">
                    <Columns>
                        <px:PXGridColumn DataField="RefNbr" />
                        <px:PXGridColumn DataField="OrderDate" Width="90px" />
                        <px:PXGridColumn DataField="UOM" Width="90px" />
                        <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="Qty" TextAlign="Right" Width="100px" />
                    </Columns>
                </px:PXGridLevel>
            </Levels>
        </px:PXGrid>
        <px:PXPanel ID="PXPanel4" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton9" runat="server" DialogResult="Cancel" Text="Close"/>
        </px:PXPanel>
    </px:PXSmartPanel>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Document" Caption="Document Summary"
        NoteIndicator="True" FilesIndicator="True" LinkIndicator="true" ActivityIndicator="true" ActivityField="NoteActivity"
        emailinggraph="PX.Objects.CR.CREmailActivityMaint,PX.Objects" DefaultControlID="edOrderType" NotifyIndicator="True">
        <CallbackCommands>
            <Save PostData="Self" ></Save>
        </CallbackCommands>
        <Activity HighlightColor="" SelectedColor="" Width="" Height=""></Activity>
        <Parameters>
            <px:PXControlParam ControlID="form" Name="POOrder.orderType" PropertyName="NewDataKey[&quot;OrderType&quot;]" Type="String" ></px:PXControlParam>
        </Parameters>
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="S" ></px:PXLayoutRule>
            <px:PXDropDown ID="edOrderType" runat="server" DataField="OrderType" SelectedIndex="-1"></px:PXDropDown>
            <px:PXSelector ID="edOrderNbr" runat="server" DataField="OrderNbr" AutoRefresh="true">
                <GridProperties FastFilterFields="VendorRefNbr,VendorID,VendorID_Vendor_acctName">
                    <PagerSettings Mode="NextPrevFirstLast" ></PagerSettings>
                </GridProperties>
            </px:PXSelector>
            <px:PXCheckBox ID="chkHold" runat="server" DataField="Hold">
                <AutoCallBack Command="Hold" Target="ds">
                </AutoCallBack>
            </px:PXCheckBox>
            <px:PXDropDown ID="edStatus" runat="server" AllowNull="False" DataField="Status" Enabled="False" ></px:PXDropDown>
            <px:PXCheckBox ID="chkRequestApproval" runat="server" DataField="RequestApproval" ></px:PXCheckBox>                    
            <px:PXCheckBox ID="chkApproved" runat="server" DataField="Approved" ></px:PXCheckBox>
	<px:PXCheckBox runat="server" ID="CstPXCheckBox2" DataField="UsrFARelated" CommitChanges="True" />
            <px:PXDateTimeEdit CommitChanges="True" ID="edOrderDate" runat="server" DataField="OrderDate" ></px:PXDateTimeEdit>
            <px:PXDateTimeEdit CommitChanges="True" ID="edExpectedDate" runat="server" DataField="ExpectedDate" ></px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edExpirationDate" runat="server" DataField="ExpirationDate" ></px:PXDateTimeEdit>
            <px:PXLayoutRule runat="server" ColumnSpan="2" ></px:PXLayoutRule>
            <px:PXTextEdit ID="edOrderDesc" runat="server" DataField="OrderDesc" ></px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" ></px:PXLayoutRule>
            <px:PXSegmentMask CommitChanges="True" ID="edVendorID" runat="server" DataField="VendorID" AllowAddNew="True" AllowEdit="True" AutoRefresh="True" ></px:PXSegmentMask>
	<px:PXSelector runat="server" ID="erUsrContractID" DataField="UsrContractID" AutoRefresh="True" CommitChanges="True" />
            <px:PXSegmentMask CommitChanges="True" ID="edVendorLocationID" runat="server" AutoRefresh="True" DataField="VendorLocationID" ></px:PXSegmentMask>
            <px:PXSegmentMask CommitChanges="True" ID="edEmployeeID" runat="server" DataField="EmployeeID" ></px:PXSegmentMask>
            <pxa:PXCurrencyRate ID="edCury" DataField="CuryID" runat="server" DataSourceID="ds" RateTypeView="_POOrder_CurrencyInfo_"
                DataMember="_Currency_" ></pxa:PXCurrencyRate>
            <px:PXTextEdit CommitChanges="True" ID="edVendorRefNbr" runat="server" DataField="VendorRefNbr" ></px:PXTextEdit>
	<px:PXDropDown runat="server" ID="CstPXDropDown3" DataField="UsrBoardValidationType" CommitChanges="True" />
	<px:PXTextEdit runat="server" ID="CstPXTextEdit1" DataField="UsrBoardAgrNbr" CommitChanges="" />
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" ></px:PXLayoutRule>
	<px:PXNumberEdit runat="server" ID="CstPXNumberEdit4" DataField="UsrPrepaymentAmt" CommitChanges="True" />
	<px:PXCheckBox runat="server" ID="CstPXCheckBox6" DataField="UsrPrepaymentIsRequired" CommitChanges="True" />
            <px:PXNumberEdit ID="edCuryLineTotal" runat="server" DataField="CuryLineTotal" Enabled="False" ></px:PXNumberEdit>
            <px:PXNumberEdit ID="edCuryDiscTot" runat="server" Enabled="False" DataField="CuryDiscTot" ></px:PXNumberEdit>
            <px:PXNumberEdit ID="edCuryVatExemptTotal" runat="server" DataField="CuryVatExemptTotal" Enabled="False" ></px:PXNumberEdit>
            <px:PXNumberEdit ID="edCuryVatTaxableTotal" runat="server" DataField="CuryVatTaxableTotal" Enabled="False" ></px:PXNumberEdit>
            <px:PXNumberEdit ID="edCuryTaxTotal" runat="server" DataField="CuryTaxTotal" Enabled="False" ></px:PXNumberEdit>
            <px:PXNumberEdit ID="edCuryOrderTotal" runat="server" DataField="CuryOrderTotal" Enabled="False" ></px:PXNumberEdit>
            <px:PXNumberEdit CommitChanges="True" ID="edCuryControlTotal" runat="server" DataField="CuryControlTotal" ></px:PXNumberEdit></Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <script type="text/javascript">
        function UpdateItemSiteCell(n, c) {
            var activeRow = c.cell.row;
            var sCell = activeRow.getCell("Selected");
            var qCell = activeRow.getCell("QtySelected");
            if (sCell == c.cell) {
                if (sCell.getValue() == true)
                    qCell.setValue("1");
                else
                    qCell.setValue("0");
            }
            if (qCell == c.cell) {
                if (qCell.getValue() == "0")
                    sCell.setValue(false);
                else
                    sCell.setValue(true);
            }
        }
    </script>
    <px:PXTab ID="tab" runat="server" Height="504px" Style="z-index: 100;" Width="100%" DataSourceID="ds" DataMember="CurrentDocument">
        <Activity HighlightColor="" SelectedColor="" Width="" Height=""></Activity>
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="grid" runat="server" DataSourceID="ds" Style="z-index: 100; left: 0px; top: 0px; height: 384px;" Width="100%"
                        BorderWidth="0px" SkinID="Details" SyncPosition="True" Height="384px" TabIndex="2500">
                        <Levels>
                            <px:PXGridLevel DataMember="Transactions">
                                <Columns>
                                    <px:PXGridColumn DataField="BranchID" DisplayFormat="&gt;AAAAAAAAAA" Width="81px" RenderEditorText="True"
                                        AllowShowHide="Server" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="InventoryID" DisplayFormat="&gt;AAAAAAAAAA" Width="81px" CommitChanges="True" AllowDragDrop="true"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SubItemID" DisplayFormat="&gt;AA-A-A" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="LineType" Type="DropDownList" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SiteID" DisplayFormat="&gt;AAAAAAAAAA" CommitChanges="True" AllowDragDrop="true"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="TranDesc" Width="180px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="UOM" Width="54px" DisplayFormat="&gt;aaaaaa" CommitChanges="True" AllowDragDrop="true"></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="OrderQty" TextAlign="Right" Width="81px" CommitChanges="True" AllowDragDrop="true"></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="BaseOrderQty" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="ReceivedQty" TextAlign="Right" Width="100px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="CuryUnitCost" TextAlign="Right" Width="81px" CommitChanges="true" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ManualPrice" TextAlign="Center" AllowNull="False" Type="CheckBox" CommitChanges="True"></px:PXGridColumn>   
                                    <px:PXGridColumn AllowNull="False" DataField="CuryLineAmt" TextAlign="Right" Width="81px" CommitChanges="true" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscPct" TextAlign="Right" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryDiscAmt" TextAlign="Right" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryDiscCost" TextAlign="Right" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ManualDisc" TextAlign="Center" Type="CheckBox" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountID" RenderEditorText="True" TextAlign="Left" AllowShowHide="Server" Width="90px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountSequenceID" TextAlign="Left" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="CuryExtCost" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="CuryReceivedCost" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="AlternateID" Width="180px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="LotSerialNbr" AllowShowHide="Server" Width="120px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="RcptQtyMin" TextAlign="Right" Width="100px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="RcptQtyMax" TextAlign="Right" Width="100px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RcptQtyThreshold" TextAlign="Right" Width="120px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="RcptQtyAction" Width="108px" Type="DropDownList" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="TaxCategoryID" DisplayFormat="&gt;aaaaaaaaaa" Width="54px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ExpenseAcctID" DisplayFormat="&gt;######" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ExpenseAcctID_Account_description" Width="120px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ExpenseSubID" DisplayFormat="&gt;AA-AA-AA-AA-AAA" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ProjectID" Label="Project" Width="108px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="TaskID" DisplayFormat="&gt;AAAAAAAAAA" Label="Task" Width="81px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CostCodeID" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="OrderType" Visible="False" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="LineNbr" TextAlign="Right" Visible="False" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SortOrder" TextAlign="Right" Visible="False" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RequestedDate" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="PromisedDate" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CompletePOLine" Visible="False" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Completed" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Cancelled" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowUpdate="False" DataField="POType" RenderEditorText="True" Width="72px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowUpdate="False" DataField="PONbr" DisplayFormat="&gt;CCCCCCCCCCCCCCC" Width="117px" ></px:PXGridColumn>
                                </Columns>
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" ></px:PXLayoutRule>
                                    <px:PXSegmentMask CommitChanges="True" ID="edInventoryID" runat="server" DataField="InventoryID" AllowEdit="True" 
                                        AutoRefresh="True">
                                        <Parameters>
                                            <px:PXControlParam ControlID="grid" Name="POLine.lineType" PropertyName="DataValues[&quot;LineType&quot;]" Type="String" ></px:PXControlParam>
                                        </Parameters>
                                    </px:PXSegmentMask>
                                    <px:PXSegmentMask CommitChanges="True" ID="edSubItemID" runat="server" DataField="SubItemID">
                                        <Parameters>
                                            <px:PXControlParam ControlID="grid" Name="POLine.inventoryID" PropertyName="DataValues[&quot;InventoryID&quot;]" Type="String" ></px:PXControlParam>
                                        </Parameters>
                                    </px:PXSegmentMask>
                                    <px:PXSegmentMask CommitChanges="True" ID="edSiteID" runat="server" DataField="SiteID" AutoRefresh="True" ></px:PXSegmentMask>
                                    <px:PXDropDown CommitChanges="True" ID="edLineType" runat="server" AllowNull="False" DataField="LineType" ></px:PXDropDown>
                                    <px:PXSelector CommitChanges="True" ID="edUOM" runat="server" DataField="UOM" AutoRefresh="True">
                                        <Parameters>
                                            <px:PXSyncGridParam ControlID="grid" ></px:PXSyncGridParam>
                                        </Parameters>
                                    </px:PXSelector>
                                    <px:PXNumberEdit ID="edOrderQty" runat="server" DataField="OrderQty" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edReceivedQty" runat="server" DataField="ReceivedQty" Enabled="False" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edCuryUnitCost" runat="server" DataField="CuryUnitCost" CommitChanges="true" ></px:PXNumberEdit>
                                    <px:PXCheckBox ID="chkManualPrice" runat="server" DataField="ManualPrice" CommitChanges="True" ></px:PXCheckBox>
                                    <px:PXSelector ID="edDiscountCode" runat="server" DataField="DiscountID" CommitChanges="True" AllowEdit="True" edit="1" ></px:PXSelector>
                                    <px:PXNumberEdit ID="edDiscPct" runat="server" DataField="DiscPct" ></px:PXNumberEdit>
									<px:PXNumberEdit ID="edCuryDiscAmt" runat="server" DataField="CuryDiscAmt" ></px:PXNumberEdit>
									<px:PXCheckBox ID="chkManualDisc" runat="server" DataField="ManualDisc" CommitChanges="True" ></px:PXCheckBox>
                                    <px:PXNumberEdit ID="edCuryLineAmt" runat="server" DataField="CuryLineAmt" CommitChanges="true" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edCuryExtCost" runat="server" DataField="CuryExtCost" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edCuryReceivedCost" runat="server" DataField="CuryReceivedCost" Enabled="False" ></px:PXNumberEdit>
                                    <px:PXLayoutRule runat="server" ColumnSpan="2" ></px:PXLayoutRule>
                                    <px:PXTextEdit ID="edTranDesc" runat="server" DataField="TranDesc" ></px:PXTextEdit>

                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="S" ></px:PXLayoutRule>
                                    <px:PXNumberEdit ID="edRcptQtyMin" runat="server" DataField="RcptQtyMin" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRcptQtyMax" runat="server" DataField="RcptQtyMax" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRcptQtyThreshold" runat="server" DataField="RcptQtyThreshold" ></px:PXNumberEdit>
                                    <px:PXDropDown ID="edRcptQtyAction" runat="server" DataField="RcptQtyAction" ></px:PXDropDown>
									<px:PXCheckBox ID="chkCompleted" runat="server" DataField="Completed" ></px:PXCheckBox>
                                    <px:PXCheckBox ID="chkCancelled" runat="server" DataField="Cancelled" ></px:PXCheckBox>

                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="M" ></px:PXLayoutRule>
                                    <px:PXSegmentMask CommitChanges="True" Height="19px" ID="edBranchID" runat="server" DataField="BranchID" ></px:PXSegmentMask>
                                    <px:PXSegmentMask CommitChanges="True" ID="edExpenseAcctID" runat="server" DataField="ExpenseAcctID" 
                                        AutoRefresh="True" ></px:PXSegmentMask>
                                    <px:PXSegmentMask ID="edExpenseSubID" runat="server" DataField="ExpenseSubID" AutoRefresh="True">
                                        <Parameters>
                                            <px:PXSyncGridParam ControlID="grid" ></px:PXSyncGridParam>
                                        </Parameters>
                                    </px:PXSegmentMask>
                                    <px:PXDateTimeEdit ID="edRequestedDate" runat="server" DataField="RequestedDate" ></px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit ID="edPromisedDate" runat="server" DataField="PromisedDate" ></px:PXDateTimeEdit>
                                    <px:PXSelector ID="edTaxCategoryID" runat="server" DataField="TaxCategoryID" AutoRefresh="True"></px:PXSelector>
                                    <px:PXTextEdit ID="edAlternateID" runat="server" DataField="AlternateID" ></px:PXTextEdit>
                                    <px:PXSegmentMask CommitChanges="True" ID="edProjectID" runat="server" DataField="ProjectID" ></px:PXSegmentMask>
                                    <px:PXSegmentMask CommitChanges="True" ID="edTaskID" runat="server" DataField="TaskID" AutoRefresh="True">
                                        <Parameters>
                                            <px:PXSyncGridParam ControlID="grid" ></px:PXSyncGridParam>
                                        </Parameters>
                                    </px:PXSegmentMask>
                                    <px:PXSegmentMask ID="edCostCode" runat="server" DataField="CostCodeID" AutoRefresh="True" AllowAddNew="true" ></px:PXSegmentMask>
                                    <px:PXDropDown ID="edPOType" runat="server" DataField="POType" ></px:PXDropDown>
                                    <px:PXSelector ID="edPONbr" runat="server" DataField="PONbr" AllowEdit="True" ></px:PXSelector>
                                </RowTemplate>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True" MinHeight="150" ></AutoSize>
                        <CallbackCommands PasteCommand="PasteLine">
                            <Save PostData="Container" ></Save>
                        </CallbackCommands>
                        <Mode InitNewRow="True" AllowFormEdit="True" AllowUpload="True" AllowDragRows="true"></Mode>
                        <ActionBar>
                            <CustomItems>
                                <px:PXToolBarButton Text="Add Item" Key="cmdASI">
                                    <AutoCallBack Command="AddInvBySite" Target="ds">
                                        <Behavior PostData="Page" CommitChanges="True" ></Behavior>
                                    </AutoCallBack>
                                </px:PXToolBarButton>
	<px:PXToolBarButton Text="Add Item1">
		<AutoCallBack Command="AddInvBySite1" Target="ds" /></px:PXToolBarButton>
                                <px:PXToolBarButton Text="View Demand">
                                    <AutoCallBack Command="ViewDemand" Target="ds">
                                        <Behavior PostData="Page" CommitChanges="True" ></Behavior>
                                    </AutoCallBack>
                                </px:PXToolBarButton>
                                <px:PXToolBarButton Text="Add Order" Key="cmdPO" CommandSourceID="ds" CommandName="AddPOOrder" ></px:PXToolBarButton>
                                <px:PXToolBarButton Text="Add Order Line" Key="cmdAddPOLine" CommandSourceID="ds" CommandName="AddPOOrderLine" ></px:PXToolBarButton>                                                                
                                 <px:PXToolBarButton Text="Insert Row" SyncText="false" ImageSet="main" ImageKey="AddNew">
																	<AutoCallBack Target="grid" Command="AddNew" Argument="1"></AutoCallBack>
																	<ActionBar ToolBarVisible="External" MenuVisible="true" ></ActionBar>
                                </px:PXToolBarButton>
                                <px:PXToolBarButton Text="Cut Row" SyncText="false" ImageSet="main" ImageKey="Copy">
																	<AutoCallBack Target="grid" Command="Copy"></AutoCallBack>
																	<ActionBar ToolBarVisible="External" MenuVisible="true" ></ActionBar>
                                </px:PXToolBarButton>
                                <px:PXToolBarButton Text="Insert Cut Row" SyncText="false" ImageSet="main" ImageKey="Paste">
																	<AutoCallBack Target="grid" Command="Paste"></AutoCallBack>
																	<ActionBar ToolBarVisible="External" MenuVisible="true" ></ActionBar>
                                </px:PXToolBarButton></CustomItems>
                        </ActionBar>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Tax Details">
                <Template>
                    <px:PXGrid ID="gridTaxes" runat="server" DataSourceID="ds" Height="150px" Style="z-index: 100" Width="100%" ActionsPosition="Top"
                        BorderWidth="0px" SkinID="Details">
                        <AutoSize Enabled="True" MinHeight="150" ></AutoSize>
                        <ActionBar>
                            <Actions>
                                <Search Enabled="False" ></Search>
                                <Save Enabled="False" ></Save>
                                <EditRecord Enabled="False" ></EditRecord>
                            </Actions>
                        </ActionBar>
                        <Levels>
                            <px:PXGridLevel DataMember="Taxes">
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
                                    <px:PXSelector SuppressLabel="True" ID="edTaxID" runat="server" DataField="TaxID" ></px:PXSelector>
                                    <px:PXNumberEdit SuppressLabel="True" ID="edTaxRate" runat="server" DataField="TaxRate" Enabled="False" ></px:PXNumberEdit>
                                    <px:PXNumberEdit SuppressLabel="True" ID="edCuryTaxableAmt" runat="server" DataField="CuryTaxableAmt" ></px:PXNumberEdit>
                                    <px:PXNumberEdit SuppressLabel="True" ID="edCuryTaxAmt" runat="server" DataField="CuryTaxAmt" ></px:PXNumberEdit></RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="TaxID" Width="81px" AllowUpdate="False" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" AllowUpdate="False" DataField="TaxRate" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="CuryTaxableAmt" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="CuryTaxAmt" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Tax__TaxType" Label="Tax Type" RenderEditorText="True" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Tax__PendingTax" Label="Pending VAT" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Tax__ReverseTax" Label="Reverse VAT" TextAlign="Center" Type="CheckBox" Width="60px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Tax__ExemptTax" Label="Exempt From VAT" TextAlign="Center" Type="CheckBox"
                                        Width="60px" ></px:PXGridColumn>
                                    <px:PXGridColumn AllowNull="False" DataField="Tax__StatisticalTax" Label="Statistical VAT" TextAlign="Center" Type="CheckBox"
                                        Width="60px" ></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Shipping Instructions">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Ship To:" ></px:PXLayoutRule>
                    <px:PXDropDown CommitChanges="True" ID="edShipDestType" runat="server" AllowNull="False" DataField="ShipDestType" 
                        SuppressLabel="False" ></px:PXDropDown>
                    <px:PXSelector CommitChanges="True" ID="edShipToBAccountID" runat="server" DataField="ShipToBAccountID" 
                        AutoRefresh="True"  SuppressLabel="False"></px:PXSelector>

                    <px:PXSegmentMask CommitChanges="True" SuppressLabel="False" ID="edSiteID" runat="server" DataField="SiteID" ></px:PXSegmentMask>

                    <px:PXSegmentMask CommitChanges="True" ID="edShipToLocationID" runat="server" AutoRefresh="True" DataField="ShipToLocationID" ></px:PXSegmentMask>
                    <px:PXFormView ID="formSC" runat="server" Caption="Ship-To Contact" DataMember="Shipping_Contact" DataSourceID="ds" RenderStyle="Fieldset">
                        <Template>
                            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                            <px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkOverrideContact" runat="server" DataField="OverrideContact" ></px:PXCheckBox>
                            <px:PXTextEdit ID="edSalutation" runat="server" DataField="Salutation" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edFullName" runat="server" DataField="FullName" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edPhone1" runat="server" DataField="Phone1" ></px:PXTextEdit>
                            <px:PXMailEdit ID="edEmail" runat="server" DataField="Email" CommitChanges="True"></px:PXMailEdit>
                        </Template>
                        <ContentStyle BackColor="Transparent" BorderStyle="None">
                        </ContentStyle>
                        <AutoSize MinWidth="100" ></AutoSize>
                    </px:PXFormView>
                    <px:PXFormView ID="formSA" DataMember="Shipping_Address" runat="server" DataSourceID="ds" Caption="Ship-To Address" SyncPosition="True" RenderStyle="Fieldset">
                        <AutoSize MinHeight="100" MinWidth="100" ></AutoSize>
                        <Template>
                            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                            <px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
                            <px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkOverrideAddress" runat="server" DataField="OverrideAddress" ></px:PXCheckBox>
                            <px:PXCheckBox ID="chkIsValidated" runat="server" DataField="IsValidated" Enabled="False"></px:PXCheckBox>
                            <px:PXLayoutRule runat="server" ></px:PXLayoutRule>
                            <px:PXTextEdit ID="edAddressLine1" runat="server" DataField="AddressLine1" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edAddressLine2" runat="server" DataField="AddressLine2" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edCity" runat="server" DataField="City" ></px:PXTextEdit>
                            <px:PXSelector ID="edCountryID" runat="server" DataField="CountryID" AutoRefresh="True" DataSourceID="ds" CommitChanges="true" ></px:PXSelector>
                            <px:PXSelector ID="edState" runat="server" DataField="State" AutoRefresh="True" DataSourceID="ds">
                                <CallBackMode PostData="Container" ></CallBackMode>
                                <Parameters>
                                    <px:PXControlParam ControlID="formSA" Name="POShipAddress.countryID" PropertyName="DataControls[&quot;edCountryID&quot;].Value"
                                        Type="String" ></px:PXControlParam>
                                </Parameters>
                            </px:PXSelector>
                            <px:PXMaskEdit ID="edPostalCode" runat="server" DataField="PostalCode" CommitChanges="true" ></px:PXMaskEdit>
                        </Template>
                        <ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
                    </px:PXFormView>
                    <px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Ship Via:" ></px:PXLayoutRule>
                    <px:PXSelector ID="edFOBPoint" runat="server" DataField="FOBPoint" ></px:PXSelector>
                    <px:PXSelector ID="edShipVia" runat="server" DataField="ShipVia" ></px:PXSelector>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Vendor Info">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="True" SuppressLabel="True" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" StartGroup="True" ></px:PXLayoutRule>
                    <px:PXFormView ID="formVC" runat="server" Caption="Vendor Contact" DataMember="Remit_Contact" DataSourceID="ds" RenderStyle="Fieldset">
                        <Template>
                            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                            <px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkOverrideContact" runat="server" DataField="OverrideContact" ></px:PXCheckBox>
                            <px:PXTextEdit ID="edFullName" runat="server" DataField="FullName" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edSalutation" runat="server" DataField="Salutation" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edPhone1" runat="server" DataField="Phone1" ></px:PXTextEdit>
                            <px:PXMailEdit ID="edEmail" runat="server" DataField="Email" CommitChanges="True"></px:PXMailEdit>
                        </Template>
                        <ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
                    </px:PXFormView>
                    <px:PXFormView ID="formVA" DataMember="Remit_Address" runat="server" Caption="Vendor Address" DataSourceID="ds" SyncPosition="True" RenderStyle="Fieldset">
                        <Template>
                            <px:PXLayoutRule runat="server" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                            <px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
                            <px:PXCheckBox CommitChanges="True" SuppressLabel="True" ID="chkOverrideAddress" runat="server" DataField="OverrideAddress" ></px:PXCheckBox>
                            <px:PXCheckBox ID="chkIsValidated" runat="server" DataField="IsValidated" Enabled="False"></px:PXCheckBox>
                            <px:PXLayoutRule runat="server" ></px:PXLayoutRule>
                            <px:PXTextEdit ID="edAddressLine1" runat="server" DataField="AddressLine1" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edAddressLine2" runat="server" DataField="AddressLine2" ></px:PXTextEdit>
                            <px:PXTextEdit ID="edCity" runat="server" DataField="City" ></px:PXTextEdit>
                            <px:PXSelector ID="edCountryID" runat="server" DataField="CountryID" AutoRefresh="True" DataSourceID="ds" CommitChanges="true" ></px:PXSelector>
                            <px:PXSelector ID="edState" runat="server" DataField="State" AutoRefresh="True" DataSourceID="ds">
                                <CallBackMode PostData="Container" ></CallBackMode>
                                <Parameters>
                                    <px:PXControlParam ControlID="formVA" Name="PORemitAddress.countryID" PropertyName="DataControls[&quot;edCountryID&quot;].Value"
                                                       Type="String" ></px:PXControlParam>
                                </Parameters>
                            </px:PXSelector>
                            <px:PXMaskEdit ID="edPostalCode" runat="server" DataField="PostalCode" CommitChanges="true" ></px:PXMaskEdit>
                        </Template>
                        <ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
                    </px:PXFormView>
                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Info" ></px:PXLayoutRule>
                    <px:PXSelector CommitChanges="True" ID="edTermsID" runat="server" DataField="TermsID" ></px:PXSelector>
                    <px:PXSelector CommitChanges="True" ID="edTaxZoneID" runat="server" DataField="TaxZoneID" Text="ZONE1" ></px:PXSelector>
	<px:PXCheckBox runat="server" ID="CstPXCheckBox4" DataField="UsrVEInsider" CommitChanges="True" />
					<px:PXSegmentMask CommitChanges="True" ID="edPayToVendorID" runat="server" DataField="PayToVendorID" AllowEdit="True" AutoRefresh="True" ></px:PXSegmentMask></Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Approval Details" BindingContext="form" VisibleExp="DataControls[&quot;chkRequestApproval&quot;].Value = 1">
                <Template>
                    <px:PXGrid ID="gridApproval" runat="server" DataSourceID="ds" Width="100%" SkinID="DetailsInTab" NoteIndicator="True" Style="left: 0px;
                        top: 0px;">
                        <AutoSize Enabled="True" ></AutoSize>
                        <Mode AllowAddNew="False" AllowDelete="False" AllowUpdate="False" ></Mode>
                        <Levels>
                            <px:PXGridLevel DataMember="Approval">
                                <Columns>
                                    <px:PXGridColumn DataField="ApproverEmployee__AcctCD" Width="160px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ApproverEmployee__AcctName" Width="160px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ApprovedByEmployee__AcctCD" Width="100px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ApprovedByEmployee__AcctName" Width="160px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ApproveDate" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status" AllowNull="False" AllowUpdate="False" RenderEditorText="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="WorkgroupID" Width="150px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AssignmentMapID"  Visible="false" SyncVisible="false" Width="160px"></px:PXGridColumn>
									<px:PXGridColumn DataField="StepID" Visible="false" SyncVisible="false" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="RuleID" Visible="false" SyncVisible="false" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CreatedDateTime" Visible="false" SyncVisible="false" Width="100px" ></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Discount Details">
                <Template>
                    <px:PXGrid ID="formDiscountDetail" runat="server" DataSourceID="ds" Width="100%" SkinID="Details" BorderStyle="None">
                        <Levels>
                            <px:PXGridLevel DataMember="DiscountDetails">
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM" ></px:PXLayoutRule>
                                    <px:PXCheckBox ID="chkSkipDiscount" runat="server" DataField="SkipDiscount" ></px:PXCheckBox>
                                    <px:PXSelector ID="edDiscountID" runat="server" DataField="DiscountID" 
                                        AllowEdit="True" edit="1" ></px:PXSelector>
                                    <px:PXDropDown ID="edType" runat="server" DataField="Type" Enabled="False" ></px:PXDropDown>
                                    <px:PXCheckBox ID="chkIsManual" runat="server" DataField="IsManual" ></px:PXCheckBox>
                                    <px:PXSelector ID="edDiscountSequenceID" runat="server" DataField="DiscountSequenceID" AllowEdit="True" AutoRefresh="True" edit="1" ></px:PXSelector>
                                    <px:PXNumberEdit ID="edCuryDiscountableAmt" runat="server" DataField="CuryDiscountableAmt" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edDiscountableQty" runat="server" DataField="DiscountableQty" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edCuryDiscountAmt" runat="server" DataField="CuryDiscountAmt" ></px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edDiscountPct" runat="server" DataField="DiscountPct" ></px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="SkipDiscount" Width="75px" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountID" Width="90px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountSequenceID" Width="90px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Type" RenderEditorText="True" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="IsManual" Width="75px" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryDiscountableAmt" TextAlign="Right" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountableQty" TextAlign="Right" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryDiscountAmt" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="DiscountPct" TextAlign="Right" Width="81px" ></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True" MinHeight="150" ></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
              <px:PXTabItem Text="Receipts">
                <Template>
                    <px:PXGrid ID="formReceipts" runat="server" DataSourceID="ds" Width="100%" SkinID="Details" BorderStyle="None">
                        <Levels>
                            <px:PXGridLevel DataMember="Receipts" >
                                <RowTemplate>
                                 <px:PXSelector SuppressLabel="True" Size="s" ID="edReceiptNbr" runat="server" 
                                        DataField="ReceiptNbr" AutoRefresh="True"
                                        AllowEdit="True" edit="1" ></px:PXSelector>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="ReceiptType" Width="90px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ReceiptNbr" Width="90px" CommitChanges="True" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status" RenderEditorText="True" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="ReceiptDate" RenderEditorText="True" Width="90px" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="OrderQty" RenderEditorText="True" Width="90px" ></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True" MinHeight="150" ></AutoSize>
                         <Mode AllowAddNew="False" AllowDelete="False" AllowUpdate="False" ></Mode>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Other Information">
                <Template>
                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM"  ></px:PXLayoutRule>
                    <px:PXSegmentMask CommitChanges="True" ID="edBranchID" runat="server" DataField="BranchID" ></px:PXSegmentMask>
                    <px:PXSelector CommitChanges="True" ID="edSOOrderType" runat="server" DataField="SOOrderType" ></px:PXSelector>
                    <px:PXSelector ID="edSOOrderNbr" runat="server" AllowEdit="True" DataField="SOOrderNbr" edit="1" ></px:PXSelector>
                    <px:PXSelector ID="edRQReqNbr" runat="server" AllowEdit="True" DataField="RQReqNbr" Enabled="False" ></px:PXSelector>
                    <px:PXSelector ID="edPrepaymentRefNbr" runat="server" DataField="PrepaymentRefNbr" AllowEdit="True" Enabled="False" ></px:PXSelector>
                    <px:PXSelector ID="edOwnerWorkgroupID" runat="server" DataField="OwnerWorkgroupID" ></px:PXSelector>
                    <px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
                    <px:PXCheckBox ID="chkDontPrint" runat="server" Checked="True" DataField="DontPrint" Size="SM" ></px:PXCheckBox>
                    <px:PXCheckBox ID="chkPrinted" runat="server" DataField="Printed" Enabled="False" Size="SM" ></px:PXCheckBox>
                    <px:PXLayoutRule runat="server" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
                    <px:PXCheckBox ID="chkDontEmail" runat="server" Checked="True" DataField="DontEmail" Size="SM" ></px:PXCheckBox>
                    <px:PXCheckBox ID="chkEmailed" runat="server" DataField="Emailed" Enabled="False" Size="SM" ></px:PXCheckBox>
                    <px:PXLayoutRule runat="server" ></px:PXLayoutRule>
                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" ></px:PXLayoutRule>
                    <px:PXNumberEdit ID="edOpenOrderQty" runat="server" DataField="OpenOrderQty" ></px:PXNumberEdit>
                    <px:PXNumberEdit ID="edCuryOpenOrderTotal" runat="server" DataField="CuryOpenOrderTotal" ></px:PXNumberEdit>
	<px:PXSelector runat="server" ID="CstPXSelector10" DataField="UsrEmployeeID" CommitChanges="True" />
	<px:PXSelector runat="server" ID="CstPXSelector9" DataField="UsrDepartment" CommitChanges="True" /></Template>
            </px:PXTabItem>
	<px:PXTabItem Text="Related FA" BindingContext="form" RepaintOnDemand="False" VisibleExp="DataControls[&quot;CstPXCheckBox2&quot;].Value==1">
		<Template>
			<px:PXGrid runat="server" ID="CstPXGrid4" SkinID="DetailsInTab" DataSourceID="ds" Width="100%">
				<Levels>
					<px:PXGridLevel DataMember="FARelateds">
						<Columns>
							<px:PXGridColumn DataField="AssetCD" Width="120" CommitChanges="True" />
							<px:PXGridColumn DataField="Description" Width="200" />
							<px:PXGridColumn DataField="AssetClass" Width="120" />
							<px:PXGridColumn DataField="Status" Width="70" />
							<px:PXGridColumn DataField="ReceiptDate" Width="120" />
							<px:PXGridColumn DataField="PlacedInServiceDate" Width="150" />
							<px:PXGridColumn DataField="OrigAcquisitionCost" Width="140" />
							<px:PXGridColumn DataField="UsefulLifeYears" Width="130" />
							<px:PXGridColumn DataField="Department" Width="120" />
							<px:PXGridColumn DataField="ExpenseAmount" Width="140" /></Columns>
						<RowTemplate>
							<px:PXSelector runat="server" ID="CstPXSelector3" DataField="AssetCD" AllowEdit="True" /></RowTemplate></px:PXGridLevel></Levels>
				<AutoSize Enabled="True" MinHeight="200" /></px:PXGrid></Template></px:PXTabItem></Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="180" ></AutoSize>
    </px:PXTab>
    <px:PXSmartPanel ID="PanelAddSiteStatus" runat="server" Key="sitestatus" LoadOnDemand="true" Width="1100px" Height="500px"
        Caption="Inventory Lookup" CaptionVisible="true" AutoCallBack-Command='Refresh' AutoCallBack-Enabled="True" AutoCallBack-Target="formSitesStatus"
        DesignView="Hidden">
        <px:PXFormView ID="formSitesStatus" runat="server" CaptionVisible="False" DataMember="sitestatusfilter" DataSourceID="ds"
            Width="100%" SkinID="Transparent">
            <Template>
                <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="M" ></px:PXLayoutRule>
                <px:PXTextEdit CommitChanges="True" ID="edInventory" runat="server" DataField="Inventory" ></px:PXTextEdit>
                <px:PXTextEdit CommitChanges="True" ID="edBarCode" runat="server" DataField="BarCode" ></px:PXTextEdit>
                <px:PXCheckBox CommitChanges="True" ID="chkOnlyAvailable" runat="server" Checked="True" DataField="OnlyAvailable" AutoCallBack="true"  ></px:PXCheckBox>
                <px:PXLayoutRule  runat="server" StartColumn="True" LabelsWidth="S" ControlSize="XM" ></px:PXLayoutRule>
                <px:PXSegmentMask CommitChanges="True" ID="edSiteID" runat="server" DataField="SiteID" ></px:PXSegmentMask>
                <px:PXSegmentMask CommitChanges="True" ID="edItemClassID" runat="server" DataField="ItemClass" ></px:PXSegmentMask>
                <px:PXSegmentMask CommitChanges="True" ID="edSubItem" runat="server" DataField="SubItem" AutoRefresh="true" ></px:PXSegmentMask></Template>
        </px:PXFormView>
	<px:PXGrid runat="server" ID="gripSiteStatus" Height="135px" SkinID="Details" Width="100%" AutoAdjustColumns="true" BatchUpdate="true" DataSourceID="ds" AdjustPageSize="Auto" AllowSearch="True" FastFilterFields="InventoryCD,Descr,AlternateID" FastFilterID="edInventory" Style='left:0px;top:0px;'>
		<ClientEvents AfterCellUpdate="UpdateItemSiteCell" />
		<AutoSize Enabled="true" />
		<ActionBar PagerVisible="False" />
		<CallbackCommands>
			<Refresh CommitChanges="true" /></CallbackCommands>
		<Levels>
			<px:PXGridLevel DataMember="siteStatus">
				<Mode AllowAddNew="false" AllowDelete="false" />
				<RowTemplate>
					<px:PXSegmentMask runat="server" DataField="ItemClassID" ID="editemClass" /></RowTemplate>
				<Columns>
					<px:PXGridColumn DataField="Selected" Type="CheckBox" TextAlign="Center" Width="80px" AutoCallBack="true" AllowNull="False" AllowCheckAll="true" />
					<px:PXGridColumn DataField="QtySelected" TextAlign="Right" Width="200px" AllowNull="False" />
					<px:PXGridColumn DataField="SiteID" Width="160px" />
					<px:PXGridColumn DataField="ItemClassID" Width="140px" />
					<px:PXGridColumn DataField="ItemClassDescription" Width="140px" />
					<px:PXGridColumn DataField="PriceClassID" Width="140px" />
					<px:PXGridColumn DataField="PriceClassDescription" Width="140px" />
					<px:PXGridColumn DataField="PreferredVendorID" Width="160px" />
					<px:PXGridColumn DataField="PreferredVendorDescription" Width="140px" />
					<px:PXGridColumn DataField="InventoryCD" DisplayFormat=">AAAAAAAAAA" Width="260px" />
					<px:PXGridColumn DataField="SubItemID" DisplayFormat=">AA-A-A" Width="150px" />
					<px:PXGridColumn DataField="Descr" Width="220px" />
					<px:PXGridColumn DataField="PurchaseUnit" DisplayFormat=">aaaaaa" Width="200px" />
					<px:PXGridColumn DataField="QtyAvailExt" TextAlign="Right" Width="180px" AllowNull="False" />
					<px:PXGridColumn DataField="QtyOnHandExt" TextAlign="Right" Width="180px" AllowNull="False" />
					<px:PXGridColumn DataField="QtyPOOrdersExt" TextAlign="Right" Width="180px" AllowNull="False" />
					<px:PXGridColumn DataField="QtyPOReceiptsExt" TextAlign="Right" Width="180px" AllowNull="False" />
					<px:PXGridColumn DataField="AlternateID" Width="120px" AllowNull="False" />
					<px:PXGridColumn DataField="AlternateType" Width="120px" AllowNull="False" />
					<px:PXGridColumn DataField="AlternateDescr" Width="120px" AllowNull="False" /></Columns></px:PXGridLevel></Levels></px:PXGrid>
        <px:PXPanel ID="PXPanel5" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton6" runat="server" CommandName="AddInvSelBySite" CommandSourceID="ds" Text="Add" SyncVisible="false"></px:PXButton>
            <px:PXButton ID="PXButton7" runat="server" Text="Add & Close" DialogResult="OK"></px:PXButton>
            <px:PXButton ID="PXButton8" runat="server" DialogResult="Cancel" Text="Cancel" ></px:PXButton>
        </px:PXPanel></px:PXSmartPanel>
    <px:PXSmartPanel ID="PanelRecalcDiscounts" runat="server" Caption="Recalculate Prices" CaptionVisible="true" LoadOnDemand="true" Key="recalcdiscountsfilter"
        AutoCallBack-Enabled="true" AutoCallBack-Target="formRecalcDiscounts" AutoCallBack-Command="Refresh" CallBackMode-CommitChanges="True"
        CallBackMode-PostData="Page">
        <div style="padding: 5px">
            <px:PXFormView ID="formRecalcDiscounts" runat="server" DataSourceID="ds" CaptionVisible="False" DataMember="recalcdiscountsfilter">
                <Activity Height="" HighlightColor="" SelectedColor="" Width="" ></Activity>
                <ContentStyle BackColor="Transparent" BorderStyle="None" ></ContentStyle>
                <Template>
                    <px:PXLayoutRule ID="PXLayoutRule3" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="SM" ></px:PXLayoutRule>
                    <px:PXDropDown ID="edRecalcTerget" runat="server" DataField="RecalcTarget" CommitChanges ="true" ></px:PXDropDown>
                    <px:PXCheckBox CommitChanges="True" ID="chkRecalcUnitPrices" runat="server" DataField="RecalcUnitPrices" ></px:PXCheckBox>
                    <px:PXCheckBox CommitChanges="True" ID="chkOverrideManualPrices" runat="server" DataField="OverrideManualPrices" ></px:PXCheckBox>
                    <px:PXCheckBox CommitChanges="True" ID="chkRecalcDiscounts" runat="server" DataField="RecalcDiscounts" ></px:PXCheckBox>
                    <px:PXCheckBox CommitChanges="True" ID="chkOverrideManualDiscounts" runat="server" DataField="OverrideManualDiscounts" ></px:PXCheckBox>
                </Template>
            </px:PXFormView>
        </div>
        <px:PXPanel ID="PXPanel6" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton10" runat="server" DialogResult="OK" Text="OK" CommandName="RecalcOk" CommandSourceID="ds" ></px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
	<px:PXSmartPanel runat="server" ID="CstSmartPanel11" Caption="Inventory Lookup 1" CaptionVisible="True" Height="500px" Width="1100px" DesignView="Hidden" Key="sitestatus1" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Target="CstFormView12">
		<px:PXFormView runat="server" ID="CstFormView28" RenderStyle="Simple" Height="0" Width="100%" DataSourceID="ds" DataMember="TreeViewAndPrimaryViewSynchronizationHelper">
			<Template>
				<px:PXSegmentMask runat="server" DataField="ItemClassCD" Visible="False" ID="CstPXSegmentMask29" /></Template></px:PXFormView>
		<px:PXSplitContainer runat="server" ID="sp1" SplitterPosition="300" Height="450">
			<AutoSize Container="Window" />
			<Template1>
				<px:PXFormView runat="server" ID="treeFilter" Caption="Category Info" Width="100%" DataSourceID="ds" DataMember="ItemClassFilter">
					<Template>
						<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="XS" ControlSize="M" />
						<px:PXSegmentMask runat="server" CommitChanges="True" DataField="ItemClassCD" Enabled="True" ID="edItemClassID" /></Template></px:PXFormView>
				<px:PXTreeView runat="server" DataSourceID="ds" Height="500px" ID="tree" PopulateOnDemand="True" SelectFirstNode="True" Caption="Item Class Tree" AllowCollapse="False" ExpandDepth="0" AutoRepaint="True" DataMember="ItemClasses" ShowRootNode="False" SyncPosition="True" SyncPositionWithGraph="True" PreserveExpanded="True">
					<AutoCallBack Target="ds" Command="GoToNodeSelectedInTree">
						<Behavior PostData="Self" CommitChanges="True" /></AutoCallBack>
					<DataBindings>
						<px:PXTreeItemBinding DataMember="ItemClasses" TextField="SegmentedClassCD" ValueField="ItemClassID" DescriptionField="Descr" /></DataBindings></px:PXTreeView></Template1>
			<Template2>
				<px:PXFormView runat="server" ID="CstFormView12" SkinID="Transparent" CaptionVisible="False" Width="100%" DataMember="sitestatusfilter1">
					<Template>
						<px:PXLayoutRule runat="server" ID="CstPXLayoutRule19" StartColumn="True" />
						<px:PXTextEdit runat="server" CommitChanges="True" DataField="Inventory" ID="CstPXTextEdit22" />
						<px:PXTextEdit runat="server" CommitChanges="True" DataField="BarCode" ID="CstPXTextEdit21" />
						<px:PXCheckBox runat="server" CommitChanges="True" DataField="OnlyAvailable" ID="CstPXCheckBox24" />
						<px:PXLayoutRule runat="server" ID="CstPXLayoutRule20" StartColumn="True" />
						<px:PXTextEdit runat="server" DataField="LocationID_description" ID="CstPXTextEdit27" />
						<px:PXSegmentMask runat="server" CommitChanges="True" DataField="SiteID" ID="CstPXSegmentMask25" />
						<px:PXSegmentMask runat="server" CommitChanges="True" DataField="ItemClass" ID="CstPXSegmentMask23" />
						<px:PXSegmentMask runat="server" CommitChanges="True" DataField="SubItem" ID="CstPXSegmentMask26" /></Template></px:PXFormView>
				<px:PXGrid runat="server" ID="CstPXGrid13" SyncPosition="True" Height="135px" SkinID="Details" Width="100%" AutoAdjustColumns="True" BatchUpdate="True" DataSourceID="ds" AdjustPageSize="Auto" AllowSearch="True" NoteIndicator="False" FilesIndicator="False">
					<ClientEvents AfterCellUpdate="UpdateItemSiteCell" />
					<AutoSize Enabled="True" />
					<ActionBar PagerVisible="False">
						<Actions>
							<Search Enabled="False" />
							<EditRecord Enabled="False" />
							<NoteShow Enabled="False" />
							<FilterShow Enabled="False" />
							<ExportExcel Enabled="False" /></Actions></ActionBar>
					<CallbackCommands>
						<Refresh CommitChanges="True" /></CallbackCommands>
					<Levels>
						<px:PXGridLevel DataMember="sitestatus1">
							<Mode AllowAddNew="False" AllowDelete="False" />
							<RowTemplate>
								<px:PXCheckBox runat="server" DataField="Selected" ID="CstPXCheckBox30" /></RowTemplate>
							<Columns>
								<px:PXGridColumn DataField="Selected" Type="CheckBox" TextAlign="Center" Width="80px" AutoCallBack="True" AllowCheckAll="True" />
								<px:PXGridColumn DataField="QtySelected" TextAlign="Right" Width="200" />
								<px:PXGridColumn DataField="SiteID" Width="160" />
								<px:PXGridColumn DataField="ItemClassID" Width="140" />
								<px:PXGridColumn DataField="ItemClassDescription" Width="140" />
								<px:PXGridColumn DataField="PriceClassID" Width="140" />
								<px:PXGridColumn DataField="PriceClassDescription" Width="140" />
								<px:PXGridColumn DataField="PreferredVendorID" Width="160" />
								<px:PXGridColumn DataField="PreferredVendorDescription" Width="140" />
								<px:PXGridColumn DataField="InventoryCD" DisplayFormat=">AAAAAAAAAA" Width="260" />
								<px:PXGridColumn DataField="SubItemID" DisplayFormat="AA-A-A" Width="150" />
								<px:PXGridColumn DataField="Descr" Width="220" />
								<px:PXGridColumn DataField="PurchaseUnit" DisplayFormat=">aaaaaa" Width="200" />
								<px:PXGridColumn DataField="QtyAvailExt" Width="180" />
								<px:PXGridColumn DataField="QtyOnHandExt" Width="180" />
								<px:PXGridColumn DataField="QtyPOOrdersExt" Width="180" />
								<px:PXGridColumn DataField="QtyPOPreparedExt" Width="180" />
								<px:PXGridColumn DataField="AlternateID" Width="120" />
								<px:PXGridColumn DataField="AlternateType" Width="120" />
								<px:PXGridColumn DataField="AlternateDescr" Width="120" /></Columns></px:PXGridLevel></Levels></px:PXGrid></Template2></px:PXSplitContainer>
		<px:PXPanel runat="server" ID="CstPanel15" SkinID="Buttons">
			<px:PXButton runat="server" ID="CstButton16" Text="Add" />
			<px:PXButton runat="server" ID="CstButton17" Text="Add &amp; Close" DialogResult="OK" />
			<px:PXButton runat="server" ID="CstButton18" Text="Cancel" DialogResult="Cancel" /></px:PXPanel></px:PXSmartPanel></asp:Content>

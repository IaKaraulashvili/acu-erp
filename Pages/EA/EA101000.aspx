<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EA101000.aspx.cs" Inherits="Page_EA101000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="AG.EA.AssetSetupMaint" PrimaryView="Setup" SuspendUnloading="False">
        <CallbackCommands>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Setup" TabIndex="1100" AllowCollapse="False">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" StartGroup="True" GroupCaption="Numbering" ControlSize="M" LabelsWidth="M" />
            <px:PXSelector runat="server" DataField="AssetNumberingID" ID="edAssetNumberingID" AllowEdit="True" edit="1"></px:PXSelector>
            <px:PXSelector runat="server" DataField="LCABulkRegistrationNumberingID" ID="edLCABulkRegistrationNumberingID" AllowEdit="True" edit="1"></px:PXSelector>
            <px:PXSelector runat="server" DataField="LCABulkDisposeNumberingID" ID="edLCABulkDisposeNumberingID" AllowEdit="True" edit="1"></px:PXSelector>
            <px:PXSelector runat="server" DataField="LCABulkTransferNumberingID" ID="edLCABulkTransferNumberingID" AllowEdit="True" edit="1"></px:PXSelector>

            <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Accounts For Integration" ControlSize="M" LabelsWidth="M"></px:PXLayoutRule>
            <px:PXTextEdit runat="server" DataField="LCAOffBalanceAccount" AlreadyLocalized="False" ID="edLCAOffBalanceAccount"></px:PXTextEdit>
            <px:PXTextEdit runat="server" DataField="LCATransitOffBalanceAccount" AlreadyLocalized="False" ID="edLCATransitOffBalanceAccount"></px:PXTextEdit>

            <px:PXLayoutRule runat="server" StartColumn="True" StartGroup="True" GroupCaption="Data Entry Settings" ControlSize="M" LabelsWidth="M"></px:PXLayoutRule>
            <px:PXSelector runat="server" DataField="DefAssetClassID" ID="edDefAssetClassID" AllowEdit="True" edit="1"></px:PXSelector>
            <px:PXLayoutRule runat="server" Merge="True" />
            <px:PXNumberEdit runat="server" DataField="PlaceInServiceAfterDays" AlreadyLocalized="False" ID="edPlaceInServiceAfterDays" Size="xxs"></px:PXNumberEdit>
            <px:PXLabel Size="xs" ID="lblDays" runat="server" AlreadyLocalized="False">Days</px:PXLabel>
            <px:PXLayoutRule runat="server" />
            <px:PXCheckBox runat="server" DataField="AssetHoldOnEntry" AlreadyLocalized="False" ID="edAssetHoldOnEntry" Text="Hold Asset On Entry" CommitChanges="True"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="AssetAutoRelease" AlreadyLocalized="False" ID="edAssetAutoRelease"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="LCABulkRegistrationHoldOnEntry" AlreadyLocalized="False" ID="edLCABulkRegistrationHoldOnEntry" CommitChanges="True"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="LCABulkDisposeHoldOnEntry" AlreadyLocalized="False" ID="edLCABulkDisposeHoldOnEntry"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="LCABulkTransferHoldOnEntry" AlreadyLocalized="False" ID="edLCABulkTransferHoldOnEntry" Text="Hold LCA Bulk Transfer On Entry"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="LCABulkRegistrationAutoCreate" AlreadyLocalized="False" ID="edLCABulkRegistrationAutoCreate" CommitChanges="True"></px:PXCheckBox>
            <px:PXCheckBox runat="server" DataField="LCABulkRegistrationAutoRelease" AlreadyLocalized="False" ID="edLCABulkRegistrationAutoRelease"></px:PXCheckBox>
        </Template>
        <AutoSize Enabled="true" Container="Window" />
    </px:PXFormView>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="EA201000.aspx.cs" Inherits="Page_EA201000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="AG.EA.AssetClassMaint" PrimaryView="AssetClasses">
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="AssetClasses" TabIndex="2400">
		<Template>
            <px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" />
			<px:PXLayoutRule runat="server" StartRow="True"/>
		    <px:PXSelector ID="edAssetClassID" runat="server" DataField="AssetClassID">
            </px:PXSelector>
            <px:PXTextEdit ID="edDescription" runat="server" AlreadyLocalized="False" DataField="Description" DefaultLocale="">
            </px:PXTextEdit>
		</Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
		<Items>
			<px:PXTabItem Text="General Settings">
			    <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" DataMember="CurrentAssetClass" DataSourceID="ds"  RenderStyle="Simple" TabIndex="3100">
                        <Template>
                            <px:PXLayoutRule runat="server" ControlSize="XM" LabelsWidth="SM" StartColumn="True" />
                            <px:PXDropDown ID="edState" runat="server" DataField="State">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edCondition" runat="server" DataField="Condition">
                            </px:PXDropDown>
                            <px:PXDropDown ID="edCategory" runat="server" DataField="Category">
                            </px:PXDropDown>
                            <px:PXSelector ID="edUOM" runat="server" DataField="UOM">
                            </px:PXSelector>
                        </Template>
                    </px:PXFormView>
                </Template>
			</px:PXTabItem>
		</Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" />
	</px:PXTab>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="EA301000.aspx.cs" Inherits="Page_EA301000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="AG.EA.AssetEntry" PrimaryView="Assets" SuspendUnloading="False">
        <CallbackCommands>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Assets" Style="z-index: 100" Width="100%" TabIndex="6700" NotifyIndicator="True">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" LabelsWidth="SM" />
            <px:PXSelector runat="server" DataField="AssetCD" ID="edAssetCD" Size="M"></px:PXSelector>
            <px:PXTextEdit runat="server" DataField="Description" DefaultLocale="" AlreadyLocalized="False" ID="edDescription" Size="XXL"></px:PXTextEdit>

        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Asset Details">
                <Template>
                    <px:PXFormView ID="fvAssetDetails" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="CurrentAsset"
                        SkinID="Transparent" CaptionVisible="False" AllowCollapse="False" TabIndex="13200">
                        <Template>
                            <px:PXLayoutRule runat="server" GroupCaption="Asset Summary" StartColumn="True" StartGroup="True" LabelsWidth="SM" />
                            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
                            <px:PXCheckBox runat="server" Text="Hold" DataField="Hold" CommitChanges="True" AlreadyLocalized="False" ID="edHold"></px:PXCheckBox>
                            <px:PXSelector runat="server" DataField="ClassID" ID="edClassID" CommitChanges="True" AllowEdit="True" edit="1"></px:PXSelector>
                            <px:PXDropDown runat="server" DataField="State" ID="edState"></px:PXDropDown>
                            <px:PXDropDown runat="server" DataField="Category" ID="edCategory"></px:PXDropDown>
                            <px:PXDropDown runat="server" DataField="Condition" ID="edCondition"></px:PXDropDown>
                            <px:PXSelector runat="server" DataField="UOM" ID="edUOM" AllowEdit="True" edit="1"></px:PXSelector>
                            <px:PXNumberEdit runat="server" DataField="Cost" AlreadyLocalized="False" ID="edCost"></px:PXNumberEdit>
                            <px:PXNumberEdit runat="server" DataField="Qty" AlreadyLocalized="False" ID="edQty"></px:PXNumberEdit>
                            <px:PXDateTimeEdit runat="server" DataField="IssueDate" AlreadyLocalized="False" ID="edIssueDate"></px:PXDateTimeEdit>
                            <px:PXDateTimeEdit runat="server" DataField="PlacedInServiceDate" AlreadyLocalized="False" ID="edPlacedInServiceDate"></px:PXDateTimeEdit>
                            <px:PXDateTimeEdit runat="server" DataField="DisposalDate" AlreadyLocalized="False" ID="edDisposalDate"></px:PXDateTimeEdit>
                            <px:PXTextEdit runat="server" DataField="DisposalReason" AlreadyLocalized="False" ID="edDisposalReason" Enabled="False"></px:PXTextEdit>
                            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" GroupCaption="Tracking Info" StartGroup="True"></px:PXLayoutRule>
                            <px:PXDateTimeEdit runat="server" DataField="LastTransportationDate" AlreadyLocalized="False" ID="edLastTransportationDate"></px:PXDateTimeEdit>
                            <px:PXFormView ID="fvLocation" runat="server" DataSourceID="ds" DataMember="AssetLocation" RenderStyle="Simple" TabIndex="6300">
                                <Template>
                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM"></px:PXLayoutRule>
                                    <px:PXSegmentMask CommitChanges="True" ID="edBranchID" runat="server" DataField="BranchID" AllowEdit="True" DataSourceID="ds">
                                        <CallBackMode CommitChanges="True"></CallBackMode>
                                    </px:PXSegmentMask>
                                    <px:PXSelector CommitChanges="True" ID="edEmployeeID" runat="server" DataField="EmployeeID" AllowEdit="True" DataSourceID="ds" edit="1"></px:PXSelector>
                                    <px:PXSelector CommitChanges="True" ID="edDepartment" runat="server" DataField="Department" AllowEdit="True" DataSourceID="ds" edit="1"></px:PXSelector>
                                    <px:PXSegmentMask CommitChanges="True" ID="edSiteID" runat="server" DataField="SiteID" AllowEdit="True" edit="1"></px:PXSegmentMask>
                                    <px:PXSelector CommitChanges="True" ID="edBuildingID" runat="server" DataField="BuildingID" AllowEdit="True" AutoRefresh="True"
                                        DataSourceID="ds" edit="1">
                                    </px:PXSelector>
                                    <px:PXTextEdit CommitChanges="True" ID="edFloor" runat="server" DataField="Floor" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
                                    <px:PXTextEdit CommitChanges="True" ID="edRoom" runat="server" DataField="Room" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
                                    <px:PXTextEdit CommitChanges="True" ID="edReason" runat="server" DataField="Reason" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
                                </Template>
                            </px:PXFormView>
                            <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Other Info" LabelsWidth="SM"></px:PXLayoutRule>
                            <px:PXSelector runat="server" DataField="LCARegistrationID" ID="edLCARegistrationID" AllowEdit="True" Enabled="False"></px:PXSelector>
                            <px:PXSelector runat="server" DataField="LCADisposalID" ID="edLCADisposalID" AllowEdit="True" Enabled="False"></px:PXSelector>
                            <px:PXSegmentMask runat="server" DataField="InventoryID" ID="edInventoryID" AllowEdit="True" Enabled="False"></px:PXSegmentMask>
                            <px:PXTextEdit runat="server" DataField="LotSerialNbr" AlreadyLocalized="False" ID="edLotSerialNbr" Enabled="False"></px:PXTextEdit>
                            <px:PXTextEdit runat="server" DataField="ExtRefID" AlreadyLocalized="False" ID="edExtRefID"></px:PXTextEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Location History">
                <Template>
                    <px:PXGrid SkinID="Details" ID="gridLocationHistory" BorderWidth="0px" runat="server" DataSourceID="ds" Style="z-index: 100; height: 327px;"
                        Width="100%" AllowPaging="True" AdjustPageSize="Auto" ActionsPosition="Top" AllowSearch="True" Height="327px" TabIndex="7800" TemporaryFilterCaption="Filter Applied">
                        <Mode AllowAddNew="False" AllowUpdate="False" AllowDelete="False"></Mode>
                        <Levels>
                            <px:PXGridLevel DataMember="LocationHistory" DataKeyNames="AssetID,RevisionID">
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM"></px:PXLayoutRule>
                                    <px:PXDateTimeEdit ID="edTransactionDate1" runat="server" DataField="TransactionDate" AlreadyLocalized="False" DefaultLocale=""></px:PXDateTimeEdit>
                                    <px:PXSegmentMask ID="edBranchID" runat="server" DataField="BranchID" AllowEdit="True" AutoRefresh="True"></px:PXSegmentMask>
                                    <px:PXSelector ID="edBuildingID" runat="server" DataField="BuildingID" AllowEdit="True" AutoRefresh="True" edit="1"></px:PXSelector>
                                    <px:PXTextEdit ID="edFloor" runat="server" DataField="Floor" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
                                    <px:PXTextEdit ID="edRoom" runat="server" DataField="Room" AlreadyLocalized="False" DefaultLocale=""></px:PXTextEdit>
                                    <px:PXSelector ID="edEmployeeID" runat="server" DataField="EmployeeID" AllowEdit="True" AutoRefresh="True" edit="1"></px:PXSelector>
                                    <px:PXSelector ID="edDepartment" runat="server" DataField="Department" AllowEdit="True" AutoRefresh="True" edit="1"></px:PXSelector>
                                    <px:PXSelector ID="edSiteID" runat="server" DataField="SiteID" AllowEdit="True" AutoRefresh="True" edit="1"></px:PXSelector>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn Width="99px" DataField="TransactionDate"></px:PXGridColumn>
                                    <px:PXGridColumn Width="80px" DataField="BranchID" DisplayFormat="&gt;AAAAAA"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="BuildingID" Width="80px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Floor" Width="36px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Room" Width="36px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EmployeeID" Width="80px"></px:PXGridColumn>
                                    <px:PXGridColumn AllowUpdate="False" DataField="EmployeeID_EPEmployee_acctName" Label="Employee Name" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Department" DisplayFormat="&gt;aaaaaaaaaa" Width="80px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SiteID" Width="80px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Reason" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn AllowUpdate="False" DataField="LastModifiedByID_Modifier_username" Label="Last Modified By" Width="90px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="LastModifiedDateTime" DisplayFormat="g" Width="120px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybills">
                <Template>
                    <px:PXGrid ID="Waybills" TabIndex="13200"
                        runat="server"
                        Style="z-index: 100; height: 372px;"
                        Height="327px"
                        BorderWidth="0px"
                        ActionsPosition="Top"
                        AllowPaging="True" DataSourceID="ds" SkinID="Inquire" TemporaryFilterCaption="Filter Applied" Width="100%">

                        <Levels>
                            <px:PXGridLevel DataMember="Waybills" DataKeyNames="AssetID,WaybillCD">
                                <RowTemplate>
                                    <px:PXSelector ID="edWaybillCD" runat="server" DataField="WaybillCD" CommitChanges="true" AllowEdit="true" Enabled="false">
                                    </px:PXSelector>
                                    <px:PXDropDown ID="edRSWaybill__Status" runat="server" DataField="RSWaybill__Status" Enabled="false">
                                    </px:PXDropDown>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillCD" CommitChanges="true">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSWaybill__Status">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
    <px:PXSmartPanel ID="spDisposalDetails" runat="server" Key="DisposalDialog" Caption="Disposal Details" CaptionVisible="True"
        DesignView="Content" TabIndex="1500" AlreadyLocalized="False" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="true" AutoCallBack-Target="fvDisposalDetails"
        CallBackMode-CommitChanges="True" CallBackMode-PostData="Page" AcceptButtonID="btnOK" AutoRepaint="true">
        <px:PXFormView ID="fvDisposalDetails" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="DisposalDialog"
            CaptionVisible="False" TabIndex="13901" SkinID="Transparent">
            <Template>
                <px:PXLayoutRule runat="server" ControlSize="M" LabelsWidth="SM" StartRow="True" />
                <px:PXTextEdit ID="edDisposalReason" runat="server" DataField="DisposalReason" CommitChanges="true"></px:PXTextEdit>
            </Template>
        </px:PXFormView>
        <px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
            <px:PXButton ID="btnOK" runat="server" Text="Dispose" DialogResult="OK"></px:PXButton>
            <px:PXButton ID="btnCancel" runat="server" DialogResult="Cancel" Text="Cancel"></px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="EA303000.aspx.cs" Inherits="Page_EA303000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="AG.EA.LCADisposalEntry" PrimaryView="LCADisposals">
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="LCADisposals" TabIndex="5100">
        <Template>
            <px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartColumn="True" LabelsWidth="S" ControlSize="S" />
            <px:PXSelector ID="edLCADisposalCD" runat="server" DataField="LCADisposalCD">
            </px:PXSelector>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status">
            </px:PXDropDown>
            <px:PXCheckBox ID="edHold" Size="S" runat="server" AlreadyLocalized="False" DataField="Hold" Text="Hold" CommitChanges="true">
            </px:PXCheckBox>
            <px:PXLayoutRule runat="server" ControlSize="S" LabelsWidth="S" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edDisposalReason" runat="server" AlreadyLocalized="False" DataField="DisposalReason" DefaultLocale="" Size="L">
            </px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" DataMember="LCADisposalDetails">
        <Items>
            <px:PXTabItem Text="Details">
                <Template>
                    <px:PXGrid ID="PXGrid1" AdjustPageSize="Auto" runat="server" DataSourceID="ds" Width="100%" SyncPosition="True" SkinID="DetailsInTab" KeepPosition="True" TemporaryFilterCaption="Filter Applied">
                        <Levels>
                            <px:PXGridLevel DataMember="LCADisposalDetails">
                                <RowTemplate>
                                    <px:PXSelector ID="edAssetID" runat="server" DataField="AssetID" CommitChanges="True" AutoRefresh="True" AllowEdit="true">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edEAAsset__Description" runat="server" AlreadyLocalized="False" DataField="EAAsset__Description" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXDropDown ID="edEAAsset__Status" runat="server" DataField="EAAsset__Status">
                                    </px:PXDropDown>
                                    <px:PXDropDown ID="edEAAsset__Condition" runat="server" DataField="EAAsset__Condition">
                                    </px:PXDropDown>
                                    <px:PXSelector ID="edEAAsset__UOM" runat="server" DataField="EAAsset__UOM">
                                    </px:PXSelector>
                                    <px:PXNumberEdit ID="edEAAsset__Cost" runat="server" AlreadyLocalized="False" DataField="EAAsset__Cost" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXSelector ID="edEAAssetLocationHistory__BuildingID" runat="server" DataField="EAAssetLocationHistory__BuildingID">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edEAAssetLocationHistory__Floor" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__Floor" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXTextEdit ID="edEAAssetLocationHistory__Room" runat="server" AlreadyLocalized="False" DataField="EAAssetLocationHistory__Room" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXSelector ID="edEAAssetLocationHistory__EmployeeID" runat="server" DataField="EAAssetLocationHistory__EmployeeID">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edEAAssetLocationHistory__Department" runat="server" DataField="EAAssetLocationHistory__Department">
                                    </px:PXSelector>
                                    <px:PXSegmentMask ID="edEAAssetLocationHistory__SiteID" runat="server" DataField="EAAssetLocationHistory__SiteID">
                                    </px:PXSegmentMask>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="AssetID" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__Description" Width="200px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__Cost" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__UOM">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__BranchID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__BuildingID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__Floor">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__Room">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__EmployeeID" Width="120px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__Department">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAssetLocationHistory__SiteID" Width="120px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__Status">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__Condition">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                        <Mode AllowUpload="True" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

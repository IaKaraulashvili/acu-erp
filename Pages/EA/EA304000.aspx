<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="EA304000.aspx.cs" Inherits="Page_EA304000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="AG.EA.LCATransferEntry" PrimaryView="Document">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="addAsset" Visible="false"></px:PXDSCallbackCommand>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Document" TabIndex="300">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
            <px:PXSelector runat="server" DataField="LCATransferCD" ID="edLCATransferCD"></px:PXSelector>
            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
            <px:PXCheckBox runat="server" Text="Hold" DataField="Hold" AlreadyLocalized="False" ID="edHold" CommitChanges="True"></px:PXCheckBox>
            <px:PXCheckBox runat="server" Text="Create Waybill" CommitChanges="True" DataField="CreateWaybill" AlreadyLocalized="False" ID="edCreateWaybill"></px:PXCheckBox>
            <px:PXDateTimeEdit runat="server" DataField="CreatedDateTime" AlreadyLocalized="False" ID="edCreatedDateTime" Size="Empty" DefaultLocale=""></px:PXDateTimeEdit>
            <px:PXDateTimeEdit runat="server" DataField="ReleaseDate" AlreadyLocalized="False" ID="edReleaseDate" Size="Empty"></px:PXDateTimeEdit>
            <px:PXTextEdit runat="server" DataField="TransferReason" AlreadyLocalized="False" ID="edTransferReason"></px:PXTextEdit>

            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M"></px:PXLayoutRule>
            <px:PXLayoutRule runat="server" StartGroup="True" GroupCaption="Destination"></px:PXLayoutRule>
            <px:PXSegmentMask runat="server" DataField="ToBranchID" ID="edToBranchID" AllowEdit="True" CommitChanges="True"></px:PXSegmentMask>
            <px:PXSelector runat="server" DataField="ToEmployeeID" ID="edToEmployeeID" AllowEdit="True" CommitChanges="True" edit="1"></px:PXSelector>
            <px:PXSelector runat="server" DataField="ToDepartment" ID="edToDepartment" AllowEdit="True" CommitChanges="True" edit="1"></px:PXSelector>
            <px:PXSegmentMask runat="server" DataField="ToSiteID" ID="edToSiteID" AllowEdit="True" CommitChanges="True"></px:PXSegmentMask>
            <px:PXSelector runat="server" DataField="ToBuildingID" ID="edToBuildingID" AllowEdit="True" CommitChanges="True" edit="1"></px:PXSelector>
            <px:PXTextEdit runat="server" DataField="ToFloor" AlreadyLocalized="False" ID="edToFloor" CommitChanges="True"></px:PXTextEdit>
            <px:PXTextEdit runat="server" DataField="ToRoom" AlreadyLocalized="False" ID="edToRoom" CommitChanges="True"></px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Details">
                <Template>
                    <px:PXGrid ID="gvDetails" AdjustPageSize="Auto" runat="server" DataSourceID="ds" Width="100%" SyncPosition="True" SkinID="DetailsInTab" KeepPosition="True" TemporaryFilterCaption="Filter Applied" TabIndex="4200">
                        <Levels>
                            <px:PXGridLevel DataMember="Details" DataKeyNames="LCATransferID,AssetID">
                                <RowTemplate>
                                    <px:PXSelector runat="server" DataField="AssetID" ID="edAssetID" AllowEdit="True" CommitChanges="True" edit="1"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="EAAsset__Description" AlreadyLocalized="False" ID="edEAAsset__Description"></px:PXTextEdit>
                                    <px:PXNumberEdit runat="server" DataField="Cost" AlreadyLocalized="False" ID="Cost"></px:PXNumberEdit>
                                    <px:PXSelector runat="server" DataField="UOM" ID="edUOM"></px:PXSelector>
                                    <px:PXSegmentMask runat="server" DataField="BranchID" ID="edBranchID" AllowEdit="True"></px:PXSegmentMask>
                                    <px:PXSelector runat="server" DataField="BuildingID" ID="edBuildingID" AllowEdit="True" edit="1"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="Floor" AlreadyLocalized="False" ID="edFloor"></px:PXTextEdit>
                                    <px:PXTextEdit runat="server" DataField="Room" AlreadyLocalized="False" ID="edRoom"></px:PXTextEdit>
                                    <px:PXSelector runat="server" DataField="EmployeeID" ID="edEmployeeID" AllowEdit="True" edit="1"></px:PXSelector>
                                    <px:PXSelector runat="server" DataField="Department" ID="edDepartment" AllowEdit="True" edit="1"></px:PXSelector>
                                    <px:PXSegmentMask runat="server" DataField="SiteID" ID="edSiteID" AllowEdit="True"></px:PXSegmentMask>
                                    <px:PXDropDown runat="server" DataField="EAAsset__Condition" ID="edEAAsset__Condition"></px:PXDropDown>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="AssetID" CommitChanges="True"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EAAsset__Description" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="Cost" Width="100px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="UOM"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="BranchID" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="BuildingID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Floor"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Room"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EmployeeID" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Department"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="SiteID" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Condition"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                        <Mode AllowUpload="True" />
                        <ActionBar>
                            <CustomItems>
                                <px:PXToolBarButton Text="Add Item" Key="cmdASI" AlreadyLocalized="False" SuppressHtmlEncoding="False">
                                    <AutoCallBack Command="AddAsset" Target="ds">
                                        <Behavior PostData="Page" CommitChanges="True" />
                                    </AutoCallBack>
                                </px:PXToolBarButton>
                            </CustomItems>
                        </ActionBar>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybill Source" VisibleExp="DataControls[&quot;edCreateWaybill&quot;].Value==1" BindingContext="form">
                <Template>
                    <px:PXFormView ID="PXFormView1" runat="server" DataSourceID="ds" DataMember="WaybillSource" SkinID="Transparent">
                        <Template>
                            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" ControlSize="M" />
                            <px:PXSelector ID="edWaybillRefNbr" runat="server" DataField="WaybillRefNbr" AllowEdit="true" Enabled="false">
                            </px:PXSelector>
                            <px:PXSelector ID="edWaybillSourceBuildingID" runat="server" CommitChanges="True" DataField="WaybillSourceBuildingID">
                            </px:PXSelector>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
    <px:PXSmartPanel ID="spAssets" runat="server" Key="Assets" LoadOnDemand="true" Width="1100px" Height="650px"
        Caption="Asset Lookup" CaptionVisible="true" AutoCallBack-Command="Refresh" AutoCallBack-Target="fvAssetFilter"
        DesignView="Hidden" AlreadyLocalized="False" CreateOnDemand="True" TabIndex="3800">
        <px:PXFormView ID="fvAssetFilter" runat="server" CaptionVisible="False" DataMember="assetFilter" DataSourceID="ds"
            Width="100%" SkinID="Transparent" TabIndex="2000">
            <Template>
                <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M"></px:PXLayoutRule>
                <px:PXSegmentMask runat="server" DataField="BranchID" ID="edBranchID" CommitChanges="True"></px:PXSegmentMask>
                <px:PXSelector runat="server" DataField="BuildingID" ID="edBuildingID" CommitChanges="True"></px:PXSelector>
                <px:PXTextEdit runat="server" DataField="Floor" AlreadyLocalized="False" ID="edFloor" CommitChanges="True"></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="Room" AlreadyLocalized="False" ID="edRoom" CommitChanges="True"></px:PXTextEdit><px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M"></px:PXLayoutRule>
                <px:PXSelector runat="server" DataField="EmployeeID" ID="edEmployeeID" CommitChanges="True"></px:PXSelector>
                <px:PXSelector runat="server" DataField="Department" ID="edDepartment" CommitChanges="True"></px:PXSelector>
                <px:PXSegmentMask runat="server" DataField="SiteID" ID="edSiteID" CommitChanges="True"></px:PXSegmentMask>
            </Template>
        </px:PXFormView>
        <px:PXGrid ID="gvAssets" runat="server" DataSourceID="ds" Style="border-width: 1px 0px; top: 0px; left: 0px;" AutoAdjustColumns="true"
            Width="100%" SkinID="Details" AdjustPageSize="Auto" Height="135px" AllowSearch="True" BatchUpdate="true" TabIndex="5400"
            TemporaryFilterCaption="Filter Applied">
            <CallbackCommands>
                <Refresh CommitChanges="true"></Refresh>
            </CallbackCommands>
            <ActionBar PagerVisible="False">
                <PagerSettings Mode="NextPrevFirstLast" />
            </ActionBar>
            <Levels>
                <px:PXGridLevel DataMember="Assets" DataKeyNames="AssetID">
                    <Mode AllowAddNew="false" AllowDelete="false" />
                    <RowTemplate>
                        <px:PXCheckBox runat="server" Text="Selected" DataField="Selected" AlreadyLocalized="False" ID="edSelected" CommitChanges="True"></px:PXCheckBox>
                        <px:PXSelector runat="server" DataField="AssetID" ID="edAssetID1"></px:PXSelector>
                        <px:PXTextEdit runat="server" DataField="Description" AlreadyLocalized="False" ID="edDescription1" DefaultLocale=""></px:PXTextEdit>
                        <px:PXNumberEdit runat="server" DataField="Cost" AlreadyLocalized="False" ID="edCost1" DefaultLocale=""></px:PXNumberEdit>
                        <px:PXSelector runat="server" DataField="UOM" ID="edUOM1"></px:PXSelector>
                        <px:PXSegmentMask runat="server" DataField="BranchID" ID="edBranchID1"></px:PXSegmentMask>
                        <px:PXSelector runat="server" DataField="BuildingID" ID="edBuildingID1"></px:PXSelector>
                        <px:PXTextEdit runat="server" DataField="Floor" AlreadyLocalized="False" ID="edFloor1" DefaultLocale=""></px:PXTextEdit>
                        <px:PXTextEdit runat="server" DataField="Room" AlreadyLocalized="False" ID="edRoom1" DefaultLocale=""></px:PXTextEdit>
                        <px:PXSelector runat="server" DataField="EmployeeID" ID="edEmployeeID1"></px:PXSelector>
                        <px:PXSelector runat="server" DataField="Department" ID="edDepartment1"></px:PXSelector>
                        <px:PXSegmentMask runat="server" DataField="SiteID" ID="edSiteID1"></px:PXSegmentMask>
                        <px:PXDropDown runat="server" DataField="Condition" ID="edCondition1"></px:PXDropDown>
                    </RowTemplate>
                    <Columns>
                        <px:PXGridColumn DataField="Selected" TextAlign="Center" Type="CheckBox" Width="80px" AutoCallBack="true" AllowCheckAll="true" CommitChanges="true" />
                        <px:PXGridColumn DataField="AssetID"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Description" Width="200px"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Cost" TextAlign="Right" Width="100px"></px:PXGridColumn>
                        <px:PXGridColumn DataField="UOM"></px:PXGridColumn>
                        <px:PXGridColumn DataField="BranchID" Width="120px"></px:PXGridColumn>
                        <px:PXGridColumn DataField="BuildingID"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Floor"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Room"></px:PXGridColumn>
                        <px:PXGridColumn DataField="EmployeeID" Width="120px"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Department"></px:PXGridColumn>
                        <px:PXGridColumn DataField="SiteID" Width="120px"></px:PXGridColumn>
                        <px:PXGridColumn DataField="Condition"></px:PXGridColumn>
                    </Columns>
                </px:PXGridLevel>
            </Levels>
            <AutoSize Enabled="True" />
        </px:PXGrid>
        <px:PXPanel ID="PXPanel5" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton6" runat="server" CommandName="addAssetSel" CommandSourceID="ds" Text="Add" SyncVisible="false" />
            <px:PXButton ID="PXButton7" runat="server" Text="Add & Close" DialogResult="OK" />
            <px:PXButton ID="PXButton8" runat="server" DialogResult="Cancel" Text="Cancel" />
        </px:PXPanel>
    </px:PXSmartPanel>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RA101000.aspx.cs" Inherits="Page_RA101000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="RA.RASetupMaint" PrimaryView="Setup" SuspendUnloading="False">
        <CallbackCommands>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Setup" TabIndex="32700">
        <Template>
            <px:PXLayoutRule runat="server" GroupCaption="NUMBERING SEQUENCE" StartGroup="True" LabelsWidth="M" ControlSize="XM" StartColumn="True" />
            <px:PXSelector ID="edRepossessedAssetsNumberingID" runat="server" DataField="RepossessedAssetsNumberingID"
                AllowEdit="True" edit="1" />
            <px:PXSelector ID="edRepossessionApplicationNumberingID" runat="server" DataField="RepossessionApplicationNumberingID"
                AllowEdit="True" edit="1" />
            <px:PXSelector ID="edLoanNumberingID" runat="server" DataField="LoanNumberingID"
                AllowEdit="True" edit="1" />
            <px:PXSelector ID="edEvaluationNumberingID" runat="server" DataField="EvaluationNumberingID"
                AllowEdit="True" edit="1" />
            <px:PXLayoutRule runat="server" StartGroup="True" LabelsWidth="M" ControlSize="XM" GroupCaption="DATA ENTRY SETTINGS" />
            <px:PXCheckBox ID="edHoldRepossession" runat="server" DataField="HoldRepossession" AlreadyLocalized="False" />
            <px:PXSelector ID="edCuryRateTypeID" runat="server" DataField="CuryRateTypeID" AllowEdit="True" edit="1" />
            <px:PXNumberEdit ID="edAssetAllocationAcceptableDeviationPct" runat="server" DataField="AssetAllocationAcceptableDeviationPct" AlreadyLocalized="False" LabelWidth="350px" />
            <px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="M" ControlSize="XM" StartGroup="True" GroupCaption="API INTEGRATION" />
            <px:PXTextEdit ID="edURL" runat="server" DataField="URL" AlreadyLocalized="False" />
            <px:PXTextEdit ID="edUserName" runat="server" DataField="UserName" AlreadyLocalized="False" />
            <px:PXTextEdit ID="edPassword" runat="server" DataField="Password" AlreadyLocalized="False" />
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="200" />
    </px:PXFormView>
</asp:Content>

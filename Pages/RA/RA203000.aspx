<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RA203000.aspx.cs" Inherits="Page_RA203000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" TypeName="RA.RAEvaluationMaint" PrimaryView="Evaluation">
		<CallbackCommands>
     		<px:PXDSCallbackCommand Visible="False" Name="CurrencyView" />
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Evaluation" TabIndex="32300">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" GroupCaption="Evaluation Info" LabelsWidth="SM" StartColumn="True" />
            <px:PXSelector runat="server" DataField="EvaluationCD" ID="edEvaluationCD"></px:PXSelector>
            <px:PXTextEdit runat="server" DataField="ExternalID" AlreadyLocalized="False" ID="edExternalID"></px:PXTextEdit>
            <px:PXSelector runat="server" DataField="AssetID" ID="edAssetID" AllowEdit="true"></px:PXSelector>
            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="EstimationType" ID="edEstimationType"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="EvaluationType" ID="edEvaluationType"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="EvaluationApproach" ID="edEvaluationApproach"></px:PXDropDown>
            <px:PXTextEdit runat="server" DataField="Appraiser" AlreadyLocalized="False" ID="edAppraiser"></px:PXTextEdit>
            <px:PXDateTimeEdit ID="edEvaluationRequestDate" runat="server" AlreadyLocalized="False" DataField="EvaluationRequestDate" DefaultLocale="">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit runat="server" DataField="EvaluationDate" AlreadyLocalized="False" ID="edEvaluationDate"></px:PXDateTimeEdit>
            <px:PXDateTimeEdit runat="server" DataField="InspectDate" AlreadyLocalized="False" ID="edInspectDate"></px:PXDateTimeEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" GroupCaption="Amount" LabelsWidth="SM" ControlSize="XM"></px:PXLayoutRule>
            <px:PXDateTimeEdit runat="server" DataField="CurrencyDate" CommitChanges="true" AlreadyLocalized="False" ID="edCurrencyDate"></px:PXDateTimeEdit>
           	<pxa:PXCurrencyRate DataField="CuryID" ID="edCury" runat="server"  DataMember="_Currency_" RateTypeView="_RAEvaluation_CurrencyInfo_"></pxa:PXCurrencyRate>
            <px:PXNumberEdit runat="server" DataField="FairMarketValueCury" CommitChanges="true" DefaultLocale="" AlreadyLocalized="False" ID="edFairMarketValueCury"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="SalvageValueCury" CommitChanges="true" DefaultLocale="" AlreadyLocalized="False" ID="edSalvageValueCury"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="EvaluationExpenses" AlreadyLocalized="False" ID="edEvaluationExpenses"></px:PXNumberEdit>
            <px:PXTextEdit ID="edComment" runat="server" AlreadyLocalized="False" DataField="Comment" DefaultLocale="" Height ="140" TextMode ="MultiLine">
            </px:PXTextEdit>
        </Template>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXFormView>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RA303000.aspx.cs" Inherits="Page_RA303000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="RA.RARepossessionApplicationEntry" PrimaryView="RepossessionApplication" SuspendUnloading="False">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="DeleteAsset" Visible="False" CommitChanges="True" />
            <px:PXDSCallbackCommand Name="AddAsset" Visible="False" CommitChanges="True" />
            <px:PXDSCallbackCommand CommitChanges="True" Name="showAddAsset" Visible="false" />
            <px:PXDSCallbackCommand Visible="false" Name="GetDataFromApi" CommitChanges="True"></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Visible="false" Name="OpenAsset" CommitChanges="True"></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="fillAllocation" Visible="false" CommitChanges="True"></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Visible="false" Name="Wnext" CommitChanges="True"></px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Visible="false" Name="OpenWizard" CommitChanges="True"></px:PXDSCallbackCommand>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXSmartPanel ID="PXSmartPanel1" runat="server" AcceptButtonID="PXButtonOK" AutoReload="true" CancelButtonID="PXButtonCancel"
        Caption="Repossession Asset" CaptionVisible="True" DesignView="Content" HideAfterAction="false" Width="500px" Key="NewAsset" AutoRepaint="true"
        Overflow="Hidden">
        <px:PXFormView ID="PXFormView2" runat="server" SkinID="Transparent" DataMember="NewAsset"
            DefaultControlID="edOwner">
            <Template>
                <px:PXLayoutRule ID="PXLayoutRule33" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" />
                <px:PXSelector runat="server" DataField="ClassID" CommitChanges="true" ID="edClassID" AllowEdit="True" edit="1"></px:PXSelector>
                <px:PXDropDown runat="server" DataField="Reason" CommitChanges="true" ID="edReason"></px:PXDropDown>
                <px:PXTextEdit runat="server" DataField="PropertyID" AlreadyLocalized="False" ID="edPropertyID" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="PlateNumber" AlreadyLocalized="False" ID="edPlateNumber" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="OwnerID" AlreadyLocalized="False" ID="edOwnerID" DefaultLocale=""></px:PXTextEdit>
                <px:PXTextEdit runat="server" DataField="CadastralCode" AlreadyLocalized="False" ID="PXTextEdit1" DefaultLocale=""></px:PXTextEdit>
            </Template>
        </px:PXFormView>
        <px:PXPanel ID="PXPanel2" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButton2" runat="server" DialogResult="Cancel" Text="Cancel" />
            <px:PXButton ID="PXButton6" runat="server" DialogResult="OK" Text="Get Data From API" SyncVisible="false">
                <AutoCallBack Command="GetDataFromApi" Target="ds"></AutoCallBack>
            </px:PXButton>
            <px:PXButton ID="PXButton1" runat="server" DialogResult="OK" Text="Next" SyncVisible="false">
                <AutoCallBack Command="OpenWizard" Target="ds"></AutoCallBack>
            </px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXSmartPanel ID="spPreloadArticlesDlg" runat="server" CommandName="OpenWizard" CommandSourceID="ds"
        Width="435" Key="AssetWizard" CaptionVisible="True" Caption="Add Repossession Asset"
        Style="display: none;" AutoCallBack-Enabled="true" AutoCallBack-Target="wizard1" AutoCallBack-Command="WizCancel">
        <px:PXWizard ID="wizard1" EnableExp="" runat="server" Width="800" Height="400" DataMember="AssetWizard"
            SkinID="Flat" OnSelectNextPage="wizard_OnSelectNextPage" ButtonsVisible="true">
            <SaveCommand Target="ds" Command="OpenAsset" />
            <NextCommand Target="ds" Command="wNext" />
            <Pages>
                <px:PXWizardPage Caption="Repossession Asset" Tooltip="Repossession Asset" Overflow="Scroll">
                    <Template>
                        <px:PXFormView ID="formPanel2" runat="server" SkinID="Transparent" DataMember="AssetWizard" Caption="General Info" RenderStyle="Fieldset" Width="800px">
                            <Template>
                                <px:PXLayoutRule ID="PXLayoutRule33" runat="server" StartRow="True" LabelsWidth="SM" ControlSize="M" StartGroup="true" />
                                <px:PXDropDown runat="server" DataField="RepossessionType" ID="edRepossessionType"></px:PXDropDown>
                                <px:PXTextEdit runat="server" DataField="Description" AlreadyLocalized="False" ID="edDescription"></px:PXTextEdit>
                                <px:PXDropDown runat="server" DataField="ExecutionType" ID="edExecutionType"></px:PXDropDown>
                                <px:PXDropDown runat="server" DataField="InspectionForm" ID="edInspectionForm"></px:PXDropDown>
                                <px:PXDropDown runat="server" DataField="AssetClassification" ID="edAssetClassification" CommitChanges="True"></px:PXDropDown>
                                <px:PXCheckBox runat="server" Text="Property Redeem" CommitChanges="True" DataField="PropertyRedeem" AlreadyLocalized="False" ID="edPropertyRedeem"></px:PXCheckBox>
                                <px:PXDateTimeEdit runat="server" DataField="InstalmentStartDate" AlreadyLocalized="False" ID="edInstalmentStartDate" DefaultLocale=""></px:PXDateTimeEdit>
                                <px:PXDateTimeEdit runat="server" DataField="InstalmentEndDate" AlreadyLocalized="False" ID="edInstalmentEndDate" DefaultLocale=""></px:PXDateTimeEdit>
                                <px:PXNumberEdit runat="server" DataField="legalExpenses" AlreadyLocalized="False" ID="edlegalExpenses" DefaultLocale=""></px:PXNumberEdit>
                                <px:PXSelector ID="edOption30PercProjectCurrency" runat="server" DataField="Option30PercProjectCurrency">
                                </px:PXSelector>
                                <px:PXNumberEdit ID="edOption30PercProjectAmountInCurrency" runat="server" AlreadyLocalized="False" DataField="Option30PercProjectAmountInCurrency">
                                </px:PXNumberEdit>
                                <px:PXSelector runat="server" DataField="RARecommendator" ID="edRARecommendator" AllowEdit="True" edit="1"></px:PXSelector>
                                <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM" StartGroup="true" GroupCaption="Expected Repossession Amount"></px:PXLayoutRule>
                                <px:PXDateTimeEdit runat="server" DataField="ExpectedCuryDate" AlreadyLocalized="False" ID="edExpectedCuryDate" Size="SM" CommitChanges="True"></px:PXDateTimeEdit>
                                <px:PXLayoutRule runat="server" Merge="True"></px:PXLayoutRule>
                                <px:PXSelector runat="server" DataField="ExpectedRepossessionCuryID" ID="edExpectedRepossessionCuryID" CommitChanges="True" Width="60px"></px:PXSelector>
                                <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionCuryRate" AlreadyLocalized="False" ID="edExpectedRepossessionCuryRate" SuppressLabel="True" Width="74px" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                                <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                                <px:PXNumberEdit runat="server" DataField="ExpectedRepossessionAmt" AlreadyLocalized="False" ID="edExpectedRepossessionAmt" Size="SM" CommitChanges="True" DefaultLocale=""></px:PXNumberEdit>
                                <px:PXCheckBox runat="server" Text="FixedRate" CommitChanges="true" DataField="FixedRate" AlreadyLocalized="False" ID="edFixedRate"></px:PXCheckBox>
                                <px:PXFormView runat="server" ID="PXFormViewLocationInfo" DataSourceID="ds" DataMember="RealEstateWizard" Caption="Location Info" RenderStyle="Fieldset" TabIndex="1900">
                                    <Template>
                                        <px:PXLayoutRule runat="server" StartColumn="true" LabelsWidth="SM" ControlSize="M"></px:PXLayoutRule>
                                        <px:PXDropDown ID="edRegion" runat="server" DataField="Region">
                                        </px:PXDropDown>
                                        <px:PXDropDown ID="edTbilisiDistrict" runat="server" DataField="TbilisiDistrict">
                                        </px:PXDropDown>
                                    </Template>
                                </px:PXFormView>
                            </Template>
                        </px:PXFormView>
                        <px:PXFormView ID="PXFormView4" runat="server" SkinID="Transparent" DataMember="RealEstateWizard" Caption="Inspection Fields" RenderStyle="Fieldset">
                            <Template>
                                <px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True" LabelsWidth="SM" ControlSize="M" />
                                <px:PXDropDown ID="edInformationSource" runat="server" DataField="InformationSource">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edWater" runat="server" AlreadyLocalized="False" DataField="Water" Text="Water">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edFencing" runat="server" AlreadyLocalized="False" DataField="Fencing" Text="Fencing">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edIrrigationSystem" runat="server" AlreadyLocalized="False" DataField="IrrigationSystem" Text="Irrigation System">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edElectricity" runat="server" AlreadyLocalized="False" DataField="Electricity" Text="Electricity">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edNaturalGas" runat="server" AlreadyLocalized="False" DataField="NaturalGas" Text="Natural Gas">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edSewerageSystem" runat="server" AlreadyLocalized="False" DataField="SewerageSystem" Text="Sewerage System">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edRoadType" runat="server" DataField="RoadType">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edCoastline" runat="server" DataField="Coastline">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edAreaCondition" runat="server" DataField="AreaCondition">
                                </px:PXDropDown>
                                <px:PXNumberEdit ID="edRoomQuantity" runat="server" AlreadyLocalized="False" DataField="RoomQuantity">
                                </px:PXNumberEdit>
                                <px:PXNumberEdit ID="edBuildingQuantityNumbering" runat="server" AlreadyLocalized="False" DataField="BuildingQuantityNumbering">
                                </px:PXNumberEdit>
                                <px:PXNumberEdit ID="edFloorQuantity" runat="server" AlreadyLocalized="False" DataField="FloorQuantity">
                                </px:PXNumberEdit>
                                <px:PXNumberEdit ID="edFloor" runat="server" AlreadyLocalized="False" DataField="Floor">
                                </px:PXNumberEdit>
                                <px:PXDropDown ID="edStatusOfConstruction" runat="server" DataField="StatusOfConstruction">
                                </px:PXDropDown>
                                <px:PXLayoutRule ID="PXLayoutRule2" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M"></px:PXLayoutRule>
                                <px:PXDropDown ID="edConstructionDate" runat="server" DataField="ConstructionDate">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edOwnerUseBuildingAsHabitat" runat="server" AlreadyLocalized="False" DataField="OwnerUseBuildingAsHabitat" Text="Owner Use Building As Habitat">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edRented" runat="server" AlreadyLocalized="False" DataField="Rented" Text="Rented" CommitChanges="True">
                                </px:PXDropDown>
                                <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                                <px:PXLayoutRule runat="server" Merge="True" ControlSize="Empty" LabelsWidth="0"></px:PXLayoutRule>
                                <px:PXNumberEdit ID="edMonthlyRentFeeCury" runat="server" AlreadyLocalized="False" DataField="MonthlyRentFeeCury" Width="60px" LabelWidth="150px" DefaultLocale="">
                                </px:PXNumberEdit>
                                <px:PXSelector ID="edCuryID" runat="server" DataField="CuryID" SuppressLabel="True" Width="40px">
                                </px:PXSelector>
                                <px:PXLayoutRule runat="server"></px:PXLayoutRule>
                                <px:PXDropDown ID="edApartmentType" runat="server" DataField="ApartmentType">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edApartmentInItalianYard" runat="server" AlreadyLocalized="False" DataField="ApartmentInItalianYard" Text="Apartment in Italian Yard" AlignLeft="True">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edBalcony" runat="server" AlreadyLocalized="False" DataField="Balcony" Text="Balcony">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edElevator" runat="server" AlreadyLocalized="False" DataField="Elevator" Text="Elevator">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edBathroom" runat="server" AlreadyLocalized="False" DataField="Bathroom" Text="Bathroom">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edToilet" runat="server" AlreadyLocalized="False" DataField="Toilet" Text="Toilet">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edFunctionalArea" runat="server" DataField="FunctionalArea" LabelWidth="150px" Width="200px">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edViticultureMicrozone" runat="server" DataField="ViticultureMicrozone" LabelWidth="150px" Width="200px">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edGeometryOfLandArea" runat="server" DataField="GeometryOfLandArea" LabelWidth="150px" Width="200px">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edRelief" runat="server" DataField="Relief" LabelWidth="150px" Width="200px">
                                </px:PXDropDown>
                            </Template>
                        </px:PXFormView>
                        <px:PXFormView DataMember="NonRealEstateWizard" ID="PXFormView6" runat="server" DataSourceID="ds">
                            <Template>
                                <px:PXLayoutRule runat="server" ColumnWidth="L" StartColumn="True" StartRow="True" StartGroup="true" LabelsWidth="M" GroupCaption="Non-real estate info">
                                </px:PXLayoutRule>
                                <px:PXTextEdit ID="edVINCode" runat="server" AlreadyLocalized="False" DataField="VINCode" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edChassisID" runat="server" AlreadyLocalized="False" DataField="ChassisID" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edTechPassportNumber" runat="server" AlreadyLocalized="False" DataField="TechPassportNumber" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edMark" runat="server" AlreadyLocalized="False" DataField="Mark" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edAutomotiveBodyType" runat="server" AlreadyLocalized="False" DataField="AutomotiveBodyType" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edColor" runat="server" AlreadyLocalized="False" DataField="Color" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXNumberEdit ID="edEngineVolume" runat="server" AlreadyLocalized="False" DataField="EngineVolume" Width="100%" DefaultLocale="">
                                </px:PXNumberEdit>
                                <px:PXTextEdit ID="edModel" runat="server" AlreadyLocalized="False" DataField="Model" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXDropDown ID="edVehicleCondition" runat="server" DataField="VehicleCondition">
                                </px:PXDropDown>
                                <px:PXLayoutRule LabelsWidth="M" runat="server" ColumnWidth="L" StartColumn="True">
                                </px:PXLayoutRule>
                                <px:PXDropDown AlignLeft="True" ID="edCustomsDuty" runat="server" AlreadyLocalized="False" DataField="CustomsDuty" Text="CustomsDuty">
                                </px:PXDropDown>
                                <px:PXDropDown ID="edTransmission" runat="server" DataField="Transmission">
                                </px:PXDropDown>
                                <px:PXNumberEdit ID="edMileage" runat="server" AlreadyLocalized="False" DataField="Mileage" Width="100%" DefaultLocale="">
                                </px:PXNumberEdit>
                                <px:PXDropDown ID="edWheelLocation" runat="server" DataField="WheelLocation">
                                </px:PXDropDown>
                                <px:PXTextEdit ID="edPurpose" runat="server" AlreadyLocalized="False" DataField="Purpose" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXDateTimeEdit ID="edIssueDate" runat="server" AlreadyLocalized="False" DataField="IssueDate" Width="100%" DefaultLocale="">
                                </px:PXDateTimeEdit>
                                <px:PXDateTimeEdit ID="edRegistrationDate" runat="server" AlreadyLocalized="False" DataField="RegistrationDate" Width="100%" DefaultLocale="">
                                </px:PXDateTimeEdit>
                                <px:PXTextEdit ID="edParameters" runat="server" AlreadyLocalized="False" DataField="Parameters" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edPropertyDescription" runat="server" AlreadyLocalized="False" DataField="PropertyDescription" DefaultLocale="">
                                </px:PXTextEdit>
                                <px:PXTextEdit ID="edSerialNumber" runat="server" AlreadyLocalized="False" DataField="SerialNumber" DefaultLocale="">
                                </px:PXTextEdit>
                            </Template>
                        </px:PXFormView>
                        <px:PXFormView ID="PXFormView3" runat="server" SkinID="Transparent" DataMember="AssetWizard" Caption="Other Info" RenderStyle="Fieldset">
                            <Template>
                                <px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True" StartGroup="true" LabelsWidth="SM" ControlSize="M" />
                                <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edOtherImprovements" DataField="OtherImprovements" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                                <px:PXTextEdit runat="server" AlreadyLocalized="False" ID="edComment" DataField="Comment" Height="40px" TextMode="MultiLine" LabelWidth="150px" Width="550px" DefaultLocale=""></px:PXTextEdit>
                            </Template>
                        </px:PXFormView>
                    </Template>
                </px:PXWizardPage>
                <px:PXWizardPage>
                    <Template>
                        <px:PXGrid ID="PXGrid3" runat="server" TabIndex="3400" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" SkinID="DetailsInTab" Width="800px" Height="400px" SyncPosition="True" KeepPosition="True">
                            <Levels>
                                <px:PXGridLevel DataMember="LoanWizard">
                                    <Columns>
                                        <px:PXGridColumn DataField="LMSID" Width="200px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="Validated" Type="CheckBox" Width="200px">
                                        </px:PXGridColumn>
                                    </Columns>
                                </px:PXGridLevel>
                            </Levels>
                            <ActionBar>
                                <CustomItems>
                                    <px:PXToolBarButton Text="Get Loans" Tooltip="Get Loans">
                                        <AutoCallBack Command="" Target="ds">
                                            <Behavior CommitChanges="True"></Behavior>
                                        </AutoCallBack>
                                    </px:PXToolBarButton>
                                </CustomItems>
                            </ActionBar>
                        </px:PXGrid>
                    </Template>
                </px:PXWizardPage>
                <px:PXWizardPage Caption="Asset Allocation" Tooltip="Asset Allocation" Overflow="Auto" LoadOnDemand="true">
                    <Template>
                        <px:PXFormView ID="PXFormView3" runat="server" SkinID="Transparent" DataMember="AssetAllocationsWizard" RenderStyle="Fieldset">
                            <Template>
                                <px:PXCheckBox ID="edValidated" runat="server" AlreadyLocalized="False" DataField="Validated" DefaultLocale="">
                                </px:PXCheckBox>
                            </Template>
                        </px:PXFormView>

                        <px:PXGrid runat="server" TabIndex="3000" Height="400px" SyncPosition="true" ID="grdAssets" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds" SkinID="Details" Width="100%">
                            <Levels>
                                <px:PXGridLevel DataMember="Allocation">
                                    <Columns>
                                        <px:PXGridColumn DataField="RAAsset__AssetCD">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__LoanCD">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__LMSID">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__CuryID">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__LoanAmtCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__OverdueInterestCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidPrincipleCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidInterestCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidPenaltyCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidInsuranceCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidCourtFeeCury" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidPrinciple" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidInterest" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidPenalty" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidInsurance" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="PaidCourtFee" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RemainingPrinciple" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RemainingInterest" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RemainingPenalty" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                        <px:PXGridColumn DataField="RemainingInsurance" TextAlign="Right" Width="100px">
                                        </px:PXGridColumn>
                                    </Columns>
                                </px:PXGridLevel>
                            </Levels>
                            <Mode AllowAddNew="False" />
                            <ActionBar>
                                <CustomItems>
                                    <px:PXToolBarButton Text="Fill" Tooltip="Fill">
                                        <AutoCallBack Command="FillAllocation" Target="ds">
                                            <Behavior CommitChanges="True"></Behavior>
                                        </AutoCallBack>
                                    </px:PXToolBarButton>
                                </CustomItems>
                            </ActionBar>
                        </px:PXGrid>
                    </Template>
                </px:PXWizardPage>
                <px:PXWizardPage LoadOnDemand="true" Overflow="Auto">
                    <Template>
                        <px:PXLayoutRule ID="PXLayoutRule1" runat="server" LabelsWidth="SM" ControlSize="XM"
                            StartColumn="True" ColumnSpan="2" />
                        <px:PXLayoutRule ID="PXLayoutRule5" runat="server" ColumnSpan="1" />
                        <px:PXLayoutRule ID="PXLayoutRule11" runat="server" StartColumn="True" ControlSize="M"
                            LabelsWidth="S" />
                        <px:PXLabel runat="server" ID="lblDetails1" Width="370px" Height="35px" Style="font-style: italic; overflow: visible; padding-left: 10px">Asset has been added succesfully</px:PXLabel>
                        <px:PXLabel runat="server" ID="PXLabel4" Width="370px" Height="20px" Style="font-style: italic; overflow: visible; padding-left: 10px">Note: On Asset entry page please review tabs :</px:PXLabel>
                        <px:PXLabel runat="server" ID="PXLabel1" Width="370px" Height="20px" Style="font-style: italic; overflow: visible; padding-left: 10px">1:Attachments</px:PXLabel>
                        <px:PXLabel runat="server" ID="PXLabel2" Width="370px" Height="20px" Style="font-style: italic; overflow: visible; padding-left: 10px">2.Property Owners</px:PXLabel>
                    </Template>
                </px:PXWizardPage>
            </Pages>
        </px:PXWizard>
    </px:PXSmartPanel>
    <px:PXSmartPanel ID="PXSmartPanel2" runat="server" AcceptButtonID="PXButtonOK" AutoReload="true" CancelButtonID="PXButtonCancel"
        Caption="Delete Asset" CaptionVisible="True" DesignView="Content" HideAfterAction="false" Width="300px" Key="Dialog" AutoRepaint="true"
        Overflow="Hidden">
        <px:PXFormView ID="PXFormView1" runat="server" SkinID="Transparent" DataMember="Dialog">
            <Template>
                <px:PXLayoutRule ID="PXLayoutRule33" runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="XM" />
                <px:PXLabel runat="server" CommitChanges="true" ID="edlbl1" Text="Are you sure to delete this record?" AllowEdit="True" edit="1"></px:PXLabel>
            </Template>
        </px:PXFormView>
        <px:PXPanel ID="PXPanel1" runat="server" SkinID="Buttons">
            <px:PXButton ID="PXButtonCancel" runat="server" DialogResult="Cancel" Text="Cancel" />
            <px:PXButton ID="PXButtonOK" runat="server" DialogResult="OK" Text="Yes">
            </px:PXButton>
        </px:PXPanel>
    </px:PXSmartPanel>
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="RepossessionApplication" TabIndex="4200" ActivityIndicator="True" NotifyIndicator="True">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="SM" />
            <px:PXSelector runat="server" DataField="RepossessionApplicationCD" ID="edRepossessionApplicationCD"></px:PXSelector>
            <px:PXDropDown runat="server" DataField="RepossessionType" ID="edRepossessionType"></px:PXDropDown>
            <px:PXDropDown runat="server" DataField="Status" ID="edStatus"></px:PXDropDown>
            <px:PXCheckBox runat="server" Text="Hold" CommitChanges="true" DataField="Hold" AlreadyLocalized="False" ID="edHold"></px:PXCheckBox>
            <px:PXSelector runat="server" DataField="LoanManager" ID="edLoanManager" CommitChanges="true"></px:PXSelector>
            <px:PXSelector runat="server" DataField="ReportsTo" ID="edReportsTo"></px:PXSelector>
            <px:PXDateTimeEdit runat="server" DataField="ReleaseDate" DefaultLocale="" AlreadyLocalized="False" ID="edReleaseDate"></px:PXDateTimeEdit>
            <px:PXDateTimeEdit runat="server" DataField="ApprovalDate" DefaultLocale="" AlreadyLocalized="False" ID="edApprovalDate"></px:PXDateTimeEdit>
            <px:PXLayoutRule runat="server" ColumnSpan="2"></px:PXLayoutRule>
            <px:PXTextEdit runat="server" DataField="Description" AlreadyLocalized="False" ID="edDescription" DefaultLocale=""></px:PXTextEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
            <px:PXNumberEdit runat="server" DataField="LMRemopossessionAmt" DefaultLocale="" AlreadyLocalized="False" ID="edLMRemopossessionAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalFairValue" DefaultLocale="" AlreadyLocalized="False" ID="edTotalFairValue"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalSalvageValue" DefaultLocale="" AlreadyLocalized="False" ID="edTotalSalvageValue"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="Difference" DefaultLocale="" AlreadyLocalized="False" ID="edDifference"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalDeffectiveAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalDeffectiveAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="RAManagerRemopossessionAmt" DefaultLocale="" AlreadyLocalized="False" ID="edRAManagerRemopossessionAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalLoanAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalLoanAmt"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="TotalPayedPrincipalAmt" DefaultLocale="" AlreadyLocalized="False" ID="edTotalPayedPrincipalAmt"></px:PXNumberEdit>
            <px:PXLayoutRule runat="server" StartColumn="True" ControlSize="M" LabelsWidth="SM"></px:PXLayoutRule>
            <px:PXNumberEdit runat="server" DataField="LTV" DefaultLocale="" AlreadyLocalized="False" ID="edLTV"></px:PXNumberEdit>
            <px:PXNumberEdit runat="server" DataField="LTVWithoutPenalty" DefaultLocale="" AlreadyLocalized="False" ID="edLTVWithoutPenalty"></px:PXNumberEdit>
            <px:PXCheckBox runat="server" Text="Insider" DataField="Insider" AlreadyLocalized="False" ID="edInsider"></px:PXCheckBox>
            <px:PXNumberEdit runat="server" DataField="CapitalSharePct" DefaultLocale="" AlreadyLocalized="False" ID="edCapitalSharePct"></px:PXNumberEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">

        <Items>
            <px:PXTabItem Text="Assets">
                <Template>
                    <px:PXGrid runat="server" TabIndex="3000" Height="500px" SyncPosition="true" ID="grdAssets" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds" SkinID="Details" Width="100%">
                        <Levels>
                            <px:PXGridLevel DataMember="RepossessionApplicationAsset" DataKeyNames="RepossessionApplicationAssetID,RepossessionApplicationID">
                                <Columns>
                                    <px:PXGridColumn DataField="AssetID" LinkCommand="ViewAsset"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__AssetType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Reason"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__PropertyID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__Description" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__AssetClassification"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionCuryID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__ExpectedRepossessionAmtGel"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__RecommendedRepossessionAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__RecommendedRepossessionAmtGel"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationAmt"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationSalvageAmtCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RAAsset__LastEvaluationSalvageAmt"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <ActionBar DefaultAction="AddAsset" PagerVisible="False">
                            <CustomItems>
                                <px:PXToolBarButton Text="Add" Key="AddAsset" AlreadyLocalized="False" SuppressHtmlEncoding="False">
                                    <AutoCallBack Command="AddAsset" Target="ds">
                                    </AutoCallBack>
                                    <PopupCommand Command="Refresh" Target="grdAssets">
                                    </PopupCommand>
                                </px:PXToolBarButton>
                                <px:PXToolBarButton Text="Delete" Key="deleteAsset" Visible="False" AlreadyLocalized="False" SuppressHtmlEncoding="False">
                                    <AutoCallBack Command="DeleteAsset" Target="ds">
                                    </AutoCallBack>
                                    <PopupCommand Command="Refresh" Target="grdAssets">
                                    </PopupCommand>
                                </px:PXToolBarButton>
                            </CustomItems>
                        </ActionBar>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Evaluation">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Inquire" Width="100%" ID="PXGrid2" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds">
                        <Levels>
                            <px:PXGridLevel DataMember="Evaluation" DataKeyNames="EvaluationCD">
                                <Columns>
                                    <px:PXGridColumn DataField="EvaluationCD" LinkCommand="ViewEvaluation"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationDate" Width="90px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Appraiser" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="SalvageValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="FairMarketValueCury"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CuryID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="AssetID"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EstimationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationType"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="EvaluationApproach"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="InspectDate"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="CurrencyDate"></px:PXGridColumn>
                                    <px:PXGridColumn TextAlign="Right" DataField="EvaluationExpenses"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowAddNew="False" AllowDelete="False" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Loan Information">
                <Template>
                    <px:PXGrid runat="server" Height="500px" SkinID="Inquire" Width="100%" ID="PXGrid2" TemporaryFilterCaption="Filter Applied" AllowPaging="True" DataSourceID="ds">
                        <Levels>
                            <px:PXGridLevel DataMember="Loan" DataKeyNames="LoanCD">
                                <Columns>
                                    <px:PXGridColumn LinkCommand="ViewLoan" DataField="LoanCD" CommitChanges="True"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LMSID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanProduct" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanSegment" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanEndDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanDisbursementDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanTransferDateToPLO" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__ClientID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerContactInfo" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CoBorrowerInsider" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider1" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider2" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorName3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorID3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__GuarantorInsider3" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterestCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenalty" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CurrencyDate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsurance" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterest" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CuryID" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtCury" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtGel" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__BorrowerName" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__NBGLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__IFRSLoanProvisionRate" Width="200px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueDays" Width="200px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowAddNew="False" AllowDelete="False" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>


            <px:PXTabItem Text="Allocation">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" TabIndex="3400" TemporaryFilterCaption="Filter Applied" DataSourceID="ds" SkinID="DetailsInTab" Width="100%" SyncPosition="True" KeepPosition="True">
                        <Levels>
                            <px:PXGridLevel DataMember="Allocation" DataKeyNames="AssetAllocationID">
                                <RowTemplate>
                                    <px:PXLayoutRule runat="server" StartColumn="True" StartRow="True">
                                    </px:PXLayoutRule>
                                    <px:PXSelector ID="edRAAsset__AssetCD" runat="server" DataField="RAAsset__AssetCD">
                                    </px:PXSelector>
                                    <px:PXSelector ID="edRALoan__LoanCD" runat="server" DataField="RALoan__LoanCD">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edRALoan__LMSID" runat="server" AlreadyLocalized="False" DataField="RALoan__LMSID" DefaultLocale="">
                                    </px:PXTextEdit>
                                    <px:PXSelector ID="edRALoan__CuryID" runat="server" DataField="RALoan__CuryID">
                                    </px:PXSelector>
                                    <px:PXNumberEdit ID="edRALoan__LoanAmtCury" runat="server" AlreadyLocalized="False" DataField="RALoan__LoanAmtCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverduePrincipleCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverduePrincipleCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverdueInterestCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverdueInterestCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverduePenaltyCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverduePenaltyCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRALoan__OverdueInsuranceCury" runat="server" AlreadyLocalized="False" DataField="RALoan__OverdueInsuranceCury" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPrincipleCury" runat="server" AlreadyLocalized="False" DataField="PaidPrincipleCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInterestCury" runat="server" AlreadyLocalized="False" DataField="PaidInterestCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPenaltyCury" runat="server" AlreadyLocalized="False" DataField="PaidPenaltyCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXLayoutRule runat="server" StartColumn="True">
                                    </px:PXLayoutRule>
                                    <px:PXNumberEdit ID="edPaidInsuranceCury" runat="server" AlreadyLocalized="False" DataField="PaidInsuranceCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidCourtFeeCury" runat="server" AlreadyLocalized="False" DataField="PaidCourtFeeCury" CommitChanges="True" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPrinciple" runat="server" AlreadyLocalized="False" DataField="PaidPrinciple" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInterest" runat="server" AlreadyLocalized="False" DataField="PaidInterest" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidPenalty" runat="server" AlreadyLocalized="False" DataField="PaidPenalty" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidInsurance" runat="server" AlreadyLocalized="False" DataField="PaidInsurance" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edPaidCourtFee" runat="server" AlreadyLocalized="False" DataField="PaidCourtFee" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingPrinciple" runat="server" AlreadyLocalized="False" DataField="RemainingPrinciple" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingInterest" runat="server" AlreadyLocalized="False" DataField="RemainingInterest" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingPenalty" runat="server" AlreadyLocalized="False" DataField="RemainingPenalty" DefaultLocale="">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edRemainingInsurance" runat="server" AlreadyLocalized="False" DataField="RemainingInsurance" DefaultLocale="">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="RAAsset__AssetCD">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanCD">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LMSID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__CuryID">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__LoanAmtCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePrincipleCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInterestCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverduePenaltyCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RALoan__OverdueInsuranceCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPrincipleCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInterestCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPenaltyCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInsuranceCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidCourtFeeCury" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPrinciple" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInterest" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidPenalty" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidInsurance" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="PaidCourtFee" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingPrinciple" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingInterest" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingPenalty" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RemainingInsurance" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <Mode AllowFormEdit="True" />
                        <ActionBar>
                            <Actions>
                                <AddNew Enabled="False" />
                                <Delete Enabled="False" />
                            </Actions>
                            <CustomItems>
                                <px:PXToolBarButton Text="Fill" Tooltip="Fill">
                                    <AutoCallBack Command="FillAllocation" Target="ds">
                                        <Behavior CommitChanges="True"></Behavior>
                                    </AutoCallBack>
                                </px:PXToolBarButton>
                            </CustomItems>
                        </ActionBar>
                        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Comment" LoadOnDemand="true">
                <Template>
                    <px:PXFormView runat="server" DataMember="RepossessionApplicationCurrent" RenderStyle="Simple" DataSourceID="ds"
                        ID="RepossessionApplicatioID" TabIndex="30700">
                        <Template>
                            <px:PXRichTextEdit ID="edComment" runat="server" DataField="Comment" Style="border-width: 0px; border-top-width: 1px; width: 100%;"
                                AllowAttached="true" AllowSearch="true" AllowLoadTemplate="false" AllowSourceMode="true">
                                <AutoSize Enabled="True" MinHeight="216" />
                                <LoadTemplate TypeName="PX.SM.SMNotificationMaint" DataMember="Notifications" ViewName="NotificationTemplate" ValueField="notificationID" TextField="Name" DataSourceID="ds" Size="M" />
                            </px:PXRichTextEdit>
                        </Template>
                    </px:PXFormView>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

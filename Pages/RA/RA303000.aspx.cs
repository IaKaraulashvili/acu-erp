using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PX.Web.UI;
using RA;

public partial class Page_RA303000 : PX.Web.UI.PXPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void wizard_OnSelectNextPage(object sender, PXWizardSelectPageEventArgs e)
    {
        RARepossessionApplicationEntry graph = ds.DataGraph as RARepossessionApplicationEntry;
        if (graph == null) return;

        var index = graph.GetNextPage(((PXWizard)sender).SelectedIndex);

        e.NextPage = index;

    }
}


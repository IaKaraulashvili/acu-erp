<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS203000.aspx.cs" Inherits="Page_RS201000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="AG.RS.RSUnitMaint" PrimaryView="UOM" SuspendUnloading="False">
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="UOM" Caption="UOM">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" />
            <px:PXTextEdit ID="edUOM" runat="server" DataField="ID" />
            <px:PXTextEdit ID="edName" runat="server" DataField="Name" />
            <px:PXTextEdit ID="edRSName" runat="server" DataField="RSName" />
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="200" />
    </px:PXFormView>
</asp:Content>

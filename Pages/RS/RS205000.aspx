<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RS205000.aspx.cs" Inherits="Page_RS205000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="Drivers" SuspendUnloading="False" TypeName="AG.RS.DriverMaint" >
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
    <px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100"
		AllowPaging="True" AllowSearch="True" AdjustPageSize="Auto" DataSourceID="ds" SkinID="Primary" TabIndex="2100" TemporaryFilterCaption="Filter Applied">
<EmptyMsg ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here." NamedComboMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." NamedComboAddMessage="No records found as &#39;{0}&#39;.
Try to change filter or modify parameters above to see records here." FilteredMessage="No records found.
Try to change filter to see records here." FilteredAddMessage="No records found.
Try to change filter to see records here." NamedFilteredMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." NamedFilteredAddMessage="No records found as &#39;{0}&#39;.
Try to change filter to see records here." AnonFilteredMessage="No records found.
Try to change filter to see records here." AnonFilteredAddMessage="No records found.
Try to change filter to see records here."></EmptyMsg>
		<Levels>
			<px:PXGridLevel DataMember="Drivers">
			    <RowTemplate>
                    <px:PXMaskEdit ID="edDriverTaxID" runat="server" AlreadyLocalized="False" DataField="DriverTaxID" DefaultLocale="">
                    </px:PXMaskEdit>
                    <px:PXTextEdit ID="edDriverName" runat="server" AlreadyLocalized="False" DataField="DriverName" DefaultLocale="">
                    </px:PXTextEdit>
                    <px:PXCheckBox ID="edIsForeignCitizen" runat="server" AlreadyLocalized="False" DataField="IsForeignCitizen" Text="Is Foreign Citizen">
                    </px:PXCheckBox>
                    <px:PXTextEdit ID="edDefaultCarNumber" runat="server" AlreadyLocalized="False" DataField="DefaultCarNumber" DefaultLocale="">
                    </px:PXTextEdit>
                </RowTemplate>
			    <Columns>
                    <px:PXGridColumn DataField="DriverTaxID" Width="200px" CommitChanges="True">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="DriverName" Width="160px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="IsForeignCitizen" TextAlign="Center" Type="CheckBox" Width="60px">
                    </px:PXGridColumn>
                    <px:PXGridColumn DataField="DefaultCarNumber">
                    </px:PXGridColumn>
                </Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXGrid>
</asp:Content>

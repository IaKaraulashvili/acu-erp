<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RS304000.aspx.cs" Inherits="Page_RS304000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="AG.RS.TaxInvoiceRequestEntry" PrimaryView="TaxInvoiceRequest" SuspendUnloading="False">
        <CallbackCommands>
        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" DefaultControlID="edTaxInvoiceNbr"
        Width="100%" DataMember="TaxInvoiceRequest">
        <Template>
            <px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True" ControlSize="M" LabelsWidth="M" StartColumn="True" />
            <px:PXSelector ID="edTaxInvoiceNbr" runat="server" DataField="TaxInvoiceNbr" CommitChanges="true">
            </px:PXSelector>
            <px:PXSelector ID="edPostPeriod" runat="server" DataField="PostPeriod" CommitChanges="true">
            </px:PXSelector>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False">
            </px:PXDropDown>
            <px:PXCheckBox ID="edHold" runat="server" DataField="Hold" Text="Hold" CommitChanges="True">
            </px:PXCheckBox>

            <px:PXLayoutRule ID="PXLayoutRule2" runat="server" ControlSize="M" LabelsWidth="M" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXDateTimeEdit ID="edCreateDate" runat="server" DataField="CreateDate" Enabled="false">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edRequestDate" runat="server" DataField="RequestDate" Enabled="false">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edViewDate" runat="server" DataField="ViewDate" Enabled="false">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edConfirmDate" runat="server" DataField="ConfirmDate" Enabled="false">
            </px:PXDateTimeEdit>
            <px:PXTextEdit runat="server" DataField="TaxInvoiceNumber" ID="edTaxInvoiceNumber"></px:PXTextEdit>

            <px:PXLayoutRule ID="PXLayoutRule3" runat="server" StartRow="True" ControlSize="M" LabelsWidth="M" />

            <px:PXSelector runat="server" DataField="VendorID" ID="edVendorID" CommitChanges="true"></px:PXSelector>
            <px:PXTextEdit ID="edVendorTaxRegistrationID" runat="server" DataField="VendorTaxRegistrationID" Enabled="False">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edVendorName" runat="server" DataField="VendorName" Enabled="False">
            </px:PXTextEdit>

            <px:PXLayoutRule ID="PXLayoutRule4" runat="server" ControlSize="M" LabelsWidth="M" StartColumn="True" />
            <px:PXSelector runat="server" DataField="BranchID" ID="edBranchID" CommitChanges="true"></px:PXSelector>

            <px:PXTextEdit ID="edBranchTaxRegistrationID" runat="server" DataField="BranchTaxRegistrationID" Enabled="False">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edBranchName" runat="server" DataField="BranchName" Enabled="False">
            </px:PXTextEdit>

            <px:PXLayoutRule ID="PXLayoutRule5" runat="server" StartRow="True" ColumnSpan="2" LabelsWidth="M" Merge="True" />
            <px:PXTextEdit ID="edSenderNote" runat="server" DataField="SenderNote" TextMode="MultiLine" Height="50px">
            </px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="162px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Bill & Adjustment">
                <Template>
                    <px:PXGrid ID="PXGrid1" runat="server" DataSourceID="ds" TabIndex="3200" SkinID="Details" SyncPosition="true">
                        <Levels>
                            <px:PXGridLevel DataMember="BillsAndAdjustments" DataKeyNames="TaxInvoiceNbr,TaxInvoiceItemNbr">
                                <RowTemplate>
                                    <px:PXSelector runat="server" DataField="APInvoiceNbr" TextMode="Search" ID="edAPInvoiceNbr" CommitChanges="True" AutoRefresh="true"></px:PXSelector>
                                    <px:PXSelector runat="server" DataField="APInvoice__BatchNbr" ID="edAPInvoice__BatchNbr"></px:PXSelector>
                                    <px:PXTextEdit runat="server" DataField="APInvoice__BranchID" ID="edAPInvoice__BranchID"></px:PXTextEdit>
                                    <px:PXNumberEdit runat="server" DataField="APInvoice__CuryTaxTotal" ID="edAPInvoice__CuryTaxTotal"></px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="APInvoiceNbr" CommitChanges="True"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="APInvoice__BatchNbr"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="APInvoice__BranchID" Width="120px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="APInvoice__CuryTaxTotal" Width="100px"></px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybill">
                <Template>
                    <px:PXGrid runat="server" ID="PXGrid2" DataSourceID="ds" TabIndex="11200" SkinID="Inquire">
                        <Levels>
                            <px:PXGridLevel DataMember="Waybills" DataKeyNames="WaybillType, WaybillNbr">
                                <RowTemplate>
                                    <px:PXSelector runat="server" DataField="RSReceivedWaybill__WaybillNbr" ID="edRSReceivedWaybill__WaybillNbr"></px:PXSelector>
                                    <px:PXDropDown runat="server" DataField="RSReceivedWaybill__WaybillType" ID="edRSReceivedWaybill__WaybillType"></px:PXDropDown>
                                    <px:PXTextEdit runat="server" DataField="RSReceivedWaybill__WaybillNumber" ID="edRSReceivedWaybill__WaybillNumber"></px:PXTextEdit>
                                    <px:PXSelector ID="edRSReceivedWaybill__POReceiptNbr" runat="server" DataField="RSReceivedWaybill__POReceiptNbr">
                                    </px:PXSelector>
                                    <px:PXDateTimeEdit runat="server" DataField="RSReceivedWaybill__WaybillActivationDate" ID="edRSReceivedWaybill__WaybillActivationDate"></px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit runat="server" DataField="RSReceivedWaybill__TransStartDate" ID="edRSReceivedWaybill__TransStartDate"></px:PXDateTimeEdit>
                                    <px:PXNumberEdit ID="edRSReceivedWaybill__TotalAmount" runat="server" DataField="RSReceivedWaybill__TotalAmount">
                                    </px:PXNumberEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillNbr"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__POReceiptNbr"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillNumber" Width="80px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillActivationDate" Width="90px"></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillType" ></px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__TransStartDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__TotalAmount" Width="100px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
</asp:Content>

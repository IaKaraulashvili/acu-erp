<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RS306000.aspx.cs" Inherits="Page_RS306000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" PrimaryView="ReceivedTaxInvoice" SuspendUnloading="False" TypeName="AG.RS.ReceivedTaxInvoiceEntry">
        <CallbackCommands>
            <px:PXDSCallbackCommand Name="ViewPrevTaxInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="ViewNextTaxInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="DeleteTaxInvoiceItems" Visible="False">
            </px:PXDSCallbackCommand>

            <px:PXDSCallbackCommand Name="PrepaymentTaxInvoiceAction"
                Visible="false" CommitChanges="true" RepaintControlsIDs="PXGrid1" />

        </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="ReceivedTaxInvoice" TabIndex="1400">
        <Template>
            <px:PXLayoutRule runat="server" StartColumn="True" StartRow="True" ColumnWidth="XM" />
            <px:PXSelector ID="edTaxInvoiceNbr" runat="server" DataField="TaxInvoiceNbr">
            </px:PXSelector>
            <px:PXSelector ID="edPostPeriod" runat="server" DataField="PostPeriod" CommitChanges="True">
            </px:PXSelector>
            <px:PXCheckBox ID="edPrepaymentTaxInvoice" runat="server" AlreadyLocalized="False" DataField="PrepaymentTaxInvoice" Text="Prepayment Tax Invoice" CommitChanges="true">
                <AutoCallBack Command="PrepaymentTaxInvoiceAction" Target="ds">
                    <Behavior CommitChanges="True" RepaintControlsIDs="PXGrid1" />
                </AutoCallBack>
            </px:PXCheckBox>
            <px:PXDropDown ID="edStatus" runat="server" DataField="Status" Enabled="False">
            </px:PXDropDown>
            <px:PXTextEdit ID="edCorrectedRefNbr" runat="server" AlreadyLocalized="False" DataField="CorrectedRefNbr" Enabled="False">
                <LinkCommand Command="ViewPrevTaxInvoice" Target="ds">
                </LinkCommand>
            </px:PXTextEdit>
            <px:PXTextEdit ID="edRSReceivedTaxInvoiceCorrected__TaxInvoiceNbr" runat="server" DataField="RSReceivedTaxInvoiceCorrected__TaxInvoiceNbr" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
                <LinkCommand Command="ViewNextTaxInvoice" Target="ds">
                </LinkCommand>
            </px:PXTextEdit>

            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edTaxInvoiceSeriesNumber" runat="server" DataField="TaxInvoiceSeriesNumber" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceNumber" runat="server" DataField="TaxInvoiceNumber" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceSeries" runat="server" DataField="TaxInvoiceSeries" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edTaxInvoiceID" runat="server" DataField="TaxInvoiceID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edDeclarationNumber" runat="server" DataField="DeclarationNumber" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXDropDown ID="edTaxInvoiceStatus" runat="server" DataField="TaxInvoiceStatus" Enabled="False">
            </px:PXDropDown>

            <px:PXLayoutRule runat="server" StartColumn="True" ColumnWidth="XM">
            </px:PXLayoutRule>
            <px:PXDateTimeEdit ID="edCreateDate" runat="server" DataField="CreateDate" Enabled="False" Size="Empty" AlreadyLocalized="False" DefaultLocale="">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edRequestDate" runat="server" DataField="RequestDate" Enabled="False" Size="Empty" AlreadyLocalized="False" DefaultLocale="">
            </px:PXDateTimeEdit>
            <px:PXDateTimeEdit ID="edConfirmDate" runat="server" DataField="ConfirmDate" Enabled="False" Size="Empty" AlreadyLocalized="False" DefaultLocale="">
            </px:PXDateTimeEdit>
            <px:PXSelector runat="server" DataField="ConfirmedByID" ID="edConfirmedByID"></px:PXSelector>

            <px:PXLayoutRule runat="server" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXNumberEdit ID="edFullAmt" runat="server" AlreadyLocalized="False" DataField="FullAmt" DefaultLocale="">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edVATAmt" runat="server" AlreadyLocalized="False" DataField="VATAmt">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edTotalExciseAmt" runat="server" AlreadyLocalized="False" DataField="TotalExciseAmt">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edTotalAmt" runat="server" AlreadyLocalized="False" DataField="TotalAmt">
            </px:PXNumberEdit>

            <px:PXLayoutRule runat="server" StartRow="True">
            </px:PXLayoutRule>
            <px:PXLayoutRule runat="server" ColumnWidth="XM" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edInitialTaxInvoiceNbr" runat="server" DataField="InitialTaxInvoiceNbr" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edInitialTaxInvoiceID" runat="server" DataField="InitialTaxInvoiceID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>

            <px:PXLayoutRule runat="server" ColumnWidth="XM" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXDropDown ID="edCorrectionType" runat="server" DataField="CorrectionType" Enabled="False">
            </px:PXDropDown>
            <px:PXLayoutRule runat="server" ColumnWidth="XM" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXDateTimeEdit ID="edCorrectionDate" runat="server" DataField="CorrectionDate" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXDateTimeEdit>
            <px:PXLayoutRule runat="server" StartRow="True" ControlSize="M" LabelsWidth="M">
            </px:PXLayoutRule>

            <px:PXLayoutRule runat="server" GroupCaption="Vendor" StartColumn="True" StartRow="True">
            </px:PXLayoutRule>
            <px:PXSelector ID="edVendorID" runat="server" DataField="VendorID" AllowEdit="True" AllowAddNew="True" CommitChanges="True" AutoRefresh="True" edit="1">
            </px:PXSelector>
            <px:PXTextEdit ID="edVendorTaxRegistrationID" runat="server" DataField="VendorTaxRegistrationID" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edVendorName" runat="server" DataField="VendorName" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>

            <px:PXLayoutRule runat="server" GroupCaption="Branch" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXSegmentMask ID="edBranchID" runat="server" DataField="BranchID" CommitChanges="True">
            </px:PXSegmentMask>
            <px:PXTextEdit ID="edBranchTaxRegistrationID" runat="server" DataField="BranchTaxRegistrationID" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
            <px:PXTextEdit ID="edBranchName" runat="server" DataField="BranchName" Enabled="False" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>

            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True">
            </px:PXLayoutRule>
            <px:PXTextEdit ID="edNote" runat="server" DataField="Note" TextMode="MultiLine" AlreadyLocalized="False" DefaultLocale="">
            </px:PXTextEdit>
        </Template>
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
    <px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds">
        <Items>
            <px:PXTabItem Text="Document Details">
                <Template>
                    <px:PXGrid ID="PXGrid1" runat="server" DataSourceID="ds" SkinID="Details" TabIndex="3200" Width="100%" Height="300px" SyncPosition="True" TemporaryFilterCaption="Filter Applied" RepaintColumns="true">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataMember="ReceivedTaxInvoiceItems" DataKeyNames="TaxInvoiceNbr,TaxInvoiceItemNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edInventoryID" runat="server" DataField="InventoryID" CommitChanges="True" AllowEdit="True" AutoCallback="True">
                                    </px:PXSelector>
                                    <px:PXTextEdit ID="edItemName" runat="server" DataField="ItemName" AlreadyLocalized="False">
                                    </px:PXTextEdit>

                                    <px:PXTextEdit ID="edItemBarCode" runat="server" DataField="ItemBarCode" AlreadyLocalized="False">
                                    </px:PXTextEdit>

                                    <px:PXDropDown ID="edExtUOM" runat="server" DataField="ExtUOM">
                                    </px:PXDropDown>
                                    <px:PXNumberEdit ID="edUnitPrice" runat="server" DataField="UnitPrice" AlreadyLocalized="False">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edItemQty" runat="server" DataField="ItemQty" AlreadyLocalized="False">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edItemAmt" runat="server" DataField="ItemAmt" AlreadyLocalized="False">
                                    </px:PXNumberEdit>

                                    <px:PXNumberEdit ID="edVATAmt" runat="server" DataField="VATAmt" AlreadyLocalized="False">
                                    </px:PXNumberEdit>
                                    <px:PXNumberEdit ID="edExciseAmt" runat="server" DataField="ExciseAmt" AlreadyLocalized="False">
                                    </px:PXNumberEdit>

                                    <px:PXNumberEdit ID="edFullAmt" runat="server" AlreadyLocalized="False" DataField="FullAmt" DefaultLocale="">
                                    </px:PXNumberEdit>

                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="InventoryID" CommitChanges="True">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemName">
                                    </px:PXGridColumn>

                                    <px:PXGridColumn DataField="ItemBarCode">
                                    </px:PXGridColumn>

                                    <px:PXGridColumn DataField="ExtUOM">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="UnitPrice" TextAlign="Right" Width="100px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="ItemQty">
                                    </px:PXGridColumn>

                                    <px:PXGridColumn DataField="ItemAmt">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="VATAmt">
                                    </px:PXGridColumn>

                                    <px:PXGridColumn DataField="ExciseAmt">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="FullAmt">
                                    </px:PXGridColumn>

                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Bill and Adjustment">
                <Template>
                    <px:PXGrid ID="PXGrid3" runat="server" DataSourceID="ds" TabIndex="3200" SkinID="Details" SyncPosition="True" TemporaryFilterCaption="Filter Applied" Height="300px" Width="100%">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataMember="BillsAndAdjustments" DataKeyNames="TaxInvoiceNbr,TaxInvoiceItemNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edAPInvoiceNbr" runat="server" DataField="APInvoiceNbr" AllowEdit="True" AutoCallback="True">
                                    </px:PXSelector>
                                    <px:PXDateTimeEdit ID="edAPInvoice__DocDate" runat="server" AlreadyLocalized="False" DataField="APInvoice__DocDate" DefaultLocale="">
                                    </px:PXDateTimeEdit>
                                    <px:PXSelector ID="edAPTran__ReceiptNbr" runat="server" DataField="APTran__ReceiptNbr" AllowEdit="True" AutoCallback="True">
                                    </px:PXSelector>
                                    <px:PXDateTimeEdit ID="edPOReceipt__ReceiptDate" runat="server" AlreadyLocalized="False" DataField="POReceipt__ReceiptDate" DefaultLocale="">
                                    </px:PXDateTimeEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="APInvoiceNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="APInvoice__DocDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="APTran__ReceiptNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="POReceipt__ReceiptDate" Width="90px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
            <px:PXTabItem Text="Waybill">
                <Template>
                    <px:PXGrid ID="PXGrid2" runat="server" DataSourceID="ds" TabIndex="3200" Width="100%" Height="300px" SkinID="Inquire" TemporaryFilterCaption="Filter Applied">
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here."
                            AnonFilteredMessage="No records found.
Try to change filter to see records here."
                            ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here."
                            FilteredAddMessage="No records found.
Try to change filter to see records here."
                            FilteredMessage="No records found.
Try to change filter to see records here."
                            NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here."
                            NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here."
                            NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        <Levels>
                            <px:PXGridLevel DataMember="ReceivedTaxInvoiceWaybills" DataKeyNames="TaxInvoiceNbr,WaybillNbr">
                                <RowTemplate>
                                    <px:PXSelector ID="edWaybillNbr" runat="server" DataField="WaybillNbr" AllowEdit="True" AutoCallback="True" edit="1">
                                    </px:PXSelector>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="WaybillNbr">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillType" TextAlign="Right">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__WaybillActivationDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__TransStartDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="RSReceivedWaybill__TransStartTime" Width="90px">
                                    </px:PXGridColumn>
                                </Columns>

                            </px:PXGridLevel>
                        </Levels>
                        <AutoSize Enabled="True"></AutoSize>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        <AutoSize Container="Window" Enabled="True" MinHeight="150" />
    </px:PXTab>
    <%-- <CallbackCommands>
            <px:PXDSCallbackCommand Name="addAPInvoice" Visible="False">
            </px:PXDSCallbackCommand>
            <px:PXDSCallbackCommand Name="addAPInvoice2" Visible="False">
            </px:PXDSCallbackCommand>
        </CallbackCommands>--%>
</asp:Content>

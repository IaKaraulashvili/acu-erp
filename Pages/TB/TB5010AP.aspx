<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="TB5010AP.aspx.cs" Inherits="Page_TB5010AP" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Payments" TypeName="AcuErpTBC.TB.Extensions.AP.TBSendPaymentReportProcess">
		<CallbackCommands>
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid ID="grid" runat="server" Height="400px" Width="100%" Style="z-index: 100" AllowPaging="True" AllowSearch="true" Caption="Documents" DataSourceID="ds" BatchUpdate="True" AdjustPageSize="Auto"
		SkinID="PrimaryInquire" SyncPosition="True" FastFilterFields="RefNbr,VendorID,VendorID_Vendor_acctName">
		<Levels>
			<px:PXGridLevel DataMember="Payments">
				<Columns>
					<px:PXGridColumn AllowNull="False" DataField="Selected" TextAlign="Center" Type="CheckBox" AllowCheckAll="True" AllowSort="False" AllowMove="False" Width="30px" AllowOnDashboard="false" />
					<px:PXGridColumn DataField="DocType" Width="117px" RenderEditorText="True" />
					<px:PXGridColumn DataField="RefNbr" Width="108px" LinkCommand="viewDocument" />
					<px:PXGridColumn DataField="VendorID" Width="81px" />
					<px:PXGridColumn DataField="VendorID_Vendor_acctName" Width="140px" />
					<px:PXGridColumn DataField="AdjDate" Width="90px" />
					<px:PXGridColumn DataField="FinPeriodID" Width="63px" />
					<px:PXGridColumn AllowNull="False" DataField="CuryOrigDocAmt" TextAlign="Right" Width="81px" MatrixMode="true" />
					<px:PXGridColumn DataField="CuryID" Width="54px" />
					<px:PXGridColumn AllowNull="False" DataField="DocDesc" Width="180px" />
				</Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="400" />
	</px:PXGrid>
</asp:Content>
